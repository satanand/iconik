app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.Users = [];
    $scope.data.InterestedIn = [];   

    $scope.data.pageSize = 100;


    $( "#filterFromDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      maxDate: 0,
      onClose: function( selectedDate ) {
        $( "#filterToDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#filterToDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      maxDate: 0,
      onClose: function( selectedDate ) {
        $( "#filterFromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    
    
    $scope.getFilterData = function ()
    {        
        $scope.AssignedToUsersList();

        $scope.InterestedInList();

    }





    /*list*/

    $scope.applyFilter = function ()

    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        
        $scope.getList(0);

    }


    /*------------------------------------------------------------------
    Load master filters
    -----------------------------------------------------------------*/
    $scope.AssignedToUsersList = function ()
    {
        var data = 'SessionKey='+SessionKey;
        
        $http.post(API_URL+'enquiry/getAssignedToUsersList', data, contentType).then(function(response) 
        {
            var response = response.data;
            
            if(response.ResponseCode==200 && response.Users)
            {                
                if($scope.data.Users.length <= 0)
                {
                    for (var i in response.Users) 
                    {
                        $scope.data.Users.push(response.Users[i]);
                    }
                }
            }
        });
    }

    $scope.InterestedInList = function ()
    {
        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'enquiry/getInterestedInList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.InterestedIn)
            {
                if($scope.data.InterestedIn.length <= 0)
                {
                    for (var i in response.InterestedIn) 
                    {
                        $scope.data.InterestedIn.push(response.InterestedIn[i]);
                    }
                }
            }
        });
    }


    //Process reports---------------------------------------------------
    $scope.getList = function (IsSet)
    {
        if(IsSet == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }

        $("#totalRecords").find("b").html("");
        $("#totalRecords").hide();
        
        $("#action_btn").hide();    

        $scope.data.listLoading = true;   
        $scope.data.recordsLength = 0;     

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();     

        $http.post(API_URL+'reports/generateReport', data, contentType).then(function(response) 
        {
            //$("#reports_content").html("");

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                if($("#SearchType").val() == '' || $("#SearchType").val() == 's')
                    $("#reports_content").html(response.Data.Records);
                else
                {
                    $("#reports_content").find("tbody").html('');
                    $("#reports_content").find("tbody").html(response.Data.Records);
                }    

                $scope.data.recordsLength = response.Data.TotalRecords;

                if($scope.data.recordsLength > 0)
                {
                    $("#totalRecords").show();
                    $("#totalRecords").find("b").html($scope.data.recordsLength);
                    $("#action_btn").show();                
                }
                else
                {
                    $("#action_btn").hide();
                }
                
                $scope.data.pageNo++;                
            }
            else
            {
                $scope.data.noRecords = true;
            }
           
            $scope.data.listLoading = false;

        });

    }



    $scope.export_excel = function()
    {       
        var data = 'SessionKey='+SessionKey+'&Res=e&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        $scope.data.newDataList = [];
        $scope.data.tempList = [];
        
        $scope.data.excelLoading = true;

        $http.post(API_URL + 'reports/generateReport', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200 && response.Data) 
            {                
                for (var i in response.Data.Records) 
                {
                    $scope.data.newDataList.push(response.Data.Records[i]);
                }

                $("#dvjson").excelexportjs({
                    containerid: "dvjson"
                       , datatype: 'json'
                       , dataset: $scope.data.newDataList
                       , columns: getColumns($scope.data.newDataList)     
                }); 

                $scope.data.excelLoading = false;      
            }
        });        
    }


    $scope.loadFormAdd = function (Position, CategoryGUID)
    {
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

        $('#add_model').modal({show:true});
    }


    $scope.saveSearch = function ()
    {
        var SearchName = $("#SearchName").val();

        if(SearchName == "") { alert("Please enter search name."); return false; }

        $scope.addDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&SearchName='+SearchName+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'reports/saveSearch', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                $timeout(function()
                {
                   $('#add_model .close').click();
               }, 1000);

            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;
        });
    }


    $scope.search_history = function ()
    {
        $scope.data.search_history = [];

        $scope.data.listDataLoading = true;

        $scope.templateURLList = PATH_TEMPLATE+module+'/list_form.htm?'+Math.random();        

        $('#list_model').modal({show:true});

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'reports/searchHistory', data, contentType).then(function(response) 
        {
            $scope.data.listDataLoading = false;

            var response = response.data;

            if(response.ResponseCode == 200 && response.Data)
            {
                for (var i in response.Data.Records) 
                {
                    $scope.data.search_history.push(response.Data.Records[i]);
                }               
            }
        });
    }

    $scope.searchDelete = function (SID)
    {
        if(SID <= 0) return false;

        alertify.confirm('Are you sure you want to delete?', function() 
        {
            $scope.data.deleteDataLoading = true;

            var data = 'SessionKey='+SessionKey+"&did="+SID;

            $http.post(API_URL+'reports/deleteSearch', data, contentType).then(function(response) 
            {
                $scope.data.deleteDataLoading = false;

                var response = response.data;

                if(response.ResponseCode == 200)
                {
                    $("#sectionsid_"+SID).remove();
                }
                else
                {
                    ErrorPopup(response.Message);
                }
            });
         }).set('labels', {
            ok: 'Confirm',
            cancel: 'Cancel'
        });   
    }
  
}); 


function export_pdf()
{    
    //var data = 'SessionKey='+SessionKey+'&'+$('#filterForm').serialize();
    $("#filterForm").attr("action", "reports/export_pdf");
    $("#filterForm").attr("method", "post");
    $("#filterForm").submit();
}


function export_excel()
{    
    //var data = 'SessionKey='+SessionKey+'&'+$('#filterForm').serialize();
    $("#filterForm").attr("action", "reports/export_excel");
    $("#filterForm").attr("method", "post");
    $("#filterForm").submit();
}


function report_type(rt)
{    
    $("#reports_content").html("");
    $("#action_btn").hide();

    if(rt == "")
    {    
        $("#columns_fieldset").hide();
        $("#filters_fieldset").hide();
        $(".hide_class").hide();        

        $(".disable_btn").attr("disabled", "disabled");

        $(".ReportDateLabel").html('');
    }
    else
    {
        $(".ReportDateLabel").html(set_date_label(rt));


        $("#columns_fieldset").show();
        $("#filters_fieldset").show();
        $(".hide_class").hide();


        //Open Filters and Columns For Selected Report Type------------------------------
        $("#filters_"+rt).show();
        $("#columns_"+rt).show();
        

        //Reset To Default Settings------------------------------
        $("#filters_"+rt).find("input, select").val("");
        $(".chkbox_"+rt).attr("checked", "checked");
        $(".chkbox_"+rt).prop("checked", "checked");


        $(".disable_btn").removeAttr("disabled");
    }

}


function set_date_label(rt)
{
    var lbl = '';

    if(rt == 'enquiries')                       lbl = 'Enquiry';
    else if(rt == 'courses')                    lbl = 'Course Entry';
    else if(rt == 'fees')                       lbl = 'Registration';
    else if(rt == 'fee_collections')            lbl = 'Payment';
    else if(rt == 'batches')                    lbl = 'Batch Start';
    else if(rt == 'batch_attendance_details')   lbl = 'Attendance';
    else if(rt == 'student_details')            lbl = 'Registration';
    else if(rt == 'student_key_details')        lbl = 'Assigned';

    return lbl;
}


function search_within_records()
{    
    $("#SearchType").val("w");
    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function search_records()
{    
    $("#SearchType").val("s");
    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function clear_search_records()
{    
    /*var rt = $("#filterReportType").val();

    $("#filters_"+rt).find("input, select").val(""); */   

    search_records();
}


function export_new_reg()
{
    $("#newregform").submit();
}
