<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Utilities extends API_Controller

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('Utility_model');

		$this->load->model('Users_model');

	}



	/*

	Description: 	Use to send email to webadmin.

	URL: 			/api/utilities/contact/

	*/

	public	function contact_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Name', 'Name', 'trim');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Title', 'Title', 'trim');

		$this->form_validation->set_rules('Message', 'Message', 'trim|required');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */


		$content = $this->load->view('emailer/contact', array(

				"Name" 			=> 	$this->Post['Name'],

				'Email' 		=> 	$this->Post['Email'],

				'PhoneNumber' 	=> 	$this->Post['PhoneNumber'],

				'Title' 		=> 	$this->Post['Title'],

				'Message' 		=> 	$this->Post['Message'],

				) , TRUE);
		

		sendMail(array(

			'emailTo' => SITE_CONTACT_EMAIL,

			'emailSubject' => $this->Post['Name']." filled out the contact form on the ". SITE_NAME,

			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

			));







	}



	/*

	Description: 	Use execute cron jobs.

	URL: 			/api/utilities/getCountries

	*/

	public function getCountries_post()

	{

		$CountryData=$this->Utility_model->getCountries();

		if(!empty($CountryData)){

			$this->Return['Data'] = $CountryData['Data'];

		}

	}







}

