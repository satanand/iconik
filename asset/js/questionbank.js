app.controller('PageController', function ($scope, $http,$timeout){
    $scope.categoryDataList = [];
    $scope.data.SecondLevelPermission = [];
    
    $scope.APIURL = API_URL + "upload/file";
    $scope.APIURLIMG = API_URL + "upload/image";

    /*----------------*/
    /*get category*/
    $scope.loadImportQuestion = function (){
        $scope.templateURLImport = PATH_TEMPLATE+'questionbank/import_question_answer.htm?'+Math.random();
        $scope.data.pageLoading = true;       
        $timeout(function(){   
             $('#import_model').modal({show:true});     
             $scope.data.pageLoading = false;           
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
        }, 1000);
    }


    $scope.getCategoryFilterData = function (ParentCategoryGUIDIndex,addSelected="")
    {
        //console.log(addSelected);    
        $scope.filterData = [];
        var options = "";        
        $scope.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;
        $("#ParentCategoryGUIDIndex").val(ParentCategoryGUIDIndex);
        if(ParentCategoryGUIDIndex){ //////console.log(ParentCategoryGUIDIndex);
            if($scope.categoryDataList){
                $scope.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;
                $scope.filterData.ParentCategoryName = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryName;
                if($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories){
                    var SubCategories = $scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records;
                    if(addSelected == 'addSelected'){
                        var options = '<select  name="CategoryGUID" class="form-control subject" onchange="setSubject(this.value)" required=""><option value="">Select Subject</option>';
                    }else if(addSelected == 'questionList'){
                        var options = '<select id="subject" name="CategoryGUIDs" onchange="filterQuestionList(this.value)"  class="form-control"><option value="">Select Subject</option>';
                    }else{
                        var options = '<select  name="CategoryGUID" onchange="setSubject(this.value)" id="subject"  class="form-control subject" required=""><option value="" selected>Select Subject</option>';
                    }                    
                    for (var i in SubCategories) {
                        $scope.filterData.push($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records);
                        options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               
                    } 
                    options += '</select>';

                    if(options){  //////console.log(options);
                        $("#subjects_section").html(options);
                        $("#subcategory_section").html(options);
                        $("#link_add_subject").show();
                        $timeout(function(){            
                               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                           }, 200);
                    }else{
                        $("#link_add_subject").show();
                    }
                }
            }
        }else{
            var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" required=""><option value="">Select Subject</option></select>';
            $("#subcategory_section").html(options);
            $("#link_add_subject").show();
        }
    }


    /*get subject*/
    $scope.getCategorySubjects = function (ParentCategoryGUIDIndex,apiCall="")
    {        
        $scope.filterData = [];
        var options = "";        
        $scope.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;
        $("#ParentCategoryGUIDIndex").val(ParentCategoryGUIDIndex);
        if(ParentCategoryGUIDIndex){ //////console.log(ParentCategoryGUIDIndex);
            if($scope.categoryDataList){
                $scope.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;
                $scope.filterData.ParentCategoryName = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryName;
                if(apiCall == 'getQuestionBankStatistics'){
                    $scope.getQuestionBankStatistics($scope.ParentCategoryGUID);
                    $("#ParentCategoryGUID").val(ParentCategoryGUID);
                }
                if(apiCall == 'getQuestionsList'){
                    $scope.getQuestionsList($scope.ParentCategoryGUID);
                    $("#ParentCategoryGUID").val(ParentCategoryGUID);
                }
                if($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories){
                    var SubCategories = $scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records;
                    var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control subject"  onchange="filterQuestionBankStatistics(this.value)"><option value="">Select Subject</option>';
                    for (var i in SubCategories) {
                        $scope.filterData.push($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records);
                        options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               
                    } 
                    options += '</select>';
                    if(options){
                        $("#subcategory_section").html(options);
                        $("#subjects_section").html(options);
                        $("#link_add_subject").show();
                         $scope.$apply();
                        //if($scope.SubCategoryGUID.length){
                        // $timeout(function(){            
                        //        $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                        //    }, 200);
                       // } 
                    }else{
                        $("#link_add_subject").show();
                    }
                }
            }
        }else{
            if(apiCall == 'getQuestionBankStatistics'){
                $scope.getQuestionBankStatistics();
                $("#ParentCategoryGUID").val(ParentCategoryGUID);
            }
            if(apiCall == 'getQuestionsList'){
                $scope.getQuestionsList();
                $("#ParentCategoryGUID").val(ParentCategoryGUID);
            }
            var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" onchange="formDataCheck()" onchange="check_subject_available()" required=""><option value="">Select Subject</option></select>';
            $("#subcategory_section").html(options);
            $("#link_add_subject").show();
        }
    }


    /*get Course List*/
    $scope.getCategoryList = function (categoryGUID="")
    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
        var data = '';
        $scope.categoryDataList = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) 
                {
                    for(var j in response.Data.Records[i].SubCategories.Records)
                    {
                        /*if(categoryGUID.length > 0)
                        {
                            if(response.Data.Records[i].SubCategories.Records[j].CategoryGUID == categoryGUID)
                            {
                                $scope.ParentCategoryGUIDIndex = j;
                            }
                        }*/
                        //////console.log(response.Data.Records[i].SubCategories.Records[j].CategoryGUID);
                        $scope.categoryDataList.push(response.Data.Records[i].SubCategories.Records[j]);
                        
                    } 
                }          
            } ////////console.log($scope.categoryDataList);
        });
    }


    $scope.getSubCategoryList = function (ParentCategoryGUID, Type)
    {        
        if(Type == 't')
            $scope.subTopicDataList = [];
        else
            $scope.subCategoryDataList = [];    

        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID;
        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records)
            { /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                
                if(Type == 't')
                    $scope.subTopicDataList = response.Data.Records; 
                else
                    $scope.subCategoryDataList = response.Data.Records; 
            } ////////console.log($scope.categoryDataList);
        });
        //console.log($scope.subCategoryDataList);
    }


    /*list*/
    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getCategoryList();
        $scope.getQuestionsList(0);
    }


    $scope.getList = function ()
    {
        $scope.getCategoryList();
        $scope.getQuestionBankStatistics();
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
    }


    /*list append*/
    /*list append*/
    $scope.getQuestionBankStatistics = function (courseID="",subjectID="",topicId="")
    {
        //if ($scope.data.listLoading || $scope.data.noRecords) return;
        $scope.data.listLoading = true;
        $scope.data.dataList = [];
        var data = 'SessionKey='+SessionKey+'&SubjectGUID='+subjectID+'&CourseGUID='+courseID+'&TopicID='+topicId+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        $http.post(API_URL+'questionbank/getQuestionBankStatistics', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200 && response.Data){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    for (var i in response.Data) {
                       $scope.data.dataList.push(response.Data[i]);
                    }
                    $scope.data.pageNo++;               
               }else{
                $scope.data.noRecords = true;
            }
            $scope.data.listLoading = false;
            setTimeout(function(){ tblsort();  $scope.$apply(); }, 1000);
        });
    }


    $scope.setCourseSubject = function (){

    }


    $scope.getQuestionsList = function (check="")
    {
        //console.log(check+'-----'+type);
        $scope.status = true;
        
        if(check != 1)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }

        $scope.data.listLoading = true;
        
        if(check == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
            $scope.data.noRecords = false;

            var CourseGUID = $("#CourseGUID").val();
            var SubjectGUID = $("#SubjectGUID").val();
            var TopicGUID = $("#TopicGUID").val();
            var QuestionsType = $("#QuestionsType").val();
            var QuestionsLevel = $("#QuestionsLevel").val();
            var QuestionsGroup = $("#QuestionsGroup").val();
            var Keyword = $("#Keyword").val();
        }
        else
        {
            var CourseGUID = $("#CourseGUID").val();
            var SubjectGUID = $("#SubjectGUID").val();
            var TopicGUID = $("#TopicGUID").val();

            if(CourseGUID == "" || CourseGUID == null || CourseGUID == undefined) 
                CourseGUID = $scope.getUrlParameter('CourseGUID');

            if(SubjectGUID == "" || SubjectGUID == null || SubjectGUID == undefined) 
                SubjectGUID = $scope.getUrlParameter('SubjectGUID');
            
            if(TopicGUID == "" || TopicGUID == null || TopicGUID == undefined) 
                TopicGUID = $scope.getUrlParameter('TopicGUID');
            

            var QuestionsType = $("#QuestionsType").val();
            var QuestionsLevel = $("#QuestionsLevel").val();
            var QuestionsGroup = $("#QuestionsGroup").val();
            var Keyword = $("#Keyword").val(); 

            if(QuestionsType == "" || QuestionsType == null || QuestionsType == undefined) 
                QuestionsType = ""; 

            if(QuestionsLevel == "" || QuestionsLevel == null || QuestionsLevel == undefined) 
                QuestionsLevel = "";

            if(QuestionsGroup == "" || QuestionsGroup == null || QuestionsGroup == undefined) 
                QuestionsGroup = "";

            if(Keyword == "" || Keyword == null || Keyword == undefined) 
                Keyword = "";

        }   



        /*if(check == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
            $scope.data.noRecords = false;
            if(type=='ParentCat'){
                var CourseGUID = $("#Courses").val();
                var SubjectGUID = "";
                $scope.CourseGUID =  CourseGUID;  
                $scope.SubjectGUID =  SubjectGUID; 
                $scope.getSubCategoryList(CourseGUID);
                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
            }else{
                var CourseGUID = $("#Courses").val();
                var SubjectGUID = $("#subject").val();
                $scope.CourseGUID =  CourseGUID;  
                $scope.SubjectGUID =  SubjectGUID; 
                $scope.getSubCategoryList(CourseGUID);
                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
            }            
            $scope.data.count++; 
            $scope.data.dataList = [];            
        }
        else
        {
            //$scope.data.pageNo = 1;
            $scope.data.count == 0;
            if($("#Courses").val() == "" &&  $("#subject").val() == ""){
                //console.log("fjldskfjldskflkdsfsdl");
                var SubjectGUID = $scope.getUrlParameter('CategoryGUID');
                var CourseGUID = $scope.getUrlParameter('parentCategoryGUID');
                $scope.getSubCategoryList(CourseGUID);
            }else{
                 //console.log("111111111111111111");
                var CourseGUID = $("#Courses").val();
                var SubjectGUID = $("#subject").val();
                if(!SubjectGUID.length){
                    var SubjectGUID = $(".subject").val();
                }
                $scope.getCategoryList(CourseGUID); 
            } 
            $scope.SubjectGUID =  SubjectGUID;    
            $scope.CourseGUID =  CourseGUID;
            var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        }*/  
        
        
        var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&TopicGUID='+TopicGUID+'&QuestionsType='+QuestionsType+'&QuestionsLevel='+QuestionsLevel+'&QuestionsGroup='+QuestionsGroup+'&Keyword='+Keyword+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize;

        $http.post(API_URL+'questionbank/getQuestions', data, contentType).then(function(response) 
        {
                var response = response.data;
                if(response.ResponseCode==200 && response.Data.length > 0)
                { 
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    for (var i in response.Data) 
                    {
                       $scope.data.dataList.push(response.Data[i]);
                    }
                    $scope.data.pageNo++;
                }
                else
                {                    
                    $scope.data.noRecords = true;
                }
                        
                $scope.data.listLoading = false;
                
        });
    }


    /*load add form*/
    $scope.loadFormAdd = function (check="",listCheck="")
    {
        $scope.listCheck = listCheck;
        if(check == 1){
            var SubjectGUID = $("#subject").val();
            var CourseGUID = $("#Courses").val();
            $scope.ParentCategoryGUIDIndex = CourseGUID;
            $("#CategoryGUIDs").val(SubjectGUID); 
            $scope.SubjectGUID = SubjectGUID;
           // $scope.getCategoryList(CourseGUID); 
            ////console.log(CourseGUID);
        }
        //$scope.data.Position = Position;
        $scope.templateURLAdd = PATH_TEMPLATE+'questionbank/add_form.htm?'+Math.random();
        $scope.data.pageLoading = true;       
        $timeout(function(){   
             $('#add_model').modal({show:true});     
             $scope.data.pageLoading = false;           
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
             
             tinymce.init({
                selector: '#QuestionContent',
                plugins: "table",                 
                toolbar: "table"                  
              });


             tinymce.init({
                selector: '#Explanation,#ShortAnswer'                                 
              });


        }, 1000);
        if(check == 1){
            $timeout(function(){
                // //$scope.ParentCategoryGUIDIndex = parentIndex;  
                // if($scope.ParentCategoryGUIDIndex){
                //     $(".Courses").val($scope.ParentCategoryGUIDIndex);
                //     $scope.getCategoryFilterData(CourseGUID,'addSelected');
                // }
                // $timeout(function(){ 
                //     if($scope.ParentCategoryGUIDIndex){
                //        $(".subject").val($scope.SubjectGUID); 
                //        $("#CategoryGUIDs").val($scope.SubjectGUID); 
                //     }                       
                // }, 500);
            }, 500);
        }
    }


    /*load add form*/
    $scope.loadAddQuestionImage = function (check="")
    {
        $scope.data.pageLoading = true;
        $scope.templateURLAdd = PATH_TEMPLATE+'questionbank/add_question_image.htm?'+Math.random();
        $('#add_model').modal({show:true});     
        $scope.data.pageLoading = false;         
    }


    /*load edit form*/
    $scope.loadFormEdit = function (Position, QtBankGUID)
    {
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE+'questionbank/edit_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        var SubjectGUID = $("#subject").val();
        var data = 'SessionKey='+SessionKey+'&QtBankGUID='+QtBankGUID;
        $http.post(API_URL+'questionbank/getQuestionAnswer', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.length)
            { 
                
                $scope.data.pageLoading = false;
                $('#edits_model').modal({show:true});

                /* hack to remove space - ends */
                $scope.formData = response.Data[0];
                //console.log("t="+$scope.formData);

                $scope.getSubCategoryList($scope.formData.ParentCategoryGUID, 's');
                $scope.getSubCategoryList($scope.formData.SubCategoryGUID, 't');
                

                var arr = $scope.formData.QuestionsGroup.split(', ');
                $scope.formData.QuestionsGroup = arr;
                $scope.formData.PracticeTest = $scope.formData.Quiz = $scope.formData.Assessment = $scope.formData.Contest = $scope.formData.ScholarshipTest = false;
                $timeout(function()
                { 
                    if(jQuery.inArray("Practice Test",  $scope.formData.QuestionsGroup ) >= 0)
                    {
                        $scope.formData.PracticeTest = true;
                    }

                    if(jQuery.inArray( "Quiz",  $scope.formData.QuestionsGroup ) >= 0){

                        $scope.formData.Quiz = true;
                    }

                    if(jQuery.inArray( "Assessment",  $scope.formData.QuestionsGroup ) >= 0)
                    {
                        $scope.formData.Assessment = true;
                    }   

                    if(jQuery.inArray( "Contest",  $scope.formData.QuestionsGroup ) >= 0)
                    {
                        $scope.formData.Contest = true;
                    }

                    if(jQuery.inArray( "Scholarship Test",  $scope.formData.QuestionsGroup ) >= 0)
                    {
                        $scope.formData.ScholarshipTest = true;
                    }
                    
                    if($scope.formData.Media.Records)
                    {
                       $scope.MediaCheck = true;
                    }
                    else
                    {
                        $scope.MediaCheck = false;
                    }     
                }, 0);


                if($scope.formData.QuestionsType == "Logical Answer")
                {
                    $("#logical").show();
                    $("#multilevel").hide();

                    var answers = "";
                    if($scope.formData.answer)
                    {                        
                        var ca = 0;
                        var ca_chked = "";
                        for (var i in $scope.formData.answer) 
                        {
                            ca = $scope.formData.answer[i].AnswerContent;
                            ca_chked = "";

                            if($scope.formData.answer[i].CorrectAnswer == 1)
                            {
                                ca_chked = "checked='True'";
                            }

                            answers = answers + '<div class="col-md-5" style="float: left;"><input type="radio" name="CorrectAnswers" class="CorrectAnswers" value="'+ca+'" '+ca_chked+' style="margin-right: 5px;" onclick="setCorrectLogicalAnswer(this);"><label for="radio_9">'+$scope.formData.answer[i].AnswerContent+'</label></div>';
                        }

                        $("#logical_options").html(answers);
                    }


                    var multiple_content = '<div class="form-group"><input type="hidden" id="multilevel_counter" value="1"><div class="row"><label style="width: 67%;" class="control-label mb-10 text-left col-sm-8">Multiple Answer <em style="color: red">*</em></label><label style="width: 20%;" class="control-label mb-10 text-left col-sm-4">Correct Answer <em style="color: red">*</em></label></div><div class="col-sm-12"><div class="row multiple-choice" style="margin-top: 10px;"><input type="hidden" name="CorrectAnswer" id="CorrectAnswer" value=""><div class="col-sm-8 "><input type="text" name="AnswerContent[]" id="multiple-choice-1" class="form-control multiple-choice-options" placeholder="Option" value=""></div><div class="col-md-1 has-success" style="margin-top: 10px;"><div class="mt-40"><input id="correct-answer-1" type="radio" name="CorrectAnswerOption" value="1" style="margin: 4px 10px 0 45px;" class="CorrectAnswerOption" onchange="setCorrectAnswer(this.value);"></div></div></div></div><div class="col-sm-12"><div class="input_fields_wrap"></div></div><div class="col-sm-12"><button onclick="add_field_button()" class="add_field_button btn btn-success mt-30" style="width: 13%;font-size: 13px;padding: 2px 5px 5px 5px; margin-top: 20px;background-color: gray;border-color: gray;"><span class="glyphicon glyphicon-plus" style="top: 1px;left: -5px;font-size: 14px;"></span> <span style="position: relative;left: -5px;top: -1px;">Add More Option</span></button></div></div>';

                    $("#multilevel").empty();
                    $("#multilevel").html(multiple_content);
                }
                else if($scope.formData.QuestionsType == "Multiple Choice")
                {
                    $("#multilevel").show();
                    $("#logical").hide();


                    var logical_content = '<div class="form-group"><label class="control-label mb-10 text-left">Logical Answer <em style="color: red">*</em></label><div class="row"><div class="col-md-12" id="logical_options" style="margin-top: 5px; margin-bottom: 10px;"><div class="col-md-5" style="float: left;"><input type="radio" name="CorrectAnswers" class="CorrectAnswers" value="True" style="margin-right: 5px;" onclick="setCorrectLogicalAnswer(this);"><label for="radio_9">True</label></div><div class="col-md-5" style="float: left;"><input type="radio" name="CorrectAnswers" class="CorrectAnswers" value="False" style="margin-right: 5px;" onclick="setCorrectLogicalAnswer(this);"><label for="radio_9">False</label></div></div></div></div>';

                    $("#logical").empty();
                    $("#logical").html(logical_content);

                    $("#multilevel_counter").val($scope.formData.answer.length);
                }

               
                $timeout(function()
                {
                    //$scope.getSubCategoryList($scope.formData.SubCategoryGUID, 't');
                    $("#Courses").val($scope.formData.ParentCategoryGUID);
                    $("#SubjectGUID").val($scope.formData.SubCategoryGUID);
                    $("#TopicID").val($scope.formData.TopicCategoryID);
                    $scope.CourseGUID = $scope.formData.ParentCategoryGUID;
                    $scope.SubjectGUID = $scope.formData.SubCategoryGUID;
                    //$scope.TopicCategoryID = $scope.formData.TopicCategoryID;
                    
                }, 1000);

                $timeout(function()
                {
                    tinymce.init({
                        selector: '#Explanation'
                    });

                    tinymce.init({
                        selector: '#QuestionContent',
                        plugins: "table",                 
                          toolbar: "table"                  
                      });


                }, 1000);

            }
            else
            {                
                $scope.data.pageLoading = false;
                ErrorPopup("Not available for edit!");
            }
        });
    }

    $scope.removeMultipleChoice  = function(count){

        $('#remove_field'+count).parents('.multiple-choice').remove(); x--;

    }


    $scope.changeQuestionType = function(QuestionType){
        // $scope.formData.QuestionsType = QuestionType;
        // $('#edits_model').modal({show:true});
        //console.log($scope.formData.QuestionsType);
    }


    /*load edit form*/
    $scope.loadFormView = function (Position, QtBankGUID, courseID, subjectID, view="")
    {
        //console.log(subjectID);
        $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE+'questionbank/view_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        if($("#subject").val()){
            var data = 'SessionKey='+SessionKey+'&QtBankGUID='+QtBankGUID+'&view='+view+'&courseID='+courseID+'&subjectID='+subjectID;
        }else{
            subjectID = "";
            var data = 'SessionKey='+SessionKey+'&QtBankGUID='+QtBankGUID+'&view='+view+'&courseID='+courseID+'&subjectID='+subjectID;
        }
        $http.post(API_URL+'questionbank/getQuestionAnswer', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.length){ /* success case */
                $scope.data.pageLoading = false;
               
                /* hack to remove space - ends */
                $scope.formData = response.Data[0];
                if(view == ''){
                    $('#view_model').modal({show:true});
                }
            }else{
                if(view != ''){
                    ErrorPopup("Not available!");
                }else{
                    ErrorPopup(response.Message);
                }
                $scope.data.pageLoading = false;
                
            }
        });
    }


    /*delete Entity*/
    $scope.deleteQuestion = function(Position,EntityGUID) {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&EntityGUID=' + EntityGUID;
            $http.post(API_URL + 'admin/entity/delete', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $timeout(function(){ 
                        //$('.modal-header .close').click();
                       // location.reload();
                    });
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;

    }



    /*add data*/
    $scope.addData = function (submitType,listCheck="")
    {
         $scope.loadeSubmit = true;
        tinyMCE.triggerSave();
        //tinymce.get("QuestionContent").setContent('');
        //tinymce.get("Explanation").setContent('');
        /*var data = 'SessionKey='+SessionKey;
        var QuestionsGroup = $("input[name='QuestionsGroup[]']:checked")
              .map(function(){return $(this).val();}).get();
       
        if($(".QuestionType").val() != ""){
            if($(".QuestionType").val() == 'Short Answer')
            {
                data  += '&ShortAnswer='+$("textarea[name='ShortAnswer']").val();
                data  += '&CorrectAnswer=1';
            }else if($(".QuestionType").val() == 'Multiple Choice'){
                var AnswerContent = $("input[name='AnswerContent[]']")
                .map(function(){if($(this).val()) return $(this).val();}).get();
                data  += '&CorrectAnswer='+$("input[name='CorrectAnswer']").val();
               // alert(AnswerContent);
                if(AnswerContent.length > 0){
                    data  += '&AnswerContent='+JSON.stringify(AnswerContent);
                }else{
                    data  += '&AnswerContent=';
                }
            }else if($(".QuestionType").val() == 'Logical Answer'){
               data  += '&CorrectAnswer='+$("input[name='CorrectAnswers']:checked").val();
            }
        }

        //console.log($('#subject').val());
        
        data  += '&ParentCat='+$("select[name='ParentCat']").val();
        data += '&CategoryGUID='+$('#SubjectGUID').val();
        data  += '&QuestionsGroup[]='+QuestionsGroup;
       data  += '&QuestionsLevel='+$(".QuestionsLevel").val();
        data  += '&QuestionsType='+$(".QuestionType").val();
        data  += '&QuestionsMarks='+$("input[name='QuestionsMarks']").val();
        data  += '&QuestionContent='+$("textarea[name='QuestionContent']").val();
        data  += '&Explanation='+$("textarea[name='Explanation']").val();        
        data  += '&MediaGUID='+$("input[name='MediaGUID']").val();*/

        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'questionbank/addQA', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200)
            { 
                tinymce.get("QuestionContent").setContent('');
                tinymce.get("Explanation").setContent('');

                /* success case */    
                /*if(listCheck == 'getQuestionList')  {
                    $scope.getQuestionsList(1);   
                }else{
                    $scope.getList();
                }*/
                        
                
                $timeout(function(){  
                    if(submitType != 'addNext'){ 
                    SuccessPopup(response.Message);                       
                        $('#add_model .close').click(); 
                        tinymce.remove();
                        $scope.loadeSubmit = false;
                        window.location.href = window.location.href;
                    }else{
                        //$('#add_question_form')[0].reset();
                        //$('#addQuestionBankSection').find('input:text').val('');  
                        //$('#addQuestionBankSection').find('input:hidden').val('');  
                        //$('#addQuestionBankSection').find('input:file').val('');
                        //$('.QuestionsMarks').val(''); 
                        $('.colors').find("input.multiple-choice-options").val('');
                        $('.colors').find("input[type='radio']").removeAttr("checked");
                        //$('#collapseExample').removeClass('show'); 
                        ///$('#addQuestionBankSection').find('input:number').val(0);  
                        //$('#addQuestionBankSection').find('textarea').val(''); 
                        //$('#addQuestionBankSection').find('select').val('');  
                        //$('#addQuestionBankSection').find('input:radio').removeAttr('checked');
                        //$('input:checkbox').removeAttr('checked');
                        //$('#picture-box-picture').hide();
                        //$("#media_Section").val('QuestionBank');
                         $scope.loadeSubmit = false;
                    }
                }, 100);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.loadeSubmit = false;
            //$scope.addDataLoading = false;          
        });
    }




    /*edit data*/
    $scope.editData = function ()
    {
        $scope.editDataLoading = true;
        $scope.loadeSubmit = true;

        tinyMCE.triggerSave();
        /*var data = 'SessionKey='+SessionKey;
        data  += '&QtBankGUID='+$('#QtBankGUID').val();
        var QuestionsGroup = $("input[name='QuestionsGroup[]']:checked")
              .map(function(){return $(this).val();}).get();
       
        if($(".QuestionType").val() != ""){
            if($(".QuestionType").val() == 'Short Answer'){
                data  += '&ShortAnswer='+$("textarea[name='ShortAnswer']").val();
                data  += '&CorrectAnswer=1';
            }else if($(".QuestionType").val() == 'Multiple Choice'){
                var AnswerContent = $("input[name='AnswerContent[]']")
                  .map(function(){if($(this).val()) return $(this).val();}).get();
                  //console.log(AnswerContent);
                 // alert(AnswerContent.length);
                data  += '&CorrectAnswer='+$("input[name='CorrectAnswer']").val();
                if(AnswerContent.length > 0){
                    data  += '&AnswerContent='+JSON.stringify(AnswerContent);
                }else{
                    data  += '&AnswerContent=';
                }
                
            }else if($(".QuestionType").val() == 'Logical Answer')
            {
               
               data  += '&CorrectAnswer='+$("input[name='CorrectAnswer']:checked").val();
            }
        }

        if($("#CategoryGUIDs").val()){
            var CategoryGUID = $("#CategoryGUIDs").val();
        }else if($("#subject").val()){
            var CategoryGUID = $("#subject").val();
        }else if($(".subject").val()){
            var CategoryGUID = $(".subject").val();
        }
        
        data  += '&ParentCat='+$("select[name='ParentCat']").val();
        data += '&CategoryGUID='+CategoryGUID;
        data  += '&QuestionsGroup[]='+QuestionsGroup;
        data  += '&QuestionsLevel='+$(".QuestionsLevel").val();
        data  += '&QuestionsType='+$(".QuestionType").val();
        data  += '&QuestionsMarks='+$("input[name='QuestionsMarks']").val();
        data  += '&QuestionContent='+$("textarea[name='QuestionContent']").val();
        data  += '&Explanation='+$("textarea[name='Explanation']").val();        
        data  += '&MediaGUID='+$("input[name='MediaGUID']").val();*/
        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();
        $http.post(API_URL+'questionbank/editQA', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */ 
                 
                //$scope.data.dataList[$scope.data.Position] = response.Data[0];
                SuccessPopup(response.Message);
                $timeout(function(){ 

                     $scope.editDataLoading = false;
                     $scope.loadeSubmit = false;
                    $('#edits_model .close').click();
                    window.location.href = window.location.href; 
                }, 1000);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;
             $scope.loadeSubmit = false;  
        });
    }


    $scope.changeStatus = function(position,QtBankGUID){
        $scope.data.position = position;        
        $.confirm({
            title: 'Confirm!',
            content: 'Are you sure to change the status of this question?',
            buttons: {
                Ok: function () {
                    if($('input[name="switch-status"]:checked').length == 2){
                        var StatusID = 2;
                    }else{
                        var StatusID = 6;
                    }
                    var data = 'SessionKey=' + SessionKey + '&QtBankGUID=' + QtBankGUID+ '&StatusID=' + StatusID;
                    $http.post(API_URL+'questionbank/changeStatus', data, contentType).then(function(response) {
                        var response = response.data;
                        if(response.ResponseCode==200){ /* success case */ 
                        //console.log(response.Data[0]);     
                            $scope.data.dataList[$scope.data.Position] = response.Data[0];
                            SuccessPopup(response.Message);
                            $timeout(function(){ 
                                $('#edits_model .close').click(); 
                            }, 100);
                        }else{
                            ErrorPopup(response.Message);
                        }
                        //$scope.addDataLoading = false;          
                    });
                },
                cancel: function () {
                    if($('input[name="switch-status"]:checked').length > 0){
                        $("#switch-status"+position).attr("checked",false);
                    }else{
                        $("#switch-status"+position).attr("checked",true);
                    }
                },
            }
        });        
    }


    $scope.importData = function ()
    {
        $scope.loadeSubmit = true;
       
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='importQuestions']").serialize();
        $http.post(API_URL+'questionbank/importQuestions', data, contentType).then(function(response) {
            var response = response.data;
            $scope.loadeSubmit = false;
            if(response.ResponseCode==200){ /* success case */
                SuccessPopup(response.Message);
                $scope.getQuestionBankStatistics();
                $timeout(function(){  
                   $('#import_model .close').click();
                   
                }, 200);
            }else{
                ErrorPopup(response.Message);
            }
        });
        
        setTimeout(function(){ tblsort(); }, 1000);
    }

}); 


/* sortable - starts */
function tblsort() {

  var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}

$(".table-sortable tbody").sortable({
    placeholder: 'tr_placeholder',
    helper: fixHelper,
    cursor: "move",
    tolerance: 'pointer',
    axis: 'y',
    dropOnEmpty: false,
    update: function (event, ui) {
      sendOrderToServer();
  }      
}).disableSelection();
$(".table-sortable thead").disableSelection();


function sendOrderToServer() {
    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");
    $.ajax({
        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',
        data: order,
        stop: function(response) {
            if (response.status == "success") {
                window.location.href = window.location.href;
            } else {
                alert('Some error occurred');
            }
        }
    });
}
/* sortable - ends */
}


function getCategoryFilterData(ParentCategoryGUID,addSelected=""){
  if(addSelected.length > 0){
    angular.element(document.getElementById('question-body')).scope().getCategoryFilterData(ParentCategoryGUID,addSelected); 
  }else{
    angular.element(document.getElementById('question-body')).scope().getCategoryFilterData(ParentCategoryGUID); 
  }
}

function getCategorySubjects(ParentCategoryGUIDIndex,apiCall)
{
  angular.element(document.getElementById('question-body')).scope().getCategorySubjects(ParentCategoryGUIDIndex,apiCall);  
}


function filterQuestionBankStatistics(CategoryGUID){
  var ParentCategoryGUID = $("#ParentCategoryGUID").val();
  angular.element(document.getElementById('question-body')).scope().getQuestionBankStatistics(ParentCategoryGUID,CategoryGUID);  
}

function changeStatus(key,QtBankGUID,status){
  angular.element(document.getElementById('question-body')).scope().changeStatus(key,QtBankGUID,status);  
}


function filterQuestionList(val,type=""){
    if(type.length > 0){
         $("#"+type).val(val);
    } 
    angular.element(document.getElementById('question-body')).scope().getQuestionsList(1,type,val);  
}


function filterQuestionBankStatistics(CategoryGUID){
  var ParentCategoryGUID = $("#ParentCategoryGUID").val();
  angular.element(document.getElementById('question-body')).scope().getQuestionBankStatistics(ParentCategoryGUID,CategoryGUID);  
}


/*doc file upload*/
$(document).on('change', '#fileInput', function() {
    var target = $(this).data('target');
    var croptarget = $(this).data('croptarget');

    var mediaGUID = $(this).data('targetinput');
    var progressBar = $('.progressBar'),
        bar = $('.progressBar .bar'),
        percent = $('.progressBar .percent');
    $(this).parent().ajaxForm({
        data: {
            SessionKey: SessionKey
        },
        dataType: 'json',
        beforeSend: function() {
            progressBar.fadeIn();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function(obj, statusText, xhr, $form) 
        {   
            $("#add_image_model").hide();

            if (obj.ResponseCode == 200) {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
                $(target).prop("src", obj.Data.MediaURL);
                $("input[name='MediaGUIDs']").val(obj.Data.MediaGUID);
                $(mediaGUID).val(obj.Data.MediaGUID);

                if (obj.Data.ImageCropper == "Yes") {
                    $(croptarget).show();
                    /*crop plugin - starts*/
                    jQuery('img#picture-box-picture').imgAreaSelect({
                        handles: true,
                        onSelectEnd: getCropSizes,
                        disable: false,
                        hide: false,
                    });
                    /*crop plugin - ends*/
                }

               

            } else {
                ErrorPopup(obj.Message);
            }


        },
        complete: function(xhr) {
            progressBar.fadeOut();
            $('#fileInput').val("");


        }
    }).submit();

});

$("#add_model").on("hidden.bs.modal", function () {
    tinymce.remove();
});

$("#edits_model").on("hidden.bs.modal", function () {
    tinymce.remove();
});

function getSubCategoryList(parentCategoryGUID, typePg)
{
    angular.element(document.getElementById('question-body')).scope().getSubCategoryList(parentCategoryGUID,typePg);
}


/*audio file upload*/

$(document).on('change', '#questionFileInput', function() {

    var target = $(this).data('target');

    var mediaGUID = $(this).data('targetinput');

    var progressBar = $('.progressBar'),

        bar = $('.progressBar .bar'),

        percent = $('.progressBar .percent');

    $(this).parent().ajaxForm({

        data: {

            SessionKey: SessionKey

        },

        dataType: 'json',

        beforeSend: function() {

            progressBar.fadeIn();

            var percentVal = '0%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        uploadProgress: function(event, position, total, percentComplete) {

            var percentVal = percentComplete + '%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        success: function(obj, statusText, xhr, $form) {

            if (obj.ResponseCode == 200) {

                var percentVal = '100%';

                bar.width(percentVal)

                percent.html(percentVal);

                $(target).prop("src", obj.Data.MediaURL);

                //$("input[name='MediaGUIDs']").val(obj.Data.MediaGUID);

                $(mediaGUID).val(obj.Data.MediaGUID);

                $("#MediaURL").val(obj.Data.MediaURL);

            } else {

                ErrorPopup(obj.Message);

            }

        },

        complete: function(xhr) {

            progressBar.fadeOut();

            //$('#studentFileInput').val("");

        }

    }).submit();

});


function search_records(pg)
{   
    
    if(pg == 'vql')
    {
        angular.element(document.getElementById('question-body')).scope().getQuestionsList(1);
    }
    else
    {
        var courseID = $("#Courses").val();
        var subjectID = $("#Subjects").val();
        var topicId = $("#Topics").val();

        angular.element(document.getElementById('question-body')).scope().getQuestionBankStatistics(courseID,subjectID,topicId);
    }    
}


function clear_search_records(pg)
{    
    if(pg == 'vql')
    {           
        $("#CourseGUID, #SubjectGUID, #TopicGUID, #QuestionsType, #QuestionsLevel, #QuestionsGroup").val(""); 
    }
    else
    {
        $("#Courses, #Subjects, #Topics").val("");  
    } 


    if(pg == 'vql')
    {
        angular.element(document.getElementById('question-body')).scope().getQuestionsList(1);
    }
    else
    {
        search_records(pg);
    }    
}

