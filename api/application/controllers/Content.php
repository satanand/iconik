<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Content_model');
		$this->load->model('Common_model');
	}

	/*
	Name: 			posts
	Description: 	Use to add new post
	URL: 			/api/post	
	*/
	public function addContent_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ParentPostGUID', 'ParentPostGUID', 'trim|callback_validateEntityGUID[Post,PostID]');
		//$this->form_validation->set_rules('ParentCategoryGUID', 'Parent Category', 'trim|callback_validateEntityGUID[Category,ParentCategoryID]');
		$this->form_validation->set_rules('CategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		//$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID,ParentCategoryID,CategoryGUID]');

		$this->form_validation->set_rules('PostType', 'PostType', 'trim|required|in_list[Activity,Comment,Review,Question,Content]');	
		$this->form_validation->set_rules('Privacy', 'Privacy', 'trim|in_list[Public,Friends,Private]');
		$this->form_validation->set_rules('PostContent', 'PostContent', 'trim'.(empty($this->Post['MediaGUIDs']) ? '' : ''));
		$this->form_validation->set_rules('audio_MediaGUIDs', 'audio_MediaGUIDs', 'trim'); /* Media GUIDs */
		$this->form_validation->set_rules('file_MediaGUIDs', 'file_MediaGUIDs', 'trim'); /* Media GUIDs */
		if(is_array($this->Post['Media']) && !empty($this->Post['Media'])){
			//$this->Post['Media'] = json_decode($this->Post['Media'],true);
			foreach($this->Post['Media'] as $Key=>$Value){
				$this->form_validation->set_rules('Media['.$Key.'][MediaGUID]', 'MediaGUID', 'trim|required|callback_validateEntityGUID[Media]');
			}	
		}
		$this->form_validation->validation($this);  /* Run validation */		
		// echo "<pre>";
		// print_r($this->Post); die;

		if(empty($this->Post['PostContent']) && empty($this->Post['PostVideoTitle']) && empty($this->Post['file_MediaGUIDs']) && empty($this->Post['audio_MediaGUIDs'])){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Atleast one type of content is required."; 
		}else{
			/* Validation - ends */

			/* Define section */
			$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
			/* Define section - ends */ 

			// if($this->Post['CategoryGUIDs']){
			// 	if(!is_array($this->Post['CategoryGUIDs'])){
			// 		$CategoryGUIDs = explode(' ', $this->Post['CategoryGUIDs']);
			// 	}else{
			// 		$CategoryGUIDs = $this->Post['CategoryGUIDs'];
			// 	}
			// }else{
			// 	$CategoryGUIDs = array();
			// }

			//print_r($this->Post); die;

			$PostData = $this->Content_model->addContent($this->SessionUserID, $this->EntityID, array(
				"ParentPostID"	=>	@$this->PostID,
				//"CategoryID"	=>	@$this->CategoryID,
				"CategoryGUID"	=>	@$this->Post['CategoryGUID'],
				"Privacy"		=>	(!empty($this->Post['Privacy']) ? @$this->Post['Privacy'] : 'Public'),
				"PostContent"	=>	@$this->Post['PostContent'],
				"PostCaption"	=>	@$this->Post['PostCaption'],
				"PostType"		=>	$this->Post['PostType'],
				"Rating"		=>	@$this->Post['Rating'],
				"PostVideoTitle"		=>	@$this->Post['PostVideoTitle'],
				"PostVideoLink"		=>	@$this->Post['PostVideoLink']
			));

			
			/*echo "<pre>";
			print_r($PostData); 
			//echo "Media===<br/>";
			//print_r($this->Post['Media']); 
			//echo "file_MediaGUIDs===<br/>";
			//print_r($this->Post['file_MediaGUIDs']);
			echo "audio_MediaGUIDs===<br/>";
			print_r($this->Post['audio_MediaGUIDs']);
			die;*/
			if($PostData){
				if(!empty($this->Post['PostVideoTitle'])){
					$PostVideoTitle = json_decode($this->Post['PostVideoTitle']);
					$PostVideoLink = json_decode($this->Post['PostVideoLink']);
				}	

				/*if(!empty($this->Post['Media'])){
					$data = json_decode($this->Post['Media'],true);

					$this->Media_model->updateMediaToEntity($data, $this->SessionUserID, $PostData['PostID']);	
				}*/

				/* check for media present - associate media with this Post */
				
				if(!empty($this->Post['file_MediaGUIDs']))
				{
					$MediaGUIDsArray = explode(",", $this->Post['file_MediaGUIDs']);
					$MediaGUIDsArray = array_unique($MediaGUIDsArray);
					foreach($MediaGUIDsArray as $MediaGUID)
					{
						$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
						if ($MediaData)
						{
							$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $PostData['PostID']);
						}
					}
				}
				/* check for media present - associate media with this Post - ends */


			   /* check for media present - associate media with this Post */
				if(!empty($this->Post['audio_MediaGUIDs']))
				{
					$MediaGUIDsArray = explode(",", $this->Post['audio_MediaGUIDs']);
					$MediaGUIDsArray = array_unique($MediaGUIDsArray);
					foreach($MediaGUIDsArray as $MediaGUID)
					{
						$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
						if ($MediaData)
						{
							$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $PostData['PostID']);
						}
					}
				}
				/* check for media present - associate media with this Post - ends */


				$this->Return['Data']['PostGUID'] = $PostData['PostGUID'];

			}
		}
	}


	/*
	Name: 			posts
	Description: 	Use to add new post
	URL: 			/api/post	
	*/
	public function editContent_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|callback_validateEntityGUID[Post,PostID]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('PostType', 'PostType', 'trim|required|in_list[Activity,Comment,Review,Question,Content]');	
		$this->form_validation->set_rules('Privacy', 'Privacy', 'trim|in_list[Public,Friends,Private]');
		$this->form_validation->set_rules('PostContent', 'PostContent', 'trim'.(empty($this->Post['MediaGUIDs']) ? '' : ''));
		$this->form_validation->set_rules('audio_MediaGUIDs', 'audio_MediaGUIDs', 'trim'); /* Media GUIDs */
		$this->form_validation->set_rules('file_MediaGUIDs', 'file_MediaGUIDs', 'trim'); /* Media GUIDs */
		if(is_array($this->Post['Media']) && !empty($this->Post['Media'])){
			foreach($this->Post['Media'] as $Key=>$Value){
				$this->form_validation->set_rules('Media['.$Key.'][MediaGUID]', 'MediaGUID', 'trim|required|callback_validateEntityGUID[Media]');
			}	
		}
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 

		// if($this->Post['CategoryGUIDs']){
		// 	if(!is_array($this->Post['CategoryGUIDs'])){
		// 		$CategoryGUIDs = explode(' ', $this->Post['CategoryGUIDs']);
		// 	}else{
		// 		$CategoryGUIDs = $this->Post['CategoryGUIDs'];
		// 	}
		// }else{
		// 	$CategoryGUIDs = array();
		// }

		if(isset($this->Post['PostVideoTitle'])){
			$PostVideoTitle = $this->Post['PostVideoTitle'];
			$PostVideoLink = $this->Post['PostVideoLink'];
		}else{
			$PostVideoTitle = NULL;
			$PostVideoLink = NULL;
		}


		$PostData = $this->Content_model->editContent($this->PostID, array(
			"CategoryGUIDs"	=>	@$this->Post['CategoryGUID'],
			"Privacy"		=>	(!empty($this->Post['Privacy']) ? @$this->Post['Privacy'] : 'Public'),
			"PostContent"	=>	@$this->Post['PostContent'],
			"PostCaption"	=>	@$this->Post['PostCaption'],
			"PostType"		=>	$this->Post['PostType'],
			"Rating"		=>	@$this->Post['Rating'],
			"PostVideoTitle"		=>	$PostVideoTitle,
			"PostVideoLink"		=>	$PostVideoLink
		));


	

		

		if($PostData){			

			if(!empty($this->Post['Media'])){
				$data = json_decode($this->Post['Media'],true);
				$this->Media_model->updateMediaToEntity($this->Post['Media'], $this->SessionUserID, $PostData['PostID']);	
			}

			/* check for media present - associate media with this Post */
			if(!empty($this->Post['file_MediaGUIDs'])){
				$MediaGUIDsArray = explode(",", $this->Post['file_MediaGUIDs']);
				foreach($MediaGUIDsArray as $MediaGUID){
					$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
					if ($MediaData){
						$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $PostData['PostID']);
					}
				}
			}
			/* check for media present - associate media with this Post - ends */


		   /* check for media present - associate media with this Post */
			if(!empty($this->Post['audio_MediaGUIDs'])){
				$MediaGUIDsArray = explode(",", $this->Post['audio_MediaGUIDs']);
				foreach($MediaGUIDsArray as $MediaGUID){
					$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
					if ($MediaData){
						$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $PostData['PostID']);
					}
				}
			}
			/* check for media present - associate media with this Post - ends */

			$this->Return['Data']['PostGUID'] = $this->input->post('PostGUID');
		}
	}
	


	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/getPosts
	*/
	public function getContents_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ParentPostGUID', 'ParentPostGUID', 'trim|callback_validateEntityGUID[Post,ParentPostID]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('PostType', 'PostType', 'trim|required|in_list[Activity,Review,Question,Comment,Content]');	
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|callback_validateEntityGUID[Post,PostID]');
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Posts=$this->Content_model->getContents('
			EU.EntityGUID UserGUID,
			E.EntityGUID PostGUID,

			E.LikedCount,
			E.ViewCount,
			E.SharedCount,
			E.SavedCount,
			
		(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=3 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.PostID)) AS CategoryNames,


		(SELECT set_categories.ParentCategoryID FROM set_categories WHERE set_categories.CategoryTypeID=3 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.PostID)) AS ParentCategoryID,

			P.PostID,
			P.PostContent,
			P.PostCaption,
			P.PostVideoTitle,
			P.PostVideoLink,
			E.EntryDate,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic = "","",CONCAT("'.''.'",U.ProfilePic)) AS ProfilePic,
			
			/* Check self liked or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.PostID AND Action="Liked"), "Yes", "No") AS IsLiked,

			/* Check self flagged or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.PostID  AND Action="Flagged"), "Yes", "No") AS IsFlagged,

			/* Check ssaved or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.PostID  AND Action="Saved"), "Yes", "No") AS IsSaved,

			IF(P.EntityID='.$this->SessionUserID.',"Yes", "No") AS IsMyPost

			',array(
				'Wall' 			=>	(empty($this->EntityID) ? 'Own' : 'Other'),
				'SessionUserID'	=>	$this->SessionUserID,
				'EntityID'		=>	(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),
				'PostID'		=>	@$this->PostID,
				'ParentPostID'	=>	@$this->ParentPostID,
				'CategoryID'	=>	@$this->CategoryID,	
				'Filter'		=>	@$this->Post['Filter'],
				'PostType'     =>	@$this->Post['PostType'],
				'Keyword'		=>	@$this->Post['Keyword'],
				'EntityID'		=>  $this->EntityID
			), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($Posts){
			$this->Return['Data'] = $Posts['Data'];
		}
	}
	/*
	Name: 			getPost
	Description: 	Use to get single post.
	URL: 			/api/post/getPosts
	*/
	public function getContent_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|callback_validateEntityGUID[Post,PostID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		//print_r($this->PostID); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$PostData=$this->Content_model->getContents('
			EU.EntityGUID UserGUID,
			E.EntityGUID PostGUID,

			E.LikedCount,
			E.ViewCount,
			E.SharedCount,
			E.SavedCount,

		(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=3 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.PostID)) AS CategoryNames,


		(SELECT set_categories.ParentCategoryID FROM set_categories WHERE set_categories.CategoryTypeID=3 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.PostID)) AS ParentCategoryID,
			
			P.PostID,
			P.PostContent,
			P.PostCaption,
			P.PostVideoTitle,
			P.PostVideoLink,
			E.EntryDate,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic = "","",CONCAT("'.''.'",U.ProfilePic)) AS ProfilePic,
			
			/* Check self liked or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.PostID AND Action="Liked"), "Yes", "No") AS IsLiked,
			
			/* Check self flagged or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.PostID  AND Action="Flagged"), "Yes", "No") AS IsFlagged,
			
			/* Check ssaved or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.PostID  AND Action="Saved"), "Yes", "No") AS IsSaved,

			IF(P.EntityID='.$this->SessionUserID.',"Yes", "No") AS IsMyPost

			',array(
				'PostID'		=>	$this->PostID,
				'SessionUserID'	=>	$this->SessionUserID,
				'EntityID'		=>  $this->EntityID
			));
		if($PostData){
			$this->Entity_model->addViewCount($this->SessionUserID, $this->PostID);
			$this->Return['Data'] = $PostData;
		}
	}



	/*
	Description: 	Use to get Get single category.
	URL: 			/api/content/getContentCount
	Input (Sample JSON): 		
	*/
	public function getContentCount_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PostType', 'PostType', 'trim|required|in_list[Activity,Comment,Review,Question,Content]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		if($this->input->post('SubjectGUID') != ""){
			$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}else if($this->input->post('CourseGUID') != ""){
			$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}
		$this->form_validation->validation($this);
		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
		if($this->input->post('SubjectGUID') != ""){			
			$ContentCategoryData = $this->Content_model->getContentCount($EntityID,$this->Post['PostType'], FALSE, @$this->Post['PageNo'], @$this->Post['PageSize'],$this->CategoryID);
		}else if($this->input->post('CourseGUID') != ""){
			$ContentCategoryData = $this->Content_model->getContentCount($EntityID,$this->Post['PostType'], FALSE, @$this->Post['PageNo'], @$this->Post['PageSize'],'',$this->CategoryID);
		}else{
			$ContentCategoryData = $this->Content_model->getContentCount($EntityID,$this->Post['PostType'], FALSE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		}
		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}


	/*
	Description: 	Use to get Get content by category.
	URL: 			/api/content/getContentByCategory
	Input (Sample JSON): 		
	*/
	public function getContentByCategory_post()
	{
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PostType', 'PostType', 'trim|required|in_list[Activity,Comment,Review,Question,Content]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);

		//print_r($this->input->post()); die;
		$ContentCategoryData = $this->Content_model->getContentByCategory($this->CategoryID,$this->Post['PostType'], FALSE, @$this->Post['PageNo'], @$this->Post['PageSize']);
				
		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}



	/*
	Description: 	(Web service)
	URL: 			/api/content/getContentBySubject
	Input (Sample JSON): 		
	*/
	public function getContentBySubject_post()
	{
		// $this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		// $this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		//$this->form_validation->set_rules('PostType', 'PostType', 'trim|required|in_list[Activity,Comment,Review,Question,Content]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		// $this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		// $this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);

		$ContentCategoryData = $this->Content_model->getContentBySubject($this->CategoryID);

		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}



	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|required|callback_validateEntityGUID[Post,PostID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		if(!$this->Content_model->deleteContent($this->SessionUserID, $this->PostID)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function deleteVideo_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('VideoEntityGUID', 'VideoEntityGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		if(!$this->Content_model->deleteVideo($this->input->post('VideoEntityGUID'))){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}



	public function deleteMedia_post()
	{
		
		$data=$this->Content_model->deleteMedia($this->input->Post('MediaGUID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Deleted Successfully"; 
		}
	}

	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function deleteTextContent_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|required|callback_validateEntityGUID[Post,PostID]');
		$this->form_validation->validation($this);  /* Run validation */
		
		if(!$this->Content_model->deleteTextContent($this->SessionUserID, $this->PostID)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}

	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function updateContentVideo_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('VideoEntityGUID', 'VideoEntityGUID', 'trim|required');
		$this->form_validation->set_rules('PostVideoLink', 'PostVideoLink', 'trim|required');
		$this->form_validation->set_rules('PostVideoTitle', 'PostVideoTitle', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		if(!$this->Content_model->updateContentVideo($this->input->post('VideoEntityGUID'),array('PostVideoTitle'=>$this->input->post('PostVideoTitle'),'PostVideoLink'=>$this->input->post('PostVideoLink')))){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function updateContentMedia_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|callback_validateEntityGUID[Post,PostID]');
		$this->form_validation->validation($this);
		//echo $this->SessionUserID; die;
		if($this->Post['Media']){
			$this->Post['Media'] = json_decode($this->Post['Media'],true);
			// if(is_array($this->Post['Media']) && !empty($this->Post['Media'])){
			// 	foreach($this->Post['Media'] as $Key=>$Value){
			// 		$this->form_validation->set_rules('Media['.$Key.'][MediaGUID]', 'MediaGUID', 'trim|required|callback_validateEntityGUID[Media]');
			// 	}	
			// }  
			// $this->form_validation->validation($this);  /* Run validation */	
			/* Validation - ends */
			if(!empty($this->Post['Media'])){			
				$this->Media_model->updateMediaToEntity($this->Post['Media'], $this->SessionUserID, $this->PostID);
				return TRUE;	
			}
		}
	}

}