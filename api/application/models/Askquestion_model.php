<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Askquestion_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	  /*GEt Faculty */
	function getFacultyList($EntityID)
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('U.UserID,U.UserGUID as ID, U.UserGUID as Userids, CONCAT_WS(" ",U.FirstName,U.LastName) FullName');
		$this->db->from('tbl_users U');
		$this->db->join('tbl_entity E', 'E.EntityID = U.UserID');
		$this->db->where("E.StatusID",2);
		$this->db->where("U.FirstName!=",'');
		$this->db->where("U.UserGUID!=",'');
		$this->db->where("U.UserTypeID",11);
		$this->db->where("E.InstituteID",$InstituteID);
		$this->db->GROUP_BY('UserID');
		$this->db->order_by('FullName','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		if($Query->num_rows()>0){

			return $Query->result();
		}
		return FALSE;
	}

	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getQueries($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select('P.QueryID QueryIDForUse, P.ParentQueryID ParentQueryIDForUse');
		$this->db->select($Field);
		$this->db->from('tbl_ask_a_question P');
		$this->db->from('tbl_users U');
		$this->db->from('tbl_users US');
		$this->db->from('tbl_entity E');
		$this->db->from('tbl_entity EU');
		$this->db->where("P.ToEntityID","U.UserID", FALSE);
		$this->db->where("P.QueryID","E.EntityID", FALSE);
		$this->db->where("P.EntityID","EU.EntityID", FALSE);
		$this->db->where("P.EntityID","US.UserID", FALSE);
		// $this->db->select('*');
		// $this->db->from('tbl_ask_a_question P');


		/* Filter - start */
		if(!empty($Where['Filter'])){
			if($Where['Filter']=='Saved'){
				$this->db->from('tbl_action A');
				$this->db->where("A.EntityID", $Where['SessionUserID']);
				$this->db->where("A.ToEntityID","P.PostID", FALSE);
				$this->db->where("A.Action", 'Saved');
			}
			elseif($Where['Filter']=='Liked'){
				$this->db->from('tbl_action A');
				$this->db->where("A.EntityID", $Where['SessionUserID']);
				$this->db->where("A.ToEntityID","P.PostID", FALSE);
				$this->db->where("A.Action", 'Liked');
			}
			elseif($Where['Filter']=='MyPosts'){
				$this->db->where("P.EntityID", $Where['SessionUserID']);
				$this->db->where("P.ToEntityID", $Where['SessionUserID']);
			}		
			elseif($Where['Filter']=='Popular'){
				$this->db->order_by('E.ViewCount','DESC');
			}
		}
		/* Filter - ends */	

		if(!empty($Where['Keyword'])){ /*search in post content*/
			$this->db->group_start();
			// $this->db->where('MATCH (P.QueryContent) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			// $this->db->or_where('MATCH (P.QueryCaption) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			$this->db->like("P.QueryContent", $Where['Keyword']);
			$this->db->or_like("P.QueryCaption", $Where['Keyword']);
			$this->db->or_like("U.FirstName", $Where['Keyword']);
			$this->db->or_like("U.LastName", $Where['Keyword']);
			$this->db->or_like("P.EntryDate", $Where['Keyword']);
			$this->db->or_like("CONCAT_WS('',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->or_like("CONCAT_WS(' ',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->group_end();
		}

		
		

		if(!empty($Where['CourseID'])){
			$this->db->where("P.CourseID",$Where['CourseID']);
		}

		if(!empty($Where['CategoryID'])){
			//$this->db->where("P.CategoryID",$Where['CategoryID']);
		}
		
		if(!empty($Where['QueryType']) && $Where['QueryType'] == "Reply"){
			// if(!empty($Where['UserTypeID']) && $Where['UserTypeID'] == 7){
			// 	$this->db->where("P.ToEntityID",$Where['EntityID']);
			// }else{
			// 	$this->db->where("P.EntityID",$Where['EntityID']);
			// }
			if(!empty($Where['QueryID'])){
				$this->db->where("P.ParentQueryID",$Where['QueryID']);
			}
			$this->db->where("P.QueryType",$Where['QueryType']);
		}else{
			$this->db->where("P.QueryType",$Where['QueryType']);
			if(!empty($Where['UserTypeID']) && $Where['UserTypeID'] == 7){
				$this->db->where("P.EntityID",$Where['EntityID']);
			}else{
				$this->db->where("P.ToEntityID",$Where['EntityID']);
			}
			if(!empty($Where['QueryID'])){
				$this->db->where("P.QueryID",$Where['QueryID']);
			}
		}

		$this->db->order_by('P.EntryDate', 'DESC');

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query();
		//echo "<pre>"; print_r($Query->result_array()); die;
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record)
			{
				//print_r($Record);

				if(!empty($Record['LikedByUserID']))
				{
					$LikedByUserID = explode(",", $Record['LikedByUserID']);
					$Record['LikedByUserID'] = count($LikedByUserID);
					if(in_array($Where['SessionUserID'], $LikedByUserID))
					{
						$Record['LikedByMe'] = 1;
					}else{
						$Record['LikedByMe'] = 0;
					}
				}else{
					$Record['LikedByMe'] = 0;
					$Record['LikedByUserID'] = 0;
				}

				/*get attached media logo - starts*/
				if(!empty($Record['MediaID']) && $Record['MediaID'] != NULL){
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AskQuestion',"MediaID" => $Record['MediaID']),TRUE);
					$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
				}else{
					$Record['MediaThumbURL'] = "";
					$Record['MediaURL'] = "";
				}

				
				/*get parent post if shared*/
				$Record['ParentPost'] =''; /*define return variable*/
				if(!empty($Record['ParentPostIDForUse'])){
                 $Record['ParentPost'] = $this->Post_model->getPosts('E.EntityGUID QueryGUID,P.QueryContent, P.QueryCaption,E.EntryDate,CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
						IF(U.ProfilePic = "","",CONCAT("' . BASE_URL . '",U.ProfilePic)) AS ProfilePic,', array('QueryID' => $Record['ParentQueryIDForUse'], 'SessionUserID' => $Where['SessionUserID'],));	
				}


				/*get parentcategory details*/
				// $Record['ParentCategory'] = array();
				// if(!empty($Record['ParentCategoryID'])){
				// 	$this->load->model('Category_model');
				// 		$Record['ParentCategory'] = $this->Category_model->getCategories('',array("CategoryID"=>$Record['ParentCategoryID']));
				// }

				$this->db->select('QueryID');
				$this->db->from('tbl_ask_a_question');
				$this->db->where('ParentQueryID',$Record['QueryID']);
				$this->db->where('QueryType','Reply');
				//$this->db->limit(1);
				$que = $this->db->get();
				if($que->num_rows() > 0){
					$Record['Answered'] = 1;
					$Record['Replies'] = $que->num_rows();
				}else{
					$Record['Answered'] = 0;
					$Record['Replies'] = 0;
				}

				//print_r($Record);
				$Record['EntryDate'] = date("d-m-Y H:i:s", strtotime($Record['EntryDate']));

				unset($Record['QueryIDForUse']);
				unset($Record['ParentQueryIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to add new post
	*/
	function addQuery($EntityID, $ToEntityID, $ParentQueryID="", $Input=array()){
		$this->db->trans_start();
		$QueryGUID = get_guid();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($Input['MediaGUID'])){
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();
			//echo $this->db->last_query(); die;
			if($que->num_rows() > 0){
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];

				$arr = array("InstituteID"=>$InstituteID);

				$this->db->where("MediaID",$MediaID);
				$this->db->update("tbl_media",$arr);
			}else{
				$MediaID = "";
			}

		}else{
			$MediaID = "";
		}

		
		/* Add post to entity table and get EntityID. */
		// $QueryID = $this->Entity_model->addEntity($QueryGUID, array(
		// 	"EntityTypeID"	=>	24,
		// 	"UserID"		=>	$EntityID,
		// 	"Privacy"		=>	@$Input["Privacy"],
		// 	"StatusID"		=>	2,
		// 	"Rating"		=>	$Input["Rating"]
		// ));

		$InsertData = array_filter(array(
			"EntityGUID" 		=>	$QueryGUID,
			"EntityTypeID"		=>	24,
			"EntryDate" 		=>	date("Y-m-d H:i:s"),
			"StatusID"			=>	2,
			"InstituteID"		=>	$InstituteID,
		));
		$this->db->insert('tbl_entity', $InsertData);
		//echo $this->db->last_query();		die;
		$QueryID = $this->db->insert_id();

		// $this->db->where('EntityID',$QueryID);
		// $this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		/* Add post */
		$InsertData = array_filter(array(
			"QueryID" 		=>	$QueryID,
			"QueryGUID" 		=>	$QueryGUID,			
			"ParentQueryID" 	=>	@$ParentQueryID,
			"EntityID" 		=>	$EntityID,
			"ToEntityID" 	=> 	$ToEntityID,
			"QueryContent" 	=>	$Input["QueryContent"],
			"QueryCaption" 	=>	@$Input["QueryCaption"],
			"QueryType"  => @$Input["QueryType"],
			"CourseID"  => @$Input["CourseID"],
			"CourseID"  => @$Input["CourseID"],
			"MediaID"  => @$MediaID,
			"EntryDate"  => date("Y-m-d H:i:s")
		));
		$this->db->insert('tbl_ask_a_question', $InsertData);
		//echo $this->db->last_query();		die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('QueryID' => $PostID, 'PostGUID' => $EntityGUID);
	}


	function editQuery($EntityID, $ToEntityID, $QueryID, $Input=array()){
		$this->db->trans_start();
		$QueryGUID = get_guid();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($Input['MediaGUID'])){
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();
			//echo $this->db->last_query(); die;
			if($que->num_rows() > 0){
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];

				$arr = array("InstituteID"=>$InstituteID);

				$this->db->where("MediaID",$MediaID);
				$this->db->update("tbl_media",$arr);
			}else{
				$MediaID = "";
			}

		}else{
			$MediaID = "";
		}

		/* Add post */
		$UpdateData = array_filter(array(
			"EntityID" 		=>	$EntityID,
			"ToEntityID" 	=> 	$ToEntityID,
			"QueryContent" 	=>	$Input["QueryContent"],
			"QueryCaption" 	=>	@$Input["QueryCaption"],
			"CourseID"  => @$Input["CourseID"],
			"MediaID"  => @$MediaID,
			"EntryDate"  => date("Y-m-d H:i:s")
		));
		$this->db->Where("QueryID", $QueryID);
		$this->db->update('tbl_ask_a_question', $UpdateData);
		//echo $this->db->last_query();		die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('QueryID' => $QueryID);
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deleteQuery($UserID, $QueryID){
		//echo $QueryID; die;
		// $PostData = $this->Post_model->getPosts('P.QueryID',array('QueryID'=>$QueryID, 'SessionUserID'=>$UserID));
		// print_r($PostData);
		// if(!empty($PostData) && $UserID==$PostData['EntityID']){
		// 	$this->Entity_model->deleteEntity($PostID);
			$this->db->Where("QueryID", $QueryID);
			$this->db->delete("tbl_ask_a_question");
			return TRUE;
		// }
		// return FALSE;
	}


	function setLikeDislike($EntityID, $QueryID, $Liked)
	{
		if($Liked == 0)
		{			
			$sql = $this->db->query("UPDATE tbl_ask_a_question SET  LikedByUserID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', LikedByUserID, ','), ',".$EntityID.",', ','))	WHERE FIND_IN_SET('".$EntityID."',LikedByUserID) AND  `QueryID` = ".$QueryID);

		}
		else
		{
			$sql = "SELECT LikedByUserID FROM tbl_ask_a_question WHERE `QueryID` = ".$QueryID." AND ( LikedByUserID IS NOT NULL AND LikedByUserID != '' )";

			$data = $this->db->query($sql);
			
			if($data->num_rows() > 0)
			{
				$sql = $this->db->query("UPDATE tbl_ask_a_question SET `LikedByUserID` = CONCAT(`LikedByUserID`, ',".$EntityID."') WHERE `QueryID` = ".$QueryID);
			}
			else
			{
				$sql = $this->db->query("UPDATE tbl_ask_a_question SET `LikedByUserID` = ".$EntityID." WHERE `QueryID` = ".$QueryID);
			}			
		}


		$this->db->select("LikedByUserID");
		$this->db->from('tbl_ask_a_question');
		$this->db->where('QueryID',$QueryID);
		$this->db->where('LikedByUserID != ""');
		$this->db->limit(1);
		$Query = $this->db->get();		
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$Records = $Record;
			}

			if(isset($Records['LikedByUserID']) && !empty($Records['LikedByUserID']))
			{
				$arr = explode(",", $Records['LikedByUserID']);

				return count($arr);
			}
		}		

		return 0;
	}


	/*-------------------------------------------------------------
    Ask A Question - For Every One Global Section
    -------------------------------------------------------------*/
	function getAAQList($EntityID, $Inputs = array())
	{  
		$data = $arr = array(); 

		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);

		$append = "";
		if(isset($Inputs['filterSubject']) && !empty($Inputs['filterSubject']))
		{
			$filterSubject = $Inputs['filterSubject'];
			$append .= " AND s.SubjectGUID = '".$filterSubject."' ";
		}

		if(isset($Inputs['filterRelatedTo']) && !empty($Inputs['filterRelatedTo']))
		{
			$filterRelatedTo = $Inputs['filterRelatedTo'];
			$append .= " AND r.RelatedToGUID = '".$filterRelatedTo."' ";
		}

		if(isset($Inputs['filterQuestion']) && !empty($Inputs['filterQuestion']))
		{
			$filterQuestion = $Inputs['filterQuestion'];
			$append .= " AND a.aaqContent LIKE '%".$filterQuestion."%' ";
		}

		if(isset($Inputs['Type']) && !empty($Inputs['Type']) && $Inputs['Type'] == "MyQuestion")
		{			
			if(!empty($UserTypeID) && $UserTypeID == 90){
				$append .= " AND a.aaqParentID <= 0 AND aaqID IN (select DISTINCT(aaqParentID) from tbl_aaq where `aaqEntityID` = $EntityID) ";	
			}else{				
				$append .= " AND a.aaqParentID <= 0 ";
				$append .= " AND a.aaqEntityID = '".$EntityID."' ";		
			}					
		}

		$PageNo = 1; $PageSize = 50;
		if(isset($Inputs['PageNo']) && !empty($Inputs['PageNo']))
		{
			$PageNo = $Inputs['PageNo'];
		}
		if(isset($Inputs['PageSize']) && !empty($Inputs['PageSize']))
		{
			$PageSize = $Inputs['PageSize'];
		}

		$PageNo = ($PageNo - 1) * $PageSize;

		$sql = "SELECT a.aaqID, a.aaqParentID, a.aaqContent, a.aaqPostDate, s.SubjectName, r.RelatedToName
		FROM tbl_aaq a
		INNER JOIN tbl_subjects s ON s.SubjectID = a.aaqSubject
		INNER JOIN tbl_related_to r ON r.RelatedToID = a.aaqRelatedTo 
		WHERE a.StatusID = 2  $append
		ORDER BY aaqID DESC
		LIMIT $PageNo, $PageSize		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data["TotalRecords"] = $Query->num_rows();
			foreach($Query->result_array() as $Where)
			{				
				$aaqID = $Where['aaqID']; 
				if(!empty($UserTypeID) && $UserTypeID == 90 && isset($Inputs['Type']) && !empty($Inputs['Type']) && $Inputs['Type'] == "MyQuestion"){
					$Where['ReplyCount'] = $this->getReplyCount($Where['aaqID'],$UserTypeID,$EntityID,$Inputs['Type']);
				}else{
					$Where['ReplyCount'] = $this->getReplyCount($aaqID,$UserTypeID);
				}			

				$arr[] = $Where;				
			}

			$data["Records"] = $arr;
			return $data;
		}

		return FALSE;	
	}


	function getReplyCount($aaqID,$UserTypeID="",$EntityID="",$Type="") 
	{
		$this->db->trans_start();		

		$count = 0;	$append = "";


		if(!empty($UserTypeID) && $UserTypeID == 90 && $Type == "MyQuestion"){
			$append .= "AND a.aaqParentID != 0 AND a.aaqParentID = '".$aaqID."' AND a.aaqEntityID = '".$EntityID."'";
		}else{
			$append .= "AND a.aaqParentID > 0 AND a.aaqParentID = '$aaqID'";
		}

		$sql = "SELECT count(aaqID) as total
		FROM tbl_aaq a
		WHERE a.StatusID = 2 $append
		GROUP BY a.aaqParentID";

		$Query = $this->db->query($sql);	
					
		if($Query->num_rows() > 0)
		{
			$ID = $Query->result();

			$count = $ID[0]-> total;
		}

		return $count;		
	}	


	function addAAQ($EntityID, $Input=array())
	{
		$this->db->trans_start();		

		$MediaID = 0;
		if(!empty($Input['MediaGUID']))
		{
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];
			}
		}

		$SubjectID = $RelatedToID = 0;
		//Add new "subject" if not exist in database------------------------------------
		if(!empty($Input['aaq_add_subject']))
		{
			$this->db->select('SubjectID');
			$this->db->from('tbl_subjects');
			$this->db->like('SubjectName',$Input['aaq_add_subject']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$arrID = $que->result_array();
				$SubjectID = $arrID[0]['SubjectID'];
			}
			else
			{
				$SubjectGUID = get_guid();				
				$InsertData = array_filter(array(
					"SubjectGUID" 		=>	$SubjectGUID,
					"SubjectName" 		=>	$Input['aaq_add_subject'],			
					"SubjectAddBy" 	=>	$EntityID,
					"SubjectCreatedDate" 		=>	date("Y-m-d H:i:s")
				));
				$this->db->insert('tbl_subjects', $InsertData);
				$SubjectID = $this->db->insert_id();
			}
		}

		//Add new "related to" if not exist in database------------------------------------
		if(!empty($Input['aaq_add_relatedto']))
		{
			$this->db->select('RelatedToID');
			$this->db->from('tbl_related_to');
			$this->db->like('RelatedToName',$Input['aaq_add_relatedto']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$arrID = $que->result_array();
				$RelatedToID = $arrID[0]['RelatedToID'];
			}
			else
			{
				$RelatedToGUID = get_guid();				
				$InsertData = array_filter(array(
					"RelatedToGUID" 		=>	$RelatedToGUID,
					"RelatedToName" 		=>	$Input['aaq_add_relatedto'],			
					"RelatedToAddBy" 	=>	$EntityID,
					"RelatedToCreatedDate" 		=>	date("Y-m-d H:i:s")
				));
				$this->db->insert('tbl_related_to', $InsertData);
				$RelatedToID = $this->db->insert_id();
			}
		}

		/* Add question */
		$InsertData = array_filter(array(
			"aaqSubject" 		=>	$SubjectID,
			"aaqRelatedTo" 		=>	$RelatedToID,			
			"aaqContent" 	=>	$Input['aaq_add_question'],
			"aaqPostDate" 		=>	date("Y-m-d H:i:s"),
			"aaqMediaID" 	=> 	$MediaID,
			"aaqEntityID" 	=>	$EntityID
		));
		$this->db->insert('tbl_aaq', $InsertData);		

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}



	function getAAQReply($EntityID, $Inputs = array())
	{  
		$data = $arr = array();
		$this->load->model('Media_model');	

		$aaqID = $Inputs['AAQID'];

		$sql = "SELECT a.aaqID, a.aaqContent, a.aaqPostDate, a.aaqMediaID,a.LikedByUserID, s.SubjectName, r.RelatedToName, s.SubjectID, r.RelatedToID, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as fullname, ut.UserTypeName
		FROM tbl_aaq a
		INNER JOIN tbl_subjects s ON s.SubjectID = a.aaqSubject
		INNER JOIN tbl_related_to r ON r.RelatedToID = a.aaqRelatedTo
		INNER JOIN tbl_users u ON u.UserID = a.aaqEntityID
		INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID 
		WHERE a.StatusID = 2 AND a.aaqID = '$aaqID'
		LIMIT 1			
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{	
				if(!empty($Where['LikedByUserID']))
				{
					$LikedByUserID = explode(",", $Where['LikedByUserID']);
					$Where['LikedByUserID'] = count($LikedByUserID);
					if(in_array($EntityID, $LikedByUserID))
					{
						$Where['LikedByMe'] = 1;
					}else{
						$Where['LikedByMe'] = 0;
					}
				}else{
					$Where['LikedByMe'] = 0;
					$Where['LikedByUserID'] = 0;
				}			
				$data["QuestionDetails"] = $Where;				
			}

			if(!empty($EntityID)){
				//$this->db->select("SUM(Rating) as Rating");
				$this->db->select("Rating");
				$this->db->from("tbl_aaq_rating");
				$this->db->where("UserID",$EntityID);
				$this->db->where("aaqID",$aaqID);
				$Rating = $this->db->get(); //echo $this->db->last_query(); die;
				if($Rating->num_rows() > 0){
					$Ratings = $Rating->result_array();
					$data["QuestionDetails"]['Rating'] = $Ratings[0]['Rating'];
					//$data["QuestionDetails"]['Rating'] = ceil( $Ratings[0]['Rating'] / 5 );
				}else{
					$data["QuestionDetails"]['Rating'] = 1;
				}
			}else{
				$data["QuestionDetails"]['Rating'] = 1;
			}

			$data["QuestionDetails"]['aaqPostDate'] = get_time_ago($data["QuestionDetails"]['aaqPostDate']);

			if($data["QuestionDetails"]['aaqMediaID'] > 0)			
			{				
				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AAQ',"MediaID" => $data["QuestionDetails"]['aaqMediaID']),TRUE);
				$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());				
				$data["QuestionDetails"]['aaqMediaID'] = $MediaID['Records'][0]['MediaURL'];				
				$data["QuestionDetails"]['aaqMediaExt'] = pathinfo($MediaID['Records'][0]['MediaURL'], PATHINFO_EXTENSION);	
			}
			
			return $data;
		}

		return FALSE;	
	}


	function getAAQReplyOnly($EntityID, $Inputs = array())
	{     
		$data = $arr = array();
		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);
		$this->load->model('Media_model');

		$append = "";

		if(!empty($UserTypeID) && $UserTypeID == 90 && !empty($Inputs['Type']) && $Inputs['Type'] == "MyQuestion"){
			$append .= " AND a.aaqEntityID = '".$EntityID."'";
		}

		$aaqID = $Inputs['AAQID'];
		$filterReply = $Inputs['filterReply'];

		if(isset($filterReply) && !empty($filterReply) && $filterReply != "undefined")
		{
			$filterReply = " AND (a.aaqContent LIKE '%$filterReply%' OR u.FirstName LIKE '%$filterReply%' OR u.LastName LIKE '%$filterReply%') ";
		}
		else
		{
			$filterReply = "";
		}

		$PageNo = 1; $PageSize = 50;
		if(isset($Inputs['PageNo']) && !empty($Inputs['PageNo']))
		{
			$PageNo = $Inputs['PageNo'];
		}
		if(isset($Inputs['PageSize']) && !empty($Inputs['PageSize']))
		{
			$PageSize = $Inputs['PageSize'];
		}

		$PageNo = ($PageNo - 1) * $PageSize;


		$sql = "SELECT a.aaqID, a.aaqContent, a.aaqPostDate, a.aaqMediaID, a.LikedByUserID, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as fullname, IF(u.ProfilePic IS NULL,
		CONCAT('".BASE_URL."','uploads/profile/picture/','default.jpg'),
		CONCAT('".BASE_URL."','uploads/profile/picture/',u.ProfilePic)) AS ProfilePic,
		ut.UserTypeName
		FROM tbl_aaq a			
		INNER JOIN tbl_users u ON u.UserID = a.aaqEntityID
		INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID
		WHERE a.StatusID = 2 AND a.aaqParentID = '$aaqID' $filterReply $append
		ORDER BY a.aaqID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$Where['aaqPostDate'] = get_time_ago($Where['aaqPostDate']);
				if($Where['aaqMediaID'] > 0)			
				{				
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AAQ',"MediaID" => $Where['aaqMediaID']),TRUE);
					$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Where['aaqMediaID'] = $MediaID['Records'][0]['MediaURL'];
					$Where["aaqMediaExt"] = pathinfo($MediaID['Records'][0]['MediaURL'], PATHINFO_EXTENSION);		
				}

				if(!empty($Where['LikedByUserID']))
				{
					$LikedByUserID = explode(",", $Where['LikedByUserID']);
					$Where['LikedByUserID'] = count($LikedByUserID);
					if(in_array($EntityID, $LikedByUserID))
					{
						$Where['LikedByMe'] = 1;
					}else{
						$Where['LikedByMe'] = 0;
					}
				}else{
					$Where['LikedByMe'] = 0;
					$Where['LikedByUserID'] = 0;
				}		

				if(!empty($EntityID)){
					//$this->db->select("SUM(Rating) as Rating");
					if($UserTypeID == 7 || $UserTypeID == 88){
						$this->db->select("Rating");
					}else{
						$this->db->select("SUM(Rating) as Rating");
					}
					$this->db->from("tbl_aaq_rating");
					if($UserTypeID == 7 || $UserTypeID == 88){
						$this->db->where("UserID",$EntityID);
					}					
					$this->db->where("aaqID",$Where['aaqID']);
					$Rating = $this->db->get(); //echo $this->db->last_query(); die;
					if($Rating->num_rows() > 0){
						$Ratings = $Rating->result_array();
						if($Ratings[0]['Rating'] > 0){
							$Where['Rating'] = $Ratings[0]['Rating'];
							$Where['Overall_Rating'] = ceil( $Ratings[0]['Rating'] / 5 );
						}else{
							$Where['Rating'] = 1;
							$Where['Overall_Rating'] = 1;
						}						
					}else{
						$Where['Rating'] = 1;
						$Where['Overall_Rating'] = 1;
					}
				}else{
					$Where['Rating'] = 1;
					$Where['Overall_Rating'] = 1;
				}

				$arr[] = $Where;				
			}

			$data["QuestionReplyDetails"] = $arr;
		
			return $data;
		}	

		return FALSE;	
	}



	function addAAQReply($EntityID, $Input=array())
	{
		$this->db->trans_start();		

		$MediaID = 0;
		if(!empty($Input['MediaGUID']))
		{
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];
			}
		}



		/* Add question */
		$InsertData = array_filter(array(
			"aaqParentID" 		=>	$Input['AAQID'],
			"aaqSubject" 		=>	$Input['aaq_reply_subject'],
			"aaqRelatedTo" 		=>	$Input['aaq_reply_related_to'],			
			"aaqContent" 		=>	$Input['aaq_reply_content'],
			"aaqPostDate" 		=>	date("Y-m-d H:i:s"),
			"aaqMediaID" 		=> 	$MediaID,
			"aaqEntityID" 		=>	$EntityID
		));
		$this->db->insert('tbl_aaq', $InsertData);		

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}



	function AAQSetLikeDislike($EntityID, $aaqID, $Liked)
	{
		if($Liked == 0)
		{	
			//echo "UPDATE  tbl_aaq SET  LikedByUserID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', LikedByUserID, ','), ',".$EntityID.",', ','))	WHERE FIND_IN_SET('".$EntityID."',LikedByUserID) AND  `aaqID` = '".$aaqID."'"; die;		
			$sql = $this->db->query("UPDATE  tbl_aaq SET  LikedByUserID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', LikedByUserID, ','), ',".$EntityID.",', ','))	WHERE FIND_IN_SET('".$EntityID."',LikedByUserID) AND  `aaqID` = '".$aaqID."'");

		}
		else
		{

			$sql = "SELECT LikedByUserID FROM tbl_aaq WHERE `aaqID` = '".$aaqID."' AND ( LikedByUserID IS NOT NULL AND LikedByUserID != '' )";

			$data = $this->db->query($sql);
			
			if($data->num_rows() > 0)
			{
				$sql = $this->db->query("UPDATE tbl_aaq SET `LikedByUserID` = CONCAT(`LikedByUserID`, ',".$EntityID."') WHERE `aaqID` = '".$aaqID."'");
			}
			else
			{
				$sql = $this->db->query("UPDATE tbl_aaq SET `LikedByUserID` = ".$EntityID." WHERE `aaqID` = '".$aaqID."'");
			}			
		}


		$this->db->select("LikedByUserID");
		$this->db->from('tbl_aaq');
		$this->db->where('aaqID',$aaqID);
		$this->db->where('LikedByUserID != ""');
		$this->db->limit(1);
		$Query = $this->db->get();		
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$Records = $Record;
			}

			if(isset($Records['LikedByUserID']) && !empty($Records['LikedByUserID']))
			{
				$arr = explode(",", $Records['LikedByUserID']);

				return count($arr);
			}
		}		

		return 0;
	}

	function setRating($EntityID, $aaqID, $Rating){
		$this->db->select("*");
		$this->db->from("tbl_aaq_rating");
		$this->db->where("aaqID",$aaqID);
		$this->db->where("UserID",$EntityID);
		$this->db->limit(1);
		$data = $this->db->get();
		if($data->num_rows() > 0){
			$this->db->where("aaqID",$aaqID);
			$this->db->where("UserID",$EntityID);
			$this->db->update("tbl_aaq_rating",array("Rating"=>$Rating,"RatingDate"=>date("Y-m-d h:i:s")));
			return TRUE;
		}else{
			$this->db->insert("tbl_aaq_rating",array("aaqID"=>$aaqID,"UserID"=>$EntityID,"Rating"=>$Rating,"RatingDate"=>date("Y-m-d h:i:s")));
			return TRUE;
		}
		return FALSE;
	}
}