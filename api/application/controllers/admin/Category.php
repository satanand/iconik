<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Category extends API_Controller_Secure

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('Category_model');

	}



	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function add_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		
		if($this->input->post('CategoryTypeName') == 'Subject')
		{
			$this->form_validation->set_rules('ParentCategoryGUID', 'Course', 'trim|required|callback_validateEntityGUID[Category,ParentCategoryID]');
		}
		elseif($this->input->post('CategoryTypeName') == 'Course')
		{
			$this->form_validation->set_rules('ParentCategoryGUID', 'Stream', 'trim|required|callback_validateEntityGUID[Category,ParentCategoryID]');
		}
		elseif($this->input->post('CategoryTypeName') == 'Stream')
		{
			$this->form_validation->set_rules('ParentCategoryGUID', 'Stream', 'trim|callback_validateEntityGUID[Category,ParentCategoryID]');
		}
		elseif($this->input->post('CategoryTypeName') == 'Topic')
		{
			$this->form_validation->set_rules('ParentCategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,ParentCategoryID]');
		}

		$this->form_validation->set_rules('CategoryTypeName', 'Category Type', 'trim|required|callback_validateCategoryTypeName');

		//$is_unique =  '|is_unique[set_categories.CategoryName]'

		if($this->input->post('CategoryTypeName') == 'Subject')
		{
			$this->form_validation->set_rules('CategoryName', 'Subject Name', 'trim|required');
		}
		elseif($this->input->post('CategoryTypeName') == 'Topic')
		{
			$this->form_validation->set_rules('CategoryName', 'Topic Name', 'trim|required');
		}
		else
		{
			$this->form_validation->set_rules('CategoryName', 'Category Name', 'trim|required', array("required"=>"The Name field is required"));  
		}

		if($this->input->post('CategoryTypeName') == 'Course'){
			$this->form_validation->set_rules('Duration', 'Duration', 'trim|required|greater_than[0]');
		}		

		

		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');



		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim'); /* Media GUIDs */

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$type = $this->input->post('CategoryTypeName');

		$CategoryData = $this->Category_model->addCategory(@$this->ParentCategoryID, $this->SessionUserID, $this->CategoryTypeID, $this->Post['CategoryName'], $this->StatusID, $this->Post['Duration']);

		if($CategoryData){

			if($CategoryData == 'EXIST'){
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This ".$type." name already exist.";
			}else{

				$this->Return['Data']['CategoryGUID'] = $CategoryData['CategoryGUID'];



				/* check for media present - associate media with this Post */

				if(!empty($this->Post['MediaGUIDs'])){

					$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);

					foreach($MediaGUIDsArray as $MediaGUID){

						$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));

						if ($MediaData){

							$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $CategoryData['CategoryID']);

						}

					}

				}

				/* check for media present - associate media with this Post - ends */

				$this->Return['Message']      	=	"New ".$type." added successfully."; 
			}

		}

	}


	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function editCategory_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('CategoryName', 'Category Name', 'trim|required', array("required"=>"The Name field is required"));

		if($this->input->post('CategoryTypeID') == 2){
			$this->form_validation->set_rules('Duration', 'Duration', 'trim|required|greater_than[0]');
		}

		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');



		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$type = $this->input->post('CategoryTypeName');

		$CategoryData = $this->Category_model->editCategory($this->CategoryID, array('CategoryName'=>$this->Post['CategoryName'],'Duration' =>$this->Post['Duration'], 'StatusID'=>$this->StatusID));


		if($CategoryData){

			//print_r($CategoryData); die;

			if($CategoryData == 1){

				//print_r($CategoryData); die;

				$this->Return['Data']['CategoryGUID'] = $CategoryData['CategoryGUID'];

				/* check for media present - associate media with this Post */

				if(!empty($this->Post['MediaGUIDs'])){

					$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);

					foreach($MediaGUIDsArray as $MediaGUID){

						$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));

						if ($MediaData){

							$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $this->CategoryID);

						}

					}

				}

				$CategoryData = $this->Category_model->getCategories('',array("CategoryID"=>$this->CategoryID));

				$this->Return['Data'] = $CategoryData;

				$this->Return['Message']      	=	"Updated successfully."; 
				
			}else if($CategoryData == 3){

				//print_r($CategoryData); die;

				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This category name already exist.";
			}

		}
	}





	/*

	Description: 	use to get list of filters

	URL: 			/api_admin/entity/getFilterData	

	*/

	public function getFilterData_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('ParentCategoryGUID', 'Parent Category', 'trim|callback_validateEntityGUID[Category,ParentCategoryID]');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */





		$CategoryTypes = $this->Category_model->getCategoryTypes('',array("ParentCategoryID"=>@$this->ParentCategoryID),true,1,250);

		if($CategoryTypes){

			$Return['CategoryTypes'] = $CategoryTypes['Data']['Records'];			

		}

		$this->Return['Data'] = $Return;

	}

}

