<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
class Institute extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Recovery_model');
		$this->load->model('Users_model');
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
		$this->load->model('Institute_model');
		$this->load->model('Keys_model');
		$this->load->library("excel");
	}


	/*
	Description: 	Use to get Get Attributes.
	URL: 			/api/inst/importInstitutes
	Input (Sample JSON): 		
	*/
	public function importInstitutes_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$MediaURL = explode("/", $this->input->post('MediaURL'));
		$mediaName = end($MediaURL);

		//$instData = $this->excel->excel_to_array("uploads/institutes/coaching_classes_in_india_state_wise_1561112963.xlsx");

		$instData = $this->excel->excel_to_array("uploads/institutes/".$mediaName);
		//echo "<pre>"; print_r($studentData); echo "nfdslknflksd"; die;
		$arr_keys = array_keys($instData[0]);

		$target_keys = array("State","Dist.","Name of coaching class","contact. No","address","Email address","Website");
		
		// foreach ($target_keys as $key => $value) {			
		// 	if(!in_array($value, $arr_keys)){ print_r($value);
		// 		$this->Return['ResponseCode'] 	=	500;
		// 		$this->Return['Message']      	=	'Sorry! This file format is not correct.<a style="text-decoration: underline; padding: 12px;" href="/asset/student/tbl_users.xlsx" download="">Download excel to get format to add students</a>.';
		// 	}
		// }

		//die;

		$studentsId = array();
		$InsertStudent = array();
		$UserIDs = array();
		//echo count($studentData); die;

		for ($i=0; $i < count($instData); $i++) { 
			if(!empty($instData[$i]['Name of coaching class'])){
				//print_r($studentData[$i]); die;
				$InsertData = array();
				$InsertStudent = array();

				if(!empty($instData[$i]['Email address']) && $instData[$i]['Email address'] != 'NA'){
					$Email = $instData[$i]['Email address'];
				}else{
					$Email = str_replace(' ', '', $instData[$i]['Name of coaching class']);
					$Email = $Email."".$i."@mailinator.com";
				}

				$InsertData = array(
					"Name" 	=>	@$instData[$i]['Name of coaching class'],	
					"Mobile" =>	@$instData[$i]['Mobile No.'],	
					"State" =>	@$instData[$i]['State'],
					"City" 	=>	@$instData[$i]['Dist.'],
					"Course" =>	@$instData[$i]['Course'],
					"Rating" => rand(0,5),
					"OfficeNo" => @$instData[$i]['Office No.'],
					"Address" => @$instData[$i]['Address']
				);

				$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

				//$User = $this->Users_model->addUser($InsertData, 87, 1, 1);

				$UserID = $this->Institute_model->addINST($InsertData);

				if($UserID) {
					array_push($UserIDs, $UserID);
				}			
			}
		}

		$this->Return['Message']      	=	count($UserIDs)." Institutes are added successfully."; 

	}

	/*
	Name: 			Signup
	Description: 	Use to register user to system.
	URL: 			/api/signup/
	*/
	public function add_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('FirstName', 'Institute Name', 'trim|required');

		$this->form_validation->set_rules('Email', 'Email', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail');
		
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[2,3,10]');

	    $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]');

		$this->form_validation->set_message('is_unique', 'This Mobile Number already exists');

		$this->form_validation->set_rules('DeviceType', 'Device type', 'trim|required|callback_validateDeviceType');

		$this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[6]');

		$this->form_validation->validation($this); /* Run validation */
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$UserID = $this->Institute_model->addUser($this->EntityID, $this->Post, $this->Post['UserTypeID'], 1, ($this->Post['Source'] != 'Direct' ? '2' : '1') /*if source is not Direct Account treated as Verified*/);
		if (!$UserID) {
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "An error occurred, please try again later.";
		}
		else {
			if (!empty($this->Post['Email'])) {
				/* Send welcome Email to User with Token. (only if source is Direct) */
				$Token = ($this->Post['Source'] == 'Direct' ? $this->Recovery_model->generateToken($UserID, 2) : '');

				$content = $this->load->view('emailer/verify_and_set_password', array(
						"Name" => @$this->Post['InstituteName'],
						'Token' => $Token,
						'DeviceTypeID' => $this->DeviceTypeID
					) , TRUE);
				sendMail(array(
					'emailTo' => $this->Post['Email'],
					'emailSubject' => "Thank you for registering at " . SITE_NAME,
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));
			}

			/*referal code generate*/
			$this->load->model('Utility_model');
			$this->Utility_model->generateReferralCode($UserID);
			/* Send welcome notification */
			$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);
			/* get user data */
			$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
				'UserID' => $UserID
			));
			/* create session only if source is not Direct and account treated as Verified. */
			$UserData['SessionKey'] = '';

			$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(
				"IPAddress" => @$this->Post['IPAddress'],
				"SourceID" => 1,
				"DeviceTypeID" => $this->DeviceTypeID,
				"DeviceGUID" => @$this->Post['DeviceGUID'],
				"DeviceToken" => @$this->Post['DeviceToken']
			));

			$this->Return['Data'] = $UserData;
			$this->Return['Message']  ="Thank you for registering  a Institute.
                       An email has been sent to on given email id."; 
		}
	}


	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	public function getInstitutes_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$this->Post['OrderBy'] = "E.EntryDate";
		$this->Post['Sequence'] = "DESC";
		//print_r($this->Post);
		$UsersData=$this->Institute_model->getUsers($this->EntityID,'RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}	


	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	public function getAllInstitutes_post()
	{
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('StateName', 'StateName', 'trim');
		$this->form_validation->set_rules('CityName', 'CityName', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Type', 'Type', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		/*$UsersData = $this->Institute_model->getAllInstitutes($this->EntityID,'RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);*/

		$UsersData = $this->Institute_model->getAllInstitutesSearch($this->EntityID,$this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);
		
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}	
	
	

	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	// public function exportInstitutes_post()
	// {
	// 	/* Validation section */
	// 	$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
	// 	$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
	// 	$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
	// 	$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
	// 	$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
	// 	$this->form_validation->validation($this);  /* Run validation */		
	// 	/* Validation - ends */
	// 	$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
	// 	$UsersData=$this->Institute_model->getUsers($this->EntityID,'RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
	// 	if($UsersData){
	// 		$this->Return['Data'] = $UsersData['Data'];
	// 	}
	// }	



	/*
	Name: 			updateUserInfo
	Description: 	set masterFranchisee
	URL: 			api/institute/updateMasterFranchisee/	
	*/
	public function updateMasterFranchisee_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('MasterFranchisee', 'MasterFranchisee', 'trim|required');
		$this->form_validation->set_rules('UserTypeID', 'User Type');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Users_model->updateUserInfo($this->UserID, array("MasterFranchisee"=>$this->input->post('MasterFranchisee'), "ParentUserID"=>$this->UserID));

		// if($this->input->post('MasterFranchisee') == 'yes'){
		// 	$this->Keys_model->allotKeysToMasterFranchisee($this->EntityID,$this->UserID);
		// }

		$Data = $this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee,ParentUserID',array("UserID" => $this->UserID));

		if($Data){
			if($this->input->post('MasterFranchisee') == 'yes'){
				$this->Keys_model->allotKeysToMasterFranchisee($this->EntityID,$this->UserID);
			}
			$this->Return['Data'] = $Data;
			$this->Return['Message']  =	"Successfully updated."; 
		}else{
			$this->Return['ResponseCode']  =	500; 
			$this->Return['Message']  =	"Sorry! Some error occurred"; 
		}			
	}


	/*
	Name: 			updateUserInfo
	Description: 	set masterInstitute
	URL: 			api/institute/updateMasterInstitute/	
	*/
	public function updateMasterInstitute_post(){
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,EntityID]');
		$this->form_validation->set_rules('InstituteGUID', 'InstituteGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		//print_r($this->UserID); die;
		if(!empty($this->input->post('InstituteGUID')) && ($this->input->post('InstituteGUID') != $this->input->post('UserGUID'))){
			$this->Users_model->updateUserInfo($this->EntityID,array('ParentUserID'=>$this->UserID));
		}else if(!empty($this->input->post('InstituteGUID')) && ($this->input->post('InstituteGUID') == $this->input->post('UserGUID'))){
			$this->Users_model->updateUserInfo($this->EntityID,array('ParentUserID'=>''));
		}else{
			$this->Users_model->updateUserInfo($this->EntityID,array('ParentUserID'=>''));
		}
		

		$Data = $this->Return['Data']=$this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee,ParentUserID',array("UserID" => $this->EntityID));

		if($Data){
			$this->Return['Data'] = $Data;
			$this->Return['Message']  =	"Successfully updated."; 
		}else{
			$this->Return['ResponseCode']  =	500; 
			$this->Return['Message']  =	"Sorry! Some error occurred"; 
		}
	}


	function setFavourite_post()
	{
		$this->form_validation->set_rules('InstituteGUID', 'InstituteGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Institute_model->setFavourite($this->EntityID, $this->UserID);

		if($Data){
			$this->Return['Data'] = $Data;
			$this->Return['Message']  =	"Successfully done."; 
		}else{
			$this->Return['ResponseCode']  =	500; 
			$this->Return['Message']  =	"Sorry! Some error occurred"; 
		}
	}
}

