<?php

defined('BASEPATH') OR exit('No direct script access allowed'); 



class Users extends API_Controller_Secure

{

	function __construct()

	{

		parent::__construct();
        $this->load->model('Users_model');
        $this->load->model('Staff_model');
        $this->load->model('Recovery_model');

	}


	/*

	Name: 			resendverify

	Description: 	Use to resend OTP for mobile number verification.

	URL: 			/api/Users/resendPhoneOTP

	*/

	public function resendPhoneOTP_post()

	{

		/* Validation section */

		if($this->input->post('UserTypeID') == 7 && !empty($this->input->post('UserGUID'))){
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		}else if(!empty($this->input->post('UserGUID'))){
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		}

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserData = $this->Users_model->getUsers('UserID, FirstName, StatusID, Email, PhoneNumberForChange', array(

			'UserID' => $this->UserID

		));





		if(empty($UserData)){

			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Sorry! Some error occured, Please try Later.";

		}

		elseif($UserData && $UserData['StatusID']==3){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been deleted. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==4){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been blocked. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==6){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"You have deactivated your account, please contact the Admin to reactivate.";

		}
		elseif(empty($UserData['PhoneNumberForChange'])){
			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"You have already verified your mobile number.";
		}
		else{

			/* Re-Send OTP to user to verify mobile number. */
			$Token = $this->Recovery_model->generateToken($UserData['UserID'], 3);
			$Input_arr['PhoneNumber'] =  $UserData['PhoneNumberForChange'];
			$Input_arr['Message'] =  "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes.";
			sendSMS($Input_arr);

			//print_r($UserData); die;

			$this->Return['Message'] = "Please enter the OTP, just sent on your registered mobile number.";

		}

	}



	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/accountDeactivate/	

	*/
  
	public function accountDeactivate_post()

	{

		$this->Entity_model->updateEntityInfo($this->SessionUserID, array("StatusID"=>6));

		$this->Users_model->deleteSessions($this->SessionUserID);

		$this->Return['Message'] =	"Your account has been deactivated.";

	}


	/*
	Name: 			get usertype
	Description: 	Use to get usertype.
	URL: 			/api_admin/users/usertype
	*/
	public function getusertype_post()
	{
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$UserTypeData = $this->Users_model->getusertype($this->EntityID);

		if(!empty($UserTypeData)){
			$this->Return['Data'] = $UserTypeData['Data'];
		}
	}

	/*

	Name: 			toggleAccountDisplay

	Description: 	Use to hide account to others.

	URL: 			/user/toggleAccountDisplay/	

	*/

	public function toggleAccountDisplay_post()

	{

		$formData=$this->Users_model->getUsers('StatusID',array('UserID'=>$this->SessionUserID));

		if($formData['StatusID']==2){

			$this->Entity_model->updateEntityInfo($this->SessionUserID, array("StatusID"=>8));

		}

		elseif($formData['StatusID']==8){

			$this->Entity_model->updateEntityInfo($this->SessionUserID, array("StatusID"=>2));

		}		

	}

  /*

	Name: 			search

	Description: 	Use to search users

	URL: 			/api/users/search

	*/

	public function search_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');

		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Friend,Follow,Followers,Blocked]');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|in_list[2,3]');

		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');

		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */



		$formData=$this->Users_model->getUsers('Rating,ProfilePic',

			array(

				'SessionUserID'		=>	$this->SessionUserID,

				'Keyword'			=>	@$this->Post['Keyword'],

				'Filter'			=>	@$this->Post['Filter'],

				'SpecialtyGUIDs'	=>	@$this->Post['SpecialtyGUIDs'],

				'UserTypeID'		=>	@$this->Post['UserTypeID'],

				'StatusID'			=>	2

				), TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($formData){

			$this->Return['Data'] = $formData['Data'];

		}	

	}

   /*

	Name: 			getProfile

	Description: 	Use to get user profile info.

	URL: 			/api/user/getProfile

	*/

	public function getProfile_post()

	{

		/* Validation section */

		//print_r($this->input->post('UserGUID')); die;

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->set_rules('Params', 'Params', 'trim');

		$this->form_validation->set_rules('Type', 'Type', 'trim');

		

		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */

		/*check for self profile or other user profile by GUID*/

		//$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		if(!empty($this->UserID)){
			$UserTypeData = $this->Users_model->getusertypes($this->UserID);
			$UserTypeID = $UserTypeData['Data']['Records'][0]['UserTypeID'];
		}else{
			$UserTypeID = "";
		}
		
		//print_r($UserTypeData); die;
		
		//echo $UserTypeID;
		//print_r(array('UserID'=>$this->UserID));
        /*Basic fields to select*/
        if($UserTypeID==10){
        		$formData = $this->Users_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array('UserID'=>$this->UserID));
        
        		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        		$UserTypeData = $this->Users_model->getusertype($this->EntityID);
        
        		$this->Return['Data'] = $UserTypeData['Data'];
        		$this->Return['Data'] = $formData;
        		
        }else if($this->input->post('Type') == 'MarketPlace'){
        	    $formData = $this->Users_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array('UserID'=>$this->EntityID));
        
        		$this->Return['Data'] = $formData;
        }else{
        	$UserData = $this->Staff_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array('UserID'=>$this->UserID));
        	//print_r($UserData);
			if($UserData){

				$this->Return['Data'] = $UserData;

			}
        }

	}

  /*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function updateUserInfo_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email|callback_validateEmail['.$this->Post['SessionKey'].']');
		$this->form_validation->set_rules('Username', 'Username', 'trim');
		$this->form_validation->set_rules('FirstName', 'First Name', 'trim');
		$this->form_validation->set_rules('Gender', 'Gender', 'trim|in_list[Male,Female,Other]');
		$this->form_validation->set_rules('BirthDate', 'Birth Date', 'trim|callback_validateDate');
		$this->form_validation->set_rules('PhoneNumber', 'Phone Number', 'trim|callback_validatePhoneNumber['.$this->Post['SessionKey'].']|min_length[10]|max_length[10]');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

       $this->Users_model->updateUserInfo($this->SessionUserID, $this->Post);



		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,UserTypeID,UserTypeName,Username,MasterFranchisee',array("UserID" => $this->SessionUserID));

		$this->Return['Message']      	=	"Profile successfully updated."; 	

	}

 /*

	Name: 			changePassword

	Description: 	Use to change account login password by user.

	URL: 			/api/users/changePassword

	*/

	public function changePassword_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('CurrentPassword', 'Current Password', 'trim|callback_validatePassword');
		
		$this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[6]');
		
		$this->form_validation->set_rules('ConfirmPassword', 'Confirm Password', 'trim|required|matches[Password]',array("matches"=>"Password do not match"));

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

        if($this->Users_model->updateUserLoginInfo($this->SessionUserID, array("Password"=>$this->Post['Password']), DEFAULT_SOURCE_ID)){

			$this->Return['Message'] =	"New password has been set."; 	

		}

	}
	/*

	Name: 			referEarn

	Description: 	Use to refer & earn user

	URL: 			/api/users/referEarn

	*/

	public function referEarn_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('ReferType', 'Refer Type', 'trim|required|in_list[Phone,Email]');

		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'min_length[10]|max_length[10]|trim'.(!empty($this->Post['ReferType']) && $this->Post['ReferType']=='Phone' ? '|required|callback_validateAlreadyRegistered[Phone]' : ''));

		$this->form_validation->set_rules('Email', 'Email', 'trim'.(!empty($this->Post['ReferType']) && $this->Post['ReferType']=='Email' ? '|required|valid_email|callback_validateAlreadyRegistered[Email]' : ''));

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */



		$this->Users_model->referEarn($this->Post,$this->SessionUserID);

		$this->Return['Message'] =	"User successfully invited."; 	

	}



	/*-----Validation Functions-----*/

	/*------------------------------*/	

	function validatePassword($Password)

	{	

		if(empty($Password)){

			$this->form_validation->set_message('validatePassword', '{field} is required.');

			return FALSE;

		}

		$formData=$this->Users_model->getUsers('',array('UserID'=>$this->SessionUserID, 'Password'=>$Password));

		if (!$formData){

			$this->form_validation->set_message('validatePassword', 'Invalid {field}.');

			return FALSE;

		}

		else{

			return TRUE;

		}

	}



	/**

     * Function Name: validateAlreadyRegistered

     * Description:   To validate already registered number or email

     */

	function validateAlreadyRegistered($Value,$FieldValue)

	{	

		$Query = ($FieldValue == 'Email') ? 'SELECT * FROM `tbl_users` WHERE `Email` = "'.$Value.'" OR `EmailForChange` = "'.$Value.'" LIMIT 1' : 'SELECT * FROM `tbl_users` WHERE `PhoneNumber` = "'.$Value.'" OR `PhoneNumberForChange` = "'.$Value.'" LIMIT 1';

		if ($this->db->query($Query)->num_rows() > 0){

			$this->form_validation->set_message('validateAlreadyRegistered', ($FieldValue == 'Email') ? 'Email is already registered' : 'Phone Number is already registered');

			return FALSE;

		}

		else{

			return TRUE;

		}

	}



	
	






}

