<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();		
		$this->load->model("Entity_model");
		$this->load->model('Recovery_model');
		$this->load->model('Users_model');
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
		$this->load->model('Students_model');
		$this->load->library("excel");
		$this->load->model("Media_model");
	}
	

	/*
	Description: 	Use to get Get Attributes.
	URL: 			/api/student/importStudents
	Input (Sample JSON): 		
	*/
	public function importStudents_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('CategoryGUID', 'Course', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		//$this->form_validation->set_rules('BatchID', 'BatchID', 'trim|required');
		$this->form_validation->set_rules('BatchGUID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		//$this->form_validation->set_rules('FeeGUID', 'FeeGUID','trim|required|callback_validateEntityGUID[Course fee,FeeID]');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		//echo $this->input->post('MediaURL'); die;
		$MediaURL = explode("/", $this->input->post('MediaURL'));
		$mediaName = end($MediaURL);

		//echo "uploads/students/".$mediaName; die; 



		$studentData = $this->excel->excel_to_array("uploads/students/".$mediaName);
		$arr_keys = array_keys($studentData[0]);

		//print_r($studentData); echo "nfdslknflksd"; die;

		// $student_format = $this->excel->excel_to_array("uploads/students/student_format.xlsx");
		// print_r($student_format); die();
		// $target_keys = array_keys($student_format[0]);

		$target_keys = array("FIRST NAME","LAST NAME","ADDRESS","CITY","PIN","MOBILE","EMAIL","LINKEDIN","FACEBOOK","REMARKS");
		
		foreach ($target_keys as $key => $value) {			
			if(!in_array($value, $arr_keys)){ //print_r($arr_keys);
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	'Sorry! This file format is not correct.<a style="text-decoration: underline; padding: 12px;" href="/asset/student/tbl_users.xlsx" download="">Download excel to get format to add students</a>.';
			}
		}

		$studentsId = array();
		$InsertStudent = array();
		$UserIDs = array();
		//echo count($studentData); die;

		for ($i=0; $i < count($studentData); $i++) { 
			
			$InsertData = array();
			$InsertStudent = array();
			$InsertData = array(
				"UserTypeID"	=> 7,	
				"FirstName" 	=>	@$studentData[$i]['FIRST NAME'],		
				"LastName" 		=>	@$studentData[$i]['LAST NAME'],
				"Address" 	=>	@$studentData[$i]['ADDRESS'],
				"StateName" 	=> 	@$studentData[$i]['STATE'],
				"CityName" 	=>	@$studentData[$i]['CITY'],
				"Postal" 	=>	@$studentData[$i]['PIN'],
				"PhoneNumber" => @$studentData[$i]['MOBILE'],
				"Email"	=>	@$studentData[$i]['EMAIL'],
				"LinkedInURL"	=>	@$studentData[$i]['LINKEDIN'],
				"FacebookURL"	=>	@$studentData[$i]['FACEBOOK'],
				"GuardianName" 	=>	@$studentData[$i]['GUARDIAN NAME'],
				"GuardianAddress"  =>	@$studentData[$i]['GUARDIAN ADDRESS'],
				"GuardianPhone"  =>	@$studentData[$i]['GUARDIAN PHONE'],
				"GuardianEmail"  =>	@$studentData[$i]['GUARDIAN EMAIL'],
				"FathersName"  =>	@$studentData[$i]["FATHER'S NAME"],
				"MothersName"  =>	@$studentData[$i]["MOTHER'S NAME"],
				"PermanentAddress"  =>	@$studentData[$i]['PERMANENT ADDRESS'],
				"FathersPhone"  =>	@$studentData[$i]["FATHER'S PHONE"],
				"FathersEmail"  =>	@$studentData[$i]["FATHER'S EMAIL"],
				"Remarks" =>	@$studentData[$i]['REMARKS'],
				"Password" => "iconik@123",
				"CategoryID" => $this->CategoryID,
				"BatchID" => $this->BatchID,
				"StatusID" => 1
			);

			//print_r($InsertData); die;

			//$UserID = $this->Users_model->importStudents((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $InsertData , 12, 1, 1);

			$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

			//print_r($InsertData);

			$User = $this->Students_model->addUser($EntityID,$InsertData, 7, 1, 1);

			//print_r($User); die;

			if(!empty($User) && !empty($User['UserID'])){		

				$this->Students_model->addStudentOnly($User['UserID'],$User['UserGUID'],$InsertData);
				array_push($UserIDs, $User['UserID']);
			}			
		}

		//print_r($UserIDs); die;
		$this->Return['Message']      	=	count($UserIDs)." Students are added successfully."; 

		// if(count($UserIDs) != count($studentData)){
		// 	$this->Return['ResponseCode'] 	=	500;
		// 	$this->Return['Message']      	=	"Students email and phone number are already exist."; 
		// }
	}


	/*
	Description: 	Use to get Get Attributes.
	URL: 			/api/student/importStudents
	Input (Sample JSON): 		
	*/
	public function addStudent_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('CategoryGUID', 'Course', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('BatchGUID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('Password', 'Password', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : ''));

		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');

		$this->form_validation->set_rules('LastName', 'Last Name', 'trim|required');

		$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'trim|required|callback_validatePhoneNumber|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required');

		$this->form_validation->set_rules('Address', 'Personal Address', 'trim|required');

		$this->form_validation->set_rules('StateID', 'State Name', 'trim|required');

		$this->form_validation->set_rules('CityName', 'City Name', 'trim|required');

		$this->form_validation->set_rules('Postal', 'Zip Code', 'trim');

		


		$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL', 'trim');

		$this->form_validation->set_rules('FacebookURL', 'FacebookURL', 'trim');

		$this->form_validation->set_rules('GuardianName', 'Guardian Name', 'trim');

		$this->form_validation->set_rules('GuardianAddress', 'Guardian Address', 'trim');
		
		$this->form_validation->set_rules('GuardianEmail', 'Guardian Email', 'trim|valid_email');

		$this->form_validation->set_rules('FathersName', 'Fathers Name', 'trim');

		$this->form_validation->set_rules('MothersName', 'Mothers Name', 'trim');

		$this->form_validation->set_rules('FathersPhone', 'Fathers/Mothers Mobile', 'trim|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('FathersEmail', 'Fathers/Mothers Email', 'trim|valid_email');

		$this->form_validation->set_rules('PermanentAddress', 'Permanent Address', 'trim');

		if(!empty($this->input->post('GuardianPhone'))){
			$this->form_validation->set_rules('GuardianPhone', 'Guardian Mobile', 'trim|min_length[10]|max_length[10]');
		}
		

		

		

		if($this->input->post('FeeGUID') == "Customized"){
			$this->form_validation->set_rules('TotalFeeAmount', 'Total Fee Amount', 'trim|required');
			$this->form_validation->set_rules('TotalFeeInstallments', 'No. of installment', 'trim');
			$this->form_validation->set_rules('InstallmentAmount', 'Installment amount', 'trim');
		}else if(strlen($this->input->post('FeeGUID')) ==  36){
			$this->form_validation->set_rules('FeeGUID', 'Plan','trim|required|callback_validateEntityGUID[Course fee,FeeID]');
		}else{
			$this->form_validation->set_rules('FeeGUID', 'Plan', 'trim|required');
		}
		
		
		//$this->form_validation->set_rules('BatchID', 'BatchID', 'trim|required');

		

		$this->form_validation->set_rules('FeeStatusID', 'Fee Status', 'trim|required');

		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim');

		
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[7]');

		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');

		

		$this->form_validation->set_rules('SourceGUID', 'SourceGUID', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] != 'Direct' ? '|required' : '') . '|callback_validateSourceGUID[' . @$this->Post['Source'] . ']');


		$this->form_validation->validation($this); /* Run validation */

		$duedates = $this->Post['duedates'];
		if(isset($duedates) && !empty($duedates))
		{
			foreach($duedates as $d)
			{
				if(!isset($d) || empty($d))
				{
					$this->Return['ResponseCode'] = 500;

					$this->Return['Message'] = "Due date is required.";

					die;
				}
			}
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Due date is required.";

			die;
		}


		/* Validation - ends */

		$UserID = $this->Students_model->addStudent((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),array_merge($this->Post, array("Referral" => @$this->Referral)) , $this->Post['UserTypeID'], $this->SourceID, ($this->Post['Source'] != 'Direct' ? '2' : '1'), $this->CategoryID, @$this->FeeID, $this->BatchID/*if source is not Direct Account treated as Verified*/);

		if($UserID == "PhoneNumber Exist"){

			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This mobile number is already exist.";
		}
		else if (!$UserID) {

			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";

		}

		else {

			



			/*for update phone number*/

			// if (!empty($this->Post['PhoneNumber']) && PHONE_NO_VERIFICATION) {

			// 	/* Genrate a Token for PhoneNumber verification and save to tokens table. */

			// 	$this->load->model('Recovery_model');

			// 	$Token = $this->Recovery_model->generateToken($UserID, 3);

			// 	/* Send change phonenumber SMS to User with Token. */

			// 	sendSMS(array(

			// 		'PhoneNumber' => $this->Post['PhoneNumber'],

			// 		'Text' => SITE_NAME . ", OTP for Verify Mobile No. is: $Token"

			// 	));

			// }



			/*referal code generate*/

			//$this->load->model('Utility_model');

			//$this->Utility_model->generateReferralCode($UserID);

			/* Send welcome notification */

			//$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);

			/* get user data */

			$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(

				'UserID' => $UserID

			));

			/* create session only if source is not Direct and account treated as Verified. */

			


			$this->Return['Data'] = $UserData;

			$this->Return['Message'] = "Added successfully.";

		}
	}



	public function editStudent_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('CategoryGUID', 'Course', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('BatchGUID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[Students,UserID]');

		$this->form_validation->set_rules('Password', 'Password', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : ''));

		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');

		$this->form_validation->set_rules('LastName', 'Last Name', 'trim');

		$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'trim|required|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_rules('Address', 'Address', 'trim|required');

		$this->form_validation->set_rules('StateID', 'State Name', 'required');

		$this->form_validation->set_rules('CityName', 'City Name', 'trim|required');

		$this->form_validation->set_rules('Postal', 'Zip Code', 'trim');

		//$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|required|callback_validatePhoneNumber['.$this->Post['SessionKey'].']|min_length[10]|max_length[10]');

		//$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|callback_validatePhoneNumber['.$this->Post['SessionKey'].']|min_length[10]|max_length[10]');

		
		//$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'min_length[10]|max_length[10]|trim'.(!empty($this->Post['ReferType']) && $this->Post['ReferType']=='Phone' ? '|required|callback_validateAlreadyRegistered[Phone]' : ''));

		


		$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL', 'trim');

		$this->form_validation->set_rules('FacebookURL', 'FacebookURL', 'trim');

		$this->form_validation->set_rules('GuardianName', 'Guardian Name', 'trim');

		$this->form_validation->set_rules('GuardianAddress', 'Guardian Address', 'trim');

		$this->form_validation->set_rules('GuardianPhone', 'Guardian Mobile', 'trim|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('GuardianEmail', 'Guardian Email', 'trim|valid_email');

		$this->form_validation->set_rules('FathersName', 'Fathers Name', 'trim');

		$this->form_validation->set_rules('MothersName', 'Mothers Name', 'trim');

		$this->form_validation->set_rules('PermanentAddress', 'Permanent Address', 'trim');

		$this->form_validation->set_rules('FathersPhone', 'Fathers/Mothers Mobile', 'trim|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('FathersEmail', 'Fathers/Mothers Email', 'trim|valid_email');

		

		if($this->input->post('FeeGUID') == "Customized"){
			$this->form_validation->set_rules('TotalFeeAmount', 'Total Fee Amount', 'trim|required');
			$this->form_validation->set_rules('TotalFeeInstallments', 'No. of installment', 'trim');
			$this->form_validation->set_rules('InstallmentAmount', 'Installment amount', 'trim');
		}else if(strlen($this->input->post('FeeGUID')) ==  36){
			$this->form_validation->set_rules('FeeGUID', 'Plan','trim|required|callback_validateEntityGUID[Course fee,FeeID]');
		}else{
			$this->form_validation->set_rules('FeeGUID', 'Plan', 'trim|required');
		}

		

		//$this->form_validation->set_rules('FeeID', 'FeeID', 'trim|required');

		$this->form_validation->set_rules('FeeStatusID', 'Fee Status', 'trim|required');

		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim');

		
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[7]');

		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');

		

		$this->form_validation->set_rules('SourceGUID', 'SourceGUID', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] != 'Direct' ? '|required' : '') . '|callback_validateSourceGUID[' . @$this->Post['Source'] . ']');


		$this->form_validation->validation($this); /* Run validation */

		$duedates = $this->Post['duedates'];
		if(isset($duedates) && !empty($duedates))
		{
			foreach($duedates as $d)
			{
				if(!isset($d) || empty($d))
				{
					$this->Return['ResponseCode'] = 500;

					$this->Return['Message'] = "Due date is required.";

					die;
				}
			}
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Due date is required.";

			die;
		}

		$UserID = $this->Students_model->editStudent((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->UserID,array_merge($this->Post, array("Referral" => @$this->Referral)) , $this->Post['UserTypeID'], $this->SourceID, ($this->Post['Source'] != 'Direct' ? '2' : '1'), $this->CategoryID, @$this->FeeID, @$this->BatchID/*if source is not Direct Account treated as Verified*/);
		if($UserID == "PhoneNumber Exist"){
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This mobile number is already exist.";
		}
		else if (!$UserID) {

			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";

		}

		else {

			$UserData = $this->Students_model->getStudentByID((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->UserID);

			$this->Return['Data'] = $UserData;

			$this->Return['Message'] = "Updated successfully.";
		}
	}


	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCourses
	Input (Sample JSON): 		
	*/
	public function getCourses_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryTypeName', 'CategoryTypeName', 'trim|callback_validateCategoryTypeName[Category,CategoryTypeID]');
		$this->form_validation->validation($this);  /* Run validation */		
		
		$Course = $this->Students_model->getCourses((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryTypeID);
		//print_r($Course); die;
		if(!empty($Course)){
			$this->Return['Data'] = $Course;
		}	
	}


	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCourses
	Input (Sample JSON): 		
	*/
	public function getFeePlans_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->validation($this);  /* Run validation */		
		
		$fee = $this->Students_model->getFeePlans((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID);
		if(!empty($fee)){
			$this->Return['Data'] = $fee;
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/student/getStudentStatistics
	Input (Sample JSON): 		
	*/
	public function getStudentStatistics_post()
	{
		

		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->validation($this);  /* Run validation */		
		
		if(!empty($this->input->post('CategoryGUID'))){
			$StudentData = $this->Students_model->getStudentStatistics((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID);
		}else if(!empty($this->input->post('BatchGUID'))){
			$StudentData = $this->Students_model->getStudentStatistics((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),"",$this->BatchID);
		}else{ 
			$StudentData = $this->Students_model->getStudentStatistics((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID));
		}
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}   

	/*
	Description: 	Use to get Get student list.
	URL: 			/api/student/getStudents
	Input (Sample JSON): 		
	*/
	public function getStudents_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);	

		$StudentData = $this->Students_model->getStudents($EntityID,array("BatchID"=>@$this->BatchID,"CategoryID"=>@$this->CategoryID,"Keyword"=>@$this->input->post('Keyword'),"StudentType"=>@$this->input->post('StudentType')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		//print_r($StudentData);
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}




	/*
	Description: 	Use to get Get MP student list.
	URL: 			/api/student/getMPStudents
	Input (Sample JSON): 		
	*/
	public function getMPStudents_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|integer');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);	

		$StudentData = $this->Users_model->getUsers("UserGUID,UserTypeID,FirstName,Email,PhoneNumber,Address,Postal,CountryCode,CityName,	StateName,StateID,PhoneNumberForChange,ProfilePic,EntryDate", $this->input->post(), TRUE,  
			@$this->Post['PageNo'], @$this->Post['PageSize']);
		//print_r($StudentData);
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData['Data'];
		}	
	}



	/*
	Description: 	Use to get student pending profile list.
	URL: 			/api/student/getPendingProfile
	Input (Sample JSON): 		
	*/
	public function getPendingProfile_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);	

		$StudentData = $this->Students_model->getPendingProfile($EntityID,array("BatchID"=>@$this->BatchID,"CategoryID"=>@$this->CategoryID,"Keyword"=>@$this->input->post('Keyword'),"Approved"=>@$this->input->post('Approved')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		//print_r($StudentData);
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}


	/*

	Description: 	use to get list of batch

	URL: 			/api/student/getBatchByCourse	

	*/

	public function getBatchByCourse_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */


		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);


		$batchData = $this->Common_model->getBatchByCourse($this->EntityID,$this->CategoryID);

		if($batchData){

			$this->Return['Data'] = $batchData;	

		}else{
			$this->Return['Message'] = "Data not found";
		}

	}


	/*
	Description: 	Use to get Get student by id.
	URL: 			/api/student/getStudentByID
	Input (Sample JSON): 		 
	*/
	public function getStudentByID_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[Students,UserID]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->validation($this);  /* Run validation */		
		
		$StudentData = $this->Students_model->getStudentByID((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),@$this->UserID,@$this->CategoryID,@$this->BatchID);
		
		if(!empty($StudentData) && count($StudentData) > 0){
			$this->Return['Data'] = $StudentData;
		}else if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData[0];
		}
	}


	public function getStatesList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);

		$StatesList = $this->Common_model->getStatesList();
		
		if(!empty($StatesList)){
			$this->Return['Data'] = $StatesList;
		}	
	}


	public function getCitiesList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('State_id', 'State_id', 'trim|required');
		$this->form_validation->validation($this);

		//print_r($this->input->post()); die;

		$CitiesList = $this->Common_model->getCitiesList($this->input->post('State_id'));
		
		if(!empty($CitiesList)){
			$this->Return['Data'] = $CitiesList;
		}	
	}


	public function getCitiesByStateName_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('StateName', 'StateName', 'trim|required');
		$this->form_validation->validation($this);

		//print_r($this->input->post()); die;

		$CitiesList = $this->Common_model->getCitiesListByStateName($this->input->post('StateName'));
		
		if(!empty($CitiesList)){
			$this->Return['Data'] = $CitiesList;
		}	
	}	

	public function delete_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		$this->form_validation->set_rules('KeyStatusID','KeyStatusID','trim|required');
		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		$StudentStatus = $this->Students_model->delete($EntityID, $this->UserID, $this->input->post('KeyStatusID'));
		
		if(!$StudentStatus){
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";
		}	
	}


	public function saveImage_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		$this->form_validation->set_rules('MediaGUIDs', 'Image', 'trim|required');
		$this->form_validation->set_rules('MediaName','MediaName','trim|required');
		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		$StudentStatus = $this->Students_model->saveImage($EntityID, $this->UserID, $this->input->post());
		
		if(!$StudentStatus){
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "An error occurred, please try again later.";
		}else{
			$this->Return['Message'] = "Picture uploaded successfully.";
		}	
	}	



	/*----------------------------------------------------------------------------------------
    Scholarship Test Section
    ----------------------------------------------------------------------------------------*/	
	public function getScholarshipTests_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		
		$this->form_validation->validation($this);

		$Data = $this->Students_model->getScholarshipTests($this->Post);
		
		$this->Return['Data'] = $Data;
	}


	public function getAppliedScholarshipTests_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		
		$this->form_validation->validation($this);

		$Data = $this->Students_model->getAppliedScholarshipTests($this->Post);
		
		$this->Return['Data'] = $Data;
	}



	public function getScholarshipPayments_post()
	{		
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ApplyDate', 'Apply Date', 'trim');
		$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim');
		$this->form_validation->set_rules('filterFromDate', 'From Date', 'trim');
		$this->form_validation->set_rules('filterToDate', 'To Date', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('filterKeyword', 'filter Keyword', 'trim');
		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		$Data = $this->Students_model->getScholarshipPayments($EntityID, $this->Post, TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($Data)){
			$this->Return['Data'] = $Data;
		}else{
			$this->Return['ResponseCode'] = 500;
		}
		
	}



	public function applyForScholarshipTest_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		$data = $this->Students_model->applyForScholarshipTest($EntityID, $this->QtPaperID, 0);
		
		if($data)
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "You already applied for test.";
		}	
	}

 
	public function purchaseForScholarshipTest_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('ApplyDate', 'Apply Date', 'trim|required');
		$this->form_validation->set_rules('PaidAmount', 'Paid Amount', 'trim|required');
		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
 
		$data = $this->Students_model->applyForScholarshipTest($EntityID, $this->QtPaperID, 1);
		
		if($data)
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "You already applied for test.";
		}	
	}


	public function savePaymentForScholarshipTest_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('ApplyID', 'Application Number', 'trim|required');
		$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim');
		$this->form_validation->set_rules('TrackingID', 'Tracking ID', 'trim|required');
		$this->form_validation->set_rules('PaymentType', 'Payment Type', 'trim');
		$this->form_validation->set_rules('PaidAmount', 'Paid Amount', 'trim');  
		$this->form_validation->set_rules('OrderStatus', 'Order Status', 'trim');  
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim');
		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
 
		$data = $this->Students_model->savePaymentForScholarshipTest($EntityID, $this->QtPaperID, $this->input->post());
		
		if($data)
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Oops ! Something went wrong. Please try again.";
		}	
	}



	public function getQuestionAnswerList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');	
		$this->form_validation->validation($this);		
		

		$fields = array('QtPaperGUID','CourseID','SubjectID','QtPaperTitle','TotalQuestions','QuestionsGroup','QuestionsLevelEasy','QuestionsLevelModerate','QuestionsLevelHigh','PassingMarks','NegativeMarks','QuestionsID','EasyLevelQuestionsID','ModerateLevelQuestionsID','HighLevelQuestionsID','StartDateTime','EndDateTime');

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		$Response = $this->Students_model->getQuestionAnswerList($EntityID, $this->QtPaperID, $fields);
		
		if($Response == "not_apply")
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "You do not applied for scholarship test.";
			$this->Return['Data'] = array();
		}
		elseif($Response == "attempt")
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "You already attempt scholarship test.";
			$this->Return['Data'] = array();
		}
		elseif($Response == "time_check")
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Please check scheduled start and end time of scholarship test.";
			$this->Return['Data'] = array();
		}
		elseif($Response)
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $Response['Data'];
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "You already applied for test.";
			$this->Return['Data'] = array();
		}
	}


	public function approveProfileUpdate_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PendingProfileID', 'Pending Profile ID', 'trim|required');
		$this->form_validation->set_rules('StudentID', 'Student ID', 'trim|required');
		$this->form_validation->set_rules('CourseID', 'Course ID', 'trim|required');
		$this->form_validation->set_rules('BatchID', 'Batch ID', 'trim|required');
		$this->form_validation->validation($this);		
		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
		$Response = $this->Students_model->approveProfileUpdate($EntityID, $this->input->post());
		
		if($Response)
		{
			$this->Return['Message'] = "Profile updated successfully.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! some error occurred, please try later.";
		}
	}


	public function revokeLoan_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('LoanID', 'Loan ID', 'trim|required');
		$this->form_validation->validation($this);		
		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
		$Response = $this->Students_model->revokeLoan($EntityID, $this->input->post('LoanID'));
		
		if($Response)
		{
			$this->Return['Message'] = "Your loan application revoked successfully.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! some error occurred, please try later.";
		}
	}


	public function studentByIDForDrop_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'Student ID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->validation($this);  /* Run validation */		
		
		$StudentData = $this->Students_model->studentByIDForDrop((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),@$this->UserID,@$this->CategoryID,@$this->BatchID);
		
		if(!empty($StudentData) && count($StudentData) > 0){
			$this->Return['Data'] = $StudentData;
		}else if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData[0];
		}
	}


	public function dropStudent_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('StudentUserGUID', 'Student ID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		$this->form_validation->set_rules('do_date', 'Date Of Dropping', 'trim|required');
		$this->form_validation->set_rules('do_reason', 'Reason', 'trim|required');
		$this->form_validation->set_rules('do_amount_refund', 'Amount Refund', 'trim|required');
		$this->form_validation->set_rules('do_payment_type', 'Payment Type', 'trim|required');
		
		if($this->Post['do_payment_type'] == "cheque")
		{
			$this->form_validation->set_rules('do_cheque_number', 'Cheque Number', 'trim|required');
			$this->form_validation->set_rules('do_cheque_date', 'Cheque Date', 'trim|required');
			$this->form_validation->set_rules('do_cheque_bank', 'Bank Name', 'trim|required');
		}
		elseif($this->Post['do_payment_type'] == "online")
		{
			$this->form_validation->set_rules('do_online_transactionid', 'Transaction ID', 'trim|required');
			$this->form_validation->set_rules('do_online_details', 'Other Details', 'trim|required');			
		}

		$this->form_validation->validation($this);

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
 
		$data = $this->Students_model->dropStudent($EntityID, $this->UserID, $this->input->post());
		
		if($data === "NotExist")
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Student details not found in the system.";
			$this->Return['Data'] = array();
		}
		else
		{	
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "Saved Successfully.";
			$this->Return['Data'] = $data;
		}	
	}


	public function getUnAssignBatchStudent_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		
		$this->form_validation->validation($this);		

		$RecordsData = $this->Students_model->getUnAssignBatchStudent((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID));

		if(!empty($RecordsData))
		{
			$this->Return['Data'] = $RecordsData;
		}	
	}

	public function BulkAssignBatchSave_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('BASCourseID', 'Course', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		
		$this->form_validation->set_rules('BASBatchID', 'Batch', 'trim|required|callback_validateEntityGUID[Batch,BatchID]');

		$this->form_validation->set_rules('BASChkBox[]', 'Student', 'trim|required');		

		$this->form_validation->validation($this);

		if(!isset($this->Post['BASChkBox']) || empty($this->Post['BASChkBox']))
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Please select atleast one student.";
			$this->Return['Data'] = array();
			die;
		}

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);		

		$data = $this->Students_model->BulkAssignBatchSave($EntityID, $this->CategoryID, $this->BatchID, $this->input->post());
		
		if($data)
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "Saved Successfully.";
			$this->Return['Data'] = $data;
		}
		else
		{	
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Oops! Something went wrong. Please try again.";
			$this->Return['Data'] = array();
		}	
	}


	public function getAppliedScholarshipTestsAPI_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('STKey', 'Scholarship Test Key', 'trim|required');
		
		$this->form_validation->validation($this);

		$Data = $this->Students_model->getAppliedScholarshipTestsAPI($this->Post);
		
		$this->Return['Data'] = $Data;
	}

}