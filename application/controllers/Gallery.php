<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gallery extends Admin_Controller_Secure {
	
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			// 'asset/plugins/BasicJcarousel/jcarousel.basic.css',
			// 'asset/plugins/BasicJcarousel/style.css'
		);
		
		$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',
			//'asset/plugins/BasicJcarousel/jquery.js',
			'asset/plugins/BasicJcarousel/jquery.jcarousel.min.js',
			'asset/plugins/BasicJcarousel/jcarousel.basic.js',			
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',		
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js'
		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"Picture Gallery" => ""
		);

		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('gallery/gallery_list');
		$this->load->view('includes/footer');
	}



}
