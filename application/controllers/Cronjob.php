<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller 
{
	public function api_call($cronName)
	{
        $url = API_URL.'admin/cronjob/'.$cronName;                     

        $ch = curl_init($url);        

        $data = ['param1'=>'', 'param2'=>''];        

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);        

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            'App-Key: 123456',
            'App-Secret: 123'
        ));       

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                   

        $result = curl_exec($ch);
           
        //echo "<pre>"; print_r($result);        

        curl_close($ch);

		if($Response['ResponseCode'] == 200)
		{
			echo $Response['Message'];
		}

		exit;
	}


	public function trigger_sms()
	{		
		$this->api_call("triggerSMS");		
	}


	public function trigger_email()
	{		
		$this->api_call("triggerEmail");		
	}
	

	public function part_time_jobs()
	{		
		$this->api_call("partTimeJobs");		
	}


	public function before_due_date()
	{		
		$this->api_call("beforeDueDate");				
	}


	public function after_due_date()
	{		
		$this->api_call("afterDueDate");		
	}


	public function scholarship_test()
	{		
		$this->api_call("scholarshipTestEdited");		
	}
	

}