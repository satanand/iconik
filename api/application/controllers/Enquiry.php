<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('Enquiry_model');
	}


	//Master Entries-----------------------------------------------
	public function getAssignedToUsersList_post()
	{
		
		//get Assigned Users---------------------------	
		$users =  $this->Enquiry_model->getAssignedUsers();
		
		$this->Return['Users'] = $users; 
	}

	public function getInterestedInList_post()
	{
		//get Interested In---------------------------
		$interestedIn =  $this->Enquiry_model->getInterestedIn();
		$this->Return['InterestedIn'] = $interestedIn;
	}



	/*
	Description: 	Use to get all enquiry.
	URL: 			/api/enquiry/getEnquiryList
	Input (Sample JSON): 		
	*/
	public function getEnquiryList_post()
	{
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');				
		$this->form_validation->validation($this);  /* Run validation */		

		$EnquiryData = $this->Enquiry_model->getEnquiryList($this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);		
		
		/*//get Assigned Users---------------------------	
		$users =  $this->Enquiry_model->getAssignedUsers(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
		$this->Return['Users'] = $users;

		//get Interested In---------------------------
		$interestedIn =  $this->Enquiry_model->getInterestedIn();
		$this->Return['InterestedIn'] = $interestedIn;*/

		if(!empty($EnquiryData))
		{
			$this->Return['Data'] = $EnquiryData['Data'];
		}	
	}



	public function addEnquiry_post()
	{		
		if(!preg_match('/^[a-zA-Z\s]+$/', $_POST['FullName']) && $_POST['FullName'] != "")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Only aplhabets are allowed in Full Name.";

			die;
		}
		elseif(!preg_match('/^[a-zA-Z0-9\s]+$/', $_POST['InterestedIn']) && $_POST['InterestedIn'] != "")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Interested In can be only aplhanumeric.";

			die;
		}


		$this->form_validation->set_rules('FullName', 'Full Name', 'trim|required');

		$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'trim|required|min_length[10]|max_length[10]');

		//$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_rules('InterestedIn', 'InterestedIn', 'trim|required');

		//$this->form_validation->set_rules('Remarks', 'Remarks', 'trim|required');

		$this->form_validation->set_rules('Source', 'Source', 'trim|required');
		
		$this->form_validation->validation($this); /* Run validation */		

				
		if(!preg_match('/[a-zA-Z\s]/', $this->Post['FullName'], $matches))
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Aplhabets are allowed in Full Name.";
		}


		$EnquiryID = $this->Enquiry_model->addEnquiry(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID, $this->Post);

		if($EnquiryID === "Enquiry Assigned")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Can not update enquiry. It is assigned to user.";
		}
		else
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Enquiry saved successfully.";
		}		
	}


	/*
	Description: 	Use to get enquiry by id.
	URL: 			/api/enquiry/getEnquiry
	Input (Sample JSON): 		
	*/
	public function getEnquiry_post()
	{
		$Data = $this->Enquiry_model->getEnquiry((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data[0];
		}
	}



	/*
	Description: 	Use to assign enquiry to user.
	URL: 			/api/enquiry/assignEnquiry
	Input (Sample JSON): 		
	*/
	public function assignEnquiry_post()
	{
		$Data = $this->Enquiry_model->assignEnquiry((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);		
		

		if(!empty($Data))
		{
			if($Data[0]['EnquiryAssignToUser'] > 0)
			{
				$this->Return['ResponseCode'] = 500;

				$this->Return['Message'] = "Enquiry is already assigned.";

				$this->Return['Data'] = array();
			}
			else
			{			
				$users = $this-> getInstituteUsers((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID));

				if(isset($users) && !empty($users))
				{
					$this->Return['ResponseCode'] = 200;

					$this->Return['Message'] = "";

					$this->Return['Data'] = $Data[0];

					$this->Return['Data']['Users'] = $users;
				}
				else
				{
					$this->Return['ResponseCode'] = 500;

					$this->Return['Message'] = "Please first add staff in your institute and assign permission.";

					$this->Return['Data'] = $Data[0];

					$this->Return['Data']['Users'] = array();
				}	
			}			
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Enquiry details not found in the system.";

			$this->Return['Data'] = array();
		}
	}


	function getInstituteUsers($EntityID)
	{
		return $this->Enquiry_model->getInstituteUsers($EntityID);
	}


	/*
	Description: 	Use to add Assign Enquiry.
	URL: 			/api/enquiry/addAssignEnquiry
	Input (Sample JSON): 		
	*/
	public function addAssignEnquiry_post()
	{		
		$this->form_validation->set_rules('AssignedTo', 'Assigned To', 'trim|required');		

		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim|required');
		
		$this->form_validation->validation($this); /* Run validation */		

		$EnquiryID = $this->Enquiry_model->addAssignEnquiry(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID, $this->Post);

		if($EnquiryID == "Enquiry Assigned")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Can not update enquiry. It is assigned to user.";
		}
		elseif($EnquiryID > 0)
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Enquiry assigned successfully.";
		}		
	}



	/*
	Description: 	Use to delete enquiry by id.
	URL: 			/api/enquiry/delete
	Input (Sample JSON): 		
	*/
	public function delete_post()
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$count = $this->Enquiry_model->deleteEnquiry($this->EntityID, $this->Post['EnquiryGUID']);
		
		if($count <= 0)
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Error occur while deleting enquiry or you do not have permission to delete.";
		}
		else
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Enquiry deleted successfully.";
		}
	}



	public function getAssignedEnquiryCount_post()
	{
		//$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		//$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		//$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');		
		//$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);	

		$EnquiryData = $this->Enquiry_model->getAssignedEnquiryCount($this->EntityID, $this->Post);

		$users =  $this->Enquiry_model->getAssignedUsers($this->EntityID);

		$this->Return['Data']['Content'] = $EnquiryData;			

		$this->Return['Data']['Users'] = $users;
	}


	/*
	Description: 	Use to get enquiry by id.
	URL: 			/api/enquiry/getEnquiry
	Input (Sample JSON): 		
	*/
	public function loadEnquiryByStatus_post()
	{
		$Data = $this->Enquiry_model->loadEnquiryByStatus((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['SourceID'], $this->Post['StatusID']);
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}
	}



	/*
	Description: 	Use to get all enquiry assigned to user.
	URL: 			/api/enquiry/getEnquiryAssignedToList
	Input (Sample JSON): 		
	*/
	public function getEnquiryAssignedToList_post()
	{
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');		
		$this->form_validation->validation($this);  /* Run validation */

		$EnquiryData = $this->Enquiry_model->getEnquiryAssignedToList(@$this->Post['PageNo'], @$this->Post['PageSize'], $this->Post);
		
		if(!empty($EnquiryData))
		{
			$this->Return['Data'] = $EnquiryData['Data'];
		}	
	}


	/*
	Description: 	Use to get Enquiry Of AssignedUser
	URL: 			/api/enquiry/getEnquiryOfAssignedUser
	Input (Sample JSON): 		
	*/
	public function getEnquiryOfAssignedUser_post()
	{
		$Data = $this->Enquiry_model->getEnquiryOfAssignedUser((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);

		$CallData = $this->Enquiry_model->getCallLogHistoryOfUser((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);

		$this->Return['CallData']['Data']['Records'] = array();;
		if(isset($CallData) && !empty($CallData))
		{
			$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
			$this->Return['LoginUser'] = $this->EntityID;

			$this->Return['CallData'] = $CallData;
		}

		if(!empty($Data))
		{
			$this->Return['Data']['EnquiryData'] = $Data[0];
		}
	}



	/*
	Description: 	Use to add Call Data
	URL: 			/api/enquiry/addCallData
	Input (Sample JSON): 		
	*/
	public function addCallData_post()
	{
		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim|required');

		$this->form_validation->set_rules('Status', 'Status', 'trim|required');

		$this->form_validation->set_rules('FollowUpDate', 'Follow Up Date', 'trim|required');
		
		$this->form_validation->validation($this); /* Run validation */		


		$Data = $this->Enquiry_model->getEnquiryByID((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);
		
		if(!isset($Data) || empty($Data))
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Enquiry details not found in the system.";
		}
		else
		{			
			$EnquiryID = $Data[0]['EnquiryID'];

			$EnquiryID = $this->Enquiry_model->addCallData(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID, $this->Post, $EnquiryID);

			if($EnquiryID <= 0)
			{
				$this->Return['ResponseCode'] = 500;

				$this->Return['Message'] = "Error error while saving record. Please check form entry and try again.";
			}
			elseif($EnquiryID > 0)
			{
				$this->Return['ResponseCode'] = 200;

				$this->Return['Message'] = "Call saved successfully.";
			}
		}			
	}



	/*
	Description: 	Use to get Call Log History Of User
	URL: 			/api/enquiry/getCallLogHistoryOfUser
	Input (Sample JSON): 		
	*/
	public function getCallLogHistoryOfUser_post()
	{
		$Data = $this->Enquiry_model->getCallLogHistoryOfUser((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);
		
		if(!empty($Data))
		{
			$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
			$this->Return['LoginUser'] = $this->EntityID;

			$this->Return['Data'] = $Data['Data'];
		}
	}


	/*
	Description: 	Use to get all Churned Enquiry List.
	URL: 			/api/enquiry/getChurnedEnquiryList
	Input (Sample JSON): 		
	*/
	public function getChurnedEnquiryList_post()
	{
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');		
		$this->form_validation->validation($this);  /* Run validation */		
		

		$EnquiryData = $this->Enquiry_model->getChurnedEnquiryList(@$this->Post['PageNo'], @$this->Post['PageSize'], $this->Post);

		//$users =  $this->Enquiry_model->getAssignedUsers($this->EntityID);

		/*$this->Return['Data']['Content']['Data'] = array();
		$this->Return['Data']['Users']= array();

		if(isset($EnquiryData) && !empty($EnquiryData))
		{
			$this->Return['Data']['Content'] = $EnquiryData;			
		}
		
		if(isset($users) && !empty($users))	
		{
			$this->Return['Data']['Users'] = $users;
		}*/	
		
		//echo "<pre>"; print_r($EnquiryData);
		if(!empty($EnquiryData))
		{
			$this->Return['Data'] = $EnquiryData['Data'];
		}

	}



	/*
	Description: 	Use to re assign enquiry to user.
	URL: 			/api/enquiry/reassignEnquiry
	Input (Sample JSON): 		
	*/
	public function ReassignEnquiry_post()
	{
		$Data = $this->Enquiry_model->assignEnquiry((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);		

		if(!empty($Data))
		{			
			$users = $this->Enquiry_model->getInstituteUsers((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID), $this->Post['EnquiryGUID']);

			if(isset($users) && !empty($users))
			{
				$this->Return['ResponseCode'] = 200;

				$this->Return['Message'] = "";

				$this->Return['Data'] = $Data[0];

				$this->Return['Data']['Users'] = $users;
			}
			else
			{
				$this->Return['ResponseCode'] = 500;

				$this->Return['Message'] = "Please first add staff in your institute and assign permission.";

				$this->Return['Data'] = $Data[0];

				$this->Return['Data']['Users'] = array();
			}	
						
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Enquiry details not found in the system.";

			$this->Return['Data'] = array();
		}
	}


	/*
	Description: 	Use to add Re Assign Enquiry.
	URL: 			/api/enquiry/addReAssignEnquiry
	Input (Sample JSON): 		
	*/
	public function addReAssignEnquiry_post()
	{		
		$this->form_validation->set_rules('AssignedTo', 'Assigned To', 'trim|required');		

		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim|required');
		
		$this->form_validation->validation($this); /* Run validation */		

		$EnquiryID = $this->Enquiry_model->addAssignEnquiry(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID, $this->Post, 1);

		if($EnquiryID > 0)
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Enquiry re-assigned successfully.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Unknown error occur while processing. Please check form entry and try again.";
		}		
	}



	/*
	Description: 	Use to bulk assign enquiry to user.
	URL: 			/api/enquiry/bulkAssignEnquiry
	Input (Sample JSON): 		
	*/
	public function bulkAssignEnquiry_post()
	{								
		if(!isset($this->Post['EnquiryChk']) || empty($this->Post['EnquiryChk']))
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Please select atleast one record to perform action.";

			$this->Return['Data'] = array();

			$this->Return['Data']['Users'] = array();
		}
		else
		{
			$EnquiryChk = $this->Post['EnquiryChk'];		

			$users = $this-> getInstituteUsers((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID));

			if(isset($users) && !empty($users))
			{
				$this->Return['ResponseCode'] = 200;

				$this->Return['Message'] = "";				

				$this->Return['Data']['EnquirySelectedCount'] = count($EnquiryChk);

				$this->Return['Data']['EnquiryGUIDS'] = implode(",", $EnquiryChk);

				$this->Return['Data']['Users'] = $users;
			}
			else
			{
				$this->Return['ResponseCode'] = 500;

				$this->Return['Message'] = "Please first add staff in your institute and assign permission.";

				$this->Return['Data'] = array();

				$this->Return['Data']['Users'] = array();
			}
		}		
	}


	/*
	Description: 	Use to save Bulk Assign Enquiry.
	URL: 			/api/enquiry/addBulkAssignEnquiry
	Input (Sample JSON): 		
	*/
	public function addBulkAssignEnquiry_post()
	{		
		$this->form_validation->set_rules('EnquiryGUID', 'EnquiryGUID', 'trim|required');		

		$this->form_validation->set_rules('AssignedTo', 'Assigned To', 'trim|required');		

		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim|required');
		
		$this->form_validation->validation($this); /* Run validation */		

		$EnquiryID = $this->Enquiry_model->addBulkAssignEnquiry(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID, $this->Post);

		if(count($EnquiryID) > 0)
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Enquiry assigned successfully.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Invalid request. Please check inputs and try again.";
		}		
	}


	/*
	Description: 	Use to save Bulk Un Assign Enquiry.
	URL: 			/api/enquiry/addBulkUnAssignEnquiry
	Input (Sample JSON): 		
	*/
	public function addBulkUnAssignEnquiry_post()
	{		
		$this->form_validation->set_rules('EnquiryChk[]', 'Enquiry', 'trim|required');		

		$this->form_validation->set_rules('AssignedTo', 'Assigned To', 'trim|required');
		
		$this->form_validation->validation($this); /* Run validation */		

		$EnquiryID = $this->Enquiry_model->addBulkUnAssignEnquiry(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID, $this->Post);

		if(count($EnquiryID) > 0)
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Enquiry Unassigned successfully.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Invalid request. Please check inputs and try again.";
		}		
	}

}