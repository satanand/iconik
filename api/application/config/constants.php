<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/*

|--------------------------------------------------------------------------

| Display Debug backtrace

|--------------------------------------------------------------------------

|

| If set to TRUE, a backtrace will be displayed along with php errors. If

| error_reporting is disabled, the backtrace will not display, regardless

| of this setting

|

*/

defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);



/*

|--------------------------------------------------------------------------

| File and Directory Modes

|--------------------------------------------------------------------------

|

| These prefs are used when checking and setting modes when working

| with the file system.  The defaults are fine on servers with proper

| security, but you may wish (or even need) to change the values in

| certain environments (Apache running a separate process for each

| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should

| always be used to set the mode correctly.

|

*/

defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);

defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);

defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);

defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);



/*

|--------------------------------------------------------------------------

| File Stream Modes

|--------------------------------------------------------------------------

|

| These modes are used when working with fopen()/popen()

|

*/

defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');

defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');

defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care

defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care

defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');

defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');

defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');

defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');



/*

|--------------------------------------------------------------------------

| Exit Status Codes

|--------------------------------------------------------------------------

|

| Used to indicate the conditions under which the script is exit()ing.

| While there is no universal standard for error codes, there are some

| broad conventions.  Three such conventions are mentioned below, for

| those who wish to make use of them.  The CodeIgniter defaults were

| chosen for the least overlap with these conventions, while still

| leaving room for others to be defined in future versions and user

| applications.

|

| The three main conventions used for determining exit status codes

| are as follows:

|

|    Standard C/C++ Library (stdlibc):

|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html

|       (This link also contains other GNU-specific conventions)

|    BSD sysexits.h:

|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits

|    Bash scripting:

|       http://tldp.org/LDP/abs/html/exitcodes.html

|

*/

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors

defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error

defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error

defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found

defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class

defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member

defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input

defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error

defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code

defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*---------Site Settings--------*/

/*------------------------------*/	



/*Site Related Settings*/

define('SITE_NAME', 'ICONIK');

define('SITE_CONTACT_EMAIL', 'contact@iconik.in');

define('SITE_ACCOUNT_EMAIL', 'contact@iconik.in');

define('MULTISESSION', true);

define('MULTISTORE', false); /*is multistore shopping cart?*/

define('PHONE_NO_VERIFICATION', true);

define('DATE_FORMAT',"%b %e, %Y"); /* dd-mm-yyyy */

define('DEFAULT_SOURCE_ID', 1);

define('DEFAULT_DEVICE_TYPE_ID', 1);



/*Image*/

define('THUMBNAIL_SIZE', 110); /*Required*/



/*Social */

define('FACEBOOK_URL', 'connectsin');

define('TWITTER_URL', 'connectsin');

define('LINKEDIN_URL', 'connectsin');

define('GOOGLE_PLUS_URL', 'connectsin');





/* SMS API Details */

define('SMS_API_URL', 'https://login.bulksmsgateway.in/sendmessage.php');

define('SMS_API_USERNAME', 'fonty');

define('SMS_API_PASSWORD', 'concept1');

define('SIGNATURE', "<p style='font-family: 'Roboto', sans-serif; font-size:16px; color:#444444; font-weight:500; margin-bottom:30px;'>For any queries, feel free to email us at contact@iconik.in or call us at +91.731.4989666 from Monday to Saturday between 10.00 AM to 06.00 PM</p><p>Regards</p><p style='font-family: 'Roboto', sans-serif; font-size:16px; color:#444444; font-weight:500; margin-bottom:30px;'>Support Team<br>Iconik Technologies LLP<br># 615, Apollo Premier, Vijay Nagar<br>Indore – 452010 Madhya Pradesh INDIA<br>Tel: +91-731-498-9666<br>Web: iconik.in </p>");

switch (ENVIRONMENT)

{

	case 'local':

	/*Paths*/

	define('SITE_HOST', 'http://127.0.0.1/');

	define('MP_BASE_URL', 'http://127.0.0.1/marketplace/');

	define('BA_BASE_URL', 'http://127.0.0.1/marketplace/ba/');

	define('ROOT_FOLDER', '');



	/*SMTP Settings*/

	define('SMTP_PROTOCOL', 'mail');

	define('SMTP_HOST', 'mail.iconik.in');

	define('SMTP_PORT', '587');

	define('SMTP_USER', 'contact@iconik.in');

	define('SMTP_PASS', 'Qnoreply@2019');

	define('SMTP_CRYPTO', 'tls'); /*ssl





	/*From Email Settings*/

	define('FROM_EMAIL', 'contact@iconik.in');

	define('FROM_EMAIL_NAME', SITE_NAME);



	/*No-Reply Email Settings*/

	define('NOREPLY_EMAIL', SITE_NAME);

	define('NOREPLY_NAME', "contact@iconik.in");



	/*Site Related Settings*/

	define('API_SAVE_LOG', true);

	/* payUmoney action url */

	define('PAYUMONEY_BASE_URL', 'https://secure.payu.in');

	break;

	case 'testing':



	/*Paths*/

	define('SITE_HOST', 'https://sandbox2.iconik.in/');

	define('MP_BASE_URL', 'https://mp.iconik.in/marketplace/');

	define('BA_BASE_URL', 'https://mp.iconik.in/marketplace/ba/');

	define('ROOT_FOLDER', '');



	/*SMTP Settings*/

	define('SMTP_PROTOCOL', 'mail');

	define('SMTP_HOST', 'mail.iconik.in');

	define('SMTP_PORT', '587');

	define('SMTP_USER', 'contact@iconik.in');

	define('SMTP_PASS', 'Qnoreply@2019');

	define('SMTP_CRYPTO', 'tls'); /*ssl



	/*From Email Settings*/

	define('FROM_EMAIL', 'contact@iconik.in');

	define('FROM_EMAIL_NAME', SITE_NAME);    


	/*No-Reply Email Settings*/

	define('NOREPLY_EMAIL', SITE_NAME);

	define('NOREPLY_NAME', "contact@iconik.in");

	/*Site Related Settings*/

	define('API_SAVE_LOG', true);

	/* payUmoney action url */

	define('PAYUMONEY_BASE_URL', 'https://secure.payu.in');

	break;



	case 'demo':

	/*Paths*/

	define('SITE_HOST', 'https://cp.iconik.in/');

	define('MP_BASE_URL', 'https://iconik.in/');

	define('BA_BASE_URL', 'https://ba.iconik.in/');

	define('ROOT_FOLDER', '');



	/*SMTP Settings*/

	/*define('SMTP_PROTOCOL', 'smtp');

	define('SMTP_HOST', 'smtp-relay.sendinblue.com');

	define('SMTP_PORT', '587');

	define('SMTP_USER', 'contact@iconik.in');

	define('SMTP_PASS', 'dCpjkaxh3ADBLGmQ');

	define('SMTP_CRYPTO', 'tls'); /*ssl*/

	define('SMTP_PROTOCOL', 'mail');

	define('SMTP_HOST', 'in-v3.mailjet.com');

	define('SMTP_PORT', '465');

	define('SMTP_USER', 'cf62cec6261ecaeda7e4bdbdb83828a1');

	define('SMTP_PASS', '105e3f432f9a22929bb85306bde476cc');

	define('SMTP_CRYPTO', 'ssl'); /*tls



	/*SMTP Settings*/

	/*define('SMTP_PROTOCOL', 'smtp');

	define('SMTP_HOST', 'smtp-relay.sendinblue.com');

	define('SMTP_PORT', '587');

	define('SMTP_USER', 'contact@iconik.in');

	define('SMTP_PASS', 'dCpjkaxh3ADBLGmQ');

	define('SMTP_CRYPTO', 'tls'); /*ssl*/



	/*From Email Settings*/

	//define('FROM_EMAIL', 'contact@iconik.in');

	define('FROM_EMAIL', 'contact@iconik.in');

	define('FROM_EMAIL_NAME', SITE_NAME);



	/*No-Reply Email Settings*/

	define('NOREPLY_EMAIL', SITE_NAME);

	define('NOREPLY_NAME', "contact@iconik.in");



	/*Site Related Settings*/

	define('API_SAVE_LOG', true);

	/* payUmoney action url */

	define('PAYUMONEY_BASE_URL', 'https://secure.payu.in');

	break;

case 'production':

	/*Paths*/

	define('SITE_HOST', 'https://cp.iconik.in/');

	define('MP_BASE_URL', 'https://iconik.in/');

	define('BA_BASE_URL', 'https://ba.iconik.in/');

	define('ROOT_FOLDER', '');



	/*SMTP Settings*/

	define('SMTP_PROTOCOL', 'mail');

	define('SMTP_HOST', 'mail.iconik.in');

	define('SMTP_PORT', '587');

	define('SMTP_USER', 'contact@iconik.in');

	define('SMTP_PASS', 'Qnoreply@2019');

	define('SMTP_CRYPTO', 'tls'); /*ssl





	/*From Email Settings*/

	define('FROM_EMAIL', 'contact@iconik.in');

	define('FROM_EMAIL_NAME', SITE_NAME);



	/*No-Reply Email Settings*/

	define('NOREPLY_EMAIL', SITE_NAME);

	define('NOREPLY_NAME', "contact@iconik.in");



	/*Site Related Settings*/

	define('API_SAVE_LOG', true);



	/* payUmoney action url */

	define('PAYUMONEY_BASE_URL', 'https://secure.payu.in');

	break;

}



define('ADMIN_BASE_URL', SITE_HOST . ROOT_FOLDER);

define('BASE_URL', SITE_HOST . ROOT_FOLDER .'api/');

define('ASSET_BASE_URL', BASE_URL . 'asset/');

define('PROFILE_PICTURE_URL', BASE_URL . 'uploads/profile/picture');


/* S3 Settings */
define('BUCKET', 'BucketName');
define('AWS_ACCESS_KEY', '');
define('AWS_SECRET_KEY', '');
define('IMAGE_SERVER', '');
define('IMAGE_SERVER_PATH', (IMAGE_SERVER == 'remote' ? "https://".BUCKET.'.s3.amazonaws.com/':BASE_URL));