<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4" id="top-heading">Manage Enquiry</h1>
</header>
<?php //$this->load->view('includes/breadcrumb'); ?>

<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body"><!-- Body -->


<nav aria-label="breadcrumb" >
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>  
		<li class="breadcrumb-item">Manage Enquiry</li>
	</ol> 
</nav>


	<!-- Top container -->

<div class="clearfix mt-2 mb-2">
	<form class="" id="filterForm" role="form" autocomplete="off">
		<!-- <div class="row">
			<div class="col-md-2">
				<span class="float-left records d-none d-sm-block">
					<span class="h5">Total records: {{data.dataList.length}}</span>
				</span>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<div id="universal_search"> 
						<input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
						<span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
						<a  class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
					</div>				 	 
			    </div>
			</div>
		</div> -->

		<div class="row">
			<div class="col-md-2">
				<span class="float-left records d-none d-sm-block">
					<span class="h5">Total records: {{data.dataList.length}}</span>
					<input type="hidden" name="TotalRecordsEnquiry" id="TotalRecordsEnquiry" value="{{data.dataList.length}}">
				</span>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<select name="filterStatus" id="filterStatus" class="form-control">
						<option value="1">Unassigned</option>

						<option value="2">Assigned</option>
					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterSource" id="filterSource" class="form-control">

						<option value="">Select Source</option>

						<option value="Reference">Reference</option>

						<option value="Website">Website</option>

						<option value="App">App</option>

						<option value="Walkin">Walk-in</option>

						<option value="Advertisement">Advertisement</option>

					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterAssignedToUser" id="filterAssignedToUser" class="form-control">

						<option value="">Select Assigned To</option>

						<option ng-repeat="xx in data.Users" value="{{xx.UserID}}">{{xx.FirstName}}&nbsp;{{xx.LastName}}&nbsp;({{xx.UserTypeName}})</option>

					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterInterestedIn" id="filterInterestedIn" class="form-control">

						<option value="">Select Interested In</option>

						<option ng-repeat="xx in data.InterestedIn" value="{{xx.EnquiryInterestedIn}}">{{xx.EnquiryInterestedIn}}</option>

					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="" maxlength="50" placeholder="From Date">
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="" maxlength="50" placeholder="To Date">
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
			
		</div>
	</form>			


	<div class="float-right col-md-6" style="margin-top: -35px;">

				

	</div>
	
</div>

<div class="row">
	<div class="col-md-12"><hr/></div>
</div>

<div class="row" style="margin-bottom: 5px;">
	<div class="col-md-6">
		<button class="btn btn-success btn-sm" ng-click="loadFormChurned();">Churned Enquiry</button>&nbsp;
		<button class="btn btn-success btn-sm" ng-click="loadFormAssigned();">Assigned Enquiry</button>&nbsp;
		<button class="btn btn-success btn-sm" ng-click="loadFormAdd();">Add Enquiry</button>
	</div>	

	<div class="col-md-6" style="text-align: right;">
		<button class="btn btn-warning btn-sm" id="BulkAssignEnquiry" disabled ng-click="loadFormBulkAssign();">Bulk Assign Enquiry</button>
		&nbsp;&nbsp;&nbsp;
		<button class="btn btn-warning btn-sm" id="BulkUnAssignEnquiry" disabled ng-click="loadFormBulkUnAssign();">Bulk Unassign Enquiry</button>
	</div>
</div>			

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 
		

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		<form class="" name="bulk_assign_form" id="bulk_assign_form" role="form" autocomplete="off">
		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width:50px;" class="text-center">Enquiry Date</th>

					<th style="width: 150px;">Name</th>

					<th style="width: 150px;">Mobile / Email</th>

					<th style="width: 100px;">Interested In</th>					

					<th style="width: 100px;">Remarks</th>

					<th style="width: 10px;"># Current Status</th>

					<th style="width: 100px;">Assigned To</th>

					<th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">

				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.EnquiryGUID}}">

					<td>{{row.EnquiryDate}}</td>

					<td>{{row.EnquiryPersonName}}<br/>({{row.EnquirySource}})</td>					

					<td>{{row.EnquiryMobile}}<br/>{{row.EnquiryEmail}}</td>

					<td>{{row.EnquiryInterestedIn}}</td>

					<td>{{row.EnquiryRemarks}}</td>

					<td><input type="checkbox" class="EnquiryChk" name="EnquiryChk[]" onchange="checkEnquiryLength();" onclick="checkEnquiryLength();" value="{{row.EnquiryGUID}}">&nbsp;{{row.EnquiryStatusName}}</td>


					<td>{{row.AssignedToUser}}</td>


					<td class="text-center">
						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.EnquiryGUID)" title="View"></a>
						
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.EnquiryGUID)" title="Edit"></a>
						
						<a class="glyphicon glyphicon-plus" href="" ng-if="row.EnquiryAssignToUser <= 0 && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" ng-click="loadFormAssign(key, row.EnquiryGUID)" title="Assign"></a>

						<a ng-if="((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0) && row.EnquiryAssignToUser <= 0 " class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.EnquiryGUID)" data-toggle="tooltip" title="Delete"></a>
					</td>

				</tr>

			</tbody>

		</table>
		</form>


		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->




	<!-- add Modal -->
	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Enquiry</h3>					    	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>







	<!-- edit Modal -->

	<div class="modal fade" id="edits_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit Enquiry</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>



	<!-- View Modal -->
	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Enquiry</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>





	<!-- Assign Modal -->

	<div class="modal fade" id="assign_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Assign Enquiry</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAssign"></div>

			</div>

		</div>

	</div>



	<!-- Bulk Assign Modal -->

	<div class="modal fade" id="bulk_assign_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Bulk Assign Enquiry</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLBulkAssign"></div>

			</div>

		</div>

	</div>


</div><!-- Body/ -->