<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
class Signup extends API_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Recovery_model');
		$this->load->model('Users_model');
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
		$this->load->model('Entity_model');
		$this->load->model('Common_model');
	}

	/*
	Name: 			Signup
	Description: 	Use to register user to system.
	URL: 			/api/signup/
	*/
	public function index_post()
	{
		$this->form_validation->set_rules('FirstName', 'Name', 'trim|required');

		$this->form_validation->set_rules('UserID', 'UserID', 'trim');

		//echo $this->UserID; 

		if(empty($this->input->Post('UserEmail'))){

			$this->form_validation->set_rules('Email', 'Email', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail',array("validateEmail"=>"Email's address is already in used"));

		}else{
			$this->form_validation->set_rules('Email', 'Valid Email Id', 'trim|required');			
		}


		if(empty($this->input->Post('type'))){
			$this->form_validation->set_rules('ConfirmEmail', 'Confirm email', 'trim|required|valid_email|matches[Email]',array("matches"=>"Email's address do not match"));

			$this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[6]');
			
			$this->form_validation->set_rules('ConfirmPassword', 'Confirm Password', 'trim|required|matches[Password]',array("matches"=>"Password do not match"));
	      	$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]');
			$this->form_validation->set_rules('MiddleName', 'MiddleName', 'trim');
			$this->form_validation->set_rules('LastName', 'LastName', 'trim');
			$this->form_validation->set_rules('UserTypeID', 'Registration Type', 'trim|required|in_list[2,3,10]');
			$this->form_validation->set_rules('Gender', 'Gender', 'trim|in_list[Male,Female,Other]');
			$this->form_validation->set_rules('BirthDate', 'BirthDate', 'trim|callback_validateDate');
			$this->form_validation->set_rules('Age', 'Age', 'trim|integer');
		   
			$this->form_validation->set_message('is_unique', 'This Mobile Number already exists');
			$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');

			$this->form_validation->set_rules('SourceGUID', 'SourceGUID', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] != 'Direct' ? '|required' : '') . '|callback_validateSourceGUID[' . @$this->Post['Source'] . ']');

			$this->form_validation->set_rules('DeviceType', 'Device type', 'trim|required|callback_validateDeviceType');
			$this->form_validation->set_rules('IPAddress', 'IPAddress', 'trim|callback_validateIP');
			$this->form_validation->set_rules('ReferralCode', 'ReferralCode', 'trim|callback_validateReferralCode');
		}else{
			$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]');
			//$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[7,10,11,82,83]');			
			$this->form_validation->set_rules('AdminAccess', 'AdminAccess', 'trim|required');
			$this->form_validation->set_rules('TNC', 'Terms and conditions', 'trim|required');
		}
		$this->form_validation->validation($this); /* Run validation */


		if(!in_array($_POST['UserTypeID'], array(7,10,11,82,83,79,87,88,80,81,90)))
		{
			$this->Return['ResponseCode'] = 500;
			
			$this->Return['Message'] = "Please select registration type.";

			die;
		}


		if(empty($this->input->Post('UserEmail'))){

				$UserID = $this->Users_model->addUser(array_merge($this->Post, array(
					"Referral" => @$this->Referral
				)) , $this->Post['UserTypeID'], $this->SourceID, ($this->Post['Source'] != 'Direct' ? '2' : '1') /*if source is not Direct Account treated as Verified*/);

				if(!empty($this->input->Post('type'))){
	                $this->load->model('Recovery_model');

					$Token = $this->Recovery_model->generateToken($UserID, 1);
					//$Token = $this->generateToken($EntityID, 1);
					$Tokens = $this->Recovery_model->generateToken($UserID, 2);
					
					$EmailText = "One time password reset code is given below:";
					
					$content = $this->load->view('emailer/marketplace/registration',array("Name" => $this->Post['Email'], 'Token' => @$Token, 'EmailText' => $EmailText,"Tokens"=>@$Tokens),TRUE);

					$SendMail = sendMail(array(

						'emailTo' 		=> $this->Post['Email'],			

						'emailSubject'	=> SITE_NAME . " Verify Email and Set A Password",

						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

					));
				}else if (!empty($this->Post['Email'])) {
					/* Send welcome Email to User with Token. (only if source is Direct) */
					$Token = ($this->Post['Source'] == 'Direct' ? $this->Recovery_model->generateToken($UserID, 2) : '');

					$content = $this->load->view('emailer/signup', array(
							"Name" => @$this->Post['Username'],
							'Token' => $Token,
							'DeviceTypeID' => $this->DeviceTypeID
						) , TRUE);
					sendMail(array(
						'emailTo' => $this->Post['Email'],
						'emailSubject' => "Thank you for registering at " . SITE_NAME,
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
					));
				}
		}else{
			$UserData = $this->Users_model->getUsers('Email', array(
				'Email' => $this->input->post('Email'),
				'StatusID' => 2
			));

			if(!empty($UserData)){
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Email's address is already in used.";
			}else{
				$UserID = $this->Users_model->updateUserInfoOnSignup($this->input->Post('UserEmail'),$this->Post);
			}
		}

		if (!$UserID) {
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! This email is already exist or some error occurred, please try later.";
		}
		else {
			/*referal code generate*/
			$this->load->model('Utility_model');
			$this->Utility_model->generateReferralCode($UserID);
			/* Send welcome notification */
			$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);
			/* get user data */
			$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
				'UserID' => $UserID
			));
			/* create session only if source is not Direct and account treated as Verified. */
			$UserData['SessionKey'] = '';

			$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(
				"IPAddress" => @$this->Post['IPAddress'],
				"SourceID" => $this->SourceID,
				"DeviceTypeID" => $this->DeviceTypeID,
				"DeviceGUID" => @$this->Post['DeviceGUID'],
				"DeviceToken" => @$this->Post['DeviceToken']
			));

			$this->Return['Data'] = $UserData;
			$this->Return['Message']  ="Thank you for registering  with us.
                       An email has been sent to your email id."; 
		}
		//}
	}




	/*
	Name: 			Signup
	Description: 	Use to register user to system.
	URL: 			/api/signup/
	*/
	public function marketPlaceRegistration_post()
	{/*
		$content = '<h2 style="font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; ">Hello Rohit singh</h2><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p><p>Thankfully<br>Iconik Technologies LLP</p>';

		sendMail(array(
				'emailTo' 		=> "developer@iconik.in",	
				'emailSubject'	=> "Testing email template.",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE)) 
			));

		die;*/

		// $data = sendSMS(array(
		// 			'PhoneNumber' 	=> "9893535292",			
		// 			'Message'			=> SITE_NAME.", OTP to verify Mobile no. is:",
		// 		));
		// print_r($data); die;
		$this->form_validation->set_rules('FirstName', 'Name', 'trim|required');

		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail',array("validateEmail"=>"Email address is already in use"));

		$this->form_validation->set_rules('PhoneNumberForChange', 'PhoneNumberForChange', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]',array("is_unique"=>"Phone number is already in use"));

		$this->form_validation->set_rules('PhoneNumberForChange', 'PhoneNumber', 'trim|required|is_unique[tbl_users.PhoneNumberForChange]|min_length[10]|max_length[10]',array("is_unique"=>"Phone number is already in use"));

		//$this->form_validation->set_rules('UserTypeID', 'Registration Type', 'trim|required|in_list[7,10,11,82,83,79,87,88,80,81,90]');

		

		$this->form_validation->set_rules('TNC', 'Terms and conditions', 'trim|required');

		$this->form_validation->set_rules('SourceGUID', 'SourceGUID', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] != 'Direct' ? '|required' : '') . '|callback_validateSourceGUID[' . @$this->Post['Source'] . ']');

		$this->form_validation->set_rules('AdminAccess', 'AdminAccess', 'trim|required');

		$this->form_validation->set_rules('Resource', 'Resource', 'trim');

		$this->form_validation->validation($this); /* Run validation */
		

		if(!preg_match("/^([A-Za-z ])+$/i", $this->Post['FirstName']))
		{
			$this->Return['ResponseCode'] = 500;
			
			$this->Return['Message'] = "Only alphabets are allowed in Name field.";

			die;
		}


		if($_POST['UserTypeID'] == 99999)
		{
			$this->Return['ResponseCode'] = 500;
			
			$this->Return['Message'] = "Please select any one Institute/Training Center type.";

			die;
		}
		elseif(!in_array($_POST['UserTypeID'], array(7,10,11,82,83,79,87,88,80,81,90)))
		{
			$this->Return['ResponseCode'] = 500;
			
			$this->Return['Message'] = "Please select registration type.";

			die;
		}

		$UserID = $this->Users_model->addUser(array_merge($this->Post, array("Referral" => @$this->Referral)) , $this->Post['UserTypeID'], 1, ($this->Post['Source'] != 'Direct' ? '2' : '1'));
		if(!empty($UserID)){
				$this->load->model('Recovery_model');

				/*referal code generate*/
				$this->load->model('Utility_model');
				$this->Utility_model->generateReferralCode($UserID);
				/* Send welcome notification */
				$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);
				/* get user data */
				$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
					'UserID' => $UserID
				));

				/* create session only if source is not Direct and account treated as Verified. */
				$UserData['SessionKey'] = '';

				$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(
					"IPAddress" => @$this->Post['IPAddress'],
					"SourceID" => $this->SourceID,
					"DeviceTypeID" => $this->DeviceTypeID,
					"DeviceGUID" => @$this->Post['DeviceGUID'],
					"DeviceToken" => @$this->Post['DeviceToken']
				));

				$UserData['OTP'] = '';
				$UserData['OTP_PASS'] = '';
				
				if($_POST['UserTypeID'] == 88)
				{
					$this->Return['Message']  ="OTP has been sent on your mobile number."; 

					$Tokens1 = $this->Recovery_model->getTokenInfoByID($UserID, 1);
					$Tokens2 = $this->Recovery_model->getTokenInfoByID($UserID, 2);
					if(isset($Tokens1) && !empty($Tokens1) && isset($Tokens2) && !empty($Tokens2))
					{
						$UserData['OTP'] = $Tokens2;
						$UserData['OTP_PASS'] = $Tokens1;
					}					
				}
				else
				{
					$this->Return['Message']  ="Thank you for registering  with us.
	                       An email has been sent to your email id."; 
	            } 


	            $this->Return['Data'] = $UserData;


		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! This email is already exist or some error occurred, please try later.";
			
		}
	}

	/*
	Name: 			resendverify
	Description: 	Use to resend OTP for Email address verification.
	URL: 			/api/signup/resendverify
	*/
	

	public function checkEmailExist_post(){
		$this->form_validation->set_rules('Email', 'Email','trim|required');
		$this->form_validation->validation($this); 

		$UserData = $this->Common_model->checkEmailExist($this->input->post('Email'));

		//print_r($UserData);
		return $this->input->post('Email');
	}



	//Apply for loan----------------------------------------------
	public function applyForLoan_post()
	{
		$this->form_validation->set_rules('FirstName', 'Name', 'trim|required');

		$this->form_validation->set_rules('Email', 'Email', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email');

		$this->form_validation->set_rules('PhoneNumberForChange', 'PhoneNumberForChange', 'trim|required|min_length[10]|max_length[10]');
		
		
		$this->form_validation->set_rules('LoanPurpose', 'Purpose of loan', 'trim|required');
		$this->form_validation->set_rules('LoanAmount', 'Loan amount required', 'trim|required');
		$this->form_validation->set_rules('LoanFamilyIncome', 'Family annual income', 'trim|required');

		$this->form_validation->validation($this); /* Run validation */

		$check = $this->Users_model->checkLoanStatus($this->input->post('Email'));
		//print_r($check); die;
		if($check == "Already Applied"){
			$this->Return['ResponseCode'] = 300;
			$this->Return['Message'] = "You have already applied for the loan. <a href='./signin.php?page=loan' style='color:blue;text-decoration:underline;'>Sign in</a> to check the status.";
		}else if($check == "Already Registered"){
			$this->Return['ResponseCode'] = 400;
			$this->Return['Message'] = "You are already registered with us. <a href='./signin.php?page=loan' style='color:blue;text-decoration:underline;'>Click here</a> to Sign in  to apply for the loan.";
		}else{
		

			$UserID = $this->Users_model->addUser(array_merge($this->Post, array("Referral" => @$this->Referral)) , 88, 1, 1);

			if(!empty($UserID))
			{
				$this->load->model('Recovery_model');

				// $Tokens = $this->Recovery_model->generateToken($UserID, 1);
				
				// $Token = $this->Recovery_model->generateToken($UserID, 2);				
				
				// $EmailText = "One time password reset code is given below:";
		
				// $SendMail = sendMail(array(

				// 	'emailTo' 		=> $this->Post['Email'],			

				// 	'emailSubject'	=> SITE_NAME . " Verify Email and Set A Password",

				// 	'emailMessage'	=> emailTemplate($this->load->view('emailer/marketplace/registration',array("Name" => $this->Post['Email'], 'Token' => @$Token, 'EmailText' => $EmailText,"Tokens"=>@$Tokens),TRUE))

				// ));

				/*referal code generate*/
				// $this->load->model('Utility_model');
				// $this->Utility_model->generateReferralCode($UserID);
				/* Send welcome notification */
				//$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);
				/* get user data */
				$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
					'UserID' => $UserID
				));
				/* create session only if source is not Direct and account treated as Verified. */
				$UserData['SessionKey'] = '';

				$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(
					"IPAddress" => @$this->Post['IPAddress'],
					"SourceID" => $this->SourceID,
					"DeviceTypeID" => $this->DeviceTypeID,
					"DeviceGUID" => @$this->Post['DeviceGUID'],
					"DeviceToken" => @$this->Post['DeviceToken']
				));

				$LoanID = $this->Users_model->applyForLoan($UserID, $this->Post);
				if($LoanID > 0)
				{
					$Message = " and successfully applied for loan.";
				}
				$this->Return['Data'] = $UserData;
				$this->Return['Message']  ="You for successfully registered with us $Message. An email has been sent to your email id."; 
			}
			else
			{
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Sorry! Some error occurred, Please try later.";	
			}
		}
	}



	public function BARegistration_post()
	{
		$this->form_validation->set_rules('BusinessName', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('GSTNumber', 'GST Number', 'trim|required');

		$this->form_validation->set_rules('FirstName', 'Name', 'trim|required');
		$this->form_validation->set_rules('Address', 'Address', 'trim|required');

		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail',array("validateEmail"=>"Email address is already in use"));

		$this->form_validation->set_rules('PhoneNumberForChange', 'Mobile Number', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]',array("is_unique"=>"Phone number is already in use"));

		$this->form_validation->set_rules('PhoneNumberForChange', 'Mobile Number', 'trim|required|is_unique[tbl_users.PhoneNumberForChange]|min_length[10]|max_length[10]',array("is_unique"=>"Phone number is already in use"));
		
		$this->form_validation->set_rules('StateCode', 'State Name', 'trim|required');
		$this->form_validation->set_rules('CityName', 'City Name', 'trim|required');
		$this->form_validation->set_rules('ZipCode', 'Zip Code', 'trim|required');

		$this->form_validation->set_rules('BankName', 'Bank Name', 'trim|required');
		$this->form_validation->set_rules('AccountNumber', 'Account Number', 'trim|required');
		$this->form_validation->set_rules('AccountName', 'Account Name', 'trim|required');
		$this->form_validation->set_rules('AccountIFSCCode', 'IFSC', 'trim|required');
		$this->form_validation->set_rules('BankAddress', 'Bank Address', 'trim|required');

		
		$this->form_validation->set_rules('Source', 'Source', 'trim|required');
		$this->form_validation->set_rules('AdminAccess', 'AdminAccess', 'trim|required');
		$this->form_validation->set_rules('Resource', 'Resource', 'trim');

		$this->form_validation->validation($this); /* Run validation */
		

		if(!preg_match("/^([A-Za-z ])+$/i", $this->Post['FirstName']))
		{
			$this->Return['ResponseCode'] = 500;
			
			$this->Return['Message'] = "Only alphabets are allowed in Name field.";

			die;
		}

		if(!preg_match("/^([A-Za-z ])+$/i", $this->Post['BusinessName']))
		{
			$this->Return['ResponseCode'] = 500;
			
			$this->Return['Message'] = "Only alphabets are allowed in Business Name field.";

			die;
		}


		if($this->input->post('Website') != "")
		{
			if(!validate_input_url($this->input->post('UrlType')."".$this->input->post('Website')))
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter valid Website URL.";
				die;
			}
		}

		if($this->input->post('FacebookURL') != "")
		{
			if(!validate_input_url($this->input->post('FacebookURL')))
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter valid Facebook URL.";
				die;
			}
		}

		if($this->input->post('TwitterURL') != "")
		{
			if(!validate_input_url($this->input->post('TwitterURL')))
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter valid Twitter URL.";
				die;
			}
		}
		
		if($this->input->post('LinkedInURL') != "")
		{
			if(!validate_input_url($this->input->post('LinkedInURL')))
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter valid LinkedIn URL.";
				die;
			}
		}

		if($this->input->post('GoogleURL') != "")
		{
			if(!validate_input_url($this->input->post('GoogleURL')))
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter valid Google URL.";
				die;
			}
		}
		

		$UserID = $this->Users_model->addBAUser(array_merge($this->Post, array("Referral" => @$this->Referral)) , $this->Post['UserTypeID'], 1, ($this->Post['Source'] != 'Direct' ? '2' : '1'));
		if(!empty($UserID)){
				$this->load->model('Recovery_model');

				/*referal code generate*/
				$this->load->model('Utility_model');
				$this->Utility_model->generateReferralCode($UserID);
				/* Send welcome notification */
				$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);
				/* get user data */
				$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
					'UserID' => $UserID
				));

				/* create session only if source is not Direct and account treated as Verified. */
				$UserData['SessionKey'] = '';

				$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(
					"IPAddress" => @$this->Post['IPAddress'],
					"SourceID" => $this->SourceID,
					"DeviceTypeID" => $this->DeviceTypeID,
					"DeviceGUID" => @$this->Post['DeviceGUID'],
					"DeviceToken" => @$this->Post['DeviceToken']
				));

				$this->Return['Data'] = $UserData;
				$this->Return['Message']  ="Thank you for registering  with us. An email has been sent to your email id."; 
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! This email is already exist or some error occurred, please try later.";
			
		}
	}



	public function getCourses_post()
	{		
		$this->load->model('Students_model');
		$Course = $this->Students_model->getCourses(10, 2);
		
		if(!empty($Course))
		{
			$this->Return['Data'] = $Course;
		}	
	}

	/*public function getBatchByCourse_post()
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$batchData = $this->Common_model->getBatchByCourse($this->EntityID,$this->CategoryID);

		if($batchData)
		{
			$this->Return['Data'] = $batchData;	
		}

	}*/

	public function addStudentDirect_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('instID', 'Institute Name', 'trim|required');

		$this->form_validation->set_rules('Course', 'Course', 'trim|required');
		$this->form_validation->set_rules('Batch', 'Batch','trim|required');		

		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');
		$this->form_validation->set_rules('LastName', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'trim|required|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_rules('Address', 'Personal Address', 'trim');
		$this->form_validation->set_rules('StateID', 'State Name', 'trim');
		$this->form_validation->set_rules('CityName', 'City Name', 'trim');		
		
		$this->form_validation->set_rules('FathersName', 'Fathers Name', 'trim');
		$this->form_validation->set_rules('MothersName', 'Mothers Name', 'trim');
		$this->form_validation->set_rules('FathersPhone', 'Fathers/Mothers Mobile', 'trim|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('FathersEmail', 'Fathers/Mothers Email', 'trim|valid_email');
					
		$this->form_validation->validation($this);

		if(!in_array($this->Post['instID'], array(224, 252, 2843)))
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Illegal Institute Name.";

			die;
		}

		
		$UserID = $this->Users_model->addStudentDirect($this->Post);
		
		if($UserID === "PhoneNumber Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This mobile number is already exist.";
		}
		elseif($UserID === "Email Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This email already exist.";
		}
		elseif(isset($UserID) && !empty($UserID)) 
		{
			$this->Return['Data'] = 1;

			$this->Return['Message'] = "Saved successfully.";
		}
		else 
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";
		}
	}


	public function registrationCodeVerify_post()
	{
		$this->form_validation->set_rules('InstCode', 'Code', 'trim|required');							
		$this->form_validation->validation($this);		
		
		$InstCode = $this->Post['InstCode'];

		$arr = array("1" => "A Institute",
		"2" => "B Institute",
		"3" => "C Institute",
		"4" => "D Institute",
		"5" => "E Institute");

		if(!isset($arr[$InstCode]) || empty($arr[$InstCode]))
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Invalid code.";
			die;
		}
		else
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "ok";
			$this->Return['Data'] = $arr[$InstCode];
		}		
	}

	public function registrationByCodeDirectValidate_post()
	{		
		$this->form_validation->set_rules('FirstName', 'Name', 'trim|required');
		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email|required|callback_validateEmail',array("validateEmail"=>"Email address is already in use."));
		$this->form_validation->set_rules('PhoneNumberForChange', 'Mobile', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]',array("is_unique"=>"Mobile number is already in use"));
		$this->form_validation->set_rules('PhoneNumberForChange', 'Mobile', 'trim|required|is_unique[tbl_users.PhoneNumberForChange]|min_length[10]|max_length[10]',array("is_unique"=>"Phone number is already in use."));
					
		$this->form_validation->validation($this);

		$UserID = $this->Users_model->registrationByCodeDirectValidate($this->Post);
		
		if($UserID === "PhoneNumber Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Mobile number is already in use.";
		}
		elseif($UserID === "Email Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Email address is already in use.";
		}
		else 
		{
			$this->Return['Data'] = 1;
		}
			
	}


	public function addStudentToInstituteForBuyApp_post()
	{
		$this->form_validation->set_rules('InstCode', 'InstCode', 'trim|required');
		$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|required');
		$this->form_validation->set_rules('Email', 'Email', 'trim|required');
		$this->form_validation->set_rules('Mobile', 'Mobile', 'trim|required');

		$this->form_validation->set_rules('PaidAmount', 'Paid Amount', 'trim');
		$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim');
		$this->form_validation->set_rules('OrderStatus', 'Order Status', 'trim');
		$this->form_validation->set_rules('TrackingID', 'Tracking ID', 'trim');

		$this->form_validation->validation($this);

		$data = $this->Users_model->addStudentToInstituteForBuyApp($this->Post);

		if($data)
		{
			$this->Return['Data'] 	=	$data;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}

}