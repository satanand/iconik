<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}

	#ui-id-1
	{
		z-index: 9999 !important;
		overflow-x: hidden;
		overflow-y: auto;
		max-height: 150px;
	}
</style>  
<header class="panel-heading"> <h1 class="h4">Manage Downloaded Brochures</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="content-body" ng-init="getBrochuresList(0);"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<span class="h5">Total Downloaded Brochures: {{data.dataList.length}}</span>
				</div>
			</div>			
		</div>


		<form id="filterForm" role="form" autocomplete="off">
		<div class="row">		
		
		<div class="col-md-2">
			<div class="form-group">			 	
	        	<select class="form-control" name="DType" id="DType">
	        		<option value="">All</option>
	        		<option value="B2B">B2B</option>
	        		<option value="B2C">B2C</option>
	        	</select>	
	    	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterStartDate" type="text" placeholder="Download From Date" id="FilterStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterEndDate" type="text" placeholder="Download To Date" id="FilterEndDate" class="form-control">
			</div>
		</div>
				


		<div class="col-md-3">
			<div class="form-group">			 	
	        	<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Search by word" >
	    	</div>
		</div>

		
		<div class="col-md-3">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" ng-click="getBrochuresList(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_filters();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>		

	</div>
	</form>		

	</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getBrochuresList(0);" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading --> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 20px;">#</th>					
					<th style="width: 100px;">Full Name</th>
					<th style="width: 100px;">Email</th> 					
					<th style="width: 70px;">Mobile Number</th>
					<th style="width: 70px;">Brochures Type</th>
					<th style="width: 100px;">Download Date</th>
				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.EnquiryID}}">

					<td>{{row.EnquiryID}}</td>
					
					<td>{{row.FullName}}</td>

					<td>{{row.Email}}</td>

					<td>{{row.Mobile}}</td>					

					<td>{{row.DType | date}}</td>

					<td>{{row.CreatedDate}}</td>					

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>


</div><!-- Body/ -->