<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobprovider extends API_Controller_Secure
{
	function __construct()

	{

		parent::__construct();

		$this->load->model('Common_model');

		$this->load->model('Jobprovider_model');

	}



	/*

	Description: 	Use to add new job

	URL: 			/api_admin/jobs/add	

	*/

	public function add_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Position', 'Position','trim|required');
		$this->form_validation->set_rules('Description', 'Description','trim|required');
		$this->form_validation->set_rules('EndDate', 'End Date','trim|required');
		$this->form_validation->set_rules('PartTimeJobState', 'Job Location State','trim|required');
		$this->form_validation->set_rules('PartTimeJobCity', 'PartTimeJobCity','trim|required');
		$this->form_validation->set_rules('JobStatus', 'Job Status','trim|required');
		$this->form_validation->set_rules('Salary', 'Salary','trim');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobprovider_model->add($this->EntityID, $this->Post);

		if($JobsData == 1)
		{
			$this->Return['Message']      	=	"New Job Posted Successfully."; 
		}
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";			
		}
	}


	/*

	Description: 	Use to get jobs list

	URL: 			/api/jobs/getJobs	

	*/

	public function getJobs_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Qualification', 'Qualification', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobprovider_model->getJobs($this->EntityID, $this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*

	Description: 	Use to edit job

	URL: 			/api_admin/jobs/edit	

	*/

	public function edit_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PartTimeJobGUID', 'PartTimeJobGUID', 'trim|required');
		$this->form_validation->set_rules('Position', 'Position','trim|required');
		$this->form_validation->set_rules('Description', 'Description','trim|required');
		$this->form_validation->set_rules('EndDate', 'End Date','trim|required');
		$this->form_validation->set_rules('PartTimeJobState', 'Job Location State','trim|required');
		$this->form_validation->set_rules('PartTimeJobCity', 'PartTimeJobCity','trim|required');
		$this->form_validation->set_rules('JobStatus', 'Job Status','trim|required');
		$this->form_validation->set_rules('Salary', 'Salary','trim');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobprovider_model->edit($this->EntityID, $this->Post);

		if($JobsData == 1)
		{
			$this->Return['Message']      	=	"Job Updated Successfully."; 
		}
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";			
		}
	}

	/*

	Description: 	Use to delete job

	URL: 			/api_admin/jobs/delete	

	*/

	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PartTimeJobGUID', 'PartTimeJobGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobprovider_model->delete($this->EntityID, $this->Post);

		//echo $NewsData;

		if($JobsData == 1){
			$this->Return['Message']      	=	"Job Deleted Successfully.";

		} else {
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";
			
		}
	}


	/*
	Description: 	Use to get Applicants for particular Job
	URL: 			/api/jobs/getApplicants	
	*/

	public function getApplicants_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PartTimeJobID', 'PartTimeJobID', 'trim|integer');
		$this->form_validation->set_rules('FilterStartDate', 'FilterStartDate', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'FilterEndDate', 'trim');
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobprovider_model->getApplicants($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	



	/*-------------------------------------------------------------
	Section For Faculty Market Place
	-------------------------------------------------------------*/
	/*
	Description: 	Use to get jobs posted by job provider
	URL: 			/api/jobs/getPartTimeJobs
	*/
	public function getPartTimeJobs_post()
	{
		//$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|required|callback_validateEntityGUID');
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');			
		$this->form_validation->set_rules('ForStudents', 'ForStudents', 'trim');
		$this->form_validation->set_rules('FilterStartDate', 'FilterStartDate', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'FilterEndDate', 'trim');
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobprovider_model->getPartTimeJobs($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*
	Description: 	Use to apply For Job by Faculty
	URL: 			/api/jobs/applyForJob
	*/
	public function applyForJob_post() 
	{	
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('PartTimeJobID', 'PartTimeJobID', 'trim|required');
		$this->form_validation->set_rules('ProviderID', 'ProviderID', 'trim');
		$this->form_validation->set_rules('ProviderEmail', 'ProviderEmail', 'trim');
		$this->form_validation->set_rules('ProviderPhoneNumber', 'ProviderPhoneNumber', 'trim');
		$this->form_validation->set_rules('Type', 'Type', 'trim|required');

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Status = $this->Jobprovider_model->applyForJob($this->EntityID, $this->Post);

		if(!$Status)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";
			$this->Return['Data']      		=	array(); 
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;
			if($this->input->post('Type') == 1){
				$this->Return['Message']      	=	"Successfully applied for job.";
			}else{
				$this->Return['Message']      	=	"Successfully declined for job.";
			}			
			$this->Return['Data']      		=	array();
		}
	}


	/*
	Description: 	Use to get Applied Jobs by Faculty
	URL: 			/api/jobs/getAppliedJobs
	*/
	public function getAppliedJobs_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('FilterStartDate', 'FilterStartDate', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'FilterEndDate', 'trim');
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobprovider_model->getAppliedJobs($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}



	/*
	Description: 	Use to get ShortListed Jobs by Faculty
	URL: 			/api/jobs/getShortListedJobs
	*/
	public function getShortListedJobs_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobprovider_model->getShortListedJobs($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*
	Description: 	Use to set Shortlist Jobs by faculty
	URL: 			/api/jobs/setShortlistJobs
	*/
	public function setShortlistJobs_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('PartTimeJobID', 'Job ID', 'trim|required');
		$this->form_validation->set_rules('Type', 'Type', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobprovider_model->setShortlistJobs($this->EntityID, $this->Post);

		if($JobsData)
		{
			if($this->Post['Type'] == 1)
				$this->Return['Message']   =	"Successfully added in shortlist jobs.";
			else
				$this->Return['Message']   =	"Successfully removed from shortlist jobs.";

		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message']      =	"Sorry! Some error occured"; 
		}
	}


	


 	public function uploadLatestResume_post() 
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ApplyID', 'ApplyID', 'trim|required');		
		$this->form_validation->set_rules('MediaGUID', 'Resume', 'trim|required');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Status = $this->Jobprovider_model->uploadLatestResume($this->EntityID, $this->Post);

		if($Status == -1)
		{			
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Your application not found.";
			$this->Return['Data']      		=	array(); 
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Message']      	=	"Resume updated successfully.";
			$this->Return['Data']      		=	array();
		}
	}
	


}