<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fee_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /*
    Description: 	Use to get list of post.
    Note:			$Field should be comma seprated and as per selected tables alias. 
    */
    function getFee($EntityID, $Where = array(), $multiRecords = FALSE, $PageNo = 1, $PageSize = 10)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
       // print_r($Where); die;
        if (!empty($Where['ParentCategoryGUID'])) {
            $query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = " . $InstituteID . " and tbl_entity.StatusID = 2 and CategoryTypeID = 1 and CategoryGUID = '".$Where['ParentCategoryGUID']."'");
        } else {
            $query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = " . $InstituteID . " and tbl_entity.StatusID = 2 and CategoryTypeID = 1");
        }
        
        
        
        $course      = $query->result_array();
        $result      = array();
        $new_result  = array();
        $check_array = array();
        $fee_array   = array();
        $check       = 1;
        $audio_count = 0;
        $video_count = 0;
        $file_count  = 0;
        
        if (count($course) > 0) {
            $p = 0;
         foreach ($course as $key => $value) {
            
            if (!empty($Where['CategoryID'])) {

            	//echo "select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID,Duration from set_categories where CategoryID = '" . $Where['CategoryID'] . "'"; 
                $query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID,Duration from set_categories where CategoryID = '" . $Where['CategoryID'] . "'");
                
                
                $SubCategoryData    = $query->result_array();
                $parentCategoryName = $value['CategoryName'];
                $parentCategoryGUID = $value['CategoryGUID'];
                
                
                foreach ($SubCategoryData as $k => $v) {
                    
                    $result[$k]['ParentCategoryName'] = $parentCategoryName;                    
                    $result[$k]['parentCategoryGUID'] = $parentCategoryGUID;
                    $result[$k]['SubCategoryName']    = $v['CategoryName'];
                    $result[$k]['CategoryGUID']       = $v['CategoryGUID'];
                    $result[$k]['Duration']           = $v['Duration'];

                    //echo "select * from tbl_setting_fee f join tbl_entity On tbl_entity.EntityID = f.FeeID where tbl_entity.InstituteID = ". $InstituteID ." and tbl_entity.StatusID = 2 and f.CourseID='". $v['CategoryID'] ."' order by f.FeeID DESC limit 1"; die;

                    $sql = $this->db->query("select * from tbl_setting_fee f join tbl_entity On tbl_entity.EntityID = f.FeeID where tbl_entity.InstituteID = ". $InstituteID ." and tbl_entity.StatusID = 2 and f.CourseID='". $v['CategoryID'] ."' order by f.FeeID DESC limit 1");
                    
                    
                    if ($sql->num_rows() > 0) {
                        $Feedata = $sql->result_array();
                        $sql     = $this->db->query("select * from tbl_setting_installment  where StatusID = 2 and  FeeID='" . $feeData[0]['FeeID'] . "'");
                        if ($sql->num_rows() > 0) {
                            $InstallmentData = $sql->result_array();
                        } else {
                            $InstallmentData = array();
                        }
                    } else {
                        $Feedata         = array();
                        $InstallmentData = array();
                    }
                    
                    $result[$k]['Feedata']         = $Feedata;
                    $result[$k]['InstallmentData'] = $InstallmentData;
                    //print_r($result);
                    
                    array_push($new_result, $result[$k]);
                }
                
                
            } else {
                
                    
                $query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID,Duration from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = " . $InstituteID . "  and tbl_entity.StatusID = 2 and CategoryTypeID = 2 and ParentCategoryID = '" . $value['CategoryID'] . "'");
                
                
                $SubCategoryData    = $query->result_array();
                $parentCategoryName = $value['CategoryName'];
                $parentCategoryGUID = $value['CategoryGUID'];
                
                
                foreach ($SubCategoryData as $k => $v) {
                    
                    $result[$k]['ParentCategoryName'] = "";
                    if (!in_array($parentCategoryName, $check_array)) {
                        array_push($check_array, $parentCategoryName);
                        $result[$k]['ParentCategoryName'] = $parentCategoryName;
                    }
                    
                    
                    $result[$k]['parentCategoryGUID'] = $parentCategoryGUID;
                    $result[$k]['SubCategoryName']    = $v['CategoryName'];
                    $result[$k]['CategoryGUID']       = $v['CategoryGUID'];
                    $result[$k]['Duration']           = $v['Duration'];
                    
                    $sql = $this->db->query("select * from tbl_setting_fee f join tbl_entity On tbl_entity.EntityID = f.FeeID where tbl_entity.InstituteID = ". $InstituteID ." and tbl_entity.StatusID = 2 and f.CourseID='" . $v['CategoryID'] . "' order by f.FeeID DESC limit 1");
                    
                    
                    if ($sql->num_rows() > 0) {
                        $Feedata = $sql->result_array();
                        $sql     = $this->db->query("select * from tbl_setting_installment where StatusID=2 and FeeID='" . $feeData[0]['FeeID'] . "'");
                        if ($sql->num_rows() > 0) {
                            $InstallmentData = $sql->result_array();
                        } else {
                            $InstallmentData = array();
                        }
                    } else {
                        $Feedata         = array();
                        $InstallmentData = array();
                    }
                    
                    $result[$k]['Feedata']         = $Feedata;
                    $result[$k]['InstallmentData'] = $InstallmentData;
                    //print_r($result);
                    
                    array_push($new_result, $result[$k]);
                }
               
            }
          }
            //print_r($new_result); die;
            
            return $fee_array['Data'] = $new_result;
        }
        return $result;
    }
    
    
    function getFees($EntityID, $FeeGUID)
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID    = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('F.*,C.CategoryName Stream,C.CategoryGUID ParentCategoryGUID,CB.CategoryGUID CategoryGUID ,CB.CategoryName Course,CB.Duration Duration');
        $this->db->from('tbl_setting_fee F');
        $this->db->join('tbl_entity E', 'F.FeeID=E.EntityID');
        $this->db->join('set_categories C', 'F.StreamID=C.CategoryID');
        $this->db->join('set_categories CB', 'F.CourseID=CB.CategoryID');
        //$this->db->join('tbl_setting_installment I','F.FeeID=I.FeeID');
        $this->db->where('E.CreatedByUserID', $InstituteID);
        $this->db->where('E.EntityTypeID', 13);
        $this->db->where('F.FeeGUID', $FeeGUID);
        
        $Query = $this->db->get();
        //echo $this->db->last_query(); die();
        if ($Query->num_rows() > 0) 
        {
            foreach ($Query->result_array() as $Record) 
            {                
                $this->db->select('*');
                $this->db->from('tbl_setting_installment');
                $this->db->where('FeeID', $Record['FeeID']);
                $this->db->where('StatusID', 2);
                $query = $this->db->get();
                 
                if ($query->num_rows() > 0) {
                    
                    foreach ($query->result_array() as $records) {
                        
                        $Recorded[] = $records;
                        
                    }
                    
                    $Record['installment'] = $Recorded;
                }
                
                $Records[] = $Record;
            }


            $FeeID = $this-> FeeID;
            $Records['FeeBreakupList'] = $this-> getFeeBreakupDetails($FeeID, $InstituteID);          
            
            
            $Return['Data'] = $Records;
            
            return $Return;
        }
        
        return false;
        
    }
    
    
    
    
    
    /*
    Description: 	Use to add new post
    */
    function addFee($EntityID, $Input = array(), $PostData = array())
    {
        $this->db->trans_start();
        $EntityGUID = get_guid();
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
      
        /* Add post to entity table and get EntityID. */
        $FeeID       = $this->Entity_model->addEntitys($EntityGUID, array(
            "EntityTypeID" => 13,
            "UserID" => $EntityID,
            "InstituteID" => $InstituteID,
            "Privacy" => @$Input["Privacy"],
            "StatusID" => 2,
            "Rating" => @$Input["Rating"]
        ));
        $InsertData  = array_filter(array(
            "FeeID" => $FeeID,
            "FeeGUID" => $EntityGUID,
            "StreamID" => $Input["StreamID"],
            "CourseID" => $Input["CourseID"],
            "NoMonth" => @$Input['NoMonth'],
            "Description" => @$Input['Description'],
            "FullAmount" => $_POST['single_fee'],
            "FullDiscountFee" => $_POST['final_amt'],
            "Discount" => @$Input['Discount'],
            "DiscountAmount" => @$Input['DiscountAmount']
        ));
        
        $this->db->insert('tbl_setting_fee', $InsertData);

        
        if (count($Input['NoOfInstallment']) > 0) 
        {
            foreach ($Input['NoOfInstallment'] as $key => $val) {
                if (!empty($Input['NoOfInstallment'][$key])) {
                    $installment = array(
                        'NoOfInstallment' => $Input['NoOfInstallment'][$key],
                        'Markup' => $Input['Markup'][$key],
                        'FeeID' => $FeeID,
                        "TotalFee" => $Input['TotalFee'][$key],
                        'InstallmentAmount' => $Input['InstallmentAmount'][$key]
                    );
                }

                if(!empty($installment)){
                    $this->db->insert('tbl_setting_installment', $installment);
                }
            }
        }
        
        
        if(isset($PostData["hdn"]) && !empty($PostData["hdn"]))
        {
            foreach($PostData["hdn"] as $fbid)
            {
                if(isset($PostData["fb_chk_".$fbid]) && $PostData["fb_chk_".$fbid] == 'on')
                {
                    if(isset($PostData["fb_amt_".$fbid]) && !empty($PostData["fb_amt_".$fbid]))
                    {
                        $amt = $netamt = $refund_per = "";
                        $tax = "";
                        $amt = $PostData["fb_amt_".$fbid];
                        if(isset($PostData["fb_tax_".$fbid]) && !empty($PostData["fb_tax_".$fbid]))
                        {
                            $tax = $PostData["fb_tax_".$fbid];

                            $netamt = round($amt + ($amt * $tax) / 100); 
                        }
                        else
                        {
                            $netamt = $amt;
                        }


                        if(isset($PostData["fb_refund_".$fbid]) && !empty($PostData["fb_refund_".$fbid]))
                        {
                            $refund_per = $PostData["fb_refund_".$fbid];
                        }    

                        $InsertData  = array(
                        "InstituteID" => $InstituteID,
                        "FeeID" => $FeeID,
                        "BreakTypeID" => $fbid,
                        "Amount" => $amt,
                        "Tax" => $tax,
                        "NetAmount" => $netamt,
                        "RefundPercent" => $refund_per
                        );

                        $this->db->insert('tbl_setting_fee_breakup', $InsertData);
                    }
                }
            }            
        }    
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return array(
            'PostID' => $FeeID,
            'PostGUID' => $EntityGUID
        );
    }
    
    
    
    function editFee($EntityID, $Input = array(), $PostData = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $this->db->select('CourseID');
        $this->db->where('CourseID', $Input["CourseID"]);
        $this->db->from('tbl_setting_fee');
        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            $row               = $query->row_array();
            $Input["StreamID"] = $row['ParentCategoryID'];
            
            
            /* Add post */
            $UpdateData = array_filter(array(
                "StreamID" => @$Input["StreamID"],
                "CourseID" => @$Input["CourseID"],
                "NoMonth" => @$Input['NoMonth'],
                "Description" => @$Input['Description'],
                "FullAmount" => $_POST['single_fee'],
                "FullDiscountFee" => $_POST['final_amt'],
                "Discount" => @$Input['Discount'],
                "DiscountAmount" => @$Input['DiscountAmount']
            ));
            
            $this->db->where('FeeID', $Input['FeeID']);
            $this->db->update('tbl_setting_fee', $UpdateData);

            $this->db->where('FeeID', $Input['FeeID']);
            $this->db->where('StatusID',2);
            $this->db->delete('tbl_setting_installment');

            if (count($Input['NoOfInstallment']) > 0) {
                foreach ($Input['NoOfInstallment'] as $key => $val) {
                    if (!empty($val)) {
                        $installment = array(
                            'NoOfInstallment' => $Input['NoOfInstallment'][$key],
                            'Markup' => $Input['Markup'][$key],
                            'TotalFee' => $Input['TotalFee'][$key],
                            'InstallmentAmount' => $Input['InstallmentAmount'][$key],
                            'FeeID' => $Input['FeeID'],
                        );
                        //print_r($installment);
                        $this->db->insert('tbl_setting_installment', $installment);

                    }
                }
            }

            
            $FeeID = $Input['FeeID'];
            if(isset($PostData["hdn"]) && !empty($PostData["hdn"]))
            {
                $this->db->where('InstituteID', $InstituteID);
                $this->db->where('FeeID', $FeeID);                
                $this->db->delete('tbl_setting_fee_breakup');

                foreach($PostData["hdn"] as $fbid)
                {
                    if(isset($PostData["fb_chk_".$fbid]) && $PostData["fb_chk_".$fbid] == 'on')
                    {
                        if(isset($PostData["fb_amt_".$fbid]) && !empty($PostData["fb_amt_".$fbid]))
                        {
                            $amt = $netamt = $refund_per = "";
                            $tax = "";
                            $amt = $PostData["fb_amt_".$fbid];
                            if(isset($PostData["fb_tax_".$fbid]) && !empty($PostData["fb_tax_".$fbid]))
                            {
                                $tax = $PostData["fb_tax_".$fbid];

                                $netamt = round($amt + ($amt * $tax) / 100);
                            }
                            else
                            {
                                $netamt = $amt;
                            } 


                            if(isset($PostData["fb_refund_".$fbid]) && !empty($PostData["fb_refund_".$fbid]))
                            {
                                $refund_per = $PostData["fb_refund_".$fbid];
                            }                             

                            $InsertData  = array(
                            "InstituteID" => $InstituteID,
                            "FeeID" => $FeeID,
                            "BreakTypeID" => $fbid,
                            "Amount" => $amt,
                            "Tax" => $tax,
                            "NetAmount" => $netamt,
                            "RefundPercent" => $refund_per);

                            $this->db->insert('tbl_setting_fee_breakup', $InsertData);
                        }
                    }
                }            
            }
            
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    
    /*
    Description: 	Use to delete post by owner
    */
    function deleteInstallment($UserID, $Installment)
    {
        $this->db->delete('tbl_setting_installment', $Installment);
        return TRUE;
    }

    /*delete fee*/
    function delete($FeeID){
        // $this->db->select("CourseID");
        // $this->db->from("tbl_setting_fee");
        // $this->db->where("FeeID",$FeeID);
        // $this->db->limit(1);
        // $data = $this->db->get();
        // if($data->num_rows() > 0){
        //     $data = $this->db->result_array();
        // }

        /*$this->db->where("FeeID",$FeeID);
        $this->db->update("tbl_setting_fee", array(
            "FullAmount" => 0,
            "NoMonth" => 0,
            "Description" => "",
            "Discount" => 0,
            "DiscountAmount" => 0,
            "FullDiscountFee" => 0
        ));*/

        $this->db->where("FeeID",$FeeID);
        $this->db->update("tbl_setting_installment", array(
            "StatusID" => 3));


        $this->db->where("EntityID",$FeeID);
        $this->db->update("tbl_entity",array("StatusID"=>3));
        return TRUE;
    } 

    /*delete fee*/
    function Installmentremove($InstallmentID){

       $this->db->where("InstallmentID", $InstallmentID);
        $this->db->update("tbl_setting_installment", array(
            "StatusID" => 3
        ));
        return TRUE;
    }


    function addFeeBreakup($Input = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->trans_start();

        $this->db->select('FeeBreakupID');
        $this->db->from('tbl_fee_breakup');
        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('BreakTypeID=', $Input['fee_head_name']);
        $this->db->where('StatusID', 2);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) 
        {            
            return 'Exist'; die;
        }

        if(!isset($Input['fee_is_taxable']) || empty($Input['fee_is_taxable'])) $Input['fee_is_taxable'] = 0;
        if(!isset($Input['fee_is_refundable']) || empty($Input['fee_is_refundable'])) $Input['fee_is_refundable'] = 0;
        
        $InsertData = (array(               
        "InstituteID" =>    $InstituteID,
        "BreakTypeID" => @$Input['fee_head_name'],        
        "IsTaxable" => @$Input['fee_is_taxable'],
        "TaxablePercent" =>   @$Input['fee_taxable_percent'],
        "IsRefundable" => @$Input['fee_is_refundable'],
        "RefundablePercent" => @$Input['fee_refundable_percent'],     
        "SaveDate"  =>  date('Y-m-d H:i:s')));

        $this->db->insert('tbl_fee_breakup', $InsertData);
        
        $ID = $this->db->insert_id();       
        
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }

        return $ID;     
    }

    function feeBreakupListing()
    {
        $Records = array();

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $sql = "SELECT fb.FeeBreakupID, fbn.BreakTypeID, fbn.BreakupName, IF(fb.IsTaxable=1,'Yes',IF(fb.IsTaxable=0,'No','')) as IsTaxable, fb.TaxablePercent, IF(fb.IsRefundable=1,'Yes',IF(fb.IsRefundable=0,'No','')) as IsRefundable, fb.RefundablePercent, DATE_FORMAT(fb.SaveDate, '%d-%M-%Y') as SaveDate
        FROM tbl_fee_breakup fb
        INNER JOIN tbl_fee_breakup_name fbn ON fbn.BreakTypeID = fb.BreakTypeID AND fbn.InstituteID = fb.InstituteID       
        WHERE fb.InstituteID = $InstituteID AND fb.StatusID = 2
        ORDER BY fbn.BreakupName, fb.FeeBreakupID";

        $Query = $this->db->query($sql);
        
        $Return['Data']['TotalRecords'] = 0;        
        
        if($Query->num_rows()>0)
        {
            $Return['Data']['TotalRecords'] = $Query->num_rows();           

            foreach($Query->result_array() as $Record)
            {                
                $Records[] = $Record;
            }           
        }

        $Return['Data']['Records'] = $Records;

        return $Return;
    }

    function feeBreakupDelete($Inputs)
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $fid = $Inputs['fid'];

        $this->db->select('FeeBreakupID');
        $this->db->from('tbl_fee_breakup');
        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('FeeBreakupID=', $fid);
        $this->db->where('StatusID', 2);
        $query = $this->db->get();        
        if ($query->num_rows() <= 0) 
        {            
            return 'NotExist'; die;
        }

        $this->db->where("InstituteID", $InstituteID);                
        $this->db->where('FeeBreakupID', $fid);
        $this->db->update('tbl_fee_breakup', array("StatusID" => 3));        
        return $this->db->affected_rows();            
    }




    function addBreakup($Input = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $BreakTypeID = $Input['breakup_head_id'];

        $this->db->trans_start();

        $this->db->select('BreakTypeID');
        $this->db->from('tbl_fee_breakup_name');
        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('BreakTypeID != "'.$BreakTypeID.'"');
        $this->db->where("BreakupName = '".$Input['breakup_head_name']."'");        
        $query = $this->db->get();        
        if ($query->num_rows() > 0) 
        {            
            return 'Exist'; die;
        }        
        
        if($BreakTypeID > 0)
        {
            $UpdateData = (array(                           
            "BreakupName" => @$Input['breakup_head_name'],
            "UpdateDate"  =>  date('Y-m-d H:i:s')
            ));

            $this->db->where('BreakTypeID', $BreakTypeID);
            $this->db->where('InstituteID', $InstituteID);
            $this->db->update('tbl_fee_breakup_name', $UpdateData);
            
            $ID = $this->db->affected_rows(); 
        }
        else
        {
            $InsertData = (array(               
            "InstituteID" =>    $InstituteID,
            "BreakupName" => @$Input['breakup_head_name'],           
            "SaveDate"  =>  date('Y-m-d H:i:s'),
            "UpdateDate"  =>  date('Y-m-d H:i:s')
            ));
            $this->db->insert('tbl_fee_breakup_name', $InsertData);        
            $ID = $this->db->insert_id();       
        }

        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }

        return $ID;     
    }

    function breakupListing()
    {
        $Records = array();

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $sql = "SELECT BreakTypeID, BreakupName, DATE_FORMAT(SaveDate, '%d-%M-%Y') as SaveDate
        FROM tbl_fee_breakup_name        
        WHERE InstituteID = $InstituteID
        ORDER BY BreakupName, BreakTypeID";

        $Query = $this->db->query($sql);
        
        $Return['Data']['TotalRecords'] = 0;        
        
        if($Query->num_rows()>0)
        {
            $Return['Data']['TotalRecords'] = $Query->num_rows();           

            foreach($Query->result_array() as $Record)
            {                
                $Records[] = $Record;
            }           
        }

        $Return['Data']['Records'] = $Records;

        return $Return;
    }


    function getFeeBreakupDetails($FeeID, $InstituteID)
    {
        $data = array();

        $sql = "SELECT fb.FeeBreakupID, fbn.BreakTypeID, fbn.BreakupName, IF(fb.IsTaxable=1,'Yes',IF(fb.IsTaxable=0,'No','')) as IsTaxable, fb.TaxablePercent, fbs.Amount, fbs.Tax, fbs.NetAmount, fb.RefundablePercent, fbs.RefundPercent
        FROM tbl_fee_breakup fb
        INNER JOIN tbl_fee_breakup_name fbn ON fbn.BreakTypeID = fb.BreakTypeID AND fbn.InstituteID = fb.InstituteID 
        LEFT JOIN tbl_setting_fee_breakup fbs ON fbs.BreakTypeID = fb.BreakTypeID AND fbs.FeeID = $FeeID AND fb.InstituteID = fbs.InstituteID 
        WHERE fb.InstituteID = $InstituteID AND fb.StatusID = 2
        ORDER BY fbn.BreakupName, fbn.BreakTypeID";

        $Query = $this->db->query($sql);                    
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                $data[] = $Record;
            }                     
        } 

        return $data;
    }


    /*--------------------------------------------------------
    Before due date settings
    ---------------------------------------------------------*/
    function saveSettingsBeforeDueDate($PostData = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('RemindWhen', 'before');
        $this->db->where('RemindPhaseType', 'nophase');                      
        $this->db->delete('tbl_fee_reminders');


        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('RemindWhen', 'before');
        $this->db->where('RemindPhaseType', 'nophase');                      
        $this->db->delete('tbl_fee_reminders_days');

                                                     

        $InsertData  = array(
        "InstituteID" => $InstituteID,
        "EmailDaysCount" => $PostData['bf_email_days'],
        "EmailContent" => $PostData['bf_email_content'],        
        "SmsDaysCount" => $PostData['bf_sms_days'],
        "SmsContent" => $PostData['bf_sms_content'],
        "SaveDate" => date("Y-m-d H:i:s"));

        $this->db->insert('tbl_fee_reminders', $InsertData); 

        $ID = $this->db->insert_id();

        foreach($PostData["bf_email_remind_days"] as $d)
        {
            $InsertData  = array(
            "InstituteID" => $InstituteID,
            "RemindFeeSettingID" => $ID,
            "RemindFor" => 'email',
            "OnDays" => $d);

            $this->db->insert('tbl_fee_reminders_days', $InsertData); 
        } 

        foreach($PostData["bf_sms_remind_days"] as $d)
        {
            $InsertData  = array(
            "InstituteID" => $InstituteID,
            "RemindFeeSettingID" => $ID,
            "RemindFor" => 'sms',
            "OnDays" => $d);

            $this->db->insert('tbl_fee_reminders_days', $InsertData); 
        }   
            
        return TRUE;        
    }


    function getBeforeDueDateSetting()
    {
        $Return['Data']['BFMaster'] = $Return['Data']['BFEmailDays'] = $Return['Data']['BFSmsDays'] = array();

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $sql = "SELECT EmailDaysCount, EmailContent, SmsDaysCount, SmsContent
        FROM tbl_fee_reminders        
        WHERE InstituteID = $InstituteID AND RemindWhen = 'before' AND RemindPhaseType = 'nophase'
        LIMIT 1";

        $Query = $this->db->query($sql);       
        
        if($Query->num_rows()>0)
        {
            $Return['Data']['BFMaster'] = $Query->result_array();
            $Return['Data']['BFMaster'] = $Return['Data']['BFMaster'][0];

            $sql = "SELECT OnDays
            FROM tbl_fee_reminders_days        
            WHERE InstituteID = $InstituteID AND RemindWhen = 'before' AND RemindPhaseType = 'nophase' AND RemindFor = 'email' ";
            $Query = $this->db->query($sql); 
            if($Query->num_rows()>0)
            {
                $temp = array();
                foreach($Query->result_array() as $Record)
                {                
                    $temp[] = $Record['OnDays']; 
                }                 
                $Return['Data']['BFEmailDays'] = $temp;          
            }


            $sql = "SELECT OnDays
            FROM tbl_fee_reminders_days        
            WHERE InstituteID = $InstituteID AND RemindWhen = 'before' AND RemindPhaseType = 'nophase' AND RemindFor = 'sms' ";
            $Query = $this->db->query($sql); 
            if($Query->num_rows()>0)
            {
                $temp = array();
                foreach($Query->result_array() as $Record)
                {                
                    $temp[] = $Record['OnDays']; 
                }                 
                $Return['Data']['BFSmsDays'] = $temp;          
            }
            
        }        

        return $Return;
    }


    /*--------------------------------------------------------
    After due date settings
    ---------------------------------------------------------*/
    function saveSettingsAfterDueDate($PostData = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $formID = $PostData["formID"];


        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('RemindWhen', 'after');
        $this->db->where('RemindPhaseType', 'phase'.$formID);                      
        $this->db->delete('tbl_fee_reminders');


        $this->db->where('InstituteID', $InstituteID);
        $this->db->where('RemindWhen', 'after');
        $this->db->where('RemindPhaseType', 'phase'.$formID);                      
        $this->db->delete('tbl_fee_reminders_days');
                                                     

        $InsertData  = array(
        "InstituteID" => $InstituteID,
        "RemindWhen" => 'after',
        "RemindPhaseType" => 'phase'.$formID,
        "EmailDaysCount" => $PostData['af_email_days'.$formID],
        "EmailContent" => $PostData['af_email_content'.$formID],        
        "SmsDaysCount" => $PostData['af_sms_days'.$formID],
        "SmsContent" => $PostData['af_sms_content'.$formID],
        "SaveDate" => date("Y-m-d H:i:s"));

        if($formID == 1)
        {
            $InsertData['RemindToParent'] = @$PostData['af_alert_p'.$formID];                      
        }
        elseif($formID == 2)
        {
            $InsertData['RemindToParent'] = @$PostData['af_alert_p'.$formID]; 
            $InsertData['RemindToFaculty'] = @$PostData['af_alert_f'.$formID];           
        }
        elseif($formID == 3)
        {
            $InsertData['RemindToParent'] = @$PostData['af_alert_p'.$formID]; 
            $InsertData['RemindToFaculty'] = @$PostData['af_alert_f'.$formID];
            $InsertData['RemindToManagement'] = @$PostData['af_alert_m'.$formID];
        }

        $this->db->insert('tbl_fee_reminders', $InsertData); 

        $ID = $this->db->insert_id();

        foreach($PostData["af_email_remind_days".$formID] as $d)
        {
            $InsertData  = array(
            "InstituteID" => $InstituteID,
            "RemindWhen" => 'after',
            "RemindPhaseType" => 'phase'.$formID,
            "RemindFor" => 'email',
            "RemindFeeSettingID" => $ID,            
            "OnDays" => $d);

            $this->db->insert('tbl_fee_reminders_days', $InsertData); 
        } 

        foreach($PostData["af_sms_remind_days".$formID] as $d)
        {
            $InsertData  = array(
            "InstituteID" => $InstituteID,
            "RemindWhen" => 'after',
            "RemindPhaseType" => 'phase'.$formID,
            "RemindFor" => 'sms',
            "RemindFeeSettingID" => $ID,            
            "OnDays" => $d);

            $this->db->insert('tbl_fee_reminders_days', $InsertData); 
        }   
            
        return TRUE;        
    }


    function getAfterDueDateSetting()
    {
        $Return['Data']['AFMaster'] = $Return['Data']['AFEmailDays'] = $Return['Data']['AFSmsDays'] = array();

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $sql = "SELECT RemindPhaseType, EmailDaysCount, EmailContent, SmsDaysCount, SmsContent, RemindToStudent, RemindToParent, RemindToFaculty, RemindToManagement
        FROM tbl_fee_reminders        
        WHERE InstituteID = $InstituteID AND RemindWhen = 'after'
        ";

        $Query = $this->db->query($sql);       
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                $temp[$Record['RemindPhaseType']] = $Record; 
            } 

            $Return['Data']['AFMaster'] = $temp;

            

            $sql = "SELECT RemindPhaseType, OnDays
            FROM tbl_fee_reminders_days        
            WHERE InstituteID = $InstituteID AND RemindWhen = 'after' AND RemindFor = 'email' ";
            $Query = $this->db->query($sql); 
            if($Query->num_rows()>0)
            {
                $temp = array();
                foreach($Query->result_array() as $Record)
                {                
                    $temp[$Record['RemindPhaseType']][] = $Record['OnDays']; 
                }                 
                $Return['Data']['AFEmailDays'] = $temp;          
            }


            $sql = "SELECT RemindPhaseType, OnDays
            FROM tbl_fee_reminders_days        
            WHERE InstituteID = $InstituteID AND RemindWhen = 'after' AND RemindFor = 'sms' ";
            $Query = $this->db->query($sql); 
            if($Query->num_rows()>0)
            {
                $temp = array();
                foreach($Query->result_array() as $Record)
                {                
                    $temp[$Record['RemindPhaseType']][] = $Record['OnDays']; 
                }                 
                $Return['Data']['AFSmsDays'] = $temp;          
            }
            
        }        

        return $Return;
    }
}