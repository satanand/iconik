<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('Products_model');
	}


	/*------------------------------------------------------------------------
	Manage Category
	------------------------------------------------------------------------*/
	public function getCategoryList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getCategoryList($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}

	/*------------------------------------------------------------------------
	Manage Product
	------------------------------------------------------------------------*/
	public function add_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
			

		$this->form_validation->set_rules('ProductCategory', 'ProductCategory','trim|required');
		$this->form_validation->set_rules('ProductSubCategory', 'ProductSubCategory','trim|required');
		$this->form_validation->set_rules('IsDownlodable', 'Is Downlodable','trim');
		$this->form_validation->set_rules('ProductName', 'Product Name','trim|required');
		$this->form_validation->set_rules('ProductPrice', 'Unit Price','trim|required');
		$this->form_validation->set_rules('ProductQty', 'Quantity','trim|required');
		$this->form_validation->set_rules('MaxQtyCanPurchase', 'Max. Qty. Each User Can Purchase','trim|required');	
		
		$this->form_validation->set_rules('Tax', 'Tax','trim');
		$this->form_validation->set_rules('ShippingCost', 'ShippingCost','trim');		
		$this->form_validation->set_rules('ProductDesc', 'Description','trim|required');
		$this->form_validation->set_rules('ProductFeatures', 'ProductFeatures','trim');

		$this->form_validation->set_rules('MediaGUIDs', 'Product Image','trim|required');
		

		if($this->Post['ProductCategory'] == 2 || $this->Post['ProductCategory'] == 10 || $this->Post['ProductCategory'] == 12)
		{
			$this->Post['ShippingCost'] = "";
			$this->Post['ProductQty'] = "unlimited";
			$this->Post['IsDownlodable'] = 1;			
			$this->form_validation->set_rules('MediaGUIDss', 'Attach Product File','trim|required');
		}
		else
		{			
			$this->Post['IsDownlodable'] = 0;
			$this->Post['MediaGUIDss'] = "";
			$this->form_validation->set_rules('ShippingCost', 'Shipping Cost','trim|required');
		}

		if($this->Post['ProductCategory'] == 13)
		{
			$this->form_validation->set_rules('NoOfPages', 'No. Of Pages','trim|required');		
			$this->form_validation->set_rules('PublisherName', 'Publisher Name','trim|required');
			$this->form_validation->set_rules('AuthorName', 'Author Name','trim|required');
			$this->form_validation->set_rules('Language', 'Language','trim|required');
		}


		$this->form_validation->validation($this);  /* Run validation */

		$this->Post['ProductPriceMRP'] = $this->Post['ProductPrice'];
		if(isset($this->Post['Tax']) && !empty($this->Post['Tax']))
		{
			if($this->Post['Tax'] < 0 && $this->Post['Tax'] > 100)
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter tax between 0 to 100.";
				die;
			}
			else
			{
				$this->Post['ProductPriceMRP'] = round($this->Post['ProductPrice'] + ($this->Post['ProductPrice'] * $this->Post['Tax']) / 100);
			}	
		}


		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->add($this->EntityID, $this->Post);

		if($Data === "Exist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Product name already exist in selected product subcategory.";
		}
		else 
		{
            $this->Return['Message']      	=	"Product added successfully."; 
		}
	}


	
	public function getProducts_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterStartDate', 'Start Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Created End Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getProducts($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}


	public function getProductsBySubCategory_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ProductsSubCategory[]', 'Products SubCategory', 'trim');		
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->getProductsBySubCategory($this->EntityID, $this->Post);
		if($Data)
		{
			$this->Return['Data']      	=	$Data; 
		}
	}



	public function edit_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
			

		$this->form_validation->set_rules('ProductCategory', 'ProductCategory','trim|required');
		$this->form_validation->set_rules('ProductSubCategory', 'ProductSubCategory','trim|required');
		$this->form_validation->set_rules('IsDownlodable', 'Is Downlodable','trim');
		$this->form_validation->set_rules('ProductName', 'Product Name','trim|required');
		$this->form_validation->set_rules('ProductPrice', 'Unit Price','trim|required');		
		$this->form_validation->set_rules('MaxQtyCanPurchase', 'Max. Qty. Each User Can Purchase','trim|required');	
		
		$this->form_validation->set_rules('IncreaseProductQty', 'Increase Stock Quantity','trim|required');
		$this->form_validation->set_rules('ShippingCost', 'ShippingCost','trim');		
		$this->form_validation->set_rules('ProductDesc', 'Description','trim|required');
		$this->form_validation->set_rules('ProductFeatures', 'ProductFeatures','trim');

		$this->form_validation->set_rules('MediaGUIDs', 'Product Image','trim|required');
		

		if($this->Post['ProductCategory'] == 2 || $this->Post['ProductCategory'] == 10 || $this->Post['ProductCategory'] == 12)
		{
			$this->Post['ShippingCost'] = "";
			$this->Post['ProductQty'] = "unlimited";
			$this->Post['IsDownlodable'] = 1;			
			$this->form_validation->set_rules('MediaGUIDss', 'Attach Product File','trim|required');
		}
		else
		{			
			$this->Post['IsDownlodable'] = 0;
			$this->Post['MediaGUIDss'] = "";
			$this->form_validation->set_rules('ShippingCost', 'Shipping Cost','trim|required');
		}

		if($this->Post['ProductCategory'] == 13)
		{
			$this->form_validation->set_rules('NoOfPages', 'No. Of Pages','trim|required');		
			$this->form_validation->set_rules('PublisherName', 'Publisher Name','trim|required');
			$this->form_validation->set_rules('AuthorName', 'Author Name','trim|required');
			$this->form_validation->set_rules('Language', 'Language','trim|required');
		}

	
		$this->form_validation->validation($this);  /* Run validation */		


		$this->Post['ProductPriceMRP'] = $this->Post['ProductPrice'];
		if(isset($this->Post['Tax']) && !empty($this->Post['Tax']))
		{
			if($this->Post['Tax'] < 0 && $this->Post['Tax'] > 100)
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Please enter tax between 0 to 100.";
				die;
			}
			else
			{
				$this->Post['ProductPriceMRP'] = round($this->Post['ProductPrice'] + ($this->Post['ProductPrice'] * $this->Post['Tax']) / 100);
			}	
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->edit($this->EntityID, $this->Post);
		
		if($Data === "NotExist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Product details not found in the system.";
		}
		elseif($Data === "Exist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Product name already exist in selected product subcategory.";
		}
		elseif($Data === true)
		{
			$this->Return['Message']      	=	"Product updated successfully.";
		} 
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";			
		}
	}

	

	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ProductID', 'ProductID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->delete($this->EntityID, $this->Post);
	

		if($Data === "NotExist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Product details not found in the system.";
		}
		elseif($Data === true)
		{
			$this->Return['Message']      	=	"Product deleted successfully.";
		} 
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";
			
		}
	}


	public function importProduct_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->validation($this);		

		$MediaURL = explode("/", $this->input->post('MediaURL'));
		$mediaName = end($MediaURL);

		require_once APPPATH . "/third_party/PHPExcel/PHPExcel.php";
		$path = 'uploads/storeproducts/';
		$inputFileName = $path.$mediaName;

		try
		{		
			$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	        $objPHPExcel = $objReader->load($inputFileName);
	        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
	        $flag = true;
	        $i=0;
			
			//echo "<pre>";print_r($allDataInSheet);

			if(isset($allDataInSheet) && !empty($allDataInSheet) && count($allDataInSheet) > 3)
			{	
				$len = count($allDataInSheet) - 3;
				$total_rows = $incounter = $duplicate = 0;
				for($i=4; $i<=$len; $i++)
				{
					$CategoryID = trim($allDataInSheet[$i]['A']);
					$SubCategoryName = trim($allDataInSheet[$i]['B']);
					$ProductName = trim($allDataInSheet[$i]['C']);
					$ProductDesc = trim($allDataInSheet[$i]['D']);
					$ProductFeature = trim($allDataInSheet[$i]['E']);
					$ProductPrice = trim($allDataInSheet[$i]['F']);
					$Tax = trim($allDataInSheet[$i]['G']);
					$ProductQty = trim($allDataInSheet[$i]['H']);
					$MaxQtyCanPurchase = trim($allDataInSheet[$i]['I']);
					$ShippingCost = trim($allDataInSheet[$i]['J']);

					if(isset($CategoryID) && !empty($CategoryID) && isset($SubCategoryName) && !empty($SubCategoryName)  && isset($ProductName) && !empty($ProductName) && isset($ProductPrice) && !empty($ProductPrice) && isset($MaxQtyCanPurchase) && !empty($MaxQtyCanPurchase) && isset($Tax) && !empty($Tax))
					{
						$total_rows++;

						$ProductPriceMRP = 0;
						$ProductPriceMRP = round($ProductPrice + (($ProductPrice * $Tax) / 100));
						$IsDownlodable = 0;
						if($CategoryID == 2 || $CategoryID == 10 || $CategoryID == 12)
						{
							$ShippingCost = "";
							$ProductQty = "unlimited";
							$IsDownlodable = 1;
						}

						if(!isset($ProductQty) || empty($ProductQty)) $ProductQty = 1;
						if(!isset($Tax) || empty($Tax)) $Tax = 0;
						if(!isset($MaxQtyCanPurchase) || empty($MaxQtyCanPurchase)) $MaxQtyCanPurchase = 1;
						
						$inserdata = array();
						$inserdata = array(										
							"ProductName"=>$ProductName,			
							"ProductDesc"=>$ProductDesc,
							"ProductPrice"=>$ProductPrice,
							"ProductQty"=>$ProductQty,
							"IsDownlodable"=>$IsDownlodable,
							"MaxQtyCanPurchase"=>$MaxQtyCanPurchase,
							"ShippingCost"=>$ShippingCost,
							"MediaGUIDs"=>"",
							"MediaGUIDss"=>"",
							"Tax"=>$Tax,
							"ProductCategory"=>$CategoryID,							
							"ProductSubCategory"=>$SubCategoryName,
							"ProductFeatures"=>$ProductFeature,										
							"ProductPriceMRP"=>$ProductPriceMRP
						);	

						$Data = $this->Products_model->add($this->EntityID, $inserdata);

						if($Data === "Exist")
						{
							$duplicate++;
						}
						else 
						{
				            $incounter++;				            
						}					
					}
				}

				$this->Return['Message'] = "Details:<br/>Total Rows = $total_rows<br/>Total Duplicate = $duplicate<br/>Total Newly Added = $incounter";
			}
			else
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Either file is empty or you are trying to upload wrong format of file. Please check and try again.";
				die;
			}

		}
		catch (Exception $e)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Error occur ".$e->getMessage();
			die;
		}		
	}

	/*------------------------------------------------------------------------
	Manage Coupons
	------------------------------------------------------------------------*/
	public function coupons_add_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CouponsCode', 'Name','trim|required');		
		$this->form_validation->set_rules('ExpiryDate', 'Expiry Date','trim|required');		
		$this->form_validation->set_rules('Discount', 'Discount','trim');		
		$this->form_validation->set_rules('ProductsInclude[]', 'Applicable on Products','trim|required');

		$this->form_validation->validation($this);  /* Run validation */		
				
		
		if($this->Post['Discount'] < 0 || $this->Post['Discount'] > 100)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Please enter discount percentage between 0 to 100.";
			die;
		}	


		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->coupons_add($this->EntityID, $this->Post);

		if($Data === "Exist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Discount name already exist.";
		}
		elseif($Data === "CouponOnProductExist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Discount is already applied on some selected product. Please select other products.";
		}
		else 
		{
            $this->Return['Message']      	=	"Discount added successfully."; 
		}
	}


	
	public function getCoupons_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterStartDate', 'Start Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Created End Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Products_model->getCoupons($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}



	public function coupons_edit_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CouponsID', 'Coupons ID','trim|required');		
		$this->form_validation->set_rules('ExpiryDate', 'Expiry Date','trim|required');		
		$this->form_validation->set_rules('Discount', 'Discount','trim');		
		$this->form_validation->set_rules('ProductsInclude[]', 'Applicable on Products','trim|required');

		$this->form_validation->validation($this);  /* Run validation */		
				
		if($this->Post['Discount'] < 0 || $this->Post['Discount'] > 100)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Please enter discount percentage between 0 to 100.";
			die;
		}		
		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->coupons_edit($this->EntityID, $this->Post);

		if($Data === "Exist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Discount name already exist.";
		}
		elseif($Data === "CouponOnProductExist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Product already having different coupon. Please remove selected product and select other products.";
		}
		else 
		{
            $this->Return['Message']      	=	"Discount updated successfully."; 
		}	
	}

	

	public function coupons_delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CouponsID', 'CouponsID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Data = $this->Products_model->coupons_delete($this->EntityID, $this->Post);
	

		if($Data === "NotExist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Discount details not found in the system.";
		}
		elseif($Data === true)
		{
			$this->Return['Message']      	=	"Discount deleted successfully.";
		} 
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";
			
		}
	}


	/*------------------------------------------------------------------------
	Manage Orders
	------------------------------------------------------------------------*/
	public function getOrders_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterStartDate', 'Start Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Created End Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getOrders($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}

	public function getOrdersDetails_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getOrdersDetailsInstitute($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}


	public function getInvoiceDetails_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getInvoiceDetails($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data; 
		}
	}


	public function getSoldProductDetails_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ProductID', 'Product ID', 'trim|required');		
		$this->form_validation->validation($this);	

		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getSoldProductDetails($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}
}