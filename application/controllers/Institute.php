<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Institute extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',	

			'asset/js/excelexportjs.js',			

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js'

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"My Business" => "",
			"Manage Customer" => ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('institute/institute_list', $load['breadcrumb']);

		$this->load->view('includes/footer');

	}







}

