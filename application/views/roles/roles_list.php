<style type="text/css">
	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}
</style>
<header class="panel-heading"> <h1 class="h4">Manage Roles & Authority</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" id="panel-body" ng-controller="PageController" ng-init="getFilterData()"><!-- Body -->



<div class="row clearfix mt-2 mb-2">
<div class="col-md-9">
	<span class="float-left records d-none d-sm-block">
		<span class="h5">Total Records: {{data.totalRecords}}</span>
	</span>
</div>
<div class="col-md-3"  style="float: right;">
	<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Role</button>		
</div>
</div>


<form class="" id="filterForm" role="form" autocomplete="off" > <!--ng-submit="applyFilter()"-->
<div class="row clearfix mt-2 mb-2">		
<div class="col-md-3">
	<div class="form-group">
		<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Enter search word" autocomplete="off">
	</div>
</div>		

<div class="col-md-3">
	<div class="form-group">
		<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
	<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
	</div>
</div>
</div>
</form>	








	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th>Role</th>

					<th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">

					<td>
						<strong>{{row.UserTypeName | capitalizeFirstLetter}}</strong>

					</td>

					

					<td class="text-center" style="text-align: center;">

						<!-- <div class="dropdown">

							<button class="btn btn-secondary  btn-sm action" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8230;</button>

							<div class="dropdown-menu dropdown-menu-left"> -->
								<a class=" glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.UserTypeID)" data-toggle="tooltip" title="View"></a>
								<!-- <a class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEdit(key, row.UserTypeID)" data-toggle="tooltip" title="Edit"></a> -->

								<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-lock" href="" ng-click="loadFormPermission(key, row.UserTypeID)" title="Permission"> </a>

								<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class=" glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.UserTypeID)" data-toggle="tooltip" title="Delete"></a>

							<!-- </div>

						</div> -->

					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->


	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Roles</h3>

					   	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>
				<div  ng-include="templateURLAdd"></div> 

			</div>

		</div>

	</div>

	<!-- edit Modal -->
	<div class="modal fade" id="edit_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

	<!-- view Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div  ng-include="templateURLView"></div> 
				<!-- /form -->
			</div>
		</div>
	</div>

<!-- permission form -->

<div class="modal fade" id="permission_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Permission </h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="permission_form" name="permission_form" autocomplete="off" ng-include="templateURLPermission" method="POST">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

</div><!-- Body/ -->







