<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questionassign  extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

			'asset/plugins/timepicker/jquery.timepicker.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',		

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.min.js',

			'asset/plugins/timepicker/jquery.timepicker.min.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Question Bank" =>  "",
			"Assign Test" =>  ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionAssign/Question_assign');

		$this->load->view('includes/footer');

	}



	public function quiz()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

			'asset/plugins/timepicker/jquery.timepicker.css'

		);

		$load['js']=array(

			'asset/js/questionassign.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',		

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.min.js',

			'asset/plugins/timepicker/jquery.timepicker.min.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Question Bank" =>  "",
			"Assign quiz" =>  ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionAssign/Quiz_assign');

		$this->load->view('includes/footer');

	}



	public function list()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

			'asset/plugins/timepicker/jquery.timepicker.css'

		);

		$load['js']=array(

			'asset/js/questionassign.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',		

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.min.js',

			'asset/plugins/timepicker/jquery.timepicker.min.js'


		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Assign Test" => base_url()."questionassign",
			"Tests" =>  ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionAssign/Questionassign_list');

		$this->load->view('includes/footer');

	}



	public function Paper()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

		);

		$load['js']=array(

			'asset/js/questionassign.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',		

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.min.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Assign Test" => base_url()."questionassign",
			"Tests" =>  $_SERVER['HTTP_REFERER'],
			"Results" => ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionAssign/Questionassign_details');

		$this->load->view('includes/footer');

	}


	public function results(){
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/questionassign.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',		

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js',

			'asset/js/excelexportjs.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Assign Test" => base_url()."questionassign",
			"Tests" =>  $_SERVER['HTTP_REFERER'],
			"Results" => ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionAssign/Results');

		$this->load->view('includes/footer');
	}



	public function result_paper(){
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/questionassign.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',		

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Assign Test" => base_url()."questionassign",
			"Results" =>  $_SERVER['HTTP_REFERER'],
			"Detail" => ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionAssign/Result_paper');

		$this->load->view('includes/footer');
	}


	public function result_export_pdf()
	{
		$data['listing_data'] = array();
		
		if(isset($_GET['QtPaperGUID']) && !empty($_GET['QtPaperGUID']) && isset($_SESSION['UserData']['SessionKey']) && !empty($_SESSION['UserData']['SessionKey']))
		{
			$JSON = json_encode(array("QtPaperGUID" => $_GET['QtPaperGUID'], "SessionKey" => $_SESSION['UserData']['SessionKey'], "CheckPaperData" => "Yes"));
			
			$Response = APICall(API_URL.'questionpaper/getTestResults', $JSON);				
			
			$data['listing_data'] = $Response;
			$data['session_data'] = $_SESSION['UserData'];
			
			if(isset($Response['ResponseCode']) && $Response['ResponseCode'] == 200)
			{				
				$header_title = ucfirst($data['listing_data']['Data'][0]['CommonDetails']['QtPaperTitle']);	
				$data['header_title'] = $header_title;

				$html = $this->load->view('questionAssign/Results_pdf', $data, true);

				$pdfFilePath = $header_title."_".date('d-M-Y').".pdf";
		        
		        $this->load->library('m_pdf');
		        
		 		$this->m_pdf->pdf->autoScriptToLang = true;
				$this->m_pdf->pdf->baseScript = 1;
				$this->m_pdf->pdf->autoVietnamese = true;
				$this->m_pdf->pdf->autoLangToFont = true;

		 		$this->m_pdf->pdf->setFooter($header_title." (".'{PAGENO}'.")");
		       	
		        $this->m_pdf->pdf->WriteHTML($html);		 
		        
		        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
			}
			else
			{
				if(isset($Response['Message']))
					$msg = $Response['Message'];
				else
					$msg = "No record found.";

				echo "<script> alert('".$msg."'); window.history.go(-1); </script>";
				die;
			}
		}
	}

}