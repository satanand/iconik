app.controller('PageController', function($scope, $http, $timeout) {
    $scope.data.pageSize = 100;
    $scope.data.dataList = [];
    $scope.data.filterDataA = [];
    $scope.data.filterDataC = [];
    $scope.data.CategoryData = [];
    $scope.Category = [];
    $scope.Categorys = [];

    $scope.format = 'd/m/yy h:mm:ss a';
    //$filter('date')($scope.currDate, "dd/MM/yyyy");

    /*----------------*/

    $scope.showStatus = function() {
        var selected = [];
        angular.forEach($scope.statuses, function(s) {
            if ($scope.user.status.indexOf(s.value) >= 0) {
                selected.push(s.text);
            }
        });
        return selected.length ? selected.join(', ') : 'Not set';
    }

    $scope.getFilterData = function(Category = "") {
        $scope.getCategory();
        $scope.getSecondLevelPermission();
        $scope.getStaffList();
    }

    /*get user NAme*/
    $scope.getStaffList = function()
    {

        var data = 'SessionKey=' + SessionKey;
        $scope.data.UserData = [];
        $http.post(API_URL + 'Event/getStaffList', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data) {
                /* success case */


                for (var i in response.Data) {

                    $scope.data.UserData.push(response.Data[i]);
                }

              
            }

        });
    }

    /*get set category book*/
    $scope.getCategory = function()
    {

        var data = 'SessionKey=' + SessionKey;
        $scope.data.CategoryData = [];
        $http.post(API_URL + 'Event/getCategory', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */


                for (var i in response.Data.Records) {

                    $scope.data.CategoryData.push(response.Data.Records[i]);
                }

                $timeout(function() {

                    $("select.chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8
                    }).trigger("chosen:updated");

                }, 200);
            }

        });
    }


    /*list*/
    $scope.applyFilter = function() {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
        $scope.getFilterData();
    }



    $scope.applytinymce = function() {
        tinymce.init({
            selector: '#EventDescription',
            setup: function(editor) {
                editor.on('keyup', function(e) {
                    console.log('edited. Contents: ' + editor.getContent());
                    formDataCheck(editor.getContent());
                });
            }
        });
    }


    /*list append*/

    // $scope.formattedDate =   $filter('date')($scope.currDate, "dd-MM-yyyy");

    $scope.getList = function(Keyword = "", FromPG = 0) {
        /*if (Category == '') {
            $scope.Category = Category;
            $scope.data.pageSize = 20;
        } else {

            $scope.data.pageNo = 1;
            $scope.data.dataList = [];
        }*/

        if(FromPG == 1)
        {
            $scope.data.pageNo = 1;
            $scope.data.dataList = [];
        }

        $scope.data.listLoading = true;


        var data = 'SessionKey=' + SessionKey + '&Keyword=' + Keyword + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize();

        $http.post(API_URL + 'Event/getEvent', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }
                // console.log($scope.data.dataList);
                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;

            // setTimeout(function(){ tblsort(); }, 1000);

        });

    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) {
        // $scope.getCategory();
        $scope.Categories = [];
        $scope.Category = [];
        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();

        $('#add_model').modal({
            show: true
        });

        var d = new Date();

        var month = d.getMonth();

        var day = d.getDate();

        var year = d.getFullYear();
        // $('input[name=EndTime]').datetimepicker({pickDate: false });


        $timeout(function() {
            tinymce.init({
                selector: '#EventDescription'
            });
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 700);
    }

    /*load add form*/
    $scope.loadFormAddCategory = function(Position) {


        $scope.formtype = Position;

        if ($scope.formtype == 1) {

            $('#edit_model .close').click();

        } else if ($scope.formtype == 2) {

            $('#add_model .close').click();
        }


        $scope.templateURLCategory = PATH_TEMPLATE + module + '/addcategory_form.htm?' + Math.random();

        $('#addcategory_model').modal({
            show: true
        });

        $timeout(function() {

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 200);



    }


    $scope.cancelAddActivity = function(){
        tinymce.remove('.EventDescription');
         if ($scope.formtype == 1) {

            $timeout(function() {
                $timeout(function() {
                    $('#edit_model').modal({
                        show: true
                    }); // console.log($scope.Categories);
                }, 800);
                $('#StartDates,#EndDate').datetimepicker({
                    format: 'dd-mm-yyyy H:i',
                    pickDate: false
                });
                $(".chosen-select").chosen({
                    width: '100%',
                    "disable_search_threshold": 8,
                    "placeholder_text_multiple": "Please Select",
                }).trigger("chosen:updated");
                tinymce.init({
                    selector: '.EventDescription'
                });
            }, 800);
        } else if ($scope.formtype == 2) {
            $timeout(function() {

                $timeout(function() {
                    $('#add_model').modal({
                        show: true
                    });
                }, 800);
                $('#StartDates,#EndDate').datetimepicker({
                    format: 'dd-mm-yyyy H:i',
                    pickDate: false
                });
                $(".chosen-select").chosen({
                    width: '100%',
                    "disable_search_threshold": 8,
                    "placeholder_text_multiple": "Please Select",
                }).trigger("chosen:updated");
                tinymce.init({
                    selector: '.EventDescription'
                });
            }, 800);
        }
    }

    /*add category book*/

    $scope.addCategoryData = function() {
        $scope.NewCategory = $('.Category').val();

        //alert($scope.Category);

        $scope.addDataLoadingc = true;

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='category_form']").serialize();

        $http.post(API_URL + 'Event/addcategory', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.getCategory();

                $('#addcategory_model .close').click();

                if ($scope.Categories.length > 0) {
                    $scope.Categories.push($scope.NewCategory);
                    $scope.Category = $scope.Categories;
                    $scope.Categories = [];
                } else {
                    $scope.Category.push($scope.NewCategory);
                    $scope.Categories = $scope.Category;
                    $scope.Category = [];
                }



                //alert(Category);
                if ($scope.formtype == 1) {
                    $('#edit_model').modal({
                        show: true
                    }); // console.log($scope.Categories);
                    $(".EventDescription").show();
                    tinymce.remove('.EventDescription');
                    $timeout(function() {                      
                        $('#StartDates,#EndDate').datetimepicker({
                            format: 'dd-mm-yyyy H:i',
                            pickDate: false
                        });
                        $(".chosen-select").chosen({
                            width: '100%',
                            "disable_search_threshold": 8,
                            "placeholder_text_multiple": "Please Select",
                        }).trigger("chosen:updated");
                        tinymce.init({
                            selector: '.EventDescription'
                        });
                    }, 1000);
                } else if ($scope.formtype == 2) {
                    $timeout(function() {

                        $timeout(function() {
                            $('#add_model').modal({
                                show: true
                            });
                            tinymce.remove('.EventDescription');
                        }, 800);
                        $('#StartDates,#EndDate').datetimepicker({
                            format: 'dd-mm-yyyy H:i',
                            pickDate: false
                        });
                        $(".chosen-select").chosen({
                            width: '100%',
                            "disable_search_threshold": 8,
                            "placeholder_text_multiple": "Please Select",
                        }).trigger("chosen:updated");
                        tinymce.init({
                            selector: '.EventDescription'
                        });
                    }, 800);
                }

            } else {

                ErrorPopup(response.Message);

            }

            $scope.addDataLoadingc = false;

        });

    }



    /*load Book View*/



    $scope.loadFormView = function(Position, EventGUID) {
        $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE + module + '/view_event_details.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'Event/getEventDetails', 'SessionKey=' + SessionKey + '&EventGUID=' + EventGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                //$scope.Category = $scope.formData.CategoryName.split(",");

                $('#View_model').modal({
                    show: true
                });


                // $timeout(function() {
                //     $(".chosen-select").chosen({
                //         width: '100%',
                //         "disable_search_threshold": 8,
                //         "placeholder_text_multiple": "Please Select",
                //     }).trigger("chosen:updated");
                // }, 700);
            }
        });
    }


    $scope.loadFormActivityView = function(Position, data) {
        $('#manage_activity_model .close').click();
        $scope.data.ViewActivityData = data;
        $scope.templateURLActivityView = PATH_TEMPLATE + module + '/view_activity.htm?' + Math.random();
        $timeout(function() {
            $('#view_activity_model').modal({
                show: true
            });
        }, 800);
    }

    $scope.cancelActivityView = function(){
       // alert("sdflks");
        $timeout(function() {
            $scope.loadFormActivity($scope.data.Position, $scope.EventGUID);
        }, 800);        
    }


    /*load edit form*/
    $scope.pushCategory = function(NewCate) {
        console.log(NewCate);
        if ($scope.Categories.length > 0) {
            $scope.Category = NewCate;
            $scope.Categories = [];
        } else {
            $scope.Categories = NewCate;
            $scope.Category = [];
        }
        $timeout(function() {
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");
            tinymce.init({
                selector: '.EventDescription'
            });
        }, 800);
    }



    $scope.loadFormEdit = function(Position, EventGUID) {

        $scope.data.Position = Position;
        

        $scope.data.pageLoading = true;
        $scope.Categories = [];
        $scope.Category = [];
        $http.post(API_URL + 'Event/getEventByID', 'SessionKey=' + SessionKey + '&EventGUID=' + EventGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;

                //$scope.Category = $scope.formData.CategoryName.split(",");

                $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();

        $('#edit_model').modal({
            show: true
        });
                


                $timeout(function() {
                    
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                    tinymce.init({
                        selector: '.EventDescription'
                    });
                }, 800);

            }

        });

    }


    $scope.loadFormActivity = function(Position, EventGUID) {
        $scope.data.Position = Position;
        $scope.EventGUID = EventGUID;
        $scope.templateURLActivity = PATH_TEMPLATE + module + '/manage_activity.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $scope.Categories = [];
        $scope.Category = [];
        $http.post(API_URL + 'Event/getEventActivity', 'SessionKey=' + SessionKey + '&EventGUID=' + EventGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.data.Activity = response.Data;

                console.log($scope.data.Activity);

                //$scope.Category = $scope.formData.CategoryName.split(",");

                console.log($scope.Category);

                $('#manage_activity_model').modal({
                    show: true
                });

                var d = new Date();

                var month = d.getMonth();

                var day = d.getDate();

                var year = d.getFullYear();


                $timeout(function() {
                    console.log("gfjdhgdkfjh");
                    $('#CompletionDate0').datepicker();
                    // $('#EndDate').datetimepicker({format: 'dd-mm-yyyy H:i',pickDate: false }).datetimepicker('setStartDate',$scope.formData.EventStartDateTime);
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                    tinymce.init({
                        selector: '.EventDescription'
                    });
                }, 800);

            }

        });

    }


    $scope.loadEventParticipants = function(Position, EventGUID) {
        $scope.data.Position = Position;
        $scope.EventGUID = EventGUID;
        $scope.templateURLParticipants = PATH_TEMPLATE + module + '/manage_participants.htm?' + Math.random();
        $scope.data.pageLoading = true;       
        $http.post(API_URL + 'Event/getEventParticipants', 'SessionKey=' + SessionKey + '&EventGUID=' + EventGUID, contentType).then(function(response) {

            var response = response.data;
             $('#manage_participants_model').modal({
                    show: true
                });  

             $scope.data.pageLoading = false;

            if (response.ResponseCode == 200) {
                /* success case */

                

                $scope.data.Participants = response.Data;

               

            }
        });
    }



    $scope.SetParticipantsPermission = function(Permission,UserID,EventActivityID){
        $scope.deleteDataLoading = true;
        //$scope.data.Position = Position;
        alertify.confirm('Are you sure to change the participate permission of this student?', function() {
            $("#loader"+UserID).show();
            $http.post(API_URL + 'event/SetParticipantsPermission', 'SessionKey=' + SessionKey + '&Permission=' + Permission + '&UserID=' + UserID+ '&EventActivityID=' + EventActivityID, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) {
                    $("#loader"+UserID).hide();
                    if(Permission == 1){
                        $("#radio0"+UserID+''+EventActivityID).prop('checked', false);
                    }else{
                        $("#radio1"+UserID+''+EventActivityID).prop('checked', false);
                    }
                    SuccessPopup(response.Message);
                    // $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    // $scope.data.totalRecords--;
                    // $('.modal-header .close').click();
                    //$scope.deleteDataLoading = false;
                }
            });
        }, function(){ $("#radio"+Permission+''+UserID+''+EventActivityID).prop('checked', false); }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }



    /*delete*/
    $scope.loadFormEventDelete = function(Position, EventGUID) {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'event/delete', 'SessionKey=' + SessionKey + '&EventGUID=' + EventGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


    /*delete*/
    $scope.loadFormActivityDelete = function(Position, EventGUID, EventActivityID) {
        $scope.deleteDataLoading = true;
        $scope.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'event/ActivityDelete', 'SessionKey=' + SessionKey + '&EventActivityID=' + EventActivityID + '&EventGUID=' + EventGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.Activity.splice($scope.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    // $('.modal-header .close').click();
                    // $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }



    /*edit*/
    $scope.loadFormActivityEdit = function(Position, data) {
        $('#manage_activity_model .close').click();
        $scope.data.EditActivityData = data;
        var res = data.ActivityDuration.split(":");
        $scope.data.EditActivityData.hour = res[0];
        $scope.data.EditActivityData.minute = res[1];
        $scope.templateURLActivityEdit = PATH_TEMPLATE + module + '/edit_activity.htm?' + Math.random();
        $timeout(function() {
            $('#edit_activity_model').modal({
                show: true
            });
            $('.CompletionDate').datepicker();
        }, 800);
    }



    /*add datta*/
    $scope.addData = function() {
        $scope.addDataLoading = true;

        tinyMCE.triggerSave();
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();
        $http.post(API_URL + 'Event/add', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);
                $scope.applyFilter();
                $timeout(function() {

                    $('.modal-header .close').click();

                }, 200);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }
    /*edit data*/


    $scope.editData = function() {
        $scope.editDataLoading = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();
        $http.post(API_URL + 'Event/editEvent', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position] = response.Data;

                $('#edit_model .close').click();
            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });

    } /*PermissionData data*/


    $scope.ManageActivity = function(check="") {
        $scope.editDataLoading = true;

        //tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='manage_activity_form']").serialize();
        $http.post(API_URL + 'Event/ManageActivity', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
               SuccessPopup(response.Message);
               // $scope.data.dataList[$scope.data.Position] = response.Data;
               if(check==1){
                    $('#manage_activity_model .close').click();
               }else{
                    $('#manage_activity_model .close').click();
                    $timeout(function() {
                        $scope.loadFormActivity($scope.data.Position, $scope.EventGUID);
                    }, 800);
               }
                //
            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });
    }


    $scope.editActivity = function(check="") {
        //$('#manage_activity_model .close').click(); 
        $scope.editDataLoading = true;
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_activity_form']").serialize();
        $http.post(API_URL + 'Event/EditActivity', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
               SuccessPopup(response.Message);
               
              
               $timeout(function() { 
                    $('#edit_activity_model .close').click();
                    
                    $timeout(function() { 
                        $("#cancelActivityView").click();
                    }, 800);
                    
                    // $scope.loadFormActivity($scope.data.Position, $scope.EventGUID);
               }, 300);

            } else {
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;
        });
    }

    $scope.removeTask = function(cnt){
        $("#taskEdit"+cnt).remove();
    }

    $scope.addMoreTask = function(cnt,chk){
        var check = check_empty_input('editTask');
        if (check) {
            var numItems = $(".taskEdit").length;
            
            var count = cnt + 1;        
            var pre = numItems-1;

            var NextNumItems = numItems+1;

            var newpre = chk - 1;

            var type = "'edit'";

            console.log("#task_button"+newpre+""+numItems);

            $(".task_button_edit").last().html('<button type="submit" class="btn btn-danger btn-sm"  onclick="removeTask(\''+numItems+'\','+type+')" style="margin-top: 30px;min-width: 0px;">-</button>');

            var html = $(".taskEdit").last().after('<div class="row taskEdit" id="taskEdit'+NextNumItems+'"><div class="col-md-2"><div class="form-group"><h6 style="margin-top: 30px;font-size: 14px;">Activity Tasks:</h6></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Task <span class="text-danger">*</span></label><input name="Task[]" type="text" class="form-control editTask" value="" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Responsible Person <span class="text-danger">*</span></label><input name="ResponsiblePerson[]" type="text" class="form-control editTask"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Completion Date <span class="text-danger">*</span></label><input name="CompletionDate[]" type="text" class="form-control editTask CompletionDate" value="" maxlength="50" id="CompletionDate'+NextNumItems+'"></div></div><div class="col-md-1"><div class="form-group task_button_edit" id="task_button'+newpre+''+NextNumItems+'"><button type="submit" class="btn btn-success btn-sm"  onclick="addMoreTask('+count+','+chk+','+type+')" style="margin-top: 30px;min-width: 0px;">+</button></div></div></div>');
            
            setTimeout(function(){
             $('#CompletionDate'+NextNumItems).last().datepicker();
            }, 500);
        }
    }

});



$("#add_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});

$("#edit_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});


function getCategorybyList(Category) {
    angular.element(document.getElementById('content-body')).scope().getList(Category);
}



function setEventEndDateTime(date) {
    var d = new Date(date);
    console.log(d);
    d.setMinutes(d.getMinutes() + 5);
    console.log(d);
    //var d = d.toString('dd-mm-yyyy H:i');

    console.log(d);
   // alert(d);  
    // $('#EndDate').datetimepicker({
    //     format: 'dd-mm-yyyy H:i',
    //     pickDate: false
    // }).datetimepicker('setStartDateTime', d);
   // alert(d);
        var d = new Date(date);

        var month = d.getMonth();

        var day = d.getDate();

        var year = d.getFullYear();

        d.setMinutes(d.getMinutes() + 5);
        // $('input[name=EndTime]').datetimepicker({pickDate: false });

    $('#EndDate').datetimepicker({format: 'dd-mm-yyyy H:i',pickDate: false }).datetimepicker('setStartDate',d);
}
/* sortable - ends */


/*check empty inputs and duplicate titles*/
function check_empty_input(title_name) {
    //alert("ggjdfg");
    var check = true;
   
    var data = $("."+title_name)
        .map(function() {
            return $(this).val();
        }).get();

    console.log(data);
    $.each(data, function(index, value) {
        if (value == "") {
            alert("Please fill all fields before adding a new task.");
            check = false;
            return false;
        } 
    });
    
    return check;
}

var i = 0;
function addMoreTask(cnt,chk,type=""){
    if(type=='edit'){
        var check = check_empty_input('editTask');
        var addSecButton = "task_button_edit";
        var addSec = "taskEdit";
        var type = "'edit'";
        var numItems = $(".taskEdit").length;
    }else{
        var check = check_empty_input('Task_0');
        var addSecButton = "task_button";  
        var addSec = "task1";
        var type = "";
        var numItems = $(".task"+chk).length;
    }
    
    if (check) {
        
        var count = cnt + 1;        
        var pre = numItems-1;

        var NextNumItems = numItems+1;

        var newpre = chk - 1;

        console.log("#task_button"+newpre+""+numItems);

        $("."+addSecButton).last().html('<button type="submit" class="btn btn-danger btn-sm"  onclick="removeTask(\''+numItems+'\','+type+')" style="margin-top: 30px;min-width: 0px;">-</button>');

        var html = $("."+addSec).last().after('<div class="row '+addSec+'" id="'+addSec+''+NextNumItems+'"><div class="col-md-2"><div class="form-group"><h6 style="margin-top: 30px;font-size: 14px;">Activity Tasks:</h6></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Task <span class="text-danger">*</span></label><input name="Task[]" type="text" class="form-control Task_0" value="" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Responsible Person <span class="text-danger">*</span></label><input name="ResponsiblePerson[]" type="text" class="form-control Task_0"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Completion Date <span class="text-danger">*</span></label><input name="CompletionDate[]" type="text" class="form-control Task_0 CompletionDate" value="" maxlength="50" id="CompletionDate'+NextNumItems+'"></div></div><div class="col-md-1"><div class="form-group '+addSecButton+'" id="task_button'+newpre+''+NextNumItems+'"><button type="submit" class="btn btn-success btn-sm"  onclick="addMoreTask('+count+','+chk+','+type+')" style="margin-top: 30px;min-width: 0px;">+</button></div></div></div>');
        
        setTimeout(function(){
         $('#CompletionDate'+NextNumItems).last().datepicker();
        }, 500);
    }
}


function removeTask(cnt,type){
    if(type == 'edit'){
        $("#taskEdit"+cnt).remove();
    }else{
        $("#task"+cnt).remove();
    }
    
}

function removeActivity(cnt){
    $("#Activity"+cnt).remove();
    $("#removeActivity"+cnt).remove();
}

function addMoreActivity(cnt){
    var check = check_empty_input('Task_'+cnt);

    if (check) {
        //var numItems = $('.after-add-more').length;
        var pre = cnt - 1;  
        var count = cnt + 1;   

        //console.log("#addMoreActivity_"+pre);

        $("#addMoreActivity_"+cnt).remove(); 

        //addMoreActivity_1    

        //$("#addMoreActivity_"+pre).html('<button type="submit" class="btn btn-danger btn-sm"  onclick="removeActivity(\''+cnt+'\')" id="removeActivity'+cnt+'" style="margin-top: 30px;min-width: 0px;">-</button>');

        var html = $(".Activities").last().after('<button type="submit" class="btn btn-danger btn-sm"  onclick="removeActivity(\''+cnt+'\')" id="removeActivity'+cnt+'" style="margin-top: 30px;min-width: 0px;">-</button><div class="Activities" id="Activity'+cnt+'" style="border: 1px solid gray;padding: 10px;margin: 10px;border-radius: 5px;"><div class="row"><div class="col-md-3"><div class="form-group"><label class="control-label">Activity Name <span class="text-danger">*</span></label><input name="ActivityName[]" type="text" class="form-control" value="{{formData.EventName}}" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Accountability <span class="text-danger">*</span></label><select name="Accountaibility[]" class="form-control chosen-select Categorys" multiple tabindex="4" ng-model="Category" ng-change="pushCategory(Category)"><option ng-repeat="rows in data.CategoryData" value="{{rows.Category}}" ng-selected="rows.Category==Category">{{rows.Category}}</option></select></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Coordinator <span class="text-danger">*</span></label><input name="Coordinator[]" type="text" class="form-control" value="{{formData.EventName}}" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Duration <span class="text-danger">* </span>(hh:mm)</label><input name="ActivityDuration[]" type="text" class="form-control" value="{{formData.EventName}}" maxlength="50" placeholder="HH:MM"></div></div></div><div class="row task'+cnt+'"><div class="col-md-2"><div class="form-group"><h6 style="margin-top: 30px;font-size: 14px;">Activity Tasks:</h6></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Task <span class="text-danger">*</span></label><input name="Task_'+pre+'[]" type="text" class="form-control Task_'+pre+'" value="{{formData.EventName}}" maxlength="50"></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Responsible Person <span class="text-danger">*</span></label><select name="ResponsiblePerson_0[]" class="form-control chosen-select Categorys Task_'+pre+'" multiple tabindex="4" ng-model="Category" ng-change="pushCategory(Category)"><option ng-repeat="rows in data.CategoryData" value="{{rows.Category}}" ng-selected="rows.Category==Category">{{rows.Category}}</option></select></div></div><div class="col-md-3"><div class="form-group"><label class="control-label">Completion Date <span class="text-danger">*</span></label><input name="CompletionDate_0[]" type="text" class="form-control Task_'+pre+'" value="{{formData.CompletionDate}}" maxlength="50"></div></div><div class="col-md-1"><div class="form-group" id="task_button'+pre+''+cnt+'"><button type="submit" class="btn btn-success btn-sm"  onclick="addMoreTask('+pre+','+cnt+')" style="margin-top: 30px;min-width: 0px;">+</button></div></div></div></div><button type="submit" class="btn btn-success btn-sm addMoreActivity addMoreActivity'+count+'" id="addMoreActivity_'+count+'"  onclick="addMoreActivity('+count+')" style="margin-top: 30px;min-width: 0px;">Add More Activity</button>');
      
    }
}

// (function($) {
//   $.fn.inputFilter = function(inputFilter) {
//     return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
//       if (inputFilter(this.value)) {
//         this.oldValue = this.value;
//         this.oldSelectionStart = this.selectionStart;
//         this.oldSelectionEnd = this.selectionEnd;
//       } else if (this.hasOwnProperty("oldValue")) {
//         this.value = this.oldValue;
//         this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
//       }
//     });
//   };
// }(jQuery));


// $("#myTextBox").inputFilter(function(value) {
//   return /^\d*$/.test(value);
// });

function checkDuration(TimeVal){
 if(TimeVal){
    if (TimeVal.toLowerCase().indexOf(":") >= 0){
        var ret = TimeVal.split(":");
        if(ret[1] > 59){
            alert("Minutes can not be greater than 59");
        }
    }
 }
}


function search_records()
{
    var Keyword = $("#Keyword").val();

    angular.element(document.getElementById('content-body')).scope().getList(Keyword, 1);
}


function clear_search_records()
{    
    $("#Keyword").val("");    

    search_records();
}