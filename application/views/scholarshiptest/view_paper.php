<style type="text/css">
.jconfirm-box-container{
flex: 0 0 56.333333% !important;
max-width: 56.333333% !important;
}
.glyphicon-trash
{
top:0px !important;
}	
</style>
<header class="panel-heading">
<h1 class="h4">View Paper</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="viewPaper()">
<div class="page-wrapper">
<div class="container-fluid">


<p ng-if="data.pageLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

<!-- Row -->
<div class="row">
<div class="col-md-12">
<div class="panel panel-default card-view">
<div class="panel-wrapper">
<div class="panel-body">
<div class="row" style="margin:5px;"> 
<div class="col-md-4">
<h1 class="h6 ng-binding">Title : {{formData.QtPaperTitle}}</h1>
</div>
<div class="col-md-4">
<h1 class="h6 ng-binding">Total Questions : {{formData.TotalQuestions}}</h1>
</div>
<div class="col-md-4">
<h1 class="h6 ng-binding">Total Marks : {{formData.Total_marks}}</h1>
</div>
</div>
<hr class="light-grey-hr" />
<div class="row">
<div class="col-sm-12 col-xs-12">

<div class="form-wrap">
<form action="#" class="form-horizontal" style="margin-top: 20px;">

<div class="first current"  ng-repeat="(key, row) in formData.question_answer" id="div{{key}}">
<div class="row">
<div class="col-sm-11">
<p class="pull-left" style="float: left;
width: 100%;">
Question {{key+1}} :- &nbsp; <label ng-bind-html="row.QuestionContent1"></label> ?
</p><br>
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{key+1}}">Popup image</button> -->

<div id="myModal{{key+1}}" class="modal fade myModal{{key+1}}" role="dialog">
<div class="modal-dialog">              
<div class="modal-content">

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>                  
</div>

<div class="modal-body">
  <img src="{{row.Media.Records[0].MediaURL}}" class="img-responsive">
</div>
</div>
</div>
</div>
<img ng-if="row.Media.Records" id="picture-box-picture"  style="height: 100px;width: 150px;    margin: 10px 10px 10px 80px; cursor: pointer;" src="{{row.Media.Records[0].MediaURL}}" data-toggle="modal" data-target="#myModal{{key+1}}">
</div>
<br/>
<div class="col-sm-1">
<p class="pull-right">MARK {{row.QuestionsMarks}}</p>
</div>
<div class="col-sm-12">
<div class="row">
<div class="col-sm-1">
<p class="pull-left">Answer :</p>
</div>
<div ng-if="row.QuestionsType == 'Multiple Choice'" class="col-sm-8">
<div class="row">
	<div class="col-sm-12" ng-repeat="(ks, ans) in row.answer">
		<div class="checkbox">
			<input  type="checkbox" disabled="disabled">
			<label for="checkbox1">
				{{ans.AnswerContent}}
			</label>
			<span ng-if="ans.CorrectAnswer == 1"><span class="glyphicon glyphicon-ok" style="top:0px;font-size: 16px;color: green; margin-left: 10px;"></span> <span style="color: green;">Correct Answer</span></span>
		</div>

	</div>
</div>						
</div>

<div ng-if="row.QuestionsType == 'Logical Answer'" class="col-sm-8">
<div class="row">
	<div class="col-sm-12" ng-repeat="(ks, ans) in row.answer">
		<div class="checkbox">
			<input  type="radio" disabled="disabled">
			<label for="checkbox1">
				{{ans.AnswerContent}}
			</label>
			<span ng-if="ans.CorrectAnswer == 1" style="float: left;"><span class="glyphicon glyphicon-ok" style="top:0px;font-size: 16px;color: green; margin-left: 10px;"></span> <span style="color: green;">Correct Answer</span></span>
		</div>
	</div>
</div>						
</div>

</div>
</div>
<div class="col-sm-12">
<p class="pull-right">
<button ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" type="button" class="btn btn-danger" ng-click="removeQuestion(key,formData.QtPaperGUID,row.QtBankGUID,row.QuestionsMarks)">
<i class="fa fa-trash"></i>
</button>
</p>
</div>
</div>
<hr class="light-grey-hr" />
</div>

<div class="clear">&nbsp;</div>
<!-- <div class="pull-left">
<button type="button" id="prev" class="btn btn-primary">Prev</button>
</div>

<div class="pull-right">
<a href="#" class="btn btn-primary btn-icon left-icon  mr-10" style="float: left;"> <i class="fa fa-plus"></i> <span>Exit</span></a>
<button type="submit" id="next" class="btn btn-success mr-10"> Next</button>
</div>		 -->											
</form>
</div>
</div>
</div>
</div>
</div>
</div>		
</div>
</div>
<!-- /Row -->

<p class="no-records text-center" ng-if="data.noRecords">
<span>No records found.</span>
</p>
</div>


</div>
<!-- /Main Content -->

<!-- add Modal -->
<div class="modal fade" id="add_model">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h3 class="modal-title h5">Generate Question Paper</h3>     	
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div ng-include="templateURLAdd"></div>
</div>
</div>
</div>
</div>