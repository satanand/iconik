app.controller('PageController', function ($scope, $http,$timeout,$compile){
    $scope.data.dataRole=[];
    $scope.data.dataList=[];
    $scope.data.Roles=[];
    $scope.data.Subject = [];
    $scope.data.Course = [];
    $scope.CourseIDA;
    $scope.formData = [];
    $scope.APIURL = API_URL + "upload/image";

    $scope.PassingOutYear = [];
    var d = new Date();
    var y = d.getFullYear();
    var options_year = "";
    for(;y>=1950; y--)
    {
        $scope.PassingOutYear.push(y);
        options_year = options_year + "<option value='"+y+"'>"+y+"</option>";
    }
  
    $scope.availableKeys = function (validity)

    {

        var data = 'SessionKey='+SessionKey+'&validity='+validity;

        $http.post(API_URL+'keys/availableKeys', data, contentType).then(function(response) {

            var response = response.data; 

            if(response.ResponseCode==200 && response.Data){ /* success case */

                 $scope.availableData =  response.Data[0]; console.log($scope.availableData);

                 $timeout(function(){

                    $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

                }, 300);    

            }else{
                $scope.availableData.AvailableKeys = 0;
            }

        });

    }


    $scope.removeQualification = function(index) {

         $('#quali'+index).remove();
      };

      $scope.removeexperience = function(index) {

         $('#experi'+index).remove();
      };  

      $scope.removebtn_Quali = function(index) {

         $('#rows'+index).remove();
      };  

      $scope.removebtn_Exper = function(index) {

         $('#row'+index).remove();
      };

    
      $scope.clicked = 0;
      $scope.click = function() {
        var i=$scope.clicked++;
        $('#dynamic_field_exp').append($compile('<div id="row'+i+'"><div class="row" ng-init="getIndustries()"><div class="col-md-6"><div class="form-group"><label class="control-label">Employer</label><br><input name="Employer[]" type="text" class="form-control" value=""></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Industries</label><br><select name="Industries[]" type="text" class="form-control chosen-select" value=""><option></option><option ng-repeat="indu in data.Industries" value="{{indu.IndustryID}}">{{indu.IndustryName}}</option></select></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Designation</label><input name="ExpDesignation[]" type="text" class="form-control" value="" ></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Job Profile</label><input name="JobProfile[]" type="text" class="form-control" value="" ></div></div></div><div class="row"><div class="col-md-6"><div class="form-group"><label class="control-label">Started Working Form</label><input name="Form[]" type="text" class="form-control integer Form" value="" onclick="getCalendarForm()" placeholder="dd-mm-yyyy" ng-model="startDate"></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">To</label><input name="To[]" type="text" class="form-control integer To" value="{{startDate}}" onclick="getCalendarTo()" placeholder="dd-mm-yyyy"></div></div></div><div class="col"><div class="form-group change"><label for="" style="">&nbsp;</label><br><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_removes" ng-click="removebtn_Exper('+i+')" style="">Remove</button></div></div>')($scope));
        $(".Form,.To").datepicker();
      } 

      $scope.count = 0;
      $scope.clickQua = function() {
      var j=$scope.count++;

        $('#dynamic_field').append($compile('<div id="rows'+j+'"><div class="row"><div class="col-md-6"><div class="form-group"><label class="control-label"> Qualification</label><select name="Qualification[]"  class="form-control chosen-select"  ng-model="Educations" ><option></option><option ng-repeat="qua in data.qualification" value="{{qua.Qualification}}">{{qua.Qualification}}</option></select></div></div></div><div class="row"><div class="col-md-6"><div class="form-group"><label class="control-label">Specialization</label><input name="Specialization[]" type="text" class="form-control" value="" ></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Univercity/Institute</label><br><input name="Univercity[]" type="text" class="form-control" value=""></div></div></div><div class="row"><div class="col-md-6"><div class="form-group"><label class="control-label">Passing Out Year</label><br><select name="PassingYear[]" id="Year" class="form-control"><option value="">Select</option>'+options_year+'</select></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Percentage %</label><input name="Percentage[]" type="text" class="form-control integer" value="" placeholder="%" maxlength="3"></div></div></div><div class="col"><div class="form-group change"><label for="" style="">&nbsp;</label><br><button type="button" name="remove" id="'+j+'" ng-click="removebtn_Quali('+j+')" class="btn btn-danger btn_remove" style="">Remove</button></div></div></div>')($scope));
      }

       $scope.getJoindate = function(Dates){
        //$('#NewsDate').datepicker();
      
       $("#AppraisalDate").datepicker().datepicker('setStartDate',Dates);
    }

    $scope.getFilterData = function ()
    {
        $scope.getRoles();
        $scope.getStates();
        $scope.getCourse(); 
        $scope.getQualification();
        $scope.getIndustries();
        $scope.getDepartment();
        $scope.getSecondLevelPermission();
    }


    $scope.applyFilter = function (){

        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
        $scope.getFilterData();
       
    }


    /*list append*/

    $scope.getList = function (UserTypeID="",check="")
    {
        if(check!=1){            
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }else{
            $scope.data.pageNo = 1;
            $scope.data.dataList = [];
        }
        

        $scope.UserTypeID = UserTypeID;

        $scope.data.pageSize = 20;
    

        $scope.data.listLoading = true;
       // $scope.getFilterData();

        var data = 'SessionKey='+SessionKey+'&UserTypeID='+UserTypeID+'&IsAdmin=Yes&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        
       // $scope.data.dataList = [];

        $http.post(API_URL+'admin/staff', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data.Records) {

                       $scope.data.dataList.push(response.Data.Records[i]);

                   }

                   $scope.data.pageNo++;               
               }else{

                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;

             // setTimeout(function(){ tblsort(); }, 1000);

        });
    }


    $scope.getRoles = function(){

        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        $scope.data.Roles = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'roles/getRoles', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {

                 $scope.data.Roles.push(response.Data.Records[i]);
             }
            
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

    } 


    $scope.getCourse = function(){

        $scope.data.Course = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'admin/staff/getCourse', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

             for (var i in response.Data) {

                 $scope.data.Course.push(response.Data[i]);
             }
            

             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

      } 




      $scope.getSubject = function(CourseID,check){
        //alert(CourseID);

        
        if(check == "edit"){

         $scope.formData.Course_subjects = [];
        }else{
           $scope.data.Subject = [];
        }
        var data = 'SessionKey='+SessionKey+'&CategoryID='+CourseID;

        $http.post(API_URL+'admin/staff/getSubject', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {
                if(check == "edit"){

                 $scope.formData.Course_subjects.push(response.Data.Records[i]);
                }else{
                    $scope.data.Subject.push(response.Data.Records[i]);
                }
             }
            

             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

      }   


      $scope.getSubjectOnEdit = function(CourseID){
        //alert(CourseID);

        $scope.formData.Course_subjects = [];

        var data = 'SessionKey='+SessionKey+'&CategoryID='+CourseID;

        $http.post(API_URL+'admin/staff/getSubject', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {

                 $scope.formData.Course_subjects.push(response.Data.Records[i]);
             }
            

             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

      }   


     $scope.getStates = function(){

        var data = 'SessionKey='+SessionKey;

        $scope.data.states = [];

        $http.post(API_URL+'students/getStatesList', data, contentType).then(function(response) {

            var response = response.data;

          if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.states = response.Data;

            }
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



             }, 200);

        });

    }





    $scope.getCities = function(State_id){
        $scope.data.cities = [];
        var data = 'SessionKey='+SessionKey+'&State_id='+State_id;
       
        $http.post(API_URL+'students/getCitiesList', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.cities.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }
 
    $scope.getCitiesByStateName = function(StateName){

        $scope.data.CitiesNominee = [];
        var data = 'SessionKey='+SessionKey+'&StateName='+StateName;
        $http.post(API_URL+'students/getCitiesByStateName', data, contentType).then(function(response) {

            var response = response.data;

             if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.CitiesNominee.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }
    $scope.getCitiesByStateNames = function(StateName){

        $scope.data.CitiesEmergency = [];
        var data = 'SessionKey='+SessionKey+'&StateName='+StateName;
        $http.post(API_URL+'students/getCitiesByStateName', data, contentType).then(function(response) {

            var response = response.data;

             if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.CitiesEmergency.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }

    $scope.getQualification = function(){
        $scope.data.qualification = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'qualification/getQualification', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.qualification.push(response.Data.Records[i]);
            }

          }

        });
    }

    
    $scope.getIndustries = function(){
        $scope.data.Industries = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'industry/getIndustry', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.Industries.push(response.Data.Records[i]);
          }

          }

        });

    } 
    $scope.getDepartment = function(){
        $scope.data.Department = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'Department/getDepartment', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.Department.push(response.Data.Records[i]);
          }

          }

        });

    }


    /*load add form*/

    $scope.loadImportStaff = function (){
        $scope.templateURLImport = PATH_TEMPLATE+module+'/import_staff.htm?'+Math.random();
        $scope.data.pageLoading = true;       
        $timeout(function(){   
             $('#import_model').modal({show:true});     
             $scope.data.pageLoading = false;           
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
        }, 1000);
    }


    $scope.loadFormAdd = function (Position, CategoryGUID)
    {

        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

        $scope.InstituteName=InstituteName;
        $scope.InstituteID=InstituteID;

        $scope.data.pageLoading = true;

        $('#add_model').modal({show:true});

        $scope.data.pageLoading = false;

            $timeout(function(){           
                


                $("#BirthDate, #AnniversaryDate").datepicker({
                    maxDate:0
                });         
                 
                /*$("#JoiningDate, #AppraisalDate").datepicker();
                $(".Form,.To").datepicker(); */
                
                //$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
            }, 900);


            $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

            }, 600);
    }



/*add category*/



    $scope.loadFormCategory = function (Position, CategoryGUID)
    {
        $scope.formtype=Position;

        if($scope.formtype==1){

          $('#edits_model .close').click();

        }else{

          $('#add_model .close').click();
        }

        $scope.templateURLCategory = PATH_TEMPLATE+module+'/add_form_category.htm?'+Math.random();
       
        $('#add_category_model').modal({show:true});

        $timeout(function(){            
           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);  

    }

    /*load edit form*/
    $scope.loadFormEdit = function (Position, UserID)
    {
        //$scope.getList("",1);
        $scope.data.Position = Position;
        
        $scope.data.pageLoading = true;
        //$scope.getRoles();
        
        
        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();
            
            

        $http.post(API_URL+'admin/staff/getProfile','SessionKey='+SessionKey+'&UserID='+UserID+'&Params=Status,ProfilePic', contentType).then(function(response) {

            

            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;

                $('#edits_model').modal({show:true});
                $scope.formData = response.Data;
              //  alert($scope.formData.MaritalStatus)
                if($scope.formData.AdminAccess == 'No'){
                    $scope.showDivs = true;
                }else{
                     $scope.showDivs = false;
                }

                //alert($scope.showDiv);
  
                $scope.getCities($scope.formData.State_id);
                $scope.getCitiesByStateNames($scope.formData.EmergencyState);
                $scope.getCitiesByStateName($scope.formData.NomineeState);

                $scope.StateID=$scope.formData.State_id;

                $scope.NomineeState=$scope.formData.NomineeState;
                $scope.EmergencyState=$scope.formData.EmergencyState;

                $scope.Qualification=$scope.formData.Qualification;

                if($scope.formData.MaritalStatus.length > 0){
                    $scope.MaritalStatus=$scope.formData.MaritalStatus;
                }else{
                   $scope.MaritalStatus='';
                }
                

                //$scope.getSubject($scope.formData.CourseID);

                $scope.CourseID=$scope.formData.CourseID.split(",");

                $scope.SubjectIDA=$scope.formData.SubjectID.split(",");

                $scope.UserTypeID=$scope.formData.UserTypeID;

                
                //console.log($scope.SubjectIDA);

                $timeout(function()
                {  
                    $("#BirthDate, #AnniversaryDate").datepicker({
                        maxDate:0
                    });
                    

                    /*
   
                    $("#JoiningDate,#AppraisalDate").datepicker();
                    
                    $("#Form").datepicker({onSelect: function(selected) {

                    $("#To").datepicker("option","minDate", selected) }});


                    $("#To").datepicker({onSelect: function(selected) {

                       $("#Form").datepicker("option","maxDate", selected)

                    }
                    });*/

                    if($scope.formData.UserTypeID == 11){
                        $("#Subject").show();
                    }


                $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 1000);

           }

        });

    }


    /*load edit form*/
    $scope.loadFormView = function (Position, UserID)
    {
      
       $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        //$scope.getRoles();
        $http.post(API_URL+'admin/staff/getProfile','SessionKey='+SessionKey+'&UserID='+UserID+'&Params=Status,ProfilePic', contentType).then(function(response) {

            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;


                $scope.formData = response.Data;

                $scope.getCities($scope.formData.State_id);

                
                
                $scope.getSubject($scope.formData.CourseID);

                $scope.CourseID=$scope.formData.CourseID.split(",");

                $scope.SubjectIDA=$scope.formData.SubjectID.split(",");

                $('#view_model').modal({show:true});
           }

        });

    }

     /*wall of fame */
    $scope.loadFormWalloffame = function (Position, UserID, FullName, ProfilePic)
    {
      //$scope.getUserData();

        $scope.templateURLAddWall = PATH_TEMPLATE+module+'/addwall_form.htm?'+Math.random();

        $scope.UserID=UserID;

        $scope.FullName=FullName;
        $scope.ProfilePic=ProfilePic;

        $('#addwall_model').modal({show:true});

        $timeout(function(){
            tinymce.init({selector: '.MediaContent' });
            $("#NewsDate").datepicker({format: 'dd-mm-yyyy',minView: 2});
            $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
           //$("#NewsDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});
       }, 800);

    }

  /*add data*/
     $scope.addData = function (type="",formStatus="")
      {

           $scope.loadeSubmit = true;
            var data = 'SessionKey='+SessionKey+'&type='+type+'&'+$("form[name='add_form']").serialize();
            var UserID = $("#UserID").val();
            if(UserID > 0){
                var APIURL = API_URL+'admin/staff/updateUserInfo';
            }else{
                var APIURL = API_URL+'admin/staff/add';
            }
            $http.post(APIURL, data, contentType).then(function(response) {

                var response = response.data;


                if(response.ResponseCode==200){ /* success case */               

                    //console.log(response.Data); 

                    $("#UserID").val(response.Data.UserID);
                    SuccessPopup(response.Message);

                     $scope.loadeSubmit = false;  

                     if(formStatus == "exit"){
                        location.reload();
                    }

                }else{



                    ErrorPopup(response.Message);



                }



                $scope.loadeSubmit = false;          



            });



        } 

        /*add wall of fame */
         $scope.addwallData = function ()
           {
                $scope.loadeSubmit = true; 

               tinyMCE.triggerSave();
               
                var data = 'SessionKey='+SessionKey+'&'+$("form[name='addwall_form']").serialize();
                $http.post(API_URL+'walloffame/add', data, contentType).then(function(response) {
                    var response = response.data;
                    if(response.ResponseCode==200){ /* success case */               

                        SuccessPopup(response.Message);
                        //$scope.applyFilter();
                        $timeout(function(){            

                          window.location.href =  window.location.href;

                       }, 2000);

                    }else{

                        ErrorPopup(response.Message);
                    }

                    $scope.loadeSubmit = false;          

                });

            }

             /*add data category*/



    $scope.addDataCategory = function ()
    {
         //var UserTypeName = $('#UserTypeName').val();

        $scope.data.listLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form_category']").serialize();

        $http.post(API_URL+'roles/add', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.getRoles();

                $scope.UserTypeID = response.UserTypeID;

                 $('#add_category_model .close').click();

                     $timeout(function(){
                       

                     if($scope.formtype==1){

                       $('#edits_model').modal({show:true});

                    }else{

                       $('#add_model').modal({show:true});
                    }            
                   
                }, 900);

            }else{

                ErrorPopup(response.Message);

            }

         $scope.data.listLoading = false;          

      });

    }





    /*edit data*/



    $scope.editData = function (type="",formStatus="")
    {

        $scope.loadeSubmit = true;
        // var Industries = $("select[name='Industries[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for experience or remove blank section.");
        //        return false;
        //     }
        // }).get();

        // var Employer = $("input[name='Employer[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for experience or remove blank section.");
        //        return false;
        //     }
        // }).get();

        // var ExpDesignation = $("input[name='ExpDesignation[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for experience or remove blank section.");
        //        return false;
        //     }
        // }).get();

        // var JobProfile = $("input[name='JobProfile[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for experience or remove blank section.");
        //        return false;
        //     }
        // }).get();

        // var From = $("input[name='From[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for experience or remove blank section.");
        //        return false;
        //     }
        // }).get();


        // var To = $("input[name='To[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for experience or remove blank section.");
        //        return false;
        //     }
        // }).get();

        // var Qualification = $("select[name='Qualification[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for qualification or remove blank section.");
        //        return false;
        //     }
        // }).get();

        //  var Specialization = $("input[name='Specialization[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for qualification or remove blank section.");
        //        return false;
        //     }
        // }).get();

        //  var Univercity = $("input[name='Univercity[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for qualification or remove blank section.");
        //        return false;
        //     }
        // }).get();

        //  var PassingYear = $("input[name='PassingYear[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for qualification or remove blank section.");
        //        return false;
        //     }
        // }).get();

        //  var Percentage = $("input[name='Percentage[]']")
        // .map(function() {
        //     if(!$(this).val()){
        //        alert("Please fill all added sections for qualification or remove blank section.");
        //        return false;
        //     }
        // }).get();

        var data = 'SessionKey='+SessionKey+'&type='+type+"&"+$("form[name='edit_form']").serialize();
        $http.post(API_URL+'admin/staff/updateUserInfo', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;

                if(formStatus == "exit"){
                    location.reload();
                }

                //$('#edits_model .close').click();
            }else{
                ErrorPopup(response.Message);

            }

            $scope.loadeSubmit = false;          
        });

    }

    $scope.loadFormDelete = function(Position,UserID)
    {
      
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL+'admin/staff/delete', 'SessionKey='+SessionKey+'&UserID='+UserID, contentType).then(function(response) {
              
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
       
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


     $scope.AssignKey = function(){
        $scope.assignDataLoading = true;        
        var UserGUID = $("input[name='UserGUID[]']:checkbox:checked").map(function() {
            if($(this).val()){
                return $(this).val();
            }
        }).get();

       // return false;

       // alert(UserGUID.length);

        if(UserGUID.length > 0){
            var Validity = $("#Validity").val();
            var AvailableKeys = $("#availableKeys").val();
            //alert(Validity.length);
            if(Validity.length == 0){
                ErrorPopup("Please select keys validity to assign keys.");
                $scope.assignDataLoading = false;      
            }else if(AvailableKeys.length == 0){
                ErrorPopup("Keys are not available to assign keys.");
                $scope.assignDataLoading = false;      
            }else{


                $scope.addDataLoading = true;

                var data = 'SessionKey='+SessionKey+'&Validity='+Validity+'&AvailableKeys='+AvailableKeys+'&UserGUID='+JSON.stringify(UserGUID);
                //console.log(data); return false;
                $http.post(API_URL+'keys/assignKeysToStaff', data, contentType).then(function(response) {

                    var response = response.data;

                    if(response.ResponseCode==200){ /* success case */               

                        SuccessPopup(response.Message);

                        $scope.assignDataLoading = false;  

                        var availableKeys = $("#availableKeys").val();

                        var availableKeys = parseInt(availableKeys) - parseInt(response.Data);
                        if (availableKeys > 0 || availableKeys == 0) {
                           $("#availableKeys").val(availableKeys);
                            $(".availableKeys").val(availableKeys);
                        }

                        $scope.getList($scope.UserTypeID,1);

                        $timeout(function(){            

                           $('.modal-header .close').click();

                       }, 200);

                    }else{

                        ErrorPopup(response.Message);

                    }

                    $scope.assignDataLoading = false;      

                });
            }
        }else{
            ErrorPopup("Please select staff to assign keys.");
            $scope.assignDataLoading = false;   
        }
       // $scope.assignDataLoading = false;   
    }


    $scope.getCalendar = function(){
        console.log("gdgdfghkj");
        $("#AnniversaryDate").datepicker();              
    }


    $scope.importData = function ()
    {
        $scope.loadeSubmit = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='importStaff']").serialize();
        $http.post(API_URL+'admin/staff/importStaff', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                SuccessPopup(response.Message);
                $scope.getList("",1);
                $timeout(function(){  
                   $('#import_model .close').click();
                   $scope.loadeSubmit = false;
                }, 200);
            }else{
                ErrorPopup(response.Message);
            }
        });
        $scope.loadeSubmit = false;
        setTimeout(function(){ tblsort(); }, 1000);
    }

}); 

$("#addwall_model").on("hidden.bs.modal", function () {
    tinymce.remove();
});

function getRoleByList(UserTypeID){
    //alert(UserTypeID);
   $("#BtnTotalStudent").text("Selected Staff 0");
   angular.element(document.getElementById('staff-body')).scope().getList(UserTypeID,1); 

}

$( document ).ready(function() {
   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var target = $(e.target).attr("href");
      if ((target == '#messages')) {
          alert('ok');
      } else {
          alert('not ok');
      }
  });
});


function getCalendar()
{  
    $("#AnniversaryDate").datepicker();                
}

function getCalendarForm(){
     var d = new Date();
    $(".Form").datepicker().datepicker('setEndDate',d);
                
}
function getCalendarTo(){
     var d = $('.Form').val();
     var end = new Date();
    $(".To").datepicker().datepicker('setStartDate',d).datepicker('setEndDate',end);
                
}

function getCalendarDOB(){
    var d = new Date();
    $("#BirthDate").datepicker().datepicker('setEndDate',d);
                
}
function getAppraisalDate(){
    var d = new Date();
    $("#AppraisalDate").datepicker().datepicker('setStartDate',d);
                
}
function getJoiningDate(){
    var d = new Date();
    $("#JoiningDate").datepicker();
                
}


function checkSubject(UserTypeID){
  //alert(UserTypeID);
  if(UserTypeID==11){
    $('#Subject').show();
  }else{
    $('#Subject').hide();
  }

}

function selectAllStudent(){ 
    if($('#checkAll').prop("checked") == false){
        $('.SelectStudent').prop('checked', false);
    }else{
        $('.SelectStudent').prop('checked', true);
    }
    var length = $('input.SelectStudent:checked').length; 
    $("#BtnTotalStudent").text("Selected Staff "+length);
}


function  checkLength() {
    var length = $('input.SelectStudent:checked').length;
    $("#BtnTotalStudent").text("Selected Staff "+length);
}


/*audio file upload*/

$(document).on('change', '#staffFileInput', function() {

    console.log('#staffFileInput');

    var target = $(this).data('target');

    var mediaGUID = $(this).data('targetinput');

    var progressBar = $('.progressBar'),

    bar = $('.progressBar .bar'),

    percent = $('.progressBar .percent');

    $(this).parent().ajaxForm({

        data: {

            SessionKey: SessionKey

        },

        dataType: 'json',

        beforeSend: function() {

            progressBar.fadeIn();

            var percentVal = '0%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        uploadProgress: function(event, position, total, percentComplete) {

            var percentVal = percentComplete + '%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        success: function(obj, statusText, xhr, $form) {

            console.log(obj);

            if (obj.ResponseCode == 200) {

                var percentVal = '100%';

                bar.width(percentVal)

                percent.html(percentVal);

                $(target).prop("src", obj.Data.MediaURL);

                $("input[name='MediaGUIDs']").val(obj.Data.MediaGUID);

                $(mediaGUID).val(obj.Data.MediaGUID);

                $("#MediaURL").val(obj.Data.MediaURL);

            } else {

                ErrorPopup(obj.Message);

            }

        },

        complete: function(xhr) {

            progressBar.fadeOut();

            //$('#studentFileInput').val("");

        }

    }).submit();

});


function search_records()
{
    angular.element(document.getElementById('staff-body')).scope().getList("",1); 
}

function clear_search_records()
{    
    $("#UserTypeID, #AppAccess, #AdminAccess, #Keyword").val("");    

    search_records();
}


function search_records_keys()
{
    angular.element(document.getElementById('staff-body')).scope().getList("",1); 
}

function clear_search_records_keys()
{    
    $("#UserTypeID, #Keyword, #Validity, #availableKeysTxt").val("");
    $("#availableKeys").val(0);    

    search_records();
}