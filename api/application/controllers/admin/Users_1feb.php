<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
	}



	/*
	Description: 	Use to broadcast message.
	URL: 			/api_admin/users/broadcast/	
	*/
	public function broadcast_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Title', 'Title', 'trim|required');
		$this->form_validation->set_rules('Message', 'Title', 'trim|required');
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim'); /* Media GUIDs */
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

/*		$EntityGUID = get_guid();
		// Add to entity table and get EntityID.
		$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>11, "UserID"=>$this->SessionUserID, "StatusID"=>2));

		//check for media present - associate media with this Post
		if(!empty($this->Post['MediaGUIDs'])){
			$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);
			foreach($MediaGUIDsArray as $MediaGUID){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $EntityID);
				}
			}
		}*/
		/* check for media present - associate media with this Post - ends */

		$UsersData=$this->Users_model->getUsers('
			U.UserID,	
			U.Username			
			',array('AdminUsers' =>'No'), TRUE, 1, 1000);
		if($UsersData){
			$NotificationText 		= $this->Post['Title'];
			$NotificationMessage 	= $this->Post['Message'];
			foreach($UsersData['Data']['Records'] as $Value){
				/* send notification - starts */
				$this->Notification_model->addNotification('broadcast', $NotificationText, $this->SessionUserID, $Value['UserID'], '' , $NotificationMessage);
				/* send notification - ends */
			}
		}

		$this->Return['Message'] = 'Message broadcasted.';
	}







	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	public function index_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.($this->UserTypeID==4 ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$UsersData=$this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize'],$this->UserTypeID);
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}


	/*
	Description: 	Use to update user profile info.
	URL: 			/api_admin/entity/changeStatus/	
	*/
	public function changeStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->Entity_model->updateEntityInfo($this->UserID, array("StatusID"=>$this->StatusID));
		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,Status',array("UserID" => $this->UserID));
		$this->Return['Message']      	=	"Status has been changed.";
	}


	/*
	Description: 	Use to update user details as pan and bank details.
	URL: 			/api_admin/entity/changeVerificationStatus/	
	*/
	public function changeVerificationStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('VetificationType', 'VetificationType', 'trim|required');
		if($this->Post['VetificationType']=='PAN'){
			$this->form_validation->set_rules('PanStatus', 'PanStatus', 'trim|required|callback_validateStatus');	
		}
		if($this->Post['VetificationType']=='BANK'){
			$this->form_validation->set_rules('BankStatus', 'BankStatus', 'trim|required|callback_validateStatus');
		}
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		if($this->Post['VetificationType']=='PAN' && !empty($this->Post['PanStatus'])){
			$UpdateData = array("PanStatus"=>$this->StatusID);
		}
		if($this->Post['VetificationType']=='BANK' && !empty($this->Post['BankStatus'])){
			$UpdateData = array("BankStatus"=>$this->StatusID);
		}
		$this->Users_model->updateUserInfo($this->UserID, $UpdateData );
		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,Status,PanStatus,BankStatus',array("UserID" => $this->UserID));
		$this->Return['Message']      	=	"Status has been changed.";
	}




	/*
	Name: 			updateUserInfo
	Description: 	Use to update user profile info.
	URL: 			/api_admin/updateUserInfo/	
	*/
	public function updateUserInfo_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');
		$this->form_validation->set_rules('UserTypeID', 'User Type', 'trim|in_list[3,4]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->Users_model->updateUserInfo($this->UserID, array_merge($this->Post, array("StatusID"=>@$this->StatusID, "SkipPhoneNoVerification"=>true)));
		$this->Return['Data'] = $this->Users_model->getUsers('StatusID,Status,ProfilePic,Email,Username,Gender,BirthDate,PhoneNumber,UserTypeName,RegisteredOn,LastLoginDate',array("UserID" => $this->UserID));
		$this->Return['Message']  =	"Successfully updated."; 	
	}



	/*
	Name: 			add
	Description: 	Use to register user to system.
	URL: 			/api_admin/users/add/
	*/
	public function add_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('Email', 'Email','trim|required|valid_email|callback_validateEmail');
		$this->form_validation->set_rules('Password', 'Password', 'trim'.(empty($this->Post['Source']) || $this->Post['Source']=='Direct' ? '|required' : ''));
		$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|required');
		$this->form_validation->set_rules('LastName', 'LastName', 'trim');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[3,4]');
		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|callback_validatePhoneNumber');
		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');	
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');

		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim|callback_validateEntityGUID[Store,StoreID]');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$UserID = $this->Users_model->addUser($this->Post, $this->Post['UserTypeID'], $this->SourceID, $this->StatusID); 
		if(!$UserID){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"An error occurred, please try again later.";  
		}else{
			/* Send welcome Email to User with login details */
			sendMail(array(
				'emailTo' 		=> $this->Post['Email'],			
				'emailSubject'	=> "Your Login Credentials - ".SITE_NAME,
				'emailMessage'	=> emailTemplate($this->load->view('emailer/adduser',array("Name" =>  $this->Post['FirstName'], 'Password' => $this->Post['Password']),TRUE)) 
			));
			return true;
		}
	}

	
}