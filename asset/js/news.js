app.controller('PageController', function($scope, $http, $timeout) {
    $scope.data.pageSize = 100;
    $scope.data.dataList = [];
    /*----------------*/

    // var currentTime = new Date();
    // $("#NewsDates").datetimepicker({
    //     format: 'dd-mm-yyyy',
    //     minView: 2
    // });

    $('#NewsDate').datepicker();


    $scope.getFilterData = function(Category = "") {
        $scope.getSecondLevelPermission();
    }

    $scope.applyFilter = function() {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }

    $scope.applytinymce = function() {
        tinymce.init({
            selector: '#NewsDescription',
            setup: function(editor) {
                editor.on('keyup', function(e) {
                    console.log('edited. Contents: ' + editor.getContent());
                    formDataCheck(editor.getContent());
                });
            }
        });
    }


    /*list append*/

    $scope.getList = function(FromPG = 0, NewsDates = "", Keyword = "") {
        //alert('dddddd');
        // if ($scope.data.listLoading || $scope.data.noRecords) return;


        $scope.data.pageSize = 20;

        if (FromPG == 1) 
        {

            $scope.data.pageNo = 1;

            $scope.data.dataList = [];
        }

        $scope.data.listLoading = true;


        var data = 'SessionKey=' + SessionKey + '&NewsDates=' + NewsDates + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize();

        $http.post(API_URL + 'News/getNews', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }

                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;


        });

    }
    /*delete*/
    /*   $scope.loadFormNewsDelete = function(Position,NewsGUID){
       alert(NewsGUID);
         $scope.deleteNews(NewsGUID);

      }*/

    $scope.loadFormNewsDelete = function(Position, NewsGUID) {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'news/delete', 'SessionKey=' + SessionKey + '&NewsGUID=' + NewsGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;

                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) {

        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();

        $('#add_model').modal({
            show: true
        });
        var currentTime = new Date();

        $timeout(function() {

            $('#NewsDate').datepicker();

            tinymce.init({
                selector: '#NewsDescription'
            });
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 700);

    }


    /*load edit form*/



    $scope.loadFormEdit = function(Position, NewsGUID) {

        $scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'News/getNewsByID', 'SessionKey=' + SessionKey + '&NewsGUID=' + NewsGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#edits_model').modal({
                    show: true
                });

                var d = new Date();

                var month = d.getMonth();

                var day = d.getDate();

                var year = d.getFullYear();

                $timeout(function() {
                    $('#NewsDate').datepicker();

                    tinymce.init({
                        selector: '#NewsDescription'
                    });
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                    tinymce.init({
                        selector: '#NewsDescription'
                    });
                }, 700);


            }


        });


    }

    $scope.loadFormView = function(Position, NewsGUID) {

        $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE + module + '/view_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'News/getNewsByID', 'SessionKey=' + SessionKey + '&NewsGUID=' + NewsGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#View_model').modal({
                    show: true
                });


                $timeout(function() {
                    /*  $("#StartDates,#EndDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});
                      tinymce.init({selector: '#EventDescription' });*/
                    // $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");


                }, 700);

            }

        });

    }

    $scope.addData = function() {
        $scope.addDataLoading = true;

        tinyMCE.triggerSave();
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();
        $http.post(API_URL + 'News/add', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);
                $scope.applyFilter();
                $timeout(function() {

                    $('.modal-header .close').click();

                }, 200);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }
    /*edit data*/


    $scope.editData = function() {
        $scope.editDataLoading = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();
        $http.post(API_URL + 'News/editNews', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position] = response.Data;

                $('#edits_model .close').click();
            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });

    } /*PermissionData data*/




});



$("#add_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});

$("#edits_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});



function search_records()
{
    var NewsDates = $("#NewsDates").val();
    var Keyword = $("#Keyword").val();

    angular.element(document.getElementById('content-body')).scope().getList(1, NewsDates, Keyword);
}


function clear_search_records()
{    
    $("#NewsDates, #Keyword").val("");    

    search_records();
}

/* sortable - ends */