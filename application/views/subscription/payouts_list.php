<style type="text/css">
.glyphicon 
{
    position: relative;
    top: -4px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    font-size: 20px;
    padding: 4px;
}
</style>  
<header class="panel-heading"> <h1 class="h4">Manage Payouts</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getPayoutsList(1);" id="content-body"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<span class="h5">Total Payouts: {{data.dataList.length}}</span>
				</div>
			</div>
			
			<div class="col-md-6 float-right" align="right">
				<a href="./subscription/osorders">
					<button class="btn btn-primary btn-sm ml-1" name="payout_btn">Orders</button>
				</a>	
				
				<a href="" ng-click="generatePayoutPDF();" data-toggle="tooltip" title="Export In PDF">
					<button class="btn btn-primary btn-sm ml-1" name="payout_export_btn">Export In PDF</button>
				</a>
			</div>

		</div>


		<form id="filterForm" role="form" autocomplete="off">
		<div class="row">		
		
		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterPaymentStartDate" type="text" placeholder="Payment From Date" id="FilterPaymentStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterPaymentEndDate" type="text" placeholder="Payment To Date" id="FilterPaymentEndDate" class="form-control">
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterStartDate" type="text" placeholder="Payout From Date" id="FilterStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterEndDate" type="text" placeholder="Payout To Date" id="FilterEndDate" class="form-control">
			</div>
		</div>


		
				

		<div class="col-md-3">
			<div class="form-group">			 	
	        	<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Search by word" >
	    	</div>
		</div>

		
		<div class="col-md-3">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" ng-click="getPayoutsList(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_payout_filters();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>		

	</div>
	</form>		

	</div>

	
 
	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getPayoutsList(0);" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading --> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



<table class="table table-striped table-condensed table-hover table-sortable">
<thead>
<tr>
	<th style="min-width: 70px;">Order ID</th>
    <th style="min-width: 100px;">Order Date</th>    
    <th style="min-width: 150px;">Product Of Institute</th>     
    <th style="min-width: 120px;">Amount</th>    
    <th style="min-width: 130px;">Payable Amount</th>  
    <th style="min-width: 100px;">Commission</th>  
    <th style="min-width: 100px;">Payment<br/>Date</th>   
    <th style="min-width: 100px;">Paid To Institute Date</th>
    <th style="min-width: 100px;">Description</th>
    <th style="min-width: 100px;">Action</th>
</tr>

</thead>

<tbody id="tabledivbody">

<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.OrderID}}">

	<td>{{row.OrderID}}</td>
	
	<td>{{row.OrderDate | date}}</td>		

	<td>{{row.FirstName}} {{row.LastName}}</td>	

	<td>{{row.Payouts.NetAmountSum}}</td>	

	<td>{{row.Payouts.PayableAmount}}</td>	

	<td>{{row.Payouts.Commission}}</td>	

	<td>{{row.PaymentDate}}</td>	

	<td>{{row.PayoutDate}}</td>

	<td>
		<label ng-bind-html="row.PayoutDesc"></label>
	</td>

	<td>
		<a ng-if="row.PayoutDate == ''" class="glyphicon glyphicon-rupee" href="" ng-click="loadFormMakePayment(row.OrderID, row.InstituteID, row.Payouts.NetAmountSum, row.Payouts.PayableAmount, row.Payouts.Commission, row.PaymentDateChk);" data-toggle="tooltip" title="Make Payment">Make Payment</a>
		<!-- <a class="glyphicon glyphicon-eye-open" href="" ng-click="loadViewOrder(row.OrderID, row.InstituteID);" data-toggle="tooltip" title="View"></a> -->

		<!-- <a ng-if="row.OrderStatus == 'success'" class="glyphicon glyphicon-file" href="" ng-click="generateInvoice(row.OrderID, row.InstituteID);" data-toggle="tooltip" title="Generate Invoice"></a> -->

		
	</td>

</tr>
</tbody>

</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->



	
<!-- add Modal -->
	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Make Payment</h3>					    	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>



</div><!-- Body/ -->


