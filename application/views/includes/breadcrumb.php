<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <?php
    $i = 0;
    foreach ($breadcrumb as $key => $value) {
      $active = ""; $i++;
      if(count($breadcrumb) == $i){
        echo '<li class="breadcrumb-item">'.$key.'</li>';
      }else if($value == ""){
        echo '<li class="breadcrumb-item">'.$key.'</li>';
      }else{
        echo '<li class="breadcrumb-item"><a href="'.$value.'">'.$key.'</a></li>';
      }
      
    }
    ?>
  </ol>
</nav>