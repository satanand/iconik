<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to get Jobs
	*/
	function getJobs($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10)
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(j.StartDate) >= '$FilterStartDate' AND DATE(j.EndDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (j.JobTitle LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR 
			j.EmployementType LIKE '%$Keyword%' OR 
			j.Experience LIKE '%$Keyword%' OR 
			j.Qualification LIKE '%$Keyword%' OR 
			j.Salary LIKE '%$Keyword%' OR
			c.name LIKE '%$Keyword%' OR
			s.StateName LIKE '%$Keyword%'

			) ";
		}

		/*if(isset($Where['FilterEmployementType']) && !empty($Where['FilterEmployementType']))
		{
			$FilterEmployementType = trim($Where['FilterEmployementType']);
			
			$append .= " AND (j.EmployementType = '$Keyword') ";
		}*/


		$sql = "SELECT j.*, DATE_FORMAT(j.StartDate, '%d-%m-%Y') as StartDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, s.StateName, c.name as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, COUNT(ja.ApplyID) as Responses
		FROM tbl_post_job j
		INNER JOIN set_location_states s ON j.JobLocationState = s.State_id
		INNER JOIN cities c ON j.JobLocationCity = c.id
		LEFT JOIN tbl_post_job_applications ja ON ja.PostJobID = j.PostJobID
		WHERE j.StatusID != 3 AND j.InstituteID = '$InstituteID' $append
		GROUP BY j.PostJobID
		ORDER BY j.PostJobID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function add($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		$PostJobGUID = get_guid();

		$InsertData = array_filter(array(
			"PostJobGUID"=>$PostJobGUID,
			"InstituteID"=>$InstituteID,
			"JobTitle"=>$Input['JobTitle'],
			"Description"=>$Input['Description'],
			"EmployementType"=>$Input['EmployementType'],
			"StartDate"=>date("Y-m-d",strtotime($Input['StartDate'])),
			"EndDate"=>date("Y-m-d",strtotime($Input['EndDate'])),
			"Experience"=>$Input['Experience'],
			"Qualification"=>$Input['Qualification'],
			"JobLocationState"=>$Input['JobLocationState'],
			"JobLocationCity"=>$Input['JobLocationCity'],
			"StatusID"=>$Input['JobStatus'],
			"Salary"=>$Input['Salary'],
			"EntryDate"=>date("Y-m-d H:i:s"),
			"ModifiedDate"=>date("Y-m-d H:i:s")
		));
		
		$PostJobID = $this->db->insert('tbl_post_job', $InsertData);
		

		//Add qualification if it is New---------------------------------------
		$this->db->select('QualificationID');
		$this->db->from('set_qualification');
		$this->db->where('LOWER(`Qualification`)="'.strtolower($Input['Qualification']).'"');        
        $this->db->where('StatusID != 3');	   
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			$InsertData = array(
			"Qualification"  =>	$Input['Qualification'],
			"UserID" 		=>	$EntityID,	
			"InstituteID" 		=>	$InstituteID,
			"EntryDate"  => date("Y-m-d H:i:s")
			);			
			$this->db->insert('set_qualification', $InsertData);
		}


		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function edit($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$UpdateData = array_filter(array(
			"JobTitle"=>$Input['JobTitle'],
			"Description"=>$Input['Description'],
			"EmployementType"=>$Input['EmployementType'],
			"StartDate"=>date("Y-m-d",strtotime($Input['StartDate'])),
			"EndDate"=>date("Y-m-d",strtotime($Input['EndDate'])),
			"Experience"=>$Input['Experience'],
			"Qualification"=>$Input['Qualification'],
			"JobLocationState"=>$Input['JobLocationState'],
			"JobLocationCity"=>$Input['JobLocationCity'],
			"StatusID"=>$Input['JobStatus'],
			"Salary"=>$Input['Salary'],
			"ModifiedDate"=>date("Y-m-d H:i:s")
		));
		
		$this->db->where('PostJobGUID', $Input['PostJobGUID']);
		$this->db->update('tbl_post_job', $UpdateData);
		

		//Add qualification if it is New---------------------------------------
		$this->db->select('QualificationID');
		$this->db->from('set_qualification');
		$this->db->where('LOWER(`Qualification`)="'.strtolower($Input['Qualification']).'"');        
        $this->db->where('StatusID != 3');	   
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			$InsertData = array(
			"Qualification"  =>	$Input['Qualification'],
			"UserID" 		=>	$EntityID,	
			"InstituteID" 		=>	$InstituteID,
			"EntryDate"  => date("Y-m-d H:i:s")
			);			
			$this->db->insert('set_qualification', $InsertData);
		}


		$this->db->trans_complete();
 
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function delete($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->where('PostJobGUID', $Input['PostJobGUID']);
		$this->db->where('InstituteID', $InstituteID);
		$this->db->update('tbl_post_job', array("StatusID" => 3));
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function saveFacultyStatus($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,FirstName");
		$InstEmail = $InstituteData['Email'];
		$InstName = ucwords($InstituteData['FirstName']);

		if(isset($Input['FacultyStatus']) && !empty($Input['FacultyStatus']))
		{
			$t = count($Input['FacultyStatus']);
			$e = 0;
			foreach($Input['FacultyStatus'] as $ApplyID => $statusID)
			{
				if($statusID != "")
				{					
					if($this->checkStatus($ApplyID, $statusID))
					{
						$InsertData = array("ApplyID"=>$ApplyID,
						"LogDate"=>date("Y-m-d H:i:s"),
						"LogStatus"=>$statusID);					
						$this->db->insert('tbl_post_job_applications_log', $InsertData);
					}						

					if($statusID == 11)
					{

					}
					else
					{						
						//unset($Input['FacultyName'][$ApplyID]);
						unset($Input['FacultyEmail'][$ApplyID]);
						unset($Input['FacultyMobile'][$ApplyID]);
					}

					$UpdateData = array("ApplyStatus"=>$statusID);					
					$this->db->where('ApplyID', $ApplyID);
					$this->db->update('tbl_post_job_applications', $UpdateData);
				}
				else
				{
					$e++;
					//unset($Input['FacultyName'][$ApplyID]);
					unset($Input['FacultyEmail'][$ApplyID]);
					unset($Input['FacultyMobile'][$ApplyID]);
				}	
			}

			if($t == $e)
			{
				return "Empty";
			}

			//Get Job Details-----------------------------------------
			$PostJobID = $Input['PostJobIDForStatus'];
			$sql = "SELECT j.JobTitle, DATE_FORMAT(j.StartDate, '%d-%m-%Y') as StartDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, s.StateName, c.name as CityName
			FROM tbl_post_job j
			INNER JOIN set_location_states s ON j.JobLocationState = s.State_id
			INNER JOIN cities c ON j.JobLocationCity = c.id			
			WHERE j.InstituteID = '$InstituteID' AND j.PostJobID = '$PostJobID'			
			LIMIT 1";

			$Query = $this->db->query($sql);
			
			$jdetails = "";
			if($Query->num_rows()>0)
			{
				$Result = $Query->result_array();
				
				$jdetails = $Result[0]['JobTitle']."-".$Result[0]['CityName'].', '.$Result[0]['StateName'];
			}
			
			//Send Email to faculty if email is available--------------------
			if(isset($Input['FacultyEmail']) && !empty($Input['FacultyEmail']))
			{
				foreach($Input['FacultyEmail'] as $key => $email)
				{
					$content = "Congratulation!! You are shortlisted for applied job.<br/>Job Details:<br/>".$jdetails;

					$mail = array("From_email" => $InstEmail,
						"From_name"	=> $InstName,
						"emailTo"	=> $email,
						"emailSubject"	=> "Shortlisted For Applied Job: ".$Result['JobTitle'],
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
					);

					sendMail($mail);
				}
			}


			//Send SMS to faculty if mobile number is available--------------------
			if(isset($Input['FacultyMobile']) && !empty($Input['FacultyMobile']))
			{
				foreach($Input['FacultyMobile'] as $key => $mobile)
				{
					$content = array("Message"	=> "Congratulation!! You are shortlisted for applied job. Job Details: ".$jdetails,
						"PhoneNumber"	=> $mobile);

					sendSMS($content);
				}
			}
		}		
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return "Error";
		}
		
		return "Ok";
	}


	function addFixInterview($EntityID, $Input)
	{
		//echo "<pre>"; print_r($Input); die;
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		$ApplyID = $Input['ApplyID'];
		$InterviewType = $Input['InterviewType'];

		if($InterviewType == 1)
		{
			$TelephonicInterviewDate = "";
			if(isset($Input['TelephonicInterviewDate1']) && !empty($Input['TelephonicInterviewDate1']))
			{
				$TelephonicInterviewDate = $Input['TelephonicInterviewDate1'];

				if($this->checkInterviewDate("TelInterviewDate", $TelephonicInterviewDate, $ApplyID))
				{
					return -1;
				}
			}
			elseif(isset($Input['TelephonicInterviewDate']) && !empty($Input['TelephonicInterviewDate']))
				$TelephonicInterviewDate = $Input['TelephonicInterviewDate'];

			
			$ApplyStatus = 14;
			$StatusLog = $TelephonicInterviewStatus = $Input['TelephonicInterviewStatus'];		
			if(isset($TelephonicInterviewStatus) && !empty($TelephonicInterviewStatus))
			{
				if($TelephonicInterviewStatus == 16 || $TelephonicInterviewStatus == 17 || $TelephonicInterviewStatus == 21)
					$ApplyStatus = 13;
				elseif($TelephonicInterviewStatus == 19 || $TelephonicInterviewStatus == 20)
					$ApplyStatus = $TelephonicInterviewStatus;
			}
			else
			{
				$StatusLog = 14;
			}

			
			$UpdateData = array("ApplyStatus"=>$ApplyStatus);

			if(isset($TelephonicInterviewDate) && !empty($TelephonicInterviewDate))
				$UpdateData["TelInterviewDate"] = date("Y-m-d H:i:s", strtotime($TelephonicInterviewDate));

			if(isset($TelephonicInterviewStatus) && !empty($TelephonicInterviewStatus))
				$UpdateData["TelInterviewStatus"] = $TelephonicInterviewStatus;
			
			$this->db->where('ApplyID', $ApplyID);			
			$this->db->update('tbl_post_job_applications', $UpdateData);
			

			$InsertData = array("ApplyID"=>$ApplyID,
			"LogDate"=>date("Y-m-d H:i:s"),
			"LogStatus"=>$StatusLog);		
			$this->db->insert('tbl_post_job_applications_log', $InsertData);

		}
		elseif($InterviewType == 2)
		{			
			$PersonalInterviewDate = "";
			if(isset($Input['PersonalInterviewDate1']) && !empty($Input['PersonalInterviewDate1']))
			{
				$PersonalInterviewDate = $Input['PersonalInterviewDate1'];

				if($this->checkInterviewDate("PerInterviewDate", $PersonalInterviewDate, $ApplyID))
				{
					return -1;
				}

				if($this->checkInterviewDate("TelInterviewDate", $PersonalInterviewDate, $ApplyID))
				{
					return -2;
				}
			}
			elseif(isset($Input['PersonalInterviewDate']) && !empty($Input['PersonalInterviewDate']))
			{
				$PersonalInterviewDate = $Input['PersonalInterviewDate'];

				if($this->checkInterviewDate("TelInterviewDate", $PersonalInterviewDate, $ApplyID))
				{
					return -2;
				}
			}	

			

			$ApplyStatus = 15;
			$StatusLog = $PersonalInterviewStatus = $Input['PersonalInterviewStatus'];	
			if(isset($PersonalInterviewStatus) && !empty($PersonalInterviewStatus))
			{
				if($PersonalInterviewStatus == 22 || $PersonalInterviewStatus == 23 || $PersonalInterviewStatus == 26)
					$ApplyStatus = 13;
				elseif($PersonalInterviewStatus == 25)
					$ApplyStatus = $PersonalInterviewStatus;
			}
			else
			{
				$StatusLog = 15;
			}


			$UpdateData = array("ApplyStatus"=>$ApplyStatus);

			if(isset($PersonalInterviewDate) && !empty($PersonalInterviewDate))
				$UpdateData["PerInterviewDate"] = date("Y-m-d H:i:s", strtotime($PersonalInterviewDate));

			if(isset($PersonalInterviewStatus) && !empty($PersonalInterviewStatus))
				$UpdateData["PerInterviewStatus"] = $PersonalInterviewStatus;
			
			$this->db->where('ApplyID', $ApplyID);			
			$this->db->update('tbl_post_job_applications', $UpdateData);


			$InsertData = array("ApplyID"=>$ApplyID,
			"LogDate"=>date("Y-m-d H:i:s"),
			"LogStatus"=>$StatusLog);		
			$this->db->insert('tbl_post_job_applications_log', $InsertData);
		}
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 0;
		}
		return 1;
	}

	

	
	function checkInterviewDate($Field, $Date, $ApplyID)
	{
		$Date = date("Y-m-d H:i:s", strtotime($Date));

		$sql_chk = "SELECT ApplyID
		FROM tbl_post_job_applications
		WHERE ApplyID = '$ApplyID' AND $Field >= '$Date'
		LIMIT 1";
		$Query_chk = $this->db->query($sql_chk);
		if($Query_chk->num_rows() > 0)
		{
			return true;
		}

		return false;	
	}


	function checkStatus($ApplyID, $statusID)
	{
		$sql_chk = "SELECT LogID
		FROM tbl_post_job_applications_log
		WHERE ApplyID = '$ApplyID' AND LogStatus = '$statusID'
		LIMIT 1";
		$Query_chk = $this->db->query($sql_chk);
		if($Query_chk->num_rows() <= 0)
		{
			return true;
		}

		return false;	
	}


	function getJobLog($EntityID, $Input=array())
	{
		$arr = array();

		$ApplyID = $Input['ApplyID'];

		$sql = "SELECT s.StatusName, DATE_FORMAT(l.LogDate, '%d-%M-%Y %H:%i') as LogDate
		FROM tbl_post_job_applications_log l		
		INNER JOIN set_status s ON s.StatusID = l.LogStatus
		WHERE l.ApplyID = '$ApplyID'
		ORDER BY l.LogID";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}



	/*-------------------------------------------------------------
	Section For Faculty
	-------------------------------------------------------------*/
	function getPostedJobs($EntityID="", $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(j.StartDate) >= '$FilterStartDate' AND DATE(j.EndDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.JobTitle LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR 
			j.EmployementType LIKE '%$Keyword%' OR 
			j.Experience LIKE '%$Keyword%' OR 
			j.Qualification LIKE '%$Keyword%' OR 
			j.Salary LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			c.name LIKE '%$Keyword%' OR
			s.StateName LIKE '%$Keyword%'
			) ";
		}

		if(!empty($EntityID)){
			$append .= " AND j.PostJobID NOT IN(SELECT ja.PostJobID FROM tbl_post_job_applications ja WHERE ja.FacultyID = '$EntityID')";
		}


		$sql = "SELECT j.*, DATE_FORMAT(j.StartDate, '%d-%m-%Y') as StartDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, s.StateName, c.name as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, u.FirstName as InstituteName, '' as ApplyID
		FROM tbl_post_job j
		INNER JOIN tbl_users u ON u.UserID = j.InstituteID
		INNER JOIN set_location_states s ON j.JobLocationState = s.State_id
		INNER JOIN cities c ON j.JobLocationCity = c.id				
		WHERE j.StatusID != 3 AND (DATE(j.StartDate) <= CURDATE() AND DATE(j.EndDate) >= CURDATE())  $append ORDER BY j.PostJobID DESC LIMIT $PageNo, $PageSize";

		$Query = $this->db->query($sql);  $ShortList = 0;
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				if(!empty($Where['ShortListedByUserID']) && strpos(",", $Where['ShortListedByUserID']) !== false){
					$ShortListedByUserID = explode(",", $Where['ShortListedByUserID']);
					if(in_array($EntityID, $ShortListedByUserID)){
						$Where['ShortList'] = 1;
						$ShortList++;
					}else{
						$Where['ShortList'] = 0;
					}
				}else if(!empty($Where['ShortListedByUserID']) && $Where['ShortListedByUserID'] == $EntityID){
					$Where['ShortList'] = 1;
					$ShortList++;
				}else{
					$Where['ShortList'] = 0;
				}
				$arr[] = $Where;				
			}
			$Return['Data']['ShortListCount'] = $ShortList;
			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function applyForJob($EntityID, $Input)
	{
		$AppID = 0;

		$PostJobID = $Input['PostJobID'];

		$sql = "SELECT ja.ApplyID
		FROM tbl_post_job_applications ja
		WHERE ja.PostJobID = '$PostJobID' AND ja.FacultyID = '$EntityID'
		LIMIT 1
		";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			return -1;
		}

		$MediaID = 0;
		if(!empty($Input['MediaGUID']))
		{
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];
			}
		}

		$InsertData = array(
		"PostJobID"=>$PostJobID,
		"FacultyID"=>$EntityID,
		"ApplyQualification"=>$Input['app_add_qualification'],
		"ApplyExperience"=>$Input['aap_add_experience'],
		"ApplyAge"=>$Input['aap_add_age'],
		"NoteToEmployer"=>$Input['aap_add_note_to_employer'],
		"MediaID"=>$MediaID,
		"ApplyDate"=>date("Y-m-d H:i:s"),
		"ModifiedDate"=>date("Y-m-d H:i:s")
		);
		
		$this->db->insert('tbl_post_job_applications', $InsertData);
		
		$AppID = $this->db->insert_id();


		$InsertData = array("ApplyID"=>$AppID,
		"LogDate"=>date("Y-m-d H:i:s"),
		"LogStatus"=>10);		
		$this->db->insert('tbl_post_job_applications_log', $InsertData);

		$this->db->trans_complete();

		return $AppID;
	}


	function getAppliedJobs($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(ja.ApplyDate) >= '$FilterStartDate' AND DATE(ja.ApplyDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.JobTitle LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR 
			j.EmployementType LIKE '%$Keyword%' OR 
			j.Experience LIKE '%$Keyword%' OR 
			j.Qualification LIKE '%$Keyword%' OR 
			j.Salary LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			c.name LIKE '%$Keyword%' OR
			s.StateName LIKE '%$Keyword%' OR

			ja.ApplyQualification LIKE '%$Keyword%' OR
			ja.ApplyExperience LIKE '%$Keyword%' OR
			ja.ApplyAge LIKE '%$Keyword%' OR
			ja.NoteToEmployer LIKE '%$Keyword%' OR 

			ss.StatusName LIKE '%$Keyword%'
			) ";
		}


		$sql = "SELECT j.*, DATE_FORMAT(j.StartDate, '%d-%m-%Y') as StartDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, s.StateName, c.name as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, u.FirstName as InstituteName, ss.StatusName, ja.ApplyID, DATE_FORMAT(ja.ApplyDate, '%d-%m-%Y') as ApplyDate, ja.ApplyQualification, ja.ApplyExperience, ja.ApplyAge, ja.NoteToEmployer, ja.MediaID
		FROM tbl_post_job j
		INNER JOIN tbl_users u ON u.UserID = j.InstituteID
		INNER JOIN set_location_states s ON j.JobLocationState = s.State_id
		INNER JOIN cities c ON j.JobLocationCity = c.id		
		INNER JOIN tbl_post_job_applications ja ON ja.PostJobID = j.PostJobID AND ja.FacultyID = '$EntityID'
		INNER JOIN set_status ss ON ss.StatusID = ja.ApplyStatus
		WHERE j.StatusID != 3 AND ja.FacultyID = '$EntityID' $append
		ORDER BY ja.ApplyID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();
			
			$this->load->model('Media_model');

			foreach($Query->result_array() as $Where)
			{
				if(isset($Where['MediaID']) && $Where['MediaID'] > 0)			
				{				
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'FacultyResume',"MediaID" => $Where['MediaID']),TRUE);
					$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Where['MediaID'] = $MediaID['Records'][0]['MediaURL'];
					$Where["MediaExt"] = pathinfo($MediaID['Records'][0]['MediaURL'], PATHINFO_EXTENSION);		
				}

				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getShortListedJobs($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(j.StartDate) >= '$FilterStartDate' AND DATE(j.EndDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.JobTitle LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR 
			j.EmployementType LIKE '%$Keyword%' OR 
			j.Experience LIKE '%$Keyword%' OR 
			j.Qualification LIKE '%$Keyword%' OR 
			j.Salary LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			c.name LIKE '%$Keyword%' OR
			s.StateName LIKE '%$Keyword%'
			) ";
		}

		if(!empty($EntityID)){
			$append .= " AND j.PostJobID NOT IN(SELECT ja.PostJobID FROM tbl_post_job_applications ja WHERE ja.FacultyID = '$EntityID')";
		}


		$sql = "SELECT j.*, DATE_FORMAT(j.StartDate, '%d-%m-%Y') as StartDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, s.StateName, c.name as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, u.FirstName as InstituteName, '' as ApplyID
		FROM tbl_post_job j
		INNER JOIN tbl_users u ON u.UserID = j.InstituteID
		INNER JOIN set_location_states s ON j.JobLocationState = s.State_id
		INNER JOIN cities c ON j.JobLocationCity = c.id				
		WHERE j.StatusID != 3 AND (DATE(j.StartDate) <= CURDATE() AND DATE(j.EndDate) >= CURDATE())  AND FIND_IN_SET('".$EntityID."',j.ShortListedByUserID) $append ORDER BY j.PostJobID DESC LIMIT $PageNo, $PageSize";

		$Query = $this->db->query($sql);  $ShortList = 0;
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				if(!empty($Where['ShortListedByUserID']) && strpos(",", $Where['ShortListedByUserID']) !== false){
					$ShortListedByUserID = explode(",", $Where['ShortListedByUserID']);
					if(in_array($EntityID, $ShortListedByUserID)){
						$Where['ShortList'] = 1;
						$ShortList++;
					}else{
						$Where['ShortList'] = 0;
					}
				}else if(!empty($Where['ShortListedByUserID']) && $Where['ShortListedByUserID'] == $EntityID){
					$Where['ShortList'] = 1;
					$ShortList++;
				}else{
					$Where['ShortList'] = 0;
				}
				$arr[] = $Where;				
			}
			$Return['Data']['ShortListCount'] = $ShortList;
			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;			
	}


	function getApplicants($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
		$PostJobID = $Where['PostJobID'];
		$FilterStatus = $Where['FilterStatus'];

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(ja.ApplyDate) >= '$FilterStartDate' AND DATE(ja.ApplyDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (
			u.FirstName LIKE '%$Keyword%' OR
			u.LastName LIKE '%$Keyword%' OR		

			ja.ApplyQualification LIKE '%$Keyword%' OR
			ja.ApplyExperience LIKE '%$Keyword%' OR
			ja.ApplyAge LIKE '%$Keyword%' OR
			ja.NoteToEmployer LIKE '%$Keyword%' OR 

			ss.StatusName LIKE '%$Keyword%'
			) ";
		} 


		if($FilterStatus != "")
		{
			$FilterStatus = " AND ja.ApplyStatus = '$FilterStatus' ";
		}


		$sql = "SELECT ja.*, u.FirstName, u.LastName, u.Email, u.PhoneNumberForChange, DATE_FORMAT(ja.ApplyDate, '%d-%m-%Y %H:%i') as ApplyDate, ss.StatusName, IF(ja.TelInterviewDate != '0000-00-00 00:00:00',DATE_FORMAT(ja.TelInterviewDate, '%d-%m-%Y %H:%i'),'') as TelInterviewDate, IF(ja.PerInterviewDate != '0000-00-00 00:00:00', DATE_FORMAT(ja.PerInterviewDate, '%d-%m-%Y %H:%i'),'') as PerInterviewDate, DATE_FORMAT(ja.ModifiedDate, '%d-%m-%Y %H:%i') as ModifiedDate, DATE_FORMAT(ja.TelInterviewDate, '%d-%m-%Y %H:%i') as TelInterviewDate, DATE_FORMAT(ja.PerInterviewDate, '%d-%m-%Y %H:%i') as PerInterviewDate
		FROM tbl_post_job j				
		INNER JOIN tbl_post_job_applications ja ON ja.PostJobID = j.PostJobID AND ja.PostJobID = '$PostJobID' $FilterStatus 		
		INNER JOIN tbl_users u ON u.UserID = ja.FacultyID
		INNER JOIN set_status ss ON ss.StatusID = ja.ApplyStatus
		WHERE j.InstituteID = '$InstituteID' AND j.PostJobID = '$PostJobID' $append
		ORDER BY ja.ApplyID
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();
			
			$this->load->model('Media_model');

			foreach($Query->result_array() as $Where)
			{
				if(isset($Where['MediaID']) && $Where['MediaID'] > 0)			
				{				
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'FacultyResume',"MediaID" => $Where['MediaID']),TRUE);
					$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Where['MediaID'] = $MediaID['Records'][0]['MediaURL'];
					//$Where["MediaExt"] = pathinfo($MediaID['Records'][0]['MediaURL'], PATHINFO_EXTENSION);		
				}

				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function setShortlistJobs($EntityID, $Input)
	{
		if($Input['Type'] == 0)
		{			
			$sql = $this->db->query("UPDATE  tbl_post_job SET  ShortListedByUserID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', ShortListedByUserID, ','), ',".$EntityID.",', ','))	WHERE FIND_IN_SET('".$EntityID."',ShortListedByUserID) AND  `PostJobID` = '".$Input['PostJobID']."'");

		}
		else
		{
			$sql = "SELECT ShortListedByUserID FROM tbl_post_job WHERE `PostJobID` = '".$Input['PostJobID']."' AND ( ShortListedByUserID IS NOT NULL AND ShortListedByUserID != '' )";

			$data = $this->db->query($sql);
			
			if($data->num_rows() > 0)
			{
				$sql = $this->db->query("UPDATE tbl_post_job SET `ShortListedByUserID` = CONCAT(`ShortListedByUserID`, ',".$EntityID."') WHERE `PostJobID` = '".$Input['PostJobID']."'");
			}
			else
			{
				$sql = $this->db->query("UPDATE tbl_post_job SET `ShortListedByUserID` = ".$EntityID." WHERE `PostJobID` = '".$Input['PostJobID']."'");
			}			
		}

		return TRUE;
	}


	function uploadLatestResume($EntityID, $Input)
	{
		$ApplyID = $Input['ApplyID'];

		$sql = "SELECT m.MediaName
		FROM tbl_post_job_applications ja
		INNER JOIN tbl_media m ON ja.MediaID = m.MediaID
		WHERE ja.ApplyID = '$ApplyID'
		LIMIT 1	";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows() <= 0)
		{
			return -1;
		}

		$mediaObj 	= $Query->result_array();
		$MediaName 	= $mediaObj[0]['MediaName']; 

		$MediaID = 0;
		if(!empty($Input['MediaGUID']))
		{
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];
			}
		}


		$UpdateData = array(
		"IsResumeUpdated"=>1,		
		"MediaID"=>$MediaID,
		"ModifiedDate"=>date("Y-m-d H:i:s"));
		$this->db->where('ApplyID', $ApplyID);
		$this->db->update('tbl_post_job_applications', $UpdateData);


		$InsertData = array("ApplyID"=>$ApplyID,
		"LogDate"=>date("Y-m-d H:i:s"),
		"LogStatus"=>27);		
		$this->db->insert('tbl_post_job_applications_log', $InsertData);

		$this->db->trans_complete();

		return $ApplyID;
	}

}