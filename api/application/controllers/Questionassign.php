<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionassign extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->model('Questionassign_model');
	}


	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionBank/getQuestionBankStatistics
	Input (Sample JSON): 		
	*/
	public function getQuestionAssignStatistics_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		if($this->input->post('BatchGUID') != ""){
			$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		}else if($this->input->post('CategoryGUID') != ""){
			$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}

		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('CategoryGUID') != "" && $this->input->post('BatchGUID') != ""){			
			$QuestionAssignData = $this->Questionassign_model->getQuestionAssignStatistics($this->EntityID,$this->input->post('CategoryGUID'),$this->BatchID);
		}else if($this->input->post('CategoryGUID') != ""){
			$QuestionAssignData = $this->Questionassign_model->getQuestionAssignStatistics($this->EntityID,$this->input->post('CategoryGUID'));
		}else if($this->input->post('BatchGUID') != ""){
			$QuestionAssignData = $this->Questionassign_model->getQuestionAssignStatistics($this->EntityID,'',$this->BatchID);
		}else{
			$QuestionAssignData = $this->Questionassign_model->getQuestionAssignStatistics($this->EntityID);
		}
		
		if(!empty($QuestionAssignData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $QuestionAssignData;
		}	
	}

	public function getTestFullInfoByPaperID_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|callback_validateEntityGUID[Question Assign,QtAssignID]');
		$this->form_validation->set_rules('QuestionsGroup', 'QuestionsGroup', 'trim');
		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$QuestionAssignData = $this->Questionassign_model->getTestFullInfoByPaperID($this->EntityID,$this->QtPaperID, $this->input->post(), $this->QtAssignID);

		if(!empty($QuestionAssignData)){
			$this->Return['Data'] = $QuestionAssignData;
		}	
	} 

	  

	public function getAssignedTestByMonth_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QuestionsGroup', 'QuestionsGroup', 'trim');
		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$QuestionAssignData = $this->Questionassign_model->getAssignedTestByMonth($this->EntityID,$this->input->post('QuestionsGroup'));

		if(!empty($QuestionAssignData)){
			$this->Return['Data'] = $QuestionAssignData;
		}	
	}  



	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionBank/getQuestionBankStatistics
	Input (Sample JSON): 		
	*/
	public function getQuizAssignStatistics_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$QuestionAssignData = $this->Questionassign_model->getQuizAssignStatistics($this->EntityID,array("BatchID"=>@$this->BatchID, "Keyword"=>@$this->input->post('Keyword')));

		if(!empty($QuestionAssignData)){
			$this->Return['Data'] = $QuestionAssignData;
		}	
	}  



	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionBank/getQuestionBankStatistics
	Input (Sample JSON): 		
	*/
	public function getResult_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|callback_validateEntityGUID[Question Assign,QtAssignID]');

		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('QtAssignID') != ""){			
			$QuestionAssignData = $this->Questionassign_model->getResult($this->EntityID,$this->QtAssignID);
		}else{
			$QuestionAssignData = $this->Questionassign_model->getResult($this->EntityID);
		}
		
		if(!empty($QuestionAssignData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $QuestionAssignData;
		}	
	}


	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionBank/getQuestionBankStatistics
	Input (Sample JSON): 		
	*/
	public function getBatchPapersList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SubjectGUID', 'SubjectGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('QuestionsGroup', 'Question Group', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		// if($this->input->post('SubjectGUID') != ""){
		// 	$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		// }else if($this->input->post('CourseGUID') != ""){
		// 	$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		// }

		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$QuestionAssignData = $this->Questionassign_model->getBatchPapersList($this->EntityID,array("BatchID"=>@$this->BatchID,"SubjectID"=>@$this->CategoryID,'QuestionsGroup' =>	@$this->Post['QuestionsGroup'], 'Keyword' => @$this->Post['Keyword']),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		//print_r($QuestionAssignData);
		
		if($QuestionAssignData){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $QuestionAssignData['Data'];
		}	
	}


	public function getAssignedTestByBatch_post(){  
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QuestionsGroup', 'QuestionsGroup', 'trim');
		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$QuestionAssignData = $this->Questionassign_model->getAssignedTestByBatch($this->EntityID,@$this->input->post('QuestionsGroup'));
		$this->Return['Data'] = $QuestionAssignData;
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function editAssignPaper_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|required|callback_validateEntityGUID[Question Assign,QtAssignID]');
		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('BatchGUID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('Date', 'Date', 'trim|required');		
		
		$this->form_validation->set_rules('ActivatedFromTime', 'Activated From Time', 'trim|required');
		$this->form_validation->set_rules('ActivatedToTime', 'Activated To Time', 'trim|required');

		$this->form_validation->set_rules('StartTime', 'START DATE TIME', 'trim|required');
		$this->form_validation->set_rules('EndTime', 'END DATE TIME', 'trim|required');


		$this->form_validation->validation($this);  /* Run validation */	
		/* Validation - ends */
		//print_r($_POST); die;
		$assigned = $this->Questionassign_model->editAssignPaper($this->SessionUserID, array(
			"QtAssignID" =>	$this->QtAssignID,
			"QtPaperID" =>	$this->QtPaperID,
			"Date" 		=>	$this->input->post('Date'),
			"QtAssignBatch" =>	@$this->BatchID,
			"StartTime" 		=>	$this->input->post('StartTime'),
			"EndTime" 		=>	$this->input->post('EndTime'),
			"ActivatedFrom" 		=>	$this->input->post('ActivatedFromTime'),
			"ActivatedTo" 		=>	$this->input->post('ActivatedToTime')
			));
		if($assigned){
			if(is_array($assigned) && in_array('exist', $assigned)){
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	$assigned[1]; 
			}
		}
	}


	public function getAssignedTestByID_post()
	{
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|required|callback_validateEntityGUID[Question Assign,QtAssignID]');
		$this->form_validation->validation($this);
		$assigned = $this->Questionassign_model->getAssignedTestByID($this->SessionUserID, array("QtAssignID" => $this->QtAssignID));
		if($assigned){
			$this->Return['Data'] = $assigned;
		}
	}


	public function delete_post()
	{
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|required|callback_validateEntityGUID[Question Assign,QtAssignID]');
		$this->form_validation->validation($this);
		$assigned = $this->Questionassign_model->delete($this->SessionUserID, array("QtAssignID" => $this->QtAssignID));
		if(!$assigned){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured"; 
		}
	}



	/*
	Description: 	Use to Get batch assigned to faculty and its details
	URL: 			/api/QuestionBank/getBatchAssignedFaculty
	Input (Sample JSON): 		
	*/
	public function getBatchAssignedFaculty_post()
	{		
		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$QuestionAssignData = $this->Questionassign_model->getBatchAssignedFaculty($this->EntityID);		
		
		if(isset($QuestionAssignData) && !empty($QuestionAssignData))
		{
			$this->Return['Data'] = $QuestionAssignData;
			$this->Return['Datas'] = $QuestionAssignData;		
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";			
		}
		else
		{
			$data = array();
			$this->Return['Data'] = $data;
			$this->Return['Datas'] = $data;
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found";
			
		}	
	}

}
