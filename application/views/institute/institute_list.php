<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<header class="panel-heading">
	<h1 class="h4">Manage Customers</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" id="institute-body" ng-init="getFilterData()">
	<!-- Body -->
	<!-- Top container -->
	<div class="clearfix mt-2 mb-2">
		<div class="row float-left ml-6">
		  <div class="col-md-6">
	      	<div class="form-group">
		      <span class="float-left records hidden-sm-down">
				<span class="h5">Total records: {{data.dataList.length}}</span>
			  </span>
			 </div>
		   </div>
	      <div class="col-md-3">
	      	<div class="form-group">
			 	<form class="form-inline float-left ng-pristine ng-valid ng-submitted" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()"> <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="margin-top: 0px;    width: 200px;" autocomplete="off"><span class="glyphicon glyphicon-search" style="left: 165px;
    cursor: pointer;    top: -35px;" ng-click="applyFilter()"></span> </form>
			 </div>
		  </div>     

	   </div>
		


		<button class="btn btn-success btn-sm ml-1 float-right" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Send Invitation</button>

		<button class="btn btn-success btn-sm ml-1 float-right"  ng-if="(UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-click="loadImportInstitute();">Import</button>

		<button ng-click="tableDatatoExcel()"  ng-if="data.dataList.length && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" class="btn btn-success btn-sm ml-1 float-right">Export Data to Excel</button>

		<a class="btn btn-success btn-sm ml-1 float-right" href="<?php echo base_url(); ?>order/list" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Manage Orders</a>
		<span id="dvjson"></span>		
	</div>
	<!-- Top container/ -->



	<!-- Data table -->
	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0">
		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader">
			<img src="asset/img/loader.svg">
			</p>
			<form name="records_form" id="records_form">
				<!-- data table -->
				<table class="table table-striped table-hover" id="tableData">
					<!-- table heading -->
					<thead>
						<tr>
							<th style="width: 300px;">Institute</th>
							<th >Address</th>
							<th style="width: 160px;" >Contact No.</th>
							<th style="width: 160px;" >Invited On </th>
							<!-- <th style="width: 160px;" class="text-center">Last Login</th> -->
							<th style="width: 100px;" >Login Status</th>
							<th style="width: 160px;" ng-if="UserTypeID==1">Master Institute</th>
							<!-- <th style="width: 160px;" ng-if="UserTypeID==1">Master Franchisee</th> -->
							<th style="width: 100px;" class="text-center">Action</th>
						</tr>
					</thead>
					<!-- table body -->
					<tbody>
						<tr scope="row" ng-repeat="(key, row) in data.dataList">							
								<td class="listed sm clearfix" style="height: 80px;">
									<img class="rounded-circle float-left" ng-src="{{row.ProfilePic}}">
									<div class="content float-left"><strong>{{row.FullName}}</strong>
									<div ng-if="row.Email"><a href="mailto:{{row.Email}}" target="_top">{{row.Email}}</a></div><div ng-if="!row.Email">-</div>
									<a  href="<?php echo base_url().'order/list/?UserGUID={{row.UserGUID}}'; ?>" data-toggle="tooltip" title="View Orders" style="cursor: pointer;text-decoration: underline;">View Orders</a> 
									</div>
								</td> 
								<td class="">
									<div><strong>{{row.Address}}</strong>									
										<div ng-if="row.StateName">{{row.StateName}}<span ng-if="row.CityName">,&nbsp;</span>{{row.CityName}}</div>
									</div>

								</td> 
								<td >{{row.PhoneNumber}}
									<span ng-if="row.PhoneNumber.length > 0">{{row.PhoneNumber}}</span>
									<span ng-if="row.PhoneNumber.length > 0" class="text-center text-success">
									(Active)</span>
								
									<span ng-if="row.PhoneNumberForChange.length > 0">{{row.PhoneNumberForChange}}</span>
									<span ng-if="row.PhoneNumberForChange.length > 0" class="text-center text-danger">
									(Inactive)</span>
								</td>
								<td ng-bind="row.RegisteredOn"></td>
								<!-- <td>
									<span ng-if="row.LastLoginDate">{{row.LastLoginDate}}</span>
									<span ng-if="!row.LastLoginDate">-</span>
								</td> -->
								<td class="text-center text-success" ng-if="row.StatusID==2">Active</td>
								<td class="text-center text-danger" ng-if="row.StatusID!=2">Inactive</td>
								
								<td class="text-center" ng-if="UserTypeID==1">
									<select ng-if="row.MasterFranchisee=='no'" id="institute{{key}}"  class="form-control  chosen-select" onchange="setMasterInstitute(this.value)" name="ParentCat">
										<option value="{{row.UserGUID}}&-&{{row.UserGUID}}&-&{{key}}">Select Institute</option>
										<option ng-repeat="Institute in data.dataList" ng-if="Institute.UserGUID!=row.UserGUID && Institute.Status=='Verified'" ng-selected="Institute.InstituteID==row.InstituteID"  value="{{row.UserGUID}}&-&{{Institute.UserGUID}}&-&{{key}}">{{Institute.FullName}}</option>
									</select>
								</td>
								<!-- <td class="text-center" ng-if="UserTypeID==1">
									<div ng-if="row.MasterFranchisee=='yes'">
										<select class="form-control chosen-select" onchange="updateMasterFranchisee(this.value)">
											<option value="yes&-&{{row.UserGUID}}&-&{{key}}" selected="">Yes</option>
											<option value="no&-&{{row.UserGUID}}&-&{{key}}">No</option>
										</select>
									</div>
									<div ng-if="row.MasterFranchisee=='no'">
										<select class="form-control chosen-select" onchange="updateMasterFranchisee(this.value)">
											<option value="no&-&{{row.UserGUID}}&-&{{key}}" selected="">No</option>
											<option value="yes&-&{{row.UserGUID}}&-&{{key}}">Yes</option>
										</select>
									</div>
								</td> -->
								<td class="text-center" style="text-align: center;">
									<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key,row.UserGUID)" data-toggle="tooltip" title="View Profile"></a>
									
									 <!-- <a class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEditprofile(key,row.UserGUID)"></a> -->
									<a class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEdit(key,row.UserGUID)"></a> 
								</td>
								<!-- <td class="text-center" style=""><div class="dropdown">
									<button class="btn btn-secondary  btn-sm action" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ng-if="data.UserGUID!=row.UserGUID">&#8230;</button>
									<div class="dropdown-menu dropdown-menu-left">
										<a class="dropdown-item" href="" ng-click="loadFormEditprofile(key,row.UserGUID)">Edit</a><a class="dropdown-item" href="" ng-click="loadFormEdit(key, row.UserGUID)">Change Status</a><a class="dropdown-item" href="" ng-click="loadFormDelete(key, row.UserGUID)">Delete</a>
									</div></div>
								</td> -->
							</tr>
						</tbody>
					</table>
				</form>
				<!-- no record -->
				<p class="no-records text-center" ng-if="data.noRecords">
					<span ng-if="data.dataList.length">No more records found.</span>
					<span ng-if="!data.dataList.length">No records found.</span>
				</p>
			</div>
			<!-- Data table/ -->
			<!-- add Modal -->
			<div class="modal fade" id="add_model" ng-init="getFilterData()">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title h5">Send Invitation 
								<?php //echo $this->ModuleData['ModuleName'];?>
							</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div ng-include="templateURLAdd"></div>
					</div>
				</div>
			</div>


			<!-- view profile -->
			<div class="modal fade" id="view_profile">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title h5">View Customer Details
								<?php //echo $this->ModuleData['ModuleName'];?>
							</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<!-- form -->
						<div ng-include="templateURLView"></div>
						<!-- <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit"></form> -->
						<!-- /form -->
					</div>
				</div>
			</div>


			<!-- edit profile -->
			<div class="modal fade" id="edit_profile">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title h5">Edit 
								<?php echo $this->ModuleData['ModuleName'];?>
							</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<!-- form -->
						<div ng-include="templateURLEdit"></div>
						<!-- <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit"></form> -->
						<!-- /form -->
					</div>
				</div>
			</div>


			<!-- edit status -->
			<div class="modal fade" id="edit_model">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title h5">Edit 
								<?php echo $this->ModuleData['ModuleName'];?>
							</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<!-- form -->
						<!--<div ng-include="templateURLEdit"></div>-->
						 <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit"></form> 
						<!-- /form -->
					</div>
				</div>
			</div>


			<!-- edit status -->
			<div class="modal fade" id="view_model">
				<div class="modal-dialog modal-md" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title h5">Edit 
								<?php echo $this->ModuleData['ModuleName'];?>
							</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<!-- form -->
						<div ng-include="templateURLView"></div>
						<!-- <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit"></form> -->
						<!-- /form -->
					</div>
				</div>
			</div>


			<div class="modal fade" id="import_excel_model">

				<div class="modal-dialog" role="document">

					<div class="modal-content">

						<div class="modal-header">

							<h3 class="modal-title h5">Import Institute</h3>     	

							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

						</div>

						<div ng-include="templateURLImport"></div>

					</div>

				</div>

			</div>
</div>
<!-- Body/