<style type="text/css">
.content {
/* width: 240px;*/
overflow: hidden;
word-wrap: break-word;
text-overflow: ellipsis;
line-height: 18px;
text-align: center;
}
.less {
max-height: 54px;
}
.menu-text {
/*display: block !important;*/
}
.modal { overflow: auto !important; }
.glyphicon {
position: relative;
top: -4px;
display: inline-block;
font-family: 'Glyphicons Halflings';
font-style: normal;
font-weight: normal;
line-height: 1;
-webkit-font-smoothing: antialiased;
font-size: 20px;
padding: 4px;
}
</style>
<style>
.switch {
position: relative;
display: inline-block;
width: 60px;
height: 34px;
}

.switch input { 
opacity: 0;
width: 0;
height: 0;
}

.slider {
position: absolute;
cursor: pointer;
top: 0;
left: 0;
right: 0;
bottom: 0;
background-color: #ccc;
-webkit-transition: .4s;
transition: .4s;
}

.slider:before {
position: absolute;
content: "";
height: 26px;
width: 26px;
left: 4px;
bottom: 4px;
background-color: white;
-webkit-transition: .4s;
transition: .4s;
}

input:checked + .slider {
background-color: #2196F3;
}

input:focus + .slider {
box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
-webkit-transform: translateX(26px);
-ms-transform: translateX(26px);
transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
border-radius: 34px;
}

.slider.round:before {
border-radius: 50%;
}
</style>
<header class="panel-heading">
<h1 class="h4">Manage Scholarship Paper</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getScholarshipList(0)">

<div class="row" style="margin-top: 10px;">		 
	<div class="col-md-10">
      	<div class="form-group">
	      <span class="float-left records hidden-sm-down">
			<span class="h5">Total Records: {{data.dataList.length}}</span>
		  </span>
		 </div>
	</div>
	<div class="col-md-2" style="margin-top: 0px; float: right;">
		<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormGeneratePaper(0);" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Generate New Paper</button>
	</div>	
</div>

<form id="filterForm" role="form" autocomplete="off">
<div class="row" style="margin-top: 20px;">	

	<div class="col-md-3">
		<div class="form-group">
			<input name="filterKeyword" id="filterKeyword" type="text" class="form-control" placeholder="Search by test name">
		</div>
	</div>		


	<div class="col-md-3">
		<div class="form-group">
			<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="" maxlength="50" placeholder="Schedule From Date" readonly>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="" maxlength="50" placeholder="Schedule To Date" readonly>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records('genpaper');"><?php echo SEARCH_BTN;?></button>&nbsp;
		<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records('genpaper');"><?php echo CLEAR_SEARCH_BTN;?></button>
		</div>
	</div>

</div>												
</form>


<div class="table-responsive block_pad_md" style="margin-top: 20px;" infinite-scroll="getScholarshipList(0)" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 


<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>


<table class="table table-striped table-hover">	
<thead>
<tr>
	<th style="width: 10px;">S.No</th>
	<th style="width: 150px;">Test Name</th>			
	<th style="width: 70px; text-align: center;">Easy</th>
	<th style="width: 70px; text-align: center;">Moderate</th>
	<th style="width: 70px; text-align: center;">High</th>
	<th style="width: 70px; text-align: center;">Total</th>
	<th style="width: 100px; text-align: center;">Passing Marks</th>
	<th style="width: 100px; text-align: center;">Negative Marks</th>			
	<th style="width: 150px; text-align: center;">Action</th>
</tr>
</thead>

<tbody>
<tr scope="row"  ng-repeat="(key, row) in data.dataList">
	<td>{{key+1}}</td>
	<td>{{row.QtPaperTitle}}<br/><label class="badge badge-warning">{{row.ScheduleDate | date}}</label></td>

	
	<td style="text-align: center;">
		<label ng-if="row.QuestionsLevelEasy > 0">{{row.QuestionsLevelEasy}}</label>
		<label ng-if="row.QuestionsLevelEasy <= 0">0</label>
	</td>

	<td style="text-align: center;">
		<label ng-if="row.QuestionsLevelModerate > 0">{{row.QuestionsLevelModerate}}</label>
		<label ng-if="row.QuestionsLevelModerate <= 0">0</label>
	</td>

	<td style="text-align: center;">
		<label ng-if="row.QuestionsLevelHigh > 0">{{row.QuestionsLevelHigh}}</label>
		<label ng-if="row.QuestionsLevelHigh <= 0">0</label>
	</td>
	
	<td style="text-align: center;">{{row.TotalQuestions}}</td>

	<td style="text-align: center;">{{row.PassingMarks}}</td>

	<td style="text-align: center;">
		<label ng-if="row.NegativeMarks != ''">{{row.NegativeMarks}}</label>
		<label ng-if="row.NegativeMarks == ''">0</label>
	</td>	
	
	<td class="text-center" style="text-align: center;">
		<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'scholarshiptest/view_paper/?QtPaperGUID={{row.QtPaperGUID}}'; ?>" data-toggle="tooltip" title="View Question Paper"></a> 

		<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormGeneratePaperEdit(row, row.QtPaperID)" title="Edit Question Paper"></a>						
		
		<a class="glyphicon glyphicon-trash" href="" ng-click="GeneratePaperDelete(row.QtPaperGUID, row.QuestionsGroup)" title="Delete Question Paper"></a>	

		<a class="glyphicon glyphicon-export" href="<?php echo base_url().'scholarshiptest/paper_export/?QtPaperGUID={{row.QtPaperGUID}}'; ?>"  data-toggle="tooltip" title="Export Question Paper In PDF"></a> 
	</td>
</tr>
</tbody>
</table>


<p class="no-records text-center" ng-if="!data.dataList.length">	
	<span ng-if="!data.dataList.length">No records found.</span>
</p>
</div>



<!-- add Modal -->
<div class="modal fade" id="add_model">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="modal-title h5">Generate Paper</h3>     	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<div ng-include="templateURLAdd"></div>
	</div>
</div>
</div>


<!-- edit Modal -->
<div class="modal fade" id="edit_model">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="modal-title h5">Edit Question Paper</h3>     	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<div ng-include="templateURLEdit"></div>
	</div>
</div>
</div>




<!-- add Modal -->
<div class="modal fade" id="assign_model">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="modal-title h5">Assign Question Paper</h3>     	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<div ng-include="templateURLAssign"></div>
	</div>
</div>
</div>


<div class="modal fade" id="add_subject">
<div class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="modal-title h5">Add Subjcet</h3>     	
			<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<div ng-include="subjectTemplateURLAdd"></div>
	</div>
</div>
</div>







<!-- edit Modal -->
<div class="modal fade" id="view_model">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="modal-title h5">View Question Details</h3>     	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<div ng-include="templateURLView"></div>
	</div>
</div>
</div>


<!-- delete Modal -->
<div class="modal fade" id="delete_model">
<div class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
		<!-- form -->
		<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">
		</form>
		<!-- /form -->
	</div>
</div>
</div>
</div><!-- Body/ -->