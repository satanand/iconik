<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}

	.capitalize
	{
		text-transform: capitalize;
	}
</style>  
<header class="panel-heading"> <h1 class="h4">Applicants For Part Time Jobs</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getApplicants(0);" id="content-body"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">
 		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<span class="h5">Total Jobs: {{data.dataList.length}}</span>
				</div>
			</div>	
 		</div>

		<form id="filterForm" role="form" autocomplete="off">
		<input type="hidden" name="PostJobID" id="PostJobID" value="<?php echo $_GET['PostJobID'];?>">
		<div class="row">
			<!-- <div class="col-md-2">
				<div class="form-group">
					<select class="form-control" name="FilterStatus" id="FilterStatus">		
						<option value="">All</option>
						<option value="10">Applied For Job</option>
						<option value="11">Shortlisted</option>
						<option value="12">Archived</option>
						<option value="13">Deleted</option>
					</select>
				</div>
			</div> -->

			<div class="col-md-2">
				<div class="form-group">
		            <input name="FilterStartDate" type="text" placeholder="Apply From" id="FilterStartDate" class="form-control">
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
		            <input name="FilterEndDate" type="text" placeholder="Apply To" id="FilterEndDate" class="form-control">
				</div>
			</div>				


			<div class="col-md-3">
				<div class="form-group">			 	
		        	<input type="text" class="form-control" name="FilterKeyword" id="FilterKeyword" placeholder="Search by word" >
		    	</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" title="Search" ng-click="getApplicants(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
					<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="RemoveApplicantsFilters();" title="Refresh"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
			
		</div>
	</form>		

	</div>


	<!-- Data table -->
	<form id="facultyStatusForm" name="facultyStatusForm" role="form" autocomplete="off">
	<input type="hidden" name="PostJobIDForStatus" id="PostJobIDForStatus" value="<?php echo $_GET['PostJobID'];?>">
	<div class="table-responsive block_pad_md" infinite-scroll="getApplicants();" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
		
		<table class="table table-striped table-condensed table-hover table-sortable">			
			<thead>
				<tr class="bg-green">
						<th style="width:150px;" class="text-left">Position</th>

						<th style="width: 100px;">Student</th>
						
						<th style="width: 70px;">Email</th>

						<th style="width: 100px;">Contact Number</th>

						<th style="width: 50px;">Apply Date &amp; Time</th>

						<th style="width: 50px;" class="text-center">Action</th>
				</tr>
			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr class="data-content" scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.ApplyID}}">

						<td><strong>{{row.Position}}</strong></td>

						<td>{{row.FullName}}</td>
						
						<td>{{row.Email}}</td>						

						<td ng-if="row.PhoneNumber.length > 0">{{row.PhoneNumber}}</td>

						<td ng-if="row.PhoneNumberForChange.length > 0">{{row.PhoneNumberForChange}}</td> 
				 
						<td>{{row.ApplyDate | date}}</td> 	

						<td class="text-center" style="text-align: center;">

							<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormViewApplicants(key, row.PartTimeJobID, row)" data-toggle="tooltip" title="View Job Details"></a>&nbsp;&nbsp;
								
						</td>
					</tr>
			</tbody>

		</table>
		
		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>
	</form>
	<!-- Data table/ -->

	<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5 font-weight-semibold txt-blue">Job Details</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div class="modal-body">
				  <div class="form-area">

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label"><b>Position:</b></label>
								{{formData.Position}}
							</div>
						</div>		
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label"><b>Description:</b></label>
								<p ng-bind-html="formData.Description" style="word-break: break-all;"></p>
							</div>
						</div>
					</div>

					<div class="row">					

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label"><b>Entry Date:</b></label>
								{{formData.EntryDate}}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label"><b>End Date:</b></label>
								{{formData.EndDate}}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label"><b>Salary:</b></label>
								{{formData.Salary}}
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label"><b>Job Location State:</b></label>
								{{formData.StateName}}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label"><b>Job Location City:</b></label>
								{{formData.CityName}}
							</div>
						</div>

						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label"><b>Modify Date:</b></label>
								{{formData.ModifyDate}}
							</div>
						</div>
					</div>
				  </div>
				</div>
				<!-- /form -->
				<div class="modal-header">
					<h3 class="modal-title h5 font-weight-semibold txt-blue">Student Details</h3>     	
					<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
				</div>
				<!-- form -->
				<div class="modal-body">
				  <div class="form-area">

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label"><b>Name:</b></label>
								{{formData.FullName}}
							</div>
						</div>		
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label"><b>Email:</b></label>
								{{formData.Email}}
							</div>
						</div>
					</div>

					<div class="row">					

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label"><b>Contact Number:</b></label>
								<label ng-if="formData.PhoneNumber.length > 0">{{formData.PhoneNumber}}</label>
								<label ng-if="formData.PhoneNumberForChange.length > 0">{{formData.PhoneNumberForChange}}</label>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label"><b>Apply Date:</b></label>
								{{formData.ApplyDate}}
							</div>
						</div>
					</div>
				  </div>
				</div>
				<!-- /form -->
			</div>
		</div>
	</div>
		
	<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Job</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>	


	<!-- View Modal -->
	<div class="modal fade" id="job_log_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">History</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form autocomplete="off" ng-include="templateURLJobLog"></form>
				
				<!-- /form -->
			</div>
		</div>
	</div>


	<div class="modal fade" id="fix_interview_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Fix Interview</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_fix_interview_form" name="add_fix_interview_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>	



</div><!-- Body/ -->