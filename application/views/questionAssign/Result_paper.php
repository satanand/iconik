<style type="text/css">
.jconfirm-box-container{
flex: 0 0 56.333333% !important;
max-width: 56.333333% !important;
}
</style>
<header class="panel-heading">
<h1 class="h4">Result Paper</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="viewResultPaper()">
<div class="page-wrapper">
<div class="container-fluid">
<div class="modal fade" id="assign_popup" tabindex="-1" role="dialog" aria-labelledby="assign_popupLabel1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h5 class="modal-title" id="assign_popupLabel1">ASSIGN</h5>
</div>
<div class="modal-body">
<form>
<div class="row">
<div class="col-md-6">
<div class="form-group">
<label for="recipient-name" class="control-label mb-10">Course</label>
<select class="form-control">
<option>PSC</option>
<option>SSC</option>
<option>Bank</option>
</select>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<label for="recipient-name" class="control-label mb-10">Batch id</label>
<select class="form-control">
<option>Maths</option>
<option>QA</option>
<option>GA</option>
</select>
</div>
</div>
</div>	
<div class="row">
<div class="col-md-6">
<div class="form-group">
<label for="recipient-name" class="control-label mb-10">Date</label>
<div class='input-group date' id='datetimepicker1'>
<input type='text' class="form-control" />
<span class="input-group-addon">
<span class="fa fa-calendar"></span>
</span>
</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<label class="control-label mb-10">Time</label>
<div class="row">
<div class="col-md-4">
<select class="form-control">
	<option>1</option>
	<option>2</option>
	<option>3</option>
</select>
</div>
<div class="col-md-2 pa-0">
<p class="mt-10">Hours</p>
</div>
<div class="col-md-4">
<select class="form-control">
	<option>00</option>
	<option>01</option>
	<option>02</option>
</select>
</div>
<div class="col-md-2 pa-0">
<p class="mt-10">Min</p>
</div>
</div>													
</div>
</div>
</div>									
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
<button type="button" class="btn btn-primary">Assign</button>
</div>
</div>
</div>
</div>



<!-- Row -->
<div class="row">
<div class="col-md-12">
<div class="panel panel-default card-view">
<div class="panel-wrapper">
<div class="panel-body">
<div class="row" style="margin:5px;"> 
<div class="col-md-3">
<h1 class="h6 ng-binding">Student Name : {{results.FirstName}} {{results.LastName}}</h1>
</div>								  
<div class="col-md-3">
<h1 class="h6 ng-binding">Total Questions : {{results.TotalQuestions}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Attempted : {{results.AttemptQuestions}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Skip Questions : {{results.SkipQuestions}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Correct Answers : {{results.CorrectAnswers}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Wrong Answers : {{results.WrongAnswers}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Total Marks : {{results.TotalMarks}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Passing % : {{results.PassingMarks}}</h1>
</div>
<div class="col-md-3">
<h1 class="h6 ng-binding">Obtained marks : {{results.GainedMarks}}</h1>
</div>
<div class="col-md-6">
<h1 class="h6 ng-binding">Attempted On : {{results.AttemptDate}}</h1>
</div>
</div>
<hr class="light-grey-hr" />

<div class="row">
<img src="./asset/img/ajax-loader.gif" ng-if="data.listLoading" style="position: absolute;    top: 50%;    left: 50%;    transform: translate(-50%, -50%);">
<div class="col-sm-12 col-xs-12">
<div class="form-wrap">
<table class="table">	 		
	<tbody>
	<tr ng-repeat="(key, row) in results.ResultData">
		<td>
			<b>Question {{(key + 1)}}:</b> {{row.QuestionContent}}<br/>			
			<table width="100%">
				<tr>
					<td><b>Options</b></td>
					
					<td><b>Correct Answer</b></td>
					
					<td><b>You Attempt</b></td>
					
					<td><b>Correct/Incorrect</b></td>
				</tr>

					
				<tr ng-repeat="(key1, row1) in row.answer">
					
					<td>{{row1.AnswerContent}}</td>
					
					<td><label ng-if="row1.CorrectAnswer == 1"><img src="./asset/img/correct.png"></label></td>
					
					<td><label ng-if="row.attempt == (key1 + 1) && row.attempt > 0"><img src="./asset/img/attempt.png"></label></td>
					

					<td>
						<label ng-if="row1.CorrectAnswer == 1 && row.attempt == (key1 + 1) && row.attempt > 0"><img src="./asset/img/tick.png"></label>
						
						<label ng-if="row1.CorrectAnswer == 0 && row.attempt == (key1 + 1) && row.attempt > 0"><img src="./asset/img/incorrect.png"></label>
					</td>
				</tr>
			</table>								
		</td>

		
	</tr>	
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>		
</div>
</div>
<!-- /Row -->

<p class="no-records text-center" ng-if="data.noRecords">
<span>No records found.</span>
</p>
</div>


</div>
<!-- /Main Content -->

<!-- add Modal -->
<div class="modal fade" id="add_model">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h3 class="modal-title h5">Generate Question Paper</h3>     	
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div ng-include="templateURLAdd"></div>
</div>
</div>
</div>
</div>