<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Library_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to add new category
	*/
	function addLibrary($Input=array()){

		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('B.BookID');
	    $this->db->from('tbl_library B');
	    $this->db->join('tbl_entity E', 'E.EntityGUID = B.BookGUID', 'left');
	    $this->db->where('LOWER(`AccessionNumber`)="'.strtolower($Input['AccessionNumber']).'"');
	    $this->db->where('E.InstituteID',$InstituteID);
         $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

	   		//$this->db->trans_start();
		$BookGUID = get_guid();

       

		$BookID=$this->Entity_model->addEntitys($BookGUID,array('UserID'=>$this->UserID,"EntityTypeID"=>19,'InstituteID'=>$InstituteID,"StatusID"=>2));

		if(!empty($Input['AudioGUID'])){

			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['AudioGUID']);
	        $query = $this->db->get();
			$data = $query->result_array();
	        $AudioID=$data[0]['MediaID'];

		}

		if(!empty($Input['MediaGUID'])){

			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
	        $query = $this->db->get();
			$data = $query->result_array();
			$MediaID=$data[0]['MediaID'];
		}

		if(!empty($Input['MediaGUIDBook'])){

			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUIDBook']);
	        $query = $this->db->get();
		    $data = $query->result_array();
			$BookMediaID=$data[0]['MediaID'];

		}

			/* Add Library */
			$InsertData = array_filter(array(

				"BookID"=>$BookID,
				"BookGUID"=>$BookGUID,
				"MediaID"=>@$MediaID,
				"BookMediaID"=>@$BookMediaID,
				"Title"=>@$Input['Title'],
				"AccessionNumber"=>@$Input['AccessionNumber'],
				"Author"=>@$Input['Author'],
				"CategoryID"=>@$Input['Category'],
				"Synopsis"=>@$Input['Synopsis'],
				"TotalBook"=>@$Input['TotalBook'],
				"Audio"=>@$AudioID,
				"VedioURL"=>@$Input['VedioURL'],
				"FormType"=>@$Input['FormType'],
				"DigitalType"=>@$Input['DigitalType']
				
			));
			
			$BookID=$this->db->insert('tbl_library', $InsertData);
			//echo $this->db->last_query(); die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return $BookID;
		}
	}



	/*
	Description: 	Use to update Library.
	*/
	function editLibrary($Input=array()){

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);


		$MediaID=$Input['MediaID'];
		$BookMediaID=$Input['BookMediaID'];
		$$AudioID=$Input['Audio'];

		if(!empty($Input['AudioGUID'])){

		$this->db->select('MediaID');
		$this->db->from('tbl_media');
		$this->db->where('MediaGUID',$Input['AudioGUID']);
        $query = $this->db->get();
	    $data = $query->result_array();

		$AudioID=$data[0]['MediaID'];
		}

		if(!empty($Input['MediaGUID'])){

		$this->db->select('MediaID');
		$this->db->from('tbl_media');
		$this->db->where('MediaGUID',$Input['MediaGUID']);
        $query = $this->db->get();
	    $data = $query->result_array();

		$MediaID=$data[0]['MediaID'];
 
		}
		if(!empty($Input['MediaGUIDBook'])){

		$this->db->select('MediaID');
		$this->db->from('tbl_media');
		$this->db->where('MediaGUID',$Input['MediaGUIDBook']);
        $query = $this->db->get();
	    $data = $query->result_array();

		$BookMediaID=$data[0]['MediaID'];

		}
		
		$UpdateArray = array_filter(array(
		    "MediaID"=>$MediaID,
		    "BookMediaID"=>$BookMediaID,
			"Title"=>@$Input['Title'],
			"AccessionNumber"=>@$Input['AccessionNumber'],
			"Author"=>@$Input['Author'],
			"CategoryID"=>@$Input['Category'],
			"Synopsis"=>@$Input['Synopsis'],
			"TotalBook"=>@$Input['TotalBook'],
			"Audio"=>$AudioID,
			"VedioURL"=>@$Input['VedioURL'],
			"FormType"=>@$Input['FormType'],
			"DigitalType"=>@$Input['DigitalType']
		));

		$this->db->select('B.BookID');
	    $this->db->from('tbl_library B');
	    $this->db->join('tbl_entity E', 'E.EntityGUID = B.BookGUID', 'left');
	    $this->db->where('LOWER(`AccessionNumber`)="'.strtolower($Input['AccessionNumber']).'"');
	    $this->db->where('E.InstituteID',$InstituteID);
	    $this->db->where('B.BookGUID != "'.$Input['BookGUID'].'"');
         $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

			if(!empty($UpdateArray)){
				/* Update User details to users table. */
				$this->db->where('BookGUID', $Input['BookGUID']);
				//$this->db->limit(1);
				$data=$this->db->update('tbl_library', $UpdateArray);
				//echo $this->db->last_query(); die;
			}
			return TRUE;
		}
		
	}
	
				/*
	Description: 	Use to  DeteleMedia.
	*/
	function DeteleMedia($MediaID){

		if(!empty($MediaID)){
			$this->db->where('MediaID', $MediaID);
			$data=$this->db->delete('tbl_media');
			
		}
		return TRUE;
	}
	     /*
	Description: 	Use to  DeteleMedia.
	*/
	function delete($BookGUID){

		if(!empty($BookGUID)){

			$UpdateData = array_filter(array(

				"DeleteStatus"  =>2
				
			));

			$this->db->where('BookGUID', $BookGUID);

			$this->db->update('tbl_library',$UpdateData);
			
			
		}
		return TRUE;
	}

	function AddIssueBook($data=array()){

		$originalDate = $data['IssueDate'];
		 $newDate = date("Y-m-d", strtotime($originalDate));

			$InsertData = array_filter(array(
				"BookID"  =>	$data['BookID'],
				"UserID"  =>	$data['UserID'],	
				"IssueDate" =>	$newDate,	
				"StatusID" =>	1,		
			));
			$this->db->insert('tbl_library_issue_books', $InsertData);

			$UpdateArray = array_filter(array(
		    "StatusID"=>1
			
		    ));
			
			if(!empty($UpdateArray)){
			
			$this->db->where('BookID', $data['BookID']);
		
			$data=$this->db->update('tbl_library', $UpdateArray);
		
		    }
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}

	
		return TRUE;
	}

/*add category */
	function addcategory($data=array()){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('ID');

	    $this->db->from('set_book_category');
	    
	    $this->db->where('LOWER(`Category`)="'.strtolower($data['Category']).'"');

	    $this->db->where('InstituteID',$InstituteID);

        $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

			$InsertData = array_filter(array(
				"Category"  =>	$data['Category'],
				"InstituteID"  => $InstituteID,	
			));

			$this->db->insert('set_book_category', $InsertData);
		}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}

	
		return TRUE;
	}	

	function EditReturnBook($data=array()){

		$originalDate = $data['ReturnDate'];
			$newDate = date("Y-m-d", strtotime($originalDate));

			$UpdateArray = array_filter(array(
				
				"ReturnDate" =>	$newDate,	
				"StatusID" =>	2,	
			));
			if(!empty($UpdateArray)){
			
			$this->db->where('UserID', $data['UserID']);

			$this->db->where('StatusID', 1);

			$this->db->update('tbl_library_issue_books', $UpdateArray);

			// $UpdateArrayb = array_filter(array(
		 //    "StatusID"=>2
			
		 //    ));
		 //    if(!empty($UpdateArrayb)){

   //  		    $this->db->where('BookID', $data['BookID']);
    		
   //  			$this->db->update('tbl_library', $UpdateArrayb);
		 //    }
		
		   }
			
        	$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}

	
		return TRUE;
	}





	/*
	Description: 	Use to  get LibraryBy
	*/
	function getLibraryBy($BookGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){


		$this->db->select('*');
		$this->db->from('tbl_library b');
		$this->db->join('set_book_category c','b.CategoryID=c.ID','left');
		//$this->db->join('tbl_media m','m.MediaID=b.MediaID');
		if(!empty($Where['BookGUID'])){
			$this->db->where('b.BookGUID',$Where['BookGUID']);
		}
		if(!empty($Where['FormType'])){
			$this->db->where('b.FormType',$Where['FormType']);
		}
		//$this->db->where('m.SectionID','LibraryBook');
		//$this->db->where('b.StatusID',2);
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

				$query = $this->db->query("SELECT count(BookID) as count_issue_book FROM tbl_library_issue_books where BookID = ".$Record['BookID']." and StatusID = 1");

				$data = $query->result_array();

				if($data[0]['count_issue_book'] != 0){
					$Record['count_issue_book'] = $data[0]['count_issue_book'];
					$Record['available_book'] = $Record['TotalBook'] - $Record['count_issue_book'];
					$Record['unavailable_book'] = $Record['count_issue_book'];
				}else{
					$Record['count_issue_book'] = 0;
					$Record['available_book'] = $Record['TotalBook'];
					$Record['unavailable_book'] = 0;
				}

				
				if(!empty($Record['MediaID'])){
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Library',"MediaID" => $Record['MediaID']),TRUE);
					$MediaData = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaData['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaData['Records'][0]['MediaURL'];
				}else{
					$Record['MediaThumbURL'] = "";
					$Record['MediaURL'] = "";
				}

				if(!empty($Record['BookMediaID'])){
					$BookMediaData = $this->Media_model->getMedia('',array("SectionID" => 'LibraryBook',"MediaID" => $Record['BookMediaID']),TRUE);
					$BookMediaData = ($BookMediaData ? $BookMediaData['Data'] : new stdClass());
					$Record['BMediaThumbURL'] = $BookMediaData['Records'][0]['MediaThumbURL'];
					$Record['BMediaURL'] = $BookMediaData['Records'][0]['MediaURL'];
				}else{
					$Record['BMediaThumbURL'] = "";
					$Record['BMediaURL'] = "";
				}

				if(!empty($Record['Audio'])){
					$AudioData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"MediaID" => $Record['Audio']),TRUE);
					$AudioData = ($AudioData ? $AudioData['Data'] : new stdClass());
					$Record['AudioThumbURL'] = $AudioData['Records'][0]['MediaThumbURL'];
					$Record['AudioURL'] = $AudioData['Records'][0]['MediaURL'];
				}else{
					$Record['AudioThumbURL'] = "";
					$Record['AudioURL'] = "";
				}
				

				if(!$multiRecords){

					return $Record;
				}
		
				$Records[] = $Record;
				}
			 
			
			return $Records;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to get getReturnBook
	*/
	function getReturnBook($BookGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){


		$this->db->select('b.*,bi.*,CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,c.Category');
		$this->db->from('tbl_library b');
		$this->db->join('tbl_library_issue_books bi','b.BookID=bi.BookID');
		$this->db->join('tbl_users u','u.UserID=bi.UserID');
		$this->db->join('set_book_category c','c.ID=b.CategoryID');
		$this->db->where('b.BookGUID',$Where['BookGUID']);
		$this->db->where('bi.StatusID',1);
		$this->db->where('b.StatusID',1);
		//$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				// if(!$multiRecords){
				// 	return $Record;
				// }
			 $Records[] = $Record;
			}
			
			return $Records;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to get Library
	*/
	function getLibrary($BookGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		
		$this->db->select('b.*,c.*');
		$this->db->from('tbl_library b');
		$this->db->join('tbl_entity e','e.EntityID=b.BookID','left');
		$this->db->join('set_book_category c','b.CategoryID=c.ID','left');
		//$this->db->join('tbl_library_issue_books ib','ib.BookID=b.BookID');
		$this->db->where('e.InstituteID',$InstituteID);
		 $this->db->where('b.DeleteStatus',1);

		if(!empty($Where['Keyword'])){
			$this->db->group_start();
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->like("b.Title", $Where['Keyword']);
			$this->db->or_like("b.Author", $Where['Keyword']);
			$this->db->or_like("b.AccessionNumber", $Where['Keyword']);
			$this->db->or_like("c.Category", $Where['Keyword']);
			$this->db->or_like("b.TotalBook", $Where['Keyword']);
			$this->db->or_like("b.FormType", $Where['Keyword']);
			$this->db->or_like("b.DigitalType", $Where['Keyword']);
		    $this->db->group_end();
		}

		if(!empty($Where['Category'])){

			$this->db->where('b.CategoryID',$Where['Category']);
		}
		if(!empty($Where['FormType'])){

			$this->db->where('b.FormType',$Where['FormType']);
		}
		if(!empty($Where['Author'])){

			$this->db->where('b.Author',$Where['Author']);
		}

		if(!empty($Where['BookGUID'])){
			$this->db->where('BookGUID',$Where['BookGUID']);
		}

		$this->db->order_by('b.BookID','DESC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//print_r($Query); die();
		//echo $this->db->last_query();  die();
		
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

				//echo "SELECT count(BookID) as count_isue_book FROM tbl_library_issue_books where BookID = ".$Record['BookID']." and StatusID = 1";

				$query = $this->db->query("SELECT count(BookID) as count_issue_book FROM tbl_library_issue_books where BookID = ".$Record['BookID']." and StatusID = 1");

				$data = $query->result_array();

				if($data[0]['count_issue_book'] != 0){
					$Record['count_issue_book'] = $data[0]['count_issue_book'];
					$Record['available_book'] = $Record['TotalBook'] - $Record['count_issue_book'];
					$Record['unavailable_book'] = $Record['count_issue_book'];
				}else{
					$Record['count_issue_book'] = 0;
					$Record['available_book'] = $Record['TotalBook'];
					$Record['unavailable_book'] = 0;
				}

				
				//print_r($Record);

				if(!empty($Record['MediaID'])){
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Library',"MediaID" => $Record['MediaID']),TRUE);
					$MediaData = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaData['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaData['Records'][0]['MediaURL'];
				}else{
					$Record['MediaThumbURL'] = "";
					$Record['MediaURL'] = "";
				}

				if(!empty($Record['BookMediaID'])){
					$BookMediaData = $this->Media_model->getMedia('',array("SectionID" => 'LibraryBook',"MediaID" => $Record['BookMediaID']),TRUE);
					$BookMediaData = ($BookMediaData ? $BookMediaData['Data'] : new stdClass());
					$Record['BMediaThumbURL'] = $BookMediaData['Records'][0]['MediaThumbURL'];
					$Record['BMediaURL'] = $BookMediaData['Records'][0]['MediaURL'];
				}else{
					$Record['BMediaThumbURL'] = "";
					$Record['BMediaURL'] = "";
				}

				if(!empty($Record['Audio'])){
					$AudioData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"MediaID" => $Record['Audio']),TRUE);
					$AudioData = ($AudioData ? $AudioData['Data'] : new stdClass());
					$Record['AudioThumbURL'] = $AudioData['Records'][0]['MediaThumbURL'];
					$Record['AudioURL'] = $AudioData['Records'][0]['MediaURL'];
				}else{
					$Record['AudioThumbURL'] = "";
					$Record['AudioURL'] = "";
				}
				if(!empty($Record['MediaURL'])){

                    $Record['Extension']=preg_replace('/^.*\.([^.]+)$/D', '$1', $Record['MediaURL']);
				}else{
					 $Record['Extension']='';
				}
				//print_r($Record);  die();
				// if(!$multiRecords){

				// 	return $Record;
				// }
				$Records[] = $Record;

			}

			$Return['Data']['Records'] = $Records;

			//print_r($Return); die();
			return $Return;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to get Cetegories
	*/
	function getCategory($BookGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('ID,Category');
		$this->db->from('set_book_category');
		$this->db->where('InstituteID',$InstituteID);
		$this->db->order_by('Category','ASC');
	

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}
		/*
	Description: 	Use to get Author
	*/
	function getAuthor($BookGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->db->select('b.Author');		
		$this->db->from('tbl_library b');
		$this->db->join('tbl_entity e','e.EntityID=b.BookID','left');
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('b.Author IS NOT NULL');
		$this->db->distinct('Author');
		$this->db->order_by('Title','ASC');
		
		
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return = $Records;

			return $Return;
		}
		return FALSE;		
	}	
	/*
	Description: 	Use to get Modules
	*/
	function getUserData($Input=array()){
	

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		// if(!empty($Input['BookGUID'])){
		// 	$this->db->select("BookID");
		// 	$this->db->from("");
		// 	$this->db->where();
		// }

		$this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID');
		
		$this->db->from('tbl_users u');
		$this->db->join('tbl_entity e','e.EntityID=u.UserID','left');

		if(!empty($Input['BookGUID'])){
			$this->db->where('u.UserID NOT IN (select UserID from tbl_library_issue_books where BookID = (select BookID from tbl_library where BookGUID = "'.$Input['BookGUID'].'" limit 1) and StatusID = 1)');
		}
		
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('u.UserTypeID!=',10);
		$this->db->where('u.FirstName!=','');

		if(!empty($Input['keyvalue'])){
			$this->db->like('u.FirstName=',$Input['keyvalue']);
		}
		
		$this->db->order_by('FirstName','ASC');
		$Query = $this->db->get();	//echo $this->db->last_query();
      	$data = $Query->result_array();
		if(count($data) > 0){
			foreach($data as $Record){

			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	

/*edit get data*/
	function getEDLibrary($Field='UserTypeID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select('*');
		$this->db->from('tbl_library');
		
		$this->db->order_by('UserTypeID','ASC');

		if(!empty($Where['UserTypeID'])){
			$this->db->where("UserTypeID",$Where['UserTypeID']);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			
				$Records = $Record;
			}
			//$Return['Records'] = $Records;

			return $Records;
		}
		return FALSE;		
	}


}