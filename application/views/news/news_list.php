<style type="text/css">
	.glyphicon {
    position: relative;
    top: -4px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    font-size: 20px;
    padding: 4px;
}
</style>
<header class="panel-heading"> <h1 class="h4">Manage News</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body"><!-- Body -->



	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<span class="h5">Total News: {{data.totalRecords}}</span>
				</div>
			</div>

			<div class="col-md-6" style="float: right;">
				<div class="float-right">
					<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add News</button>

				</div>
			</div>
		</div>
		
	<div class="row">
		
			<div class="col-md-4">
					<div class="form-group">
				<!--  <label class="control-label">Category</label> <br> -->
				
				 <form class="form-inline" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()">
				 	<div id="universal_search">
	                    <input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Type and search" style="" autocomplete="off">
	                </div>
				</form>
			</div>
		
			</div>

			<div class="col-md-2">
				<input type="text" name="NewsDate" class="form-control integer NewsDates" id="NewsDates" placeholder="Search By Date" readonly>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
		
		
	</div>
			

	</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 250px;">News Title</th>
					<th style="width: 200px;">Date</th>
					<th style="width: 200px;">News Image</th>
					<!-- <th >Description</th> -->
					<th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">

					<td>
						<strong>{{row.NewsName}}</strong>

					</td>
					
					<td>
						{{row.NewsDate}} 
						
					</td>
					
					<td>
						<img src="{{row.MediaThumbURL}}">

					</td>
					<!-- <td >
						<strong ng-bind-html="row.NewsDescription">{{row.NewsDescription}}</strong>
						
					</td> -->

					

					<td class="text-center" style="text-align: center;">

								<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.NewsGUID)" data-toggle="tooltip" title="View"></a>
								
								<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.NewsGUID)" data-toggle="tooltip" title="Edit"></a>
								<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormNewsDelete(key, row.NewsGUID)" data-toggle="tooltip" title="Delete"></a>

							
					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->



	<!-- add Book Category -->

	<div class="modal fade" id="addcategory_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add New Activity</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="category_form" name="category_form" autocomplete="off" ng-include="templateURLCategory">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
		<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View News</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add News</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit News</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>

<!-- issunebook_model form -->

<div class="modal fade" id="issunebook_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Book Issue </h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="issuebook_form" name="issuebook_form" autocomplete="off" ng-include="templateURLIssueBook">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

	<!-- returnbook_model form -->

<div class="modal fade" id="returnbook_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Book Return </h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="returnbook_form" name="returnbook_form" autocomplete="off" ng-include="templateURLReturnBook">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

</div><!-- Body/ -->


<script type="text/javascript">
$(function()
{
	$('#NewsDates').datepicker();
});	
</script>




