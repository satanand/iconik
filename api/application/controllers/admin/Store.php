<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends API_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Store_model');
		$this->load->model('Category_model');
	}



	/*
	Name: 			getSalesReport
	Description: 	Use to get Get list of Stores with sales figure.
	URL: 			/api_admin/store/getSalesReport	
	*/
	public function getSalesReport_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('FromDate', 'FromDate', 'trim|callback_validateDate');
		$this->form_validation->set_rules('ToDate', 'ToDate', 'trim|callback_validateDate');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$QueryString = '';
		if(!empty($this->Post['FromDate']) && !empty($this->Post['ToDate'])){
			$QueryString = " AND EntryDate BETWEEN '".$this->Post['FromDate']."' AND '".$this->Post['ToDate']."' ";
		}
		$ProductsData = $this->Store_model->getStores("
			ST.StoreName,
			(SELECT COUNT(OrderID) FROM `ecom_orders` WHERE StoreID=ST.StoreID $QueryString) TotalOrders,
			(SELECT COUNT(OrderID) FROM `ecom_orders` WHERE StoreID=ST.StoreID AND StatusID=5 $QueryString) TotalOrdersDelivered,
			(SELECT SUM(OrderPrice) FROM `ecom_orders` WHERE StoreID=ST.StoreID  AND StatusID=5 $QueryString) TotalDeliveredOrderValue,
			(SELECT SUM(OrderPrice) FROM `ecom_orders` WHERE StoreID=ST.StoreID $QueryString) OrderValue
			",
			array("StatusID"=>2),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($ProductsData){
			$this->Return['Data'] = $ProductsData['Data'];
		}	
	}







	/*
	Name: 			getBooking
	Description: 	Use to get Get list of bookings.
	URL: 			/api/store/getBooking	
	*/
	public function getBookings_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$BookingsData = $this->Store_model->getBookings('
			BookingID,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,	
			BookingOn,
			NumberOfPeople,
			EntryDate
			',
			array(),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($BookingsData){
			$this->Return['Data'] = $BookingsData['Data'];
		}	
	}

	/*
	Name: 			getBookin
	Description: 	Use to get Get single bookings.
	URL: 			/api/store/getBooking	
	*/
	public function getBooking_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('BookingID', 'BookingID', 'trim|required|callback_validateBookingID');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$BookingData = $this->Store_model->getBookings('
			BookingID,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,	
			BookingOn,
			NumberOfPeople,
			EntryDate
			',array("BookingID"=>$this->BookingID));
		if($BookingData){
			$this->Return['Data'] = $BookingData;
		}	
	}



	/*
	Description: 	Use to update booking info
	URL: 			/api_admin/store/editBooking/	
	*/
	public function editBooking_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('BookingID', 'BookingID', 'trim|required|callback_validateBookingID');
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$this->Store_model->editBooking($this->BookingID, array_merge($this->Post,array("StatusID"=>$this->StatusID)));
		


		$BookingData = $this->Store_model->getBookings('
			B.BookingID,
			U.UserID,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,	
			BookingOn,
			NumberOfPeople,
			EntryDate
			',array("BookingID"=>$this->BookingID));
		$this->Return['Data'] = $BookingData;

		if($this->StatusID==2){
			/* send notification on Accept - starts */
			$NotificationText = "Your Booking has been confirmed.";
			$this->Notification_model->addNotification('booking_confirmed', $NotificationText, $this->SessionUserID, $BookingData['UserID'], $BookingData['BookingID']);
			/* send notification on Accept - ends */
		}
		
		$this->Return['Message']      	=	"Updated successfully."; 
	}




	function validateBookingID($BookingID)
	{	
		if(empty($BookingID)){
			return TRUE;
		}
		$BookingData = $this->Store_model->getBookings('BookingID',array("BookingID"=>$BookingID));
		if($BookingData){
			$this->BookingID = $BookingData['BookingID'];
			return TRUE;
		}
		$this->form_validation->set_message('validateBookingID', 'Invalid {field}.');  
		return FALSE;
	}








	/*
	Description: 	Use to update order info.
	URL: 			/api_admin/store/editOrder/	
	*/
	public function editOrder_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim|required|callback_validateOrderGUID');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$this->Store_model->editOrder($this->OrderID, $this->Post, $this->SessionUserID, @$this->StatusID);
		
		$OrderData=$this->Store_model->getOrders('
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,
			O.OrderID,
			O.OrderGUID,
			O.OrderPrice,
			O.FirstName,
			O.LastName,
			O.Address,
			O.Address1,
			O.City,
			O.PhoneNumber,
			O.DeliveryType,
			O.PaymentMode,
			O.PaymentGateway,
			O.TransactionID,
			O.EntryDate,
			O.FromDateTime,
			O.ToDateTime,
			O.Note,
			(SELECT GROUP_CONCAT(P.ProductName SEPARATOR ", ") FROM `ecom_products` P, `ecom_order_details` OD WHERE P.ProductID=OD.ProductID AND OD.OrderID=O.OrderID) AS ProcuctNames,

			(SELECT GROUP_CONCAT(CONCAT_WS(" ",U.FirstName,U.LastName)SEPARATOR ", ") FROM `ecom_order_assigned` OA, `tbl_users` U WHERE OA.`ToUserID`=U.`UserID` AND OA.OrderID=O.OrderID) AS AssignedTo,

			(SELECT GROUP_CONCAT(CONCAT_WS(" ",U.FirstName,U.LastName)SEPARATOR ", ") FROM `ecom_order_assigned` OA, `tbl_users` U WHERE OA.`ToUserID`=U.`UserID` AND OA.OrderID=O.OrderID AND OA.StatusID=2) AS AcceptedBy 

			',array('OrderID'=>$this->OrderID));
		$this->Return['Data'] = $OrderData;


		$this->Return['Message']      	=	"Updated successfully."; 
	}



	/*
	Description: Use to get order list.
	URL: /api_admin/store/getOrders
	*/
	public function getOrders_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Status', 'Status', 'trim|in_list[Pending,Accepted,Delivered]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Orders=$this->Store_model->getOrders('
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,
			O.OrderID,
			O.OrderGUID,

			O.FromDateTime,
			O.ToDateTime,
			O.Note,	
			O.BookedDuration,	
			
			O.OrderPrice,

			O.FirstName,
			O.LastName,
			
			O.Address,
			O.Address1,
			O.City,
			O.PhoneNumber,

			O.DeliveryType,
			O.PaymentMode,
			O.PaymentGateway,
			O.TransactionID,
			O.EntryDate,
			(SELECT GROUP_CONCAT(P.ProductName SEPARATOR ", ") FROM `ecom_products` P, `ecom_order_details` OD WHERE P.ProductID=OD.ProductID AND OD.OrderID=O.OrderID) AS ProcuctNames,

			(SELECT GROUP_CONCAT(CONCAT_WS(" ",U.FirstName,U.LastName)SEPARATOR ", ") FROM `ecom_order_assigned` OA, `tbl_users` U WHERE OA.`ToUserID`=U.`UserID` AND OA.OrderID=O.OrderID) AS AssignedTo,

			(SELECT GROUP_CONCAT(CONCAT_WS(" ",U.FirstName,U.LastName)SEPARATOR ", ") FROM `ecom_order_assigned` OA, `tbl_users` U WHERE OA.`ToUserID`=U.`UserID` AND OA.OrderID=O.OrderID AND OA.StatusID=2) AS AcceptedBy 

			',array('Status'=>@$this->Post['Status'], 'StoreID'=>@$this->StoreID), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($Orders){
			$this->Return['Data'] = $Orders['Data'];
		}
	}


	/*
	Description: Use to get order list.
	URL: /api_admin/store/getOrder
	*/

	public function getOrder_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim|required|callback_validateOrderGUID');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$OrderData=$this->Store_model->getOrders('
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,
			O.OrderID,
			O.OrderGUID,
			O.OrderPrice,
			O.FirstName,
			O.LastName,
			O.Address,
			O.Address1,
			O.City,
			O.PhoneNumber,
			O.DeliveryType,
			O.PaymentMode,
			O.PaymentGateway,
			O.TransactionID,
			O.EntryDate,
			(SELECT GROUP_CONCAT(P.ProductName SEPARATOR ", ") FROM `ecom_products` P, `ecom_order_details` OD WHERE P.ProductID=OD.ProductID AND OD.OrderID=O.OrderID) AS ProcuctNames,

			(SELECT GROUP_CONCAT(CONCAT_WS(" ",U.FirstName,U.LastName)SEPARATOR ", ") FROM `ecom_order_assigned` OA, `tbl_users` U WHERE OA.`ToUserID`=U.`UserID` AND OA.OrderID=O.OrderID) AS AssignedTo,

			(SELECT GROUP_CONCAT(E.EntityGUID) FROM `ecom_order_assigned` OA, `tbl_entity` E WHERE OA.`ToUserID`=E.`EntityID` AND OA.OrderID=O.OrderID) AS AssignedToUserGUIDs,


			(SELECT GROUP_CONCAT(CONCAT_WS(" ",U.FirstName,U.LastName)SEPARATOR ", ") FROM `ecom_order_assigned` OA, `tbl_users` U WHERE OA.`ToUserID`=U.`UserID` AND OA.OrderID=O.OrderID AND OA.StatusID=2) AS AcceptedBy 

			',array('OrderID'=>$this->OrderID));
		if($OrderData){
			$this->Return['Data'] = $OrderData;
		}
	}



	/*
	Description: 	Use to add new store
	URL: 			/api_admin/addStore
	*/
	public function addStore_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreName', 'Store Name', 'trim|required');

		$this->form_validation->set_rules('Location', 'Location', 'trim');

		$this->form_validation->set_rules('IsVeg', 'IsVeg', 'trim|required|in_list[Yes,No]');
		$this->form_validation->set_rules('EstimatedDeliTime', 'Estimated Delivery Time', 'trim|required');

		$this->form_validation->set_rules('StoreOwnerGUID', 'Store Owner', 'trim|required|callback_validateEntityGUID[User,UserID]');


		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim'); /* Media GUIDs */
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$StoreData = $this->Store_model->addStore($this->SessionUserID, array_merge($this->Post,array("StoreOwnerUserID"=>$this->UserID)), $this->StatusID);
		if($StoreData){
			$this->Return['Data']['StoreGUID'] = $StoreData['StoreGUID'];

			/* check for media present - associate media with this */
			if(!empty($this->Post['MediaGUIDs'])){
				$MediaGUIDsArray[] = $this->Post['MediaGUIDs'];
			}
			if(!empty($this->Post['MediaGUID'])){
				$MediaGUIDsArray[] = $this->Post['MediaGUID'];
			}

			if(!empty($MediaGUIDsArray)){
				foreach($MediaGUIDsArray as $MediaGUID){
					$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
					if ($MediaData){
						$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $StoreData['StoreID']);
					}
				}
			}
			/* check for media present - associate media with this Post - ends */



			$this->Return['Message']      	=	"Store added successfully."; 
		}
	}



	/*
	Description: 	Use to update store details.
	URL: 			/api_admin/editStore/	
	*/
	public function editStore_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID', 'trim|required|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('StoreName', 'Store Name', 'trim|required');

		$this->form_validation->set_rules('Location', 'Location', 'trim');

		$this->form_validation->set_rules('IsVeg', 'IsVeg', 'trim|required|in_list[Yes,No]');
		$this->form_validation->set_rules('EstimatedDeliTime', 'Estimated Delivery Time', 'trim|required');	

		$this->form_validation->set_rules('StoreOwnerGUID', 'Store Owner', 'trim|required|callback_validateEntityGUID[User,UserID]');


		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim'); /* Media GUIDs */

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$this->Store_model->editStore($this->StoreID, array_merge($this->Post, array("StoreOwnerUserID"=>$this->UserID)), $this->StatusID);

		/* check for media present - associate media with this */
		if(!empty($this->Post['MediaGUIDs'])){
			$MediaGUIDsArray[] = $this->Post['MediaGUIDs'];
		}
		if(!empty($this->Post['MediaGUID'])){
			$MediaGUIDsArray[] = $this->Post['MediaGUID'];
		}


		if(!empty($MediaGUIDsArray)){
			foreach($MediaGUIDsArray as $MediaGUID){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $this->StoreID);
				}
			}
		}
		/* check for media present - associate media with this Post - ends */


		$StoreData = $this->Store_model->getStores('
			E.EntityGUID AS StoreGUID,
			E.Rating,
			ST.StoreName,
			ST.Location,		
			ST.IsVeg,
			ST.EstimatedDeliTime,
			ST.MinOrderPrice,
			(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=ST.StoreID)) AS CategoryNames',
			array("StoreID"=>$this->StoreID));
		$this->Return['Data'] = $StoreData;
		$this->Return['Message']      	=	"Updated successfully."; 
	}








	/*
	Description: 	use to get list of filters
	URL: 			/api_admin/store/getOrderFilterData	
	*/
	public function getOrderFilterData_post()
	{


		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/*Get admin staffs*/
		if(MULTISTORE){
			if($this->UserTypeID==1){
				$this->Post['UserTypeID'] = 4;
			}elseif($this->UserTypeID==4){
				$this->Post['UserTypeID'] = 3;
			}	
		}



		$UsersData=$this->Users_model->getUsers(
			'
			E.EntityGUID AS UserGUID,
			E.StatusID,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
			IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,			
			U.Email,
			U.Username,
			U.Gender,
			U.BirthDate,
			U.PhoneNumber,
			U.RewardPoints,
			UT.Name as UserTypeName,			
			E.EntryDate,
			UL.LastLoginDate			
			',array('AdminUsers' => 'Yes', 'UserTypeID' => @$this->Post['UserTypeID'], 'StoreID'=>@$this->StoreID), TRUE, 1, 100);
		if($UsersData){
			$Return['Users'] = $UsersData['Data']['Records'];
		}

		$this->Return['Data'] = $Return;
	}




	/*
	Description: 	use to get list of filters
	URL: 			/api_admin/store/getStoreFilterData	
	*/
	public function getStoreFilterData_post()
	{
		/*Get admin staffs*/
		$UsersData=$this->Users_model->getUsers(
			'
			E.EntityGUID AS UserGUID,
			CONCAT_WS(" ",U.FirstName,U.LastName) FullName			
			',array('AdminUsers' => 'Yes', 'Role' => 'Store Owner'), TRUE, 1, 100);
		if($UsersData){
			$Return['Users'] = $UsersData['Data']['Records'];
		}

		$this->Return['Data'] = $Return;
	}





	/*
	Description: 	use to get list of filters
	URL: 			/api_admin/store/getProductFilterData	
	*/
	public function getProductFilterData_post()
	{
		/*Get product categories*/
		$Categories = $this->Category_model->getCategories('',array("CategoryTypeID"=>2),true,1,250);
		if($Categories){
			$Return['Categories'] = $Categories['Data']['Records'];
			$this->Return['Data'] = $Return;					
		}

	}









	/*
	Name: 			getProducts
	Description: 	Use to get Get list of products.
	URL: 			/api/store/getProducts	
	*/
	public function getProducts_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Veg,NonVeg]');

		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');
		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$ProductsData = $this->Store_model->getProducts('
			E.EntityGUID AS ProductGUID,
			P.ProductSKU,
			P.ProductName,
			P.ProductRegPrice,		
			P.ProductBuyPrice,
			P.ProductShortDesc ProductDesc,
			P.ProductInStock,
			P.ProductBackorders,
			P.IsVeg,
			P.ProductPrepTime,
			(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=2 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryNames',
			array(
				"StoreID"		=>	@$this->StoreID,
				"CategoryID"	=>	@$this->CategoryID,
				'Keyword'		=>	@$this->Post['Keyword'],
				"Filter"		=>	@$this->Post['Filter'],
				'Latitude'		=>	@$this->Post['Latitude'],
				'Longitude'		=>	@$this->Post['Longitude']
			),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($ProductsData){
			$this->Return['Data'] = $ProductsData['Data'];
		}	
	}








	/*
	Name: 			getProduct
	Description: 	Use to get Get single product info.
	URL: 			/api/store/getProduct	
	*/
	public function getProduct_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('ProductGUID', 'ProductGUID', 'trim|callback_validateEntityGUID[Product,ProductID]');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		if(!empty($this->ProductID)){


			/* get product info */
			$ProductData = $this->Store_model->getProducts('
				E.EntityGUID AS ProductGUID,
				P.ProductSKU,
				P.ProductName,
				P.ProductRegPrice,		
				P.ProductBuyPrice,
				P.ProductDesc,
				P.ProductShortDesc,
				P.ProductInStock,
				P.ProductBackorders,
				P.IsVeg,
				P.ProductPrepTime,
				(SELECT GROUP_CONCAT(CategoryName SEPARATOR ",") FROM set_categories WHERE set_categories.CategoryTypeID=2 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryNames,

				(SELECT GROUP_CONCAT(CategoryName SEPARATOR ",") FROM set_categories WHERE set_categories.CategoryTypeID=4 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS Cuisine,

				(SELECT GROUP_CONCAT(E.EntityGUID SEPARATOR ",") FROM set_categories C, tbl_entity E WHERE C.CategoryID=E.EntityID AND C.CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryGUIDs
				',
				array("ProductID"=>$this->ProductID)
			);
			if($ProductData){
				$ProductData['AddOnProductsGUIDs'] = '';
				/*merge AddOnsProducts GUIDS to one value*/
				if(!empty($ProductData['AddOnProducts'])){
					foreach($ProductData['AddOnProducts'] as $Value){
						$ProductGUID[] = $Value['ProductGUID'];
					}
					$ProductData['AddOnProductsGUIDs'] = implode(",",$ProductGUID);
				}
				$Return['Product'] = $ProductData;
			}
		}
		/*Get navigation menu*/
		$Navigation = $this->Category_model->getCategories('',array("CategoryTypeID"=>1),true,1,250);
		if($Navigation){
			$Return['Navigation'] = $Navigation['Data']['Records'];			
		}		
		/*Get product Cuisine*/
		$Cuisine = $this->Category_model->getCategories('',array("CategoryTypeID"=>4),true,1,250);
		if($Cuisine){
			$Return['Cuisine'] = $Cuisine['Data']['Records'];			
		}
		/*To display Add-ons products list in input*/
		$ProductData = $this->Store_model->getProducts('
			E.EntityGUID ProductGUID,
			P.ProductName',
			array("StatusID"=>2, "StoreID"	=>	@$this->StoreID,),TRUE,1,255);
		if($ProductData){
			$Return['Products'] = $ProductData['Data']['Records'];			
		}
		if(!empty($Return)){
		$this->Return['Data'] = $Return;
		}

	}



	/*
	Description: 	Use to add new product.
	URL: 			/api_admin/store/editProduct/	
	*/
	public function addProduct_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('ProductSKU', 'Product SKU', 'trim');
		$this->form_validation->set_rules('ProductName', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('ProductRegPrice', 'ProductRegPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('ProductBuyPrice', 'ProductBuyPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('ProductDesc', 'ProductDesc', 'trim');
		$this->form_validation->set_rules('ProductShortDesc', 'ProductShortDesc', 'trim');

		$this->form_validation->set_rules('ProductInStock', 'ProductInStock', 'trim|integer');
		$this->form_validation->set_rules('ProductBackorders', 'ProductBackorders', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('IsVeg', 'IsVeg', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('ProductPrepTime', 'ProductPrepTime', 'trim');
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$ProductData = $this->Store_model->addProduct($this->SessionUserID, array_merge($this->Post, array("StoreID"=>@$this->StoreID)),$this->StatusID);
		if($ProductData){
			$this->Return['Data']['ProductGUID'] = $ProductData['ProductGUID'];
			$this->Return['Message']      	=	"Product added successfully."; 
		}


	}






	/*
	Description: 	Use to update product info.
	URL: 			/api_admin/store/editProduct/	
	*/
	public function editProduct_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ProductGUID', 'ProductGUID', 'trim|required|callback_validateEntityGUID[Product,ProductID]');

		$this->form_validation->set_rules('ProductSKU', 'Product SKU', 'trim');
		$this->form_validation->set_rules('ProductName', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('ProductRegPrice', 'ProductRegPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('ProductBuyPrice', 'ProductBuyPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('ProductDesc', 'ProductDesc', 'trim');
		$this->form_validation->set_rules('ProductShortDesc', 'ProductShortDesc', 'trim');

		$this->form_validation->set_rules('ProductInStock', 'ProductInStock', 'trim|integer');
		$this->form_validation->set_rules('ProductBackorders', 'ProductBackorders', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('IsVeg', 'IsVeg', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('ProductPrepTime', 'ProductPrepTime', 'trim');
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$this->Store_model->editProduct($this->ProductID, array_merge($this->Post,array("StatusID"=>$this->StatusID)), $this->SessionUserID);
		
		$ProductData = $this->Store_model->getProducts('
			E.EntityGUID ProductGUID,
			P.ProductSKU,
			P.ProductName,
			P.ProductRegPrice,		
			P.ProductBuyPrice,
			P.ProductShortDesc ProductDesc,
			P.ProductInStock,
			P.ProductBackorders,
			P.IsVeg,
			P.ProductPrepTime',
			array("ProductID"=>$this->ProductID));
		$this->Return['Data'] = $ProductData;
		$this->Return['Message']      	=	"Product updated successfully."; 
	}


	/*
	Name: 			getCoupons
	Description: 	Use to get Get list of Coupons.
	URL: 			/api/store/getCoupons	
	*/
	public function getCoupons_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CouponData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			E.EntryDate,
			E.StatusID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate,
			C.Broadcast
			',
			array("StoreID"=>@$this->StoreID),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($CouponData){
			$this->Return['Data'] = $CouponData['Data'];
		}	
	}




	/*
	Description: 	Use to add coupon.
	URL: 			/api_admin/store/addCoupon/	
	*/
	public function addCoupon_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('CouponCode', 'CouponCode', 'trim|required');
		$this->form_validation->set_rules('CouponValue', 'CouponValue', 'trim|required');
		$this->form_validation->set_rules('CouponType', 'CouponType', 'trim|required');
		$this->form_validation->set_rules('CouponTitle', 'CouponTitle', 'trim|required');
		$this->form_validation->set_rules('CouponDescription', 'CouponDescription', 'trim');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */


		$CouponData = $this->Store_model->addCoupon($this->SessionUserID, array_merge($this->Post, array("StoreID"=>$this->StoreID)),$this->StatusID);
		if($CouponData){

			/* check for media present - associate media with this Post */
			if(!empty($this->Post['MediaGUIDs'])){
				$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);
				foreach($MediaGUIDsArray as $MediaGUID){
					$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
					if ($MediaData){
						$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $CouponData['CouponID']);
					}
				}
			}
			/* check for media present - associate media with this Post - ends */

			$this->Return['Message']      	=	"Coupon added successfully."; 
		}
	}







	/*
	Description: 	Use to update coupon.
	URL: 			/api_admin/store/editCoupon/	
	*/
	public function editCoupon_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('CouponGUID', 'CouponGUID', 'trim|required|callback_validateEntityGUID[Coupon,CouponID]');
		$this->form_validation->set_rules('CouponValidTillDate', 'CouponValidTillDate', 'trim|callback_validateDate');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->Store_model->updateCoupon($this->CouponID, array_merge($this->Post,array("StatusID"=>$this->StatusID)));

		/* check for media present - associate media with this Post */
		if(!empty($this->Post['MediaGUIDs'])){
			$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);
			foreach($MediaGUIDsArray as $MediaGUID){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $this->CouponID);
				}
			}
		}
		/* check for media present - associate media with this Post - ends */

		$CouponData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			E.EntryDate,
			E.StatusID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate,
			C.Broadcast
			',
			array("CouponID"=>@$this->CouponID));

		$this->Return['Data'] = $CouponData;
		$this->Return['Message']      	=	"Status has been changed.";
	}





	/*
	Description: 	Use to broadcast coupon.
	URL: 			/api_admin/store/broadcastCoupon/	
	*/
	public function broadcastCoupon_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('CouponGUID', 'CouponGUID', 'trim|required|callback_validateEntityGUID[Coupon,CouponID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CouponData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			E.EntryDate,
			E.StatusID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate
			',
			array("CouponID"=>@$this->CouponID));


		$UsersData=$this->Users_model->getUsers(
			'
			U.UserID,	
			U.Username			
			',array('AdminUsers' =>'No'), TRUE, 1, 1000);
		if($UsersData){
			foreach($UsersData['Data']['Records'] as $Value){
				/* send notification on like - starts */
				$NotificationText = $CouponData['CouponTitle'];
				$this->Notification_model->addNotification('coupon', $NotificationText, $this->SessionUserID, $Value['UserID'], $CouponData['CouponGUID'], '', @$CouponData['Media']['Records'][0]['MediaID']);
				/* send notification on like - ends */
			}

			$this->Store_model->updateCoupon($this->CouponID, array("Broadcast"=>"Yes"));

		}


		$CouponData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			E.EntryDate,
			E.StatusID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate,
			C.Broadcast
			',
			array("CouponID"=>@$this->CouponID));

		$this->Return['Data'] = $CouponData;



	}




}
