$(function() {

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event
    var FacultyID = '';

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };




    $('#color').colorpicker(); // Colopicker
    $.ajax({

        url: API_URL + 'calendar/getFacultyList',
        type: 'post',
        data: {
            InstituteGUID: InstituteGUID,
            SessionKey: SessionKey
        },
        dataType: 'json',
        success: function(response) {

            $.each(response.Data, function(key, value) {



                var StaffID = getUrlParameter('FacultyID');

                $('select#Facultys').append($("<option/>", {

               
                    text: value.FullName,
                    value: value.UserID
                    
                }));

                if(StaffID){
                    var StaffID=StaffID;

                   // console.log(StaffID);

                     $('select#Facultys option[value="'+StaffID+'"]').attr('selected','selected');
                }
               

               
         
            });

        }

    });

    $("#Facultys").change(function() {

        var FacultyID = $("#Facultys option:selected").val();

        $('#FacultyIDs').val(FacultyID);

        window.location.href = BASE_URL+"timescheduling?FacultyID=" + FacultyID;


    });
    //alert(UserGUID);


    var current = new Date();

    var FacultyID = getUrlParameter('FacultyID');

    if (FacultyID == undefined) {
        FacultyID = '';
    }

    $('#calendar').fullCalendar({

        header: {
            left: 'prev, next, today',
            center: 'title',
            right: 'month, basicWeek, basicDay'
        },
        defaultView: 'month',
        allDay: true,
        editable: true,

        eventLimit: true, // allow "more" link when too many events
        //events: API_URL+'Calendar/test?SessionKey='+SessionKey,


        events: API_URL + 'timeschuling/getEvents?SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID + '&UserTypeID=' + UserTypeID + '&FacultyID=' + FacultyID,
        //events: array('url'=>API_URL+'calendar/getEvents','type'=>'POST'), 
        selectable: true,
        selectHelper: true,
        editable: true, // Make the event resizable true           
        select: function(start, end) {
            
            var started = moment(start).format('YYYY-MM-DD');
            var current = moment(current).format('YYYY-MM-DD');

            $('#start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
            $('#end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));

            if (started >= current) { 
                // Open modal to add event
                modal({
                    // Available buttons when adding
                    buttons: {
                        add: {
                            id: 'add-event', // Buttons id
                            css: 'btn-success', // Buttons class
                            label: 'Add' // Buttons label
                        }
                    },
                    title: 'Add Time Scheduling', // Modal title
                });


                $("#forDates").removeAttr("disabled");
                $("#forDates").val("");
                var d = moment(start).format('DD-MM-YYYY');
                $("#forDates").val(d);
                resetMultiDate(d);
                
            }
        },

        eventDrop: function(event, delta, revertFunc, start, end) {/*

            start = event.start.format('YYYY-MM-DD HH:mm:ss');
            if (event.end) {
                end = event.end.format('YYYY-MM-DD HH:mm:ss');
            } else {
                end = start;
            }

            $.post(API_URL + 'calendar/dragUpdateEvent', {
                id: event.id,
                start: start,
                end: end,
                SessionKey: SessionKey,
                InstituteGUID: InstituteGUID
            }, function(result) {
                $('.alert').addClass('alert-success').text('Event updated successfuly');
                hide_notify();


            });



        */},
        eventResize: function(event, dayDelta, minuteDelta, revertFunc) {/*

            start = event.start.format('YYYY-MM-DD HH:mm:ss');
            if (event.end) {
                end = event.end.format('YYYY-MM-DD HH:mm:ss');
            } else {
                end = start;
            }

            $.post(API_URL + 'calendar/dragUpdateEvent', {
                id: event.id,
                start: start,
                end: end,
                SessionKey: SessionKey,
                InstituteGUID: InstituteGUID
            }, function(result) {
                $('.alert').addClass('alert-success').text('Event updated successfuly');
                hide_notify();

            });
        */},

        // Event Mouseover
        eventMouseover: function(calEvent, jsEvent, view) {

            var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
            $("body").append(tooltip);

            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.event-tooltip').fadeIn('500');
                $('.event-tooltip').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                $('.event-tooltip').css('top', e.pageY + 10);
                $('.event-tooltip').css('left', e.pageX + 20);
            });
        },
        eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.event-tooltip').remove();
        },
        // Handle Existing Event Click
        eventClick: function(calEvent, jsEvent, view) {

            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;

            // Open modal to edit or delete event
            modal({
                // Available buttons when editing
                buttons: {
                    /*  delete: {
                        id: 'delete-event',
                        css: 'btn-danger',
                        label: 'Delete'
                    },*/
                    update: {
                        id: 'update-event',
                        css: 'btn-success',
                        label: 'Update'
                    }
                },
                title: 'Edit Time Scheduling "' + calEvent.BatchName + '"',
                event: calEvent
            });
        }

    });


    // Prepares the modal window according to data passed
    function modal(data) 
    {
         $("#loader").show(); 
        
        // Set modal title
        $('#batchfaculty .modal-title').html(data.title);
        // Clear buttons except Cancel
        $('.modal-footer button:not(".btn-default")').remove();

        if (data.event != '' && data.event != null) {

            Facultys(data.event.CourseID, data.event.FacultyID);

            Batchs(data.event.FacultyID, data.event.BatchID);

            Subjects(data.event.BatchID, data.event.SubjectID);

        }

        $("select#CourseID").find("option").remove();

        $('select#CourseID').find('option').remove().end().append('<option value="">Please Select</option>').val(data.event ? data.event.CourseID : '');
        $('select#FacultyID').find('option').remove().end().append('<option value="">Please Select</option>').val(data.event ? data.event.FacultyID : '');
        $('select#BatchID').find('option').remove().end().append('<option value="">Please Select</option>').val(data.event ? data.event.BatchID : '');
        $('select#SubjectID').find('option').remove().end().append('<option value="">Please Select</option>').val(data.event ? data.event.SubjectID : '');

        //var  option = "option[text=" + data.event ? data.event.FacultyID : '' + "]";
        
        var select = $('select#CourseID');
        if (select.prop) {
            var options = select.prop('options');
        } else {
            var options = select.attr('options');
        }
        $('option', select).remove();

        $.ajax({
            url: API_URL + 'admin/staff/getCourse',
            type: 'post',
            data: {
                InstituteGUID: InstituteGUID,
                SessionKey: SessionKey
            },
            dataType: 'json',
            success: function(response) 
            {
               $("select#CourseID").find("option").remove();
               
                $.each(response.Data, function(key, value) {

                    options[options.length] = new Option(value.CategoryName, value.CategoryID);

                });

                select.val(data.event ? data.event.CourseID : '');

            }

        });



        $('#inTime').val(data.event ? data.event.StartTime : '');
        $('#outTime').val(data.event ? data.event.EndTime : '');
        $('#title').val(data.event ? data.event.BatchName : '');
        $('#description').val(data.event ? data.event.description : '');
        $('#color').val(data.event ? data.event.color : '#3a87ad');
        $('#inTime, #outTime').timepicker({
            step: 30
        });

        
        $("#forDates").attr("disabled", "disabled");
        clearMultiDate();

        // Create Butttons
        $.each(data.buttons, function(index, button) {
            $('.modal-footer').prepend('<button type="button" id="' + button.id + '" class="btn ' + button.css + '">' + button.label + '</button>')
        });


        //Show Modal
        setTimeout(function() {
            $('#batchfaculty').modal('show');
            $("#loader").hide(); 
        }, 900);
    }

    // Handle Click on Add Button
    $("#CourseID").change(function() {
        var CourseID = $("#CourseID option:selected").val();
        Facultys(CourseID);
    });

    /*  */
    function Facultys(CourseID = "", FacultyID = "") {
        $('select#FacultyID').find('option').remove();
        $.ajax({
            url: API_URL + 'calendar/getFaculty',
            type: 'post',
            data: {
                InstituteGUID: InstituteGUID,
                SessionKey: SessionKey,
                CourseID: CourseID
            },
            dataType: 'json',
            success: function(response) {
                if(!response.Data.length){
                    alert("Sorry! This course is not assigned to any faculty.");
                }else{
                     $.each(response.Data, function(key, value) {
                        $('select#FacultyID').append($("<option/>", {
                            text: value.FullName,
                            value: value.UserID
                        })).val(FacultyID);
                    });
                 }               
            }
        });
    }

    $("#FacultyID").change(function() {

        var FacultyID = $("#FacultyID option:selected").val();

        if(FacultyID != "" && FacultyID > 0)
        {
            $("#Facultys").val(FacultyID);
        }

        Batchs(FacultyID);

    });


    function Batchs(FacultyID, BatchID = "") {
        $('select#BatchID').find('option').remove();
        $.ajax({

            url: API_URL + 'calendar/getBatch',
            type: 'post',
            data: {
                InstituteGUID: InstituteGUID,
                SessionKey: SessionKey,
                FacultyID: FacultyID
            },
            dataType: 'json',
            success: function(response) {
                if(!response.Data.length){
                    alert("Sorry! Batches are not assigned to this faculty.");
                }else{
                    $.each(response.Data, function(key, value) {

                        $('select#BatchID').append($("<option/>", {

                            value: value.BatchID,
                            text: value.BatchName

                        })).val(BatchID);
                    });
                }
            }

        });
    }

    $("#BatchID").change(function() {

        //$('select#SubjectID').find('option').remove();

        var BatchID = $("#BatchID option:selected").val();

        Subjects(BatchID);


    });

    function Subjects(BatchID, SubjectID = "") {
        $('select#SubjectID').find('option').remove();
        $.ajax({

            url: API_URL + 'calendar/getSubject',
            type: 'post',
            data: {
                InstituteGUID: InstituteGUID,
                SessionKey: SessionKey,
                BatchID: BatchID
            },
            dataType: 'json',
            success: function(response) {
                if(!response.Data.length){
                    alert("Sorry! Subjects list are not available for this course.");
                }else{
                    $.each(response.Data, function(key, value) {

                        $('select#SubjectID').append($("<option/>", {

                            value: value.CategoryID,
                            text: value.CategoryName

                        })).val(SubjectID.split(","));

                    });
                }

            }

        });
    }

    $('.modal').on('click', '#add-event', function(e) {

        $("#addDataLoading").show();
        $("#CancelBtn, #add-event").attr("disabled", "disabled");

        $.post(API_URL + 'calendar/addEvent', {
            CourseID: $('#CourseID').val(),
            FacultyID: $('#FacultyID').val(),
            BatchID: $('#BatchID').val(),
            SubjectID: $('#SubjectID').val(),
            StartTime: $('#inTime').val(),
            EndTime: $('#outTime').val(),
            description: $('#description').val(),
            //color: $('#color').val(),
            start: $('#start').val(),
            end: $('#end').val(),
            forDates: $('#forDates').val(),
            InstituteGUID: InstituteGUID,
            SessionKey: SessionKey,
            UserGUID: UserGUID
        }, function(response) 
        {
            
            $("#addDataLoading").hide();
            $("#CancelBtn, #add-event").removeAttr("disabled");


            if (response.ResponseCode == 200) { /* success case */

                SuccessPopup(response.Message);

                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
                hide_notify();

                window.location.href = BASE_URL+"timescheduling?FacultyID=" + $('#FacultyID').val();

            } else {

                ErrorPopup(response.Message);

            }
        });

    });


    // Handle click on Update Button
    $('.modal').on('click', '#update-event', function(e) {

        $("#addDataLoading").show();
        $("#CancelBtn, #update-event").attr("disabled", "disabled");

        $.post(API_URL + 'calendar/updateEvent', {
            id: currentEvent._id,
            CourseID: $('#CourseID').val(),
            FacultyID: $('#FacultyID').val(),
            BatchID: $('#BatchID').val(),
            SubjectID: $('#SubjectID').val(),
            StartTime: $('#inTime').val(),
            EndTime: $('#outTime').val(),
            forDates: $('#forDates').val(),
            description: $('#description').val(),
            // color: $('#color').val(),
            InstituteGUID: InstituteGUID,
            SessionKey: SessionKey,
            UserGUID: UserGUID
        }, function(response) 
        {
            $("#addDataLoading").hide();
            $("#CancelBtn, #update-event").removeAttr("disabled");

            if (response.ResponseCode == 200) { /* success case */

                SuccessPopup(response.Message);

                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
                hide_notify();

                window.location.href = BASE_URL+"timescheduling?FacultyID=" + $('#FacultyID').val();

            } else {

                ErrorPopup(response.Message);

            }
        });

    });

    function hide_notify() {
        setTimeout(function() {
            $('.alert').removeClass('alert-success').text('');
        }, 900);
    }


    $('#cp8').colorpicker({
        colorSelectors: {
            'black': '#000000',
            'white': '#ffffff',
            'red': '#FF0000',
            'default': '#777777',
            'primary': '#337ab7',
            'success': '#5cb85c',
            'info': '#5bc0de',
            'warning': '#f0ad4e',
            'danger': '#d9534f'
        }
    });



});