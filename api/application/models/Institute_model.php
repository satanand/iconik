<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institute_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}

	function addINST($Input){
		$InsertData = array_filter(array(
			"FullName" 		=>	@$Input['Name'],
			"StateName" 	=>	@$Input['State'],
			"CityName" 		=>	@$Input['City'],			
			"Course" 	=>	@$Input['Course'],
			"Rating" 	=>	@$Input['Rating'],
			"OfficeNo" => @$Input['OfficeNo'],
			"Address" 	=>	@$Input['Address'],
			"PhoneNumber" 	=>	@$Input['Mobile']
		));		
		$this->db->insert('tbl_mp_institutes', $InsertData);
		return $insert_id = $this->db->insert_id();
	}

	/*
	Description: 	ADD user to system.
	Procedures:
	1. Add user to user table and get UserID.
	2. Save login info to users_login table.
	3. Save User details to users_profile table.
	4. Genrate a Token for Email verification and save to tokens table.
	5. Send welcome Email to User with Token.
	*/
	function addUser($createdByID, $Input=array(), $UserTypeID, $SourceID, $StatusID=1){
		$this->db->trans_start();
		$EntityGUID = get_guid();

		/* Add user to entity table and get EntityID. */
		$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>1, "StatusID"=>$StatusID));

		if(!empty($createdByID)){
			$this->db->where('EntityID',$EntityID);
			$this->db->update('tbl_entity',array('InstituteID' => $EntityID, 'CreatedByUserID' => $createdByID));
		}

		/* Add user to user table . */
		if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
			$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
			unset($Input['PhoneNumber']);
		}
		$InsertData = array_filter(array(
			"UserID" 				=> 	$EntityID,
			"UserGUID" 				=> 	$EntityGUID,			
			"UserTypeID" 			=>	$UserTypeID,
			"StoreID" 				=>	@$Input['StoreID'],
			"FirstName" 			=> 	@ucfirst(strtolower($Input['FirstName'])),
			"MiddleName" 			=> 	@ucfirst(strtolower($Input['MiddleName'])),			
			"LastName" 				=> 	@ucfirst(strtolower($Input['LastName'])),
			"About" 				=>	@$Input['About'],
			"ProfilePic" 			=>	@$Input['ProfilePic'],
			"ProfileCoverPic" 		=>	@$Input['ProfileCoverPic'],			
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],
			"BirthDate" 			=>	@$Input['BirthDate'],

			"Address" 				=>	@$Input['Address'],
			"Address1" 				=>	@$Input['Address1'],
			"Postal" 				=>	@$Input['Postal'],	
			"CountryCode" 			=>	@$Input['CountryCode'],
			"StateName"				=>	@$Input['StateName'],
			"CityName"				=>	@$Input['CityName'],
			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"Latitude" 				=>	@$Input['Latitude'],
			"Longitude"				=>	@$Input['Longitude'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"PhoneNumberForChange" 	=>	@$Input['PhoneNumberForChange'],
			"Website" 				=>	@strtolower($Input['Website']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),
			"TwitterURL" 			=>	@strtolower($Input['TwitterURL']),
			"ReferredByUserID" 		=>	@$Input['Referral']->UserID,
		));

		
		$insert = $this->db->insert('tbl_users', $InsertData);

		if (!$insert && $this->db->_error_number()==1062) {
		   return $EntityID;
		} else {
		    /* Save login info to users_login table. */
			$InsertDataa = array_filter(array(
				"UserID" 		=> $EntityID,
				"SourceID"		=> $SourceID,
				"EntryDate"		=> date("Y-m-d H:i:s")));

			$this->db->insert('tbl_users_login', $InsertDataa);

			/*save user settings*/
			$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return $EntityID;
		}

		//echo $this->db->last_query();
		
	}

	function getINST($createdByID, $Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		$this->db->select('*');
		$this->db->from('tbl_mp_institutes');

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
				$this->db->group_start();
				$this->db->like("Course", $Where['Keyword']);
				$this->db->or_like("FullName", $Where['Keyword']);
				$this->db->group_end();	
		}

		if(!empty($Where['StateName'])){
			$this->db->like("StateName",$Where['StateName']);
		}

		if(!empty($Where['CityName'])){
			$this->db->like("CityName",$Where['CityName']);
		}

		$this->db->order_by('FullName','ASC');			


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die();

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
							
				if(!empty($Record['FavouriteByUserID']) && !empty($createdByID)){
					//print_r($Record['FavouriteByUserID']);	
					$FavouriteByUserID = explode(",", $Record['FavouriteByUserID']);
					//print_r($FavouriteByUserID);
					if(in_array($createdByID, $FavouriteByUserID)){
						$Record['Favourite'] = 1;
					}else if($createdByID == $Record['FavouriteByUserID']){
						$Record['Favourite'] = 1;
					}else{
						$Record['Favourite'] = 0;
					}
				}else{
					$Record['Favourite'] = 0;
				}

				if(!empty($Record['Course'])){
					$Course = str_replace(", ", ",", $Record['Course']);
					$Course = str_replace(" ,", ",", $Course);
					$Course = explode(",", $Course);
					$Course = implode($Course, ", ");
					$Record['Course'] = rtrim($Course,', ');					
				}

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function getAllInstitutesSearch($createdByID, $Input=array(), $PageNo=1, $PageSize=50)
	{		
		$PageNo = ($PageNo - 1) * $PageSize;

		$Keyword = $Input['Keyword'];
		$StateName = $Input['StateName'];
		$CityName = $Input['CityName'];		
		
		$append1 = $append2 = "";
		if(isset($Keyword) && !empty($Keyword))
		{
			$append1 .= " LOWER(U.FirstName) LIKE '%".strtolower($Keyword)."%' OR LOWER(U.LastName) LIKE '%".strtolower($Keyword)."%' ";
			//$append1 .= " OR U.Email LIKE '$Keyword%' OR U.PhoneNumber LIKE '$Keyword%' ";
			//$append1 .= " OR U.Address LIKE '$Keyword%' OR U.Postal LIKE '$Keyword%' ";
			$append1 .= " OR LOWER(U.CityName) LIKE '%".strtolower($Keyword)."%' OR LOWER(U.StateName) LIKE '%".strtolower($Keyword)."%' ";
			//$append1 .= " OR U.Website LIKE '$Keyword%' OR U.FacebookURL LIKE '$Keyword%' ";
			//$append1 .= " OR U.TwitterURL LIKE '$Keyword%' OR U.GoogleURL LIKE '$Keyword%' ";
			//$append1 .= " OR U.InstagramURL LIKE '$Keyword%' OR U.LinkedInURL LIKE '$Keyword%' ";

			$append1 = " AND ( $append1 ) ";

			$append2 .= " 			
			AND E.EntityID IN(
			SELECT E.InstituteID 
			FROM set_categories s
			INNER JOIN tbl_entity E ON E.EntityID = s.CategoryID AND E.StatusID = 2
			WHERE s.CategoryTypeID = 2 AND LOWER(s.CategoryName) = '".strtolower($Keyword)."' 
			)";
		}

		if(isset($StateName) && !empty($StateName))
		{
			$append1 .= " AND ( U.StateName = '$StateName' OR U.StateID = (select State_id from set_location_states where StateName = '$StateName' limit 1) OR (U.StateName = '$StateName' AND U.StateID = (select State_id from set_location_states where StateName = '$StateName' limit 1)) ) ";
			$append2 .= " AND ( U.StateName = '$StateName' OR U.StateID = (select State_id from set_location_states where StateName = '$StateName' limit 1) OR (U.StateName = '$StateName' AND U.StateID = (select State_id from set_location_states where StateName = '$StateName' limit 1)) ) ";
		}	

		if(isset($Input['Type']) && !empty($Input['Type']) && $Input['Type'] == "MyFavourite")
		{
			$append1 .= " AND ( FIND_IN_SET('".$createdByID."',U.FavouriteByUserID) ) ";
			$append2 .= " AND ( FIND_IN_SET('".$createdByID."',U.FavouriteByUserID) ) ";
		}


		if(isset($CityName) && !empty($CityName))
		{
			$append1 .= " AND ( U.CityName = '$CityName' ) ";
			$append2 .= " AND ( U.CityName = '$CityName' ) ";
		}

		$append1 .= " AND ( U.UserID != 10 ) ";
		$append2 .= " AND ( U.UserID != 10 ) ";

		$sql = "
		SELECT * FROM (
		SELECT U.UserGUID, U.UserID, E.EntityID,  CONCAT_WS(' ',U.FirstName,U.LastName) as FullName,U.Email,U.PhoneNumber,U.CityName,U.StateName,U.StateID,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.TelePhoneNumber,U.UrlType,U.MasterFranchisee,E.InstituteID,FavouriteByUserID, IF(U.ProfilePic IS NULL,'".BASE_URL."uploads/profile/picture/default.jpg',CONCAT('".BASE_URL."','uploads/profile/picture/',U.ProfilePic)) AS ProfilePic
		FROM tbl_users U 
		INNER JOIN tbl_entity E ON U.UserID = E.EntityID AND E.StatusID = 2
		WHERE U.UserTypeID = 10 AND E.StatusID = 2 $append1	

		UNION ALL

		SELECT U.UserGUID, U.UserID, E.EntityID,  CONCAT_WS(' ',U.FirstName,U.LastName) as FullName,U.Email,U.PhoneNumber,U.CityName,U.StateName,U.StateID,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.TelePhoneNumber,U.UrlType,U.MasterFranchisee,E.InstituteID,FavouriteByUserID, IF(U.ProfilePic IS NULL,'".BASE_URL."uploads/profile/picture/default.jpg',CONCAT('".BASE_URL."','uploads/profile/picture/',U.ProfilePic)) AS ProfilePic
		FROM tbl_users U 
		INNER JOIN tbl_entity E ON U.UserID = E.EntityID AND E.StatusID = 2
		WHERE U.UserTypeID = 10 AND E.StatusID = 2 $append2
		
		) as temp
		GROUP BY UserGUID
		ORDER BY FullName
		LIMIT $PageNo, $PageSize
		";		

		$Query = $this->db->query($sql);  //echo $this->db->last_query();
		
		$arr = array();

		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Record)
			{
				if(empty($Record['StateName']) && !empty($Record['StateID'])){
					$this->db->select('StateName');
					$this->db->from('set_location_states');
					$this->db->where("State_id",$Record['StateID']);
					$Query = $this->db->get();	
					//echo $this->db->last_query();	
					if($Query->num_rows()>0){
						$data = $Query->row();
						$Record['StateName'] = $data->StateName;
						//$instituteID->InstituteID;
					}else{
						$Record['StateName'] = "";
					}
				}
				//Check FAV-------------------------------------------
				if(!empty($Record['FavouriteByUserID']) && !empty($createdByID))
				{	
					$FavouriteByUserID = explode(",", $Record['FavouriteByUserID']);
					
					if(in_array($createdByID, $FavouriteByUserID))
					{
						$Record['Favourite'] = 1;
					}
					else if($createdByID == $Record['FavouriteByUserID'])
					{
						$Record['Favourite'] = 1;
					}
					else
					{
						$Record['Favourite'] = 0;
					}
				}
				else
				{
					$Record['Favourite'] = 0;
				}

				
				//Check Category---------------------------------------------
				$sql1 = "SELECT s.CategoryName 
				FROM set_categories s
				INNER JOIN tbl_entity E ON E.EntityID = s.CategoryID AND E.StatusID = 2
				INNER JOIN tbl_entity EP ON EP.EntityID = s.ParentCategoryID AND EP.StatusID = 2 AND EP.InstituteID = ".$Record['UserID']."
				WHERE s.CategoryTypeID = 2	
				ORDER BY s.CategoryName";
				$Query1 = $this->db->query($sql1);
				if($Query1->num_rows()>0)
				{
					$course = $Query1->result_array();
					$CategoryName = array_column($course,"CategoryName");
					$Record['Course'] =	implode(',', $CategoryName);
				}
				else
				{
					$Record["Course"] = "";
				}

				$arr[] = $Record;												
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}
	}	

	function getAllInstitutes($createdByID, $Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		if(!empty($createdByID)){
			$UserTypeID = $this->Common_model->getUserTypeByEntityID($createdByID);
			$InstituteID = $this->Common_model->getInstituteByEntity($createdByID);
		}

		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'ParentUserID'			=>	'U.ParentUserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'MasterFranchisee'				=>	'U.MasterFranchisee',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'U.StateName',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	=>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	=>	'U.CityCode',
				'UrlType'	=>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
				'CreatedByUserID' =>'E.CreatedByUserID',
				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			=>	'E.StatusID',
				'PanStatusID'				=>	'U.PanStatus',
				'BankStatusID'				=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.StateID,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.TelePhoneNumber,U.UrlType,U.MasterFranchisee,E.InstituteID,FavouriteByUserID');
		$this->db->select($Field,false);

		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}		
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		$this->db->where("U.UserID","E.EntityID", FALSE);	

		$this->db->where("U.UserTypeID",10);

		// if($UserTypeID!=1){
		// 	//$this->db->where("E.InstituteID",$createdByID);
		// 	if(!empty($createdByID)){
		// 		$this->db->where("E.CreatedByUserID",$createdByID);
		// 	}			
		// 	$this->db->where("ParentUserID IS NULL");
		// }

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
		}
		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		

		if(array_keys_exist($Params, array('CountryName'))) {
			$this->db->join('set_location_country CO', 'U.CountryCode = CO.CountryCode', 'left');
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			// if(validateEmail($Where['Keyword'])){
			// 	$Where['Email'] = $Where['Keyword'];
			// }
			// elseif(is_numeric($Where['Keyword'])){
			// 	$Where['PhoneNumber'] = $Where['Keyword'];
			// }else{
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.LastName", $Where['Keyword']);
				$this->db->or_like("U.Email", $Where['Keyword']);
				$this->db->or_like("U.PhoneNumber", $Where['Keyword']);	
				//$this->db->or_like("E.EntryDate", $Where['Keyword']);				
				$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
				$this->db->or_like("U.Address", $Where['Keyword']);
				//$this->db->or_like("U.ProfilePic", $Where['Keyword']);
				$this->db->or_like("U.Postal", $Where['Keyword']);
				$this->db->or_like("U.CityName", $Where['Keyword']);
				$this->db->or_like("U.StateName", $Where['Keyword']);
				//$this->db->or_like("U.RegistrationNumber", $Where['Keyword']);
				$this->db->or_like("U.Website", $Where['Keyword']);
				$this->db->or_like("U.FacebookURL", $Where['Keyword']);
				$this->db->or_like("U.TwitterURL", $Where['Keyword']);
				$this->db->or_like("U.GoogleURL", $Where['Keyword']);
				$this->db->or_like("U.InstagramURL", $Where['Keyword']);
				$this->db->or_like("U.LinkedInURL", $Where['Keyword']);	
				if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
	    			$this->db->or_where("E.StatusID",1);
				}			
				else if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
	    			$this->db->or_where("E.StatusID",2);
				}

				$this->db->group_end();				
			//}

		}

		if(!empty($createdByID)){
			$this->db->where("U.UserID != ".$createdByID);
		}

		if(!empty($Where['SourceID'])){
			$this->db->where("UL.SourceID",$Where['SourceID']);			
		}

		if(!empty($Where['UserTypeID'])){
			$this->db->where_in("U.UserTypeID",$Where['UserTypeID']);
		}
		
		if(!empty($Where['UserTypeIDNot']) && $Where['UserTypeIDNot']=='Yes'){
			$this->db->where("U.UserTypeID!=",$Where['UserTypeIDNot']);
		}


		if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}
		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}

		if(!empty($Where['StateName'])){
			$this->db->group_start();
			$this->db->where("U.StateID = (select State_id from set_location_states where StateName = '".$Where['StateName']."')");
			$this->db->or_where("U.StateName",$Where['StateName']);
			$this->db->group_end();
		}

		if(!empty($Where['CityName'])){
			$this->db->where("U.CityName",$Where['CityName']);
		}


		if(!empty($Where['LoginKeyword'])){
			$this->db->group_start();
			$this->db->where("U.Email",$Where['LoginKeyword']);
			$this->db->or_where("U.Username",$Where['LoginKeyword']);
			$this->db->or_where("U.PhoneNumber",$Where['LoginKeyword']);
			$this->db->group_end();
		}
		if(!empty($Where['Password'])){
			$this->db->where("UL.Password",md5($Where['Password']));
		}

		if(!empty($Where['IsAdmin'])){
			$this->db->where("UT.IsAdmin",$Where['IsAdmin']);
		}
		if(!empty($Where['StatusID'])){
			$this->db->where("E.StatusID",$Where['StatusID']);
		}
		if(!empty($Where['PanStatus'])){
			$this->db->where("U.PanStatus",$Where['PanStatus']);
		}
		if(!empty($Where['BankStatus'])){
			$this->db->where("U.BankStatus",$Where['BankStatus']);
		}

		if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
			$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		}else{
			$this->db->order_by('FullName','ASC');			
		}


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die();

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				if(empty($Record['StateName']) && !empty($Record['StateID'])){
					$this->db->select('StateName');
					$this->db->from('set_location_states');
					$this->db->where("State_id",$Record['StateID']);
					$Query = $this->db->get();	
					//echo $this->db->last_query();	
					if($Query->num_rows()>0){
						$data = $Query->row();
						$Record['StateName'] = $data->StateName;
						//$instituteID->InstituteID;
					}else{
						$Record['StateName'] = "";
					}
				}
							
				if(!empty($Record['FavouriteByUserID']) && !empty($createdByID)){
					//print_r($Record['FavouriteByUserID']);	
					$FavouriteByUserID = explode(",", $Record['FavouriteByUserID']);
					//print_r($FavouriteByUserID);
					if(in_array($createdByID, $FavouriteByUserID)){
						$Record['Favourite'] = 1;
					}else if($createdByID == $Record['FavouriteByUserID']){
						$Record['Favourite'] = 1;
					}else{
						$Record['Favourite'] = 0;
					}
				}else{
					$Record['Favourite'] = 0;
				}

				$this->db->select("CategoryName");
				$this->db->from("set_categories");
				$this->db->join("tbl_entity as E", "E.EntityID = set_categories.CategoryID");
				$this->db->join("tbl_entity as EP", "EP.EntityID = set_categories.ParentCategoryID");
				$this->db->where("E.StatusID", 2);
				$this->db->where("EP.StatusID", 2);
				$this->db->where("EP.InstituteID", $Record['UserID']);
				$this->db->where("CategoryTypeID", 2);

				// if(!empty($Where['Keyword'])){
				// 	$this->db->like("CategoryName", $Where['Keyword']);
				// }
				$query = $this->db->get();
				//echo $this->db->last_query(); //die;
				if($query->num_rows() > 0){
					$course = $query->result_array();
					$CategoryName = array_column($course,"CategoryName");
					$Record['Course'] =	implode(',', $CategoryName);
				}

				if(empty($Record['Course']) && !empty($Where['Keyword'])){					
					if (strpos($Record['FullName'], $Where['Keyword']) !== false) {
					    if(!$multiRecords){
							return $Record;
						}
						$Records[] = $Record;
					}else{
						unset($Record);
					}
				}else{
					if(!$multiRecords){
						return $Record;
					}
					$Records[] = $Record;
				}
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function getUsers($createdByID, $Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		if(!empty($createdByID)){
			$UserTypeID = $this->Common_model->getUserTypeByEntityID($createdByID);
			$InstituteID = $this->Common_model->getInstituteByEntity($createdByID);
		}

		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'ParentUserID'			=>	'U.ParentUserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'MasterFranchisee'				=>	'U.MasterFranchisee',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'U.StateName',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	=>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	=>	'U.CityCode',
				'UrlType'	=>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
				'CreatedByUserID' =>'E.CreatedByUserID',
				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			=>	'E.StatusID',
				'PanStatusID'				=>	'U.PanStatus',
				'BankStatusID'				=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.TelePhoneNumber,U.UrlType,U.MasterFranchisee,E.InstituteID,FavouriteByUserID');
		$this->db->select($Field,false);

		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}		
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		$this->db->where("U.UserID","E.EntityID", FALSE);	

		$this->db->where("U.UserTypeID",10);

		if($UserTypeID!=1){
			//$this->db->where("E.InstituteID",$createdByID);
			if(!empty($createdByID)){
				$this->db->where("E.CreatedByUserID",$createdByID);
			}			
			$this->db->where("ParentUserID IS NULL");
		}

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
		}
		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		

		if(array_keys_exist($Params, array('CountryName'))) {
			$this->db->join('set_location_country CO', 'U.CountryCode = CO.CountryCode', 'left');
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			// if(validateEmail($Where['Keyword'])){
			// 	$Where['Email'] = $Where['Keyword'];
			// }
			// elseif(is_numeric($Where['Keyword'])){
			// 	$Where['PhoneNumber'] = $Where['Keyword'];
			// }else{
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.LastName", $Where['Keyword']);
				$this->db->or_like("U.Email", $Where['Keyword']);
				$this->db->or_like("U.PhoneNumber", $Where['Keyword']);	
				$this->db->or_like("E.EntryDate", $Where['Keyword']);				
				$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
				$this->db->or_like("U.Address", $Where['Keyword']);
				$this->db->or_like("U.ProfilePic", $Where['Keyword']);
				$this->db->or_like("U.Postal", $Where['Keyword']);
				$this->db->or_like("U.CityName", $Where['Keyword']);
				$this->db->or_like("U.StateName", $Where['Keyword']);
				$this->db->or_like("U.RegistrationNumber", $Where['Keyword']);
				$this->db->or_like("U.Website", $Where['Keyword']);
				$this->db->or_like("U.FacebookURL", $Where['Keyword']);
				$this->db->or_like("U.TwitterURL", $Where['Keyword']);
				$this->db->or_like("U.GoogleURL", $Where['Keyword']);
				$this->db->or_like("U.InstagramURL", $Where['Keyword']);
				$this->db->or_like("U.LinkedInURL", $Where['Keyword']);	
				if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
	    			$this->db->or_where("E.StatusID",1);
				}			
				else if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
	    			$this->db->or_where("E.StatusID",2);
				}

				$this->db->group_end();				
			//}

		}

		if(!empty($createdByID)){
			$this->db->where("U.UserID != ".$createdByID);
		}

		if(!empty($Where['SourceID'])){
			$this->db->where("UL.SourceID",$Where['SourceID']);			
		}

		if(!empty($Where['UserTypeID'])){
			$this->db->where_in("U.UserTypeID",$Where['UserTypeID']);
		}
		
		if(!empty($Where['UserTypeIDNot']) && $Where['UserTypeIDNot']=='Yes'){
			$this->db->where("U.UserTypeID!=",$Where['UserTypeIDNot']);
		}


		if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}
		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}

		if(!empty($Where['StateName'])){
			$this->db->where("U.StateID = (select State_id from set_location_states where StateName = '".$Where['StateName']."')");
		}

		if(!empty($Where['CityName'])){
			$this->db->where("U.CityName",$Where['CityName']);
		}


		if(!empty($Where['LoginKeyword'])){
			$this->db->group_start();
			$this->db->where("U.Email",$Where['LoginKeyword']);
			$this->db->or_where("U.Username",$Where['LoginKeyword']);
			$this->db->or_where("U.PhoneNumber",$Where['LoginKeyword']);
			$this->db->group_end();
		}
		if(!empty($Where['Password'])){
			$this->db->where("UL.Password",md5($Where['Password']));
		}

		if(!empty($Where['IsAdmin'])){
			$this->db->where("UT.IsAdmin",$Where['IsAdmin']);
		}
		if(!empty($Where['StatusID'])){
			$this->db->where("E.StatusID",$Where['StatusID']);
		}
		if(!empty($Where['PanStatus'])){
			$this->db->where("U.PanStatus",$Where['PanStatus']);
		}
		if(!empty($Where['BankStatus'])){
			$this->db->where("U.BankStatus",$Where['BankStatus']);
		}

		if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
			$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		}else{
			$this->db->order_by('U.FirstName','ASC');			
		}

		$this->db->group_by('U.UserID');	

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die();

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				//print_r($createdByID);				
				if(!empty($Record['FavouriteByUserID']) && !empty($createdByID)){
					$FavouriteByUserID = implode(",", $Record['FavouriteByUserID']);
					if(in_array($createdByID, $FavouriteByUserID)){
						$Record['Favourite'] = 1;
					}else if($createdByID == $Record['FavouriteByUserID']){
						$Record['Favourite'] = 1;
					}else{
						$Record['Favourite'] = 0;
					}
				}else{
					$Record['Favourite'] = 0;
				}

				$this->db->select("CategoryName");
				$this->db->from("set_categories");
				$this->db->join("tbl_entity as E", "E.EntityID = set_categories.CategoryID");
				$this->db->join("tbl_entity as EP", "EP.EntityID = set_categories.ParentCategoryID");
				$this->db->where("E.StatusID", 2);
				$this->db->where("EP.StatusID", 2);
				$this->db->where("EP.InstituteID", $Record['UserID']);
				$this->db->where("CategoryTypeID", 2);

				if(!empty($Where['Course'])){
					$this->db->like("CategoryName", $Where['Course']);
				}
				$query = $this->db->get();
				//echo $this->db->last_query(); //die;
				if($query->num_rows() > 0){
					$course = $query->result_array();
					$Record['Course'] =	implode(',', $course[0]);
				}

				if(empty($Record['Course']) && !empty($Where['Course'])){
					unset($Record);
				}else{
					if(!$multiRecords){
						return $Record;
					}
					$Records[] = $Record;
				}

				//else{
					
				//}
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function exportInstitutes($createdByID, $Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/

		$UserTypeID = $this->Common_model->getUserTypeByEntityID($createdByID);

		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'MasterFranchisee'				=>	'U.MasterFranchisee',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'U.StateName',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	=>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	=>	'U.CityCode',
				'UrlType'	=>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
				'CreatedByUserID' =>'E.CreatedByUserID',
				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			=>	'E.StatusID',
				'PanStatusID'				=>	'U.PanStatus',
				'BankStatusID'				=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		$this->db->select('U.InstituteName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.Address,U.Postal,U.RegistrationNumber,U.CityCode,U.TelePhoneNumber,U.MasterFranchisee');
		$this->db->select($Field,false);

		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}		
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		$this->db->where("U.UserID","E.EntityID", FALSE);	

		$this->db->where("U.UserTypeID",10);

		if($UserTypeID!=1){
			$this->db->where("E.InstituteID",$createdByID);
		}

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
		}
		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		

		if(array_keys_exist($Params, array('CountryName'))) {
			$this->db->join('set_location_country CO', 'U.CountryCode = CO.CountryCode', 'left');
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			if(validateEmail($Where['Keyword'])){
				$Where['Email'] = $Where['Keyword'];
			}
			elseif(is_numeric($Where['Keyword'])){
				$Where['PhoneNumber'] = $Where['Keyword'];
			}else{
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.LastName", $Where['Keyword']);
				$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
				$this->db->group_end();				
			}

		}

		if(!empty($Where['SourceID'])){
			$this->db->where("UL.SourceID",$Where['SourceID']);			
		}

		if(!empty($Where['UserTypeID'])){
			$this->db->where_in("U.UserTypeID",$Where['UserTypeID']);
		}
		
		if(!empty($Where['UserTypeIDNot']) && $Where['UserTypeIDNot']=='Yes'){
			$this->db->where("U.UserTypeID!=",$Where['UserTypeIDNot']);
		}


		if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}
		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}

		if(!empty($Where['LoginKeyword'])){
			$this->db->group_start();
			$this->db->where("U.Email",$Where['LoginKeyword']);
			$this->db->or_where("U.Username",$Where['LoginKeyword']);
			$this->db->or_where("U.PhoneNumber",$Where['LoginKeyword']);
			$this->db->group_end();
		}
		if(!empty($Where['Password'])){
			$this->db->where("UL.Password",md5($Where['Password']));
		}

		if(!empty($Where['IsAdmin'])){
			$this->db->where("UT.IsAdmin",$Where['IsAdmin']);
		}
		if(!empty($Where['StatusID'])){
			$this->db->where("E.StatusID",$Where['StatusID']);
		}
		if(!empty($Where['PanStatus'])){
			$this->db->where("U.PanStatus",$Where['PanStatus']);
		}
		if(!empty($Where['BankStatus'])){
			$this->db->where("U.BankStatus",$Where['BankStatus']);
		}

		if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
			$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		}else{
			$this->db->order_by('U.FirstName','ASC');			
		}


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die();

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function setFavourite($EntityID,$UserID){
		$this->db->select("FavouriteByUserID");
		$this->db->from('tbl_users');
		$this->db->where('UserID',$UserID);
		$this->db->where('FavouriteByUserID IS NOT NULL');
		$this->db->limit(1);
		$data = $this->db->get();
		
		if($data->num_rows() > 0){
			$sql = $this->db->query("UPDATE tbl_users SET `FavouriteByUserID` = CONCAT(`FavouriteByUserID`, ',".$EntityID."') WHERE `UserID` = ".$UserID);
			return TRUE;
		}else{
			$sql = $this->db->query("UPDATE tbl_users SET `FavouriteByUserID` = ".$EntityID." WHERE `UserID` = ".$UserID);
			return TRUE;
		}

		$UserData = $this->Common_model->getInstituteDetailByID($EntityID,"Email,UserID,FirstName");
		$InstituteData = $this->Common_model->getInstituteDetailByID($UserID,"Email,UserID,FirstName");

		$content = $this->load->view('emailer/common', 
				array("Name" => $InstituteData['FirstName'],'EmailText' => "Congratulations! You have received one more favourite by ".$UserData['FirstName']) , TRUE);
		sendMail(array(
			'emailTo' => $InstituteData['Email'],
			'emailSubject' => "Set as Favourite",
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
		));
	}



	function getCourses()
	{
		$this->db->select("CategoryName,CategoryGUID,CategoryID");
		$this->db->from("set_categories");
		$this->db->join("tbl_entity as E", "E.EntityID = set_categories.CategoryID");
		$this->db->join("tbl_entity as EP", "EP.EntityID = set_categories.ParentCategoryID");
		$this->db->where("E.StatusID", 2);
		$this->db->where("EP.StatusID", 2);
		$this->db->where("CategoryTypeID", 2);

		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$course = $query->result_array();
			return 	$course;
		}
		return FALSE;

   } 

}

