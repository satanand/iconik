app.controller('PageController', function ($scope, $http,$timeout){



    $scope.data.pageSize = 100;

    $scope.data.dataList = [];

    $scope.data.dataLists = [];

    $scope.APIURL = API_URL + "upload/image";
    $scope.APIURLFILE = API_URL + "upload/file";

    $scope.Approved = 0;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    /*----------------*/



    $scope.getFilterData = function (check=0)



    {

            $scope.getSecondLevelPermission();
            
            var CourseGUID = $scope.getUrlParameter('parentCategoryGUID');

            var BatchGUID = $scope.getUrlParameter('BatchGUID'); 

            $scope.getCourses();

            $scope.getBatchByCourse(CourseGUID);

            $scope.getFeePlans(CourseGUID);

            $scope.getStates();            

            $scope.CourseGUID = $scope.getUrlParameter('parentCategoryGUID');

            $scope.BatchGUID = $scope.getUrlParameter('BatchGUID');
            //alert(check);
            if(check==0){
                $scope.getStudentStatistics();
            }


            // $scope.getStudentStatistics();

            


     //    var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();



     //    $http.post(API_URL+'admin/category/getFilterData', data, contentType).then(function(response) {



     //        var response = response.data;



     //        if(response.ResponseCode==200 && response.Data){ /* success case */



     //         $scope.filterData =  response.Data;



     //         $timeout(function(){



     //            $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");



     //        }, 300);          



     //     }



     // });



    }



    $scope.getBatchAndPlans = function(CourseGUID){
        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

        $scope.data.batch = [];

        $http.post(API_URL+'keys/getBatchByCourse', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200 && response.Data.length){ /* success case */



                for (var i in response.Data) {



                     $scope.data.batch.push(response.Data[i]);



                }

                 $timeout(function(){            



                       $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



                   }, 200);

            }else{
                ErrorPopup("Please add batch before importing the data.");
            }

        });
    }



    $scope.getBatchByCourse = function(CourseGUID){

        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

        $scope.data.batch = [];

        $http.post(API_URL+'keys/getBatchByCourse', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                for (var i in response.Data) {

                     $scope.data.batch.push(response.Data[i]);
                }

                $timeout(function(){ 
                       $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                }, 200);

            }

        });

    }






    $scope.getBatchByCourse = function(CourseGUID){

        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

        $scope.data.batch = [];

        $http.post(API_URL+'keys/getBatchByCourse', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200 && response.Data){ /* success case */



                for (var i in response.Data) {



                     $scope.data.batch.push(response.Data[i]);



                }

                 $timeout(function(){            



                       $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



                   }, 200);

            }

        });

    }











    /*list*/



    $scope.applyFilter = function (check=0)



    {



       // $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

       if(check==1){
         $scope.getPendingProfile(1,$scope.CourseGUID,$scope.BatchGUID);
         $scope.Approved = $("#Approved").val();
         //alert($scope.Approved);
       }else{
         $scope.getStudentsList(1,$scope.CourseGUID,$scope.BatchGUID);
       }

      



    }





    $scope.getFeePlans = function(CourseGUID){

        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

        $scope.data.fee = [];
        $scope.data.installment = [];
        $http.post(API_URL+'students/getFeePlans', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200 && response.Data){ /* success case */


                for (var i in response.Data.fee) {

                    $scope.data.fee.push(response.Data.fee[i]);
                }

                for (var i in response.Data.installment) {

                    if(response.Data.installment[i]){

                         $scope.data.installment.push(response.Data.installment[i]);

                    }
                }
                $timeout(function(){            



                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



                }, 200);

            }

        });

    }







    $scope.getCourses = function(){

        var data = 'SessionKey='+SessionKey+'&CategoryTypeName=Course';

        $scope.data.course = [];

        $http.post(API_URL+'students/getCourses', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.course = response.Data;

            }

             $timeout(function(){            



               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



            }, 200);

        });

    }





    $scope.getStates = function()
    {
        var data = 'SessionKey='+SessionKey;

        $scope.data.states = [];

        $http.post(API_URL+'students/getStatesList', data, contentType).then(function(response) {
            var response = response.data;
            
            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.states = response.Data;

                var option = "";

                for (var i in $scope.data.states) {

                  option += '<option value="'+$scope.data.states[i].State_id+'">'+$scope.data.states[i].StateName+'</option>';
               }


               $scope.StateOptions = option;


            }
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

        });

    }





    $scope.getCities = function(State_id){

        var data = 'SessionKey='+SessionKey+'&State_id='+State_id;

        $scope.data.cities = [];

        $http.post(API_URL+'students/getCitiesList', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200 && response.Data){ /* success case */

                //$scope.data.cities = response.Data;

                for (var i in response.Data) {



                     $scope.data.cities.push(response.Data[i]);



                }

                 $timeout(function(){            



                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



                }, 200);
            }

        });

    }





    $scope.getCitiesByStateName = function(StateName){

        var data = 'SessionKey='+SessionKey+'&StateName='+StateName;

        $scope.data.cities = [];

        $http.post(API_URL+'students/getCitiesByStateName', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                //$scope.data.cities = response.Data;

                for (var i in response.Data) {

                    $scope.data.cities.push(response.Data[i]);
                }

                $timeout(function(){
                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                }, 200);

            }

        });

    }



    /*list append*/



    $scope.getStudentStatistics = function (CourseGUID="",BatchGUID="")
    {

        // if(!CourseGUID.length && !BatchGUID.length){

        //     $scope.getCourses();

        // }        

        //alert($scope.data.noRecords);

        //if ($scope.data.listLoading || $scope.data.noRecords) return;



        $scope.data.listLoading = true;



        $scope.data.dataList = [];

        

        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID+'&BatchGUID='+BatchGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();



        $http.post(API_URL+'students/getStudentStatistics', data, contentType).then(function(response) {



                var response = response.data;



                if(response.ResponseCode==200 && response.Data.length){ /* success case */



                    $scope.data.totalRecords = response.Data.TotalRecords;



                    for (var i in response.Data) {



                     $scope.data.dataList.push(response.Data[i]);



                 }



                 $scope.data.pageNo++;   

                 $scope.data.noRecords = false;            



             }else{



                $scope.data.noRecords = true;



            }



            $scope.data.listLoading = false;



            setTimeout(function(){ tblsort(); }, 1000);



        });



    }







    $scope.getStudentsList = function (check="",CourseGUID="",BatchGUID="")

    { 

        // alert(1);   

        
        if(check!=1){
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }else{
            $scope.data.pageNo = 1;
        }       



        $scope.data.listLoading = true;



        $scope.data.pageSize = 20;



        if(check!=1){

            $scope.CourseGUID = $scope.getUrlParameter('parentCategoryGUID');

            $scope.BatchGUID = $scope.getUrlParameter('BatchGUID');

        }else if(BatchGUID.length){

           
            $scope.BatchGUID = BatchGUID;

            $scope.data.dataLists = [];

        }else{
            $scope.CourseGUID = CourseGUID;

            $scope.BatchGUID = BatchGUID;

            $scope.data.dataLists = [];

        }

        
        var StudentType = $("#StudentType").val();
        

        var data = 'SessionKey='+SessionKey+'&StudentType='+StudentType+'&CategoryGUID='+$scope.CourseGUID+'&BatchGUID='+$scope.BatchGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        

        $http.post(API_URL+'students/getStudents', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataLists.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;

                }else{

                    $scope.data.noRecords = true;

                   // ////console.log(data.dataList.length);

                }

                        

                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);

        });

    }




    $scope.getMPStudents = function (check=0)
    {
        if(check!=1){
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }else{
            $scope.data.pageNo = 1;
        }  

        $scope.data.listLoading = true;

        $scope.data.pageSize = 20;        

        var data = 'SessionKey='+SessionKey+'&UserTypeID=88'+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

       // console.log(data);

        $http.post(API_URL+'students/getMPStudents', data, contentType).then(function(response) {

                var response = response.data;
                 //console.log(response.Data);
                if(response.ResponseCode==200 && response.Data.TotalRecords > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data.Records) {

                       $scope.data.dataLists.push(response.Data.Records[i]);
                       console.log($scope.data.dataLists);
                    }

                   // console.log($scope.data.dataLists);

                    $scope.data.pageNo++;

                }else{

                    $scope.data.noRecords = true;

                }   

                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);

        });
    }



    $scope.getPendingProfile = function (check="",CourseGUID="",BatchGUID="")

    { 

        // alert(1);   

        
        if(check!=1){
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }else{
            $scope.data.pageNo = 1;
            $scope.data.dataLists = [];
        }       



        $scope.data.listLoading = true;



        $scope.data.pageSize = 20;
        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        console.log(data);

        $http.post(API_URL+'students/getPendingProfile', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataLists.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;

                }else{

                    $scope.data.noRecords = true;

                   // ////console.log(data.dataList.length);

                }

                        

                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);

        });

    }



    $scope.loadImageForm = function (Position, StudentGUID, ProfilePic)

    {

        $scope.data.Position = Position;



        $scope.uploadTemplateURLAdd = PATH_TEMPLATE+'students/image_upload_form.htm?'+Math.random();



        $scope.data.pageLoading = true;



        $http.post(API_URL+'students/getStudentByID', 'SessionKey='+SessionKey+'&UserGUID='+StudentGUID, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */



                $scope.data.pageLoading = false;



                $scope.formData = response.Data[0];

                $scope.StudentGUID = $scope.formData.UserGUID;
                $scope.ProfilePic = $scope.formData.ProfilePic;


                $("#upload_image").modal({show:true});


                $scope.data.pageLoading = false;
                


            }



        });
    }







    /*load add form*/



    $scope.loadFormAdd = function (check)



    {

        //$scope.getCourses();

        $scope.check = check;

       // $scope.getStates();



        $scope.templateURLAdd = PATH_TEMPLATE+'students/add_form.htm?'+Math.random();



        $('#add_model').modal({show:true});

        $timeout(function(){  
            tinymce.init({
                selector: '#Remarks'
            });  
        }, 1000);  

        $timeout(function(){  

           //$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



       }, 1000);



    }









    $scope.loadImportStudents  = function (Position, CategoryGUID)



    {



        $scope.templateURLImport = PATH_TEMPLATE+'students/import_form.htm?'+Math.random();



        $('#import_excel_model').modal({show:true});



    }















    /*load edit form*/



    $scope.loadFormEdit = function (Position, UserGUID, StateName)



    {

       // $scope.getStates();



        $scope.getCitiesByStateName(StateName);



        $scope.data.Position = Position;



        $scope.templateURLEdit = PATH_TEMPLATE+'students/edit_form.htm?'+Math.random();



        $scope.data.pageLoading = true;
        

        $scope.formData = [];
        $scope.data.DueDates = [];

        var CourseGUID = $("#Courses").val();
        var BatchGUID = $("#subject").val();


        

        $http.post(API_URL+'students/getStudentByID', 'SessionKey='+SessionKey+'&UserGUID='+UserGUID+'&CategoryGUID='+CourseGUID+'&BatchGUID='+BatchGUID, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */



                $scope.data.pageLoading = false;



                $scope.formData = response.Data[0]; 
                

                if($scope.formData.FeeID.length > 0){
                    $scope.FeeID = $scope.formData.FeeID;
                    $scope.customFee = false;
                }


                if($scope.formData.InstallmentID != 0){
                    $scope.InstallmentID = $scope.formData.InstallmentID;
                    $scope.customFee = false;
                }

                if($scope.formData.FeeID.length == 0 && $scope.formData.InstallmentID == 0){
                    $scope.customFee = true;
                }

                if(response.Data[0].DueDates)
                {                    
                    for (var i in response.Data[0].DueDates) 
                    {
                        $scope.data.DueDates.push(response.Data[0].DueDates[i]);                       
                    }
                }  

                $('#edits_model').modal({show:true});
                
                   

                

                $timeout(function(){

                    $(".duedates").datepicker({dateFormat: 'dd-mm-yy'});

                    tinymce.init({
                        selector: '#Remarks'
                    }); 

                    if($scope.formData.FeeID == '' && $scope.formData.InstallmentID == ''){
                        $("#CustomizedFee").show();
                    }else{
                        $("#CustomizedFee").hide();
                    }

                    // $timeout(function(){  
                    //     $("#State option").filter(function() {

                    //         //may want to use $.trim in here

                    //         return $(this).text() == $scope.formData.StateName; 

                    //     }).prop('selected', true);   

                    //     $("#City option").filter(function() {

                    //         //may want to use $.trim in here

                    //         return $(this).text() == $scope.formData.CityName; 

                    //     }).prop('selected', true);      

                    // }, 200);

                     

                  // $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



               }, 2000);


            }



        });



    }





    $scope.loadFormView = function (Position, UserGUID)
    {

        $scope.data.fee_breakup_list = [];

        $scope.data.Position = Position;



        $scope.templateURLView = PATH_TEMPLATE+'students/view_form.htm?'+Math.random();



        $scope.data.pageLoading = true;


        var CourseGUID = $("#Courses").val();
        var BatchGUID = $("#subject").val();


        $http.post(API_URL+'students/getStudentByID', 'SessionKey='+SessionKey+'&UserGUID='+UserGUID+'&CategoryGUID='+CourseGUID+'&BatchGUID='+BatchGUID, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */
 


                $scope.data.pageLoading = false;



                $scope.formData = response.Data[0];

                //console.log($scope.formData);



                $scope.getFeePlans($scope.formData.CategoryGUID);



                $('#view_model').modal({show:true});

                for (var i in $scope.formData.FBList) 
                {
                    $scope.data.fee_breakup_list.push($scope.formData.FBList[i]);
                }  

                $timeout(function(){            



                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



               }, 200);



            }



        });



    }



    $scope.loadPendingProfileView = function (position,profileData){
        $scope.data.position = position;
        $scope.data.pageLoading = true;
        $scope.formData = profileData;
        $scope.templateURLView = PATH_TEMPLATE+'students/profile_approval.htm?'+Math.random();
        $('#view_model').modal({show:true});
        $scope.data.pageLoading = false;
    }







    /*load delete form*/



    $scope.loadFormDelete = function (Position, UserGUID, KeyStatusID)
    {
        $scope.data.Position = Position;
        $scope.data.listLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() 
        {
            $http.post(API_URL+'students/delete', 'SessionKey='+SessionKey+'&UserGUID='+UserGUID+'&KeyStatusID='+KeyStatusID, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode == 200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataLists.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.data.listLoading = false;  
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.data.listLoading = false; 
    }







    /*add data*/



    $scope.addData = function ()



    {



        $scope.loadeSubmit = true;

        tinyMCE.triggerSave();

        var data = "";

        data = 'SessionKey='+SessionKey+'&'+$("form[name='add_student']").serialize();



        $http.post(API_URL+'students/addStudent', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */               



                SuccessPopup(response.Message);

                //alert($scope.check);

                if($scope.check == 'FromList'){
                    //$scope.getStudentsList(); $route.reload();
                    location.reload();
                }else if($scope.check == 'FromStatistics'){
                    $scope.getStudentStatistics();
                }

                

                $timeout(function(){            



                   $('#add_model .close').click();

                    $scope.loadeSubmit = false;

               }, 200);



            }else{



                ErrorPopup(response.Message);



            }



            $scope.loadeSubmit = false;    



        });



    }





    $scope.editData = function ()



    {

        tinyMCE.triggerSave();

        $scope.loadeSubmit = true;

        var data = 'SessionKey='+SessionKey+'&UserGUID='+$scope.formData.UserGUID+'&'+$("form[name='add_student']").serialize();



        $http.post(API_URL+'students/editStudent', data, contentType).then(function(response) {



            var response = response.data;



            console.log(response);



            if(response.ResponseCode==200){ /* success case */               



                SuccessPopup(response.Message);


                $scope.loadeSubmit = false;
                //$scope.data.dataLists[$scope.data.Position] = response.Data[0];


                //$scope.applyFilter(1,$scope.CourseGUID, $scope.BatchGUID);



                $timeout(function(){            



                   $('#edits_model .close').click();

                   window.location.href = window.location.href;

               }, 2000);



            }else{



                ErrorPopup(response.Message);



            }



             $scope.loadeSubmit = false;



        });



    }



    $scope.approveProfileUpdate = function (PendingProfileID,StudentID,CourseID,BatchID){
        $scope.loadeSubmit = true;

        var data = 'SessionKey='+SessionKey+'&StudentID='+StudentID+'&PendingProfileID='+PendingProfileID+'&CourseID='+CourseID+'&BatchID='+BatchID;

        $http.post(API_URL+'students/approveProfileUpdate', data, contentType).then(function(response) {
            
            var response = response.data;

            

            if(response.ResponseCode==200){ /* success case */

                

                

                SuccessPopup(response.Message);

                

                $timeout(function(){

                   

                   $scope.data.dataLists.splice($scope.data.Position, 1); /*remove row*/

                   

                   window.location.href = window.location.href;

               }, 1000);



            }else{

                $scope.loadeSubmit = false;

                ErrorPopup(response.Message);

            }

        });
    }



    $scope.saveImage = function (){
         $scope.loadeSubmit = true;



        var data = 'SessionKey='+SessionKey+'&'+$("form[name='picture_upload_form']").serialize();



        $http.post(API_URL+'students/saveImage', data, contentType).then(function(response) {



            var response = response.data;


            if(response.ResponseCode==200){ /* success case */               



                SuccessPopup(response.Message);


                $timeout(function(){

                   $('#upload_image .close').click();

                   $scope.loadeSubmit = false;

               }, 200);



            }else{

                $scope.loadeSubmit = false;

                ErrorPopup(response.Message);



            }

        });



       //$scope.loadeSubmit = false;



        //setTimeout(function(){ tblsort(); }, 1000);

    }







    $scope.importData = function ()



    {



        $scope.loadeSubmit = true;



        var data = 'SessionKey='+SessionKey+'&'+$("form[name='importStudents']").serialize();



        console.log(data);



        $http.post(API_URL+'students/importStudents', data, contentType).then(function(response) {



            var response = response.data;



            

            if(response.ResponseCode==200){ /* success case */               



                SuccessPopup(response.Message);



               // $scope.data.dataLists[$scope.data.Position] = response.Data[0];


               $scope.getStudentStatistics();



                $timeout(function(){            



                   $('#import_excel_model .close').click();

                   $scope.loadeSubmit = false;

               }, 200);



            }else{

                $scope.loadeSubmit = false;

                ErrorPopup(response.Message);

                

            }

        });



       



        setTimeout(function(){ tblsort(); }, 1000);



    }



    $scope.tableDatatoExcel = function(){
        var data = 'SessionKey=' + SessionKey+'&CategoryGUID='+$scope.CourseGUID+'&BatchGUID='+$scope.BatchGUID;
        $scope.data.newDataList = [];
        alertify.confirm('Are you sure you want to export student data?', function() {
            $http.post(API_URL + 'students/getStudentByID', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200 && response.Data) { /* success case */
                    for (var i in response.Data) {
                       delete response.Data[i].BatchID;
                       delete response.Data[i].StatusID;
                       delete response.Data[i].CourseID;
                       delete response.Data[i].FeeStatusID;
                       delete response.Data[i].StatusID;
                       delete response.Data[i].UserGUID;
                       delete response.Data[i].FeeID;
                       delete response.Data[i].CategoryGUID;
                       delete response.Data[i].CategoryGUID;
                       delete response.Data[i].ProfilePic;
                       delete response.Data[i].FBList;

                       delete response.Data[i].DueDates;
                       delete response.Data[i].DroppingDate;
                       delete response.Data[i].DroppingReason;
                       delete response.Data[i].AmountBalance;
                       delete response.Data[i].PaymentType;
                       delete response.Data[i].ChequeDate;
                       delete response.Data[i].ChequeNumberOrTransaction;
                       delete response.Data[i].BankOrOther;
                       delete response.Data[i].PaidFeeHistory
                       
                       $scope.data.newDataList.push(response.Data[i]);
                    }
                    $("#dvjson").excelexportjs({
                        containerid: "dvjson"
                           , datatype: 'json'
                           , dataset: $scope.data.newDataList
                           , columns: getColumns(response.Data)     
                    });       
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
    }

    $scope.getCustomizedFee = function(feePlan){

        if(feePlan == "Customized"){
            $scope.customFee = true;
            $("#CustomizedFee").show();
        }else{
            $scope.customFee = false;
            $("#CustomizedFee").hide();
            
        }

        

        console.log($scope.customFee); 
    }



    $scope.loadFormDropout = function (Position, UserGUID)
    {
        $scope.data.fee_breakup_list = [];
        $scope.data.StudentUserGUID = UserGUID;
        $scope.data.Position = Position;
        $scope.templateURLDropOut = PATH_TEMPLATE+'students/drop_out_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        var CourseGUID = $("#Courses").val();
        var BatchGUID = $("#subject").val();

        $http.post(API_URL+'students/studentByIDForDrop', 'SessionKey='+SessionKey+'&UserGUID='+UserGUID+'&CategoryGUID='+CourseGUID+'&BatchGUID='+BatchGUID, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            {
                $scope.data.pageLoading = false;
                $scope.formData = response.Data[0];
                $scope.SystemRefundAmount = response.Data[0].SystemRefundAmount;
                $('#drop_out_model').modal({show:true});

                for (var i in $scope.formData.FBList) 
                {
                    $scope.data.fee_breakup_list.push($scope.formData.FBList[i]);
                }
            }
        });
    }


    $scope.addDroppingData = function ()
    {
        alertify.confirm('You can not edit details later on. So please confirm the entry before dropping student ?', function() 
        {            
            $scope.addFormDroppingLoading = true;

            var data = 'SessionKey='+SessionKey+'&'+$("#add_dropout_form").serialize();
            
            $http.post(API_URL+'students/dropStudent', data, contentType).then(function(response) 
            {
                var response = response.data;
                if(response.ResponseCode==200)
                {
                    SuccessPopup(response.Message);

                    $timeout(function()
                    {
                        window.location.href = window.location.href; 

                    }, 1000);
                }
                else
                {
                    ErrorPopup(response.Message);
                }

                $scope.addFormDroppingLoading = false;          
            });

        }).set('labels', {
            ok: 'Confirm',
            cancel: 'Cancel'
        });         
    }


    $scope.showAssignBatchStudentModal = function()
    {
        $('#bulk_assign_student').modal({
            show: true
        });

        $scope.getUnAssignBatchStudent();
    }


    $scope.getUnAssignBatchStudent = function ()
    {
        $scope.data.UnAssignListLoading = true;

        $scope.data.StudentDataList = [];        

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'students/getUnAssignBatchStudent', data, contentType).then(function(response) 
        {    
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;
                
                for (var i in response.Data) 
                {
                    $scope.data.StudentDataList.push(response.Data[i]);
                }      
            }

            $scope.data.UnAssignListLoading = false;  
        });
    }

    $scope.BulkAssignBatchSave = function ()
    {
        var chk = $(".BASChkBox:checked").length;            
        
        if(chk <= 0)
        {
            alert("Please select atleast one student.");

            return false;
        }    


        $scope.loadeSubmitStudent = true;

        var data = 'SessionKey='+SessionKey+'&'+$("#bulk_assign_batch_student").serialize();
        
        $http.post(API_URL+'students/BulkAssignBatchSave', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                $timeout(function()
                {
                    window.location.href = window.location.href; 

                }, 3000);
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.loadeSubmitStudent = false;          
        });                
    }

}); 



/* sortable - starts */



function tblsort() {







  var fixHelper = function(e, ui) {



    ui.children().each(function() {



        $(this).width($(this).width());



    });



    return ui;



}







$(".table-sortable tbody").sortable({



    placeholder: 'tr_placeholder',



    helper: fixHelper,



    cursor: "move",



    tolerance: 'pointer',



    axis: 'y',



    dropOnEmpty: false,



    update: function (event, ui) {



      sendOrderToServer();



  }      



}).disableSelection();



$(".table-sortable thead").disableSelection();











function sendOrderToServer() {



    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");



    $.ajax({



        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',



        data: order,



        stop: function(response) {



            if (response.status == "success") {



                window.location.href = window.location.href;



            } else {



                alert('Some error occurred');



            }



        }



    });



}















}







function getBatchAndPlans(CourseGUID){

    angular.element(document.getElementById('student-body')).scope().getBatchByCourse(CourseGUID);

    angular.element(document.getElementById('student-body')).scope().getFeePlans(CourseGUID);

}



function getCategoryBatch(CourseGUID,check){

   angular.element(document.getElementById('student-body')).scope().getBatchByCourse(CourseGUID);

   if(check == "getStudentsList"){
    angular.element(document.getElementById('student-body')).scope().getStudentsList(CourseGUID); 
   }else{
    angular.element(document.getElementById('student-body')).scope().getStudentStatistics(CourseGUID); 
   }
}



function filterStudentStatistics(BatchGUID){
   angular.element(document.getElementById('student-body')).scope().getStudentStatistics("",BatchGUID); 
}



function filterStudentsList(CourseGUID="",BatchGUID=""){
    if(CourseGUID.length){
       angular.element(document.getElementById('student-body')).scope().getBatchByCourse(CourseGUID); 
    }    
    angular.element(document.getElementById('student-body')).scope().getStudentsList(1,CourseGUID,BatchGUID); 
}

function filterPendingProfileList(CourseGUID="",BatchGUID=""){
    if(CourseGUID.length){
       angular.element(document.getElementById('student-body')).scope().getBatchByCourse(CourseGUID); 
    }    
    angular.element(document.getElementById('student-body')).scope().getPendingProfile(1,CourseGUID,BatchGUID); 
}

function applyFilter(chk){
    angular.element(document.getElementById('student-body')).scope().applyFilter(1); 
}


function getCustomizedFees(feePlan){
   // alert(feePlan);
    angular.element(document.getElementById('student-body')).scope().getCustomizedFee(feePlan);
}


function changeInstallment(amount){
    $("#TotalFeeInstallments").val('');
    $("#InstallmentAmount").val('');
}



function changeInstallmentAmount(installmentCount)
{
    if(installmentCount <= 0)
    {
        alert("Installment Count should be greater than 0.");
        $("#TotalFeeInstallments").val('');
        return false;
    }

    var TotalFeeAmount = $("#TotalFeeAmount").val();
    if(!TotalFeeAmount.length || TotalFeeAmount == 0){
        alert("Please fill total amount before installment");
        $("#TotalFeeInstallments").val('');
        $("#InstallmentAmount").val('');
    }else{
        if(parseInt(installmentCount) > parseInt(TotalFeeAmount)){
            alert("no. of installment can not be greater than total amount.");
        }else if(installmentCount.length > 2){
            alert("Installment Count can not be greater than 99.");
        }else{
            var InstallmentAmount = TotalFeeAmount/installmentCount;
            if($.isNumeric(InstallmentAmount)){
                $("#InstallmentAmount").val(InstallmentAmount.toFixed(2));
            }else{
                $("#InstallmentAmount").val('');
            }
        }
    }
}


function changeInstallmentCount(InstallmentAmount){
    var TotalFeeAmount = $("#TotalFeeAmount").val();
    if(!TotalFeeAmount.length || TotalFeeAmount == 0){
        alert("Please fill total amount before installment");
        $("#TotalFeeInstallments").val(0);
        $("#InstallmentAmount").val(0);
    }else{
        if(parseInt(InstallmentAmount) > parseInt(TotalFeeAmount)){
            $("#TotalFeeInstallments").val(0);
            $("#InstallmentAmount").val(0);
            alert("Installment amount can not be greater than total amount.");
        }else{
             var installmentCount = TotalFeeAmount/InstallmentAmount;
            if($.isNumeric(installmentCount)){
                $("#TotalFeeInstallments").val(installmentCount.toFixed(2));
            }else{
                $("#TotalFeeInstallments").val(0);
            }
        }       
    }
}


function filterStudentType()
{
   var CourseGUID = $("#Courses").val();
   var BatchGUID = $("#subject").val();

   angular.element(document.getElementById('student-body')).scope().getStudentsList(1,CourseGUID,BatchGUID); 
}

/*audio file upload*/

$(document).on('change', '#studentFileInput', function() {

    var target = $(this).data('target');

    var mediaGUID = $(this).data('targetinput');

    var progressBar = $('.progressBar'),

        bar = $('.progressBar .bar'),

        percent = $('.progressBar .percent');

    $(this).parent().ajaxForm({

        data: {

            SessionKey: SessionKey

        },

        dataType: 'json',

        beforeSend: function() {

            progressBar.fadeIn();

            var percentVal = '0%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        uploadProgress: function(event, position, total, percentComplete) {

            var percentVal = percentComplete + '%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        success: function(obj, statusText, xhr, $form) {

            if (obj.ResponseCode == 200) {

                var percentVal = '100%';

                bar.width(percentVal)

                percent.html(percentVal);

                $(target).prop("src", obj.Data.MediaURL);

                //$("input[name='MediaGUIDs']").val(obj.Data.MediaGUID);

                $(mediaGUID).val(obj.Data.MediaGUID);
                $("#MediaName").val(obj.Data.MediaName);
                $("#MediaURL").val(obj.Data.MediaURL);

            } else {

                ErrorPopup(obj.Message);

            }

        },

        complete: function(xhr) {

            progressBar.fadeOut();

            //$('#studentFileInput').val("");

        }

    }).submit();




});



/* sortable - ends */

function getCustomizedFee(feePlan){
    if(feePlan == "Customized"){
        //$("#CustomizedFee").show();
        angular.element(document.getElementById('student-body')).scope().getCustomizedFee(true); 
    }else{
        angular.element(document.getElementById('student-body')).scope().getCustomizedFee(false); 
    }
}


function tableDatatoExcel(){
    $("#tableData").excelexportjs({
        containerid: "tableData"
        , datatype: 'table'
    });
}

function getCities(state_id){
   // alert(state_id);
  angular.element(document.getElementById('student-body')).scope().getCities(state_id);  
}


function setDueDate()
{
    var Plan = $("#Plan").find("option:selected").attr("installment");
    var duration = $("#Course").find("option:selected").attr("duration");
    var startdate = $("#Batch").find("option:selected").attr("startdate");
        
    if($("#Course").val() == "" || $("#Batch").val() == "")
    {
        $("#due_date_setting_content").html('');    

        $("#due_date_setting").hide();
        
        return false;
    }    


    if(Plan == 'c')
    {
        Plan = $("#TotalFeeInstallments").val();
    }

    if(Plan != '' && Plan != null && Plan != undefined && Plan != 0)
    {
        var i = 1; var dates = "";         
        var diff = 0;
        var duedate = '';

        if(duration!='' && duration!='' && duration!=null && duration!=undefined)
        {            
            diff = Math.round(duration / Plan);
        }


        if(startdate!='' && startdate!='' && startdate!=null && startdate!=undefined)
        {
            duedate = startdate;
            $(".bsd").html(" ( Batch Start Date: " + startdate + " )");
        }
        else
        {
            $(".bsd").html("");
        }

        
        for(i = 1; i<= Plan; i++)
        {
            dates = dates + "<div class='col-md-2' style='float:left;'><label>Installment "+i+"</label><input type='text' class='form-control duedates' name='duedates[]' value='"+duedate+"' readonly></div>";

            if(duedate!='' && diff!='' && diff!=null && diff!=undefined)
            {                
                var a = duedate.split("-");
                duedate = new Date(a[2], a[1], a[0]);
                duedate.setMonth(duedate.getMonth() + diff);
                duedate = format_date(duedate);
            }    
        }    

        $("#due_date_setting").show();
        $("#due_date_setting_content").html(dates);

        $(".duedates").datepicker({dateFormat: 'dd-mm-yy'});
    }
    else
    {        
        $("#due_date_setting_content").html('');
        $("#due_date_setting").hide();
    }
}


function format_date(d)
{
    var fd = d;

    var dd = d.getDate(); 
    var mm = d.getMonth(); if(mm == 0) mm = 1;

    var yyyy = d.getFullYear(); 
    
    if (dd < 10) 
    { 
        dd = '0' + dd; 
    }

    if (mm < 10) 
    { 
        mm = '0' + mm; 
    } 

    var fd = dd + '-' + mm + '-' + yyyy; 

    return fd;
}