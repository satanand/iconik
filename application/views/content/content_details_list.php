<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4">Manage Content</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" id="content-body" ng-init="getDetailsList()"><!-- Body -->

	<div class="clearfix mt-2 mb-2">
	   <div class="row float-left ml-6" style="width:80%;">
	   	 <!-- <div class="col-md-3">
		      	<div class="form-group">
			      <span class="float-left records hidden-sm-down">
					<span class="h5">Total recordss: {{data.totalRecords}}</span>
				  </span>
				 </div>
		  </div> -->
	      <div class="col-md-3">
	         <div class="form-group">
	            <select class="form-control" onchange="getCategorySubjects(this.value)" name="ParentCat" id="Courses">
	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
	               <option ng-repeat="row in categoryDataList" value="{{row.CategoryGUID}}" ng-selected="row.CategoryGUID == ParentCategoryGUID">{{row.CategoryName}}</option>
	            </select>
	         </div>
	      </div>
	      <div class="col-md-3">
	         <div class="form-group">
	            <div id="subcategory_section">
	               <select id="subject" name="CategoryGUIDs" onchange="getDetailsListByCategory(this.value)"  class="form-control">
	                  <option value="">Select Subject</option>
	                  <option ng-repeat="row in subCategoryDataList" value="{{row.CategoryGUID}}" ng-selected="CategoryGUID == row.CategoryGUID">{{row.CategoryName}}</option>
	               </select>
	            </div>
	         </div>
	      </div>
	      <div class="col-md-3">
	         <div class="form-group">
	            <div id="subcategory_section">
	               <select  onchange="FilterByType(this.value)"  class="form-control">
	                  <option value="all">Types</option>
	                  <option value="text-tr">Text</option>
	                  <option value="audio-tr">Audio</option>
	                  <option value="video-tr">Video</option>
	               </select>
	            </div>
	         </div>
	      </div>
	   </div>
	   <!-- <div class="float-right mr-2">		
	      <a href="<?php //echo base_url().'content'; ?>" class="btn btn-secondary btn-sm" id="add-content-btn">Back</a>
	   </div> -->
	</div>

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">


	<!-- Data table -->
	<div class="table-responsive block_pad_md"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		
		<table class="table table-striped table-hover">
			<!-- table heading -->
			<thead>
				<tr>
					<th style="width: 100px;">Title</th>
					<th style="width: 100px;">Type</th>
					<th style="width: 100px;">Click to view content</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody ng-repeat="(key, rows) in data.dataList">
				<tr class="video-tr content-details-tr" scope="row" ng-if="row.media_video != 0" ng-repeat="(key, row) in rows.media_video">
					<td >{{row.PostVideoTitle}}</td>
					
					<td >Video</td>
										
					<td>
						<a class="glyphicon glyphicon-play" href="" ng-click="loadContentViewEdit(key, 'view', 'video', row.PostVideoTitle, row.PostVideoLink, row.VideoEntityGUID,row.PostGUID)"  title="View"></a>
					</td>

					
					<td class="text-center" >						
						<a class="glyphicon glyphicon-edit" href="" ng-click="loadContentViewEdit(key, 'edit', 'video', row.PostVideoTitle, row.PostVideoLink, row.VideoEntityGUID,row.PostGUID)" title="Edit"></a>
						
						<a class="glyphicon glyphicon-trash" href="" ng-click="loadFormVideoDelete(key, rows.VideoEntityGUID)" title="Delete"></a>
					</td>
				</tr>



				<tr class="text-tr content-details-tr" scope="row" ng-if="row.content != 0" ng-repeat="(key, row) in rows.content">
					<td >{{row.PostCaption}}</td>
					
					<td >Text Content</td>

					<td>
						<a class="glyphicon glyphicon-file" href="" ng-click="loadContentViewEdit(key, 'view', 'content', row.PostCaption, row.PostContent, row.PostGUID,rows.PostGUID)" title="View"></a>
					</td>
					
					<td class="text-center">						
						<a class="glyphicon glyphicon-edit" href="" ng-click="loadContentViewEdit(key, 'edit', 'content', row.PostCaption, row.PostContent, row.PostGUID,rows.PostGUID)" title="Edit"></a>
						
						<a class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.PostGUID)" title="Delete"></a>
					</td>
				</tr>



				<tr class="text-tr content-details-tr" scope="row" ng-if="row.media_file != 0" ng-repeat="(key, row) in rows.media_file">
					<td >{{row.MediaCaption}}</td>
					
					<td >File</td>

					<td>
						
					
						<p> <a class="glyphicon glyphicon-file" href="" ng-click="FileInModalBoxInline(row.MediaURL,row.MediaCaption);"></a> </p>

						<!-- <a class="glyphicon glyphicon-eye-open" href="" title="View" ng-click="FileInModalBox(API_URL,row.MediaURL,row.MediaCaption,row.MediaGUID)"></a> -->					
					</td>

					<td class="text-center">
						
						<a class="glyphicon glyphicon-edit" href="" ng-click="loadContentViewEdit(key, 'edit', 'file', row.MediaCaption, row.MediaURL, row.MediaGUID,rows.PostGUID)" title="Edit"></a>					
						<a class="glyphicon glyphicon-trash" href="" ng-click="loadFormMediaDelete(key, row.MediaGUID)" title="Delete"></a>
					</td>
				</tr>



				<tr class="audio-tr content-details-tr" scope="row" ng-if="row.media_audio != 0" ng-repeat="(key, row) in rows.media_audio">
					<td >{{row.MediaCaption}}</td>
					
					<td >Audio</td>

					<td>
						<a class="glyphicon glyphicon-play" href="" ng-click="loadContentViewEdit(key, 'view', 'audio', row.MediaCaption, row.MediaURL, row.MediaGUID, rows.PostGUID)" title="View"></a>
					</td>

					<td class="text-center">						
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadContentViewEdit(key, 'edit', 'audio', row.MediaCaption, row.MediaURL, row.MediaGUID, rows.PostGUID)" title="Edit"></a>						
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormMediaDelete(key, row.MediaGUID)" title="Delete"></a>
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="!data.dataList.length">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>

		<p id="no-records" class="no-records text-center" style="display: none;">
			<span>No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->




	<!-- add Modal -->
	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Content<?php //echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="add_subject">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Subject</h3>     	
					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="subjectTemplateURLAdd"></div>
			</div>
		</div>
	</div>





	<!-- edit Modal -->
	<div class="modal fade" id="edit_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Content<?php //echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLEdit"></div>
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Content<?php //echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>

	<div class="view_model_file">
    <div class="modal fade" id="view_model_file">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title h5">{{file_title}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                </div>
                <div class="modal-body" style="height: 400px;">                    
                    <div id="iframe-info"></div>    
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

	<!-- delete Modal -->
	<div class="modal fade" id="delete_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Delete Content<?php //echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
</div><!-- Body/ -->