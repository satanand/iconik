<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4">Manage Keys</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" id="student-body" ng-init="getKeysStatisctics()"><!-- Body -->
	<div class="clearfix mt-2 mb-2">
	   <div class="float-right mr-2" ng-if="UserTypeID == 1">
	    	<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd();">Add Keys</button>
	   		<button class="btn btn-success btn-sm ml-1 float-right"  id="add-question-btn" ng-click="loadFormAllot();" ng-if="data.Allot_keys > 0">Allot Keys</button>
	   		<button class="btn btn-success btn-sm ml-1 float-right"  id="add-question-btn" ng-click="loadFranchiseeDefaultKey();" ng-if="data.Allot_keys > 0">Franchisee default Keys</button>
		</div>
		<div class="float-right mr-2" ng-if="UserTypeID == 10">
			<!-- <button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();"><a id="add-question-btn">Assign Keys</a></button>	  -->  		
			<!-- <button  class="btn btn-success btn-sm ml-1 float-right" ng-if="FranchiseeAssignCount = 1"  ng-click="LoadUpdateKeys()">Update Alloted Keys</button> -->
		</div>
	</div>

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">


	<!-- Data table -->
	<div class="table-responsive block_pad_md"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

	
		<!-- data table -->
		<table class="table table-striped table-hover" id="tableToExport">
			<!-- table heading -->
			<thead>
				<tr>
					<th style="width: 100px;"  class="text-center">Validity</th>
					<th style="width: 100px;" class="text-center">Total Keys</th>
					<th style="width: 100px;" class="text-center">Used Keys</th>
					<th style="width: 100px;" class="text-center">Available Keys</th>
					<th style="width: 100px;" class="text-center">Keys Status</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row" ng-repeat="(key, row) in data.dataList">
					<td style="text-align: center;">{{row.Validity}} Month</td>
					<td style="text-align: center;">{{row.TotalKeys}}</td>
					<td style="text-align: center;">{{row.UsedKeys}}</td>
					<td style="text-align: center;">{{row.AvailableKeys}}</td>
					<td style="text-align: center;"><a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'keys/used/?Validity={{row.Validity}}'; ?>" data-toggle="tooltip" title="Used keys listing"></a></td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="data.noRecords">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->




	<!-- add Modal -->
	
	<div class="modal fade" id="allot_key_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Allot Keys</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAllot"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="franchisee_default_keys_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Set default keys</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLSetFranchiseeKeys"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Keys</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="edit_keys_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Update Alloted Keys</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLUpdateKeys"></div>
			</div>
		</div>
	</div>

</div><!-- Body/ -->