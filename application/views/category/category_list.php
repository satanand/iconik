<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4" id="top-heading">Manage Stream/Course/Subject</h1>
</header>
<?php //$this->load->view('includes/breadcrumb'); ?>

<!-- Head/ -->
<div class="panel-body" id="panel-body" ng-controller="PageController" ng-init="getFilterData()"><!-- Body -->


<nav aria-label="breadcrumb" ng-if="type=='Topic'"> 
	<ol class="breadcrumb">  
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
		
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category">Stream</a></li>
		
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category/?CategoryTypeID=1&ParentCategoryGUID={{data.ParentCategoryGUID}}">Course</a></li>
		
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category/?CategoryTypeID=2&ParentCategoryGUID={{CurrentCategoryGUID}}">Subject</a></li>

		<li class="breadcrumb-item ng-binding">{{type}}</li> 
	</ol> 
</nav>

<nav aria-label="breadcrumb" ng-if="type=='Subject'"> <ol class="breadcrumb">  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li><li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category">Stream</a></li><li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category/?CategoryTypeID=1&ParentCategoryGUID={{CurrentCategoryGUID}}">Course</a></li><li class="breadcrumb-item ng-binding">{{type}}</li> </ol> </nav>

<nav aria-label="breadcrumb" ng-if="type=='Course'"> <ol class="breadcrumb">  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li><li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category">Stream</a></li><li class="breadcrumb-item ng-binding">{{type}}</li> </ol> </nav>

<nav aria-label="breadcrumb" ng-if="type=='Stream'"> <ol class="breadcrumb"><li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>  <li class="breadcrumb-item">Manage Courses</li><li class="breadcrumb-item">Stream</li> </ol> </nav>


	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">
		<form class="" id="filterForm" role="form" autocomplete="off" ng-submit="getList();">
			<div class="row">
				<div class="col-md-2">
					<span class="float-left records d-none d-sm-block">
						<span class="h5">Total records: {{data.dataList.length}}</span>
					</span>
				</div>
				
				<div class="col-md-3">
					<div class="form-group">
						<div id="universal_search"> 
							<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Type and search" style="" autocomplete="off">	
						</div>				 	 
				    </div>
				</div>

				<div class="col-md-4">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" ng-click="applyFilter()"><?php echo SEARCH_BTN;?></button>&nbsp;
					<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search();"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>	
			</div>
		</form>
			<!-- <div class="row"> -->
				<div class="float-right col-md-2" style="        margin-top: -35px;"><!-- ng-if="filterData.CategoryTypes.length>1" -->

					<!-- <button  class="btn btn-default btn-secondary btn-sm ng-scope" data-toggle="modal" data-target="#filter_model"><img src="asset/img/search.svg"></button> -->

					<button ng-if="type == 'Stream' && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();">Add Stream/Course</button>

					<button ng-if="type == 'Course' && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();">Add Subject</button>

					<button ng-if="type == 'Subject' && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();">Add Topic</button>

				</div>

			<!-- </div> -->
		

		
	</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width:80px;" class="text-center">Picture</th>

					<th style="width: 200px;">Name</th>

					<th style="width: 100px;">Type</th>

					<th style="width: 200px;" ng-if="type == 'Stream' && type != 'Subject'">Courses</th>

					<th style="width: 200px;" ng-if="type == 'Course' && type != 'Subject'">Subjects</th>

					<th style="width: 200px;" ng-if="type == 'Subject'">Topics</th>

					<th style="width: 100px;" class="text-center" ng-if="type == 'Course' && type != 'Subject'">Duration (Month)</th>


					<th style="width: 100px;" class="text-center">Status</th>

					<th style="width: 100px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">







				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.CategoryID}}">

					<td class="listed sm text-center">

						<img ng-if="!row.Media.Records[0].MediaThumbURL" ng-src="./asset/img/default-category.png">

						<img ng-if="row.Media.Records[0].MediaThumbURL" ng-src="{{row.Media.Records[0].MediaThumbURL}}">

					</td>



					<td>
						<strong>{{row.CategoryName | capitalizeFirstLetter}}</strong>
					</td>

					<td>
						<label ng-if="row.CategoryTypeID == 1">Stream</label>

						<label ng-if="row.CategoryTypeID == 2">Course</label>

						<label ng-if="row.CategoryTypeID == 3">Subject</label>

						<label ng-if="row.CategoryTypeID == 4">Topic</label>
					</td>
						
						
					<td ng-if="type != 'Subject' && type != 'Topic'">

						<span ng-if="row.SubCategories.Records.length">		

							<span ng-repeat="m in row.SubCategories.Records">

								{{m.CategoryName | capitalizeFirstLetter}}<br/>

							</span>
							<a href="./category/?CategoryTypeID={{row.CategoryTypeID}}&ParentCategoryGUID={{row.CategoryGUID}}&CategoryGUID={{data.ParentCategoryGUID}}" style="font-size: 89%;">[Manage <span ng-if="row.CategoryTypeID == 1">Courses</span><span ng-if="row.CategoryTypeID == 2">Subjects</span>]</a>

						</span>

						<span ng-if="!row.SubCategories.Records.length">-</span>

					</td>


					<td ng-if="type == 'Subject'">

						<span ng-if="row.SubCategories.Records.length">		

							<span ng-repeat="m in row.SubCategories.Records">

								{{m.CategoryName | capitalizeFirstLetter}}<br/>

							</span>
							<a href="./category/?CategoryTypeID={{row.CategoryTypeID}}&ParentCategoryGUID={{row.CategoryGUID}}&CategoryGUID={{data.ParentCategoryGUID}}" style="font-size: 89%;">[Manage <span ng-if="row.CategoryTypeID == 1">Courses</span><span ng-if="row.CategoryTypeID == 2">Subjects</span><span ng-if="row.CategoryTypeID == 3">Topics</span>]</a>

						</span>						

					</td>


					<td class="text-center" ng-if="type == 'Course' && type != 'Subject'  && type != 'Topic'">{{row.Duration}} </td> 
					<!-- <td class="text-center" ng-if="!row.Duration">0 </td>  -->

					<td class="text-center"><span ng-class="{Inactive:'text-danger', Active:'text-success'}[row.Status]">{{row.Status}}</span></td> 



					<td class="text-center">
						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.CategoryGUID)" title="View"></a>
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.CategoryGUID)" title="Edit"></a>
					
					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- Filter Modal -->

	<div class="modal fade" id="filter_model"  ng-init="getFilterData()">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Filters</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>



				<!-- Filter form -->

				<form id="filterForm" role="form" autocomplete="off" class="ng-pristine ng-valid">

					<div class="modal-body">

						<div class="form-area">



							<div class="row">

								<div class="col-md-8">

									<div class="form-group">

										<label class="filter-col" for="CategoryTypeName">Category Type</label>

										<select id="CategoryTypeName" name="CategoryTypeName" class="form-control chosen-select">

											<option value="">All Categories</option>

											<option ng-repeat="row in filterData.CategoryTypes" value="{{row.CategoryTypeName}}">{{row.CategoryTypeName}}</option>

										</select>   

									</div>

								</div>

							</div>



						</div> <!-- form-area /-->

					</div> <!-- modal-body /-->



					<div class="modal-footer">

						<button type="button" class="btn btn-secondary btn-sm" onclick="$('#filterForm').trigger('reset'); $('.chosen-select').trigger('chosen:updated');">Reset</button>

						<button type="submit" class="btn btn-success btn-sm" data-dismiss="modal" ng-disabled="editDataLoading" ng-click="applyFilter()">Apply</button>

					</div>



				</form>

				<!-- Filter form/ -->

			</div>

		</div>

	</div>







	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5" ng-if="type == 'Stream'">Add Stream/Course</h3>

					<h3 class="modal-title h5" ng-if="type == 'Course'">Add Subject</h3>

					<h3 class="modal-title h5" ng-if="type == 'Subject'">Add Topic</h3>      	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>







	<!-- edit Modal -->

	<div class="modal fade" id="edits_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5" ng-if="type == 'Stream'">Edit  Details</h3>

					<h3 class="modal-title h5" ng-if="type == 'Course'">Edit  Details</h3>  

					<h3 class="modal-title h5" ng-if="type == 'Subject'">Edit  Details</h3>  

					<h3 class="modal-title h5" ng-if="type == 'Topic'">Edit  Details</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>


	<!-- View Modal -->
	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5" ng-if="type == 'Stream'">View  Details</h3>

					<h3 class="modal-title h5" ng-if="type == 'Course'">View  Details</h3>  

					<h3 class="modal-title h5" ng-if="type == 'Subject'">View  Details</h3>  

					<h3 class="modal-title h5" ng-if="type == 'Topic'">View  Details</h3> 	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>











</div><!-- Body/ -->







