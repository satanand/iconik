<?php
/*-----------------------------------------------------
Enquiry Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_enquiries">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_enquiries" checked value="Enquiry Date" readonly disabled>&nbsp;Enquiry Date
<input type="checkbox" name="chkbox_enquiries[]" checked value="Enquiry Date" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_enquiries" checked value="Full Name" readonly disabled>&nbsp;Full Name
<input type="checkbox" name="chkbox_enquiries[]" checked value="Full Name" style="display: none;">
</div>


<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_enquiries[]" class="chkbox_enquiries" checked value="Mobile">&nbsp;Mobile</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_enquiries[]" class="chkbox_enquiries" checked value="Interested in">&nbsp;Interested in</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_enquiries[]" class="chkbox_enquiries" checked value="Source">&nbsp;Source</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_enquiries[]" class="chkbox_enquiries" checked value="Assigned to">&nbsp;Assigned to</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_enquiries[]" class="chkbox_enquiries" checked value="Current Status">&nbsp;Current Status</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_enquiries[]" class="chkbox_enquiries" checked value="Latest Remarks">&nbsp;Latest Remarks</div>

</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Courses Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_courses">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_courses" checked value="Stream" readonly disabled>&nbsp;Stream
<input type="checkbox" name="chkbox_courses[]" checked value="Stream" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_courses" checked value="Course" readonly disabled>&nbsp;Course
<input type="checkbox" name="chkbox_courses[]" checked value="Course" style="display: none;">
</div>


<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_courses[]" class="chkbox_courses" checked value="No Of Batches">&nbsp;No. Of Batches</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_courses[]" class="chkbox_courses" checked value="No Of Students">&nbsp;No. Of Students</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_courses[]" class="chkbox_courses" checked value="Duration Of Course(in Months)">&nbsp;Duration Of Course</div>

</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Fees Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_fees">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_fees" checked value="Stream" readonly disabled>&nbsp;Stream
<input type="checkbox" name="chkbox_fees[]" checked value="Stream" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_fees" checked value="Course" readonly disabled>&nbsp;Course
<input type="checkbox" name="chkbox_fees[]" checked value="Course" style="display: none;">
</div>


<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_fees" checked value="Student Name" readonly disabled>&nbsp;Student Name
<input type="checkbox" name="chkbox_fees[]" checked value="Student Name" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fees[]" class="chkbox_fees" checked value="Course Fee">&nbsp;Course Fee</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fees[]" class="chkbox_fees" checked value="Discount">&nbsp;Discount</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fees[]" class="chkbox_fees" checked value="Final Fee">&nbsp;Final Fee</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fees[]" class="chkbox_fees" checked value="No of Installments">&nbsp;No. of Installments</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fees[]" class="chkbox_fees" checked value="Paid">&nbsp;Paid</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fees[]" class="chkbox_fees" checked value="Pending">&nbsp;Pending</div>
</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Fee Collections Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_fee_collections">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_fee_collections" checked value="Date of Payment" readonly disabled>&nbsp;Date of Payment
<input type="checkbox" name="chkbox_fee_collections[]" checked value="Date of Payment" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_fee_collections" checked value="Student Name" readonly disabled>&nbsp;Student Name
<input type="checkbox" name="chkbox_fee_collections[]" checked value="Student Name" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fee_collections[]" class="chkbox_fee_collections" checked value="Batch">&nbsp;Batch</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fee_collections[]" class="chkbox_fee_collections" checked value="Amount Paid">&nbsp;Amount Paid</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fee_collections[]" class="chkbox_fee_collections" checked value="Mode">&nbsp;Mode</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fee_collections[]" class="chkbox_fee_collections" checked value="Collected By">&nbsp;Collected By</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_fee_collections[]" class="chkbox_fee_collections" checked value="Remarks">&nbsp;Remarks</div>
</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Batches Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_batches">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_batches" checked value="Batch Creation Date" readonly disabled>&nbsp;Batch Creation Date
<input type="checkbox" name="chkbox_batches[]" checked value="Batch Creation Date" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_batches" checked value="Batch Start Date" readonly disabled>&nbsp;Batch Start Date
<input type="checkbox" name="chkbox_batches[]" checked value="Batch Start Date" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Batch End Date">&nbsp;Batch End Date</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Course">&nbsp;Course</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Batch Name">&nbsp;Batch Name</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Duration">&nbsp;Duration (in months)</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Subject">&nbsp;Subject</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Faculty">&nbsp;Faculty</div>

<!-- <div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="BatchTime">&nbsp;Batch Time</div> -->

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batches[]" class="chkbox_batches" checked value="Number of Students">&nbsp;Number of Students</div>
</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Batch Attendance Details Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_batch_attendance_details">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_batch_attendance_details" checked value="Stream" readonly disabled>&nbsp;Stream
<input type="checkbox" name="chkbox_batch_attendance_details[]" checked value="Stream" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_batch_attendance_details" checked value="Course" readonly disabled>&nbsp;Course
<input type="checkbox" name="chkbox_batch_attendance_details[]" checked value="Course" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batch_attendance_details[]" class="chkbox_batch_attendance_details" checked value="Batch Details">&nbsp;Batch Details</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batch_attendance_details[]" class="chkbox_batch_attendance_details" checked value="Student Name">&nbsp;Student Name</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batch_attendance_details[]" class="chkbox_batch_attendance_details" checked value="Present Days">&nbsp;Present Days</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batch_attendance_details[]" class="chkbox_batch_attendance_details" checked value="Absent Days">&nbsp;Absent Days</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batch_attendance_details[]" class="chkbox_batch_attendance_details" checked value="Leave Days">&nbsp;Leave Days</div>

<!-- <div class="col-md-2 float-left"><input type="checkbox" name="chkbox_batch_attendance_details[]" class="chkbox_batch_attendance_details" checked value="Holiday Count">&nbsp;Holiday Count</div> -->
</div>
</div>
</div>
</div>



<?php
/*-----------------------------------------------------
Student Details Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_student_details">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_details" checked value="Stream" readonly disabled>&nbsp;Stream
<input type="checkbox" name="chkbox_student_details[]" checked value="Stream" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_details" checked value="Course" readonly disabled>&nbsp;Course
<input type="checkbox" name="chkbox_student_details[]" checked value="Course" style="display: none;">
</div>


<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_details" checked value="Student Name" readonly disabled>&nbsp;Student Name
<input type="checkbox" name="chkbox_student_details[]" checked value="Student Name" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="Batch Details">&nbsp;Batch Details</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="Mobile">&nbsp;Mobile</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="Email">&nbsp;Email</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="Registration Date">&nbsp;Registration Date</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="Address">&nbsp;Address</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="City Name">&nbsp;City</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="State Name">&nbsp;State</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_details[]" class="chkbox_student_details" checked value="Fee Plan">&nbsp;Fee Plan</div>

</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Student Key Details Columns
-----------------------------------------------------*/
?>
<div class="hide_class" id="columns_student_key_details">	
<div class="row">
<div class="col-md-12">
<div class="form-group">

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_key_details" checked value="Key" readonly disabled>&nbsp;Key
<input type="checkbox" name="chkbox_student_key_details[]" checked value="Key" style="display: none;">
</div>


<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_key_details" checked value="Status" readonly disabled>&nbsp;Status
<input type="checkbox" name="chkbox_student_key_details[]" checked value="Status" style="display: none;">
</div>


<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_key_details" checked value="Assigned Date" readonly disabled>&nbsp;Assigned Date
<input type="checkbox" name="chkbox_student_key_details[]" checked value="Assigned Date" style="display: none;">
</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_key_details[]" class="chkbox_student_key_details" checked value="Activation Date">&nbsp;Activation Date</div>

<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_key_details[]" class="chkbox_student_key_details" checked value="Expiry Date">&nbsp;Expiry Date</div>


<div class="col-md-2 float-left"><input type="checkbox" name="chkbox_student_key_details[]" class="chkbox_student_key_details" checked value="Assigned To">&nbsp;Assigned To</div>

<div class="col-md-2 float-left"><input type="checkbox" class="chkbox_student_key_details" checked value="Student Name" readonly disabled>&nbsp;Full Name
<input type="checkbox" name="chkbox_student_key_details[]" checked value="Full Name" style="display: none;">
</div>

</div>
</div>
</div>
</div>