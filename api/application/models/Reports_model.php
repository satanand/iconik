<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	function prepareHTML($ReportType, $columns, $records, $PostData = array())
	{
		$html_head = $html_filter = $html_body = "";

		if($PostData['SearchType'] != 'w')
		{
			foreach($records[0] as $c)
			{
				$html_head .= "<th>$c</th>";
			}		
		}
			
		//Storing columns names--------------------
		$heads = $records[0];
		unset($records[0]);

		if(isset($records) && !empty($records))
		{
			if($PostData['SearchType'] != 'w' && $PostData['Res'] != 'p' && $PostData['Res'] != 'e')
			{
				//Excludes filters for columns--------------------			
				$excludes_filters = array("LatestRemarks", 
				"FacultyAssigned", "NoOfBatches", "NoOfStudents", "DurationOfCourse(inMonths)", 
				"CourseFee", "Discount", "FinalFee", "NoofInstallments", "Paid", "Pending", 
				"Remarks",
				"BatchEndDate", "NumberofStudents",
				"PresentDays", "AbsentDays", "LeaveDays",
				"FeePlan", "FullName"
				);

				//Link column with data for filtering records--------------------
				$filter_arr = array();
				foreach($heads as $k => $h)
				{					
					$h = str_replace(" ", "", $h);
					$t = array_column($records, $k);
					$filter_arr[$h] = $t;					
				}			

				//Drop down preapre-------------------------------------
				foreach($filter_arr as $select => $arr)
				{					
					if(!in_array($select, $excludes_filters))
					{							
						if($select == "Faculty")
						{
							$t = $f = array();
							$arr = array_unique($arr);								
							asort($arr);
							foreach($arr as $k => $v)
							{
								if(!empty($v))
								{
									$t = explode(",", $v);
									foreach($t as $k1 => $v1)
									{
										$f[] = trim($v1);
									}
								}	
							}

							$arr = $f;
						}

						$temp = "";
						$arr = array_unique($arr);
						asort($arr);
						foreach($arr as $k => $v)
						{
							$v = ucwords(trim($v));

							if(!empty($v))
							$temp .= "<option value='$v'>$v</option>";
						}

						$html_filter .= "<td><select name='innerFilter$select' onchange='search_within_records();'><option value=''>ALL</option>$temp</select></td>";	
					}
					else
					{
						$html_filter .= "<td>&nbsp;</td>";
					}
							
				}
			}
				
			//Displaying search records-------------------------------
			foreach($records as $arr)
			{
				$html_body .= "<tr>";
				
				foreach($arr as $a)
				{
					$html_body .= "<td>".$a."</td>";
				}

				$html_body .= "</tr>";
			}
		}
		else
		{
			$html_body .= "<tr><td colspan='2'>No record found.</td></tr>";
		}

		if($PostData['SearchType'] == 'w')
		{
			$html = "$html_body";
		}
		else
		{
			$html = "<thead><tr>$html_head</tr><tr>$html_filter</tr></thead><tbody>$html_body</tbody>";
		}
			
		return $html;
	}

	
	function generateReport($Inputs = array(), $PageNo = 1, $PageSize = 100)
	{
		$data = array();

		if($Inputs['filterReportType'] == "enquiries")
		{
			$data = $this->enquiries($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "courses")
		{
			$data = $this->courses($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "fees")
		{
			$data = $this->fees($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "fee_collections")
		{
			$data = $this->fee_collections($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "batches")
		{
			$data = $this->batches($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "batch_attendance_details")
		{
			$data = $this->batch_attendance_details($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "student_details")
		{
			$data = $this->student_details($Inputs, $PageNo, $PageSize);
		}
		elseif($Inputs['filterReportType'] == "student_key_details")
		{
			$data = $this->student_key_details($Inputs, $PageNo, $PageSize);
		}

		return $data;
	}	


	function prepareFilters($Where)
	{
		$filter = "";

		if($Where['filterReportType'] == "enquiries")
		{	
			if(!empty($Where['innerFilterFullName']))
			{
				$word = $Where['innerFilterFullName'];
				$filter .= " AND `Full Name` = '".$word."' ";
			}

			if(!empty($Where['innerFilterSource']))
			{
				$word = $Where['innerFilterSource'];
				$filter .= " AND Source = '".$word."' ";
			}

			if(!empty($Where['innerFilterAssignedto']))
			{			
				$word = $Where['innerFilterAssignedto'];
				$filter .= " AND `Assigned to` = '".$word."' ";
			}

			if(!empty($Where['innerFilterInterestedin']))
			{
				$word = $Where['innerFilterInterestedin'];
				$filter .= " AND `Interestedin` = '".$word."' ";
			}

			if(!empty($Where['innerFilterCurrentStatus']))
			{
				$word = $Where['innerFilterCurrentStatus'];
				$filter .= " AND `Current Status` = '".$word."' ";
			}

			if(!empty($Where['innerFilterEnquiryDate']))
			{
				$word = $Where['innerFilterEnquiryDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND DATE(`EnquiryDate`) = '".$word."' ";
			}

			if(!empty($Where['innerFilterMobile']))
			{
				$word = $Where['innerFilterMobile'];
				$filter .= " AND Mobile = '".$word."' ";
			}

			if(!empty($Where['innerFilterLatestRemarks']))
			{
				$word = $Where['innerFilterLatestRemarks'];
				$filter .= " AND `Latest Remarks` = '".$word."' ";
			}

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(`EnquiryDate`) >= '$filterFromDate' AND DATE(`EnquiryDate`) <= '$filterToDate' ) ";
			}
		}
		elseif($Where['filterReportType'] == "courses")
		{	
			if(!empty($Where['innerFilterStream']))
			{
				$word = $Where['innerFilterStream'];
				$filter .= " AND Stream = '".$word."' ";
			}

			if(!empty($Where['innerFilterCourse']))
			{
				$word = $Where['innerFilterCourse'];
				$filter .= " AND Course = '".$word."' ";
			}	

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(`EntryDate`) >= '$filterFromDate' AND DATE(`EntryDate`) <= '$filterToDate' ) ";
			}		
		}
		elseif($Where['filterReportType'] == "fees")
		{			
			if(!empty($Where['innerFilterStream']))
			{
				$word = $Where['innerFilterStream'];
				$filter .= " AND Stream = '".$word."' ";
			}

			if(!empty($Where['innerFilterCourse']))
			{
				$word = $Where['innerFilterCourse'];
				$filter .= " AND Course = '".$word."' ";
			}

			if(!empty($Where['innerFilterStudentName']))
			{
				$word = $Where['innerFilterStudentName'];
				$filter .= " AND `Student Name` LIKE '".$word."%' ";
			}

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(EntryDate) >= '$filterFromDate' AND DATE(EntryDate) <= '$filterToDate' ) ";
			}
		}
		elseif($Where['filterReportType'] == "fee_collections")
		{
			if(!empty($Where['innerFilterDateofPayment']))
			{
				$word = $Where['innerFilterDateofPayment'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `PaymentDate` = '".$word."' ";
			}

			if(!empty($Where['innerFilterStudentName']))
			{
				$word = $Where['innerFilterStudentName'];
				$filter .= " AND `Student Name` = '".$word."' ";
			}

			if(!empty($Where['innerFilterBatch']))
			{
				$word = $Where['innerFilterBatch'];
				$filter .= " AND `Batch` = '".$word."' ";
			}

			if(!empty($Where['innerFilterAmountPaid']))
			{
				$word = $Where['innerFilterAmountPaid'];
				$filter .= " AND `Amount Paid` = '".$word."' ";
			}

			if(!empty($Where['innerFilterMode']))
			{
				$word = $Where['innerFilterMode'];
				$filter .= " AND `Mode` = '".$word."' ";
			}

			if(!empty($Where['innerFilterCollectedBy']))
			{
				$word = $Where['innerFilterCollectedBy'];
				$filter .= " AND `Collected By` = '".$word."' ";
			}

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(PaymentDate) >= '$filterFromDate' AND DATE(PaymentDate) <= '$filterToDate' ) ";
			}
		}
		elseif($Where['filterReportType'] == "batches")
		{
			if(!empty($Where['innerFilterBatchCreationDate']))
			{
				$word = $Where['innerFilterBatchCreationDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `EntryDate` = '".$word."' ";
			}

			if(!empty($Where['innerFilterBatchStartDate']))
			{
				$word = $Where['innerFilterBatchStartDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `StartDate` = '".$word."' ";
			}

			if(!empty($Where['innerFilterCourse']))
			{
				$word = $Where['innerFilterCourse'];
				$filter .= " AND `Course` = '".$word."' ";
			}

			if(!empty($Where['innerFilterBatchName']))
			{
				$word = $Where['innerFilterBatchName'];
				$filter .= " AND `Batch Name` = '".$word."' ";
			}

			if(!empty($Where['innerFilterDuration']))
			{
				$word = $Where['innerFilterDuration'];
				$filter .= " AND Duration = '".$word."' ";
			}

			if(!empty($Where['innerFilterSubject']))
			{
				$word = $Where['innerFilterSubject'];
				$filter .= " AND Subject = '".$word."' ";
			}

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(StartDate) >= '$filterFromDate' AND DATE(StartDate) <= '$filterToDate' ) ";
			}
		}
		elseif($Where['filterReportType'] == "batch_attendance_details")
		{
			if(!empty($Where['innerFilterStream']))
			{
				$word = $Where['innerFilterStream'];				
				$filter .= " AND `Stream` = '".$word."' ";
			}

			if(!empty($Where['innerFilterCourse']))
			{
				$word = $Where['innerFilterCourse'];
				$filter .= " AND `Course` = '".$word."' ";
			}

			if(!empty($Where['innerFilterBatchDetails']))
			{
				$word = $Where['innerFilterBatchDetails'];
				$filter .= " AND `Batch Details` = '".$word."' ";
			}

			if(!empty($Where['innerFilterStudentName']))
			{
				$word = $Where['innerFilterStudentName'];
				$filter .= " AND `Student Name` = '".$word."' ";
			}	
		}
		elseif($Where['filterReportType'] == "student_details")
		{			
			if(!empty($Where['innerFilterStream']))
			{
				$word = $Where['innerFilterStream'];				
				$filter .= " AND `Stream` = '".$word."' ";
			}

			if(!empty($Where['innerFilterCourse']))
			{
				$word = $Where['innerFilterCourse'];
				$filter .= " AND `Course` = '".$word."' ";
			}

			if(!empty($Where['innerFilterStudentName']))
			{
				$word = $Where['innerFilterStudentName'];
				$filter .= " AND `Student Name` = '".$word."' ";
			}

			if(!empty($Where['innerFilterBatchDetails']))
			{
				$word = $Where['innerFilterBatchDetails'];
				$filter .= " AND `Batch Details` = '".$word."' ";
			}

			if(!empty($Where['innerFilterMobile']))
			{
				$word = $Where['innerFilterMobile'];
				$filter .= " AND `Mobile` = '".$word."' ";
			}

			if(!empty($Where['innerFilterEmail']))
			{
				$word = $Where['innerFilterEmail'];
				$filter .= " AND `Email` = '".$word."' ";
			}

			if(!empty($Where['innerFilterRegistrationDate']))
			{
				$word = $Where['innerFilterRegistrationDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `EntryDate` = '".$word."' ";
			}

			if(!empty($Where['innerFilterAddress']))
			{
				$word = $Where['innerFilterAddress'];
				$filter .= " AND `Address` = '".$word."' ";
			}

			if(!empty($Where['innerFilterCityName']))
			{
				$word = $Where['innerFilterCityName'];
				$filter .= " AND `City Name` = '".$word."' ";
			}

			if(!empty($Where['innerFilterStateName']))
			{
				$word = $Where['innerFilterStateName'];
				$filter .= " AND `State Name` = '".$word."' ";
			}	

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(EntryDate) >= '$filterFromDate' AND DATE(EntryDate) <= '$filterToDate' ) ";
			}		
		}
		elseif($Where['filterReportType'] == "student_key_details")
		{			
			/*if(!empty($Where['innerFilterStream']))
			{
				$word = $Where['innerFilterStream'];				
				$filter .= " AND `Stream` = '".$word."' ";
			}*/

			if(!empty($Where['innerFilterCourse']))
			{
				$word = $Where['innerFilterCourse'];
				$filter .= " AND `Course` = '".$word."' ";
			}

			if(!empty($Where['innerFilterFullName']))
			{
				$word = $Where['innerFilterFullName'];
				$filter .= " AND `Full Name` = '".$word."' ";
			}

			if(!empty($Where['innerFilterBatchDetails']))
			{
				$word = $Where['innerFilterBatchDetails'];
				$filter .= " AND `Batch Details` = '".$word."' ";
			}

			if(!empty($Where['innerFilterKey']))
			{
				$word = $Where['innerFilterKey'];
				$filter .= " AND `Key` = '".$word."' ";
			}

			if(!empty($Where['innerFilterStatus']))
			{
				$word = $Where['innerFilterStatus'];
				$filter .= " AND `Status` = '".$word."' ";
			}

			if(!empty($Where['innerFilterAssignedTo']))
			{
				$word = $Where['innerFilterAssignedTo'];
				$filter .= " AND `Assigned To` = '".$word."' ";
			}

			if(!empty($Where['innerFilterAssignedDate']))
			{
				$word = $Where['innerFilterAssignedDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `AssignedDate` = '".$word."' ";
			}

			if(!empty($Where['innerFilterActivationDate']))
			{
				$word = $Where['innerFilterActivationDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `ActivationDate` = '".$word."' ";
			}

			if(!empty($Where['innerFilterExpiryDate']))
			{
				$word = $Where['innerFilterExpiryDate'];
				$word = date("Y-m-d", strtotime($word));
				$filter .= " AND `ExpiryDate` = '".$word."' ";
			}

			if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
			{
				$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
				$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

				$filter .= " AND (DATE(AssignedDate) >= '$filterFromDate' AND DATE(AssignedDate) <= '$filterToDate' ) ";
			}
		}
		

		return $filter;
	}

	
	function enquiries($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);
				
		$sql = "SELECT * FROM
		(
		SELECT e.EnquiryPersonName as `Full Name`, e.EnquiryMobile as Mobile, e.EnquiryEmail, e.EnquiryInterestedIn as `Interested in`, e.EnquiryRemarks, e.EnquiryLastestRemark as `Latest Remarks`, e.EnquirySource as Source, DATE_FORMAT(e.EnquiryDate, '%d-%M-%Y') as `Enquiry Date`, s.EnquiryStatusName as `Current Status`, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Assigned to`, DATE(e.EnquiryDate) as EnquiryDate
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
		LEFT JOIN tbl_users u ON e.EnquiryAssignToUser = u.UserID
		WHERE e.StatusID = 2 AND e.EnquiryInstituteID = '$InstituteID' 				
		ORDER BY e.EnquiryDate, e.EnquiryID
		) as temp 
		WHERE 1 = 1 $filter
		LIMIT $PageNo, $PageSize ";			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_enquiries'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				if($Record['Latest Remarks'] == "") 
					$Record['Latest Remarks'] = $Record['EnquiryRemarks'];

				$temp = array();
				foreach($Where['chkbox_enquiries'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_enquiries'], $Records, $Where);
		}	

		return $Return;			
	}



	function courses($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);		
	
		$sql = "SELECT * FROM
		(
		SELECT c1.CategoryName as Stream, c2.CategoryName as Course, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as `Entry Date`, 'Active' as Status, DATE(e.EntryDate) as EntryDate, count(b.BatchID) as `No Of Batches`, b.CourseDuration as `Duration Of Course(in Months)`, b.BatchID, c2.CategoryID as CourseID
		FROM set_categories c1		
		INNER JOIN tbl_entity e ON c1.CategoryID = e.EntityID AND e.StatusID = 2 AND e.InstituteID = $InstituteID
		INNER JOIN set_categories c2 ON c2.ParentCategoryID = c1.CategoryID AND c2.CategoryTypeID = 2
		INNER JOIN tbl_batch b ON c2.CategoryID = b.CourseID		
		WHERE e.InstituteID = $InstituteID 
		GROUP BY c2.CategoryID
		ORDER BY c1.CategoryName, c2.CategoryName
		) as temp
		WHERE 1 = 1 $filter
		LIMIT $PageNo, $PageSize
		";
		
		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_courses'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				if(isset($Record['CourseID']) && !empty($Record['CourseID']))
				{
					$Record['No Of Students'] = $this-> getBatchStudentCount('c', $Record['CourseID'], 0);
				}

				$temp = array();
				foreach($Where['chkbox_courses'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_courses'], $Records, $Where);
		}	

		return $Return;			
	}


	function fees($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);
				
		$sql = "SELECT * FROM
		(
		SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Student Name`, s.FeeID, s.InstallmentID, s.TotalFeeAmount, s.TotalFeeInstallments, s.InstallmentAmount, c.CategoryName as Course, c1.CategoryName as Stream, s.StudentID, c.CategoryID as CourseID, DATE(e.EntryDate) as EntryDate
		FROM tbl_students s
		INNER JOIN tbl_users u ON s.StudentID = u.UserID
		INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.StatusID != 3 AND e.InstituteID = $InstituteID
		INNER JOIN set_categories c ON c.CategoryID = s.CourseID AND c.CategoryTypeID = 2
		INNER JOIN set_categories c1 ON c.CategoryID = s.CourseID AND c.ParentCategoryID = c1.CategoryID AND c1.CategoryTypeID = 1
		WHERE e.InstituteID = $InstituteID
		ORDER BY c1.CategoryName, c.CategoryName, u.FirstName, u.LastName
		) as temp
		WHERE 1 = 1 $filter
		LIMIT $PageNo, $PageSize ";			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_fees'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{				
				$fees_details = $this-> getFeeDetailsOfStudent($Record['StudentID'], $Record['CourseID'], $Record['FeeID'], $Record['InstallmentID'], $Record['TotalFeeAmount'], $Record['TotalFeeInstallments'], $Record['InstallmentAmount']);
				
				if(isset($fees_details) && !empty($fees_details))
				{
					$Record["Course Fee"] = $fees_details["TotalFee"];
					$Record["Discount"] = $fees_details["DiscountAmount"];
					$Record["Final Fee"] = $fees_details["NetTotalFee"];
					$Record["No of Installments"] = $fees_details["NoOfInstallment"];
					$Record["InstallmentAmount"] = $fees_details["InstallmentAmount"];
					$Record["Paid"] = $fees_details["PaidAmount"];
					$Record["Pending"] = $fees_details["RemainingAmount"];					
				}

				$temp = array();
				foreach($Where['chkbox_fees'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_fees'], $Records, $Where);
		}	

		return $Return;			
	}


	function fee_collections($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);
				
		$sql = "SELECT * FROM 
		(
		SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Student Name`, b.BatchName as Batch, DATE_FORMAT(f.PaymentDate, '%d-%M-%Y') as `Date of Payment`, CONCAT(u1.FirstName, ' ', IF(u1.LastName IS NOT NULL, u1.LastName,'')) as `Collected By`, f.Amount as `Amount Paid`, f.PaymentMode as Mode, DATE(f.PaymentDate) as PaymentDate
		FROM tbl_fee_collection f 
		INNER JOIN tbl_students s ON f.StudentID = s.StudentID		
		INNER JOIN tbl_users u ON u.UserID = f.StudentID
		INNER JOIN tbl_entity e ON f.StudentID = e.EntityID AND e.InstituteID = $InstituteID
		INNER JOIN tbl_batch b ON b.BatchID = f.BatchID	
		INNER JOIN tbl_users u1 ON u1.UserID = f.UserID	
		WHERE f.InstituteID = $InstituteID 
		ORDER BY f.PaymentDate, u.FirstName, u.LastName
		) as temp
		WHERE 1 = 1 $filter
		LIMIT $PageNo, $PageSize ";			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_fee_collections'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				$temp = array();
				foreach($Where['chkbox_fee_collections'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_fee_collections'], $Records, $Where);
		}	

		return $Return;			
	}



	function batches($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);
		
		$app = "";
		if(isset($Where['innerFilterFaculty']) && !empty($Where['innerFilterFaculty']))
		{
			$fword = strtolower(trim($Where['innerFilterFaculty']));
			$app = " AND (Faculty LIKE '%$fword%') ";
		}

		$sql = "SELECT * FROM
		(
		SELECT GROUP_CONCAT(CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) SEPARATOR ',') as Faculty,  
		b.BatchName as `Batch Name`, b.Duration, DATE_FORMAT(b.StartDate, '%d-%M-%Y') as `Batch Start Date`, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as `Batch Creation Date`, c1.CategoryName as Subject, c.CategoryName as Course, b.BatchID, DATE(e.EntryDate) as EntryDate, DATE(b.StartDate) as StartDate, DATE_FORMAT(DATE_ADD(b.StartDate, INTERVAL b.Duration MONTH), '%d-%M-%Y') as `Batch End Date`
		FROM tbl_batch b		
		INNER JOIN tbl_entity e ON b.BatchID = e.EntityID AND e.InstituteID = $InstituteID	
		INNER JOIN set_categories c ON c.CategoryID = b.CourseID AND c.CategoryTypeID = 2
		INNER JOIN set_categories c1 ON c1.ParentCategoryID = c.CategoryID AND c1.CategoryTypeID = 3
		LEFT JOIN tbl_batchbyfaculty bf ON bf.BatchID = b.BatchID AND bf.AssignStatus = 2 AND bf.InstituteID = $InstituteID
		LEFT JOIN tbl_users u ON u.UserID = bf.FacultyID AND bf.InstituteID = $InstituteID		
		WHERE e.InstituteID = $InstituteID AND b.StatusID = 2 
		GROUP BY b.BatchID			
		) as temp
		WHERE 1 = 1 $filter $app
		LIMIT $PageNo, $PageSize ";			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_batches'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				if(isset($Record['Faculty']) && !empty($Record['Faculty']) )
				{
					$a = explode(",", $Record['Faculty']);
					$a = array_unique($a);
					$Record['Faculty'] = implode(", ", $a);
				}

				$Record['Number of Students'] = $this-> getBatchStudentCount('b', 0, $Record['BatchID']);

				$temp = array();
				foreach($Where['chkbox_batches'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_batches'], $Records, $Where);
		}	

		return $Return;			
	}


	function batch_attendance_details($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);

		$subsql = "";
		if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
		{
			$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
			$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

			$subsql = " AND s.StudentID IN (SELECT a.StudentIDs
			FROM tbl_attendance	a
			INNER JOIN tbl_entity e ON a.StudentIDs = e.EntityID AND e.InstituteID = $InstituteID	
			WHERE (DATE(a.EntryDate) >= '$filterFromDate' AND DATE(a.EntryDate) <= '$filterToDate' ) 
			)";
		}


		$sql = "SELECT * FROM
		(
		SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Student Name`, b.BatchName as `Batch Details`, c.CategoryName as Course, c1.CategoryName as Stream, s.StudentID, b.BatchID
		FROM tbl_students s
		INNER JOIN tbl_users u ON s.StudentID = u.UserID
		INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.InstituteID = $InstituteID
		INNER JOIN tbl_batch b ON b.BatchID = s.BatchID		
		INNER JOIN set_categories c ON c.CategoryID = b.CourseID AND c.CategoryTypeID = 2
		INNER JOIN set_categories c1 ON c.ParentCategoryID = c1.CategoryID AND c1.CategoryTypeID = 1
		WHERE e.InstituteID = $InstituteID $subsql
		ORDER BY c1.CategoryName, c.CategoryName, u.FirstName
		) as temp
		WHERE 1 = 1 $filter
		LIMIT $PageNo, $PageSize ";	
			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_batch_attendance_details'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				$attendance_details = $this-> getAttendanceDetailsOfStudent($Record['StudentID'], $Record['BatchID'], $filterFromDate, $filterToDate);

				$Record['Present Days'] = $Record['Absent Days'] = $Record['Leave Days'] = "";
				if(isset($attendance_details) && !empty($attendance_details))
				{
					$Record['Present Days'] = $attendance_details['Present'];
					$Record['Absent Days'] = $attendance_details['Absent'];
					$Record['Leave Days'] = $attendance_details['OnLeave'];
				}

				$temp = array();
				foreach($Where['chkbox_batch_attendance_details'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_batch_attendance_details'], $Records, $Where);
		}	

		return $Return;			
	}


	function student_details($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);
				
		$sql = "SELECT * FROM
		(
		SELECT u.StateName as `State Name`, u.CityName as `City Name`, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Student Name`, s.Email, s.PhoneNumber as Mobile, s.FeeID, s.InstallmentID, u.PhoneNumber, s.TotalFeeAmount, b.BatchName as `Batch Details`, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as `Registration Date`, c.CategoryName as Course, c1.CategoryName as Stream, s.StudentID, c.CategoryID as CourseID, DATE(e.EntryDate) as EntryDate, u.Address
		FROM tbl_students s
		INNER JOIN tbl_users u ON s.StudentID = u.UserID
		INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.StatusID != 3 AND e.InstituteID = $InstituteID
		INNER JOIN tbl_batch b ON b.BatchID = s.BatchID
		INNER JOIN set_categories c ON c.CategoryID = s.CourseID AND c.CategoryTypeID = 2
		/*INNER JOIN set_categories c1 ON c.CategoryID = s.CourseID AND c1.ParentCategoryID = c.CategoryID AND c1.CategoryTypeID = 1*/
		INNER JOIN set_categories c1 ON c.ParentCategoryID = c1.CategoryID AND c1.CategoryTypeID = 1
		WHERE e.InstituteID = $InstituteID
		ORDER BY c1.CategoryName, c.CategoryName, u.FirstName
		) as temp
		WHERE 1 = 1 $filter 
		LIMIT $PageNo, $PageSize ";			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_student_details'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				$Record['Fee Plan'] = "";
				$fees_details = $this-> getFeeDetailsOfStudent($Record['StudentID'], $Record['CourseID'], $Record['FeeID'], $Record['InstallmentID'], $Record['TotalFeeAmount'], $Record['TotalFeeInstallments'], $Record['InstallmentAmount']);
				
				if(isset($fees_details) && !empty($fees_details))
				{					
					$Record['Fee Plan'] = $fees_details['FeePlan']." (".$fees_details['NetTotalFee'].")";	
				}

				$temp = array();
				foreach($Where['chkbox_student_details'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_student_details'], $Records, $Where);
		}	

		return $Return;			
	}


	function student_key_details($Where = array(), $PageNo = 1, $PageSize = 100)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		
		$PageNo = ($PageNo - 1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = $this-> prepareFilters($Where);
				
		$sql = "SELECT * FROM
		(
			SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Full Name`, b.BatchName as `Batch Details`, c.CategoryName as Course, s.Key, DATE_FORMAT(s.KeyAssignedOn, '%d-%M-%Y') as `Assigned Date`, DATE_FORMAT(s.ActivatedOn, '%d-%M-%Y') as `Activation Date`, DATE_FORMAT(s.ExpiredOn, '%d-%M-%Y') as `Expiry Date`, IF(s.KeyStatusID = 1, 'Inactive', IF(s.KeyStatusID = 2, 'Active', 'Not Assigned')) as Status, DATE(s.KeyAssignedOn) as AssignedDate, DATE(s.ActivatedOn) as ActivationDate, DATE(s.ExpiredOn) as ExpiryDate, ut.UserTypeName as `Assigned To`
			FROM tbl_students s
			INNER JOIN tbl_users u ON s.StudentID = u.UserID
			INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.StatusID != 3 AND e.InstituteID = $InstituteID
			INNER JOIN tbl_batch b ON b.BatchID = s.BatchID
			INNER JOIN set_categories c ON c.CategoryID = s.CourseID AND c.CategoryTypeID = 2
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID AND ut.UserTypeID = 7				
			WHERE e.InstituteID = $InstituteID		


			UNION ALL

			SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Full Name`, '' as `Batch Details`, '' as Course, s.Key, DATE_FORMAT(s.KeyAssignedOn, '%d-%M-%Y') as `Assigned Date`, DATE_FORMAT(s.ActivatedOn, '%d-%M-%Y') as `Activation Date`, DATE_FORMAT(s.ExpiredOn, '%d-%M-%Y') as `Expiry Date`, IF(s.KeyStatusID = 1, 'Inactive', IF(s.KeyStatusID = 2, 'Active', 'Not Assigned')) as Status, DATE(s.KeyAssignedOn) as AssignedDate, DATE(s.ActivatedOn) as ActivationDate, DATE(s.ExpiredOn) as ExpiryDate, ut.UserTypeName as `Assigned To`
			FROM tbl_staff_assign_keys s
			INNER JOIN tbl_users u ON s.StaffID = u.UserID		
			INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.StatusID != 3 AND e.InstituteID = $InstituteID			
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID
			WHERE e.InstituteID = $InstituteID		

		) as temp
		WHERE 1 = 1 $filter
		
		LIMIT $PageNo, $PageSize ";			

		$Query = $this->db->query($sql);

		$Records = array();
		$Return['Data']['TotalRecords'] = 0;
		
		$Records[] = $Where['chkbox_student_key_details'];
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{
				if($Record['Assigned To'] == 'Students')
				{
					$Record['Full Name'] = $Record['Full Name']. " (".$Record['Course']."->".$Record['Batch Details'].")";
				}

				$temp = array();
				foreach($Where['chkbox_student_key_details'] as $c)
				{
					$temp[] = $Record[$c];
				}
				
				$Records[] = $temp;
			}			
		}

		if(isset($Where['Res']) && $Where['Res'] == "e")
		{
			$Return['Data']['Records'] = $Records;
		}
		else
		{
			$Return['Data']['Records'] = $this-> prepareHTML($Where['filterReportType'], $Where['chkbox_student_key_details'], $Records, $Where);
		}	

		return $Return;			
	}



	function saveSearch($Input = array())
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
		
		$this->db->trans_start();
		
		$InsertData = (array(				
		"InstituteID" =>	$InstituteID,
		"ReportType" => strtolower(@$Input['filterReportType']),
		
		"ReportFromDate" =>	date("Y-m-d", strtotime($Input['filterFromDate'])),
		"ReportToDate" =>	date("Y-m-d", strtotime($Input['filterToDate'])),

		"SearchName" =>	@$Input['SearchName'],
		"FiltersContent" =>	"",		
		"SaveDate" 	=>	date('Y-m-d H:i:s')));

		$this->db->insert('tbl_save_search', $InsertData);
		
		$ID = $this->db->insert_id();		
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $ID;		
	}



	function deleteSearch($SaveID)
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $this->db->where("InstituteID", $InstituteID);        
        $this->db->where("SaveID", $SaveID);        
        $this->db->delete("tbl_save_search");
        
        return $this->db->affected_rows();
    }


    function getFacultyName($SubjectID, $InstituteID)
    {
    	$sql = "SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as FacultyName
		FROM tbl_users u
		INNER JOIN tbl_user_jobs j ON j.UserID = u.UserID
		INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.InstituteID = $InstituteID
		WHERE e.InstituteID = $InstituteID AND FIND_IN_SET($SubjectID, j.SubjectID)		
		";

		$Query = $this->db->query($sql);

		$str = "";				
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$str .= ", ".ucwords($Record['FacultyName']);
			}

			$str = substr($str, 1);
		}	

		return $str;
    }

    function getFeeDetailsOfStudent($StudentID, $CourseID, $FeeID, $InstallmentID, $TotalFeeAmount, $NoOfInstallment, $InstallmentAmount)
    {
    	$details = array("FeePlan" => "",
    		"TotalFee" => "", 
    		"DiscountAmount" => "",
    		"NetTotalFee" => "",
    		"NoOfInstallment" => "",
    		"InstallmentAmount" => "",
    		"PaidAmount" => 0,
    		"RemainingAmount" => ""    		
    	);
    	                                 
        if(isset($FeeID) && !empty($FeeID))
        {
            $query = $this->db->query("SELECT FullAmount, DiscountAmount, FullDiscountFee 
            	FROM tbl_setting_fee 
            	WHERE FeeID = ".$FeeID."
            	LIMIT 1");
            
            $FeeData = $query->result_array();
            
            $details['FeePlan'] = "Package";
            $details['TotalFee'] = $FeeData[0]['FullAmount'];
            $details['DiscountAmount'] = $FeeData[0]['DiscountAmount'];
            $details['NetTotalFee'] = $FeeData[0]['FullDiscountFee'];
            $details['NoOfInstallment'] = "";
            $details['InstallmentAmount'] = "";
        }
        elseif(isset($InstallmentID) && !empty($InstallmentID))
        {
            $sql = "SELECT TotalFee, InstallmentAmount, NoOfInstallment
            FROM tbl_setting_installment
            WHERE InstallmentID = ".$InstallmentID."
            LIMIT 1
            ";           
           
            $query = $this->db->query($sql);
            
            $InstallmentData = $query->result_array();             
            
            $details['FeePlan'] = "Plan";    	       
            $details['TotalFee'] = $InstallmentData[0]['TotalFee'];
            $details['DiscountAmount'] = 0;
            $details['NetTotalFee'] = $InstallmentData[0]['TotalFee'];
            $details['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
            $details['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
        }
        elseif(isset($TotalFeeAmount) && !empty($TotalFeeAmount))
        {
            $details['FeePlan'] = "Customized"; 
            $details['TotalFee'] = $TotalFeeAmount;
            $details['DiscountAmount'] = 0;
            $details['NetTotalFee'] = $TotalFeeAmount;
            $details['NoOfInstallment'] = $NoOfInstallment;
            $details['InstallmentAmount'] = $InstallmentAmount;
        }              

        $query = $this->db->query("SELECT Sum(Amount) as PaidAmount 
        	FROM tbl_fee_collection 
        	WHERE StudentID = ".$StudentID. " AND CourseID = ".$CourseID);
        $SumData = $query->result_array(); 
        if(!empty($SumData) && $SumData[0]['PaidAmount'] > 0)
        {
            $details['PaidAmount'] = $SumData[0]['PaidAmount'];
        }
        else
        {
            $details['PaidAmount'] = 0;
        } 

        if(isset($details['NetTotalFee']) && !empty($details['NetTotalFee']))
        {
        	$details['RemainingAmount'] = (int)$details['NetTotalFee'] - (int)$details['PaidAmount'];
        } 


        foreach($details as $k => $v)
        {        	
	        if($k != "FeePlan")
	        {
	        	$details[$k] = number_format($v, '0', '', '');
	        	if($details[$k] == 0)
	        	{
	        		$details[$k] = "";
	        	}
	        }	
        }              	

        return $details;
    }


    function getBatchStudentCount($Type = 'b', $CourseID = 0, $BatchID = 0)
    {
    	$count = 0;

	    if($Type == "c")
	    {
	    	$sql = "SELECT COUNT(StudentID) as Total
			FROM tbl_students		
			WHERE CourseID = $CourseID
			GROUP BY CourseID
			";
		}
		else
		{
			$sql = "SELECT COUNT(StudentID) as Total
			FROM tbl_students		
			WHERE BatchID = $BatchID
			GROUP BY BatchID
			";
		}	

		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			$Record = $Query->result_array();
			
			$count = $Record[0]['Total'];
		}

    	return $count;
    }

    function getAttendanceDetailsOfStudent($StudentID, $BatchID, $FromDate, $ToDate)
    {
    	$sql = "SELECT
    	(
	    	SELECT COUNT(AttendanceStatus) as Total
			FROM tbl_attendance		
			WHERE BatchID = $BatchID AND StudentIDs = $StudentID AND AttendanceStatus = 'Present'
			AND (DATE(EntryDate) >= '$FromDate' AND DATE(EntryDate) <= '$ToDate' )
			GROUP BY BatchID, StudentIDs
		) as Present, 	
			
		(
			SELECT COUNT(AttendanceStatus) as Total
			FROM tbl_attendance		
			WHERE BatchID = $BatchID AND StudentIDs = $StudentID AND AttendanceStatus = 'Absent'
			AND (DATE(EntryDate) >= '$FromDate' AND DATE(EntryDate) <= '$ToDate' )
			GROUP BY BatchID, StudentIDs
		) as Absent,	
			
		(
			SELECT COUNT(AttendanceStatus) as Total
			FROM tbl_attendance		
			WHERE BatchID = $BatchID AND StudentIDs = $StudentID AND AttendanceStatus = 'OnLeave'
			AND (DATE(EntryDate) >= '$FromDate' AND DATE(EntryDate) <= '$ToDate' )
			GROUP BY BatchID, StudentIDs
		) as OnLeave";

		$Query = $this->db->query($sql);
		
		$Result = $Query->result_array();		

		if(isset($Result) && !empty($Result))
			return $Result[0];

		
		return array();
    }


    function searchHistory($Input)
    {
    	$Records = array();

    	$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

    	$sql = "SELECT SaveID, ReportType, SearchName, DATE_FORMAT(SaveDate, '%d-%m-%Y') as SearchDate, DATE_FORMAT(ReportFromDate, '%d-%m-%Y') as ReportFromDate, DATE_FORMAT(ReportToDate, '%d-%m-%Y') as ReportToDate, FiltersContent
		FROM tbl_save_search		
		WHERE InstituteID = $InstituteID
		ORDER BY SaveID DESC ";

		$Query = $this->db->query($sql);
		
		$Return['Data']['TotalRecords'] = 0;		
		
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();			

			foreach($Query->result_array() as $Record)
			{				
				$Record['FiltersContent'] = json_decode($Record['FiltersContent']);
				$Records[] = $Record;
			}			
		}

		$Return['Data']['Records'] = $Records;

		return $Return;
    }


    function dailyRegistrations()
	{
		$sql = " SELECT * FROM 
		(
			SELECT DATE(e.EntryDate) as RegDate, COUNT(e.EntityID) as Total, ut.UserTypeName, 'Inactive' as Type, ut.UserTypeID   
			FROM tbl_entity e
			INNER JOIN tbl_users u ON u.UserID = e.EntityID
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID AND ut.UserTypeID IN(7, 10, 11, 82, 83, 87, 88, 90)			
			WHERE e.StatusID IN (1) AND e.InstituteID != 10
			GROUP BY DATE(e.EntryDate), ut.UserTypeID

			UNION ALL

			SELECT DATE(e.EntryDate) as RegDate, COUNT(e.EntityID) as Total, ut.UserTypeName, 'Active' as Type, ut.UserTypeID
			FROM tbl_entity e
			INNER JOIN tbl_users u ON u.UserID = e.EntityID
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID AND ut.UserTypeID IN(7, 10, 11, 82, 83, 87, 88, 90)			
			WHERE e.StatusID IN (2) AND e.InstituteID != 10
			GROUP BY DATE(e.EntryDate), ut.UserTypeID

		) as temp
		ORDER BY RegDate, UserTypeName, Type, Total 
		";			

		$Query = $this->db->query($sql);

		$Records = array();		
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{				
				if($Record['UserTypeID'] == 88) $Record['UserTypeName'] = "MP Students";
				elseif($Record['UserTypeID'] == 90) $Record['UserTypeName'] = "MP Faculty";

				$Records[] = $Record;
			}			
		}

		$Return['Data']['Records'] = $Records;		

		return $Return;			
	}


	function dailyRegistrationsDetails()
	{
		//7, 10, 11, 82, 83, 87, 90
		$sql = "SELECT * FROM 
		(
			SELECT ut.UserTypeName, CONCAT(u.FirstName, ' ', IF(u.LastName != '', u.LastName, '')) as Name, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as RegDate, s.StatusName, '' as InstituteName   
			FROM tbl_entity e
			INNER JOIN tbl_users u ON u.UserID = e.EntityID AND e.InstituteID != 10
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID AND u.UserTypeID IN (10, 82, 83, 87, 88, 90)
			INNER JOIN set_status s ON s.StatusID = e.StatusID						
			WHERE e.InstituteID != 10 AND u.UserTypeID IN (10, 82, 83, 87, 90) 

			
			UNION ALL

			
			SELECT 'AP Students' as UserTypeName, CONCAT(u.FirstName, ' ', IF(u.LastName != '', u.LastName, '')) as Name, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as RegDate, s.StatusName, CONCAT(u1.FirstName, ' ', IF(u1.LastName != '', u1.LastName, '')) as InstituteName   
			FROM tbl_users u 
			INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.InstituteID != 10
			INNER JOIN tbl_users u1 ON u1.UserID = e.InstituteID
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID AND u.UserTypeID IN (7)
			INNER JOIN set_status s ON s.StatusID = e.StatusID						
			WHERE e.InstituteID != 10 AND u.UserTypeID IN (7)


			UNION ALL

			
			SELECT 'AP Faculty' as UserTypeName, CONCAT(u.FirstName, ' ', IF(u.LastName != '', u.LastName, '')) as Name, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as RegDate, s.StatusName, CONCAT(u1.FirstName, ' ', IF(u1.LastName != '', u1.LastName, '')) as InstituteName   
			FROM tbl_users u 
			INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.InstituteID != 10
			INNER JOIN tbl_users u1 ON u1.UserID = e.InstituteID
			INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID AND u.UserTypeID IN (11)
			INNER JOIN set_status s ON s.StatusID = e.StatusID						
			WHERE e.InstituteID != 10 AND u.UserTypeID IN (11)		


		) as temp
		ORDER BY UserTypeName, Name 
		";			

		$Query = $this->db->query($sql);

		$Records = array();		
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$Records[] = $Record;
			}			
		}

		$Return['Data']['Records'] = $Records;		

		return $Return;	


				

	}
    
}