<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeschuling extends CI_Controller {

	function __construct()
	{
        // Call the Model constructor
		parent::__construct();
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
		$this->load->model('Calendar_model');
		$this->load->model('Common_model');
		$this->load->model('Entity_model');

	}


	public function getEvents()
	{

		$result=$this->Calendar_model->getEvents();
		echo json_encode($result);
	}


	//Get Time Scheduling Events For Faculty---------------------------------
	public function getTimeSchedulingEvents()
	{
		$today = date("Y-m-d");
				
		//check the current day
		if(date('D')!='Mon')
		  	$week_start = date('Y-m-d',strtotime('last Monday'));
		else
		   	$week_start = date('Y-m-d');   		

		//always next saturday
		if(date('D')!='Sat')
		    $week_end = date('Y-m-d',strtotime('next Saturday'));
		else
			$week_end = date('Y-m-d');
		

		$month_start = date("Y-m-01");
		$t = date("t", strtotime("Y-m-01"));
		$month_end = date("Y-m-$t");


		//Today Schedule------------------------------------------
		$result['today'] = $this->Calendar_model->getTimeSchedulingEvents($today, $today);

		//Week Schedule------------------------------------------
		$result['week'] = $this->Calendar_model->getTimeSchedulingEvents($week_start, $week_end);

		//Month Schedule------------------------------------------
		$result['month'] = $this->Calendar_model->getTimeSchedulingEvents($month_start, $month_end);
		
		$result['ResponseCode'] = 200;
		$result['Message'] = "";

		//echo "<pre>"; print_r($result); die;
		
		echo json_encode($result);


	}



}
