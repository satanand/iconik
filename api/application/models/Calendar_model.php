<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_model extends CI_Model {

    public function __construct()
	{
		parent::__construct();	
	}

	/*Read the data from DB */
	function getEvents($data=array())
	{
		 //$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

		//$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//echo $InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		//echo $_GET['InstituteGUID'];
	 $InstituteID=$this->Common_model->getInstituteByEntityGUID($_GET['InstituteGUID']);
	 $UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
	  // $UserID=$this->Common_model->getUserIDByGUID($_GET['UserGUID']);

		 $FacultyID=$_GET['FacultyID']; 
		 $UserTypeID=$_GET['UserTypeID'];
			
			if(!empty($FacultyID) && $FacultyID!='All'){

			$sql = "SELECT es.*, CONCAT_WS(' ',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,SC.CategoryName as CourseName,GROUP_CONCAT(SJ.CategoryName) as SubjectName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID join set_categories SC on SC.CategoryID=B.CourseID join set_categories SJ on find_in_set(SJ.CategoryID,es.SubjectID) WHERE  E.InstituteID=$InstituteID AND U.UserID=$FacultyID AND es.start BETWEEN ? AND ? GROUP BY es.id ORDER BY es.start ASC";

			//echo $sql; die();

		}elseif(!empty($FacultyID) && $FacultyID=='All'){

			$sql = "SELECT es.*, CONCAT_WS(' ',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,SC.CategoryName as CourseName,GROUP_CONCAT(SJ.CategoryName) as SubjectName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID join set_categories SC on SC.CategoryID=B.CourseID join set_categories SJ on find_in_set(SJ.CategoryID,es.SubjectID) WHERE  E.InstituteID=$InstituteID  AND es.start BETWEEN ? AND ? GROUP BY es.id ORDER BY es.start ASC";

			//echo $sql; die();

		}elseif($UserTypeID == 10){

			$sql = "SELECT es.*, CONCAT_WS(' ',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,SC.CategoryName as CourseName,GROUP_CONCAT(SJ.CategoryName) as SubjectName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID join set_categories SC on SC.CategoryID=B.CourseID join set_categories SJ on find_in_set(SJ.CategoryID,es.SubjectID) WHERE  E.InstituteID=$InstituteID AND es.start BETWEEN ? AND ? GROUP BY es.id ORDER BY es.start ASC";

		}else{

			$sql = "SELECT es.*, CONCAT_WS(' ',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,SC.CategoryName as CourseName,GROUP_CONCAT(SJ.CategoryName) as SubjectName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID join set_categories SC on SC.CategoryID=B.CourseID join set_categories SJ on find_in_set(SJ.CategoryID,es.SubjectID) WHERE  E.InstituteID=$InstituteID AND U.UserID=$UserID AND es.start BETWEEN ? AND ? GROUP BY es.id ORDER BY es.start ASC";
		}
	
		

		return $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();

		/*}*/

	}



	/*Read the data from DB */
	function getMySchecdules($EntityID,$UserID="",$CategoryID="",$Input=array())
	{
		 //$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

		//$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//echo $InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		//echo $_GET['InstituteGUID'];
	 	$InstituteID = $this->Common_model->getInstituteByEntityGUID($EntityID);

	 	//echo "SELECT es.*, CONCAT_WS('',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID WHERE E.InstituteID = ".$InstituteID." AND es.BatchID = (select BatchID from tbl_students where StudentID = '".$EntityID."')  ORDER BY es.start ASC";
	 	$append = "";
	 	if(!empty($UserID)){
	 		$append .= "AND es.FacultyID = '".$UserID."'";
	 	}

	 	if(!empty($CategoryID)){
	 		$append .= "AND es.CourseID = '".$CategoryID."'";
	 	}
	
		$sql = "SELECT es.*, CONCAT_WS('',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,C.CategoryName as CourseName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID join set_categories C on es.CourseID=C.CategoryID WHERE E.InstituteID = 
		".$InstituteID."  AND es.BatchID = (select BatchID from tbl_students where StudentID = '".$EntityID."') $append  ORDER BY es.start ASC";

			echo $sql; die();

		return $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();

		/*}*/

	}

    /*GEt Faculty */
	function getFaculty($CourseID)
	{
		 
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('U.UserID,U.UserGUID,CONCAT_WS(" ",U.FirstName,U.LastName) FullName');
		$this->db->from('tbl_batch B');
		$this->db->join('tbl_batchbyfaculty BF', 'BF.BatchID = B.BatchID');
		$this->db->join('tbl_users U', 'BF.FacultyID = U.UserID');
		$this->db->join('tbl_entity E', 'E.EntityID = U.UserID');
		$this->db->where("E.StatusID",2);
		$this->db->where("U.FirstName!=",'');
		$this->db->where("U.UserGUID!=",'');
		$this->db->where("U.UserTypeID",11);
	    $this->db->where("B.CourseID",$CourseID);
		$this->db->where("BF.InstituteID",$InstituteID);
		$this->db->GROUP_BY('UserID');
		$this->db->order_by('FullName','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		if($Query->num_rows()>0){

			return $Query->result();
		}
		return FALSE;

	    } 

	  /*GEt Faculty */
	function getFacultyList()
	{
		 
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('U.UserID,U.UserGUID,CONCAT_WS(" ",U.FirstName,U.LastName) FullName');
		// $this->db->from('tbl_batch B');
		// $this->db->join('tbl_batchbyfaculty BF', 'BF.BatchID = B.BatchID');
		$this->db->from('tbl_users U');
		$this->db->join('tbl_entity E', 'E.EntityID = U.UserID');
		$this->db->where("E.StatusID",2);
		$this->db->where("U.FirstName!=",'');
		$this->db->where("U.UserGUID!=",'');
		$this->db->where("U.UserTypeID",11);
		$this->db->where("E.InstituteID",$InstituteID);
		$this->db->GROUP_BY('UserID');
		$this->db->order_by('FullName','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		if($Query->num_rows()>0){

			return $Query->result();
		}
		return FALSE;

	}

	/*data getBatch */
	 function getBatch($FacultyID)
	{
		//$FacultyID=$_GET['Faculty'];
		 $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID,"");
		
		 $sql = "SELECT B.BatchID,B.BatchName FROM tbl_batch B join tbl_entity E on B.BatchID=E.EntityID join tbl_batchbyfaculty bf on B.BatchID=bf.BatchID WHERE E.InstituteID=$InstituteID  And bf.FacultyID=$FacultyID And E.StatusID!=3";
		return $this->db->query($sql,'')->result();

	}

	/*data getSubject */
	function getSubject($BatchID)
	{
		$this->db->select('C.CategoryName,C.CategoryID');
		$this->db->from('tbl_batch B');
		$this->db->join('set_categories C', 'C.ParentCategoryID = B.CourseID');
		$this->db->join('tbl_entity E', 'E.EntityID = C.CategoryID');
		$this->db->where("B.BatchID",$BatchID);
		$this->db->where("C.CategoryTypeID",3);
		$this->db->where("E.StatusID!=",6);
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		if($Query->num_rows()>0){

			return $Query->result();
		}
		return FALSE;

	}

	/*Create new set_timescheduling */

	function addEvent()
	{
		if(strtotime($_POST['StartTime']) == strtotime($_POST['EndTime'])){
			return array("exist","Start time and end time can not be same.");
		}else if(strtotime($_POST['StartTime']) > strtotime($_POST['EndTime'])){
			return array("exist","End time should be ahead of start time.");
		}else{
			$EntityGUID = get_guid();

			$color='#'.str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
	     
			$InstituteID=$this->Common_model->getInstituteByEntityGUID($_POST['InstituteGUID']);
	        $UserID=$this->Common_model->getUserIDByGUID($_POST['UserGUID']);
			
			
			$forDates = $_POST['forDates'];
			$forDatesArr = explode(",", $forDates);
			if(isset($forDatesArr) && !empty($forDatesArr))
			{
				foreach($forDatesArr as $date)
				{
					$date = date("Y-m-d", strtotime($date));
					$_POST['start'] = $_POST['end'] = $date;

					$EntityID=$this->Entity_model->addEntitys($EntityGUID, $Input = array('UserID'=>$UserID,'EntityTypeID'=>17,'InstituteID'=>$InstituteID,"StatusID"=>1));
					
					$sql = "INSERT INTO set_timescheduling (id,CourseID,FacultyID,BatchID,SubjectID,StartTime,EndTime,set_timescheduling.start,set_timescheduling.end,description, color) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		    		$this->db->query($sql, array($EntityID,$_POST['CourseID'],$_POST['FacultyID'],$_POST['BatchID'],$_POST['SubjectID'],$_POST['StartTime'],$_POST['EndTime'], $_POST['start'],$_POST['end'],$_POST['description'],$color));
				}
			}

			return ($this->db->insert_id())?true:false;

		}
	}

	/*Update  event */

	function updateEvent()
	{
		if(strtotime($_POST['StartTime']) == strtotime($_POST['EndTime'])){
			return array("exist","Start time and end time can not be same.");
		}else if(strtotime($_POST['StartTime']) > strtotime($_POST['EndTime'])){
			return array("exist","End time should be ahead of start time.");
		}else{
			$sql = "UPDATE set_timescheduling SET CourseID = ?, FacultyID = ?, BatchID = ?,SubjectID = ?,StartTime =?, EndTime =?, description = ?  WHERE id = ?";

			$this->db->query($sql, array($_POST['CourseID'], $_POST['FacultyID'],$_POST['BatchID'], $_POST['SubjectID'],$_POST['StartTime'], $_POST['EndTime'], $_POST['description'], $_POST['id']));
			return ($this->db->affected_rows()!=1)?false:true;
		}
	}


	/*Delete event */

	function deleteEvent()
	{
		$sql = "DELETE FROM set_timescheduling WHERE id = ?";
		$this->db->query($sql, array($_GET['id']));
		return ($this->db->affected_rows()!=1)?false:true;
	}

	/*Update  event */

	Public function dragUpdateEvent()
	{
		$sql = "UPDATE set_timescheduling SET  set_timescheduling.start = ? ,set_timescheduling.end = ?  WHERE id = ?";
		$this->db->query($sql, array($_POST['start'],$_POST['end'], $_POST['id']));
		return ($this->db->affected_rows()!=1)?false:true;
	}



	public function getTimeSchedulingEvents($start, $end)
	{
		//$InstituteID=$this->Common_model->getInstituteByEntityGUID($_GET['InstituteGUID']);
	 	//$UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
	  

		$FacultyID = $_GET['SessionKey'];
		$Date = $_GET['Date'];	
		if(!empty($FacultyID))
		{
			
			$sql = "SELECT es.*, CONCAT_WS(' ',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,SC.CategoryName as CourseName,GROUP_CONCAT(SJ.CategoryName) as SubjectName 
			FROM set_timescheduling es 
			join tbl_entity E on es.id=E.EntityID 
			join tbl_batch B on es.BatchID=B.BatchID 
			join tbl_users U on es.FacultyID=U.UserID AND U.UserGUID='$FacultyID' 
			join set_categories SC on SC.CategoryID=B.CourseID 
			join set_categories SJ on find_in_set(SJ.CategoryID,es.SubjectID) ";
			$sql .= "WHERE";

			if(!empty($Date)){
				//$sql .= " es.start like '%".$Date."%' AND";
				$sql .= "('".$Date."' BETWEEN es.start AND es.end) AND";
				$sql .= " U.UserGUID='$FacultyID' GROUP BY es.id ORDER BY es.start ASC";	
			}else{
				$sql .= " U.UserGUID='$FacultyID' AND (es.start BETWEEN '$start' AND '$end')
				GROUP BY es.id 
				ORDER BY es.start ASC";	
			}
			return $this->db->query($sql)->result();

		}
		

		return array();

	}
}