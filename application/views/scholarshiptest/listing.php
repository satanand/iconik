<style type="text/css">
.content {
/* width: 240px;*/
overflow: hidden;
word-wrap: break-word;
text-overflow: ellipsis;
line-height: 18px;
text-align: center;
}
.less {
max-height: 54px;
}
.menu-text {
/*display: block !important;*/
}
.modal { overflow: auto !important; }
.glyphicon {
position: relative;
top: -4px;
display: inline-block;
font-family: 'Glyphicons Halflings';
font-style: normal;
font-weight: normal;
line-height: 1;
-webkit-font-smoothing: antialiased;
font-size: 20px;
padding: 4px;
}
</style>
<header class="panel-heading">
<h1 class="h4">Manage Scholarship Test</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getScholarshipList(0);">

<div class="row">
<div class="col-md-12 float-right">
<div class="form-group mr-2 mb-2">
	<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormSchedule();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Schedule New Paper</button>
</div>
</div>
</div>

<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<h5>Total Papers: {{data.totalRecords}}</h5>
		</div>
	</div>
</div>

<form class="" id="filterForm" role="form" autocomplete="off">
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<input name="filterKeyword" id="filterKeyword" type="text" class="form-control" placeholder="Search by test name">
		</div>
	</div>		


	<div class="col-md-3">
		<div class="form-group">
			<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="" maxlength="50" placeholder="Schedule From Date" readonly>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="" maxlength="50" placeholder="Schedule To Date" readonly>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records('ppr');"><?php echo SEARCH_BTN;?></button>&nbsp;
		<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records('ppr');"><?php echo CLEAR_SEARCH_BTN;?></button>
		</div>
	</div>
</div>
</form>
<br/>

<div class="table-responsive block_pad_md"> 

<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

<table class="table table-striped table-hover">

<thead>
<tr>
	<th style="width: 150px;">Test Name</th>
	<th style="width: 100px;">Schedule Date</th>
	<th style="width: 100px;">Activation Time</th>
	<th style="width: 100px;">Start Time</th>
	<th style="width: 100px;">Duration</th>
	<th style="width: 100px;">Applicant Count</th>
	<th style="width: 100px; text-align: center;">Action</th>
</tr>
</thead>

<tbody ng-repeat="(key, row) in data.dataList">				
<tr scope="row">
	<td >{{row.QtPaperTitle}}</td>
	<td >{{row.ScheduleDate}}</td>
	<td >{{row.TestActivatedFrom}} - {{row.TestActivatedTo}}</td>
	<td >{{row.TestStartTime}}</td>
	<td >{{row.Duration}}</td>
	<td >{{row.ApplicantsCount}}</td>

	<td class="text-center" style="text-align: center;">
		<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormScholarshipEdit(row);" title="Edit"></a>
	</td>
</tr>
</tbody>
</table>


<p class="no-records text-center" ng-if="!data.dataList.length">
<span ng-if="data.dataList.length">No more records found.</span>
<span ng-if="!data.dataList.length">No records found.</span>
</p>
</div>





<!-- add Modal -->
<div class="modal fade" id="add_model">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
	<h3 class="modal-title h5">Schedule Scholarship Paper</h3>     	
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div ng-include="templateURLAdd"></div>
</div>
</div>
</div>


<div class="modal fade" id="edit_model">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
	<h3 class="modal-title h5">Schedule Scholarship Paper</h3>     	
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div ng-include="templateURLEdit"></div>
</div>
</div>
</div>


</div><!-- Body/ -->