<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4" id="top-heading">Manage Scholarships Applicants</h1>
</header>
<?php //$this->load->view('includes/breadcrumb'); ?>

<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" ng-init="getList(0);" id="content-body"><!-- Body -->


<nav aria-label="breadcrumb" >
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>  
		<li class="breadcrumb-item">Scholarships Applicants</li>
	</ol> 
</nav>


	<!-- Top container -->

<div class="clearfix mt-2 mb-2">
	<form class="" id="filterForm" role="form" autocomplete="off">		

		<div class="row">
			<div class="col-md-12">
				<span class="float-left records d-none d-sm-block">
					<span class="h5">Total records: {{data.dataList.length}}</span>					
				</span>
			</div>
		</div>	
			
			
		<div class="row mt-2">		

			<div class="col-md-4">
				<div class="form-group">
					<input name="filterKeyword" id="filterKeyword" type="text" class="form-control" placeholder="Search by word">
				</div>
			</div>		


			<!-- <div class="col-md-2">
				<div class="form-group">
					<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="" maxlength="50" placeholder="Apply From Date" readonly>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="" maxlength="50" placeholder="Apply To Date" readonly>
				</div>
			</div> -->

			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
			
		</div>
	</form>			
	
</div>




	<div class="table-responsive block_pad_md" infinite-scroll="getList(0);" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 
		
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
		
		<form class="" name="bulk_assign_form" id="bulk_assign_form" role="form" autocomplete="off">
		<table class="table table-striped table-condensed table-hover table-sortable">
			
			<thead>

				<tr>	

					<th style="width: 200px;">Institute</th>				

					<th style="width: 100px;">Title</th>

					<th style="width: 70px;">On Date</th>

					<th style="width: 200px;">Student</th>					

					<th style="width: 100px;">Apply Date</th>

					<!-- <th style="width: 100px;">Payment Type</th> -->		

					<th style="width: 50px;">Paid Amount</th>

					<th style="width: 100px;">Payment Date</th>	

					<th style="width: 50px;">Tarcking ID</th>

					<th style="width: 100px;">Status</th>		

				</tr>

			</thead>

			

			<tbody id="tabledivbody">

				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}_{{row.ApplyID}}">

					<td class="listed sm clearfix">
						<img class="rounded-circle float-left" ng-src="{{row.InstituteDetail.ProfilePic}}">
						<div class="content"><strong>{{row.InstituteDetail.FullName | capitalizeFirstLetter}}</strong>
							<div ng-if="row.Email"><a href="mailto:{{row.InstituteDetail.Email}}" target="_top">{{row.InstituteDetail.Email}}</a></div><div ng-if="!row.InstituteDetail.Email">-</div>
						</div>
					</td> 

					<td>{{row.QtPaperTitle}}</td>

					<td>{{row.StartDateTime}} / {{row.EndDateTime}}</td>					

					<td>{{row.FullName}}<br/>{{row.Email}} / {{row.PhoneNumberForChange}}</td>

					<td>{{row.ApplyDate}}</td>	

					<!-- <td>{{row.PaymentType}}</td>	 -->


					<td>{{row.PaidAmount}}</td>

					<td>{{row.PaymentDate}}</td>

					<td>{{row.TrackingID}}</td>

					<td>{{row.Status}}</td>				

				</tr>

			</tbody>

		</table>
		</form>
		

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	


	<!-- View Modal -->
	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Enquiry</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>


</div><!-- Body/ -->