<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Entity_model');
	}

/*get intt*/
	

	/*
	Description: 	Use to get Cetegories
	*/
	function getAttributes($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select($Field);
		$this->db->select('
			CASE E.StatusID
			when "2" then "Active"
			when "6" then "Inactive"
			END as Status', false);
		$this->db->from('tbl_entity E');	
		$this->db->from('set_attributes A');
		$this->db->where("E.EntityID","A.EntityID", FALSE);

		if(!empty($Where['EntityID'])){
			$this->db->where("E.EntityID",$Where['EntityID']);
		}


		$this->db->where("E.StatusID",2);

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$this->db->order_by('A.AttributeName','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to add new category
	*/
	function addCategory($ParentCategoryID='', $UserID, $CategoryTypeID, $CategoryName, $StatusID, $Duration){


		$this->db->trans_start();
		$EntityGUID = get_guid();

		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->UserID);

		$RelatedEntity=$this->Entity_model->getUserRelatedEntityList($this->UserID);

		$this->db->select('C.CategoryID');

	    $this->db->from('set_categories C');

	    $this->db->from('tbl_entity E');

		$this->db->where('C.CategoryID','E.EntityID',FALSE);

		$this->db->where('E.InstituteID',$InstituteID);

	    if(!empty($ParentCategoryID)){
	    	$this->db->where('C.ParentCategoryID', $ParentCategoryID );
	    }
	    
	    $this->db->where('LOWER(`CategoryName`)="'.strtolower($CategoryName).'"');


	    $query = $this->db->get();

	    

	    if ( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	    }else{

	    $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		    /* Add post to entity table and get EntityID. */
			$CategoryID = $this->Entity_model->addEntitys($EntityGUID, array("EntityTypeID"=>3, "UserID"=>$UserID,'InstituteID'=>$InstituteID,"StatusID"=>$StatusID));

			/* Add category */
			$InsertData = array_filter(array(
				"CategoryID" 		=>	$CategoryID,
				"CategoryGUID" 		=>	$EntityGUID,	
				"CategoryTypeID" 	=>	$CategoryTypeID,
				"ParentCategoryID" 	=>	$ParentCategoryID,
				"CategoryName" 		=>	$CategoryName,
				"Duration" 		=>	(int)$Duration
			));
			
			$this->db->insert('set_categories', $InsertData);
			//echo $this->db->last_query(); die;
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return array('CategoryID' => $CategoryID, 'CategoryGUID' => $EntityGUID);
		}
	}



	/*
	Description: 	Use to update category.
	*/
	function editCategory($CategoryID, $Input=array()){

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('C.CategoryID');

	    $this->db->from('set_categories C');

	    $this->db->from('tbl_entity E');
	    
		$this->db->where('C.CategoryID','E.EntityID',FALSE);

		$this->db->where('E.InstituteID',$InstituteID);

	    // if(!empty($ParentCategoryID)){
	    // 	$this->db->where('C.ParentCategoryID', $ParentCategoryID );
	    // }

	    $this->db->where('CategoryID != '.$CategoryID);
	    
	    $this->db->where('LOWER(`CategoryName`)="'.strtolower($Input['CategoryName']).'"');


	    $query = $this->db->get();

	    //echo $this->db->last_query();

	    if ( $query->num_rows() > 0 )
	    {
	        return 3;
	    }else{

			$UpdateArray = array_filter(array(
				"CategoryName" 			=>	@$Input['CategoryName'],
				"Duration" 		=>	@$Input['Duration']
			));

			if(!empty($UpdateArray)){
				/* Update User details to users table. */
				$this->db->where('CategoryID', $CategoryID);
				$this->db->limit(1);
				$this->db->update('set_categories', $UpdateArray);
			}

			$this->Entity_model->updateEntityInfo($CategoryID, array('StatusID'=>@$Input['StatusID']));
			return 1;
		}
	}

	/*
	Description: 	Use to get Cetegories
	*/
	function getCategoryTypes($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		/*get parent category type - ends*/

		$this->db->select('CT.*');
		$this->db->select($Field);
		$this->db->select('
			CASE CT.StatusID
			when "2" then "Active"
			when "6" then "Inactive"
			END as Status', false);
		$this->db->from('set_categories_type CT');

		if(!empty($Where['CategoryTypeGUID'])){
			$this->db->where("CT.CategoryTypeGUID",$Where['CategoryTypeGUID']);
		}


		if(!empty($CategoryTypeData)){
			$this->db->where("CT.CategoryTypeID",$CategoryTypeData['CategoryTypeID']);
		}

		$this->db->where("CT.StatusID",2);

		/* Total records count only if want to get multiple records */
	

		$this->db->order_by('CT.CategoryTypeName','ASC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to get Cetegories
	*/
	function getCategories($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		/*Additional fields to select*/

		//print_r($Where);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID); 

		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			// if(!empty($Where['Keyword'])){
			// 	$FieldArray = array(
			// 		'CategoryID'				=>	'C.CategoryID',			
			// 		'SubCategoryNames'			=>	'(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE ParentCategoryID=C.CategoryID and CategoryName like "%'.$Where['Keyword'].'%" ) AS SubCategoryNames',
			// 		'MenuOrder'					=>	'E.MenuOrder',
			// 		'Rating'					=>	'E.Rating',
			// 	);
			// }else{
				$FieldArray = array(
					'CategoryID'				=>	'C.CategoryID',			
					'SubCategoryNames'			=>	'(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE ParentCategoryID=C.CategoryID) AS SubCategoryNames',
					'MenuOrder'					=>	'E.MenuOrder',
					'Rating'					=>	'E.Rating',
				);
			//}
			
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		$RelatedEntity=array();
		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

		//$RelatedEntity=$this->Entity_model->getUserRelatedEntityList($this->UserID);
		
		$this->db->select('C.CategoryID, C.CategoryGUID, C.CategoryTypeID, C.CategoryID CategoryIDForUse, C.CategoryName, C.Duration, E.EntryDate, E.ModifiedDate');
		$this->db->select($Field);
		$this->db->select('
			CASE E.StatusID
			when "2" then "Active"
			when "6" then "Inactive"
			END as Status', false);

		$this->db->from('set_categories C');
		$this->db->from('set_categories_type CT');
		$this->db->from('tbl_entity E');

		$this->db->where('C.CategoryTypeID','CT.CategoryTypeID',FALSE);
		$this->db->where('C.CategoryID','E.EntityID',FALSE);
		$this->db->where("E.InstituteID",$InstituteID);

		
		//print_r($RelatedEntity);
		/*foreach ($RelatedEntity as $Entity) {

			$string[] = $Entity['EntityID'];
		}*/
		
		//print_r($string);
		 //$allpermi=implode(",",$string);
		//echo "<br>";
        //print_r($allpermi);
		// $allpermis=$allpermi; 
		
        //echo $RelatedEntity[0]['CreatedByUserID']; die;

		//$this->db->where('E.CreatedByUserID IN ('.$allpermi.')');

		/*if(!empty($RelatedEntity[0]['CreatedByUserID'])){

		$this->db->where_in('E.CreatedByUserID',$RelatedEntity[0]['CreatedByUserID']);

	    }else{

	    	$this->db->where('E.CreatedByUserID',$this->UserID);
	    }*/

		if(!empty($Where['StoreID'])){
			$this->db->where("C.StoreID",$Where['StoreID']);
		}

		if(!empty($Where['CategoryID'])){
			$this->db->where("C.CategoryID",$Where['CategoryID']);
		}      

		if(!empty($Where['CategoryGUID'])){
			$this->db->where("E.EntityGUID",$Where['CategoryGUID']);
		}           

		if(!empty($Where['CategoryTypeID'])){
			$this->db->where("C.CategoryTypeID",$Where['CategoryTypeID']);
		}           

		if(!empty($Where['ParentCategoryID'])){
			$this->db->where("C.ParentCategoryID",$Where['ParentCategoryID']);
		}

		if(!empty($Where['ShowOnlyParent'])){
			$this->db->where("C.ParentCategoryID is NULL", NULL, FALSE);
		}


		if(!empty($Where['Keyword'])){
			
			$this->db->group_start();
			$this->db->like("C.CategoryName", trim($Where['Keyword']));
			$this->db->or_like("C.Duration", trim($Where['Keyword']));
			$this->db->or_like('CT.CategoryTypeName',trim($Where['Keyword']));
			if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
    			$this->db->or_where("E.StatusID",6);
			}
			else if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
    			$this->db->or_where("E.StatusID",2);
			}
			$this->db->group_end();

		}

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get(); 
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$this->db->order_by('C.CategoryID','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query(); 
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				/*get attached media*/
				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Category',"EntityID" => $Record['CategoryIDForUse']), TRUE);
				$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

				$Record['SubCategories'] = new stdClass();

				if(!empty($Where['Keyword'])){
					$SubCategories = $this->getCategories('',array("ParentCategoryID"=>$Record['CategoryIDForUse'],"Keyword"=>$Where['Keyword']),true,1,25);
				}else{
					$SubCategories = $this->getCategories('',array("ParentCategoryID"=>$Record['CategoryIDForUse']),true,1,25);
				}

				$Record['EntryDate'] = date("d-m-Y H:i:s", strtotime($Record['EntryDate']));

				if(!empty($Record['ModifiedDate'])){
					$Record['ModifiedDate'] = date("d-m-Y H:i:s", strtotime($Record['ModifiedDate']));
				}
				

				if($SubCategories){
					$Record['SubCategories'] = $SubCategories['Data'];
				}

				unset($Record['CategoryIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function getContentCategories($multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$query = $this->db->query("select CategoryID,CategoryName from set_categories where CategoryTypeID = 2");

		$course = $query->result_array();

		if(count($course) > 0){

			foreach ($course as $key => $value) {
				
				//echo "SELECT es.EntityID, sc.CategoryName, sc.CategoryGUID FROM tbl_entity_categories as es JOIN set_categories as sc ON es.CategoryID = sc.CategoryID where sc.ParentCategoryID = ".$value['CategoryID'];
				$query = $this->db->query("SELECT es.EntityID, sc.CategoryName, sc.CategoryGUID FROM tbl_entity_categories as es JOIN set_categories as sc ON es.CategoryID = sc.CategoryID where sc.ParentCategoryID = ".$value['CategoryID']);

				$subject = $query->result_array(); 


				$course[$key]['SubCategories'] = $subject;

				foreach ($subject as $k => $v) {
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'File',"EntityID" => $v['EntityID']), TRUE);
					//$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());
					//$course[$key]['SubCategories'][$k]['media_file'] = $MediaData['Data'];
					if($MediaData['Data']){
						$course[$key]['SubCategories'][$k]['media_file'] = $MediaData['Data'];
					}else{
						$course[$key]['SubCategories'][$k]['media_file'] = array();
					}

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"EntityID" => $v['EntityID']), TRUE);
					if($MediaData['Data']){
						$course[$key]['SubCategories'][$k]['media_audio'] = $MediaData['Data'];
					}else{
						$course[$key]['SubCategories'][$k]['media_audio'] = array();
					}
					
				}
			}
			return $course;
		}
		return false;
	}
}