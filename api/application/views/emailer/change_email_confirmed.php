<h2 style="margin-bottom:20px; font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; ">Hi <?php echo $Name;?>,</h2>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Your change email address request has been completed successfully.</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">From now on please use this email address to login to your <?php  echo SITE_NAME; ?> account.</p>