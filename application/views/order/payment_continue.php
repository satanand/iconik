<div class="container" ng-controller="PageController"> 
  <header class="panel-heading">
  <h1 class="h4"><?php echo $heading; ?></h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<?php
$OrderGUID = $data['merchant_param2'];
$SysOrderID = $data['merchant_param5'];
?>


  <!-- <div id="logo" class="text-center"><img src="<?php echo API_URL;?>asset/img/emailer/output-onlinepngtools.png"></div>  --> 
  <!-- Form -->
  <div class="col-12 col-sm-11 col-md-8 col-lg-6 col-xl-5" style="margin: 50px auto 0 auto;" ng-init="getDiscountCoupon('<?php echo $OrderGUID;?>', '<?php echo $SysOrderID;?>');">
    <?php //print_r($data); ?>
    <form method="POST" name="customerData" action="ccavRequestHandler.php"  onsubmit="return validate_paymentform();">
            
           <!--  <table height="100" border='1' align="center">
               <tr>
                  <td>Parameter Name:</td>
                  <td>Parameter Value:</td>
               </tr>
               <tr>
                  <td colspan="2"> Compulsory information</td>
               </tr>
               <tr>
                  <td></td> 
                  <td>--><input type="hidden" name="tid" id="tid" readonly /><!--</td>
                </tr>
               <tr>
                  <td></td> 
                  <td>--><input type="hidden" name="merchant_id" value="<?php echo $data['merchant_id']; ?>"/><!--</td>
                </tr>
               <tr>
                  <td>Order Id  :</td>
                  <td> -->
                    <?php //if(!empty($data['order_id'])) { ?>
                      <div class="col-md-12">
                        <div class="form-group">
                         <label class="filter-col" for="CategoryTypeName">Order ID <span style="color:red"> *</span></label>
                         <input type="hidden" name="order_id" value="<?php echo $data['order_id']; ?>" /> 
                         <input type="text" id="order_id" class="form-control" value="<?php echo $data['order_id']; ?>" disabled="disabled" />           
                        </div>
                      </div>
                    <?php  //} ?>
                 
             <div class="col-md-12">
                <div class="form-group">
                   <label class="filter-col" for="CategoryTypeName">Amount (<i class="fa fa-rupee"></i>)<span style="color:red"> *</span></label>
                   <input type="hidden" name="amount" id="hdnamount" value="<?php echo $data['amount']; ?>"/>
                  <input type="text" id="amount"  class="form-control" value="<?php echo $data['amount']; ?>" disabled="disabled" />
                  <input type="hidden" id="realhdnamount" value="<?php echo $data['amount']; ?>"/>         
                </div>
             </div>


             <div class="col-md-12">
                <div class="form-group">
                  <label class="filter-col" for="DiscountCoupon">Discount Coupon</label>
                  <input type="text" name="DiscountCoupon" id="DiscountCoupon" class="form-control" value="" placeholder="Enter Discount Coupon Code" onblur="NetTotal();" />        
                </div>
             </div>


             <div class="col-md-12">
                <div class="form-group">
                   <label class="filter-col" for="CategoryTypeName">Net Amount (<i class="fa fa-rupee"></i>)<span style="color:red"> *</span></label>
                  
                  <input type="text" id="netamount" class="form-control" value="<?php echo $data['amount']; ?>" disabled="disabled" />         
                </div>
             </div>


             <div class="col-md-12">
                <div class="form-group">
                   <label class="filter-col" for="CategoryTypeName">GST 18% (<i class="fa fa-rupee"></i>)<span style="color:red"> *</span></label>
                  
                  <input type="text" id="gstamount" class="form-control" value="" readonly="" disabled="" />         
                </div>
             </div>


             <div class="col-md-12">
                <div class="form-group">
                   <label class="filter-col" for="CategoryTypeName">Final Amount (<i class="fa fa-rupee"></i>)<span style="color:red"> *</span></label>
                  
                  <input type="text" id="finalamount" class="form-control" value="<?php echo $data['amount']; ?>" disabled="disabled" />        
                </div>
             </div>


             <!-- </td>
               </tr>
               <tr>
                  <td>Amount  :</td>
                  <td> --><!-- </td>
               </tr>
               <tr>
                  <td></td>
                  <td> --><input type="hidden" name="currency" value="<?php echo $data['currency']; ?>"/><!-- </td>
               </tr>
               <tr>
                  <td></td>
                  <td> --><input type="hidden" name="redirect_url" value="<?php echo $data['redirect_url']; ?>"/><!-- </td>
               </tr>
               <tr>
                  <td></td>
                  <td> --><input type="hidden" name="cancel_url" value="<?php echo $data['cancel_url']; ?>"/><!-- </td>
               </tr>
               <tr>
                  <td></td>
                  <td> --><input type="hidden" name="language" value="<?php echo $data['language']; ?>"/><!-- </td>
               </tr>
               <tr>
                  <td></td>
                  <td> --><!-- </td>
               </tr>
            </table> -->
            <input type="hidden" name="merchant_param1" value="<?php echo $data['merchant_param1']; ?>"/>
            <input type="hidden" name="merchant_param2" value="<?php echo $data['merchant_param2']; ?>"/>
            <input type="hidden" name="merchant_param3" value="<?php echo $data['merchant_param3']; ?>"/>
            <input type="hidden" name="merchant_param4" value="<?php echo $data['merchant_param4']; ?>"/>
            <input type="hidden" name="SysOrderID" value="<?php echo $SysOrderID; ?>"/>

            <div class="modal-footer">
            <div class="form-group">
               <a href="<?php echo BASE_URL;?>order"><button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</button></a>&nbsp;&nbsp;
               <button type="submit" class="btn btn-success btn-sm">Confirm & Continue</button>
            </div>
    </form>

    <input type="hidden" name="IsDiscount" id="IsDiscount" value="{{IsDiscount}}" />
    <input type="hidden" name="DiscountOnAmount" id="DiscountOnAmount" value="{{DiscountOnAmount}}" />
  </div>
</div><!-- / container -->

<script type="text/javascript">
$(function()
{
    setTimeout(function()
    {
        NetTotal();
    }, "3000");
});


function NetTotal()
{
    var DiscountCoupon  = $("#DiscountCoupon").val();
    var IsDiscount      = $("#IsDiscount").val();
    var Amount = $("#realhdnamount").val();
    var RealAmount = Amount;

    var discountPercent = 0;

    if(DiscountCoupon == "")
    {
        discountPercent = 0;
    }
    else if(DiscountCoupon != "QIKME65")
    {
        alert("Invalid discount coupon."); 
        
        $("#netamount").val(RealAmount);
        //$("#hdnamount").val(RealAmount);
        //$("#finalamount").val(RealAmount);
        //return;
    }

    if(DiscountCoupon == "QIKME65")
    {
        discountPercent = 65;
    }

    //if(IsDiscount == 1)    
    if(true)
    {
        var amt = Amount;
        amt = parseFloat(amt);

        var amt1 = (amt * discountPercent) / 100;
        amt1 = parseFloat(amt1);

        var netamt = parseFloat(Amount) - amt1;
        netamt = parseFloat(netamt);
        netamt = Math.round(netamt);   

        $("#netamount").val(netamt);
        $("#hdnamount").val(netamt);

        var gstamt = (netamt * 0) / 100;
        gstamt = Math.round(gstamt); 
        $("#gstamount").val(gstamt);
        netamt = parseFloat(netamt) + parseFloat(gstamt);
        netamt = Math.round(netamt);
        $("#finalamount").val(netamt);
        $("#hdnamount").val(netamt);
    }
}


function validate_paymentform()
{ 
    var oid = $("#order_id").val();
    var amt = $("#amount").val();

    if(oid == "" || oid == null || oid == undefined)
    {
        alert("Order ID required. Please go back and check the order id.");

        return false;
    }


    if(amt == "" || amt == null || amt == undefined)
    {
        alert("Amount required. Please go back and check the amount.");

        return false;
    }

    return true;
}  
</script>