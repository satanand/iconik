<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4">Pricing</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" id="pricing-body" ng-init="getPricingList('QSP')"><!-- Body -->

	<div class="clearfix mt-2 mb-2">

	   <!-- <div class="row float-left ml-6" style="width: 65%;">

	      <div class="col-md-4" style="margin-left: 35px;">

	         <div class="form-group">

	            <select class="form-control" onchange="getCategoryBatch(this.value,'getStudentStatistics')" name="ParentCat" id="Courses">

	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>

	               <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

	            </select>

	         </div>

	      </div>

	      <div class="col-md-4">

	         <div class="form-group">

	            <div id="subcategory_section">

	               <select id="subject" name="CategoryGUIDs" onchange="filterStudentStatistics(this.value)"  class="form-control">

	                  <option value="">Select Batch</option>

	                  <option value="1">1</option>

	               </select>

	            </div>

	         </div>

	      </div>

	   </div> -->

	  <button ng-if="UserTypeID==1" class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd('QSP');">Add Pricing</button>
	  <!-- <a class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" href="<?php //echo SITE_HOST.'/pricing/rsp'; ?>">Retailer Pricing</a> -->

	</div>



	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">

	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">





	<!-- Data table -->

	<div> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<div class="row"> 
			<?php
			if(isset($_SESSION['UserData']['UserTypeID']) && $_SESSION['UserData']['UserTypeID'] == 1)
			{
			?>
			<div class="col-md-12">

				<!-- data table -->

				<h1 class="h4">Iconik Seller Pricing</h1>

				<table class="table table-striped table-hover"  id="tableToExport">

					<!-- table heading -->

					<thead>

						<tr>

							<th style="width: 100px; text-align: center;">Validity</th>

							<th style="width: 100px; text-align: center;">Price (<i class="fa fa-rupee"></i>)/Key</th>

							<th style="width: 100px; text-align: center;">Entry Date</th>

							<!-- <th style="width: 100px; text-align: center;">Modify Date</th> -->

							<!-- <th style="width: 100px; text-align: center;">Action</th> -->

						</tr>

					</thead>

					<!-- table body -->

					<tbody ng-repeat="(key, row) in data.dataList">

						<tr scope="row">

							<td style="text-align: center;">{{row.Validity}} Month</td>

							<td style="text-align: center;">{{row.Price}} </td>

							<td style="text-align: center;">{{row.EntryDate}}</td>

							<!-- <td style="text-align: center;">{{row.ModifiedDate}}</td> -->

							<!-- <td class="text-center" style="text-align: center;">

								<a class="glyphicon glyphicon-edit" href="<?php //echo base_url().'students/students_list/?parentCategoryGUID={{row.parentCategoryGUID}}&BatchGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Students list"></a>

							</td> -->

						</tr>

					</tbody>

				</table>

			</div>

			<?php
			}
			?>

			<div class="col-md-12">

				<!-- data table -->

				<h1 class="h4">Retailer Seller Pricing</h1>

				<!-- data table -->

				<table class="table table-striped table-hover" ng-if="data.rsp.length" id="tableToExport">

					<!-- table heading -->

					<thead>

						<tr>

							<th style="width: 100px; text-align: center;">Validity</th>

							<th style="width: 100px; text-align: center;">Price (<i class="fa fa-rupee"></i>)/Key</th>

							<th style="width: 100px; text-align: center;">Entry Date</th>

							<!-- <th style="width: 100px; text-align: center;">Modify Date</th> -->

							<!-- <th style="width: 100px; text-align: center;">Action</th> -->

						</tr>

					</thead>

					<!-- table body -->

					<tbody ng-repeat="(key, row) in data.rsp">

						<tr scope="row">

							<td style="text-align: center;">{{row.Validity}} Month</td>

							<td style="text-align: center;">{{row.Price}} </td>

							<td style="text-align: center;">{{row.EntryDate}}</td>

							<!-- <td style="text-align: center;">{{row.ModifiedDate}}</td> -->

							<!-- <td class="text-center" style="text-align: center;">

								<a class="glyphicon glyphicon-edit" href="<?php //echo base_url().'students/students_list/?parentCategoryGUID={{row.parentCategoryGUID}}&BatchGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Students list"></a>

							</td> -->

						</tr>

					</tbody>

				</table>

			</div>
			

		</div>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length > 0">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	

	<div class="modal fade" id="allot_key_model">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Allot Keys</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAllot"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_model">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Pricing</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_subject">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Subjcet</h3>     	

					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="subjectTemplateURLAdd"></div>

			</div>

		</div>

	</div>











	<!-- edit Modal -->

	<div class="modal fade" id="edit_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>





	<!-- edit Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>

</div><!-- Body/ -->