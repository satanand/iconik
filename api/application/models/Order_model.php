<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Order_model extends CI_Model

{

    public function __construct()
    {
        parent::__construct();  
    }



    function getIDByGUID($OrderGUID){
        $this->db->select('OrderID');
        $this->db->from('keys_orders');
        $this->db->where("OrderGUID",$OrderGUID);
        $Query = $this->db->get();  
        //echo $this->db->last_query(); 
        if($Query->num_rows()>0){
            $data = $Query->row();
            return $data->OrderID;
            //$instituteID->InstituteID;
        }else{
            return FALSE;
        }
    }



    /*

    Description:    Use to add new order

    */

    function addOrder($Input=array(), $UserID, $StatusID=1){
       // print_r($Input);

        $InstituteID = $this->Common_model->getInstituteByEntity($UserID);        

        $this->db->trans_start();

        if(!empty($Input['OrderGUID'])){
            $OrderGUID = $Input['OrderGUID'];
            $OrderID = $this->getIDByGUID($Input['OrderGUID']);
            if(!$OrderID){
                return FALSE;
            }else{
                $this->db->where('OrderID',$OrderID);
                $this->db->update('keys_orders',array(

                    "UserID"        =>  $UserID,

                    "InstituteID"  => $InstituteID,

                    "EntryDate"     =>  date("Y-m-d h:i:s"),

                    "PaymentStatusID"      =>  $StatusID,

                    "OrderPrice"  => $Input['TotalPrice']

                ));

                $this->db->where('OrderID', $OrderID);
                $this->db->delete('keys_order_details');
            }           
        }else{
            $OrderGUID = get_guid();

             /* Add order to orders table */

            $OrderData = array_filter(array(

                "UserID"        =>  $UserID,

                "InstituteID"  => $InstituteID,

                "OrderGUID"     =>  $OrderGUID,

                "EntryDate"     =>  date("Y-m-d h:i:s"),

                "PaymentStatusID"      =>  $StatusID,

                "OrderPrice"  => $Input['TotalPrice'],

                "OrderFor"  => "Keys"

            ));

            $this->db->insert('keys_orders', $OrderData);

            $OrderID = $this->db->insert_id();
        }

       
        $pst = date('m');

        if($pst>3) {

            $y=date('Y');

            $newstring = substr($y, -2);

            $pt = date('Y', strtotime('+1 year'));

            $newstring .= substr($pt, -2);

        }

        else {

            $y=date('Y', strtotime('-1 year'));

            $newstring = substr($y, -2);

            $pt =date('Y');

            $newstring .= substr($pt, -2);

        }


        if(strlen($OrderID) == 1){
            $CustomOrderID = $newstring."00".$OrderID;
        }else if(strlen($OrderID) == 2){
            $CustomOrderID = $newstring."0".$OrderID; 
        }else if(strlen($OrderID) > 2){
            $CustomOrderID = $newstring."".$OrderID; 
        }



        $this->db->where("OrderID",$OrderID);
        $this->db->update("keys_orders",array("CustomOrderID"=>$CustomOrderID));



        if(!empty($Input['Validity'])){
            $Input['Validity'] = explode(",", $Input['Validity']);
            $Input['Price'] = explode(",", $Input['Price']);
            $Input['OrderKeys'] = explode(",", $Input['OrderKeys']);
            foreach ($Input['Validity'] as $key => $value) {

                $OrderDetailData[] = array(

                    'OrderID'=>$OrderID,

                    'Validity'=>$Input['Validity'][$key],

                    'Keys'=>$Input['OrderKeys'][$key],

                    "Price"=>$Input['Price'][$key]

                );

            }

            $this->db->insert_batch('keys_order_details', $OrderDetailData); 


        } /*Add product ends*/



        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

        {

            return FALSE;

        }

        return array("OrderGUID"=>$OrderGUID, "OrderID"=>$OrderID);

    }


     /*

    Description:    Use to add sms order

    */

    function addSMSCreditOrder($Input=array(), $UserID, $StatusID=1){
       // print_r($Input);

        $InstituteID = $this->Common_model->getInstituteByEntity($UserID);        

        $this->db->trans_start();

        $OrderGUID = get_guid();

         /* Add order to orders table */

        $OrderData = array_filter(array(

            "UserID"        =>  $UserID,

            "InstituteID"  => $InstituteID,

            "OrderGUID"     =>  $OrderGUID,

            "EntryDate"     =>  date("Y-m-d h:i:s"),

            "PaymentStatusID"  =>  $StatusID,

            "OrderPrice"  => $Input['ChequeAmount'],

            "OrderFor"  => $Input['OrderFor']

        ));

        $this->db->insert('keys_orders', $OrderData);

        $OrderID = $this->db->insert_id();
       
        $pst = date('m');

        if($pst>3) {

            $y=date('Y');

            $newstring = substr($y, -2);

            $pt = date('Y', strtotime('+1 year'));

            $newstring .= substr($pt, -2);

        }

        else {

            $y=date('Y', strtotime('-1 year'));

            $newstring = substr($y, -2);

            $pt =date('Y');

            $newstring .= substr($pt, -2);

        }


        if(strlen($OrderID) == 1){
            $CustomOrderID = $newstring."00".$OrderID;
        }else if(strlen($OrderID) == 2){
            $CustomOrderID = $newstring."0".$OrderID; 
        }else if(strlen($OrderID) > 2){
            $CustomOrderID = $newstring."".$OrderID; 
        }

        

        $this->db->where("OrderID",$OrderID);
        $this->db->update("keys_orders",array("CustomOrderID"=>$CustomOrderID));


        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

        {

            return FALSE;

        }

        return array("OrderGUID"=>$OrderGUID, "OrderID"=>$OrderID, "CustomOrderID" => $CustomOrderID);

    }


    function makePayment($EntityID, $Input, $StatusID){
        //print_r($Input); die;
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $PaymentGUID = get_guid();
        $OrderID = $this->getIDByGUID($Input['OrderGUID']);
        $this->db->trans_start();

        if($Input['PaymentType'] == 'Offline'){   

            if(!empty($Input['MediaGUID'])){
                $this->db->select('MediaID');
                $this->db->from('tbl_media');
                $this->db->where('MediaGUID',$Input['MediaGUID']);
                $this->db->limit(1);
                $que = $this->db->get();
                //echo $this->db->last_query(); die;
                if($que->num_rows() > 0){
                    $mediaID = $que->result_array();
                    $MediaID = $mediaID[0]['MediaID'];

                    $arr = array("InstituteID"=>$InstituteID);

                    $this->db->where("MediaID",$MediaID);
                    $this->db->update("tbl_media",$arr);
                }else{
                    $MediaID = "";
                } 
            }else{
                $MediaID = "";
            }     

            $OrderData = array_filter(array(

                "UserID"        =>  $InstituteID,

                "PaymentGUID"   =>  $PaymentGUID,

                "OrderID"     =>  $OrderID,

                "PaymentType"  =>  $Input['PaymentType'],

                "ChequeNumber"        =>  $Input['ChequeNumber'],

                "DrawnOnBank"     =>  $Input['DrawnBank'],

                "ChequeDate"  =>  date("Y-m-d", strtotime($Input['ChequeDate'])),

                "Amount"        =>  $Input['ChequeAmount'],

                "DepositedOn"  =>  date("Y-m-d", strtotime($Input['DepositedDate'])),

                "StatusID"        =>  $StatusID,

                "MediaID"  => $MediaID,

                "PaymentFor" => $Input['OrderFor']

            ));


        }else if($Input['PaymentType'] == 'Online'){

            $OrderData = array_filter(array(

                "UserID"        =>  $InstituteID,

                "PaymentGUID"   =>  $PaymentGUID,

                "OrderID"     => $OrderID,

                "PaymentType"  =>  $Input['PaymentType'],

                "TransactionNumber"  =>  $Input['TransactionNumber'],

                "DrawnOnBank"     =>  $Input['DrawnOnBank'],

                "StatusID"        =>  $StatusID,

                "Amount"        =>  $Input['Amount'],

                "PaymentFor" => $Input['OrderFor'],

                "DepositedOn"  =>  date("Y-m-d", strtotime($Input['DepositedOn']))

            ));
            
        }

        

        if(!empty($OrderData)){
            
           $this->db->insert('keys_payment',$OrderData);

            if($StatusID == 2 && $Input['PaymentType'] == 'Online'){
                $this->db->where('OrderID',$OrderID);
                $this->db->update('keys_orders',array("PaymentStatusID"=>2));
            }

            if(!empty($MediaID)){
                $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'OfflinePayment',"MediaID" => $MediaID),TRUE);
                $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                $MediaURL = $MediaID['Records'][0]['MediaURL'];
            }else{
                $MediaURL = "";
            }

           $Institutedata =  $this->Common_model->getInstituteDetailByID($InstituteID,'FirstName,LastName');
           $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];

           $EmailText = "<p>New order is placed by ".$InstituteName."</p>";

           $EmailText .= "<p>Order details are mentioned below.</p>";

           $EmailText .= "<p>Payment Type: ".$Input['PaymentType']."</p>";

           if($Input['PaymentType'] == 'Offline'){

                $EmailText .= "<p>CHEQUE NUMBER: ".$Input['ChequeNumber']."</p>";

                $EmailText .= "<p>CHEQUE DRAWN ON (BANK NAME) : ".$Input['DrawnBank']."</p>";

                $EmailText .= "<p>CHEQUE DATE : ".$Input['ChequeDate']."</p>";
           }
           

           $EmailText .= "<p>AMOUNT : ".$Input['ChequeAmount']."</p>";

           $EmailText .= "<p>DEPOSITED ON : ".$Input['DepositedDate']."</p>";

           // echo $EmailText; die;  print_r($OrderData); die;

           //echo $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);

           $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText, "Signature"=> "Thanks,<br>".$InstituteName ),TRUE);

            sendMail(array(
                'emailTo'       => SITE_ACCOUNT_EMAIL,         
                'emailSubject'  => "New Order Placed",
                'Attachment'  => $MediaURL,
                'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE)) 
            ));

            //die;
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)

        {

            return FALSE;

        }

        return TRUE;
    }





    function getKeysPrice($EntityID){

        $MasterFranchisee = $this->Users_model->getMasterFranchisee($EntityID);

        $this->db->select("Validity, Price");
        $this->db->from("set_keys_price");
        if($MasterFranchisee == "yes")
        {
            $this->db->where("QSP",1);
        }
        else
        {
            $this->db->where("RSP",1);
        }
        $this->db->order_by("PriceID", "ASC");
        
        $Query = $this->db->get();


        if($Query->num_rows()>0)
        {
            $data = $arr = array();
            if($Query->num_rows()>0)
            {
                foreach ($Query->result_array() as $record) 
                {
                    $arr[$record['Validity']] = $record['Price'];
                }

                foreach($arr as $k => $v)
                {
                    $data[] = array("Validity" => $k, "Price" => $v);
                }
            }

        }else{

            $data = array();

        }



        return $data;

    }





    function random_strings($length_of_string) { 

      

        // String of all alphanumeric character 

        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

      

        // Shufle the $str_result and returns substring 

        // of specified length 

        return substr(str_shuffle($str_result), 0, $length_of_string); 

    } 



    function getSMSOrders($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15, $menu_type="manage_order"){
        $UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);
        $MasterFranchisee = $this->Common_model->getMasterFranchiseeByEntityID($EntityID);
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);  
        $this->db->select("KP.OrderID,KP.OrderGUID,Kp.CustomOrderID,KP.OrderPrice,KP.PaymentStatusID,KP.OrderStatusID,KP.EntryDate,KP.TransactionID,KP.UserID,KP.EntryDate,U.FirstName,U.LastName,U.Email");
        $this->db->from("keys_orders as KP");
        $this->db->join("tbl_entity as E","E.EntityID = KP.UserID");
        $this->db->join("tbl_users as U","U.UserID = KP.UserID");

        // if($UserTypeID != 1 && $MasterFranchisee != 'yes'){
        //     $this->db->where("E.InstituteID",$InstituteID);
        // }

        $this->db->where("KP.OrderFor","SMS Credits");

        if($menu_type == 'buy_now'){
            $this->db->where("E.InstituteID",$InstituteID);
        }     

        if($menu_type == 'manage_order' && $MasterFranchisee == 'yes'){
           // $this->db->where("U.ParentUserID",$EntityID);
            $this->db->where("E.CreatedByUserID",$InstituteID);
           // $this->db->where("U.ParentUserID != U.UserID");
        }      

        if($Where['OrderGUID']){
            $this->db->where("KP.OrderGUID",$Where['OrderGUID']);
        }


        if($Where['UserID']){
            $this->db->where("KP.UserID",$Where['UserID']);
        }

        $this->db->order_by("OrderID",'DESC');

        if($multiRecords){ 
            $TempOBJ = clone $this->db;
            $TempQ = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
        }

        $Query = $this->db->get(); 
        //echo $this->db->last_query();
        $Records = array();
        if($Query->num_rows() > 0){
            foreach ($Query->result_array() as $record) {
                if(!empty($record['OrderID'])){
                    $this->db->select("*");
                    $this->db->from("set_smscredit_price");
                    $this->db->where("TotalIncludeTax",$record['OrderPrice']);
                    $this->db->limit(1);

                    $Query = $this->db->get(); 
                    $record['Order_details'] = $Query->result_array();

                    $this->db->select("*");
                    $this->db->from("keys_payment");
                    $this->db->where("OrderID",$record['OrderID']);
                    $Query = $this->db->get(); 

                    if($Query->num_rows() > 0){
                        $record['Payment_details'] = $Query->result_array();

                        $PaymentType = array_column($record['Payment_details'], 'PaymentType');

                        //print_r($PaymentType); die;
                        $record['PaymentType'] = array_unique($PaymentType);


                        if(in_array('Offline', $record['PaymentType'])){
                             $record['OrderStatus'] = 'Offline'; 
                        }else{
                             $record['OrderStatus'] = 'Online'; 
                        }

                        if(in_array('Offline', $record['PaymentType']) && in_array('Online', $record['PaymentType'])){
                             $record['PaymentMethod'] = 'By cheque/Online via CCAvenue'; 
                        }else if(in_array('Offline', $record['PaymentType']) && !in_array('Online', $record['PaymentType'])){
                             $record['PaymentMethod'] = 'By cheque'; 
                        }else if(!in_array('Offline', $record['PaymentType']) && in_array('Online', $record['PaymentType'])){
                             $record['PaymentMethod'] = 'Online via CCAvenue'; 
                        }

                        $record['PaymentType'] = implode(', ', $record['PaymentType']);

                        $Amount = array_column($record['Payment_details'], 'Amount');

                        $record['TotalPaidAmount'] = array_sum($Amount);

                        if($record['TotalPaidAmount'] == $record['OrderPrice']){
                           $record['PaymentStatus'] = 'Paid';
                           $record['RemainingAmount'] = 0;
                        }else{
                            $record['PaymentStatus'] = 'Unpaid';
                            $record['RemainingAmount'] = $record['OrderPrice'] - $record['TotalPaidAmount'];
                        }



                        foreach ($record['Payment_details'] as $kp => $vp) {
                            $record['Payment_details'][$kp]['DepositedOn'] = date("d-m-Y", strtotime($record['DepositedOn']));

                            $record['Payment_details'][$kp]['ChequeDate'] = date("d-m-Y", strtotime($record['ChequeDate']));

                            if(!empty($vp['MediaID']) && $vp['MediaID'] != NULL){
                                $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'OfflinePayment',"MediaID" => $vp['MediaID']),TRUE);
                                $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                                $record['Payment_details'][$kp]['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
                                $record['Payment_details'][$kp]['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
                            }else{
                                $record['Payment_details'][$kp]['MediaThumbURL'] = "";
                                $record['Payment_details'][$kp]['MediaURL'] = "";
                            }
                        }

                        
                    }else{
                        $record['OrderStatus'] = ''; 

                        $record['PaymentType'] = '';

                        $record['TotalPaidAmount'] = 0;

                        $record['PaymentStatus'] = 'Unpaid';

                        $record['RemainingAmount'] = $record['OrderPrice'];

                        $record['PaymentMethod'] = '-'; 
                    }

                    $pst = date('m');

                    if($pst>3) {

                        $y=date('Y');

                        $newstring = substr($y, -2);

                        $pt = date('Y', strtotime('+1 year'));

                        $newstring .= substr($pt, -2);

                    }

                    else {

                        $y=date('Y', strtotime('-1 year'));

                        $newstring = substr($y, -2);

                        $pt =date('Y');

                        $newstring .= substr($pt, -2);

                    }

                    $record['EntryDate'] = date("d-m-Y", strtotime($record['EntryDate']));


                    $record['OrderIDPrefix'] = $newstring;
                    //print_r($Amount); die;
                }  

                array_push($Records, $record); 
            }
        }

        if($Records){
            return $Records;
        }else{
            return FALSE;
        }
    }





    function getOrders($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15, $menu_type="manage_order"){
        // print_r($Where);

        // die;
        $UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);
        $MasterFranchisee = $this->Common_model->getMasterFranchiseeByEntityID($EntityID);
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);  
        $this->db->select("KP.OrderID,KP.OrderGUID,KP.CustomOrderID,KP.OrderPrice,KP.PaymentStatusID,KP.OrderStatusID,KP.EntryDate,KP.TransactionID,KP.UserID,KP.EntryDate,KP.OrderFor,U.FirstName,U.LastName,U.Email");
        $this->db->from("keys_orders as KP");
        $this->db->join("tbl_entity as E","E.EntityID = KP.UserID");
        $this->db->join("tbl_users as U","U.UserID = KP.UserID");

       

        if($menu_type == 'buy_now'){
            $this->db->where("E.InstituteID",$InstituteID);
        }     

        if($menu_type == 'manage_order' && $MasterFranchisee == 'yes'){
           // $this->db->where("U.ParentUserID",$EntityID);
            $this->db->where("E.CreatedByUserID",$InstituteID);
            //$this->db->where("KP.OrderFor","Keys");
           // $this->db->where("U.ParentUserID != U.UserID");
        }

        if(!empty($Where['OrderFor'])){
            $this->db->where("KP.OrderFor",$Where['OrderFor']);
        } 


        if(!empty($Where['OrderStatusID'])){
            $this->db->where("KP.OrderStatusID",$Where['OrderStatusID']);
        } 
            

        if($Where['OrderGUID']){
            $this->db->where("KP.OrderGUID",$Where['OrderGUID']);
        }


        if($Where['UserID']){
            $this->db->where("KP.UserID",$Where['UserID']);
        }


        if(!empty($Where['Keyword'])){
            $this->db->group_start();
            $this->db->like("KP.OrderPrice", trim($Where['Keyword']));
            $this->db->or_like("KP.EntryDate", trim($Where['Keyword']));
            $this->db->or_like("KP.CustomOrderID", trim($Where['Keyword']));
            $this->db->or_like("U.FirstName", trim($Where['Keyword']));
            $this->db->or_like("U.LastName", trim($Where['Keyword']));
            $this->db->or_like("CONCAT_WS('',U.FirstName,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
            $this->db->or_like("KP.EntryDate", trim($Where['Keyword']));
            $this->db->group_end();   
        }

        $this->db->order_by("KP.OrderID",'DESC');

        if($multiRecords){ 
            $TempOBJ = clone $this->db;
            $TempQ = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
        }

        $Query = $this->db->get(); 
        //echo $this->db->last_query(); die;
        $Records = array();
        if($Query->num_rows() > 0){
            foreach ($Query->result_array() as $record) {
                //print_r($record);
                if(!empty($record['OrderID'])){

                    if($record['OrderFor'] == "Keys"){
                        $this->db->select("*");
                        $this->db->from("keys_order_details");
                        $this->db->where("OrderID",$record['OrderID']);
                        $this->db->limit(4);
                        $Query = $this->db->get();
                        if($Query->num_rows() > 0){
                            $record['Order_details'] = $Query->result_array();

                        }    
                    }else{
                        $this->db->select("*");
                        $this->db->from("set_smscredit_price");
                        $this->db->where("TotalIncludeTax",$record['OrderPrice']);
                        $this->db->limit(1);
                        $Query = $this->db->get(); 
                        if($Query->num_rows() > 0){
                            $record['Order_details'] = $Query->result_array();
                        }
                    }

                    $this->db->select("*");
                    $this->db->from("keys_payment");
                    $this->db->where("OrderID",$record['OrderID']);
                    $Query = $this->db->get(); 

                    //echo $this->db->last_query();

                    if($Query->num_rows() > 0){


                        $record['Payment_details'] = $Query->result_array();                       
                        //print_r($record['Payment_details']);
                        $StatusIDs = array_column($record['Payment_details'], 'StatusID');

                        if(in_array(1, $StatusIDs)){
                            $record['RemainingVerification'] = 'Yes';
                        }else{
                            $record['RemainingVerification'] = 'No'; 
                        }

                        $PaymentType = array_column($record['Payment_details'], 'PaymentType');

                        //print_r($PaymentType); die;
                        $record['PaymentType'] = array_unique($PaymentType);


                        if(in_array('Offline', $record['PaymentType'])){
                             $record['OrderStatus'] = 'Offline'; 
                        }else{
                             $record['OrderStatus'] = 'Online'; 
                        }

                       //print_r($Where['PaymentMethod']); die;

                        if(!empty($Where['PaymentMethod']) && $Where['PaymentMethod'] == "Cheque/Online"){
                            if(in_array('Offline', $record['PaymentType']) && in_array('Online', $record['PaymentType'])){                 
                              $record['PaymentMethod'] = 'By cheque/Online via CCAvenue'; 
                            }else{
                                unset($record); 
                                continue;
                            }
                        }else if(!empty($Where['PaymentMethod']) && $Where['PaymentMethod'] == "Cheque"){
                            if(in_array('Offline', $record['PaymentType']) && !in_array('Online', $record['PaymentType'])){
                                $record['PaymentMethod'] = 'By cheque'; 
                            }else{
                               unset($record); 
                               continue;
                            }
                        }else if(!empty($Where['PaymentMethod']) && $Where['PaymentMethod'] == "Online"){
                            if(!in_array('Offline', $record['PaymentType']) && in_array('Online', $record['PaymentType'])){
                                $record['PaymentMethod'] = 'Online via CCAvenue'; 
                            }else{
                               unset($record); 
                               continue;
                            }
                        }else{
                            if(in_array('Offline', $record['PaymentType']) && in_array('Online', $record['PaymentType'])){
                                $record['PaymentMethod'] = 'By cheque/Online via CCAvenue'; 
                            }else if(in_array('Offline', $record['PaymentType']) && !in_array('Online', $record['PaymentType'])){
                                $record['PaymentMethod'] = 'By cheque'; 
                            }else if(!in_array('Offline', $record['PaymentType']) && in_array('Online', $record['PaymentType'])){
                                $record['PaymentMethod'] = 'Online via CCAvenue'; 
                            }
                        }



                        $record['PaymentType'] = implode(', ', $record['PaymentType']);

                        $Amount = array_column($record['Payment_details'], 'Amount');

                        $record['TotalPaidAmount'] = array_sum($Amount);

                        if(!empty($Where['PaymentStatus']) && $Where['PaymentStatus'] == "Paid"){
                            if($record['TotalPaidAmount'] == $record['OrderPrice']){
                               $record['PaymentStatus'] = 'Paid';
                               $record['RemainingAmount'] = 0;
                            }else{
                                unset($record); 
                                continue;
                            }
                        }else if(!empty($Where['PaymentStatus']) && $Where['PaymentStatus'] == "Unpaid"){
                            if($record['TotalPaidAmount'] == $record['OrderPrice']){
                                unset($record); 
                                continue;
                            }else{
                                $record['PaymentStatus'] = 'Unpaid';
                $record['RemainingAmount'] = $record['TotalPaidAmount'] - $record['OrderPrice'];                          
            }
                        }else if(!empty($Where['PaymentStatus']) && $Where['PaymentStatus'] == "Partly Payment"){
                            if($record['TotalPaidAmount'] == $record['OrderPrice']){                            
                                unset($record); 
                                continue;
                            }else{
                                if(count($record['Payment_details']) > 1){
                                   $record['PaymentStatus'] = 'Partly Payment';
                                   $record['RemainingAmount'] = $record['TotalPaidAmount'] - $record['OrderPrice']; 
                                }else{
                                    unset($record); 
                                    continue;
                                }
                            }
                        }else{
                            if($record['TotalPaidAmount'] == $record['OrderPrice'] && $record['OrderStatusID'] == 2){                            
                               $record['PaymentStatus'] = 'Paid';
                               $record['RemainingAmount'] = 0;  
                            }else if(count($record['Payment_details']) > 1){
                                $record['PaymentStatus'] = 'Partly Payment';
                    $record['RemainingAmount'] = $record['TotalPaidAmount'] - $record['OrderPrice'];
                            }else{
                                $record['PaymentStatus'] = 'Unpaid';
                                $record['RemainingAmount'] = $record['TotalPaidAmount'] - $record['OrderPrice'];
                                //echo "fjflkjdsf"; die;
                            }
                        } 



                        foreach ($record['Payment_details'] as $kp => $vp) {
                            if(!empty($vp['DepositedOn'])){
                                $record['Payment_details'][$kp]['DepositedOn'] = date("d-m-Y", strtotime($vp['DepositedOn']));
                            }else{
                                $record['Payment_details'][$kp]['DepositedOn'] = "";
                            }
                            

                            $record['Payment_details'][$kp]['ChequeDate'] = date("d-m-Y", strtotime($vp['ChequeDate']));

                            //$record['Payment_details'][$kp]['DepositedOn'] = date("d-m-Y", strtotime($record['DepositedOn']));

                            if(!empty($vp['MediaID']) && $vp['MediaID'] != NULL){
                                $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'OfflinePayment',"MediaID" => $vp['MediaID']),TRUE);
                                $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                                $record['Payment_details'][$kp]['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
                                $record['Payment_details'][$kp]['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
                            }else{
                                $record['Payment_details'][$kp]['MediaThumbURL'] = "";
                                $record['Payment_details'][$kp]['MediaURL'] = "";
                            }
                        }

                        
                    }else{
                        if(!empty($Where['PaymentStatus']) && ($Where['PaymentStatus'] == "Paid" || $Where['PaymentStatus'] == "Partly Payment")){
                            unset($record); 
                            continue;
                        }

                        if(!empty($Where['PaymentMethod'])){
                            unset($record); 
                            continue;
                        }

                        $record['OrderStatus'] = ''; 

                        $record['PaymentType'] = '';

                        $record['TotalPaidAmount'] = 0;

                        $record['PaymentStatus'] = 'Unpaid';

                        $record['RemainingAmount'] = $record['OrderPrice'];

                        $record['PaymentMethod'] = '-'; 
                    }

                    $pst = date('m');

                    if($pst>3) {

                        $y=date('Y');

                        $newstring = substr($y, -2);

                        $pt = date('Y', strtotime('+1 year'));

                        $newstring .= substr($pt, -2);

                    }

                    else {

                        $y=date('Y', strtotime('-1 year'));

                        $newstring = substr($y, -2);

                        $pt =date('Y');

                        $newstring .= substr($pt, -2);

                    }

                    $record['EntryDate'] = date("d-m-Y", strtotime($record['EntryDate']));


                    $record['OrderIDPrefix'] = $newstring;
                    //print_r($record);
                    //print_r($Amount); die;
                }  

                array_push($Records, $record); 
            }
        }

        if($Records){
            return $Records;
        }else{
            return FALSE;
        }
    }


    function allotOrderedKeys($EntityID,$Input){
        $this->db->trans_start();

        $this->db->select("OrderID,InstituteID,CustomOrderID");
        $this->db->from("keys_orders");
        $this->db->where("OrderGUID", $Input['OrderGUID']);
        $this->db->limit(1);
        $Query = $this->db->get();
        //echo $Query->num_rows();
        if($Query->num_rows() > 0){
            $arr = $Query->result_array();
            $UserID = $arr[0]['InstituteID'];
            $OrderID = $arr[0]['OrderID'];
            $CustomOrderID = $arr[0]['CustomOrderID'];
            $Institutedata =  $this->Common_model->getInstituteDetailByID($UserID,'FirstName,LastName,Email');

            $this->db->select("*");
            $this->db->from("keys_order_details");
            $this->db->where("OrderID", $OrderID);
            $this->db->limit(4);
            $query = $this->db->get();

            //echo $this->db->last_query(); die;

            if($query->num_rows() > 0){
                $data = $query->result_array();
                foreach ($data as $key => $value) {
                    //echo "select KeyID from set_keys where Validity = ".$value['Validity']." and AssignedInstitute IS NULL ORDER BY RAND() limit ".$value['Keys']; die;
                    $query = $this->db->query("select KeyID from set_keys where Validity = ".$value['Validity']." and AssignedInstitute IS NULL ORDER BY RAND() limit ".$value['Keys']);
                    $keysID = $query->result_array();


                    //print_r($keysID); die;
                   // if(count($keysID) > 0){
                        //echo count($keysID); echo $value['Keys']; die;
                        if(count($keysID) < $value['Keys']){
                            $AvailableKeys = count($keysID);
                            $generateKeys = $value['Keys'] - $AvailableKeys;
                            $this->Keys_model->addKeys($EntityID,array("KeyCount"=>$value['Keys'],"Validity"=>$value['Validity']));
                            $query = $this->db->query("select KeyID from set_keys where Validity = ".$value['Validity']." and AssignedInstitute IS NULL ORDER BY RAND() limit ".$value['Keys']);
                            $keysID = $query->result_array();
                        }

                        //print_r($keysID); die;

                        foreach ($keysID as $key => $value) {
                            $updateArr = array("AssignedInstitute"=>$UserID,"AllotedDate"=>date("Y-m-d"));
                            $this->db->where("KeyID",$value['KeyID']);
                            $this->db->update("set_keys",$updateArr);

                           // echo $this->db->last_query(); die;
                        }
                    //}
                }

                $updateArr = array("OrderStatusID"=>2);
                $this->db->where("OrderID", $OrderID);
                $this->db->update("keys_orders",$updateArr);

                $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];
            
                $EmailText = "<p>Hello,".$InstituteName."</p>";

                //$EmailText .= "<p>We have alloted ordered keys successfully for order ".$CustomOrderID."</p>";

                $EmailText .= "<p>Your Order for keys/sms credits is successfully Verified.</p>";
                $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);
                sendMail(array(
                    'emailTo'       => $Institutedata['Email'],         
                    'emailSubject'  => "Order Verified",
                    'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
                ));     
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }
        return TRUE;
    }


    function unallotOrderedKeys($EntityID,$Input){
        $this->db->trans_start();
        $UsedKeys = array();
        $this->db->select("OrderID,InstituteID,CustomOrderID");
        $this->db->from("keys_orders");
        $this->db->where("OrderGUID", $Input['OrderGUID']);
        $this->db->limit(1);
        $Query = $this->db->get();
        //echo $Query->num_rows();
        if($Query->num_rows() > 0){
            $arr = $Query->result_array();
            $UserID = $arr[0]['InstituteID'];
            $OrderID = $arr[0]['OrderID'];
            $CustomOrderID = $arr[0]['CustomOrderID'];

            $this->db->select("*");
            $this->db->from("keys_order_details");
            $this->db->where("OrderID", $OrderID);
            $this->db->limit(4);
            $query = $this->db->get();

            //echo $this->db->last_query(); die;

            if($query->num_rows() > 0){
                $data = $query->result_array();
                foreach ($data as $key => $value) {
                    //echo "select KeyID from set_keys where Validity = ".$value['Validity']." and AssignedInstitute IS NULL ORDER BY RAND() limit ".$value['Keys']; die;
                    $query = $this->db->query("select KeyID,UsedStatus from set_keys where Validity = ".$value['Validity']." and AssignedInstitute = ".$UserID." ORDER BY KeyID DESC limit ".$value['Keys']);
                    $keysID = $query->result_array();

                    foreach ($keysID as $key => $value) {
                        if($value['UsedStatus'] == 1){
                            array_push($UsedKeys, $value['KeyID']);
                        }
                        $updateArr = array("AssignedInstitute"=>NULL,"AllotedDate"=>NULL,"UsedStatus"=>NULL);
                        $this->db->where("KeyID",$value['KeyID']);
                        $this->db->update("set_keys",$updateArr);
                    }
                  
                }

                if(!empty($UsedKeys)){
                    $updateArr = array("Key"=>NULL,"Validity"=>NULL,"KeyAssignedOn"=>NULL,"ActivatedOn"=>NULL,"ExpiredOn"=>NULL,"KeyStatusID" => 0);
                    $this->db->where("Key IN (".implode(",", $UsedKeys).")");
                    $this->db->update("tbl_students",$updateArr);

                    // $updateArr = array("Password"=>NULL);
                    // $this->db->where("Key IN (".implode(",", $UsedKeys).")");
                    // $this->db->update("tbl_users_login",$updateArr);
                }                

                $updateArr = array("OrderStatusID"=>1);
                $this->db->where("OrderID", $OrderID);
                $this->db->update("keys_orders",$updateArr);

                $Institutedata =  $this->Common_model->getInstituteDetailByID($UserID,'FirstName,LastName,Email');

                $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];

                $EmailText = "<p>Hello,".$InstituteName."</p>";

                $EmailText .= "<p>We have revoked ordered keys for order ".$CustomOrderID."</p>";
                $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);
                sendMail(array(
                    'emailTo'       => $Institutedata['Email'],         
                    'emailSubject'  => "Order Verified",
                    'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
                ));
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }
        return TRUE;
    }


    function allotOrderedSMS($EntityID,$Input){
        $this->db->trans_start();

        $this->db->select("OrderID, InstituteID, CustomOrderID, TransactionID, CreditedOn");
        $this->db->from("keys_orders");
        $this->db->where("OrderGUID", $Input['OrderGUID']);
        $this->db->limit(1);
        $Query = $this->db->get();

        if($Query->num_rows() > 0){
            $arr = $Query->result_array();
            $UserID = $arr[0]['InstituteID'];
            $OrderID = $arr[0]['OrderID'];
            $TransactionID = $arr[0]['TransactionID'];
            $CreditedOn = $arr[0]['CreditedOn'];
            $CustomOrderID = $arr[0]['CustomOrderID'];

            $this->db->select("*");
            $this->db->from("tbl_bulk_sms_credits");
            $this->db->where("UserID", $UserID);
            $this->db->limit(1);
            $query = $this->db->get();

            //echo $this->db->last_query(); die;

            if($query->num_rows() > 0){
                $data = $query->result_array();
                foreach ($data as $key => $value) {
                    $TotalSMS = $Input['SMSCredits']+$value['TotalSMS'];
                    $AvailableSMS = $Input['SMSCredits']+$value['AvailableSMS'];

                    $updateArr = array("TotalSMS"=>$TotalSMS, "AvailableSMS"=>$AvailableSMS);
                    $this->db->where("UserID", $UserID);
                    $this->db->update("tbl_bulk_sms_credits",$updateArr);
                    //echo $this->db->last_query(); die;
                }

                $updateArr = array("OrderStatusID"=>2);
                $this->db->where("OrderID", $OrderID);
                $this->db->update("keys_orders",$updateArr);

                $Institutedata =  $this->Common_model->getInstituteDetailByID($UserID,'FirstName,LastName,Email');

                $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];

                $EmailText = "<p>Hello,".$InstituteName."</p>";

                //$EmailText .= "<p>We have alloted ordered SMS Credits successfully for order ".$CustomOrderID."</p>";
                $EmailText .= "<p>Your Order for keys/sms credits is successfully Verified.</p>";
                $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);
                sendMail(array(
                    'emailTo'       => $Institutedata['Email'],         
                    'emailSubject'  => "Order Verified",
                    'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
                ));
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }
        return TRUE;
    }


    function verifyOrder($EntityID,$Input){
        $this->db->trans_start();

        $this->db->select("OrderPrice,OrderID,CustomOrderID,InstituteID");
        $this->db->from("keys_orders");
        $this->db->where("OrderGUID", $Input['OrderGUID']);
        $this->db->limit(1);
        $Query = $this->db->get();

        if($Query->num_rows() > 0){
            $arr = $Query->result_array();
            $OrderPrice = $arr[0]['OrderPrice'];
            $OrderID = $arr[0]['OrderID'];
            $CustomOrderID = $arr[0]['CustomOrderID'];

            $Institutedata =  $this->Common_model->getInstituteDetailByID($arr[0]['InstituteID'],'FirstName,LastName,Email');
        }

        //die;

        $updateArr = array_filter(array(
            "CreditedOn" => date("Y-m-d", strtotime($Input['CreditedOn'])),
            "TransactionNumber" => $Input['TransactionNumber'],
            "StatusID" => 2
        ));

        $this->db->where("PaymentGUID", $Input['PaymentGUID']);
        $this->db->update("keys_payment",$updateArr);        


        if($Input['OrderFor'] == "SMS Credits") {

            $updateArr = array_filter(array(
                "CreditedOn" => date("Y-m-d", strtotime($Input['CreditedOn'])),
                "TransactionID" => $Input['TransactionNumber'],
                "PaymentStatusID" => 2
            ));

            $this->db->where("OrderGUID", $Input['OrderGUID']);
            $this->db->update("keys_orders",$updateArr);  

            $this->allotOrderedSMS($EntityID,$Input);

            $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];

            $EmailText = "<p>Hello,".$InstituteName."</p>";

            //$EmailText .= "<p>Your SMS Credits Order ".$CustomOrderID." is successfully Verified</p>";

            $EmailText .= "<p>Your Order for keys/sms credits is successfully Verified.</p>";

            $EmailText .= "<p>Details are mentioned below.</p>";

            $EmailText .= "<p>Transaction Number : ".$TransactionID."</p>";

            $EmailText .= "<p>Credited ON : ".$CreditedOn."</p>";
            $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);
            sendMail(array(
                'emailTo'       => $Institutedata['Email'],         
                'emailSubject'  => "Order Verified",
                'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
            ));

        }else{

            $this->db->select("SUM(Amount) as amount");
            $this->db->from("keys_payment");
            $this->db->where("OrderID",$OrderID);
            $Query = $this->db->get();

            //echo $this->db->last_query();
            $TotalPaidAmount = 0;

            if($Query->num_rows() > 0){
                $amount = $Query->result_array();
                $TotalPaidAmount = $amount[0]['amount'];
            }  
            //echo $OrderPrice; die;
            if($TotalPaidAmount == $OrderPrice){
                $updateArr = array_filter(array(
                    "CreditedOn" => date("Y-m-d", strtotime($Input['CreditedOn'])),
                    "TransactionID" => $Input['TransactionNumber'],
                    "PaymentStatusID" => 2
                ));

                $this->db->where("OrderGUID", $Input['OrderGUID']);
                $this->db->update("keys_orders",$updateArr);  
                $this->allotOrderedKeys($EntityID,$Input); 

                $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];
            
                $EmailText = "<p>Hello,".$InstituteName."</p>";

                //$EmailText .= "<p>Your Order ".$CustomOrderID." for keys is successfully Verified</p>";

                $EmailText .= "<p>Your Order for keys/sms credits is successfully Verified.</p>";

                $EmailText .= "<p>Details are mentioned below.</p>";

                $EmailText .= "<p>Transaction Number : ".$Input['TransactionNumber']."</p>";

                $EmailText .= "<p>Credited ON : ".$Input['CreditedOn']."</p>";
                $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);
                sendMail(array(
                    'emailTo'       => $Institutedata['Email'],         
                    'emailSubject'  => "Order Verified",
                    'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
                ));     
            }else{
                $InstituteName = $Institutedata['FirstName']." ".$Institutedata['LastName'];
            
                $EmailText = "<p>Hello,".$InstituteName."</p>";

                $EmailText .= "<p>Your Order ".$CustomOrderID." for keys is successfully Verified</p>";

                $EmailText .= "<p>Details are mentioned below.</p>";

                $EmailText .= "<p>Transaction Number : ".$Input['TransactionNumber']."</p>";

                $EmailText .= "<p>Credited ON : ".$Input['CreditedOn']."</p>";
                $content = $this->load->view('emailer/placed_order',array("EmailText" =>  $EmailText),TRUE);
                sendMail(array(
                    'emailTo'       => $Institutedata['Email'],         
                    'emailSubject'  => "Order Verified",
                    'emailMessage'  =>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
                ));    
            }
                    
        }        

        //echo $this->db->last_query(); die;    

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }
        return TRUE;
    }

}