<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4" id="top-heading">Assigned Enquiries</h1>
</header>
<?php //$this->load->view('includes/breadcrumb'); ?>

<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body"><!-- Body -->

<nav aria-label="breadcrumb" >
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>  
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>enquiry">Manage Enquiry</a></li>
		<li class="breadcrumb-item">Assigned</li> 
	</ol> 
</nav>


	<!-- Top container -->

<div class="clearfix mt-2 mb-2">
	<form class="" name="filterForm" id="filterForm" role="form" autocomplete="off">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<select name="filterStatus" id="filterStatus" class="form-control">
						<option value="">Select Status</option>

						<option value="Hot">Hot</option>

						<option value="Mild">Mild</option>

						<option value="Cold">Cold</option>

						<option value="Closed">Closed</option>

						<option value="Dropped">Dropped</option>
					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterSource" id="filterSource" class="form-control">

						<option value="">Select Source</option>

						<option value="Reference">Reference</option>

						<option value="Website">Website</option>

						<option value="App">App</option>

						<option value="Walkin">Walk-in</option>

						<option value="Advertisement">Advertisement</option>

					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterAssignedToUser" id="filterAssignedToUser" class="form-control">

						<option value="">Select Assigned To</option>

						<option ng-repeat="xx in data.Users" value="{{xx.UserID}}">{{xx.FirstName}}&nbsp;{{xx.LastName}}&nbsp;({{xx.UserTypeName}})</option>

					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="" maxlength="50" placeholder="From Date Range">
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="" maxlength="50" placeholder="To Date Range">
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
		</div>
	</form>			


	<!-- <div class="float-right col-md-4" style="margin-top: -35px;">

		<button class="btn btn-success btn-sm ml-1" ng-click="exportPDF();">Export PDF</button>
		<button class="btn btn-success btn-sm ml-1" ng-click="exportExcel();">Export Excel</button>		

	</div> -->
	
</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 
		

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width:80px;" class="text-center">Source</th>

					<th style="width: 200px;">Hot</th>

					<th style="width: 100px;">Mild</th>

					<th style="width: 100px;">Cold</th>

					<th style="width: 100px;">Closed</th>

					<th style="width: 100px;">Dropped</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">

				<tr scope="row" ng-repeat="(key, row) in data.dataList">

					<td>{{key}}</td>

					<td>
						<a href="" ng-click="loadEnquiryByStatusPopup(key, 'Hot')" ng-if="row.Hot > 0"><b>{{row.Hot}}</b></a>
						<span ng-if="row.Hot <= 0">0</span>
					</td>				

					<td>
						<a href="" ng-click="loadEnquiryByStatusPopup(key, 'Mild')" ng-if="row.Mild > 0"><b>{{row.Mild}}</b></a>
						<span ng-if="row.Mild <= 0">0</span>
					</td>

					<td>
						<a href="" ng-click="loadEnquiryByStatusPopup(key, 'Cold')" ng-if="row.Cold > 0"><b>{{row.Cold}}</b></a>
						<span ng-if="row.Cold <= 0">0</span>
					</td>

					<td>
						<a href="" ng-click="loadEnquiryByStatusPopup(key, 'Closed')" ng-if="row.Closed > 0"><b>{{row.Closed}}</b></a>
						<span ng-if="row.Closed <= 0">0</span>
					</td>

					<td>
						<a href="" ng-click="loadEnquiryByStatusPopup(key, 'Dropped')" ng-if="row.Dropped > 0"><b>{{row.Dropped}}</b></a>
						<span ng-if="row.Dropped <= 0">0</span>
					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->




	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Enquiry Details</h3>					    	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>









</div><!-- Body/ -->