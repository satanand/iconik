<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questionbank_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}	

	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getQuestions($fields,$Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15)
	{
		//print_r($fields);
		$InstituteID = $this->Common_model->getInstituteByEntity($Where['EntityID']);
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select('tbl_question_bank.*');
		$this->db->from('tbl_question_bank');
		$this->db->join('tbl_entity', 'tbl_entity.EntityID = tbl_question_bank.QtBankID');
		$this->db->where("tbl_entity.InstituteID",$InstituteID);
		$this->db->where("tbl_question_bank.StatusID",2);		
		
		
		if(!empty($Where['TopicID']))
		{
			$this->db->where("tbl_question_bank.TopicID",$Where['TopicID']);
		}else{

			$this->db->group_start();
			$this->db->or_where("tbl_question_bank.TopicID < 0", null,false);
			$this->db->or_where("tbl_question_bank.TopicID", '');
			$this->db->or_where("tbl_question_bank.TopicID is NULL", null,false);
			$this->db->group_end();
		}
		

		if(!empty($Where['SubjectID']))
		{
			$this->db->where("tbl_question_bank.SubjectID",$Where['SubjectID']);
			//$this->db->where("tbl_question_bank.TopicID < 0 || tbl_question_bank.TopicID = '' || tbl_question_bank.TopicID IS NULL");
		}		
		if(!empty($Where['CourseID']))
		{
			$this->db->where("tbl_question_bank.CourseID",$Where['CourseID']);
			//$this->db->where("tbl_question_bank.TopicID < 0 || tbl_question_bank.TopicID = '' || tbl_question_bank.TopicID IS NULL");
		}	
		


		if(!empty($Where['QuestionsGroup'])){
			$this->db->where("(tbl_question_bank.QuestionsGroup like '%".$Where['QuestionsGroup']."%' OR tbl_question_bank.QuestionsGroup like '%".$Where['QuestionsGroup'].",%' OR tbl_question_bank.QuestionsGroup = '%".$Where['QuestionsGroup']."%')");
		}
		if(!empty($Where['QuestionsLevel'])){
			$this->db->where("tbl_question_bank.QuestionsLevel",$Where['QuestionsLevel']);
		}
		if(!empty($Where['QuestionsType'])){
			$this->db->where("tbl_question_bank.QuestionsType",$Where['QuestionsType']);
		}

		if(!empty($Where['Keyword'])){
			$this->db->group_start();			
			$this->db->or_like("tbl_question_bank.QuestionsLevel", trim($Where['Keyword']));
			$this->db->or_like("tbl_question_bank.QuestionsType", trim($Where['Keyword']));
			$this->db->or_like("tbl_question_bank.QuestionsMarks",$Where['Keyword']);
			$this->db->or_like("tbl_question_bank.QuestionContent",$Where['Keyword']);
			$this->db->or_like("tbl_question_bank.Explanation",$Where['Keyword']);	
			$this->db->group_end();
		}


		$this->db->order_by('tbl_question_bank.QtBankID','DESC');
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();die;
		if($Query->num_rows()>0)
		{	
			//echo "<pre>"; print_r($Query->result_array()); die;
			foreach($Query->result_array() as $Record)
			{

				$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$Record['QtBankID']."'");
				$answer = $answer->result_array();

				$Record['answer'] = $answer;

				//$Record['QuestionContent'] = stripslashes(html_entity_decode($Record['QuestionContent']));

				

				//$Record['QuestionContent'] = htmlspecialchars(trim(strip_tags($Record['QuestionContent'])));

				/*get attached media logo - starts*/
				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $Record['QtBankID']),TRUE);
				$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());
				/*get attached media - ends*/

				$QuestionsGroup = explode(',',$Record['QuestionsGroup']); $groupname = array();
				foreach ($QuestionsGroup as $key => $value) {
					$query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$value.'"');

					$result = $query->result_array();

					array_push($groupname, $result[0]['QuestionGroupName']);
				}

				$Record['QuestionsGroup'] = implode(', ', $groupname);

				$course = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['CourseID']."'");
				$course = $course->result_array()[0];
				$Record['ParentCategoryName'] = $course['CategoryName'];
				$Record['ParentCategoryGUID'] = $course['CategoryGUID'];

				
				$subject = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['SubjectID']."'");
				$subject = $subject->result_array()[0];
				$Record['SubCategoryName'] = $subject['CategoryName'];
				$Record['SubCategoryGUID'] = $subject['CategoryGUID'];


				$topic = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['TopicID']."'");
				$topic = $topic->result_array()[0];
				$Record['TopicCategoryName'] = ucfirst($topic['CategoryName']);
				$Record['TopicCategoryID'] = $topic['CategoryGUID'];

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function getQuestionAnswer($EntityID, $ToEntityID, $QtBankGUID, $courseID, $subjectID, $view=""){	
		$where_part = ""; //echo $view;

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($subjectID)){
			$where_part .= "SubjectID = '".$subjectID."'";
		}else if(!empty($courseID)){
			$where_part .= "CourseID = '".$courseID."'";
		}
		
		if($view == 'next'){
			$query = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.CourseID,QB.SubjectID,QB.QuestionContent,QB.QuestionsGroup,QB.QuestionsLevel,QB.QuestionsMarks,QB.QuestionsType,QB.Explanation,tbl_entity.StatusID, QB.TopicID from tbl_question_bank as QB join tbl_entity ON tbl_entity.EntityID = QB.QtBankID where tbl_entity.InstituteID = ".$InstituteID." and QB.QtBankID < '".$QtBankGUID."' and ".$where_part." order by QB.QtBankID DESC limit 1");
		}else if($view == 'previous'){
			$query = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.CourseID,QB.SubjectID,QB.QuestionContent,QB.QuestionsGroup,QB.QuestionsLevel,QB.QuestionsMarks,QB.QuestionsType,QB.Explanation,tbl_entity.StatusID, QB.TopicID from tbl_question_bank as QB join tbl_entity ON tbl_entity.EntityID = QB.QtBankID where tbl_entity.InstituteID = ".$InstituteID." and QB.QtBankID > '".$QtBankGUID."' and ".$where_part."  limit 1");
		}else {
			$query = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.CourseID,QB.SubjectID,QB.QuestionContent,QB.QuestionsGroup,QB.QuestionsLevel,QB.QuestionsMarks,QB.QuestionsType,QB.Explanation,tbl_entity.StatusID, QB.TopicID from tbl_question_bank as QB join tbl_entity ON tbl_entity.EntityID = QB.QtBankID where tbl_entity.InstituteID = ".$InstituteID." and QB.QtBankID = '".$QtBankGUID."' LIMIT 1");
		}	
		
		$QA = $query->result_array();
		//echo $this->db->last_query();
		if(count($QA) > 0){
			foreach($QA as $Record){

				/*get attached media logo - starts*/
				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $Record['QtBankID']),TRUE);
				$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());
				/*get attached media - ends*/

				$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$Record['QtBankID']."' order by AnswerID ASC");
				$answer = $answer->result_array();

				$Record['answer'] = $answer;

				$course = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['CourseID']."'");
				$course = $course->result_array()[0];
				$Record['ParentCategoryName'] = $course['CategoryName'];
				$Record['ParentCategoryGUID'] = $course['CategoryGUID'];


				$subject = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['SubjectID']."'");
				$subject = $subject->result_array()[0];
				$Record['SubCategoryName'] = $subject['CategoryName'];
				$Record['SubCategoryGUID'] = $subject['CategoryGUID'];


				$subject = $this->db->query("select CategoryName,CategoryID from set_categories where CategoryID = '".$Record['TopicID']."'");
				$subject = $subject->result_array()[0];

				$Record['TopicCategoryName'] = $subject['CategoryName'];
				$Record['TopicCategoryID'] = $subject['CategoryID'];

				$QuestionsGroup = explode(',',$Record['QuestionsGroup']); $groupname = array();
				foreach ($QuestionsGroup as $key => $value) {
					$query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$value.'"');

					$result = $query->result_array();

					array_push($groupname, $result[0]['QuestionGroupName']);
				}

				$Record['QuestionsGroup'] = implode(', ', $groupname);

				//$Record['QuestionContent'] = htmlspecialchars(trim(strip_tags($Record['QuestionContent'])));

				if($Record['Explanation']){
					//$Record['Explanation'] = htmlspecialchars(trim(strip_tags($Record['Explanation'])));
				}
				

				$Records[] = $Record;
			}
			$Return['Data'] = $Records;
			return $Return;
		}else{
			return false;
		}
	}

	/*
	Description: 	Use to add new post
	*/
	function addQA($EntityID, $ToEntityID, $Input=array()){
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$QtBankGUID = get_guid();
		/* Add post to entity table and get EntityID. */
		$QtBankID = $this->Entity_model->addEntity($QtBankGUID, array(
			"EntityTypeID"	=>	10,
			"UserID"		=>	$EntityID,
			"Privacy"		=>	@$Input["Privacy"],
			"StatusID"		=>	2,
			"Rating"		=>	@$Input["Rating"]
		));

		$this->db->where('EntityID',$QtBankID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		$this->db->select('ParentCategoryID');
		$this->db->where('CategoryID',$Input["SubjectID"]);
		$this->db->from('set_categories');
		$query = $this->db->get();
        //echo $this->db->last_query(); die;
		if ( $query->num_rows() > 0 )
		{
			$row = $query->row_array();
			$Input["CourseID"] = $row['ParentCategoryID'];

			$InsertData = array_filter(array(
				"QtBankID" 		=>	$QtBankID,
				"QtBankGUID" 	=>	$QtBankGUID,			
				"ParentQtBankID" =>	$QtBankID,
				"CourseID" 		=>	$Input["CourseID"],	
				"SubjectID" 	=>	$Input["SubjectID"],
				"TopicID" 		=>	$Input["TopicID"],	
				"EntityID" 		=>	$EntityID,
				"QuestionsGroup" =>	$Input["QuestionsGroup"],
				"QuestionsLevel" => 	$Input["QuestionsLevel"],
				"QuestionsType" =>	$Input["QuestionsType"],
				"QuestionsMarks" =>	$Input["QuestionsMarks"],
				"QuestionContent" => $Input["QuestionContent"],
				"Explanation"	=>	@$Input["Explanation"]
			));
			$this->db->insert('tbl_question_bank', $InsertData);

			$AnswerData = array();
			if($Input["QuestionsType"] == 'Multiple Choice')
			{ //print_r($Input["AnswerContent"]);
		$AnswerContent = ($Input["AnswerContent"]); 

		foreach ($AnswerContent as $key => $value) 
		{
			if($Input["CorrectAnswer"] == $value)
			{
				$CorrectAnswer = 1;
			}
			else
			{
				$CorrectAnswer = 0;
			}

			$AnswerData[] = array(
				"QtBankID" 		=>	$QtBankID,
				"AnswerContent" => $value,
				"CorrectAnswer"	=> $CorrectAnswer
			);
		}
		if($AnswerData){
			$this->db->insert_batch('tbl_questions_answer', $AnswerData);
		}
	}else if($Input["QuestionsType"] == 'Logical Answer'){
		if($Input["CorrectAnswer"] === True || $Input["CorrectAnswer"] === "True" || $Input["CorrectAnswer"] === true)
		{
			$CorrectAnswerTrue = 1;
			$CorrectAnswerFalse = 0;
		}else{
			$CorrectAnswerTrue = 0;
			$CorrectAnswerFalse = 1;
		}
		$AnswerData[] = array(
			"QtBankID" 		=>	$QtBankID,
			"AnswerContent" => 'True',
			"CorrectAnswer"	=> $CorrectAnswerTrue
		);
		$AnswerData[] = array(
			"QtBankID" 	=>	$QtBankID,
			"AnswerContent" => 'False',
			"CorrectAnswer"	=> $CorrectAnswerFalse
		);
		if($AnswerData){
			$this->db->insert_batch('tbl_questions_answer', $AnswerData);
		}
	}

	$this->db->trans_complete();
	if ($this->db->trans_status() === FALSE)
	{
		return FALSE;
	}
	return array('QtBankID' => $QtBankID, 'QtBankGUID' => $QtBankGUID);
}else{
	return FALSE;
}
}


	/*
	Description: 	Use to add new post
	*/
	function editQA($EntityID, $ToEntityID, $Input=array()){	

		$this->db->select('ParentCategoryID');
		$this->db->where('CategoryID',$Input["SubjectID"]);
		$this->db->from('set_categories');
		$query = $this->db->get();
        //echo $this->db->last_query(); die;
		if ( $query->num_rows() > 0 )
		{
			$row = $query->row_array();
			$Input["CourseID"] = $row['ParentCategoryID'];

			$InsertData = array_filter(array(
				"CourseID" 		=>	$Input["CourseID"],	
				"SubjectID" 	=>	$Input["SubjectID"],
				"TopicID" 	=>	$Input["TopicID"],	
				"EntityID" 		=>	$EntityID,
				"QuestionsGroup" =>	$Input["QuestionsGroup"],
				"QuestionsLevel" => 	$Input["QuestionsLevel"],
				"QuestionsType" =>	$Input["QuestionsType"],
				"QuestionsMarks" =>	$Input["QuestionsMarks"],
				"QuestionContent" => $Input["QuestionContent"],
				"Explanation"	=>	@$Input["Explanation"]
			));
			$this->db->where('QtBankID', $Input["QtBankID"]);
			$this->db->update('tbl_question_bank', $InsertData);

			$this->db->where('QtBankID', $Input["QtBankID"]);
			$this->db->delete('tbl_questions_answer');

			if($Input["QuestionsType"] == 'Multiple Choice'){
				$AnswerContent = $Input["AnswerContent"];
				foreach ($AnswerContent as $key => $value) {
					if($Input["CorrectAnswer"] == $value){
						$CorrectAnswer = 1;
					}else{
						$CorrectAnswer = 0;
					}
					
					$AnswerData[] = array(
						"QtBankID" 		=>	$Input["QtBankID"],
						"AnswerContent" => $value,
						"CorrectAnswer"	=> $CorrectAnswer
					);
				}
				if($AnswerData){
					$this->db->insert_batch('tbl_questions_answer', $AnswerData);
				}
			}
			else if($Input["QuestionsType"] == 'Logical Answer')
			{

				if($Input["CorrectAnswer"] === True || $Input["CorrectAnswer"] === "True" || $Input["CorrectAnswer"] === true)
				{
					$CorrectAnswerTrue = 1;
					$CorrectAnswerFalse = 0;
				}else{
					$CorrectAnswerTrue = 0;
					$CorrectAnswerFalse = 1;
				}
				$AnswerData[] = array(
					"QtBankID" 		=>	$Input["QtBankID"],
					"AnswerContent" => 'True',
					"CorrectAnswer"	=> $CorrectAnswerTrue
				);
				$AnswerData[] = array(
					"QtBankID" 		=>	$Input["QtBankID"],
					"AnswerContent" => 'False',
					"CorrectAnswer"	=> $CorrectAnswerFalse
				);
				if($AnswerData)
				{
					$this->db->insert_batch('tbl_questions_answer', $AnswerData);
				}
			}
			
			return TRUE;
		}else{
			return FALSE;
		}
	}

	//select CategoryID as CourseID, CategoryName as SubjectID, 0 as total, `ParentCategoryID`, 'Course' as type from set_categories where `CategoryTypeID` = 2 UNION ALL select CategoryID as CourseID, CategoryName as SubjectID, 0 as total, `ParentCategoryID` , 'Subject' as type from set_categories where `CategoryTypeID` = 3 UNION ALL select CourseID, SubjectID, count(QtBankID) as total , 0 as ParentCategoryID, 'Question' as type from tbl_question_bank where StatusID = 2 GROUP BY CourseID, SubjectID
	function getQuestionBankStatistics($EntityID,$SubjectID="",$CourseID="",$TopicID="")
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$app1 = $app2 = ""; $app3 = $app4 = ""; $app5 = $app6 = "";
		if(isset($CourseID) && !empty($CourseID))
		{
			$app1 = " AND c1.CategoryID = '".$CourseID."' ";
			$app3 = " AND q1.CourseID = '".$CourseID."' ";
		}

		if(isset($SubjectID) && !empty($SubjectID))
		{
			$app2 = " AND s1.CategoryID = '".$SubjectID."' ";
			$app4 = " AND q1.SubjectID = '".$SubjectID."' ";
		}

		if(isset($TopicID) && !empty($TopicID))
		{
			$app5 = " AND s1.CategoryID = '".$TopicID."' ";
			$app6 = " AND q1.TopicID = '".$TopicID."' ";
		}


		/*$sql = "SELECT * FROM
		(
			SELECT c1.CategoryID as CourseID, c1.CategoryName as SubjectID, 0 as total, c1.`ParentCategoryID`, 'Course' as type, e1.InstituteID, c1.CategoryGUID as GUID 
			FROM set_categories c1
			INNER JOIN tbl_entity e1 ON e1.EntityID = c1.CategoryID AND e1.InstituteID = ".$InstituteID."
			WHERE c1.`CategoryTypeID` = 2 $app1

			UNION ALL 

			SELECT s1.CategoryID as CourseID, s1.CategoryName as SubjectID, 0 as total, s1.`ParentCategoryID` , 'Subject' as type, e1.InstituteID, s1.CategoryGUID as GUID 
			FROM set_categories as s1 
			INNER JOIN tbl_entity e1 ON e1.EntityID = s1.CategoryID AND e1.InstituteID = ".$InstituteID." 
			WHERE s1.`CategoryTypeID` = 3 $app2 

			UNION ALL 

			SELECT s1.CategoryID as CourseID, s1.CategoryName as SubjectID, 0 as total, s1.`ParentCategoryID` , 'Topic' as type, e1.InstituteID, s1.CategoryGUID as GUID 
			FROM set_categories as s1 
			INNER JOIN tbl_entity e1 ON e1.EntityID = s1.CategoryID AND e1.InstituteID = ".$InstituteID." 
			WHERE s1.`CategoryTypeID` = 4 $app5

			UNION ALL 

			SELECT q1.CourseID, q1.SubjectID, count(q1.QtBankID) as total , 0 as ParentCategoryID, 'Question' as type, e1.InstituteID, '' as GUID 
			FROM tbl_question_bank q1
			INNER JOIN tbl_entity e1 ON e1.EntityID = q1.QtBankID AND e1.InstituteID = ".$InstituteID."
			WHERE q1.StatusID = 2 $app3 $app4 
			GROUP BY q1.CourseID, q1.SubjectID, q1.TopicID
		) as temp 
		WHERE InstituteID = ".$InstituteID."
		ORDER BY type, CourseID 
		";*/

		$sql = "
		SELECT * FROM
		(
		SELECT c1.CategoryID as CourseID, 0 as SubjectID, 0 as TopicID, c1.CategoryName, 0 as total, c1.`ParentCategoryID`, 'Course' as type, c1.CategoryGUID as GUID 
		FROM set_categories c1
		INNER JOIN tbl_entity e1 ON e1.EntityID = c1.CategoryID AND e1.InstituteID = ".$InstituteID."
		WHERE c1.`CategoryTypeID` = 2  $app1 

		UNION ALL 

		SELECT 0 as CourseID, s1.CategoryID as SubjectID, 0 as TopicID, s1.CategoryName, 0 as total, s1.`ParentCategoryID` , 'Subject' as type, s1.CategoryGUID as GUID 
		FROM set_categories as s1 
		INNER JOIN tbl_entity e1 ON e1.EntityID = s1.CategoryID AND e1.InstituteID = ".$InstituteID." 
		WHERE s1.`CategoryTypeID` = 3 $app2

		UNION ALL 

		SELECT 0 as CourseID, 0 as SubjectID, s1.CategoryID as TopicID, s1.CategoryName, 0 as total, s1.`ParentCategoryID` , 'Topic' as type, s1.CategoryGUID as GUID 
		FROM set_categories as s1 
		INNER JOIN tbl_entity e1 ON e1.EntityID = s1.CategoryID AND e1.InstituteID = ".$InstituteID." 
		WHERE s1.`CategoryTypeID` = 4 $app5 

		UNION ALL 

		SELECT q1.CourseID, q1.SubjectID, 0 as TopicID, '' as CategoryName, count(q1.QtBankID) as total , 0 as ParentCategoryID, 'Question' as type, '' as GUID 
		FROM tbl_question_bank q1
		INNER JOIN tbl_entity e1 ON e1.EntityID = q1.QtBankID AND e1.InstituteID = ".$InstituteID."
		WHERE q1.StatusID = 2 AND (q1.TopicID = '' OR q1.TopicID <= 0 OR q1.TopicID IS NULL) $app3 $app4 $app6  
		GROUP BY q1.CourseID, q1.SubjectID

		UNION ALL 

		SELECT q1.CourseID, q1.SubjectID, q1.TopicID, '' as CategoryName, count(q1.QtBankID) as total , 0 as ParentCategoryID, 'Question' as type, '' as GUID 
		FROM tbl_question_bank q1
		INNER JOIN tbl_entity e1 ON e1.EntityID = q1.QtBankID AND e1.InstituteID = ".$InstituteID."
		WHERE q1.StatusID = 2 AND q1.TopicID > 0 $app3 $app4 $app6  
		GROUP BY q1.CourseID, q1.SubjectID, q1.TopicID
		) as temp 
		
		ORDER BY type, CourseID, SubjectID, TopicID   
		";

		//echo "<pre>".$sql;

		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0)
		{
			$records = array(); $course = $subject = $topics = $totals = array();
			$data = $query->result_array();
			//echo "<pre>"; print_r($data);
			if(isset($data) && !empty($data))
			{
				foreach($data as $key=>$arr)
				{
					if($arr['type'] == 'Course')
					{ 
						$course[$arr['CourseID']]['CourseID'] = $arr['CourseID'];
						$course[$arr['CourseID']]['CourseName'] = $arr['CategoryName'];
						$course[$arr['CourseID']]['GUID'] = $arr['GUID'];
					}
				}
				

				foreach($data as $key=>$arr)
				{
					if($arr['type'] == 'Subject')
					{
						if(isset($course[$arr['ParentCategoryID']]) && !empty($course[$arr['ParentCategoryID']]))
						{
							//$subject[$arr['CourseID']]['SubjectID'] = $arr['CourseID'];
							
							//$subject[$arr['ParentCategoryID']][$arr['CourseID']]['SubjectName'] = $arr['SubjectID'];
							
							//$subject[$arr['ParentCategoryID']][$arr['CourseID']]['CategoryGUID'] = $arr['GUID'];

							$subject[$arr['ParentCategoryID']][$arr['SubjectID']]['SubjectID'] = $arr['SubjectID'];
							$subject[$arr['ParentCategoryID']][$arr['SubjectID']]['SubjectName'] = $arr['CategoryName'];
							$subject[$arr['ParentCategoryID']][$arr['SubjectID']]['GUID'] = $arr['GUID'];
						}	
					}
				}


				foreach($data as $key=>$arr)
				{
					if($arr['type'] == 'Topic')
					{
						//if(isset($subject[$arr['ParentCategoryID']]) && !empty($subject[$arr['ParentCategoryID']]))
						//{
							//$subject[$arr['CourseID']]['SubjectID'] = $arr['CourseID'];

							//$subject[$arr['ParentCategoryID']][$arr['CourseID']]['SubjectName'] = $arr['SubjectID'];

							//$subject[$arr['ParentCategoryID']][$arr['CourseID']]['CategoryGUID'] = $arr['GUID'];

						$topics[$arr['ParentCategoryID']][$arr['TopicID']]['TopicID'] = $arr['TopicID'];
						$topics[$arr['ParentCategoryID']][$arr['TopicID']]['TopicName'] = $arr['CategoryName'];
						$topics[$arr['ParentCategoryID']][$arr['TopicID']]['GUID'] = $arr['GUID'];
						//}	
					}
				}

				foreach($data as $key=>$arr)
				{
					if($arr['type'] == 'Question' && isset($arr['TopicID']) && !empty($arr['TopicID']) && $arr['TopicID'] > 0 )
					{
						$totals[$arr['CourseID']][$arr['SubjectID']][$arr['TopicID']]['Total'] = $arr['total'];
					}
					elseif($arr['type'] == 'Question' && ($arr['TopicID'] == "" || $arr['TopicID'] <= 0) )
					{
						$totals[$arr['CourseID']][$arr['SubjectID']][0]['Total'] = $arr['total'];
					}
				}
				//echo "<pre>";
				//print_r($totals);
				//echo "<br/><br/>===============================================================<br/>";
				/*echo "<pre>"; 
				print_r($course);
				echo "<br/><br/>===============================================================<br/>";
				echo "<br/><br/>===============================================================<br/>";
				print_r($subject);
				echo "<br/><br/>===============================================================<br/>";
				echo "<br/><br/>===============================================================<br/>";
				print_r($topics);
				echo "<br/><br/>===============================================================<br/>";
				echo "<br/><br/>===============================================================<br/>";
				print_r($totals);
				die;*/
				
				$temp = array();
				foreach ($course as $c => $carr) 
				{					
					$CourseGUID = $carr['GUID'];

					foreach ($subject[$c] as $s => $sarr) 
					{
						$SubjectGUID = $sarr['GUID'];

						$subtotal = 0; $isShow = false; $isFirst = true;

						if(isset($totals[$c][$s][0]['Total']) && !empty($totals[$c][$s][0]['Total']))
						{
							$TopicGUID = "";

							$isShow = true;							


							if($isFirst) { $CourseName = $carr['CourseName']; $SubjectName = $sarr['SubjectName'];}
							elseif(!$isFirst) 
							{ 
								$CourseName = ""; 
								$SubjectName = ""; 
							}

							$temp[] = array("CourseID" => $c,
								"SubjectID" => $s,
								"TopicID" => "",
								"CourseName" => $CourseName,
								"SubjectName" => $SubjectName,
								"TopicName" => "-",
								"CourseGUID" => $CourseGUID,
								"SubjectGUID" => $SubjectGUID,
								"TopicGUID" => $TopicGUID,
								"question_count" => $totals[$c][$s][0]['Total']
							);

							$isFirst = false;

							$subtotal = $subtotal + $totals[$c][$s][0]['Total'];								

							if($isShow)
							{
								$temp[] = array("CourseID" => "",
									"SubjectID" => "",
									"TopicID" => "",
									"CourseName" => "",
									"SubjectName" => "",
									"TopicName" => "Total",
									"CourseGUID" => "",
									"SubjectGUID" => "",
									"TopicGUID" => "",
									"question_count" => $subtotal
								);
							}
						}
						
						$subtotal = 0; $isShow = false; $isFirst = true;	
						foreach ($topics[$s] as $t => $tarr) 
						{
							$TopicGUID = $tarr['GUID'];

							$isShow = true;							

							if(!isset($totals[$c][$s][$t]['Total']) || empty($totals[$c][$s][$t]['Total']))
								$totals[$c][$s][$t]['Total'] = 0;

							if($isFirst) { $CourseName = $carr['CourseName']; $SubjectName = $sarr['SubjectName'];}
							elseif(!$isFirst) 
							{ 
								$CourseName = ""; 
								$SubjectName = ""; 
							}

							$temp[] = array("CourseID" => $c,
								"SubjectID" => $s,
								"TopicID" => $t,
								"CourseName" => $CourseName,
								"SubjectName" => $SubjectName,
								"TopicName" => $tarr['TopicName'],
								"CourseGUID" => $CourseGUID,
								"SubjectGUID" => $SubjectGUID,
								"TopicGUID" => $TopicGUID,
								"question_count" => $totals[$c][$s][$t]['Total']
							);

							$isFirst = false;

							$subtotal = $subtotal + $totals[$c][$s][$t]['Total'];
						}	

						if($isShow)
						{
							$temp[] = array("CourseID" => "",
								"SubjectID" => "",
								"TopicID" => "",
								"CourseName" => "",
								"SubjectName" => "",
								"TopicName" => "Total",
								"CourseGUID" => "",
								"SubjectGUID" => "",
								"TopicGUID" => "",
								"question_count" => $subtotal
							);
						}



					}


				}
				//echo "<pre>"; print_r($temp);die;

			}

			//echo "<pre>"; print_r($temp); die;

			return $temp;
		}
		else
		{
			return false;
		}

	}

	function getQuestionBankStatisticsBK($EntityID,$SubjectID="",$CourseID="")
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($CourseID)){
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and CategoryID='".$CourseID."'");
		}else{
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2");
		}

		if($query->num_rows() > 0){

			$course = $query->result_array();

		}else{
			$course = array();
		}

		$result = array(); 
		$new_result = array(); 
		$check_array = array();
		$check = 1; 
		if(count($course) > 0){
			//print_r($course);
			$p = 0;
			foreach ($course as $key => $value) {

				if(!empty($SubjectID)){
					$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CategoryID']."' and CategoryID = '".$SubjectID."'");
				}else{
					$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CategoryID']."'");
				}

				$SubCategoryData = $query->result_array();

					//print_r($SubCategoryData);

				$parentCategoryName = $value['CategoryName'];
				$parentCategoryGUID = $value['CategoryGUID'];
				$total_question = 0;

				foreach ($SubCategoryData as $k => $v) {

					if(!in_array($parentCategoryName, $check_array)){
						array_push($check_array, $parentCategoryName);
						$result[$parentCategoryName][$k]['ParentCategoryName'] = $parentCategoryName;
					}else{
						$result[$parentCategoryName][$k]['ParentCategoryName'] = "";	
					}
					$result[$parentCategoryName][$k]['ParentName'] = $parentCategoryName;
					$result[$parentCategoryName][$k]['parentCategoryGUID'] = $parentCategoryGUID;
					$result[$parentCategoryName][$k]['SubCategoryName'] = $v['CategoryName'];
					$result[$parentCategoryName][$k]['CategoryGUID'] = $v['CategoryGUID'];
					$result[$parentCategoryName][$k]['question_count'] = 0;

					$query = $this->db->query("SELECT count(qb.QtBankID) as count_question FROM tbl_question_bank as qb where qb.SubjectID = ".$v['CategoryID']);

					$data = $query->result_array();

					$result[$parentCategoryName][$k]['question_count'] = $data[0]['count_question'];

					$total_question = $total_question+$data[0]['count_question'];

					if(!empty($result[$parentCategoryName][$k])){
						array_push($new_result, $result[$parentCategoryName][$k]);
					}

					$count_sub = $k+1;
						if($count_sub == count($SubCategoryData)){ 	//echo $count_sub; echo "<br>";
						$result[$parentCategoryName][$count_sub]['question_count'] = $total_question;
						$result[$parentCategoryName][$count_sub]['ParentName'] = $parentCategoryName;
						$result[$parentCategoryName][$count_sub]['parentCategoryGUID'] = $parentCategoryGUID;
						$result[$parentCategoryName][$count_sub]['SubCategoryName'] = 'Total';
						$result[$parentCategoryName][$count_sub]['CategoryGUID'] = $v['CategoryGUID'];
							//print_r(expression)
						array_push($new_result, $result[$parentCategoryName][$count_sub]);
					}
				}
			}
			return $new_result;
		}
		return false;

	}


	function changeStatus($EntityID, $QtBankID, $StatusID){		
		return $status = $this->Entity_model->updateEntityInfo($QtBankID, array(
			"StatusID"	=>	$StatusID
		));
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deletePost($UserID, $QtBankID){
		$QuestionData=$this->getPosts('P.EntityID',array('QtBankID'=>$QtBankID, 'SessionUserID'=>$UserID));
		if(!empty($QuestionData) && $UserID==$QuestionData['EntityID']){
			$this->Entity_model->deleteEntity($QtBankID);
			return TRUE;
		}
		return FALSE;
	}
}
