<style type="text/css">
   .block {
   background-color: #F1F4F7!important;
   }
   .panel-group{
   float: left;
   width: 100%;
   }
   .panel-primary {
   color: #fff;
   /* background-color: #337ab7;*/
   border-color: #337ab7;
   }
   .panel {
   width: 18%;
   float: left;
   margin: 10px 155px 20px 0px;	   
   }
   .dashboard-links{
   border-right: 4px solid #ababab;
   background-color: #fff;
   padding: 5px 15px;
   position: relative;
   min-height: 65px;
   height: auto;
   /* border-radius: 10px; */
   text-align: center;
   float: left;
   width: 100%;
   background-size: cover!important;
   }
   .panel-body{
   background-color: transparent;
   /*background: url(http://127.0.0.1/public_html/asset/img/dashboard/Label.png) center center no-repeat!important;*/
   padding: 5px 15px;
   position: relative;
   min-height: 90px;
   height: auto;
   /* border-radius: 10px; */
   text-align: center;
   float: left;
   width: 100%;
   /*background-size: contain!important;*/
   }	
   .panel-body img{
   height: 50px;
   /*margin-top: 20px;*/
   }
   .panel-body span{
   font-size: 15px;
   color: #525252;
   font-weight: 400;
   padding-top: 8px;
   float: left;
   width: 100%;
   text-align: center;
   margin-bottom: 5px;
   }
   .sec-panel-common {
   width: 20%;
   height: 50px;
   }
   .sec-panel-common > .panel-body{
   width: 20%;
   height: 50px;
   }
   .sec-head{
   color: #525252 !important;
   font-size: 16px;
   font-weight: 550;
   margin-top: 45px;
   }
   .quick-connect-panel {
   /*background-image: url(http://127.0.0.1/marketplace/asset/img/banner1.jpg)!important;*/
   width: 18%;
   float: left;
   margin: 20px 50px 20px 0px;
   }
   .quick-connect-body{
   background-color: transparent;
   /* padding: 5px 0px; */
   position: relative;
   min-height: 65px;
   /* border-radius: 10px; */
   text-align: center;
   float: left;
   width: 100%;
   }	
   .quick-connect-body img{
   height: 50px;
   /* margin-top: 12px; */
   float: left;
   }
   .quick-connect-body span{
   font-size: 15px;
   color: #525252;
   font-weight: 400;
   margin-top: 10px;
   float: left;
   width: 70%;
   text-align: center;
   float: right;
   }
</style>
<div ng-controller="PageController" id="content-body" ng-init="getList(); GetBirthDaysList(); GetPendingProfile();">
   <?php //$aa = array_column($this->Menu, 'ChildMenu');  print_r($aa); ?>
   <div class="container">
      <!-- <div style="margin: 0px 78px 0px 78px;
         float: left;width: 100%;"> -->
      <div class="ml-1 row">
         <h3 class="text-left text-muted" style="color:#525252 !important;">Dashboard             
            <label id="GetBirthDaysListCount" style="display:none; cursor: pointer; font-size: 14px !important; margin-left: 15px;" class="badge badge-warning" ng-click="GetBirthDaysListPopup();">3{{data.birthDaysList.length}} Birthdays</label>

            <a id="PendingProfileCount" href="<?php echo base_url(); ?>students/pending_profile" class="badge badge-info" style="display: none; font-size: 14px !important; margin-left: 5px;">{{data.PendingProfileCount}} Pending Profiles</a>
         </h3>
      </div>
      <div class="row">
         <!-- Body -->
         <h2 class="text-left text-muted sec-head ml-3">Quick Tasks</h2>
      </div>
      <div class="row">
         <?php if(in_array("Manage Students", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 shadow-lg rounded mb-3">
            <a href="<?php echo base_url(); ?>students" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/contacts.png" class="img-fluid"><span>Manage Students</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Manage Batches", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3" ng-if="UserTypeID != 87">
            <a href="<?php echo base_url(); ?>batch" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/presentation.png" class="img-fluid"><span>Manage Batches</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Manage Time Scheduling", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>timescheduling" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/schedule-planning.png" class="img-fluid"><span>Time Scheduling</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Manage Content", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>content" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/blended-learning.png" class="img-fluid"><span>Manage Content</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Manage Question Bank", array_column($this->Menu, 'ControlName'))){ ?>
      </div>
      <div class="row">
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>questionbank" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/questions-and-answers.png" class="img-fluid"><span>Question Bank</span></div>
            </a>
         </div>
         <?php } ?>


         <?php if(in_array("Scholarships Test", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>scholarshiptest" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/scholarship.png" class="img-fluid"><span>Scholarship Test</span></div>
            </a>
         </div>
         <?php } ?>




         <?php if(in_array("Manage App Keys", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>keys" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/key-person.png" class="img-fluid"><span>Manage App Keys</span></div>
            </a>
         </div>
         <?php } ?>

         <?php if(in_array("Reports", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>reports" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/document.png" class="img-fluid"><span>Reports</span></div>
            </a>
         </div>
         <?php } ?>


         <?php if(in_array("Manage Store", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3">
            <a href="<?php echo base_url(); ?>products" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/online-store.png" class="img-fluid"><span>Manage Store</span></div>
            </a>
         </div>
         <?php } ?>


      </div>
      <div class="row">
         <h2 ng-if="UserTypeID!='79' && UserTypeID!='87'  && UserTypeID!='1'" class="text-left text-muted sec-head ml-3" style="color: #525252 !important;
            margin-top: 45px;
            float: left;
            width: 100%;">Quick Communication</h2>
      </div>
      <div class="row" ng-if="UserTypeID!='79' && UserTypeID!='87'">
         <?php if(in_array("Communications", array_column($this->Menu, 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 mb-3 rounded">
            <a href="<?php echo base_url(); ?>bulksms" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/mobile-messaging.png" ><span>Bulk SMS</span></div>
            </a>
         </div>
         <div class="col-md-3 col-sm-12 shadow-lg mb-3 rounded">
            <a href="<?php echo base_url(); ?>bulkemail" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/email.png" class="img-fluid"><span>Bulk Email</span></div>
            </a>
         </div>
         <?php } ?>
         <div class="col-md-3 col-sm-12 shadow-lg mb-3 rounded">
            <a href="" class="dashboard-links" ng-click="loadFormAttendance();" ng-if="(UserTypeID == 10 || UserTypeID != 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">
               <div class="panel-body" style="">
                  <img src="<?php echo base_url(); ?>asset/img/dashboard/group-class.png" class="img-fluid">
                  <span>Mark Attendance</span>
               </div>
            </a>
         </div>

         

         <div class="col-md-3 col-sm-12 shadow-lg mb-3 rounded">
            <a href="fee/collection" class="dashboard-links"  ng-if="(UserTypeID == 10 || UserTypeID != 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">
               <div class="panel-body" style="">
                  <img src="<?php echo base_url(); ?>asset/img/dashboard/money-bag.svg" class="img-fluid">
                  <span>Fee Collection</span>
               </div>
            </a>
         </div>
      </div>
      <div class="row">
         <h2 ng-if="UserTypeID!='79' && UserTypeID!='87' && UserTypeID!='1'" class="text-left text-muted sec-head ml-3" style="color: #525252 !important;
            margin-top: 45px;
            float: left;
            width: 100%;">Quick Connect</h2>
      </div>
      <div class="row" ng-if="UserTypeID!='79' && UserTypeID!='87'">
         <?php
            $quick_connect = array_column($this->Menu, 'ChildMenu');  
            if(in_array("News", array_column($quick_connect[0], 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 shadow-lg rounded mb-3">
            <a href="<?php echo base_url(); ?>news" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/news-headline.png" class="img-fluid"><span>News</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Events", array_column($quick_connect[0], 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 shadow-lg rounded mb-3">
            <a href="<?php echo base_url(); ?>event" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/dancing.png" class="img-fluid"><span>Events</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Picture Gallery", array_column($quick_connect[0], 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 shadow-lg rounded mb-3">
            <a href="<?php echo base_url(); ?>gallery" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/photography.png" class="img-fluid"><span>Gallery</span></div>
            </a>
         </div>
         <?php } ?>
         <?php if(in_array("Wall of Fame", array_column($quick_connect[0], 'ControlName'))){ ?>
         <div class="col-md-3 col-sm-12 shadow-lg rounded mb-3">
            <a href="<?php echo base_url(); ?>walloffame" class="dashboard-links">
               <div class="panel-body" style=""><img src="<?php echo base_url(); ?>asset/img/dashboard/reward.png" class="img-fluid"><span>Wall of Fame</span></div>
            </a>
         </div>
         <?php } ?>
      </div>
   </div>
   <?php 
    if($this->session->userdata('UserData')['UserTypeID'] != 1){ ?>
   <div class="modal fade" id="attendance_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Mark Attendance</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div ng-include="templateURLAttendance">
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="feecollection_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Fee Collection</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div ng-include="templateURLFeeCollection">
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
</div>
<!-- Body/ -->


<div class="modal fade" id="birthday_model">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title h5">Today's Birthday ({{data.birthDaysList.length}})</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <div>
            <table class="table table-striped table-condensed table-hover table-sortable">
            <thead>
               <tr>
                  <th style="max-width: 70px;">#</th>
                  <th style="max-width: 150px;">Name</th>
                  <th style="max-width: 70px;">Role</th>
                  <th style="max-width: 70px;">Email</th>
                  <th style="max-width: 70px;">Mobile</th>
                  <th style="max-width: 70px;">Location</th>
               </tr>
            </thead>
            <tbody>
               <tr scope="row" ng-repeat="(key, row) in data.birthDaysList">
                  <td><img src="{{row.ProfilePic}}"></td>
                  <td>{{row.FirstName}} {{row.LastName}}</td>
                  <td>{{row.UserTypeName}}</td>
                  <td>{{row.EmailForChange}}</td>
                  <td>{{row.PhoneNumberForChange}}</td>
                  <td>{{row.CityName}}, {{row.StateName}}</td>
               </tr>

               <tr ng-if="data.birthDaysLoading">
                  <td colspan="6" align="center">
                     <img src="./asset/img/ajax-loader.gif" ng-if="data.birthDaysLoading">
                  </td>
               </tr>   
            </tbody>
            </table>

         </div>
      </div>
   </div>
</div>


</div>
</div>