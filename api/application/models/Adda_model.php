<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adda_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	function get_timeago( $datetime )
	{
	    $now = new DateTime;
	    $ago = new DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}


	function is_image($path)
	{
		$a = getimagesize($path);
		$image_type = $a[2];
		
		if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
		{
			return true;
		}
		return false;
	}

	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getPosts($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));

		if(!empty($Where['PostGUID'])){
			$this->db->select('PostID');
			$this->db->from('tbl_adda');
			$this->db->where('PostGUID',$Where['PostGUID']);
			$this->db->limit(1);
			$que = $this->db->get();
			//echo $this->db->last_query(); die;
			if($que->num_rows() > 0){
				$addID = $que->result_array();
				$ParentPostID = $addID[0]['PostID'];
			}else{
				$ParentPostID = "";
			}
		}

		/* Define variables - ends */
		$this->db->select('tbl_adda.*,CONCAT_WS(" ",U.FirstName,U.LastName) UserName, IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic');
		$this->db->from('tbl_adda');
		$this->db->join('tbl_users as U',"U.UserID = tbl_adda.UserID");
		if($Where['PostType'] == "Post" && !empty($Where['PostGUID'])){
			$this->db->where("PostGUID",$Where['PostGUID']);
		}
		else if(!empty($Where['PostGUID'])){
			$this->db->where("ParentPostID",$ParentPostID);
			$this->db->where("PostType","Reply");
		}else{
			$this->db->where("PostType",$Where['PostType']);
		}

		if(!empty($Where['Type']) && $Where['Type'] == "MyPost" && !empty($EntityID)){
			if(empty($Where['PostGUID'])){
				$this->db->where("PostID IN (select PostID from tbl_adda where UserID = '".$EntityID."' AND PostType = 'Post' UNION ALL select `ParentPostID` from tbl_adda where UserID = '".$EntityID."' AND PostType = 'Reply')");
				//$this->db->where("PostID = (select PostID from tbl_adda where PostType != 'Reply' and UserID = '".$EntityID."')");
			}else{
				$this->db->where("U.UserID",$EntityID);
			}
		}


		if(!empty($Where['Keyword'])){ /*search in post content*/
			$this->db->group_start();
			// $this->db->where('MATCH (P.QueryContent) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			// $this->db->or_where('MATCH (P.QueryCaption) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			$this->db->like("PostContent", $Where['Keyword']);
			$this->db->or_like("U.FirstName", $Where['Keyword']);
			$this->db->or_like("U.LastName", $Where['Keyword']);
			$this->db->or_like("EntryDate", $Where['Keyword']);
			$this->db->or_like("CONCAT_WS('',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->or_like("CONCAT_WS(' ',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->group_end();
		}

		$this->db->order_by('PostID','DESC');

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		$ParentPost = 0;

		//echo $ParentPostID; //die;
		//echo $this->db->last_query();
		//echo "<pre>"; print_r($Query->result_array()); die;
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record)
			{
				//print_r($Record);
				//echo $Where['PostType'];
				$this->db->select('PostID');
				$this->db->from('tbl_adda');
				if($Where['PostType'] == "Reply"){
					$this->db->where('ParentPostID',$ParentPostID);
				}else{
					$this->db->where('ParentPostID',$Record['PostID']);
				}	
				//$this->db->where('ParentPostID',$ParentPostID);
				$this->db->where('PostType','Reply');
				//$this->db->limit(1);
				$que = $this->db->get();
				
				if($que->num_rows() > 0){
					$Record['Answered'] = 1;
					$Record['Replies'] = $que->num_rows();
				}else{
					$Record['Answered'] = 0;
					$Record['Replies'] = 0;
				}

				if(!empty($Record['LikedByUserID']))
				{
					$LikedByUserID = explode(",", $Record['LikedByUserID']);
					$Record['LikedByUserID'] = count($LikedByUserID);
					if(in_array($EntityID, $LikedByUserID))
					{
						$Record['LikedByMe'] = 1;
					}else{
						$Record['LikedByMe'] = 0;
					}
				}else{
					$Record['LikedByMe'] = 0;
					$Record['LikedByUserID'] = 0;
				}

				/*get attached media logo - starts*/
				if(!empty($Record['MediaID']) && $Record['MediaID'] != NULL){
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AskQuestion',"MediaID" => $Record['MediaID']),TRUE);
					$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
					$Record['is_image'] = $this->is_image($Record['MediaURL']);
					$Record['MediaExtension'] = pathinfo($Record['MediaURL'], PATHINFO_EXTENSION);
				}else{
					$Record['MediaThumbURL'] = "";
					$Record['MediaURL'] = "";
					$Record['is_image'] = "";
					$Record['MediaExtension'] = "";
				}

				
				/*get parent post if shared*/
				//$Record['ParentPost'] =''; /*define return variable*/
				if(!empty($Where['PostType']) && $Where['PostType'] == "Reply" && $ParentPost != 1){
                 $Record['ParentPost'] = $this->getPosts($EntityID,array("PostGUID"=>$Where['PostGUID'],"PostType"=>"Post"));	
                 $ParentPost = 1;
				}
				

				//print_r($Record);
				$Record['DaysAgo'] = $this->get_timeago($Record['EntryDate']);

				$Record['EntryDate'] = date("d-m-Y H:i:s", strtotime($Record['EntryDate']));

				unset($Record['QueryIDForUse']);
				unset($Record['ParentQueryIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			$Return['Data']['Reply'] = 1;
			return $Return;
		}else {
			if(!empty($Where['PostType']) && $Where['PostType'] == "Reply" && $ParentPost != 1){

              $Return['Data']['Records'][0]['ParentPost'] = $this->getPosts($EntityID,array("PostGUID"=>$Where['PostGUID'],"PostType"=>"Post"));
              $Return['Data']['Reply'] = 0;
             return $Return;

			}
		}
		return FALSE;		
	}


	/*
	Description: 	Use to get my add post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getMyAdda($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/* Define section  */
		//echo " AND ( p.PostContent = '$Where['Keyword']' OR U.FirstName = '$Where['Keyword']'' OR U.LastName = '$Where['Keyword']' OR U.EntryDate = '$Where['Keyword']' OR CONCAT_WS('',U.FirstName,U.LastName) = preg_replace('/\s+/', '', '$Where['Keyword']') OR CONCAT_WS(' ',U.FirstName,U.LastName) = preg_replace('/\s+/', '', '$Where['Keyword']') ";
		$Return = array('Data' => array('Records' => array()));		
		$append1 = "";
		if(!empty($Where['Keyword'])){
			//$append1 .= " AND ( p.PostContent = '$Where['Keyword']' OR U.FirstName = '$Where['Keyword']'' OR U.LastName = '$Where['Keyword']' OR U.EntryDate = '$Where['Keyword']' OR CONCAT_WS('',U.FirstName,U.LastName) = preg_replace('/\s+/', '', '$Where['Keyword']') OR CONCAT_WS(' ',U.FirstName,U.LastName) = preg_replace('/\s+/', '', '$Where['Keyword']') ";
		}

		echo $sql = "
		SELECT p.*,CONCAT_WS(' ',U.FirstName,U.LastName) UserName, IF(U.ProfilePic IS NULL,CONCAT('".BASE_URL."','uploads/profile/picture/','default.jpg'),CONCAT('".BASE_URL."','uploads/profile/picture/',U.ProfilePic)) AS ProfilePic
		FROM `tbl_adda` p 
		JOIN `tbl_users` U ON U.UserID = p.UserID
		WHERE p.UserID = 3942 AND p.PostType = 'Post' AND p.ParentPostID = 0 $append1

		UNION ALL

		SELECT p.*,CONCAT_WS(' ',U.FirstName,U.LastName) UserName, IF(U.ProfilePic IS NULL,CONCAT('".BASE_URL."','uploads/profile/picture/','default.jpg'),CONCAT('".BASE_URL."','uploads/profile/picture/',U.ProfilePic)) AS ProfilePic
		FROM `tbl_adda` p 
		JOIN `tbl_users` U ON U.UserID = p.UserID
		WHERE p.PostID IN(SELECT p1.ParentPostID FROM `tbl_adda` p1 WHERE p1.UserID = 3942 AND p1.PostType = 'Reply' AND p1.ParentPostID > 0) $append1";	

		die;	

		$Query = $this->db->query($sql);  //echo $this->db->last_query();
		
		$arr = array();

		
		/* Define variables - ends */
		$this->db->select('tbl_adda.*,CONCAT_WS(" ",U.FirstName,U.LastName) UserName, IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic');
		$this->db->from('tbl_adda');
		$this->db->join('tbl_users as U',"U.UserID = tbl_adda.UserID");
		if($Where['PostType'] == "Post" && !empty($Where['PostGUID'])){
			$this->db->where("PostGUID",$Where['PostGUID']);
		}
		else if(!empty($Where['PostGUID'])){
			$this->db->where("ParentPostID",$ParentPostID);
			$this->db->where("PostType","Reply");
		}else{
			$this->db->where("PostType",$Where['PostType']);
		}

		if(!empty($Where['Type']) && $Where['Type'] == "MyPost" && !empty($EntityID)){
			$this->db->where("U.UserID",$EntityID);
			if(empty($Where['PostGUID'])){
				$this->db->where("PostID = (select ParentPostID from tbl_adda where PostType = 'Reply' and UserID = '".$EntityID."')");
			}
		}


		if(!empty($Where['Keyword'])){ /*search in post content*/
			$this->db->group_start();
			// $this->db->where('MATCH (P.QueryContent) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			// $this->db->or_where('MATCH (P.QueryCaption) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			$this->db->like("PostContent", $Where['Keyword']);
			$this->db->or_like("U.FirstName", $Where['Keyword']);
			$this->db->or_like("U.LastName", $Where['Keyword']);
			$this->db->or_like("EntryDate", $Where['Keyword']);
			$this->db->or_like("CONCAT_WS('',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->or_like("CONCAT_WS(' ',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->group_end();
		}

		$this->db->order_by('PostID','DESC');

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		$ParentPost = 0;

		//echo $ParentPostID; //die;
		//echo $this->db->last_query();
		//echo "<pre>"; print_r($Query->result_array()); die;
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record)
			{
				//print_r($Record);
				//echo $Where['PostType'];
				$this->db->select('PostID');
				$this->db->from('tbl_adda');
				if($Where['PostType'] == "Reply"){
					$this->db->where('ParentPostID',$ParentPostID);
				}else{
					$this->db->where('ParentPostID',$Record['PostID']);
				}	
				//$this->db->where('ParentPostID',$ParentPostID);
				$this->db->where('PostType','Reply');
				//$this->db->limit(1);
				$que = $this->db->get();
				
				if($que->num_rows() > 0){
					$Record['Answered'] = 1;
					$Record['Replies'] = $que->num_rows();
				}else{
					$Record['Answered'] = 0;
					$Record['Replies'] = 0;
				}

				if(!empty($Record['LikedByUserID']))
				{
					$LikedByUserID = explode(",", $Record['LikedByUserID']);
					$Record['LikedByUserID'] = count($LikedByUserID);
					if(in_array($EntityID, $LikedByUserID))
					{
						$Record['LikedByMe'] = 1;
					}else{
						$Record['LikedByMe'] = 0;
					}
				}else{
					$Record['LikedByMe'] = 0;
					$Record['LikedByUserID'] = 0;
				}

				/*get attached media logo - starts*/
				if(!empty($Record['MediaID']) && $Record['MediaID'] != NULL)
				{
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AskQuestion',"MediaID" => $Record['MediaID']),TRUE);
					$MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
					$Record['is_image'] = $this->is_image($Record['MediaURL']);
					$Record['MediaExtension'] = pathinfo($Record['MediaURL'], PATHINFO_EXTENSION);
				}else{
					$Record['MediaThumbURL'] = "";
					$Record['MediaURL'] = "";
					$Record['is_image'] = "";
					$Record['MediaExtension'] = "";
				}

				
				/*get parent post if shared*/
				//$Record['ParentPost'] =''; /*define return variable*/
				if(!empty($Where['PostType']) && $Where['PostType'] == "Reply" && $ParentPost != 1){
                 $Record['ParentPost'] = $this->getPosts($EntityID,array("PostGUID"=>$Where['PostGUID'],"PostType"=>"Post"));	
                 $ParentPost = 1;
				}
				

				//print_r($Record);
				$Record['DaysAgo'] = $this->get_timeago($Record['EntryDate']);

				$Record['EntryDate'] = date("d-m-Y H:i:s", strtotime($Record['EntryDate']));

				unset($Record['QueryIDForUse']);
				unset($Record['ParentQueryIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			$Return['Data']['Reply'] = 1;
			return $Return;
		}else {
			if(!empty($Where['PostType']) && $Where['PostType'] == "Reply" && $ParentPost != 1){

              $Return['Data']['Records'][0]['ParentPost'] = $this->getPosts($EntityID,array("PostGUID"=>$Where['PostGUID'],"PostType"=>"Post"));
              $Return['Data']['Reply'] = 0;
             return $Return;

			}
		}
		return FALSE;		
	}

	/*
	Description: 	Use to add new post
	*/
	function addQuery($EntityID, $Input=array()){
		$this->db->trans_start();
		$PostGUID = get_guid();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($Input['MediaGUID'])){
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();
			//echo $this->db->last_query(); die;
			if($que->num_rows() > 0){
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];

				$arr = array("InstituteID"=>$InstituteID);

				$this->db->where("MediaID",$MediaID);
				$this->db->update("tbl_media",$arr);
			}else{
				$MediaID = "";
			}

		}else{
			$MediaID = "";
		}

		if(!empty($Input['PostGUID'])){
			$this->db->select('PostID');
			$this->db->from('tbl_adda');
			$this->db->where('PostGUID',$Input['PostGUID']);
			$this->db->limit(1);
			$que = $this->db->get();
			//echo $this->db->last_query(); die;
			if($que->num_rows() > 0){
				$addID = $que->result_array();
				$ParentPostID = $addID[0]['PostID'];
			}else{
				$ParentPostID = "";
			}
		}

		/* Add post */
		$InsertData = array_filter(array(
			"PostGUID" 		=>	$PostGUID,			
			"ParentPostID" 	=>	@$ParentPostID,
			"UserID" 		=>	$EntityID,
			"PostContent" 	=>	$Input["PostContent"],
			"PostType"  => @$Input["PostType"],
			"MediaID"  => @$MediaID,
			"EntryDate"  => date("Y-m-d H:i:s")
		));
		$this->db->insert('tbl_adda', $InsertData);
		//echo $this->db->last_query();		die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('PostGUID' => $PostGUID);
	}


	function editQuery($EntityID, $ToEntityID, $QueryID, $Input=array()){
		$this->db->trans_start();
		$QueryGUID = get_guid();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($Input['MediaGUID'])){
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();
			//echo $this->db->last_query(); die;
			if($que->num_rows() > 0){
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];

				$arr = array("InstituteID"=>$InstituteID);

				$this->db->where("MediaID",$MediaID);
				$this->db->update("tbl_media",$arr);
			}else{
				$MediaID = "";
			}

		}else{
			$MediaID = "";
		}

		/* Add post */
		$UpdateData = array_filter(array(
			"EntityID" 		=>	$EntityID,
			"ToEntityID" 	=> 	$ToEntityID,
			"QueryContent" 	=>	$Input["QueryContent"],
			"QueryCaption" 	=>	@$Input["QueryCaption"],
			"CourseID"  => @$Input["CourseID"],
			"MediaID"  => @$MediaID,
			"EntryDate"  => date("Y-m-d H:i:s")
		));
		$this->db->Where("QueryID", $QueryID);
		$this->db->update('tbl_ask_a_question', $UpdateData);
		//echo $this->db->last_query();		die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('QueryID' => $QueryID);
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deleteQuery($UserID, $QueryID){
		//echo $QueryID; die;
		// $PostData = $this->Post_model->getPosts('P.QueryID',array('QueryID'=>$QueryID, 'SessionUserID'=>$UserID));
		// print_r($PostData);
		// if(!empty($PostData) && $UserID==$PostData['EntityID']){
		// 	$this->Entity_model->deleteEntity($PostID);
			$this->db->Where("QueryID", $QueryID);
			$this->db->delete("tbl_ask_a_question");
			return TRUE;
		// }
		// return FALSE;
	}


	function setLikeDislike($EntityID, $PostGUID, $Liked)
	{
		if($Liked == 0)
		{			
			$sql = $this->db->query("UPDATE  tbl_adda SET  LikedByUserID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', LikedByUserID, ','), ',".$EntityID.",', ','))	WHERE FIND_IN_SET('".$EntityID."',LikedByUserID) AND  `PostGUID` = '".$PostGUID."'");

		}
		else
		{
			$sql = "SELECT LikedByUserID FROM tbl_adda WHERE `PostGUID` = '".$PostGUID."' AND ( LikedByUserID IS NOT NULL AND LikedByUserID != '' )";

			$data = $this->db->query($sql);
			
			if($data->num_rows() > 0)
			{
				$sql = $this->db->query("UPDATE tbl_adda SET `LikedByUserID` = CONCAT(`LikedByUserID`, ',".$EntityID."') WHERE `PostGUID` = '".$PostGUID."'");
			}
			else
			{
				$sql = $this->db->query("UPDATE tbl_adda SET `LikedByUserID` = ".$EntityID." WHERE `PostGUID` = '".$PostGUID."'");
			}			
		}


		$this->db->select("LikedByUserID");
		$this->db->from('tbl_adda');
		$this->db->where('PostGUID',$PostGUID);
		$this->db->where('LikedByUserID != ""');
		$this->db->limit(1);
		$Query = $this->db->get();		
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$Records = $Record;
			}

			if(isset($Records['LikedByUserID']) && !empty($Records['LikedByUserID']))
			{
				$arr = explode(",", $Records['LikedByUserID']);

				return count($arr);
			}
		}		

		return 0;
	}
}