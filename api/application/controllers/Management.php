<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Management_model');
	}




	/*
	Description: 	Use to get Get category.
	URL: 			/api/Management/getCategories
	Input (Sample JSON): 		
	*/
	public function getCategories_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('CategoryTypeID', 'CategoryTypeID', 'trim');
		$this->form_validation->set_rules('ParentCategoryID','ParentCategoryID','trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$CategoryData = $this->Management_model->getCategories($this->EntityID,
			array("ParentCategoryID"=>$this->input->post('ParentCategoryID'), "CategoryTypeID" => $this->input->post('CategoryTypeID')));

		//print_r($CategoryData); die;

		if(!empty($CategoryData)){
			$this->Return['Data'] = $CategoryData['Data'];
		}else{
			$this->Return['Message'] = "Data not available";		
		}	
	}



	/*

	Description: 	use to get list of faculty

	URL: 			/api/management/getFacultyByBatch

	*/

	public function getFacultyByBatch_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

		$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */


		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);


		$facultyData = $this->Management_model->getFacultyByBatch($this->EntityID,$this->input->post());


		if($facultyData){

			$this->Return['Data'] = $facultyData['Data'];	

		}else{
			$this->Return['Message'] = "Data not found";
		}

	}


	/*

	Description: 	use to get list 

	URL: 			/api/management/getList	

	*/

	public function getList_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('Type', 'Type', 'trim|required');

		if($this->input->post('Type') == "BatchList"){
			$this->form_validation->set_rules('StreamID', 'StreamID', 'trim');

			$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

			$this->form_validation->set_rules('SubjectID', 'SubjectID', 'trim');

			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

			$this->form_validation->set_rules('FacultyID', 'FacultyID', 'trim');

		}else if($this->input->post('Type') == "StudentCount"){

			$this->form_validation->set_rules('StreamID', 'StreamID', 'trim|required');

			$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

		}else if($this->input->post('Type') == "StudentList"){

			$this->form_validation->set_rules('StreamID', 'StreamID', 'trim');

			$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

		}else if($this->input->post('Type') == "AssignKeysList"){

			$this->form_validation->set_rules('StreamID', 'StreamID', 'trim');

			$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

			$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');

			$this->form_validation->set_rules('Validity', 'Validity', 'trim');
			
		}
		else if($this->input->post('Type') == "EnquiryList")
		{
			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');			
		}
		else if($this->input->post('Type') == "AAQStudentList")
		{
			$this->form_validation->set_rules('StreamID', 'StreamID', 'trim');

			$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		}
		else if($this->input->post('Type') == "AAQStudentQuestionList")
		{
			$this->form_validation->set_rules('StudentID', 'StudentID', 'trim|required');

			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		}
		else if($this->input->post('Type') == "AAQFacultyReplyList")
		{			
			$this->form_validation->set_rules('QuestionID', 'QuestionID', 'trim|required');

			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		}
		else if($this->input->post('Type') == "Annoucement")
		{
			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

			$this->form_validation->set_rules('FacultyID', 'FacultyID', 'trim');

			$this->form_validation->set_rules('startDate', 'startDate', 'trim');

			$this->form_validation->set_rules('endDate', 'endDate', 'trim');

			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		}
		else if($this->input->post('Type') == "AnnoucementDetail")
		{
			$this->form_validation->set_rules('AnnoucementID', 'AnnoucementID', 'trim|required');
			
			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		}
		else if($this->input->post('Type') == "FeeCollection")
		{
			$this->form_validation->set_rules('StreamID', 'StreamID', 'trim');

			$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

			$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

			$this->form_validation->set_rules('startDate', 'startDate', 'trim');

			$this->form_validation->set_rules('endDate', 'endDate', 'trim');

			$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

			$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

			$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		}

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('Type') == "BatchList"){

			$batchData = $this->Management_model->getInstituteBatches($this->EntityID, $this->input->post());
			if($batchData){

				$this->Return['Data'] = $batchData['Data'];	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}else if($this->input->post('Type') == "StudentCount"){

			$CoursesWithStudent = $this->Management_model->getCoursesWithStudentCount($this->EntityID,$this->input->post());


			if($CoursesWithStudent){

				$this->Return['Data'] = $CoursesWithStudent['Data'];	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "StudentList")
		{

			$Students = $this->Management_model->getInstituteStudents($this->EntityID,$this->input->post());


			if($Students){

				$this->Return['Data'] = $Students;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "TimeScheduleList"){

			$Schedule = $this->Management_model->getTimeScheduling($this->EntityID,$this->input->post());
			

			if($Schedule){

				$this->Return['Data'] = $Schedule;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}else if($this->input->post('Type') == "KeysInventory"){

			$Inventory = $this->Management_model->getKeysInventory($this->EntityID,$this->input->post());
			

			if($Inventory){

				$this->Return['Data'] = $Inventory;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}else if($this->input->post('Type') == "AssignKeysDetail"){

			$Inventory = $this->Management_model->AssignKeysDetail($this->EntityID,$this->input->post(),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
			

			if($Inventory){
				$this->Return['Data'] = $Inventory;	
			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "EnquiryFilters")
		{
			$listing = array('assignedUsers' => array(), 'interestedIn' => array());
			
			$this->load->model('Enquiry_model');
			$listing['assignedUsers'] =  $this->Enquiry_model->getAssignedUsers();
			$listing['interestedIn'] =  $this->Enquiry_model->getInterestedIn();

			if($listing)
			{
				$this->Return['Data'] = $listing;	
			}
			else
			{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "EnquiryList")
		{			
			$EnquiryList = $this->Management_model->getEnquiryList($this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);			

			if($EnquiryList)
			{
				$this->Return['Data'] = $EnquiryList;	
			}
			else
			{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "AAQStudentList")
		{
			$Students = $this->Management_model->AAQStudentList($this->EntityID,$this->input->post());

			if($Students){

				$this->Return['Data'] = $Students;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "AAQStudentQuestionList")
		{
			$Questions = $this->Management_model->AAQStudentQuestionList($this->EntityID,$this->input->post());

			if($Questions){

				$this->Return['Data'] = $Questions;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "AAQFacultyReplyList")
		{
			$Replies = $this->Management_model->AAQFacultyReplyList($this->EntityID,$this->input->post());

			if($Replies){

				$this->Return['Data'] = $Replies;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "Annoucement")
		{
			$Data = $this->Management_model->AnnoucementList($this->EntityID,$this->input->post());

			if($Data){

				$this->Return['Data'] = $Data;	

			}else{

				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "AnnoucementDetail")
		{
			$Data = $this->Management_model->AnnoucementDetail($this->EntityID,$this->input->post());

			if($Data){

				$this->Return['Data'] = $Data;	

			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
		else if($this->input->post('Type') == "FeeCollection")
		{
			$FeeCollection = $this->Management_model->getFeeCollections($this->EntityID, $this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);			

			if(!empty($FeeCollection)){
				$this->Return['Data'] = $FeeCollection;
			}else{
				$this->Return['Message'] = "Data not found";
			}
		}
	}
}