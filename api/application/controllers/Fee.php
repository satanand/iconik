<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fee extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Store_model');
		$this->load->model('Fee_model');
		$this->load->model('Entity_model');
		$this->load->model('Common_model');
	}


	public function addFee_post()
	{
		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 
			'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('ParentCategoryGUID', 'Select Stream', 'trim|required');

		$this->form_validation->set_rules('ParentCategoryGUID', 'Stream', 'trim|required|callback_validateEntityGUID[Category,ParentCategoryID]');

		$this->form_validation->set_rules('CategoryGUID', 'Course', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('duration', 'Duration (In Months)', 'trim|required');
		$this->form_validation->set_rules('desc', 'Description', 'trim');
		

		$this->form_validation->validation($this);  /* Run validation */		
		
		if(!isset($this-> Post["hdn"]) || empty($this-> Post["hdn"]))
        {
            $this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Please define Fee Breakup first.";
            die;
        }
        else 
        {
            $chkbox = $chkamt = 0;
            foreach($this-> Post["hdn"] as $fbid)
            {
                if(isset($this-> Post["fb_chk_".$fbid]) && $this-> Post["fb_chk_".$fbid] == 'on')
                {
                    ++$chkbox;
                    if(isset($this-> Post["fb_amt_".$fbid]) && !empty($this-> Post["fb_amt_".$fbid]))
                    {                        
                                              
                    }
                    else
                    {
                    	$this->Return['ResponseCode']     =    500;
			            $this->Return['Message']          =    "Please enter amount for selected Fee Breakup.";
			            die;
                    }
                }
            } 

            if($chkbox <= 0)
            {
            	$this->Return['ResponseCode']     =    500;
	            $this->Return['Message']          =    "Please select atleast one Fee Breakup.";
	            die;
            }	         
        }

        $this->form_validation->set_rules('single_fee', 'Course Fee', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('discount', 'Discount(%)', 'trim');
		$this->form_validation->set_rules('discount_amt', 'Discount Amount', 'trim');
		$this->form_validation->set_rules('final_amt', 'Final Fee', 'trim|required|greater_than[0]');
		$this->form_validation->validation($this);		

 
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID :$this->SessionUserID);
		/* Define section - ends */ 
		$PostData = $this->Fee_model->addFee($this->EntityID, array(
			"StreamID"	=>	$this->ParentCategoryID,
			//"StreamID"	=>	@$_POST['CategoryGUID'],
			"CourseID"	=>	$this->CategoryID,
			"NoMonth"	=>	$_POST['duration'],
			"Description"	=>$_POST['desc'],
			"FullAmount"	=>$_POST['single_fee'],
			"FullDiscountFee"=>$_POST['final_amt'],
			"Discount"		=>	@$_POST['discount'],
			"DiscountAmount" =>	@$_POST['discount_amt'],
			"NoOfInstallment"=>@$_POST['installment'],
			"Markup"		=>	@$_POST['markup'],
			"TotalFee" =>	 @$_POST['total_fees'],
			"InstallmentAmount" =>	@$_POST['installment_amt']), $this-> Post);

		if($PostData){
			if($PostData == 'Exist'){
				$this->Return['ResponseCode']     =    500;
                $this->Return['Message']          =    "This Course is already exist.";
			}else{
				$this->Return['Data']['PostGUID'] = $PostData['PostGUID'];
				$this->Return['Message']          =    "Fee added successfully.";
			}
		}
	}


	

	public function editFee_post()
	{
		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('ParentCategoryGUID', 'ParentCategoryGUID', 'trim|callback_validateEntityGUID[Category,ParentCategoryID]');

		$this->form_validation->set_rules('FeeGUID', 'FeeGUID','trim|required|callback_validateEntityGUID[Course fee,FeeID]');

		$this->form_validation->set_rules('duration', 'Duration (In Months)', 'trim|required');
		/*$this->form_validation->set_rules('desc', 'Description', 'trim');
		$this->form_validation->set_rules('single_fee', 'Course Fee', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('discount', 'Discount(%)', 'trim');
		$this->form_validation->set_rules('discount_amt', 'Discount Amount', 'trim');
		$this->form_validation->set_rules('final_amt', 'Final Fee', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('ChangeStudentFee', 'ChangeStudentFee', 'trim|required');*/

		$this->form_validation->validation($this);  /* Run validation */		
		
		if(!isset($this-> Post["hdn"]) || empty($this-> Post["hdn"]))
        {
            $this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Please define Fee Breakup first.";
            die;
        }
        else 
        {
            $chkbox = $chkamt = 0;
            foreach($this-> Post["hdn"] as $fbid)
            {
                if(isset($this-> Post["fb_chk_".$fbid]) && $this-> Post["fb_chk_".$fbid] == 'on')
                {
                    ++$chkbox;
                    if(isset($this-> Post["fb_amt_".$fbid]) && !empty($this-> Post["fb_amt_".$fbid]))
                    {                        
                                              
                    }
                    else
                    {
                    	$this->Return['ResponseCode']     =    500;
			            $this->Return['Message']          =    "Please enter amount for selected Fee Breakup.";
			            die;
                    }
                }
            } 

            if($chkbox <= 0)
            {
            	$this->Return['ResponseCode']     =    500;
	            $this->Return['Message']          =    "Please select atleast one Fee Breakup.";
	            die;
            }	         
        }

        $this->form_validation->set_rules('desc', 'Description', 'trim');
		$this->form_validation->set_rules('single_fee', 'Course Fee', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('discount', 'Discount(%)', 'trim');
		$this->form_validation->set_rules('discount_amt', 'Discount Amount', 'trim');
		$this->form_validation->set_rules('final_amt', 'Final Fee', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('ChangeStudentFee', 'ChangeStudentFee', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */



		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID :$this->SessionUserID);
		/* Define section - ends */ 
		//echo $this->FeeID; die;
		if(!empty($this->input->post('ChangeStudentFee')) && $this->input->post('ChangeStudentFee') == "No"){
			$PostData = $this->Fee_model->addFee($this->EntityID, array(
				"StreamID"	=>	$this->ParentCategoryID,
				"CourseID"	=>@$this->input->post('CourseID'),
				"NoMonth"	=>	$_POST['duration'],
				"Description"	=>$_POST['desc'],
				"FullAmount"	=>$_POST['single_fee'],
				"FullDiscountFee"=>$_POST['final_amt'],
				"Discount"		=>	@$_POST['discount'],
				"DiscountAmount" =>	@$_POST['discount_amt'],
				"NoOfInstallment"=>@$_POST['installment'],
				"Markup"		=>	@$_POST['markup'],
				"TotalFee" =>	 @$_POST['total_fees'],
				"InstallmentAmount" =>	@$_POST['installment_amt']				
			), $this-> Post);


		}else{
			$PostData = $this->Fee_model->editFee($this->EntityID, array(
				"FeeID"	=>$this->FeeID,
				"CourseID"	=>@$this->input->post('CourseID'),
				"NoMonth"	=>	@$_POST['duration'],
				"Description"	=>@$_POST['desc'],
				"FullAmount"	=>$_POST['single_fee'],
				"FullDiscountFee"=>$_POST['final_amt'],
				"Discount"	=>	@$_POST['discount'],
				"DiscountAmount" =>	@$_POST['discount_amt'],
				"NoOfInstallment"=>@$_POST['installment'],
				"Markup"		=>	@$_POST['markup'],
				"TotalFee" =>	@$_POST['total_fees'],
				"InstallmentAmount" =>	@$_POST['installment_amt']
			), $this-> Post);
		}
		

		if($PostData){
			$this->Return['Data']['FeeGUID'] = $this->input->post('FeeGUID');
			$this->Return['Message']          =    "Fee updated successfully.";
		}	
	}


	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/getPosts
	*/
	public function getFee_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		
		
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('ParentCategoryGUID', 'ParentCategoryGUID', 'trim');
		
		
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID :$this->SessionUserID);

		//if(!empty($this->input->post('CategoryGUID'))){
			$FeeCategoryData = $this->Fee_model->getFee($this->EntityID, array("ParentCategoryGUID"=>$this->input->post('ParentCategoryGUID'),"CategoryID"=>@$this->CategoryID), FALSE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		// }else{
		// 	$FeeCategoryData = $this->Fee_model->getFee($this->EntityID, array("ParentCategoryID"=>@$this->CategoryID), FALSE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		// }
		

		//print_r($FeeCategoryData); die;
		if(!empty($FeeCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $FeeCategoryData;
		}	
	}


	public function getFeeData_post()
	{

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FeeGUID', 'FeeGUID','trim|required|callback_validateEntityGUID[Course fee,FeeID]');
		$this->form_validation->validation($this);
		//echo $_POST['FeeGUID'];
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID :$this->SessionUserID);
		$FeeCategoryData = $this->Fee_model->getFees($this->EntityID,$this->input->Post('FeeGUID'));

		if(!empty($FeeCategoryData))
		{				
			$this->Return['Data'] = $FeeCategoryData['Data'];
		}	
	}


	public function deleteInstallment_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$InstallmentID = array('InstallmentID' =>$_POST['InstallmentID']);
		if(!$this->Fee_model->deleteInstallment('tbl_setting_installment', $InstallmentID)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}

	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('FeeGUID', 'FeeGUID','trim|required|callback_validateEntityGUID[Course fee,FeeID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$data=$this->Fee_model->delete($this->FeeID);

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
		
	}

	public function Installmentremove_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('InstallmentID', 'InstallmentID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$data=$this->Fee_model->Installmentremove($this->input->Post('InstallmentID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Remove"; 
		}
		
	}


	/*
	Description: 	Use to get Get single Duration.
	URL: 			/api/fee/getDuration
	
	*/
	public function getDuration_post()
	{   
		//echo $this->input->Post('CategoryGUID'); die();
		$Durationdata = $this->Common_model->getDuration($this->input->Post('CategoryGUID'));
		
		if(!empty($Durationdata)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $Durationdata;
		}	
	}	



	public function addFeeBreakup_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		
		$this->form_validation->set_rules('fee_head_name', 'Breakup Name', 'trim|required');
		
		if(isset($this-> Post['fee_is_taxable']) && $this-> Post['fee_is_taxable'] == 1)
		{
			$this->form_validation->set_rules('fee_taxable_percent', 'Taxable Percentage', 'trim|required');
		}

		if(isset($this-> Post['fee_is_refundable']) && $this-> Post['fee_is_refundable'] == 1)
		{
			$this->form_validation->set_rules('fee_refundable_percent', 'Refundable Percentage', 'trim|required');
		}


		$this->form_validation->validation($this);		
				
		$PostData = $this->Fee_model->addFeeBreakup($this-> Post);

		if($PostData)
		{
			if($PostData == 'Exist')
			{
				$this->Return['ResponseCode']     =    500;
                $this->Return['Message']          =    "This Breakup Fee is already defined.";
			}
			else
			{						
				$this->Return['ResponseCode']     =    200;
				$this->Return['Message']          =    "Breakup added successfully.";
			}
		}
	}


	public function feeBreakupListing_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);		

		$Data = $this->Fee_model->feeBreakupListing();		
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}


	public function feeBreakupDelete_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('fid', 'Fee Breakup', 'trim|required');
		$this->form_validation->validation($this);		
		
		$res = $this->Fee_model->feeBreakupDelete($this->Post);

		if($res == 'NotExist')
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Selected Fee Breakup does not exist in the system."; 
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Message']      	=	"Successfully deleted."; 
		}
		
	}



	public function addBreakup_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		
		$this->form_validation->set_rules('breakup_head_name', 'Breakup Name', 'trim|required');
		
		$this->form_validation->validation($this);		
				
		$PostData = $this->Fee_model->addBreakup($this-> Post);

		if($PostData)
		{
			if($PostData == 'Exist')
			{
				$this->Return['ResponseCode']     =    500;
                $this->Return['Message']          =    "This Breakup Name is already exist.";
			}
			else
			{						
				$this->Return['ResponseCode']     =    200;
				$this->Return['Message']          =    "Breakup saved successfully.";
			}
		}
	}


	public function breakupListing_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);		

		$Data = $this->Fee_model->breakupListing();		
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}



	/*--------------------------------------------------------
	Before due date settings
	---------------------------------------------------------*/
	public function settingsBeforeDueDate_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		
		
		$this->form_validation->set_rules('bf_email_days', 'Select Days', 'trim|required');
		$this->form_validation->set_rules('bf_email_remind_days[]', 'Send Reminders On Days', 'trim|required');
		$this->form_validation->set_rules('bf_email_content', 'Email Content', 'trim|required');
		
		
		$this->form_validation->set_rules('bf_sms_days', 'Select Days For SMS', 'trim|required');
		$this->form_validation->set_rules('bf_sms_remind_days[]', 'Send Reminders On Days For SMS', 'trim|required');
		$this->form_validation->set_rules('bf_sms_content', 'SMS Content', 'trim|required');

		
		$this->form_validation->validation($this);	
		

		if(!isset($this-> Post["bf_email_remind_days"]) || empty($this-> Post["bf_email_remind_days"]))
        {
        	$this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Please select Send Reminders On Days for Email.";
            die;
        }

        if(!isset($this-> Post["bf_sms_remind_days"]) || empty($this-> Post["bf_sms_remind_days"]))
        {
        	$this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Please select Send Reminders On Days for SMS.";
            die;
        }	


		$PostData = $this->Fee_model->saveSettingsBeforeDueDate($this-> Post);

		if($PostData)
		{			
			$this->Return['ResponseCode']     =    200;
			$this->Return['Message']          =    "Setting saved successfully.";			
		}
		else
		{
			$this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Sorry! something went wrong.";
		}
	}


	public function getBeforeDueDateSetting_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);		

		$Data = $this->Fee_model->getBeforeDueDateSetting();		
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}


	/*--------------------------------------------------------
	After due date settings
	---------------------------------------------------------*/
	public function settingsAfterDueDate_post()
	{		
		$formID = $this-> Post["formID"];

		if($formID <= 0 || $formID >3)
		{
			$this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Sorry! Illegal form submission.";
            die;
		}

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		
		$this->form_validation->set_rules('formID', 'Illegal form submission.', 'trim|required');
		

		$this->form_validation->set_rules('af_email_days'.$formID, 'Select Days', 'trim|required');
		$this->form_validation->set_rules('af_email_remind_days'.$formID.'[]', 'Send Reminders On Days', 'trim|required');
		$this->form_validation->set_rules('af_email_content'.$formID, 'Email Content', 'trim|required');
		
		
		$this->form_validation->set_rules('af_sms_days'.$formID, 'Select Days For SMS', 'trim|required');
		$this->form_validation->set_rules('af_sms_remind_days'.$formID.'[]', 'Send Reminders On Days For SMS', 'trim|required');
		$this->form_validation->set_rules('af_sms_content'.$formID, 'SMS Content', 'trim|required');
		
		$this->form_validation->set_rules('af_alert_s'.$formID, 'Reminders To Student', 'trim|required');


		if($this-> Post["formID"] == 2)
		{			
			$this->form_validation->set_rules('af_alert_p2', 'Reminders To Parent', 'trim|required');
		}
		elseif($this-> Post["formID"] == 3)
		{			
			$this->form_validation->set_rules('af_alert_p3', 'Reminders To Parent', 'trim|required');
			$this->form_validation->set_rules('af_alert_f3', 'Reminders To Faculty', 'trim|required');			
		}
		
		$this->form_validation->validation($this);		

		$PostData = $this->Fee_model->saveSettingsAfterDueDate($this-> Post);

		if($PostData)
		{			
			$this->Return['ResponseCode']     =    200;
			$this->Return['Message']          =    "Setting saved successfully.";			
		}
		else
		{
			$this->Return['ResponseCode']     =    500;
            $this->Return['Message']          =    "Sorry! something went wrong.";
		}
	}


	public function getAfterDueDateSetting_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID','trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);		

		$Data = $this->Fee_model->getAfterDueDateSetting();		
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}

}
