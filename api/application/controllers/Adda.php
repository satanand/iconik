<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adda extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Adda_model');
		$this->load->model('Entity_model');
	}


	/*

	Name: 			add ask a question

	Description: 	Use to add ask a question

	URL: 			/api/Askquestion/add

	*/
	public function add_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		$this->form_validation->set_rules('PostType', 'Post Type', 'trim|required|in_list[Post,Reply]');	
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim');
		$this->form_validation->set_rules('PostContent', 'Input', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Adda_model->addQuery($this->EntityID, $this->input->post());

		if($KeysData){

			$this->Return['Message']      	=	"Saved successfully."; 
		}
	}



	/*

	Name: 			edit ask a question

	Description: 	Use to edit ask a question

	URL: 			/api/Askquestion/edit

	*/
	public function edit_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		$this->form_validation->set_rules('QueryType', 'QueryType', 'trim|required|in_list[Query,Reply]');	
		$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|required|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->set_rules('QueryContent', 'QueryContent', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->editQuery($this->EntityID, $this->UserID, $this->QueryID, $this->input->post());

		if($KeysData){

			$this->Return['Message']      	=	$this->input->post('QueryType')." updated successfully."; 
		}
	}


	/*

	Name: 			edit ask a question

	Description: 	Use to edit ask a question

	URL: 			/api/Askquestion/edit

	*/
	public function delete_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|required|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->deleteQuery($this->EntityID,$this->QueryID);

		if($KeysData){

			$this->Return['Message']      	=	"Query deleted successfully."; 
		}
	}



	/*

	Name: 			getPosts

	Description: 	Use to get list of post.

	URL: 			/api/post/getPosts

	*/

	public function getPosts_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim');

		$this->form_validation->set_rules('PostType', 'PostType', 'trim|in_list[Post,Reply]');

		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->set_rules('Type', 'Type', 'trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		// if(!empty($this->input->post('Type')) && $this->input->post('Type') == 'MyPost'){
		// 	$Posts = $this->Adda_model->getMyAdda($EntityID, $this->Post, TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		// }else{
			$Posts=$this->Adda_model->getPosts($EntityID, $this->Post, TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		//}

		

		//print_r($Posts);

		if($Posts)
		{
			$this->Return['Data'] = $Posts['Data'];
		}
		else
		{
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message']   =	""; 
			$this->Return['Data'] = [];
		}

	}


	public function setLikeDislike_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim|required');
		$this->form_validation->set_rules('Liked', 'Liked', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Status = $this->Adda_model->setLikeDislike($EntityID,$this->input->post('PostGUID'),$this->input->post('Liked'));

		if($Status >= 0)
		{
			$this->Return['Data']['LikedByUserID'] = $Status;
			$this->Return['Message']      	=	"Updated successfully."; 
		}else{
			$this->Return['ResponseCode']   =	500; 
		}

	}

}