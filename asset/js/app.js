var app = angular.module('myApp', ['infinite-scroll','ngSanitize']);

app.filter('capitalizeFirstLetter', function() {
    return function(input) {
      return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1) : input;
    }
});


var contentType = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};

/*main controller*/
app.controller('MainController', ["$scope", "$http", "$timeout","$location", function($scope, $http, $timeout, $location) {
    // testService.saveData().then(function(res){
    //     $scope.test = res.data; 
    //     console.log(res); 
    // });
   // console.log(currentModuleID); 

    $scope.data = {
        dataList: [],
        totalRecords: '0',
        pageNo: 1,
        pageSize: 25,
        noRecords: false,
        UserGUID: UserGUID,
        notificationCount: 0,
        OrderBy: '',
        Sequence: '',
        APIURL : API_URL,
        SecondLevelPermission : []
    };

    console.log(UserTypeID);

    $scope.UserTypeID = UserTypeID;

    $scope.APIURL = API_URL

    console.log($scope.data);
     

    //   UserService.saveData().then(function(res){
    //     $scope.ProfilePic = res.data;  
    //   });

    // console.log($scope.data.ProfilePic);

    $scope.orig = angular.copy($scope.data);
    $scope.UserTypeID = UserTypeID;
    $scope.file_title = "";
    $scope.CurrentUrl = $location.absUrl();
    console.log($scope.CurrentUrl);
    console.log($scope.CurrentUrl.indexOf('content'));


    $scope.exportToExcel=function(tableId){ // ex: '#my-table'
        $(tableId).table2excel({
            filename: "Table.xls"
        });
    }





    $scope.getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    $scope.getSecondLevelPermission = function() {
        $scope.data.SecondLevelPermission = []; 
        var data = 'SessionKey=' + SessionKey + '&ModuleID=' + currentModuleID;
        $http.post(API_URL + 'admin/roles/getSecondLevelPermission', data, contentType).then(function(response) {
            var response = response.data;
            console.log(response);
            if (response.ResponseCode == 200 && response.Data) { /* success case */
                // console.log(response.Data);
                if(response.Data.length){
                 $scope.data.SecondLevelPermission.push(response.Data[0].Action);
                }else{
                 $scope.data.SecondLevelPermission.push('all');                    
                }
                // for (var i in response.Data) {
                //     console.log(response.Data[i].Action);
                  
                //    $scope.data.SecondLevelPermission.push(response.Data[i].Action);
                //     console.log($scope.data.SecondLevelPermission);
                //   // console.log(response.Data[i].Action);
                //    return false;
                // }

                console.log($scope.data.SecondLevelPermission);
                
            }else{
               $scope.data.SecondLevelPermission = []; 
            }
        });
    }


    /*delete Entity*/
    $scope.deleteData = function(EntityGUID) {
        $scope.deleteDataLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&EntityGUID=' + EntityGUID;
            $http.post(API_URL + 'admin/entity/delete', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $timeout(function(){ 
                        $('.modal-header .close').click();
                       // location.reload();
                    });
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;

    }


    /*Delete media by media guid*/
    $scope.deleteMedia = function (MediaGUID)
    {
        $http.post(API_URL+'upload/delete', 'SessionKey='+SessionKey+'&MediaGUID='+MediaGUID, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                SuccessPopup(response.Message);
            }
        });
    }


    /*change password*/
    $scope.changePassword = function() {
        $scope.changeCP = true;
        var data = 'SessionKey=' + SessionKey + '&'+$('#changePassword_form').serialize();
        $http.post(API_URL + 'users/changePassword', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200 ) { /* success case */
                $('#changePassword_modal .close').click();
                SuccessPopup(response.Message);
            } else {
                ErrorPopup(response.Message);
            }
            $scope.changeCP = false;    
        });
    }

    /* display doc files in popup */
    $scope.FileInModalBox = function (base_url, url, title, PostGUID) {
        console.log(url);
        $scope.data.pageLoading = false;
        $('#view_model .close').click();
        $scope.file_title = title;
        $('#iframe-info').html('<embed src="'+url+'" style="width: 100%;height: 400px;">');
        $('#extension_popup').modal({
            show : true
        });
        $('#PostGUID').val(PostGUID);
    } 
   


     $scope.LibraryFileInModalBox = function (base_url, url, title, PostGUID) {
       // console.log(url);
        $scope.data.pageLoading = false; 
   
        $scope.file_title = title;
        $('#iframes-info').html('<embed src="'+url+'" style="width: 100%;height: 400px;">');
        $('#library_popup').modal({
            show : true
        });
        $('#PostGUID').val(PostGUID);
        
    }

    /* get type of video */
    $scope.parseVideo = function (url) {
        url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

        if (RegExp.$3.indexOf('youtu') > -1) {
            var type = 'youtube';
        } else if (RegExp.$3.indexOf('vimeo') > -1) {
            var type = 'vimeo';
        }

        return {
            type: type,
            id: RegExp.$6
        };
    }

    /* create video iframe */
    $scope.createVideo =  function (url, width, height) {
        // Returns an iframe of the video with the specified URL.
        var videoObj = $scope.parseVideo(url);
        var $iframe = $('<iframe>', { width: width, height: height });
        $iframe.attr('frameborder', 0);
        if (videoObj.type == 'youtube') {
            $iframe.attr('src', '//www.youtube.com/embed/' + videoObj.id + '?autoplay=1');
        } else if (videoObj.type == 'vimeo') {
            $iframe.attr('src', '//player.vimeo.com/video/' + videoObj.id + '?autoplay=1');
        }
        return $iframe;
    }

    /* create video thumbnail */
    $scope.getVideoThumbnail =  function (url, cb) {
        // Obtains the video's thumbnail and passed it back to a callback function.
        var videoObj = $scope.parseVideo(url);
        if (videoObj.type == 'youtube') {
            cb('//img.youtube.com/vi/' + videoObj.id + '/maxresdefault.jpg');
        } else if (videoObj.type == 'vimeo') {
            // Requires jQuery
            $.get('http://vimeo.com/api/v2/video/' + videoObj.id + '.json', function(data) {
                cb(data[0].thumbnail_large);
            });
        }
    }

    $scope.extension_popup_close = function(){
        $scope.data.pageLoading = true;
        $('#view_model').modal({show:true});
        $scope.data.pageLoading = false;
    } 


    $scope.FileInModalBoxInline = function (url, title, modal="") 
    {
        $scope.data.pageLoading = false;        
        $scope.file_title = title;        
        $('#iframe-info').html('<iframe style="width: 100%;height: 400px;border:0px none;" src="https://docs.google.com/gview?url='+url+'&amp;embedded=true"></iframe>');
        $('#view_model_file').modal({
            show : true
        });
    }

}]);




/*jquery*/
$(document).ready(function() {
    /*Used to display menu active*/
    $('.navbar-nav ul li a.active').closest('ul').addClass('show');
    $('.navbar-nav ul li a.active').closest('ul').parent().closest('li').addClass('show');
    $(".menu-toggel").click(function(){
        $(".main-navbar").toggleClass("sidebar_hide");
    });



    /*Submit Form*/
    $(".form-control").keypress(function(e) {
        if (e.which == 13) {
           // $(this.form).find(':submit').focus().click();
        }
    });


    /*disable right click*/
    // $('html').on("contextmenu", function(e) {
    //     return false;
    // });


    $(document).on('keypress', ".numeric", function(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if (key < 48 || key > 57) {
            return false;
        } else {
            return true;
        }
    });


    $(document).on('keypress', ".integer", function(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 /* || event.keyCode === 46*/ ) {
            return true;
        } else if (key < 48 || key > 57) {
            return false;
        } else {
            return true;
        }
    });




    /*upload profile picture*/
     

  

   $(document).on('click', "#picture-uploadBtn", function() {
        $(this).parent().find('#fileInput').focus().val("").trigger('click');
    });

    $(document).on('change', '#fileInputs', function() {
        var target = $(this).data('target');
        var croptarget = $(this).data('croptarget');

        var mediaGUID = $(this).data('targetinput');
        var progressBar = $('.progressBars'),
            bar= $('.progressBars .bars'),
            percent = $('.progressBars .percents');
        $(this).parent().ajaxForm({
            data: {
                SessionKey: SessionKey
            },
            dataType: 'json',
            beforeSend: function() {
                progressBar.fadeIn();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function(obj, statusText, xhr, $form) {
                if (obj.ResponseCode == 200) {
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    $(target).prop("src", obj.Data.MediaURL);
                    //$("input[name='File']").val(obj.Data.MediaGUID);
                    $(mediaGUID).val(obj.Data.MediaGUID);

                    if (obj.Data.ImageCropper == "Yes") {
                        $(croptarget).show();
                       
                    }

                } else {
                    ErrorPopup(obj.Message);
                }
            },
            complete: function(xhr) {
                progressBar.fadeOut();
                $('#fileInputs').val(mediaGUID);
            }
        }).submit();

    });

    /*upload profile picture*/
    


    $(document).on('change', '#fileInput', function() {
        var target = $(this).data('target');
        var croptarget = $(this).data('croptarget');

        var mediaGUID = $(this).data('targetinput');
        var progressBar = $('.progressBar'),
            bar = $('.progressBar .bar'),
            percent = $('.progressBar .percent');
        $(this).parent().ajaxForm({
            data: {
                SessionKey: SessionKey
            },
            dataType: 'json',
            beforeSend: function() {
                progressBar.fadeIn();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function(obj, statusText, xhr, $form) {
                if (obj.ResponseCode == 200) {
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    $(target).prop("src", obj.Data.MediaURL);
                    //$("input[name='MediaGUIDs']").val(obj.Data.MediaGUID);
                    $(mediaGUID).val(obj.Data.MediaGUID);
                    $("#MediaName").val(obj.Data.MediaName);
                    if (obj.Data.ImageCropper == "Yes") {
                        $(croptarget).show();
                        /*crop plugin - starts*/
                        jQuery('img#picture-box-picture').imgAreaSelect({
                            handles: true,
                            onSelectEnd: getCropSizes,
                            disable: false,
                            hide: false,
                        });
                        /*crop plugin - ends*/
                    }

                } else {
                    ErrorPopup(obj.Message);
                }
            },
            complete: function(xhr) {
                progressBar.fadeOut();
                $('#fileInput').val("");


            }
        }).submit();

    });


    $(document).on('click', '#cropBtn', function() {
        var target = $(this).data('target');
        var croptarget = $(this).data('croptarget');

        var progressBar = $('.progressBar'),
            bar = $('.progressBar .bar'),
            percent = $('.progressBar .percent');
        $(this).parent().ajaxForm({
            data: {
                SessionKey: SessionKey
            },
            dataType: 'json',
            beforeSend: function() {
                progressBar.fadeIn();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function(obj, statusText, xhr, $form) {
                if (obj.ResponseCode == 200) {
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    $(target).prop("src", obj.Data.MediaURL + '?' + Math.random());

                    jQuery('img#picture-box-picture').imgAreaSelect({
                        disable: true,
                        hide: true,
                    });
                    $(croptarget).hide();

                } else {
                    ErrorPopup(obj.Message);
                }
            },
            complete: function(xhr) {
                progressBar.fadeOut();
            }
        }).submit();

    });


    /* Function to get images size */
    function getCropSizes(img, obj) {
        var x_axis = obj.x1;
        var x2_axis = obj.x2;
        var y_axis = obj.y1;
        var y2_axis = obj.y2;
        var thumb_width = obj.width;
        var thumb_height = obj.height;
        if (thumb_width > 0) {
            jQuery('#x1-axis').val(x_axis);
            jQuery('#y1-axis').val(y_axis);
            jQuery('#x2-axis').val(x2_axis);
            jQuery('#y2-axis').val(y2_axis);
            jQuery('#thumb-width').val(thumb_width);
            jQuery('#thumb-height').val(thumb_height);
        } else {
            alert("Please select portion..!");
        }
    }




    $(document).on('keypress', ".numeric", function(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if (key < 48 || key > 57) {
            return false;
        } else {
            return true;
        }
    });





}); /* document ready end */

function getQueryStringValue(key)
{ 
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for(var i = 0; i < hashes.length; i++)
  {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return (!vars[key]) ? '' : vars[key];
}

function  SearchTextClear(keywords) {
    if(keywords.length > 0){
        $("#SearchTextClear").show();
    }else{
        $("#SearchTextClear").hide();
    }
}

function  RemoveSearchKeyword() {
    $('input[name="Keyword"]').val("");    
    $("#universal_search .glyphicon-search").trigger("click");
}


function ResetAllFilters() 
{
    $("#filterForm").find("input, select").val('');
}


function character_limit_sms(objId)
{
    var str = $("#"+objId).val();
    var strlen = str.length;

    var limit = 160;

    if(strlen > limit)
    {
        str = str.substring(0, limit);

        $("#"+objId).val(str);

        strlen = 0;
    }
    else
    {
        strlen = parseInt(limit) - parseInt(strlen);
    }

    $("#"+objId+"_lbl").html(strlen + " characters remaining");
}