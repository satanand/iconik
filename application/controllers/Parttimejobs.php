<?php      
defined('BASEPATH') OR exit('No direct script access allowed');
class Parttimejobs extends Admin_Controller_Secure {
	
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/parttimejobs.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',		
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);		

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Jobs" => base_url()."jobs",
			"Part Time Jobs" => ""
		);		


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('partTimeJobs/jobs_list');
		$this->load->view('includes/footer');
	}

	public function applicants_list(){
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/parttimejobs.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',		
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);		

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Jobs" => base_url()."jobs",
			"Applicants List" => ""
		);		


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('partTimeJobs/applicants_list');
		$this->load->view('includes/footer');
	}
}