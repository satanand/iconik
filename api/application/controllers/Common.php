<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Askquestion_model');
		$this->load->model('Entity_model');
		$this->load->model('Common_model');
	}


	public function ContactUs_post(){
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Name', 'Name', 'trim|required');
		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|required');
		$this->form_validation->set_rules('Subject', 'Subject', 'trim|required');
		$this->form_validation->set_rules('Message', 'Message', 'trim|required');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->ContactUs($this->EntityID,$this->input->post());

		if(!empty($data)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $data;
		}	
	}


	


	public function RatingByStudent_post(){
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('FacultyRating', 'FacultyRating', 'trim');
		$this->form_validation->set_rules('CourseRating ', 'CourseRating ', 'trim');
		$this->form_validation->set_rules('FacilityRating', 'FacilityRating', 'trim');
		$this->form_validation->set_rules('TeachingRating', 'TeachingRating', 'trim');
		$this->form_validation->set_rules('EnviornmentRating', 'EnviornmentRating', 'trim');
		$this->form_validation->set_rules('Comment', 'Comment', 'trim');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->RatingByStudent($this->EntityID,$this->input->post());

		if(!$data)
		{
			$this->Return['ResponseCode'] 	=	500;
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Message'] 	=	"Rating has been saved successfully.";
		}	
	}


	public function GetRatingByStudentID_post(){
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->GetRatingByStudentID($this->EntityID);

		if(!empty($data)){
			$this->Return['Data'] 	=	$data;
		}else{
			$this->Return['ResponseCode'] 	=	500;
		}	
	}



	public function GetStudentAccount_post(){
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->GetStudentAccount($this->EntityID);

		if(!empty($data)){
			$this->Return['Data'] 	=	$data;
		}else{
			$this->Return['ResponseCode'] 	=	500;
		}	
	}



	public function GetBatchmates_post(){
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->GetBatchmates($this->EntityID,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($data)){
			$this->Return['Data'] 	=	$data['Data'];
		}else{
			$this->Return['ResponseCode'] 	=	500;
		}	
	}



	public function getAAQSubjects_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);	

		$data = $this->Common_model->getAAQSubjects();

		if(!empty($data))
		{
			$this->Return['Data'] =	$data;
		}
		else
		{
			$this->Return['ResponseCode'] =	500;
		}	
	}


	public function getAAQRelatedTo_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);	

		$data = $this->Common_model->getAAQRelatedTo();

		if(!empty($data))
		{
			$this->Return['Data'] =	$data;
		}
		else
		{
			$this->Return['ResponseCode'] =	500;
		}	
	}


	public function GetBirthDaysList_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->GetBirthDaysList($this->EntityID);

		if(isset($data) && !empty($data))
		{
			$this->Return['Data'] = $data;
		}	
	}


	public function GetPendingProfile_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->GetPendingProfile($this->EntityID);

		if(isset($data) && !empty($data))
		{
			$this->Return['Data'] = $data;
		}	
	}



	public function editStudentApp_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		
		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');

		$this->form_validation->set_rules('LastName', 'Last Name', 'trim');

		$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'trim|required|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');

		$this->form_validation->set_rules('Address', 'Address', 'trim|required');

		$this->form_validation->set_rules('StateID', 'State Name', 'trim');

		$this->form_validation->set_rules('CityName', 'City Name', 'trim');

		$this->form_validation->set_rules('Postal', 'Zip Code', 'trim');		


		$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL', 'trim');

		$this->form_validation->set_rules('FacebookURL', 'FacebookURL', 'trim');

		$this->form_validation->set_rules('GuardianName', 'Guardian Name', 'trim');

		$this->form_validation->set_rules('GuardianAddress', 'Guardian Address', 'trim');

		$this->form_validation->set_rules('GuardianPhone', 'Guardian Mobile', 'trim|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('GuardianEmail', 'Guardian Email', 'trim|valid_email');

		$this->form_validation->set_rules('FathersName', 'Fathers Name', 'trim');

		$this->form_validation->set_rules('MothersName', 'Mothers Name', 'trim');

		$this->form_validation->set_rules('PermanentAddress', 'Permanent Address', 'trim');

		$this->form_validation->set_rules('FathersPhone', 'Fathers/Mothers Mobile', 'trim|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('FathersEmail', 'Fathers/Mothers Email', 'trim|valid_email');
		
		$this->form_validation->set_rules('Remarks', 'Remarks', 'trim');
		
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[7]');

		
		$this->form_validation->validation($this); 


		$this->load->model('Students_model');

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$UserID = $this->Students_model->editStudentApp($this->EntityID, $this->Post);
		
		if($UserID == "PhoneNumber Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This mobile number is already exist.";
		}
		elseif (!$UserID) {

			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";

		}

		else {

			$UserData = $this->Students_model->getStudentByID($this->EntityID, $this->EntityID);
			
			$this->Return['Data'] = $UserData;

			$this->Return['Message'] = "Updated successfully.";
		}
	}



	public function getProducts_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('ProductCategoryID', 'Product Category', 'trim');
		$this->form_validation->set_rules('Sid', 'Sid', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Products_model->getProductsMP($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}


	public function getProductsDetails_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('pid', 'Product Name', 'trim');
		$this->form_validation->set_rules('vid', 'Visitor ID', 'trim');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->getProductsDetails($this->EntityID, $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] 	=	$data;
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}


	public function checkDiscountCoupon_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');
		$this->form_validation->set_rules('ProductID', 'Product ID', 'trim|required');
		$this->form_validation->set_rules('CouponsCode', 'Coupons Code', 'trim|required');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->checkDiscountCoupon($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{
			if($data['ExpiryDate'] < date("Y-m-d"))
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = "Coupon has been expired.";
			}
			elseif($data['MinSpendReq'] > $this->Post['Amount'])
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = $data['MinSpendReq']." minimum purchase required for applying coupon .";
			}
			else
			{
				$Amount = $this->Post['Amount'];
				$DiscountType = $data['DiscountType'];
				$Discount = $data['Discount'];
				if($DiscountType == "bypercent")
				{
					$Discount = round(($Amount * $Discount) / 100);
				}

				$this->Return['Message'] = "";
				$this->Return['Data']['Discount'] 	=	$Discount;
			}	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Coupon details not exist with selected product.";
		}
	}



	public function confirmOrder_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ProductID[]', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('ProductQty[]', 'Product Quantity', 'trim|required');		
		
		$this->form_validation->set_rules('CustomerName', 'Customer Name', 'trim|required');
		$this->form_validation->set_rules('CustomerMobile', 'Customer Mobile', 'trim|required');
		$this->form_validation->set_rules('CustomerEmail', 'Customer Email', 'trim|required');
		
		$this->form_validation->set_rules('BillingAddress', 'Billing Address', 'trim|required');
		$this->form_validation->set_rules('BillingCity', 'Billing City', 'trim|required');
		$this->form_validation->set_rules('BillingState', 'Billing State', 'trim|required');
		$this->form_validation->set_rules('BillingZipCode', 'Billing Zip Code', 'trim|required');

		$this->form_validation->set_rules('ShippingAddress', 'Delivery Address', 'trim|required');
		$this->form_validation->set_rules('ShippingCity', 'Delivery City', 'trim|required');
		$this->form_validation->set_rules('ShippingState', 'Delivery State', 'trim|required');
		$this->form_validation->set_rules('ShippingZipCode', 'Delivery Zip Code', 'trim|required');
				

		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->confirmOrder($this->EntityID, $this->Post);
		
		if(isset($data) && !empty($data))
		{
			if($data['msg'] === "OrderExceed")
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = $data['data'];
			}
			elseif($data['msg'] === "Exceed")
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = $data['data'];
			}
			else
			{
				$this->Return['Data'] 	=	$data['data'];
				$this->Return['Message'] = "Saved successfully.";
			}	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}


	public function proceedCheckout_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ProductID[]', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('ProductQty[]', 'Product Quantity', 'trim|required');		
		
		$this->form_validation->set_rules('CustomerName', 'Customer Name', 'trim|required');
		$this->form_validation->set_rules('CustomerMobile', 'Customer Mobile', 'trim|required');
		$this->form_validation->set_rules('CustomerEmail', 'Customer Email', 'trim|required');
		
		$this->form_validation->set_rules('BillingAddress', 'Billing Address', 'trim|required');
		$this->form_validation->set_rules('BillingCity', 'Billing City', 'trim|required');
		$this->form_validation->set_rules('BillingState', 'Billing State', 'trim|required');
		$this->form_validation->set_rules('BillingZipCode', 'Billing Zip Code', 'trim|required');

		$this->form_validation->set_rules('ShippingAddress', 'Delivery Address', 'trim|required');
		$this->form_validation->set_rules('ShippingCity', 'Delivery City', 'trim|required');
		$this->form_validation->set_rules('ShippingState', 'Delivery State', 'trim|required');
		$this->form_validation->set_rules('ShippingZipCode', 'Delivery Zip Code', 'trim|required');
				

		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->proceedCheckout($this->EntityID, $this->Post);
		
		if(isset($data) && !empty($data))
		{
			if($data['msg'] === "OrderExceed")
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = $data['data'];
			}
			elseif($data['msg'] === "Exceed")
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = $data['data'];
			}
			else
			{
				$this->Return['Data'] 	=	$data['data'];
				$this->Return['Message'] = "Saved successfully.";
			}	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}
	

	public function validateUserOrder_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->validateUserOrder($this->EntityID, $this->Post);

		if($data === true)
		{
			$this->Return['Data'] 	=	$data;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Order does not exist in the system. Place new order.";
		}
	}


	public function savePaymentSlip_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->set_rules('PaidAmount', 'Paid Amount', 'trim');
		$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim');
		$this->form_validation->set_rules('OrderStatus', 'Order Status', 'trim');
		$this->form_validation->set_rules('TrackingID', 'Tracking ID', 'trim');

		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->savePaymentSlip($this->EntityID, $this->Post);

		if($data)
		{
			$this->Return['Data'] 	=	$data;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}


	public function getMyOrders_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->getMyOrders($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{
			$this->Return['Data'] 	=	$data;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "No record found.";
		}
	}


	public function getOrdersDetails_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->getOrdersDetails($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{
			$this->Return['Data'] 	=	$data;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "No record found.";
		}
	}


	public function checkForDownloadProduct_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderedProductID', 'OrderedProductID', 'trim|required');
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->set_rules('ProductID', 'Product ID', 'trim|required');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->checkForDownloadProduct($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{
			if($data['Records']['IsDownlodable'] == 0)
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message'] = "Product is not available for download.";
			}
			else
			{
				$this->Return['Data'] 	=	$data;
				$this->Return['Message'] = "";
			}	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Details not found in the system.";
		}
	}


	public function getCartItems_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('VisitorID', 'Visitor ID', 'trim|required');		
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->getCartItems($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{			
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Data'] 	=	$data;	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}

	public function saveProductInfo_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('VisitorID', 'Visitor ID', 'trim|required');
		$this->form_validation->set_rules('ProductID', 'ProductID', 'trim|required');		
		$this->form_validation->set_rules('Qty', 'Quantity', 'trim|required');
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->saveProductInfo($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{			
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Data'] 	=	$data;	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Error occur while saving product details.";
		}
	}


	public function removeFromCartProduct_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('VisitorID', 'Visitor ID', 'trim|required');
		$this->form_validation->set_rules('ProductID', 'ProductID', 'trim|required');		
		$this->form_validation->validation($this);

		$this->load->model('Products_model');
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Products_model->removeFromCartProduct($this->EntityID, $this->Post);

		if(isset($data) && !empty($data))
		{			
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Data'] 	=	$data;	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Error occur while saving product details.";
		}
	}



	public function assignStudentToInstitute_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');
		$this->form_validation->set_rules('InstituteID', 'InstituteID', 'trim|required');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim');

		$this->form_validation->set_rules('PaidAmount', 'Paid Amount', 'trim');
		$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim');
		$this->form_validation->set_rules('OrderStatus', 'Order Status', 'trim');
		$this->form_validation->set_rules('TrackingID', 'Tracking ID', 'trim');

		$this->form_validation->validation($this);
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Common_model->assignStudentToInstitute($this->EntityID, $this->Post);

		if($data)
		{
			$this->Return['Data'] 	=	$data;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message'] = "Oops! Something went wrong.";
		}
	}

}