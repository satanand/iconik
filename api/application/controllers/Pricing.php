<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Pricing extends API_Controller_Secure

{

	function __construct()



	{



		parent::__construct();



		$this->load->model('Pricing_model');



		$this->load->model('Users_model');



		$this->load->model('Common_model');



	}







	/*



	Description: 	Use to add new keys



	URL: 			/api/keys/add	



	*/



	public function add_post()



	{



		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');

		$this->form_validation->set_rules('Price', 'Price', 'trim|required');

		$this->form_validation->set_rules('Type', 'Type', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		



		/* Validation - ends */



		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$data = $this->Pricing_model->addPricing($this->EntityID, $this->Post);

		//print_r($data); die;

		if($data){

			if($this->Post['Type'] == "QSP"){
				$data = $this->Pricing_model->getPricing($this->EntityID,$data); //print_r($data);
				$this->Return['Data']   =	$data[0]; 
		    }
			

			$this->Return['Message']   =	"Pricing added successfully."; 

		}



	}







	/*



	Description: 	Use to add new keys



	URL: 			/api/keys/add	



	*/



	public function getUser_post()



	{



		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');



		$this->form_validation->validation($this);  /* Run validation */		



		/* Validation - ends */



		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		//$UsersData = $this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,UserTypeID,UserTypeName,Username',array("UserID" => $this->SessionUserID);



		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,UserTypeID,UserTypeName,Username',array("UserTypeID" => 10));



	}







	/*



	Description: 	Use to add new keys



	URL: 			/api/keys/add	



	*/



	public function allotKeys_post()



	{



		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->set_rules('KeyCount', 'Keys Count', 'trim|required');

		$this->form_validation->set_rules('AvailableKeys', 'Keys Count', 'trim|required');



		$this->form_validation->validation($this);  /* Run validation */		



		/* Validation - ends */



		//print_r($this->post[]); die;



		if($this->input->post('KeyCount') > $this->input->post('AvailableKeys')){

			$this->Return['ResponseCode'] = 500;



			$this->Return['Message']      	=	"Keys count is greater than available keys."; 

		}else{



			$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



			$allotKeys = $this->Pricing_model->allotKeys($this->EntityID, $this->Post, $this->UserID);



			if($allotKeys){



				$this->Return['Message']      	=	"Keys alloted successfully."; 

			}

		}



	}







	/*



	Description: 	Use to add new keys



	URL: 			/api/keys/add	



	*/



	public function availableKeys_post()



	{



		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('validity', 'validity', 'trim|required');



		$this->form_validation->validation($this);  /* Run validation */		



		/* Validation - ends */



		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$allotKeys = $this->Pricing_model->availableKeys($this->EntityID, $this->Post['validity']);



		if($allotKeys){



			$this->Return['Data']      	=	$allotKeys; 

		}



	}





	





	/*



	Name: 			getKeysStatisctics



	Description: 	Use to get keys dashboard.



	URL: 			api/Keys/getKeysStatisctics/	



	*/



	public function getPricing_post()



	{



		/* Validation section */



		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');



		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');



		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');


		$this->form_validation->set_rules('type', 'type', 'trim|required');



		$this->form_validation->validation($this);  /* Run validation */		



		/* Validation - ends */



		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$data = $this->Pricing_model->getPricing($this->EntityID,array("type"=>$this->input->post('type')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		
		if(!empty($data)){
			$this->Return['Data'] = $data;
		}

		

	}











	/*



	Description: 	use to get list of filters



	URL: 			/api_admin/entity/getFilterData	



	*/



	public function getBatchByCourse_post()



	{



		/* Validation section */



		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');



		$this->form_validation->set_rules('InstituteGUID', 'InstituteGUID', 'trim|callback_validateEntityGUID');



		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');



		$this->form_validation->validation($this);  /* Run validation */		



		/* Validation - ends */





		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);





		$batchData = $this->Common_model->getBatchByCourse($this->EntityID,$this->CategoryID);



		if($batchData){



			$this->Return['Data'] = $batchData;	



		}



	}





	/*

	Description: 	Use to get Get student list.

	URL: 			/api/key/getStudents where key is null

	Input (Sample JSON): 		

	*/

	public function getStudents_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->validation($this);  /* Run validation */		

		

		if(!empty($this->input->post('CategoryGUID'))){

			$StudentData = $this->Pricing_model->getStudents((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID,"");

		}else if(!empty($this->input->post('BatchID'))){

			$StudentData = $this->Pricing_model->getStudents((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),"",$this->input->post('BatchID'));

		}else{

			$StudentData = $this->Pricing_model->getStudents((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),"","");

		}

		

		if(!empty($StudentData)){

			$this->Return['Data'] = $StudentData;

		}	

	}





	/*

	Description: 	Use to  assign key to student list.

	URL: 			/api/key/assign keys to student

	Input (Sample JSON): 		

	*/

	public function assignKeysToStudent_post()

	{

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');

		$this->form_validation->set_rules('AvailableKeys', 'AvailableKeys', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		//print_r($this->input->post('UserGUID')); die;  assignKeysToStudent



		$StudentData = $this->Keys_model->assignKeysToStudent((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),12081,$this->input->post('UserGUID'),$this->input->post('Validity'),$this->input->post('AvailableKeys'));

		if($StudentData){

			$this->Return['Message'] = "keys are assigned to ".$StudentData." students";

		}

	}

}