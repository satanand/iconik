/*app.filter('removeHTMLTags', function() 
{
    return function(text) 
    {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});*/


app.controller('PageController', function($scope, $http, $timeout) 
{
    $( "#FilterStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    


    $scope.data.qualification = [];

    $scope.data.states = [];


	$scope.getFilterData = function()
	{
		/*$scope.data.qualification = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'qualification/getQualification', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ 

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.qualification.push(response.Data.Records[i]);
            	}

         	}

        });*/


        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }    
	}


	$scope.applyFilter = function() 
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
        $scope.getFilterData();
    }


	$scope.getList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'jobprovider/getPartTimeJobs', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            { 

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                 	$scope.data.dataList.push(response.Data.Records[i]);
            	}

                $scope.data.pageNo++;
         	}
            else
            {
            	$scope.data.noRecords = true;
        	}

	        $scope.data.listLoading = false;
	    });

    }


    $scope.loadFormView = function(Position, PostJobGUID, Data) {
        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLView = PATH_TEMPLATE + module + '/view_jobs.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#View_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }



    $scope.loadFormViewApplicants = function(Position, PostJobGUID, Data) {
        $scope.data.Position = Position;
        $scope.formData = Data;

        //$scope.templateURLView = PATH_TEMPLATE + 'jobs/View_model.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#View_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }


    


    $scope.getApplicants = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }  
   
        var PartTimeJobID = $scope.getUrlParameter('PartTimeJobID');    

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PartTimeJobID='+PartTimeJobID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'jobprovider/getApplicants', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }

                $scope.data.pageNo++;
            }
            else
            { 
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });
    }


    

});


function getCities(state_id)
{
  angular.element(document.getElementById('content-body')).scope().getCities(state_id);  
}

/*function filterJob()
{
    if($("#FilterStartDate").val() !="" && $("#FilterEndDate").val() !="")
    {
        angular.element(document.getElementById('content-body')).scope().getList(1);
    }
}*/


function RemoveJobFilters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword").val('');

    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function RemoveApplicantsFilters()
{
    $("#FilterStartDate, #FilterEndDate, #FilterKeyword").val('');

    $("#FilterStatus").val(10);

    angular.element(document.getElementById('content-body')).scope().getApplicants(1);
}