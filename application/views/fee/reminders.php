<style type="text/css">
.navbar
{
	background: none !important;
	background-color: #f8f9fa !important;
	box-shadow: 0px 0px 0px 0px !important;
}	
</style>

<header class="panel-heading" > <h1 class="h4">Set Reminders</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" id="fee-body" ng-controller="PageController" ng-init="getFeeReminders();"><!-- Body -->

<div class="container my-3" >
<nav class="mt-3">
<div class="nav nav-pills" id="nav-tab" role="tablist">
<a class="nav-item nav-link active rounded-0 border " id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Reminder Before Due Date</a>
<a class="nav-item nav-link rounded-0 border ml-2" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Reminder After Due Date</a>
</div>
</nav>
<div class="tab-content" id="nav-tabContent">

<!--==============================================================================-->
<div class="tab-pane fade show active border rounded-0 mb-3" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
<form class="px-3 pt-3" method="post" name="setting_before_due_date" id="setting_before_due_date">

<div style="color: #28a745;">Note: You can change the default message in EMAIL & SMS and use the CONSTANT({STD_NAME}, {DUE_AMT}, {DUEDATE} and {INST_NAME}) as per your requirement. Do not remove and do not rename these CONSTANTS.</div>
<div class="card-header bg-success border-0 text-white">
Email Reminder
</div>


<div class="form-group mt-2">
<label for="bf_email_days">Select Days<span style="color:red;">*</span></label>
<select class="form-control rounded-0" name="bf_email_days" id="bf_email_days" onchange="show_remind_days('bfe', this.value, 'bf_email_remind_days');">
	<option value="">Select</option>
	<option ng-repeat="(key, row) in data.duedays" value="{{row}}">{{row}}</option>
</select>
</div>


<div class="form-group row mb-2">
	<div class="col-md-12">Send Reminders On Days<span style="color:red;">*</span></div>
	<div id="bf_email_remind_days"></div>
</div>



<div class="form-group mt-1">
<label for="bf_email_content">Email Content<span style="color:red;">*</span></label>
<textarea class="form-control rounded-0" name="bf_email_content" id="bf_email_content" rows="5">
Dear {STD_NAME},<br/><br/>

Your fee installment of amount Rs. {DUE_AMT} is due on {DUEDATE} at {INST_NAME}. Please pay your fee on time to avoid late fee charges. Please contact the institute’s Accounts Department for any queries.<br/>
Please ignore this email if the amount has already been paid.<br/><br/>

We appreciate your association and welcome your feedback/suggestions.<br/><br/>

Warm Regards,<br/>
{INST_NAME}	
</textarea>
</div>



<div class="card-header bg-success border-0 text-white">
SMS Reminder
</div>

<div class="form-group">
<label for="bf_sms_days">Select Days<span style="color:red;">*</span></label>
<select class="form-control rounded-0" name="bf_sms_days" id="bf_sms_days" onchange="show_remind_days('bfs', this.value, 'bf_sms_remind_days');">
	<option value="">Select</option>
	<option ng-repeat="(key, row) in data.duedays" value="{{row}}">{{row}}</option>
</select>
</div>


<div class="row mb-2">
<div class="col-md-12">Send Reminders On Days<span style="color:red;">*</span></div>
	<div id="bf_sms_remind_days"></div>
</div>


<div class="form-group mt-1">
<label for="bf_sms_content">SMS Content<span style="color:red;">*</span></label>
<textarea class="form-control rounded-0" name="bf_sms_content" id="bf_sms_content" rows="2" onkeyup="character_limit_sms(this.id);" onblur="character_limit_sms(this.id);">
Dear {STD_NAME}, your fee installment of amount Rs.{DUE_AMT} is due on {DUEDATE}. Kindly pay at the earliest, please ignore if already paid. Regards {INST_NAME}</textarea>

<span>Maximum 160 characters are allowed. <span id="bf_sms_content_lbl"></span></span>

</div>


<div class="d-flex flex-row-reverse mt-2 mb-2">
<button type="submit" name="bfBtn" ng-disabled="addBeforeDueDateLoading" class="btn btn-success radius-full p-2" ng-click="addFormBeforeDueDate()">Submit</button>&nbsp;&nbsp;
</div>

</form>
</div>
<!--==============================================================================-->







<div class="tab-pane fade border p-3" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">


<!--BLUE TAB======================================================================-->
<form class="" method="post" name="setting_after_due_date1" id="setting_after_due_date1">
<div class="pos-f-t mb-3">
<nav class="navbar navbar-light bg-light border rounded-0">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent1" aria-controls="navbarToggleExternalContent1" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span><strong class="ml-2">Blue</strong>
</button>
</nav>
<div class="collapse" id="navbarToggleExternalContent1">
<div class="bg-light border rounded-0 p-3">

<div style="color: #28a745;">Note: You can change the default message in EMAIL & SMS and use the CONSTANT({STD_NAME}, {DUE_AMT}, {DUEDATE} and {INST_NAME}) as per your requirement. Do not remove and do not rename these CONSTANTS.</div>
<div class="card-header bg-success border-0 text-white">Email Reminder</div>

<div class="form-group mt-2">
<label for="af_email_days1">Select Days</label>
<select class="form-control rounded-0" name="af_email_days1" id="af_email_days1" onchange="show_remind_days('afe1', this.value, 'af_email_remind_days1');">
	<option value="">Select</option>
	<option ng-repeat="(key, row) in data.section1" value="{{row}}">{{row}}</option>
</select>
</div>


<div class="form-group row mb-2">
	<div class="col-md-12">Send Reminders On Days</div>
	<div id="af_email_remind_days1"></div>
</div>


<div class="form-group mt-1">
<label for="af_email_content1">Email Content</label>
<textarea class="form-control rounded-0" name="af_email_content1" id="af_email_content1" rows="5">
Dear {STD_NAME},<br/><br/>

Your fee installment of amount Rs. {DUE_AMT} was due on {DUEDATE} at {INST_NAME}. Please pay your fee immediately. Please contact the institute’s Accounts Department for any queries.<br/>
Please ignore this email if the amount has already been paid.<br/><br/>

We appreciate your association and welcome your feedback/suggestions.<br/><br/>

Warm Regards,<br/>
{INST_NAME}
</textarea>
</div>


<div class="card-header bg-success border-0 text-white">SMS Reminder</div>

<div class="form-group">
<label for="af_sms_days1">Select Days</label>
<select class="form-control rounded-0" name="af_sms_days1" id="af_sms_days1" onchange="show_remind_days('afs1', this.value, 'af_sms_remind_days1');">
	<option value="">Select</option>
	<option ng-repeat="(key, row) in data.section1" value="{{row}}">{{row}}</option>
</select>
</div>


<div class="row mb-2">
	<div class="col-md-12">Send Reminders On Days</div>
	<div id="af_sms_remind_days1"></div>
</div>


<div class="form-group mt-1">
<label for="af_sms_content1">SMS Content</label>
<textarea class="form-control rounded-0" name="af_sms_content1" id="af_sms_content1" rows="3" onkeyup="character_limit_sms(this.id);" onblur="character_limit_sms(this.id);">
Dear {STD_NAME}, your fee installment of amount Rs{DUE_AMT} was due on {DUEDATE}. Kindly pay at the earliest, please ignore if already paid. Regards {INST_NAME}</textarea>
<span>Maximum 160 characters are allowed. <span id="af_sms_content1_lbl"></span></span>
</div>


<div class="row  ml-2">
<p class="mr-3">Send Reminders To:</p>
<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_s1" id="af_alert_s1" checked>
<label class="form-check-label" for="Student">Student</label>
</div>


<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_p1" id="af_alert_p1">
<label class="form-check-label" for="Parent">Parent</label>
</div>

</div>

<div class="d-flex flex-row-reverse mt-2 mb-2">
<button type="submit" name="bfBtn" ng-disabled="addFormAfterDueDateLoading1" class="btn btn-success radius-full p-2" ng-click="addFormAfterDueDate('1');">Submit</button>&nbsp;&nbsp;
</div>

</div>
</div>
</div>
</form>



<!--ORANGE TAB======================================================================-->
<form class="" method="post" name="setting_after_due_date2" id="setting_after_due_date2">
<div class="pos-f-t mb-3">
<nav class="navbar navbar-light bg-light border rounded-0">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent2" aria-controls="navbarToggleExternalContent2" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span><strong class="ml-2">Orange</strong>
</button>
</nav>
<div class="collapse" id="navbarToggleExternalContent2">
<div class="bg-light border rounded-0 p-3">

<div style="color: #28a745;">Note: You can change the default message in EMAIL & SMS and use the CONSTANT({STD_NAME}, {DUE_AMT}, {DUEDATE} and {INST_NAME}) as per your requirement. Do not remove and do not rename these CONSTANTS.</div>
<div class="card-header bg-success border-0 text-white">Email Reminder</div>


<div class="form-group mt-2">
<label for="af_email_days1">Select Days</label>
<select class="form-control rounded-0" name="af_email_days2" id="af_email_days2" onchange="show_remind_days('afe2', this.value, 'af_email_remind_days2');">
	<option value="">Select</option>
	<!-- <option ng-repeat="(key, row) in data.section2" value="{{row}}">{{row}}</option> -->
</select>
</div>


<div class="form-group row mb-2">
	<div class="col-md-12">Send Reminders On Days</div>
	<div id="af_email_remind_days2"></div>
</div>


<div class="form-group mt-1">
<label for="af_email_content2">Email Content</label>
<textarea class="form-control rounded-0" name="af_email_content2" id="af_email_content2" rows="3">
Dear {STD_NAME},<br/><br/>

Your fee installment of amount Rs. {DUE_AMT} was due on {DUEDATE} at {INST_NAME}. Please pay your fee immediately. Please contact the institute’s Accounts Department for any queries.<br/>
Please ignore this email if the amount has already been paid.<br/><br/>

We appreciate your association and welcome your feedback/suggestions.<br/><br/>

Warm Regards,<br/>
{INST_NAME}	
</textarea>
</div>


<div class="card-header bg-success border-0 text-white">SMS Reminder</div>

<div class="form-group">
<label for="af_sms_days2">Select Days</label>
<select class="form-control rounded-0" name="af_sms_days2" id="af_sms_days2" onchange="show_remind_days('afs2', this.value, 'af_sms_remind_days2');">
	<option value="">Select</option>
	<!-- <option ng-repeat="(key, row) in data.section2" value="{{row}}">{{row}}</option> -->
</select>
</div>


<div class="row mb-2">
	<div class="col-md-12">Send Reminders On Days</div>
	<div id="af_sms_remind_days2"></div>
</div>


<div class="form-group mt-1">
<label for="af_sms_content2">SMS Content</label>
<textarea class="form-control rounded-0" name="af_sms_content2" id="af_sms_content2" rows="3" onkeyup="character_limit_sms(this.id);" onblur="character_limit_sms(this.id);">
Dear {STD_NAME}, your fee installment of amount Rs.{DUE_AMT} was due on {DUEDATE}. Please pay immediately, please ignore if already paid. Regards {INST_NAME}</textarea>
<span>Maximum 160 characters are allowed. <span id="af_sms_content2_lbl"></span></span>
</div>


<div class="row  ml-2">
<p class="mr-3">Send Reminders To:</p>
<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_s2" id="af_alert_s2" checked>
<label class="form-check-label" for="Student">Student</label>
</div>


<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_p2" id="af_alert_p2" checked>
<label class="form-check-label" for="Parent">Parent</label>
</div>


<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_f2" id="af_alert_f2">
<label class="form-check-label" for="Faculty">Faculty</label>
</div>

</div>

<div class="d-flex flex-row-reverse mt-2 mb-2">
<button type="submit" name="bfBtn" ng-disabled="addFormAfterDueDateLoading2" class="btn btn-success radius-full p-2" ng-click="addFormAfterDueDate('2');">Submit</button>&nbsp;&nbsp;
</div>

</div>
</div>
</div>
</form>




<!--RED TAB======================================================================-->
<form class="" method="post" name="setting_after_due_date3" id="setting_after_due_date3">
<div class="pos-f-t mb-3">
<nav class="navbar navbar-light bg-light border rounded-0">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent3" aria-controls="navbarToggleExternalContent3" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span><strong class="ml-2">Red</strong>
</button>
</nav>
<div class="collapse" id="navbarToggleExternalContent3">
<div class="bg-light border rounded-0 p-3">

<div style="color: #28a745;">Note: You can change the default message in EMAIL & SMS and use the CONSTANT({STD_NAME}, {DUE_AMT}, {DUEDATE} and {INST_NAME}) as per your requirement. Do not remove and do not rename these CONSTANTS.</div>
<div class="card-header bg-success border-0 text-white">Email Reminder</div>


<div class="form-group mt-2">
<label for="af_email_days3">Select Days</label>
<select class="form-control rounded-0" name="af_email_days3" id="af_email_days3" onchange="show_remind_days('afe3', this.value, 'af_email_remind_days3');">
	<option value="">Select</option>
	<!-- <option ng-repeat="(key, row) in data.section3" value="{{row}}">{{row}}</option> -->
</select>
</div>


<div class="form-group row mb-2">
	<div class="col-md-12">Send Reminders On Days</div>
	<div id="af_email_remind_days3"></div>
</div>


<div class="form-group mt-1">
<label for="af_email_content3">Email Content</label>
<textarea class="form-control rounded-0" name="af_email_content3" id="af_email_content3" rows="5">
Dear {STD_NAME},<br/><br/>

Your fee installment of amount Rs. {DUE_AMT} was due on {DUEDATE} at {INST_NAME}. Immediate payment of the fee is required to avoid interruption of classes.<br/>
Please ignore this email if the amount has already been paid.<br/><br/>

We appreciate your association and welcome your feedback/suggestions.<br/><br/>

Warm Regards,<br/>
{INST_NAME}
</textarea>
</div>


<div class="card-header bg-success border-0 text-white">SMS Reminder</div>

<div class="form-group">
<label for="af_sms_days1">Select Days</label>
<select class="form-control rounded-0" name="af_sms_days3" id="af_sms_days3" onchange="show_remind_days('afs3', this.value, 'af_sms_remind_days3');">
	<option value="">Select</option>
	<!-- <option ng-repeat="(key, row) in data.section3" value="{{row}}">{{row}}</option> -->
</select>
</div>


<div class="row mb-2">
	<div class="col-md-12">Send Reminders On Days</div>
	<div id="af_sms_remind_days3"></div>
</div>


<div class="form-group mt-1">
<label for="af_sms_content3">SMS Content</label>
<textarea class="form-control rounded-0" name="af_sms_content3" id="af_sms_content3" rows="3" onkeyup="character_limit_sms(this.id);" onblur="character_limit_sms(this.id);">
Dear {STD_NAME}, your fee installment of amount Rs.{DUE_AMT} was due on {DUEDATE}. Do immediate payment to avoid interruption of classes. Regards {INST_NAME}</textarea>
<span>Maximum 160 characters are allowed. <span id="af_sms_content3_lbl"></span></span>
</div>


<div class="row  ml-2">
<p class="mr-3">Send Reminders To:</p>
<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_s3" id="af_alert_s3" checked>
<label class="form-check-label" for="Student">Student</label>
</div>


<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_p3" id="af_alert_p3" checked>
<label class="form-check-label" for="Parent">Parent</label>
</div>


<div class="form-check col-md-1">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_f3" id="af_alert_f3" checked>
<label class="form-check-label" for="Faculty">Faculty</label>
</div>


<div class="form-check col-md-2">
<input class="form-check-input rounded-0" type="checkbox" value="1" name="af_alert_m3" id="af_alert_m3">
<label class="form-check-label" for="Management">Management</label>
</div>

</div>

<div class="d-flex flex-row-reverse mt-2 mb-2">
<button type="submit" name="bfBtn" ng-disabled="addFormAfterDueDateLoading3" class="btn btn-success radius-full p-2" ng-click="addFormAfterDueDate('3');">Submit</button>&nbsp;&nbsp;
</div>

</div>
</div>
</div>
</form>






</div>
</div>
</div>
</div><!-- Body/ -->