<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questionassign_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	} 

	function getQuestionAssignStatistics($EntityID,$SubjectID="",$BatchID=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		if(!empty($BatchID)){
			$query = $this->db->query("select CourseID,BatchGUID,BatchName,BatchID from tbl_batch join tbl_entity ON tbl_batch.BatchID = tbl_entity.EntityID  where BatchID='".$BatchID."' and InstituteID = ".$InstituteID);
		}else{
			$query = $this->db->query("select CourseID,BatchGUID,BatchName,BatchID from tbl_batch join tbl_entity ON tbl_batch.BatchID = tbl_entity.EntityID where InstituteID = ".$InstituteID);
		}
		//echo $this->db->last_query(); die;
		$course = $query->result_array();

		$result = array(); 
		$new_result = array(); 
		$check_array = array();
		$check = 1; 
		if(count($course) > 0){
			//print_r($course);
			$p = 0;
			foreach ($course as $key => $value) {

					if(!empty($SubjectID)){
						$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CourseID']."' and CategoryGUID = '".$SubjectID."'");
					}else{
						$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CourseID']."'");
					}

					//echo $this->db->last_query();

					$SubCategoryData = $query->result_array();

					//print_r($SubCategoryData);

					$parentCategoryName = $value['BatchName'];
					$parentCategoryGUID = $value['BatchGUID'];
					$total_question = 0;

					foreach ($SubCategoryData as $k => $v) {

						if(!in_array($parentCategoryName, $check_array)){
							array_push($check_array, $parentCategoryName);
							$result[$parentCategoryName][$k]['ParentCategoryName'] = $parentCategoryName;
						}else{
							$result[$parentCategoryName][$k]['ParentCategoryName'] = "";	
						}
						$result[$parentCategoryName][$k]['ParentName'] = $parentCategoryName;
						$result[$parentCategoryName][$k]['parentCategoryGUID'] = $parentCategoryGUID;
						$result[$parentCategoryName][$k]['SubCategoryName'] = $v['CategoryName'];
						$result[$parentCategoryName][$k]['CategoryGUID'] = $v['CategoryGUID'];
						$result[$parentCategoryName][$k]['total_assign_count'] = 0;

						//echo "SELECT count(qa.QtAssignID) as count_assignpaper FROM tbl_question_paper_assign as qa where qa.SubjectID = ".$v['CategoryID'];
						$query = $this->db->query("SELECT count(qa.QtAssignID) as count_assignpaper FROM tbl_question_paper_assign as qa join tbl_question_paper as qp ON qa.QtPaperID = qp.QtPaperID where qp.SubjectID = ".$v['CategoryID']." and qa.QtAssignBatch = ".$value['BatchID']." and qp.QuestionsGroup != 2");

						$data = $query->result_array();

						$result[$parentCategoryName][$k]['assign_paper_count'] = $data[0]['count_assignpaper'];
						
						$total_question = $total_question+$data[0]['count_assignpaper'];

						if(!empty($result[$parentCategoryName][$k])){
							array_push($new_result, $result[$parentCategoryName][$k]);
						}

						$count_sub = $k+1;
						if($count_sub == count($SubCategoryData)){ 	//echo $count_sub; echo "<br>";
							$result[$parentCategoryName][$count_sub]['total_assign_count'] = $total_question;
							$result[$parentCategoryName][$count_sub]['ParentName'] = $parentCategoryName;
							$result[$parentCategoryName][$count_sub]['parentCategoryGUID'] = $parentCategoryGUID;
							$result[$parentCategoryName][$count_sub]['SubCategoryName'] = 'Total';
							$result[$parentCategoryName][$count_sub]['CategoryGUID'] = $v['CategoryGUID'];
							//print_r(expression)
							array_push($new_result, $result[$parentCategoryName][$count_sub]);
						}
					}
			}
			return $new_result;
		}
		return false;
	
	}


	function getQuizAssignStatistics($EntityID,$Where=""){
		//print_r($Where); die;

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select("qa.*, qp.*,tbl_batch.BatchName");
		$this->db->from("tbl_question_paper_assign  as qa");
		$this->db->join("tbl_question_paper as qp","qa.QtPaperID = qp.QtPaperID");
		$this->db->join("tbl_entity","tbl_entity.EntityID = qp.QtPaperID");
		$this->db->join("tbl_batch","tbl_batch.BatchID = qa.QtAssignBatch");
		$this->db->where("tbl_entity.InstituteID", $InstituteID);
		$this->db->where("qp.QuestionsGroup",2);

		if(!empty($Where['BatchID'])){
			$this->db->where("qa.QtAssignBatch",$Where['BatchID']);
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->group_start();			
				$this->db->or_like("qp.QtPaperTitle", trim($Where['Keyword']));
				$this->db->or_like("qa.StartTime",trim($Where['Keyword']));
				$this->db->or_like("tbl_batch.BatchName",trim($Where['Keyword']));	
			$this->db->group_end();
		}

		$this->db->order_by("qa.QtAssignID","DESC");

		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0){ 
			$data = $query->result_array();
			foreach($data as $Record){


				$query = $this->db->query('SELECT EntryDate FROM tbl_entity where EntityID = "'.$Record['QtAssignID'].'" limit 1');

				$result = $query->result_array();

				$Record['AssignedDate'] = $result[0]['EntryDate'];

				$query = $this->db->query('SELECT CategoryName,CategoryGUID FROM set_categories where CategoryID = "'.$Record['CourseID'].'" limit 1');

				$result = $query->result_array();

				$Record['CourseName'] = $result[0]['CategoryName'];
				$Record['CourseGUID'] = $result[0]['CategoryGUID'];

				$query = $this->db->query('SELECT CategoryName,CategoryGUID FROM set_categories where CategoryID = "'.$Record['SubjectID'].'" limit 1');

				$result = $query->result_array();

				$Record['SubjectName'] = $result[0]['CategoryName'];
				$Record['SubjectGUID'] = $result[0]['CategoryGUID'];

				$Record['StartTime'] = date('h:i:s',strtotime($Record['StartTime']));

				$query = $this->db->query('SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$Record['QtPaperID'].'"');

				if(!$query->num_rows() > 0){
					$Record['EditStatus'] = "No";
				}else{
					$Record['EditStatus'] = "Yes";
				}

				$Records[] = $Record;
			}			
			//$Return['Data'] = $Records;
			return $Records;
		}
		return FALSE;	
	
	}


	function getResult($EntityID,$QtAssignID=""){
		if(!empty($QtAssignID)){
			$query = $this->db->query("SELECT qa.*, qp.* FROM tbl_question_paper_assign as qa join tbl_question_paper as qp ON qa.QtPaperID = qp.QtPaperID where qa.QtAssignID = ".$QtAssignID." and qp.QuestionsGroup = 2 limit 1");
		}else{
			$query = $this->db->query("SELECT qa.*, qp.* FROM tbl_question_paper_assign as qa join tbl_question_paper as qp ON qa.QtPaperID = qp.QtPaperID where qp.QuestionsGroup = 2");
		}

		$data = $query->result_array();

		//$result[$parentCategoryName][$k]['exam_list'] = $data;

		//$Query = $this->db->get();	

		if(count($data) > 0){ 
			foreach($data as $Record){

				$query = $this->db->query('SELECT EntryDate FROM tbl_entity where EntityID = "'.$Record['QtAssignID'].'"');

				$result = $query->result_array();

				$Record['AssignedDate'] = $result[0]['EntryDate'];

				$query = $this->db->query('SELECT CategoryName FROM set_categories where CategoryID = "'.$Record['CourseID'].'"');

				$result = $query->result_array();

				$Record['CourseName'] = $result[0]['CategoryName'];

				$query = $this->db->query('SELECT CategoryName FROM set_categories where CategoryID = "'.$Record['SubjectID'].'"');

				$result = $query->result_array();

				$Record['SubjectName'] = $result[0]['CategoryName'];

				$Records[] = $Record;
			}			
			//$Return['Data'] = $Records;
			return $Records;
		}
		return FALSE;
	}


	function getBatchPapersList($EntityID, $Where="",$multiRecords=FALSE,  $PageNo=1, $PageSize=15)
	{
		//print_r($Where);
		$this->db->select("*");
		$this->db->from('tbl_question_paper_assign');
		$this->db->join('tbl_question_paper','tbl_question_paper_assign.QtPaperID = tbl_question_paper.QtPaperID');
		//$this->db->join('tbl_students','tbl_students.BatchID = tbl_question_paper_assign.QtAssignBatch');

		if(!empty($Where['BatchID'])){
			$this->db->where("tbl_question_paper_assign.QtAssignBatch",$Where['BatchID']);
		}

		if(!empty($Where['SubjectID'])){
			$this->db->where("tbl_question_paper.SubjectID",$Where['SubjectID']);
		}

		if(!empty($Where['QuestionsGroup'])){
			$this->db->where("(tbl_question_paper.QuestionsGroup like '%".$Where['QuestionsGroup']."%')");
		}else{
			$this->db->where("tbl_question_paper.QuestionsGroup != 2");
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->group_start();			
				$this->db->or_like("tbl_question_paper.QtPaperTitle", $Where['Keyword']);
				$this->db->or_like("tbl_question_paper.PassingMarks",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper_assign.StartTime",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper_assign.EndTime",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper_assign.TotalStudents",$Where['Keyword']);		
			$this->db->group_end();
		}

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();			
			if($TempQ != ""){
				$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			}else{
				$Return['Data']['TotalRecords'] = 0;
			}
			
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get();
		//echo $this->db->last_query(); die;
		if($Query->num_rows() > 0){
			$data = $Query->result_array();
			if(count($data) > 0){ 
				foreach($data as $Record){

					//echo "SELECT count(st.StudentID) as count_students FROM tbl_students as st where st.BatchID = ".$Record['QtAssignBatch'];

					$query_count_students = $this->db->query("SELECT count(st.StudentID) as count_students FROM tbl_students as st where st.BatchID = ".$Record['QtAssignBatch']);

					$count_students = $query_count_students->result_array();

					//print_r($data);

					$Record['TotalStudents'] = $count_students[0]['count_students'];


					/*$QtBankIDs = $Record['EasyLevelQuestionsID'].','.$Record['HighLevelQuestionsID'].','.$Record['ModerateLevelQuestionsID'].','.$Record['QuestionsID'];
					$QtBankIDs =  array_unique(explode(',', $QtBankIDs));
					$QtBankIDs = implode(",", $QtBankIDs);
					$QtBankIDs = str_replace(',,', ',', $QtBankIDs);*/

					
					$QtBankIDsArr = array();
					$QtBankIDsArr[] = ($Record['EasyLevelQuestionsID']) ? $Record['EasyLevelQuestionsID'] : 0;
					$QtBankIDsArr[] = ($Record['HighLevelQuestionsID']) ? $Record['HighLevelQuestionsID']: 0;
					$QtBankIDsArr[] = ($Record['ModerateLevelQuestionsID']) ? $Record['ModerateLevelQuestionsID']: 0;

					$QtBankIDsArr =  array_unique($QtBankIDsArr);

					$QtBankIDs = implode(",", $QtBankIDsArr);
					if(isset($Record['QuestionsID']) && !empty($Record['QuestionsID']))
					{
						$QtBankIDs = $QtBankIDs.",".$Record['QuestionsID'];
					}


					if($QtBankIDs){
						$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

						//echo $QuestionsMarks->num_rows(); die;

						if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows()){
							$QuestionsMarks = $QuestionsMarks->result_array();

							$Record['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
						}else{
							$Record['TotalMarks'] = 0;
						}
					}else{
						$Record['TotalMarks'] = 0;
					}
					


					$Record['StartTime'] = date('H:i:s',strtotime($Record['StartTime']));
					$Record['EndTime'] = date('H:i:s',strtotime($Record['EndTime']));

					$TodayDate = date("Y-m-d");
					$etime = $TodayDate." ".$Record['EndTime'];
					$stime = $TodayDate." ".$Record['StartTime'];
					$diff = (strtotime($etime) - strtotime($stime));
					$Record['Duration'] = $diff / 60;
					

					//echo 'SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"';

					$query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"');

					$result = $query->result_array();

					$Record['QuestionsGroup'] = $result[0]['QuestionGroupName'];


					$query = $this->db->query('SELECT EntryDate FROM tbl_entity where EntityID = "'.$Record['QtAssignID'].'"');

					$result = $query->result_array();

					$Record['AssignedDate'] = date('d-m-Y',strtotime($result[0]['EntryDate']));
					$Record['TestDateDis'] = date('d-m-Y',strtotime($Record['Date']));


					$query = $this->db->query('SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$Record['QtPaperID'].'"');

					$result = $query->result_array();

					if(!$query->num_rows() > 0){
						$Record['EditStatus'] = "No";
					}else{
						$Record['EditStatus'] = "Yes";
					}

					//print_r($Record); die;

					$Records[] = $Record;
				}		
				//print_r($Records);

				$Return['Data'] = $Records;
				return $Return;
			}
		}
		return FALSE;	
	}


	function getAssignedTestByBatch($EntityID, $QuestionsGroup=""){

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		//echo "select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories join tbl_entity ON tbl_entity.EntityID = set_categories.CategoryID where set_categories.CategoryTypeID = 3 and tbl_entity.InstituteID = ".$InstituteID;

		$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories join tbl_entity ON tbl_entity.EntityID = set_categories.CategoryID where set_categories.CategoryTypeID = 3 and tbl_entity.InstituteID = ".$InstituteID);		

		$SubCategoryData = $query->result_array();
		$result = array();

		foreach ($SubCategoryData as $k => $v) 
		{
			$result[$k]['CategoryName'] = $v['CategoryName'];
			$result[$k]['CategoryGUID'] = $v['CategoryGUID'];

			//echo "SELECT qa.QtAssignGUID,qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qa.StartTime, qa.EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID FROM tbl_question_paper as qp JOIN tbl_question_paper_assign as qa ON qp.QtPaperID = qa.QtPaperID  where qp.SubjectID = ".$v['CategoryID']." and qp.QuestionsGroup = ".$QuestionsGroup." and qa.QtAssignBatch = (select BatchID from tbl_students where StudentID = '".$EntityID."')";

			if(!empty($QuestionsGroup))
			{
				$up_test = "";
				if($QuestionsGroup == 2 || $QuestionsGroup == 3)
				{
					$up_test = " AND DATE(qa.Date) >= CURDATE() ";
				}

				$query = $this->db->query("SELECT qa.QtAssignGUID,qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qa.StartTime, qa.EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, qa.Date as TestDate, qa.QtAssignID, qa.ActivatedFrom, qa.ActivatedTo 
					FROM tbl_question_paper as qp 
					JOIN tbl_question_paper_assign as qa ON qp.QtPaperID = qa.QtPaperID  
					where qp.SubjectID = ".$v['CategoryID']."  and qp.QuestionsGroup = ".$QuestionsGroup." and qa.QtAssignBatch = (select BatchID from tbl_students where StudentID = '".$EntityID."' limit 1) $up_test ");
			}
			else
			{
				$query = $this->db->query("SELECT qa.QtAssignGUID,qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qa.StartTime, qa.EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, qa.Date as TestDate, qa.QtAssignID, qa.ActivatedFrom, qa.ActivatedTo 
					FROM tbl_question_paper as qp 
					JOIN tbl_question_paper_assign as qa ON qp.QtPaperID = qa.QtPaperID  
					where qp.SubjectID = ".$v['CategoryID']." and qa.QtAssignBatch = (select BatchID from tbl_students where StudentID = '".$EntityID."' limit 1)");
			}
			

			$data = $query->result_array();

			//echo $this->db->last_query(); die;
			//echo count($data); die;

			if(count($data) > 0){

				foreach ($data as $key => $value) 
				{
					/*$QtBankIDs = $value['EasyLevelQuestionsID'].','.$value['HighLevelQuestionsID'].','.$value['ModerateLevelQuestionsID'].','.$value['QuestionsID'];
					$QtBankIDs =  array_unique(explode(',', $QtBankIDs));
					$QtBankIDs = implode(",", $QtBankIDs);*/

					$QtBankIDsArr = array();
					$QtBankIDsArr[] = ($value['EasyLevelQuestionsID']) ? $value['EasyLevelQuestionsID'] : 0;
					$QtBankIDsArr[] = ($value['HighLevelQuestionsID']) ? $value['HighLevelQuestionsID']: 0;
					$QtBankIDsArr[] = ($value['ModerateLevelQuestionsID']) ? $value['ModerateLevelQuestionsID']: 0;

					$QtBankIDsArr =  array_unique($QtBankIDsArr);

					$QtBankIDs = implode(",", $QtBankIDsArr);
					if(isset($value['QuestionsID']) && !empty($value['QuestionsID']))
					{
						$QtBankIDs = $QtBankIDs.",".$value['QuestionsID'];
					}

					
					//echo "<br/>QtBankIDs=".$QtBankIDs;
					if($QtBankIDs)
					{
						//echo "<br/>Inn===IF";

						//echo "SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")";

						$sql = "SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")";

						//echo "<br/>sql=".$sql;

						$QuestionsMarks = $this->db->query($sql);

						//echo "<br/>num_rows=".$QuestionsMarks->num_rows();

						if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows())
						{
							$QuestionsMarks = $QuestionsMarks->result_array();

							//echo "<pre>"; print_r($QuestionsMarks);

							$result[$k]['papers'][$key]['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
						}
						else
						{
							$result[$k]['papers'][$key]['TotalMarks'] = 0;
						}
					}
					else
					{
						//echo "<br/>Inn===else";
						$result[$k]['papers'][$key]['TotalMarks'] = 0;
					}

					//Extra Condition Code For API------------------------------------------
					//echo "<br/>TotalMarks Before=".$result[$k]['papers'][$key]['TotalMarks'];

					if(!isset($result[$k]['papers'][$key]['TotalMarks']) || empty($result[$k]['papers'][$key]['TotalMarks']))
					{
						$result[$k]['papers'][$key]['TotalMarks'] = 0;
					}

					//echo "<br/>TotalMarks After=".$result[$k]['papers'][$key]['TotalMarks'];

 
					$result[$k]['papers'][$key]['QtAssignGUID'] = $value['QtAssignGUID'];
					$result[$k]['papers'][$key]['QtPaperTitle'] = $value['QtPaperTitle'];
					$result[$k]['papers'][$key]['NegativeMarks'] = $value['NegativeMarks'];
					$result[$k]['papers'][$key]['PassingMarks'] = $value['PassingMarks'];
					$result[$k]['papers'][$key]['QuestionsGroup'] = $value['QuestionsGroup'];
					$result[$k]['papers'][$key]['TotalQuestions'] = $value['TotalQuestions'];
					$result[$k]['papers'][$key]['QtPaperGUID'] = $value['QtPaperGUID'];					
					$result[$k]['papers'][$key]['StartTime'] = $value['StartTime'];
					$result[$k]['papers'][$key]['EndTime'] = $value['EndTime'];


					$result[$k]['papers'][$key]['ActivatedFrom'] = $value['ActivatedFrom'];
					$result[$k]['papers'][$key]['ActivatedTo'] = $value['ActivatedTo'];

					$result[$k]['papers'][$key]['TestDate'] = $value['TestDate'];
					$result[$k]['papers'][$key]['TestName'] = $result[$k]['CategoryName'];


					$TodayDate = date("Y-m-d");
					$etime = $TodayDate." ".$result[$k]['papers'][$key]['EndTime'];
					$stime = $TodayDate." ".$result[$k]['papers'][$key]['StartTime'];
					$diff = (strtotime($etime) - strtotime($stime));					
					$result[$k]['papers'][$key]['TestDurationSec'] = ($diff * 12345);


					$TestDate = $result[$k]['papers'][$key]['TestDate'];
					$TodayDate = date("Y-m-d");
					$CurrTime = time();
					$stime = strtotime($TodayDate." ".$result[$k]['papers'][$key]['ActivatedFrom']);
					$etime = strtotime($TodayDate." ".$result[$k]['papers'][$key]['ActivatedTo']);		
					
					if($TestDate < $TodayDate)
					{
						$result[$k]['papers'][$key]['EnableTestBtn'] = 2;
					}
					elseif($stime <= $CurrTime && $CurrTime <= $etime && $TestDate == $TodayDate)
					{
						$result[$k]['papers'][$key]['EnableTestBtn'] = 1;
					}
					else
					{
						$result[$k]['papers'][$key]['EnableTestBtn'] = 0;
					}


					//echo 'SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$value['QtPaperID'].'" and StudentID = "'.$EntityID.'"';

					$this->db->select("ResultID");
					$this->db->from("tbl_test_results");
					$this->db->where("QtPaperID",$value['QtPaperID']);
					$this->db->where("QtAssignID",$value['QtAssignID']);
					$this->db->where("StudentID",$EntityID);
					$query_test_attempt = $this->db->get();

					$result[$k]['papers'][$key]['test_attempt'] = 0;

					if($query_test_attempt->num_rows() > 0){
					 	$result[$k]['papers'][$key]['test_attempt'] = 1;						
					}

					//print_r($result[$k]); die;
					
				}
			}
		}



		return $result;
	}


	function getTestFullInfoByPaperID($EntityID, $QtPaperID, $Where=array(), $QtAssignID=""){

			$append = ""; $result = array();
			if(!empty($Where['QuestionsGroup'])){
				$append .= "and qp.QuestionsGroup = ".$Where['QuestionsGroup'];
			}
			if(!empty($QtPaperID)){
				$append .= " and qp.QtPaperID = ".$QtPaperID;
			}
			if(!empty($QtAssignID)){
				$append .= " and qa.QtAssignID = ".$QtAssignID;
			}

			$query = $this->db->query("SELECT qa.QtAssignGUID,qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qa.StartTime, qa.EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, qa.Date as TestDate, qa.QtAssignID, qp.SubjectID,qp.CourseID
				FROM tbl_question_paper as qp 
				JOIN tbl_question_paper_assign as qa ON qp.QtPaperID = qa.QtPaperID  
				where  qa.QtAssignBatch = (select BatchID from tbl_students where StudentID = '".$EntityID."' limit 1) ".$append);			
			

			$data = $query->result_array();

			if(Count($data) > 0){
				foreach ($data as $key => $value) 
				{

					$result[$key]['SubjectName'] = $this->Common_model->getCategoryNameByID($value['SubjectID']);
					$result[$key]['CourseName'] = $this->Common_model->getCategoryNameByID($value['CourseID']);

					$QtBankIDsArr = array();
					$QtBankIDsArr[] = ($value['EasyLevelQuestionsID']) ? $value['EasyLevelQuestionsID'] : 0;
					$QtBankIDsArr[] = ($value['HighLevelQuestionsID']) ? $value['HighLevelQuestionsID']: 0;
					$QtBankIDsArr[] = ($value['ModerateLevelQuestionsID']) ? $value['ModerateLevelQuestionsID']: 0;

					$QtBankIDsArr =  array_unique($QtBankIDsArr);

					$QtBankIDs = implode(",", $QtBankIDsArr);
					if(isset($value['QuestionsID']) && !empty($value['QuestionsID']))
					{
						$QtBankIDs = $QtBankIDs.",".$value['QuestionsID'];
					}

					

					if($QtBankIDs)
					{

						//echo "SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")";

						$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

						//print_r($QuestionsMarks); 

						if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows())
						{
							$QuestionsMarks = $QuestionsMarks->result_array();

							//print_r($QuestionsMarks); 

							$result[$key]['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
						}
						else
						{
							$result[$key]['TotalMarks'] = 0;
						}
					}
					else
					{
						$result[$key]['TotalMarks'] = 0;
					}

					//Extra Condition Code For API------------------------------------------
					if(!isset($result[$key]['TotalMarks']) || empty($result[$key]['TotalMarks']))
					{
						$result[$key]['TotalMarks'] = 0;
					}

 
					$result[$key]['QtAssignGUID'] = $value['QtAssignGUID'];
					$result[$key]['QtPaperTitle'] = $value['QtPaperTitle'];
					$result[$key]['NegativeMarks'] = $value['NegativeMarks'];
					$result[$key]['PassingMarks'] = $value['PassingMarks'];
					$result[$key]['QuestionsGroup'] = $value['QuestionsGroup'];
					$result[$key]['TotalQuestions'] = $value['TotalQuestions'];
					$result[$key]['QtPaperGUID'] = $value['QtPaperGUID'];					
					$result[$key]['StartTime'] = $value['StartTime'];
					$result[$key]['EndTime'] = $value['EndTime'];
					
					$result[$key]['TestDate'] = $value['TestDate'];
					$result[$key]['TestName'] = $result[$key]['CategoryName'];


					$TodayDate = date("Y-m-d");
					$etime = $TodayDate." ".$result[$key]['EndTime'];
					$stime = $TodayDate." ".$result[$key]['StartTime'];
					$diff = (strtotime($etime) - strtotime($stime));					

					$result[$key]['TestDurationSec'] = ($diff * 12345);

					//echo 'SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$value['QtPaperID'].'" and StudentID = "'.$EntityID.'"';

					$this->db->select("*");
					$this->db->from("tbl_test_results");
					$this->db->where("QtPaperID",$value['QtPaperID']);
					$this->db->where("QtAssignID",$value['QtAssignID']);
					$this->db->where("StudentID",$EntityID);
					$query_count_test_attempt = $this->db->get();

					//echo $this->db->last_query(); //die;

					//$query = $this->db->query('SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$value['QtPaperID'].'" and StudentID = "'.$EntityID.'"');

					if($query_count_test_attempt->num_rows() > 0){
						$result[$key]['test_attempt'] = $query_count_test_attempt->num_rows();
						$resArr = $query_count_test_attempt->result_array();
						$result[$key]['SkipQuestions'] =  $resArr[0]['SkipQuestions'];
						$result[$key]['AttemptQuestions'] =  $resArr[0]['AttemptQuestions'];
						$result[$key]['CorrectAnswers'] =  $resArr[0]['CorrectAnswers'];
						$result[$key]['WrongAnswers'] =  $resArr[0]['WrongAnswers'];
						$result[$key]['GainedMarks'] =  $resArr[0]['GainedMarks'];
						$result[$key]['AttemptStartTime'] =  $resArr[0]['AttemptStartTime'];
						$result[$key]['AttemptEndTime'] =  $resArr[0]['AttemptEndTime'];
					}else{
						$result[$key]['test_attempt'] = 0;
						$result[$key]['SkipQuestions'] =  "";
						$result[$key]['AttemptQuestions'] =  "";
						$result[$key]['CorrectAnswers'] =  "";
						$result[$key]['WrongAnswers'] =  "";
						$result[$key]['GainedMarks'] =  "";
						$result[$key]['AttemptStartTime'] =  "";
						$result[$key]['AttemptEndTime'] =  "";
					}

					
				}
			}
		

		return $result;
	}



	function getAssignedTestByMonth($EntityID,$QuestionsGroup=""){

		$StudentData = $this->Common_model->getStudentDetailByID("tbl_students.KeyAssignedOn,tbl_students.ActivatedOn,tbl_students.ExpiredOn",$EntityID);
		$KeyAssignedOn = date("Y-m-d",strtotime($StudentData['KeyAssignedOn']));
		$ExpiredOn = date("Y-m-d",strtotime($StudentData['ExpiredOn']));


		$start    = (new DateTime($KeyAssignedOn))->modify('first day of this month');
		$end      = (new DateTime($ExpiredOn))->modify('first day of next month');
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);

		$i = 0; $month = array(); $result = array();
		foreach ($period as $dt) {
		    $month[$i] = $dt->format("Y-m");

			$result[$i]['MonthName'] = $dt->format("Y-F");

			$append = "";
			if(!empty($QuestionsGroup)){
				$append = "and qp.QuestionsGroup = ".$QuestionsGroup;
			}
			
			$query = $this->db->query("SELECT qa.QtAssignGUID,qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qa.StartTime, qa.EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, qa.Date as TestDate, qa.QtAssignID, qp.SubjectID, qp.CourseID
				FROM tbl_question_paper as qp 
				JOIN tbl_question_paper_assign as qa ON qp.QtPaperID = qa.QtPaperID  
				where qa.Date like '%".$month[$i]."%' ".$append." and qa.QtAssignBatch = (select BatchID from tbl_students where StudentID = '".$EntityID."' limit 1)");			
			

			$data = $query->result_array();

			if(Count($data) > 0){
				foreach ($data as $key => $value) 
				{

					$result[$i]['papers'][$key]['SubjectName'] = $this->Common_model->getCategoryNameByID($value['SubjectID']);

					$result[$i]['papers'][$key]['CourseName'] = $this->Common_model->getCategoryNameByID($value['CourseID']);

					$QtBankIDsArr = array();
					$QtBankIDsArr[] = ($value['EasyLevelQuestionsID']) ? $value['EasyLevelQuestionsID'] : 0;
					$QtBankIDsArr[] = ($value['HighLevelQuestionsID']) ? $value['HighLevelQuestionsID']: 0;
					$QtBankIDsArr[] = ($value['ModerateLevelQuestionsID']) ? $value['ModerateLevelQuestionsID']: 0;

					$QtBankIDsArr =  array_unique($QtBankIDsArr);

					$QtBankIDs = implode(",", $QtBankIDsArr);
					if(isset($value['QuestionsID']) && !empty($value['QuestionsID']))
					{
						$QtBankIDs = $QtBankIDs.",".$value['QuestionsID'];
					}

					

					if($QtBankIDs)
					{

						//echo "SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")";

						$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

						//print_r($QuestionsMarks); 

						if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows())
						{
							$QuestionsMarks = $QuestionsMarks->result_array();

							//print_r($QuestionsMarks); 

							$result[$i]['papers'][$key]['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
						}
						else
						{
							$result[$i]['papers'][$key]['TotalMarks'] = 0;
						}
					}
					else
					{
						$result[$i]['papers'][$key]['TotalMarks'] = 0;
					}

					//Extra Condition Code For API------------------------------------------
					if(!isset($result[$i]['papers'][$key]['TotalMarks']) || empty($result[$i]['papers'][$key]['TotalMarks']))
					{
						$result[$i]['papers'][$key]['TotalMarks'] = 0;
					}

 
					$result[$i]['papers'][$key]['QtAssignGUID'] = $value['QtAssignGUID'];
					$result[$i]['papers'][$key]['QtPaperTitle'] = $value['QtPaperTitle'];
					$result[$i]['papers'][$key]['NegativeMarks'] = $value['NegativeMarks'];
					$result[$i]['papers'][$key]['PassingMarks'] = $value['PassingMarks'];
					$result[$i]['papers'][$key]['QuestionsGroup'] = $value['QuestionsGroup'];
					$result[$i]['papers'][$key]['TotalQuestions'] = $value['TotalQuestions'];
					$result[$i]['papers'][$key]['QtPaperGUID'] = $value['QtPaperGUID'];					
					$result[$i]['papers'][$key]['StartTime'] = $value['StartTime'];
					$result[$i]['papers'][$key]['EndTime'] = $value['EndTime'];
					
					$result[$i]['papers'][$key]['TestDate'] = $value['TestDate'];
					$result[$i]['papers'][$key]['TestName'] = $result[$i]['CategoryName'];


					$TodayDate = date("Y-m-d");
					$etime = $TodayDate." ".$result[$i]['papers'][$key]['EndTime'];
					$stime = $TodayDate." ".$result[$i]['papers'][$key]['StartTime'];
					$diff = (strtotime($etime) - strtotime($stime));					

					$result[$i]['papers'][$key]['TestDurationSec'] = ($diff * 12345);

					//echo 'SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$value['QtPaperID'].'" and StudentID = "'.$EntityID.'"';

					$this->db->select("*");
					$this->db->from("tbl_test_results");
					$this->db->where("QtPaperID",$value['QtPaperID']);
					$this->db->where("QtAssignID",$value['QtAssignID']);
					$this->db->where("StudentID",$EntityID);
					$query_count_test_attempt = $this->db->get();

					//echo $this->db->last_query(); //die;

					//$query = $this->db->query('SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$value['QtPaperID'].'" and StudentID = "'.$EntityID.'"');

					if($query_count_test_attempt->num_rows() > 0){
						$result[$i]['papers'][$key]['test_attempt'] = $query_count_test_attempt->num_rows();
						$resArr = $query_count_test_attempt->result_array();
						$result[$i]['papers'][$key]['AttemptStartTime'] =  $resArr[0]['AttemptStartTime'];
						$result[$i]['papers'][$key]['AttemptEndTime'] =  $resArr[0]['AttemptEndTime'];
					}else{
						$result[$i]['papers'][$key]['test_attempt'] = 0;
						$result[$i]['papers'][$key]['AttemptStartTime'] =  "";
						$result[$i]['papers'][$key]['AttemptEndTime'] =  "";
					}

					
				}
			}

			$i++;
		}

		return $result;
	}



	function getAssignedTestByID($EntityID,$Where=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select("*");
		$this->db->from("tbl_question_paper_assign");

		if(!empty($Where['QtAssignID'])){
			$this->db->where("QtAssignID",$Where['QtAssignID']);
			$this->db->limit(1);
		}

		$data = $this->db->get();

		if($data->num_rows() > 0){
			return $data = $data->result_array();
		}else{
			return FALSE;
		}
	}


	function editAssignPaper($EntityID, $Input){

		//print_r($Input); die;
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$AFT = strtotime($Input['ActivatedFrom']);
		$ATT = strtotime($Input['ActivatedTo']);
		
		$ST = strtotime($Input['StartTime']);
		$ET = strtotime($Input['EndTime']);
		
		if($AFT >= $ATT)
		{
			return array("exist","Activated From time should be less than of Activated To time.");
		}
		elseif($AFT > $ST || $ST > $ATT)
		{
			return array("exist","Start time should be between Activated From and To time.");
		}
		elseif($AFT > $ET || $ET > $ATT)
		{
			return array("exist","End time should be between Activated From and To time.");
		}
		elseif($ST >= $ET)
		{
			return array("exist","Start time should be less than of End time.");
		}
		elseif(strtotime($Input['StartTime']) == strtotime($Input['EndTime'])){
			return array("exist","Start time and end time can not be same.");
		}else if(strtotime($Input['StartTime']) > strtotime($Input['EndTime'])){
			return array("exist","End time should be ahead of start time.");
		}
		else{
				
			$query = $this->db->query("select QtAssignID from tbl_question_paper_assign join tbl_entity ON tbl_entity.EntityID = tbl_question_paper_assign.QtAssignID  where tbl_entity.InstituteID = '".$InstituteID."' and QtAssignID != '".$Input['QtAssignID']."' and QtAssignBatch = '".$Input['QtAssignBatch']."' and StartTime  BETWEEN '".date("H:i:s",strtotime($Input['StartTime']))."' AND '".date("H:i:s",strtotime($Input['EndTime']))."' and EndTime  BETWEEN '".date("H:i:s",strtotime($Input['StartTime']))."' AND '".date("H:i:s",strtotime($Input['EndTime']))."' and date = '".date("Y-m-d",strtotime($Input['Date']))."'");


			$QA = $query->result_array();
			//echo $this->db->last_query();
			if(count($QA) > 0){
				return array("exist","This time duration is not available for this batch.");
			}else{
				$InsertData = array_filter(array(
					"QtPaperID" =>	@$Input['QtPaperID'],
					"QtAssignBatch" 		=>	@$Input['QtAssignBatch'],
					"Date"  => date("Y-m-d",strtotime($Input['Date'])),
					"StartTime" 		=>	date("H:i:s",strtotime($Input['StartTime'])),
					"EndTime" 		=>	date("H:i:s",strtotime($Input['EndTime'])),

					"ActivatedFrom" =>	date("H:i:s",strtotime($Input['ActivatedFrom'])),
					"ActivatedTo" 	=>	date("H:i:s",strtotime($Input['ActivatedTo']))

				));

				$this->db->where('QtAssignID',$Input['QtAssignID']);
				$this->db->update('tbl_question_paper_assign', $InsertData);

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return array("exist","This time duration is not available for this batch.");
				}
				return TRUE;
			}
			
		}
	}


	function delete($EntityID, $Input)
	{
		$this->db->where('QtAssignID',$Input['QtAssignID']);
		$this->db->delete('tbl_question_paper_assign');
		return TRUE;
	}



	function getBatchAssignedFaculty($EntityID)
	{
		$bulk_data = array();

		/*$sql = "SELECT b.BatchID, b.BatchName, b.BatchGUID, c.CategoryID as SubjectID, c.CategoryGUID, c.CategoryName
		FROM tbl_batch b
		INNER JOIN tbl_batchbyfaculty f ON f.BatchID = b.BatchID AND f.FacultyID = $EntityID
		INNER JOIN set_categories c ON b.CourseID = c.ParentCategoryID AND c.CategoryTypeID = 3
		GROUP BY b.BatchID, c.ParentCategoryID";*/

		$sql = "SELECT b.BatchID, b.BatchName, b.BatchGUID
		FROM tbl_batch b
		INNER JOIN tbl_batchbyfaculty f ON f.BatchID = b.BatchID AND f.FacultyID = $EntityID		
		GROUP BY b.BatchID";

		$BulkQuery = $this->db->query($sql);

		if($BulkQuery->num_rows()>0)
		{
			foreach($BulkQuery->result_array() as $Where)
			{
				
				$this->db->select("*,course.CategoryName as CourseName,subject.CategoryName as SubjectName");
				$this->db->from('tbl_question_paper_assign');
				$this->db->join('tbl_question_paper','tbl_question_paper_assign.QtPaperID = tbl_question_paper.QtPaperID');
				$this->db->join('set_categories as course','course.CategoryID = tbl_question_paper.	CourseID');
				$this->db->join('set_categories as subject','subject.CategoryID = tbl_question_paper.	SubjectID');
				$this->db->where("tbl_question_paper_assign.QtAssignBatch",$Where['BatchID']);				

				$Query = $this->db->get();
				$indata = array();
				if($Query->num_rows() > 0)
				{
					$data = $Query->result_array();
					if(count($data) > 0)
					{ 
						foreach($data as $Record)
						{
							//print_r($Record);
							$query_count_students = $this->db->query("SELECT count(st.StudentID) as count_students FROM tbl_students as st where st.BatchID = ".$Record['QtAssignBatch']);

							$count_students = $query_count_students->result_array();

							$Record['TotalStudents'] = $count_students[0]['count_students'];

							$QtBankIDs = "";
							
							if(!empty($Record['QuestionsID']))
							{
								$QtBankIDs = $QtBankIDs.",".$Record['QuestionsID'];
							}else{
								$QtBankIDsArr = array();
								if(!empty($Record['EasyLevelQuestionsID'])){
									$QtBankIDsArr[] = $Record['EasyLevelQuestionsID'];
								}
								if(!empty($Record['HighLevelQuestionsID'])){
									$QtBankIDsArr[] = $Record['HighLevelQuestionsID'];
								}
								if(!empty($Record['ModerateLevelQuestionsID'])){
									$QtBankIDsArr[] = $Record['ModerateLevelQuestionsID'];
								}
								if(!empty($QtBankIDsArr)){
									$QtBankIDsArr =  array_unique($QtBankIDsArr);
									$QtBankIDs = implode(",", $QtBankIDsArr);
								}else{
									$QtBankIDs = "";
								}								
							}

							
							
							if(!empty($QtBankIDs))
							{
								$QtBankIDs = trim($QtBankIDs,",");
								$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

								
								if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows()){
									$QuestionsMarks = $QuestionsMarks->result_array();

									$Record['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
								}
								else
								{
									$Record['TotalMarks'] = 0;
								}
							}
							else
							{
								$Record['TotalMarks'] = 0;
							}				


							$Record['StartTime'] = date('h:i:s',strtotime($Record['StartTime']));


							$Record['EndTime'] = date('h:i:s',strtotime($Record['EndTime']));

							

							$query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"');

							$result = $query->result_array();

							$Record['QuestionsGroup'] = $result[0]['QuestionGroupName'];


							$query = $this->db->query('SELECT EntryDate FROM tbl_entity where EntityID = "'.$Record['QtAssignID'].'"');

							$result = $query->result_array();

							$Record['AssignedDate'] = date('d-m-Y',strtotime($result[0]['EntryDate']));

							

							$indata[] = $Record;
						}						 
					}
				}

				$bulk_data[] = array("BatchName" => $Where['BatchName'], "TestList" => $indata);
				
			}

			return $bulk_data;
		}

		return FALSE;	
	}
	
}