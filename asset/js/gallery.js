app.controller('PageController', function($scope, $http, $timeout) {

    $scope.photos = [
        {src: 'http://farm9.staticflickr.com/8042/7918423710_e6dd168d7c_b.jpg', desc: 'Image 01'},
        {src: 'http://farm9.staticflickr.com/8449/7918424278_4835c85e7a_b.jpg', desc: 'Image 02'},
        {src: 'http://farm9.staticflickr.com/8457/7918424412_bb641455c7_b.jpg', desc: 'Image 03'},
        {src: 'http://farm9.staticflickr.com/8179/7918424842_c79f7e345c_b.jpg', desc: 'Image 04'},
        {src: 'http://farm9.staticflickr.com/8315/7918425138_b739f0df53_b.jpg', desc: 'Image 05'},
        {src: 'http://farm9.staticflickr.com/8461/7918425364_fe6753aa75_b.jpg', desc: 'Image 06'}
    ];
   

    $scope.data.pageSize = 100;
    $scope.data.dataList = [];
    $scope.data.filterDataA = [];
    $scope.data.filterDataC = [];
    $scope.APIURL = API_URL + "upload/image";

    /*list*/
    $scope.getFilterData = function(Category = "") {
        $scope.getCategory();
        $scope.getSecondLevelPermission();
    }


    $scope.applyFilter = function() {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getCategory();
        $scope.getGalleryStatistics();
    }


    $scope.getGalleryStatistics = function(check="",Category="") {
        //if ($scope.data.listLoading || $scope.data.noRecords) return;
        if (check != 1) {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        } else {
            $scope.Category = Category;
            $scope.data.pageNo = 1;
            $scope.data.dataList = [];
        }


        $scope.data.listLoading = true;

        var data = 'SessionKey=' + SessionKey + '&Category=' + Category + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize();

        $http.post(API_URL + 'gallery/getGalleryStatistics', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.length) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) {

                    $scope.data.dataList.push(response.Data[i]);

                }
                 console.log($scope.data.dataList);
                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;

            // $timeout(function(){  }, 800);

        });

    }

    /*list append*/



    $scope.getList = function(Category = "") {
        if (Category == '') {

            $scope.data.pageSize = 20;
            //  if ($scope.data.listLoading || $scope.data.noRecords) return;
        } else {
            $scope.Category = Category;
            $scope.data.pageNo = 1;
            $scope.data.dataList = [];

        }


        $scope.data.listLoading = true;

        var data = 'SessionKey=' + SessionKey + '&Category=' + Category + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize();

        $http.post(API_URL + 'gallery/getgallerys', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }
                // console.log($scope.data.dataList);
                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;

            // $timeout(function(){  }, 800);

        });

    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) {

        console.log(API_URL);

        $scope.AlbumID = "";

        $scope.AlbumName = "";

        $scope.Categories = [];
        $scope.Category = [];
        $scope.data.pageLoading = true;

        $scope.data.Position = Position;

        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();

        $scope.data.pageLoading = false;

        $('#add_model').modal({
            show: true
        });

        $timeout(function() {

            tinymce.init({
                selector: '.MediaContent'
            });

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");


        }, 700);
    }


    /*load add form*/
    $scope.loadFormAddPhoto = function(AlbumID,AlbumName) {
        $scope.Categories = [];

        $scope.Category = [];

        $scope.data.pageLoading = true;

        $scope.AlbumID = AlbumID;

        $scope.AlbumName = AlbumName;

        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();

        $scope.data.pageLoading = false;

        $('#add_model').modal({
            show: true
        });

        $timeout(function() {

            tinymce.init({
                selector: '.MediaContent'
            });

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");


        }, 700);
    }


    /*load add form*/
    $scope.loadFormAddCategory = function(Position) {
        $scope.addDataLoadingc = false;

        $scope.formtype = Position;

        if ($scope.formtype == 1) {

            $('#edits_model .close').click();

        } else {

            $('#add_model .close').click();
        }


        $scope.templateURLCategory = PATH_TEMPLATE + module + '/addcategory_form.htm?' + Math.random();

        $('#addcategory_model').modal({
            show: true
        });

        $timeout(function() {

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 200);



    }
    /*add album*/
    /*get set category book*/
    $scope.getCategory = function()

    {

        var data = 'SessionKey=' + SessionKey;
        $scope.data.CategoryData = [];
        $http.post(API_URL + 'gallery/getCategory', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */


                for (var i in response.Data.Records) {

                    $scope.data.CategoryData.push(response.Data.Records[i]);
                }

                $timeout(function() {

                    $("select.chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8
                    }).trigger("chosen:updated");

                }, 200);
            }

        });


    }
    /*add category book*/

    $scope.addCategoryData = function() {
        $scope.NewCategory = $('.Category').val();

        //alert($scope.Category);

        $scope.addDataLoadingc = true;

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='category_form']").serialize();

        $http.post(API_URL + 'gallery/addcategory', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.getCategory();

                $('#addcategory_model .close').click();

                $scope.getGalleryStatistics(1);

                if ($scope.Categories.length > 0) {
                    $scope.Categories.push($scope.NewCategory);
                    $scope.Category = $scope.Categories;
                    $scope.Categories = [];
                } else {
                    $scope.Category.push($scope.NewCategory);
                    $scope.Categories = $scope.Category;
                    $scope.Category = [];
                }
                if ($scope.formtype == 1) {

                    $timeout(function() {

                        $('#edits_model').modal({
                            show: true
                        }); // console.log($scope.Categories);
                        $(".chosen-select").chosen({
                            width: '100%',
                            "disable_search_threshold": 8,
                            "placeholder_text_multiple": "Please Select",
                        }).trigger("chosen:updated");

                    }, 800);
                } else {

                    $timeout(function() {
                        $('#add_model').modal({
                            show: true
                        });
                        $(".chosen-select").chosen({
                            width: '100%',
                            "disable_search_threshold": 8,
                            "placeholder_text_multiple": "Please Select",
                        }).trigger("chosen:updated");
                    }, 800);
                }

            } else {

                ErrorPopup(response.Message);

            }

            $scope.addDataLoadingc = false;

        });

    }

    $scope.pushCategory = function(NewCate) {

        if ($scope.Categories.length > 0) {
            $scope.Category = NewCate;
            $scope.Categories = [];
        } else {
            $scope.Categories = NewCate;
            $scope.Category = [];
        }
        $timeout(function() {
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");
        }, 800);
    }
    /*load edit form*/



    $scope.loadFormEdit = function(Position, MediaGUID) {
         $scope.editDataLoadingc = false;
        $scope.data.Position = Position;
        $scope.Categories = [];
        $scope.Category = [];
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'gallery/getgalleryBy', 'SessionKey=' + SessionKey + '&MediaGUID=' + MediaGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
                $scope.Category = $scope.formData.CategoryID.split(",");

                $('#edits_model').modal({
                    show: true
                });


                $timeout(function() {

                    tinymce.init({
                        selector: '#MediaContent'
                    });

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 700);

            }

        });

    }



    $scope.loadFormView = function(Position, AlbumID) {

        $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE + module + '/view_category.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'gallery/getCategory', 'SessionKey=' + SessionKey + '&AlbumID=' + AlbumID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data.Records[0];

                $('#view_model').modal({
                    show: true
                });
            }

        });
    }


    $scope.loadPhotoGallery = function(Position, AlbumID, AlbumName) {

        $scope.data.Position = Position;

        $scope.AlbumID = AlbumID;
        $scope.AlbumName = AlbumName;

        $scope.templateURLViewGallery = PATH_TEMPLATE + module + '/view_gallery.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'gallery/getPhotosGallery', 'SessionKey=' + SessionKey + '&AlbumID=' + AlbumID + '&AlbumName=' + AlbumName, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                if(response.Data.Records){
                    $scope.photos = response.Data.Records;
                    $('#view_gallery_model').modal({
                        show: true
                    });
                }else{
                    ErrorPopup("This album is empty.");
                }

                console.log($scope.formData);

                
            }

        });

    }


    $scope.loadFormEditAlbum = function(Position, AlbumID) {

        
        
        $scope.data.Position = Position;

        $scope.templateURLEditAlbum = PATH_TEMPLATE + module + '/edit_category_form.htm?' + Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL + 'gallery/getCategory', 'SessionKey=' + SessionKey + '&AlbumID=' + AlbumID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data.Records[0];
                
                console.log($scope.formData);

                $('#edit_album_model').modal({
                    show: true
                });
            }

        });

    }




    $scope.addData = function() {
        $scope.addDataLoading = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();
        $http.post(API_URL + 'gallery/add', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);
                $scope.applyFilter();
                $timeout(function() {

                    $('#add_model .close').click();

                }, 200);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }


    /*delete*/
    $scope.loadFormMediaDelete = function(Position, MediaGUID) {

        $scope.deleteDataLoading = true;
        //$scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'gallery/delete', 'SessionKey=' + SessionKey + '&MediaGUID=' + MediaGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */
                    SuccessPopup(response.Message);
                    // $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    // $scope.data.totalRecords--;
                     $('#view_gallery_model .close').click();
                     $scope.getGalleryStatistics(1);
                    $timeout(function() {
                            $scope.loadPhotoGallery($scope.data.Position, $scope.AlbumID, $scope.AlbumName);
                    }, 200);
                }

            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }
    /*edit data*/


    $scope.editData = function() {

        $scope.editDataLoading = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();

        $http.post(API_URL + 'gallery/editgallery', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position] = response.Data;

                $('#edits_model .close').click();
            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });

    } /*PermissionData data*/



    $scope.editAlbum = function() {

        

        $scope.editDataLoadingc = true;

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='category_form']").serialize();

        $http.post(API_URL + 'gallery/EditAlbum', data, contentType).then(function(response) {
            var response = response.data;
            
            $scope.editDataLoadingc = false;

            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);

                $scope.getGalleryStatistics(1);

                $('#edit_album_model .close').click();
            } else {

                ErrorPopup(response.Message);

            }

        });

    } /*PermissionData data*/


    /*delete*/
    $scope.DeleteAlbum = function(Position, AlbumID, AlbumName) {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL + 'gallery/DeleteAlbum', 'SessionKey=' + SessionKey + '&AlbumID=' + AlbumID + '&AlbumName=' + AlbumName, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    //$('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }

});

function getCategorybyList(Category) {

    angular.element(document.getElementById('content-body')).scope().getGalleryStatistics(1,Category);

}




/* sortable - ends */