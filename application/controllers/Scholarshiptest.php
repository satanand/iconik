<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Scholarshiptest  extends Admin_Controller_Secure 
{
	public function index()
	{
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

			'asset/plugins/timepicker/jquery.timepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'

		);

		$load['js']=array(

			'asset/js/questionpaper.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',	
			
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.min.js',

			'asset/plugins/timepicker/jquery.timepicker.min.js'


		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Scholarship Test" => "",			
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('scholarshiptest/listing');

		$this->load->view('includes/footer');
	}


	public function generate_paper()
	{
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',			

			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);

		$load['js']=array(

			'asset/js/questionpaper.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',				

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',				

			'https://code.jquery.com/ui/1.12.1/jquery-ui.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Scholarship Test" =>  base_url()."scholarshiptest",
			"Generate Paper" =>  ""
		);

		

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('scholarshiptest/generate_paper');

		$this->load->view('includes/footer');

	}


	public function view_paper()
	{
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			//'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',			

			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);

		$load['js']=array(

			'asset/js/questionpaper.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',				

			//'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',				

			'https://code.jquery.com/ui/1.12.1/jquery-ui.js',

			//'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			//'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Scholarship Test" =>  base_url()."scholarshiptest",
			"Generate Paper" =>  base_url()."scholarshiptest/generate_paper",
			"View Paper" =>  ""
		);

		

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('scholarshiptest/view_paper');

		$this->load->view('includes/footer');

	}



	public function applicants()
	{
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',			

			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);

		$load['js']=array(

			'asset/js/questionpaper.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',				

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',				

			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Scholarship Test" =>  base_url()."scholarshiptest",
			"Applicants" =>  ""
		);

		 

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('scholarshiptest/applicants');

		$this->load->view('includes/footer');

	}


	public function paper_export()
	{
		$data['listing_data'] = array();
		
		if(isset($_GET['QtPaperGUID']) && !empty($_GET['QtPaperGUID']) && isset($_SESSION['UserData']['SessionKey']) && !empty($_SESSION['UserData']['SessionKey']))
		{
			$JSON = json_encode(array("QtPaperGUID" => $_GET['QtPaperGUID'], "SessionKey" => $_SESSION['UserData']['SessionKey']));

			$Response = APICall(API_URL.'questionpaper/viewPaper', $JSON);	
			
			$data['listing_data'] = $Response;
			$data['session_data'] = $_SESSION['UserData'];

			if($Response['ResponseCode'] == 200)
			{
				$html = $this->load->view('scholarshiptest/export_paper', $data, true);
				
				$header_title = ucfirst($data['listing_data']['Data']['QtPaperTitle']);

				$pdfFilePath = $header_title."_".date('d-M-Y').".pdf";
		        
		        $this->load->library('m_pdf');
		        
		 		$this->m_pdf->pdf->autoScriptToLang = true;
				$this->m_pdf->pdf->baseScript = 1;
				$this->m_pdf->pdf->autoVietnamese = true;
				$this->m_pdf->pdf->autoLangToFont = true;

		 		$this->m_pdf->pdf->setFooter($header_title." (".'{PAGENO}'.")");
		       	
		        $this->m_pdf->pdf->WriteHTML($html);		 
		        
		        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
			}
			else
			{
				$msg = $Response['Message'];
				echo "<script> alert('".$msg."'); window.history.go(-1); </script>";
				die;
			}
		}
	}


	public function results()
	{
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',			

			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);

		$load['js']=array(

			'asset/js/questionpaper.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',				

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',				

			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Scholarship Test" =>  base_url()."scholarshiptest",
			"Results" =>  ""
		);

		

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('scholarshiptest/results');

		$this->load->view('includes/footer');

	}
	


	

}
