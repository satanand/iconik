<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}

	#ui-id-1
	{
		z-index: 9999 !important;
		overflow-x: hidden;
		overflow-y: auto;
		max-height: 150px;
	}
</style>  
<header class="panel-heading"> <h1 class="h4">Manage Products</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData();" id="content-body"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<span class="h5">Total Products: {{data.dataList.length}}</span>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<div class="float-right">
						<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 87 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add New Product</button>

						<button class="btn btn-success btn-sm ml-1" ng-click="loadFormImport();" ng-if="(UserTypeID == 10 || UserTypeID == 87 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Import Product</button>
					</div>
				</div>
			</div>
		</div>


		<form id="filterForm" role="form" autocomplete="off">
		<div class="row">		
		

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterStartDate" type="text" placeholder="Created Start Date" id="FilterStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterEndDate" type="text" placeholder="Created End Date" id="FilterEndDate" class="form-control">
			</div>
		</div>
				

		<div class="col-md-3">
			<div class="form-group">			 	
	        	<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Search by word" >
	    	</div>
		</div>

		
		<div class="col-md-3">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" ng-click="getList(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_filters();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>
		

		

	</div>
	</form>		

	</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList(0);" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading --> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 50px;">Image</th>					
					<th style="width: 150px;">Product Name</th>
					<th style="width: 70px;">Unit Price</th>
					<th style="width: 50px;">Tax</th> 					
					<th style="width: 80px;">Is Downlodable</th>
					<th style="width: 100px;">Quantity Available</th>
					<th style="width: 70px;">Quantity Sold</th>
					<th style="width: 100px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.ProductID}}">

					<td>
						<a ng-if="row.ProductImageURL != ''" href="" ng-click="FileInModalBoxLocal(row.ProductImageURL);" target="_blank">
							<img width="30px" height="30px" src="{{row.ProductImageURL}}" alt="{{row.ProductName}}">
						</a>

						<img ng-if="row.ProductImageURL == ''" width="30px" height="30px" src="./asset/img/default-product.png" alt="{{row.ProductName}}">

					</td>
					
					<td>{{row.ProductName}}</td>

					<td>{{row.ProductPrice}}</td>

					<td>{{row.Tax}}</td>					

					<td>
						<span ng-if="row.IsDownlodable == 1">Yes</span>
						<span ng-if="row.IsDownlodable == 0">No</span>
					</td>

					<td>{{row.ProductQtyAvail}}</td>

					<td>
						<a ng-if="row.SoldQuantity > 0" class="" href="" ng-click="loadSoldProductDetails(row.ProductID);" data-toggle="tooltip" title="View">{{row.SoldQuantity}}</a>

						<label ng-if="row.SoldQuantity <= 0">0</label>

					</td>

					<td class="text-center" style="text-align: center;">
 
						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.ProductID, row)" data-toggle="tooltip" title="View"></a>
						
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.ProductID, row)" data-toggle="tooltip" title="Edit"></a>
						
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.ProductID)" data-toggle="tooltip" title="Delete"></a>
							
					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->



	
	<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Product</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="import_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add New Product</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div autocomplete="off" ng-include="templateURLImport">
				</div>
				<!-- /form -->
			</div>
		</div>
	</div>

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add New Product</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div autocomplete="off" ng-include="templateURLAdd">
				</div>
				<!-- /form -->
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Product</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div autocomplete="off" ng-include="templateURLEdit">
				</div>
				
				<!-- /form -->
			</div>
		</div>
	</div>


<!-- View Modal -->
<div class="modal fade" id="view_order_model">
<div class="modal-dialog modal-lg" role="document" style="max-width: 90% !important;">
<div class="modal-content">
<div class="modal-header">
	<h3 class="modal-title h5">Sold Product Details</h3>     	
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

<div class="row">
<div class="col-md-12">
<table class="table table-striped table-condensed table-hover table-sortable">
<thead>
<tr>
	<th>Order ID</th>
    <th>Order Date</th>
    <th>Order Status</th>    
    <th>Customer Name</th>
    <th>Contact Details</th>
    <th>Product Name</th>    
    <th>Unit Price</th>
    <th>Ordered Qty</th>
    <th>Amount</th> 
    <th>Payable Amount</th>   
</tr>

</thead>

<tbody id="tabledivbody">

<tr scope="row" ng-repeat="(key, row) in data.SoldProductDetails" id="sectionsid_{{row.OrderID}}">

	<td>{{row.OrderID}}</td>
	
	<td>{{row.OrderDate | date}}</td>	

	<td>{{row.OrderStatus}}</td>

	<td>{{row.CustomerName}}</td>

	<td>{{row.CustomerEmail}}<br/>{{row.CustomerMobile}}</td>

	<td>{{row.ProductName}}</td> 

	<td>{{row.ProductUnitPrice}}</td>

	<td>{{row.OrderedQty}}</td>

	<td>{{row.Payouts.NetAmountSum}}</td>	

	<td>{{row.Payouts.PayableAmount}}</td>	

</tr>

<tr ng-if="SoldProductDetailsLoading">
    <td colspan="11">
        <img src="./asset/img/ajax-loader.gif" class="processingLoader">
    </td>
</tr>

<tr ng-if="data.SoldProductDetails.length <= 0 && !SoldProductDetailsLoading">
  <td colspan="11">No record found.</td>
</tr> 
</tbody>

</table>
</div>
</div>
</div>
</div>
</div>

</div><!-- Body/ -->