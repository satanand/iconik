<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/jquery.imgareaselect/css/imgareaselect-default.css',	

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'asset/plugins/jquery.imgareaselect/scripts/jquery.imgareaselect.pack.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js'

		);	



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('product/product_list');

		$this->load->view('includes/footer');

	}







}

