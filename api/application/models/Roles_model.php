<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to add new category
	*/
	function addRoles($Input){

		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->trans_start();

		$EntityGUID = get_guid();

		$this->db->select('UserTypeID');
		$this->db->from('tbl_users_type');
	    $this->db->where('LOWER(`UserTypeName`)="'.strtolower($Input['UserTypeName']).'"');
        $this->db->where('UserTypeID NOT IN (select UserTypeID from tbl_delete_usertype where InstituteID = '.$InstituteID.')');
	   
	    $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if ( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

	    	$this->db->select('UserTypeID');
			$this->db->from('tbl_users_type');
		    $this->db->where('LOWER(`UserTypeName`)="'.strtolower($Input['UserTypeName']).'"');
	        $this->db->where('UserTypeID IN (select UserTypeID from tbl_delete_usertype where InstituteID = '.$InstituteID.')');
		    $this->db->limit(1);
		    $query = $this->db->get();

		    if ( $query->num_rows() > 0 ){
		    	$data = $query->result_array();
		    	$this->db->where('UserTypeID',$data[0]['UserTypeID']);
		    	$this->db->where('InstituteID',$InstituteID);
		    	$this->db->delete('tbl_delete_usertype');
		    	return array('UserTypeName' => $Input['UserTypeName']);
		    }else{
		    	/* Add Roles */
				$InsertData = array_filter(array(
					"UserTypeName"  =>	$Input['UserTypeName'],
					"Description"  =>	@$Input['Description'],
					"EntityID" 		=>	$InstituteID,	
					"IsAdmin" 		=>	'No'					
				));
				
				$this->db->insert('tbl_users_type', $InsertData);
				$UserTypeID = $this->db->insert_id();
				$this->AddPrmissionRoles($UserTypeID,1);

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return FALSE;
				}
				return array('UserTypeID'=>$UserTypeID,'UserTypeName' => $UserTypeName);
		    }	    	
		}
	}



	/*
	Description: 	Use to update roles.
	*/
	function editRoles($UserTypeID, $UserTypeName){
		//echo $UserTypeID; die;
		//$EntityGUID = get_guid();
		$UpdateArray = array_filter(array(
			"UserTypeName" => $UserTypeName,
		));

		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('UserTypeID', $UserTypeID);
			//$this->db->limit(1);
			$data=$this->db->update('tbl_users_type', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		
		return TRUE;
	}

		/*
	Description: 	Use to Pemission add roles.
	*/
	function permissiondel($UserTypeID)
	{
		$this->db->where('UserTypeID', $UserTypeID);
        $this->db->delete('admin_user_type_permission');
        //echo $this->db->last_query();

        $this->db->where('UserTypeID', $UserTypeID);
        $this->db->delete('admin_second_level_permission');

        //echo $this->db->last_query(); die;
      
	}

	function AddPrmissionRoles($UserTypeID, $ModuleID){
      	$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select("*");
		$this->db->from("admin_user_type_permission");
		$this->db->where("UserTypeID",$UserTypeID);
		$this->db->where("ModuleID",$ModuleID);
		$this->db->where("InstituteID",$InstituteID);
		$query = $this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows() == 0){
			$InsertData = array_filter(array(
				"UserTypeID"    =>	$UserTypeID,
				"ModuleID" 		=>	$ModuleID,	
				"InstituteID" 	=>	$InstituteID,		
			));
			$this->db->insert('admin_user_type_permission', $InsertData);
			//echo $this->db->last_query();
		}

		//die;
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return TRUE;
	}


	function AddSecondLevelPermission($UserTypeID, $Action, $ModuleID)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$InsertData = array_filter(array(
			"UserTypeID"    =>	$UserTypeID,
			"ModuleID" 		=>	$ModuleID,	
			"Action"  => $Action,
			"InstituteID" 	=>	$InstituteID,
			"Permission" => 1
		));
		
		$this->db->insert('admin_second_level_permission', $InsertData);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return TRUE;
	}



	
	/*
	Description: 	Use to get Cetegories
	*/
	function getRolesd($UserTypeID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$this->db->select('*');
		
		//$this->db->where('Entity',$EntityGUID);
		$this->db->from('tbl_users_type');
		//$this->db->where("UserTypeID",$UserTypeID);
		$this->db->order_by('UserTypeID','ASC');
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				if(!$multiRecords){
					return $Record;
				}
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	


	/*
	Description: 	Use to get Cetegories
	*/
	function getRoles($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('*');		
		//$this->db->where('Entity',$EntityGUID);
		$this->db->from('tbl_users_type');
		$this->db->where('IsAdmin','No');
		$this->db->where('UserTypeID NOT IN (select UserTypeID from tbl_delete_usertype where InstituteID = '.$InstituteID.')');

		$this->db->where('UserTypeID NOT IN (90,88,83,82,87)');

		if(!empty($Where['Keyword'])){
			$this->db->like("UserTypeName", $Where['Keyword']);
		}

		if(isset($EntityID) && !empty($EntityID))
		{
			//$where = " EntityID = $EntityID OR EntityID = '' OR EntityID IS NULL ";
			//$this->db->where($where);
		}

		$this->db->order_by('UserTypeID','DESC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			 //    $this->db->select('*');
				// $this->db->from('tbl_users_type');
				// $this->db->where('IsAdmin','No');
				// $this->db->where('EntityID',$EntityID);

				// if(!empty($Where['Keyword'])){
				// 	$this->db->like("UserTypeName", $Where['Keyword']);
				// }

				// $this->db->order_by('UserTypeID','DESC');

				// $querys = $this->db->get();

				// 	foreach($querys->result_array() as $Recordes){

				// 		$Records[]=$Recordes;
				// 	}

				$Return['Data']['Records'] = $Records;
			

          //print_r($Return); die();
			return $Return;
		}

		return FALSE;		
	}



	function getSecondLevelPermission($ModuleID)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$UserTypeID = $this->Common_model->getUserTypeByEntityID($this->EntityID);
	    $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select("*");
		$this->db->from("admin_second_level_permission");
		$this->db->where("ModuleID",$ModuleID);
		$this->db->where("InstituteID",$InstituteID);
		$this->db->where("UserTypeID",$UserTypeID);
		$this->db->limit(3);
		$QUE = $this->db->get();
	    //echo $this->db->last_query(); die;
		if($QUE->num_rows() > 0){
			$data = $QUE->result_array();
			foreach($data as $Record){
			 	$Records[] = $Record;
			}

			return $Records;
		}

		return FALSE;
	}


	/*
	Description: 	Use to get Modules
	*/
	function getmodules($UserTypeID){
	     $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

	     $InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);


	     // echo "(SELECT `ModuleID`, `ControlName`,'c' as C FROM `admin_control` where `ModuleID` not in(select `ModuleID` from admin_user_type_permission where UserTypeID=$UserTypeID and ModuleID IS NOT NULL) and `ModuleID` IS NOT NULL order by ParentControlID ASC)  UNION SELECT m.`ModuleID`, m.`ControlName`,'sc' as C FROM `admin_control` m JOIN admin_user_type_permission a on m.ModuleID=a.ModuleID where UserTypeID=$UserTypeID and InstituteID=$InstituteID and m.`ModuleID` IS NOT NULL";
	     //die;
	     $Query = $this->db->query("(SELECT `ModuleID`, `ControlName`,'c' as C FROM `admin_control` where  `ModuleID` IS NOT NULL order by ParentControlID ASC)  UNION SELECT m.`ModuleID`, m.`ControlName`,'sc' as C FROM `admin_control` m JOIN admin_user_type_permission a on m.ModuleID=a.ModuleID where UserTypeID=$UserTypeID and InstituteID=$InstituteID and m.`ModuleID` IS NOT NULL");

		 // $this->db->select('ModuleID,ControlName');
		 // $this->db->from('admin_control as c');
		 // //$this->db->join('admin_user_type_permission as p','c.ModuleID = p.ModuleID','left');
		 // //$this->db->where('p.UserTypeID',$UserTypeID);
		 // $this->db->where('c.ModuleID IS NOT NULL');
		 // $this->db->order_by("c.ParentControlID",'ASC');
		 // $Query = $this->db->get();
   
		// echo $this->db->last_query(); die;
      	$TotalModule = $Query->num_rows();
		if($Query->num_rows() > 0){
			$data = $Query->result_array();
			$data = array_reverse($data);  $view = 0; $all = 0;
			foreach($data as $Record){
				//print_r($Record); die;
				if($Record['ModuleID'] == 58){
					$Record['ControlName'] = "Assign Keys to Staff";
				}else if($Record['ModuleID'] == 30){
					$Record['ControlName'] = "Assign Keys to Student";
				}
				$this->db->select("*");
				$this->db->from("admin_second_level_permission");
				$this->db->where("UserTypeID",$UserTypeID);
				$this->db->where("ModuleID",$Record['ModuleID']);
				$this->db->where("InstituteID",$InstituteID);
				$this->db->limit(3);
				$QUE = $this->db->get();
				$TotalPermission = $QUE->num_rows(); 
				
				if($QUE->num_rows() > 0){
					$SecondPermission = $QUE->result_array();
					foreach ($SecondPermission as $key => $value) {
						if($value['Action'] == "view"){
							$view++;
						}else if($value['Action'] == "all"){
							$all++;
						}
						$Record["Action"][$key] =  $value['Action'];
					}
				}

			 	$Records[] = $Record;
			}
			//echo $view; die;
			if($view == $TotalModule && $all == 0){
				$Return['Data']['type'] = "View Only";
			}else if($all == $TotalModule && $view == 0){
				$Return['Data']['type'] = "All";
			}else{
				$Return['Data']['type'] = "Customized";
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	

	/*edit get data*/
	function getEDRoles($Field='UserTypeID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select('*');
		$this->db->from('tbl_users_type');
		
		$this->db->order_by('UserTypeID','ASC');

		if(!empty($Where['UserTypeID'])){
			$this->db->where("UserTypeID",$Where['UserTypeID']);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			
				$Records = $Record;
			}
			//$Return['Records'] = $Records;

			return $Records;
		}
		return FALSE;		
	}

	
	function deleteRole($EntityID, $Input){

		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);

		$DeleteArray = array_filter(array(
			"UserTypeID" =>	$Input['UserTypeID'],
			"InstituteID"  => $InstituteID
		));

		if(!empty($DeleteArray)){
			$data=$this->db->insert('tbl_delete_usertype', $DeleteArray);
		}
		

		$this->db->select('U.UserID');
		$this->db->from('tbl_users U');
		$this->db->join('tbl_entity E','E.EntityID=U.UserID');

		if(!empty($Input['UserTypeID'])){
		  $this->db->where("U.UserTypeID",$Input['UserTypeID']);
		}
		$this->db->where("E.InstituteID",$InstituteID);
		//$this->db->where("E.EntityTypeID",14);

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
			if($Query->num_rows()>0){

				foreach($Query->result_array() as $Record){

					if(!empty($Record['UserID'])){

						$this->db->where("EntityID",$Record['UserID']);
						$data=$this->db->update('tbl_entity', array("StatusID"  => 3));

					}

				}

			}
		
		
		return TRUE;
	}

}