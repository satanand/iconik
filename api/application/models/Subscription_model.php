<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscription_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}



	function getSubscribedUsers($EntityID='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select("*");
		$this->db->from('tbl_subscribe_users');

		if(!empty($Where['SubscribeID'])){
			$this->db->where("SubscribeID",$Where['SubscribeID']);
		}           

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){	
				//print_r($Record['EntryDate']);
				$Record['EntryDate'] = date("d-m-Y h:i:s", strtotime($Record['EntryDate']));
				if(!$multiRecords){
					return $Record;
				}

				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;
	}


	function getBrochures($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(p.CreatedDate) >= '$FilterStartDate' AND DATE(p.CreatedDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (p.FullName LIKE '%$Keyword%' OR 
			p.Mobile LIKE '$Keyword%' OR 
			p.Email LIKE '$Keyword%' OR 
			p.DType LIKE '$Keyword%'
			) ";
		}

		if(isset($Where['DType']) && !empty($Where['DType']))
		{
			$DType = $Where['DType'];
			$append .= " AND p.DType = '".$DType."' ";
		}	
		

		$sql = "SELECT p.EnquiryID, p.FullName, p.Mobile, p.Email, p.DType, DATE_FORMAT(p.CreatedDate, '%d-%M-%Y %H:%i') as CreatedDate
		FROM ba_enquiry p				
		WHERE 1 = 1 $append		
		ORDER BY p.EnquiryID DESC 
		LIMIT $PageNo, $PageSize ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}
}

