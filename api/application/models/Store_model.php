<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}	





	/*
	Description: 	Use to update order.
	*/
	function editOrder($OrderID, $Input=array(), $UserID, $StatusID){

		if(!empty($Input['UserGUIDs'])){
			/*delete old assigned - starts*/
			$this->db->where(array("OrderID"=>$OrderID, "StatusID"=>1));
			$this->db->delete('ecom_order_assigned');
			/*delete old assigned - ends*/

			/*Assign to Staff - starts*/
			foreach($Input['UserGUIDs'] as $UserGUID){
				$UserGUID = str_replace("string:","",$UserGUID);
				$UserData = $this->Users_model->getUsers('U.UserID', array('EntityGUID'=>$UserGUID));
				if($UserData){
					$InsertOrderAssign[] = array('OrderID'=>$OrderID, 'ToUserID'=>$UserData['UserID']);

					/* send notification on like - starts */
					$NotificationText = "New order ".$OrderID." has been assigned for delivery.";
					$this->Notification_model->addNotification('order_assigned', $NotificationText, $this->SessionUserID, $UserData['UserID'], $Input['OrderGUID']);
					/* send notification on like - ends */


				}
			}
			if(!empty($InsertOrderAssign)){
				$this->db->insert_batch('ecom_order_assigned', $InsertOrderAssign); 		
			}
			/*Assign to Staff - ends*/
		}

		if(!empty($StatusID)){
			$UpdateArray = array_filter(array(
				"StatusID" 			=> 	$StatusID,
				"StoreName" 			=>	$Input['StoreName']		
			));

			if(!empty($UpdateArray)){ /*Update product details*/
				$this->db->where('OrderID', $OrderID);
				$this->db->limit(1);
				$this->db->update('ecom_orders', $UpdateArray);
			}	
		}
		return TRUE;
	}


















/*
Description: 	Use to get Get list of Stores.
*/
function getStores($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
	/* Define section  */
	$Return = array('Data' => array('Records' => array()));
	/* Define variables - ends */

	$this->db->select($Field);
	$this->db->select('
		CASE E.StatusID
		when "2" then "Active"
		when "6" then "Inactive"
		END as Status', false);
	$this->db->select('ST.StoreID StoreIDForUse');

	/* distance calculation - starts */
	/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
	if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
		$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(ST.Latitude))*cos(radians(ST.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(ST.Latitude)))) AS Distance",false);
		$this->db->order_by('Distance','ASC');
	}else{
		$this->db->order_by('ST.StoreName','ASC');
		$this->db->select('1 AS Distance');
	}
	//$this->db->having('Distance < 100',null,false);		
	/* distance calculation - ends */

	$this->db->from('tbl_entity E');	
	$this->db->from('ecom_stores ST');		
	$this->db->where("E.EntityID","ST.StoreID", FALSE);
	$this->db->from('tbl_entity ESO');	
	$this->db->where("ESO.EntityID","ST.UserID", FALSE);


	if(!empty($Where['Filter'])){
		if($Where['Filter']=='Veg'){
			$this->db->where("ST.IsVeg","Yes");
		}else{
			$this->db->where("ST.IsVeg","No");
		}
	}



	if(!empty($Where['StoreID'])){
		$this->db->where("ST.StoreID",$Where['StoreID']);
	}

	if(!empty($Where['Keyword'])){ /*search in name*/
		$this->db->like('ST.StoreName',$Where['Keyword']);
	}

	if(!empty($Where['StatusID'])){
		$this->db->where("E.StatusID",$Where['StatusID']);
	}



	/* Total records count only if want to get multiple records */
	if($multiRecords){ 
		$TempOBJ = clone $this->db;
		$TempQ = $TempOBJ->get();
		$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
	}else{
		$this->db->limit(1);
	}

	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		foreach($Query->result_array() as $Record){

			/*get attached media logo - starts*/
			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'StoreCover',"EntityID" => $Record['StoreIDForUse']));
			$Record['Media'] = ($MediaData ? $MediaData : new stdClass());
			/*get attached media - ends*/

			/*get attached media logo - starts*/
			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Store',"EntityID" => $Record['StoreIDForUse']));
			$Record['Logo'] = ($MediaData ? $MediaData : new stdClass());
			/*get attached media - ends*/
			
			unset($Record['StoreIDForUse']);
			if(!$multiRecords){
				return $Record;
			}
			$Records[] = $Record;
		}
		$Return['Data']['Records'] = $Records;
		return $Return;
	}
	return FALSE;		
}


	/*
	Description: 	ADD new store.
	*/
	function addStore($UserID, $Input=array(), $StatusID){
		$this->db->trans_start();
		$EntityGUID = get_guid();
		/* Add to entity table and get ID. */
		$StoreID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>7, "UserID"=>$UserID, "StatusID"=>$StatusID));

		/* Add */
		$InsertData = array_filter(array(
			"StoreID" 				=> 	$StoreID,
			"UserID" 				=> 	$Input['StoreOwnerUserID'],
			"StoreName" 			=>	$Input['StoreName'],
			"Location" 				=> 	@$Input['Location'],
			"Latitude" 				=> 	@$Input['Latitude'],
			"Longitude" 			=> 	@$Input['Longitude'],
			"IsVeg" 				=> 	@$Input['IsVeg'],
			"EstimatedDeliTime" 	=> 	@$Input['EstimatedDeliTime'],
			"MinOrderPrice" 		=> 	@$Input['MinOrderPrice']		
		));
		$this->db->insert('ecom_stores', $InsertData);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('StoreID' => $StoreID, 'StoreGUID' => $EntityGUID);
	}


	/*
	Description: 	Use to update store.
	*/
	function editStore($StoreID, $Input=array(), $StatusID){
		$UpdateArray = array_filter(array(
			"UserID" 				=> 	$Input['StoreOwnerUserID'],
			"StoreName" 			=>	$Input['StoreName'],
			"Location" 				=> 	@$Input['Location'],
			"Latitude" 				=> 	@$Input['Latitude'],
			"Longitude" 			=> 	@$Input['Longitude'],
			"IsVeg" 				=> 	@$Input['IsVeg'],
			"EstimatedDeliTime" 	=> 	@$Input['EstimatedDeliTime'],
			"MinOrderPrice" 		=> 	@$Input['MinOrderPrice']		
		));


		if(!empty($UpdateArray)){ /*Update product details*/
			$this->db->where('StoreID', $StoreID);
			$this->db->limit(1);
			$this->db->update('ecom_stores', $UpdateArray);
		}

		/*Update Status*/
		$this->Entity_model->updateEntityInfo($StoreID, array('StatusID'=>@$StatusID));

		return TRUE;
	}






	/*
	Description: 	ADD new product.
	*/
	function addProduct($UserID, $Input=array(), $StatusID=2){
		$this->db->trans_start();
		$EntityGUID = get_guid();
		/* Add to entity table and get ID. */
		$ProductID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>8, "UserID"=>$UserID, "StatusID"=>$StatusID, "Attributes"=>@$Input['Attributes']));
		/* Add product to product table*/
		$this->db->insert('ecom_products', array("ProductID"=>	$ProductID));

		$this->editProduct($ProductID, $Input, $UserID);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('ProductID' => $ProductID, 'ProductGUID' => $EntityGUID);
	}


	/*
	Description: 	Use to update category.
	*/
	function editProduct($ProductID, $Input=array(), $UserID){
		$UpdateArray = array_filter(array(
			"StoreID" 				=>	@$Input['StoreID'],
			"ProductSKU" 			=>	@$Input['ProductSKU'],
			"ProductName" 			=>	@$Input['ProductName'],
			"ProductRegPrice" 		=>	@$Input['ProductRegPrice'],
			"ProductBuyPrice" 		=>	@$Input['ProductBuyPrice'],
			"PercentageOff" 		=>	@$Input['PercentageOff'],
			"CurrencyType" 			=>	@$Input['CurrencyType'],
			"ProductDesc" 			=>	@$Input['ProductDesc'],
			"ProductShortDesc" 		=>	@$Input['ProductShortDesc'],
			"Tags" 					=>	@$Input['Tags'],
			"ProductBackorders" 	=>	@$Input['ProductBackorders'],
			"SellVia" 				=>	@$Input['SellVia'],
			"Negotiable" 			=>	@$Input['Negotiable'],
			"IsVeg" 				=>	@$Input['IsVeg'],
			"ListingType" 			=>	@$Input['ListingType'],		
			"ProductPrepTime" 		=>	@$Input['ProductPrepTime']				
		));


		if(isset($Input['ProductSKU']) && $Input['ProductSKU']==''){ $UpdateArray['ProductSKU'] = null; }
		if(isset($Input['ProductDesc']) && $Input['ProductDesc']==''){ $UpdateArray['ProductDesc'] = null; }
		if(isset($Input['ProductShortDesc']) && $Input['ProductShortDesc']==''){ $UpdateArray['ProductShortDesc'] = null; }


		/*Assign Add-on products - starts*/
		$UpdateArray['AddOnsProductIDs'] = '';		
		if(!empty($Input['ProductGUIDs'])){
			foreach($Input['ProductGUIDs'] as $ProductGUID){
				$ProductGUIDArray[] = str_replace("string:","",$ProductGUID);
			}
			$this->db->select("GROUP_CONCAT(P.ProductID) AS ProductIDs",false);
			$this->db->from('ecom_products P');
			$this->db->from('tbl_entity E');
			$this->db->where("E.EntityID","P.ProductID", FALSE);	
			$this->db->where_in("E.EntityGUID", $ProductGUIDArray);	
			$Query = $this->db->get();	
			$UpdateArray = array_merge($UpdateArray, array("AddOnsProductIDs" => $Query->row()->ProductIDs));
		}
		/*Assign Add-on products - ends*/


		if(!empty($UpdateArray)){ /*Update product details*/
			//if(!empty($Input['ProductInStock'])){ /*update product stock quantity*/
				$this->db->set('ProductInStock', (empty($Input['ProductInStock']) ? 1 : 0), FALSE);
			//}
			$this->db->where('ProductID', $ProductID);
			$this->db->limit(1);
			$this->db->update('ecom_products', $UpdateArray);
			//echo $this->db->last_query();
		}

		/*Update Entity*/
		$this->Entity_model->updateEntityInfo($ProductID, array(
			'StatusID'		=>  @$Input['StatusID'],
			"Location" 		=>	@$Input['Location'],
			"Latitude" 		=>	@$Input['Latitude'],
			"Longitude" 	=>	@$Input['Longitude']
		));			


		/* check for media present - associate media with this Product */
		if(!empty($Input['MediaGUIDs'])){
			$MediaGUIDsArray = explode(",", $Input['MediaGUIDs']);
			foreach($MediaGUIDsArray as $MediaGUID){
				$EntityData=$this->Entity_model->getEntity('E.EntityID MediaID',array('EntityGUID'=>$MediaGUID, 'EntityTypeID'=>4));
				if ($EntityData){
					$this->Media_model->addMediaToEntity($EntityData['MediaID'], $UserID, $ProductID);
				}
			}
		}
		/* check for media present - associate media with this Post - ends */


		if(!empty($Input['CategoryGUIDs'])){
			/*Revoke categories - starts*/
			$this->db->where(array("EntityID"=>$ProductID));
			$this->db->delete('tbl_entity_categories');
			/*Revoke categories - ends*/

			/*Assign categories - starts*/
			$this->load->model('Category_model');
			foreach($Input['CategoryGUIDs'] as $CategoryGUID){
				$CategoryGUID = str_replace("string:","",$CategoryGUID);
				$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
				if($CategoryData){
					$InsertCategory[] = array('EntityID'=>$ProductID, 'CategoryID'=>$CategoryData['CategoryID']);
				}
			}
			if(!empty($InsertCategory)){
				$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
			}
			/*Assign categories - ends*/
		}
		return TRUE;
	}









/*
Description: 	Use for table booking in the restaurant.
*/
function booking($Input=array(), $UserID, $StoreID=''){
	$BookedData = $this->getBookings("B.BookingID",array("StoreID" => $StoreID, "UserID" => $UserID, "BookingOn" => $Input['BookingOn']));
	if($BookedData){
		return false;
	}
	/* Add order to orders table */
	$OrderData = array_filter(array(
		"StoreID"			=>	$StoreID,
		"UserID"			=>	$UserID,
		"BookingOn"			=>	$Input['BookingOn'],
		"NumberOfPeople"	=>	$Input['NumberOfPeople'],		
		"EntryDate"			=>	date("Y-m-d h:i;s"),
	));
	$this->db->insert('ecom_bookings', $OrderData);
	return $this->db->insert_id();
}



	/*
	Description: 	Use to update booking.
	*/
	function editBooking($BookingID, $Input=array()){
		$UpdateArray = array_filter(array(
			"StatusID" 			=>	@$Input['StatusID']
		));

		if(!empty($UpdateArray)){
			/* Update details */
			$this->db->where('BookingID', $BookingID);
			$this->db->limit(1);
			$this->db->update('ecom_bookings', $UpdateArray);
		}

		return TRUE;
	}






/*
Description: 	Use to get single order or list of booking.
*/
function getBookings($Field, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
	/* Define section  */
	$Return = array('Data' => array('Records' => array()));
	/* Define variables - ends */

	$this->db->select($Field);
	$this->db->select('
		CASE B.StatusID
		when "1" then "Pending"
		when "2" then "Booked"
		when "3" then "Cancelled"
		END as Status', false);

	$this->db->from('ecom_bookings B');
	$this->db->from('tbl_users U');
	$this->db->where("B.UserID","U.UserID", FALSE);
	

	if(!empty($Where['BookingID'])){
		$this->db->where("B.BookingID",$Where['BookingID']);			
	}

	if(!empty($Where['StoreID'])){
		$this->db->where("B.StoreID",$Where['StoreID']);			
	}

	if(!empty($Where['UserID'])){
		$this->db->where("B.UserID",$Where['UserID']);			
	}

	if(!empty($Where['BookingOn'])){
		$this->db->where("B.BookingOn",$Where['BookingOn']);			
	}


	$this->db->order_by('B.BookingID','DESC');
	/* Total records count only if want to get multiple records */
	if($multiRecords){ 
		$TempOBJ = clone $this->db;
		$TempQ = $TempOBJ->get();
		$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
	}else{
		$this->db->limit(1);
	}

	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		if($multiRecords){
			$Return['Data']['Records'] = $Query->result_array();
			return $Return;	
		}else{
			return $Query->row_array();
		}
	}
	return FALSE;		
}



/*
Description: 	Use to get Get list of products.
*/
function getProducts($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
	/* Define section  */
	$Return = array('Data' => array('Records' => array()));
	/* Define variables - ends */

	$this->db->select($Field);
	$this->db->select('
		CASE E.StatusID
		when "2" then "Active"
		when "6" then "Inactive"
		END as Status', false);

	$this->db->select('P.ProductID, P.AddOnsProductIDs');

	/* distance calculation - starts */
	/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
	if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
		$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
		$this->db->order_by('Distance','ASC');

		if(!empty($Where['Radius'])){
			$this->db->having("Distance <= ".$Where['Radius'],null,false);
		}		
	}else{
		$this->db->order_by('E.MenuOrder','ASC');
		$this->db->order_by('P.ProductName','ASC');
		$this->db->select('1 AS Distance');
	}
	
	/* distance calculation - ends */

	$this->db->from('tbl_entity E');	
	$this->db->from('ecom_products P');
	$this->db->from('tbl_users U');
	$this->db->from('tbl_entity EU');	
	$this->db->where("E.EntityID","P.ProductID", FALSE);
	$this->db->where("E.CreatedByUserID","U.UserID", FALSE);
	$this->db->where("EU.EntityID","U.UserID", FALSE);

	if(!empty($Where['CategoryID'])){
		$this->db->where('EXISTS(SELECT 1 FROM `tbl_entity_categories` WHERE CategoryID='.$Where['CategoryID'].' AND EntityID=P.ProductID)');
	}
	if(!empty($Where['ProductID'])){
		$this->db->where("P.ProductID",$Where['ProductID']);
	}

	if(!empty($Where['ProductGUID'])){
		$this->db->where("E.EntityGUID",$Where['ProductGUID']);
	}

	if(!empty($Where['Filter'])){
		if($Where['Filter']=='Veg'){
			$this->db->where("P.IsVeg","Yes");
		}
		elseif($Where['Filter']=='NonVeg'){
			$this->db->where("P.IsVeg","No");
		}
		else{
			$this->db->where("P.ListingType",$Where['Filter']);			
		}
	}
	if(!empty($Where['Keyword'])){ /*search in name*/
		$this->db->group_start();
		$this->db->like('P.ProductName',$Where['Keyword']);
		$this->db->or_like("P.Tags", $Where['Keyword']);
		$this->db->group_end();	
	}

	if(!empty($Where['StatusID'])){
		$this->db->where("E.StatusID",$Where['StatusID']);
	}


	if(!empty($Where['StoreID'])){
		$this->db->where("P.StoreID",$Where['StoreID']);
	}


	/* Total records count only if want to get multiple records */
	if($multiRecords){ 
		$TempOBJ = clone $this->db;
		$TempQ = $TempOBJ->get();
		$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
	}else{
		$this->db->limit(1);
	}

	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		foreach($Query->result_array() as $Record){

			/*get event attributes*/
			$Record['Attributes'] = $this->Entity_model->getEntityAttributes($Record['ProductID']);

			/*get attached media - starts*/
			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Product',"EntityID" => $Record['ProductID']), TRUE);
			$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());
			/*get attached media - ends*/

			/*get Add-ons products - starts*/
			$Record['AddOnProducts'] = new stdClass();
			if(!empty($Record['AddOnsProductIDs'])){
				foreach(explode(',',$Record['AddOnsProductIDs']) as $Val){
					$Record['AddOnProducts'][] = $this->getAddOnProducts('
						E.EntityGUID AS ProductGUID,
						P.ProductName,		
						P.ProductBuyPrice',
						array("ProductID"=>$Val)
					);
				}
			}
			/*get Add-ons products - ends*/

			unset($Record['AddOnsProductIDs']);

			if(!$multiRecords){
				/*get related products - starts*/
				$Record['RelatedProducts'] = new stdClass();				
				$RelatedProductsData = $this->getProducts('
					E.EntityGUID AS ProductGUID,
					P.ProductName,
					P.ProductRegPrice,
					P.ProductBuyPrice,
					P.CurrencyType,
					(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=2 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryNames
					',array(), TRUE,1,8);
				if($RelatedProductsData){
					$Record['RelatedProducts'] = $RelatedProductsData['Data']['Records'];					
				}
				/*get related products - ends*/

				/*get related products - starts*/
				$Record['SellerListedProducts'] = new stdClass();				
				$SellerListedProductsData = $this->getProducts('
					E.EntityGUID AS ProductGUID,
					P.ProductName,
					P.ProductRegPrice,
					P.ProductBuyPrice,
					P.CurrencyType,
					(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=2 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryNames
					',array(), TRUE,1,8);
				if($SellerListedProductsData){
					$Record['SellerListedProducts'] = $SellerListedProductsData['Data']['Records'];					
				}
				/*get related products - ends*/


				return $Record;
			}
			$Records[] = $Record;
		}
		$Return['Data']['Records'] = $Records;
		return $Return;
	}
	return FALSE;		
}


/*
Description: 	Use to get AddOn Products
*/
function getAddOnProducts($Field='E.EntityGUID', $Where=array()){
	$this->db->select($Field);
	$this->db->select('P.ProductID');
	$this->db->from('tbl_entity E');		
	$this->db->from('ecom_products P');
	$this->db->where("E.EntityID","P.ProductID", FALSE);

	if(!empty($Where['ProductID'])){
		$this->db->where("P.ProductID",$Where['ProductID']);
	} 
	$this->db->order_by('E.MenuOrder','ASC');
	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		return $Query->row_array();	
	}
	return new stdClass();		
}


/*
Description: 	Use to get single coupon or list of coupons.
*/
function getCoupons($Field, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
	/* Define section  */
	$Return = array('Data' => array('Records' => array()));
	/* Define variables - ends */
	$this->db->select('C.CouponID CouponIDForUse');
	$this->db->select($Field);
	$this->db->select('
		CASE E.StatusID
		when "2" then "Active"
		when "6" then "Inactive"
		END as Status', false);
	$this->db->from('tbl_entity E');
	$this->db->from('ecom_coupon C');
	$this->db->where("C.CouponID","E.EntityID", FALSE);

	if(!empty($Where['CouponID'])){
		$this->db->where("C.CouponID",$Where['CouponID']);
	}


	if(!empty($Where['EntityGUID'])){
		$this->db->where("E.EntityGUID",$Where['EntityGUID']);
	}

	if(!empty($Where['StoreID'])){
		$this->db->where("C.StoreID",$Where['StoreID']);
	}
	
	if(!empty($Where['CouponCode'])){
		$this->db->where("C.CouponCode",$Where['CouponCode']);			
	}
	if(!empty($Where['StatusID'])){
		$this->db->where("E.StatusID",$Where['StatusID']);			
	}

	$this->db->order_by('C.CouponID','DESC');
	/* Total records count only if want to get multiple records */
	if($multiRecords){ 
		$TempOBJ = clone $this->db;
		$TempQ = $TempOBJ->get();
		$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
	}else{
		$this->db->limit(1);
	}

	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		foreach($Query->result_array() as $Record){

			/*get attached media*/
			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Category',"EntityID" => $Record['CouponIDForUse']), TRUE);
			$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

			unset($Record['CouponIDForUse']);
			if(!$multiRecords){
				return $Record;
			}
			$Records[] = $Record;
		}
		$Return['Data']['Records'] = $Records;
		return $Return;
	}
	return FALSE;		
}




	/*
	Description: 	ADD new coupon.
	*/
	function addCoupon($UserID, $Input=array(), $StatusID){
		$this->db->trans_start();
		$EntityGUID = get_guid();
		/* Add to entity table and get ID. */
		$CouponID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>9, "UserID"=>$UserID, "StatusID"=>$StatusID));
		/* Add product to product table*/
		$this->db->insert('ecom_coupon', array("CouponID"=>$CouponID));

		$this->updateCoupon($CouponID, $Input, $UserID);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('CouponID' => $CouponID, 'CouponGUID' => $EntityGUID);
	}



	/*
	Description: 	Use to update category.
	*/
	function updateCoupon($CouponID, $Input=array()){
		$UpdateArray = array_filter(array(
			"StoreID" 				=>	@$Input['StoreID'],
			"CouponTitle" 			=>	@$Input['CouponTitle'],
			"CouponDescription" 	=>	@$Input['CouponDescription'],
			"ProductRegPrice" 		=>	@$Input['ProductRegPrice'],
			"CouponCode" 			=>	@$Input['CouponCode'],
			"CouponType" 			=>	@$Input['CouponType'],
			"CouponValue" 			=>	@$Input['CouponValue'],
			"CouponValidTillDate" 	=>	@$Input['CouponValidTillDate'],
			"Broadcast" 			=>	@$Input['Broadcast'],				
		));

		if(isset($Input['CouponDescription']) && $Input['CouponDescription']==''){ $UpdateArray['CouponDescription'] = null; }

		if(!empty($UpdateArray)){ /*Update product details*/
			$this->db->where('CouponID', $CouponID);
			$this->db->limit(1);
			$this->db->update('ecom_coupon', $UpdateArray);
		}
		/*Update Status*/
		if(!empty($Input['StatusID'])){
			$this->Entity_model->updateEntityInfo($CouponID, array('StatusID'=>@$Input['StatusID']));			
		}
		return TRUE;
	}






/*
Description: 	Use to add new order
*/
function addOrder($Input=array(), $UserID, $StoreID='', $CouponID='', $StatusID=1){

	$OrderGUID = get_guid();
	$this->db->trans_start();

	/* get coupon data for apply */
	if(!empty($CouponID)){
		$CouponData = $this->getCoupons('C.CouponType, C.CouponValue',array("CouponID"=>$CouponID, "StatusID"=>2));
	}

	/*booking duration calculation*/
	if(!empty($Input['FromDateTime']) && !empty($Input['ToDateTime'])){
		$BookedDuration = diffInHours($Input['FromDateTime'], $Input['ToDateTime']);
	}


	/* Add order to orders table */
	$OrderData = array_filter(array(
		"UserID"		=>	$UserID,
		"OrderGUID"		=>	$OrderGUID,
		"StoreID"		=>	$StoreID,

		"FromDateTime"	=>	@$Input['FromDateTime'],
		"ToDateTime"	=>	@$Input['ToDateTime'],
		"Note"			=>	@$Input['Note'],
		"BookedDuration"=>	@$BookedDuration,

		"EntryDate"		=>	date("Y-m-d h:i;s"),
		"FirstName"		=>	$Input['Recipient']['FirstName'],
		"LastName"		=>	@$Input['Recipient']['LastName'],
		"Address"		=>	@$Input['DeliveryPlace']['Address'],
		"Address1"		=>	@$Input['DeliveryPlace']['Address1'],
		"City"			=>	@$Input['DeliveryPlace']['City'],
		"PhoneNumber"	=>	@$Input['DeliveryPlace']['PhoneNumber'],
		"DeliveryType"	=>	$Input['DeliveryType'],
		"PaymentMode"	=>	$Input['PaymentMode'],
		"PaymentGateway"=>	@$Input['PaymentGateway'],
		"CouponID"		=>	@$CouponID,
		"RewardPoints"	=>	@$Input['RewardPoints'],
		"StatusID"		=>	$StatusID
	));
	$this->db->insert('ecom_orders', $OrderData);
	$OrderID = $this->db->insert_id();

	/* Add products order info to order_details table */
	$OrderPrice = 0;
	if(!empty($Input['Products']) && is_array($Input['Products'])){
		foreach ($Input['Products'] as $key => $value) {
			$ProductData = $this->getProducts('P.ProductID, P.ProductRegPrice, P.ProductBuyPrice',array("ProductGUID"=>$value['ProductGUID']));
			$OrderDetailData[] = array(
				'OrderID'=>$OrderID,
				'ProductID'=>$ProductData['ProductID'],
				'ProductQuantity'=>(!empty($value['Quantity']) ? $value['Quantity']:1),
				"ProductRegPrice"=>$ProductData['ProductRegPrice'],			
				"ProductBuyPrice"=>$ProductData['ProductBuyPrice'] 
			);
			$OrderPrice += $ProductData['ProductBuyPrice']*$value['Quantity'];

			//if($Input['PaymentMode']=='COD'){
			/*minus stock quantity*/
			$this->editProduct($ProductData['ProductID'], array("ProductInStock"=>"ProductInStock-".$value['Quantity']), $UserID);
			//}
		}
		$this->db->insert_batch('ecom_order_details', $OrderDetailData); 


		/* apply discount (discount calculation) - start */
		$DiscountedPrice = 0;
		if(!empty($CouponData)){
			$DiscountedPrice = ($CouponData['CouponType']=='Flat' ? $CouponData['CouponValue'] : ($OrderPrice / 100) * $CouponData['CouponValue']);
			$OrderPrice = ($OrderPrice-$DiscountedPrice);
		}
		/* apply discount (discount calculation) - ends */

		/* apply reward points - start */
		if(!empty($Input['RewardPoints'])){
			$DiscountedPrice += $Input['RewardPoints'];
			$OrderPrice = ($OrderPrice-$Input['RewardPoints']);
			/*add reward points*/
			$this->Users_model->updateRewardPoints(
				array("TransactionID" => $OrderID, "TransactionDetails" => ''),
				$UserID, $Input['RewardPoints'], 'Subtract', 'Redemption');

		}
		/* apply reward points - ends */

	} /*Add product ends*/

	/* update OrderPrice to orders table */
	$this->db->where('OrderID', $OrderID);
	$this->db->limit(1);
	$this->db->update('ecom_orders', array("OrderPrice"=>$OrderPrice, "DiscountedPrice"=>$DiscountedPrice));

	$this->db->trans_complete();
	if ($this->db->trans_status() === FALSE)
	{
		return FALSE;
	}
	return array("OrderGUID"=>$OrderGUID, "OrderID"=>$OrderID);
}




/*
Description: 	Use to update the order.
*/
function orderConfirmation($UserID, $OrderGUID){
	$UserData=$this->Users_model->getUsers('U.FirstName, U.Email',array('UserID'=>$UserID));
	if(!empty($UserData['Email'])){
		/* Send order placed email to user */

		/* get order details - starts */
		$OrderData=$this->Store_model->getOrders('O.OrderID, CONCAT_WS(" ",O.FirstName,O.LastName) Name, O.Address, O.Address1, O.City, O.Postal, O.PhoneNumber',array('UserID'=>$UserID, 'OrderGUID'=> $OrderGUID));
		/*set variables (used to send with email)*/
		$DeliveryAddress =
		$OrderData['Name'].'<br>'.
		$OrderData['Address'].'<br>'.
		$OrderData['Address1'].'<br>'.
		$OrderData['City'].' '.(!empty($OrderData['Postal']) ? '- '.$OrderData['Postal'] : '').'<br>'.
		'Contact No. '.$OrderData['PhoneNumber'];
		/* get order details - ends */

		$content = $this->load->view('emailer/placed_order',array("Name" =>  $UserData['FirstName'], "OrderID" => $OrderData['OrderID'], "DeliveryAddress" => $DeliveryAddress),TRUE);

		sendMail(array(
			'emailTo' 		=> $UserData['Email'],			
			'emailSubject'	=> "Order Confirmation - Your Order with ".SITE_NAME." [".$OrderData['OrderID']."] has been successfully placed!",
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
		));
	}
}


/*
Description: 	Use to update the order.
*/
public function updateOrder($OrderID, $Input=array()){

	/*update order status*/
	$UpdateArray = array_filter(array(
		"TransactionID" 		=>	@$Input['TransactionID'],
		"StatusID" 				=>	@$Input['StatusID'],
		"ModifiedDate" 			=>	 date("Y-m-d H:i:s")
	));

	$this->db->where('OrderID', $OrderID);
	return ($this->db->update("ecom_orders",$UpdateArray) ? true : false);
}











/*
Description: 	Use to get single order or list of order.
*/
function getOrders($Field, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
	/* Define section  */
	$Return = array('Data' => array('Records' => array()));
	/* Define variables - ends */

	$this->db->select($Field);
	$this->db->select('
		CASE O.StatusID
		when "1" then "Pending"
		when "2" then "Confirmed"
		when "3" then "Cancelled"
		when "5" then "Delivered"
		END as Status', false);
	$this->db->from('ecom_orders O');
	$this->db->from('tbl_users U');
	$this->db->where("O.UserID","U.UserID", FALSE);
	/* get order assigned to user - starts */
	if(!empty($Where['Filter']) && $Where['Filter']=='Assigned'){
		$this->db->select('OA.OnRoute');
		$this->db->from('ecom_order_assigned OA');
		$this->db->where("O.OrderID","OA.OrderID", FALSE);

		$this->db->where("OA.ToUserID", $Where['ToUserID']);
		$this->db->where("OA.OrderID","O.OrderID", FALSE);
		if(!empty($Where['Status'])){
			if($Where['Status']=='Pending'){
				$this->db->where("OA.StatusID", '1');	
			}
			elseif($Where['Status']=='Accepted'){
				$this->db->where("OA.StatusID", '2');	
			}
			elseif($Where['Status']=='Delivered'){
				$this->db->where("OA.StatusID", '5');	
			}
		}		
	}
	/* get order assigned to user - ends */


	if(!empty($Where['UserID'])){
		$this->db->where("U.UserID",$Where['UserID']);			
	}
	if(!empty($Where['OrderID'])){
		$this->db->where("O.OrderID",$Where['OrderID']);			
	}
	if(!empty($Where['OrderGUID'])){
		$this->db->where("O.OrderGUID",$Where['OrderGUID']);			
	}

	if(!empty($Where['Filter'])){
		if($Where['Filter']=='Processing'){ /*Pending*/
			$this->db->where_in("O.StatusID",array(2));
		}
		elseif($Where['Filter']=='Past'){ /*Delivered and other status*/
			$this->db->where_in("O.StatusID",array(1,5));
		}
	}

	if(!empty($Where['StoreID'])){
		$this->db->where("O.StoreID",$Where['StoreID']);
	}

	/* Total records count only if want to get multiple records */
	if($multiRecords){ 
		$TempOBJ = clone $this->db;
		$TempQ = $TempOBJ->get();
		$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
	}else{
		$this->db->limit(1);
	}

	$this->db->order_by('O.OrderID','DESC');
	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		if($multiRecords){
			//$Return['Data']['Records'] = $Query->result_array();

			foreach($Query->result_array() as $Record){
				$Record['Products'] = $this->getProductsOrdered($Record['OrderID']);

				/*get ordered products - starts*/
/*				$this->db->select("E.EntityGUID AS ProductGUID, P.ProductID, P.ProductName");
				$this->db->from('ecom_order_details OD');
				$this->db->where("OD.ProductID","P.ProductID", FALSE);
				$this->db->from('ecom_products P');
				$this->db->from('tbl_entity E');
				$this->db->where("E.EntityID","P.ProductID", FALSE);	
				$this->db->where("OD.OrderID",$Record['OrderID']);
				$QueryM = $this->db->get();	

				if($QueryM->num_rows()>0){
					foreach($QueryM->result_array() as $RecordM){

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Product',"EntityID" => $RecordM['ProductID']), TRUE);
						$RecordM['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

						$RecordsN[] = $RecordM;
					}
					$Record['Products'] = $RecordsN;
					unset($RecordsN);	
				}*/
				/*get ordered products - ends*/
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}else{
			return $Query->row_array();
		}
	}
	return FALSE;		
}





/*
Description: 	Use to get single order or list of confirmed order.
*/
function trackOrders($Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

	$this->db->select('
		CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
		IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,	
		U.PhoneNumber PhoneNumberofUser,
		U.Latitude,
		U.Longitude,

		O.OrderID,
		O.OrderPrice,
		O.DiscountedPrice,
		O.FirstName,
		O.LastName,
		O.Address,
		O.Address1,
		O.City,
		O.PhoneNumber,
		O.DeliveryType,
		O.PaymentMode,
		O.PaymentGateway,
		O.TransactionID,
		O.RewardPoints,
		O.EntryDate,

		C. CouponCode

		');
	$this->db->select('
		CASE O.StatusID
		when "1" then "Pending"
		when "2" then "Confirmed"
		when "3" then "Cancelled"
		when "5" then "Delivered"
		END as Status', false);
	$this->db->from('ecom_orders O');
	$this->db->join('ecom_order_assigned OA', 'O.OrderID=OA.OrderID AND OA.StatusID=2', 'left');
	$this->db->join('tbl_users U', 'U.UserID=OA.ToUserID', 'left');
	$this->db->join('ecom_coupon C', 'C.CouponID=O.CouponID', 'left');

	if(!empty($Where['OrderID'])){
		$this->db->where("O.OrderID",$Where['OrderID']);			
	}

	/* Total records count only if want to get multiple records */
	if($multiRecords){ 
		$TempOBJ = clone $this->db;
		$TempQ = $TempOBJ->get();
		$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
	}else{
		$this->db->limit(1);
	}

	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		if($multiRecords){
			$Return['Data']['Records'] = $Query->result_array();
			return $Return;	
		}else{
			$Record = $Query->row_array();

			$Return['OrderID'] 		= $Record['OrderID'];
			$Return['OrderPrice'] 	= $Record['OrderPrice'];		
			$Return['EntryDate'] 	= $Record['EntryDate'];
			$Return['Recipient'] = array("FirstName" => $Record['FirstName'], "LastName" => $Record['LastName']);
			$Return['DeliveryPlace'] = array(
				'Address' 				=> $Record['Address'],
				'Address1'				=> $Record['Address1'],
				'City'					=> $Record['City'],
				'PhoneNumber'			=> $Record['PhoneNumber']	
			);
			$Return['DeliveryType'] 	= $Record['DeliveryType'];
			$Return['PaymentMode'] 		= $Record['PaymentMode'];
			$Return['PaymentGateway']	= $Record['PaymentGateway'];
			$Return['TransactionID']	= $Record['TransactionID'];

			$Return['AssignedTo'] = array(
				'FullName' 				=> @$Record['FullName'],
				'ProfilePic'			=> @$Record['ProfilePic'],
				'PhoneNumber'			=> @$Record['PhoneNumberofUser'],
				'Latitude'				=> @$Record['Latitude'],
				'Longitude'				=> @$Record['Longitude']
			);

			$Return['Discount'] = array(
				'DiscountedPrice' 		=> @$Record['DiscountedPrice'],
				'CouponCode'			=> @$Record['CouponCode'],
				'RewardPoints'			=> @$Record['RewardPoints']
			);

			$Return['Products'] = $this->getProductsOrdered($Record['OrderID']);

			/*get ordered products - starts*/
			/*			$this->db->select("E.EntityGUID, P.ProductID, P.ProductName, OD.ProductQuantity, OD.ProductRegPrice, OD.ProductBuyPrice");
			$this->db->from('ecom_order_details OD');
			$this->db->where("OD.ProductID","P.ProductID", FALSE);
			$this->db->from('ecom_products P');
			$this->db->from('tbl_entity E');
			$this->db->where("E.EntityID","P.ProductID", FALSE);	
			$this->db->where("OD.OrderID",$Record['OrderID']);
			$Query = $this->db->get();	
			if($Query->num_rows()>0){
				foreach($Query->result_array() as $Record){
					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Product',"EntityID" => $Record['ProductID']), TRUE);
					$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Records[] = $Record;
				}
				$Return['Products'] = $Records;	
			}*/
			/*get ordered products - ends*/
			unset($Record);
			return $Return;
		}
	}
	return FALSE;		
}







/*
Description: 	Use to get ordered products list.
*/

function getProductsOrdered($OrderID){
	/*get ordered products - starts*/
	$this->db->select("E.EntityGUID AS ProductGUID, P.ProductID, P.ProductName, OD.ProductQuantity");
	$this->db->from('ecom_order_details OD');
	$this->db->where("OD.ProductID","P.ProductID", FALSE);
	$this->db->from('ecom_products P');
	$this->db->from('tbl_entity E');
	$this->db->where("E.EntityID","P.ProductID", FALSE);	
	$this->db->where("OD.OrderID",$OrderID);
	$QueryM = $this->db->get();	
				//echo $this->db->last_query();
	if($QueryM->num_rows()>0){
		foreach($QueryM->result_array() as $RecordM){
			/*get attached media - starts*/
			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Product',"EntityID" => $RecordM['ProductID']), TRUE);
			$RecordM['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());
			/*get attached media - ends*/
			unset($RecordM['ProductID']);
			$RecordsN[] = $RecordM;
		}
		return $RecordsN;	
	}
	/*get ordered products - ends*/



}






















/*
Description: 	Use to update order assigned status.
*/
public function updateOrderAssigned($ToUserID, $OrderID, $Input=array()){

	$UpdateArray = array_filter(array(
		"OnRoute" 				=>	@$Input['OnRoute'],
		"StatusID" 				=>	@$Input['StatusID'],
		"ModifiedDate" 			=>	date("Y-m-d H:i:s")
	));

	$this->db->where(array('OrderID'=>$OrderID, 'ToUserID'=>$ToUserID));
	return ($this->db->update("ecom_order_assigned",$UpdateArray) ? true : false);
}







/*
Description: 	Use to get store data of user.
*/
function getUserStores($UserID){

	$this->db->select("E.EntityGUID StoreGUID");
	$this->db->from('ecom_stores S');
	$this->db->from('tbl_entity E');	
	$this->db->where("E.EntityID","S.StoreID", FALSE);

	$this->db->where("S.UserID",$UserID);			
	$this->db->limit(1);

	$Query = $this->db->get();	
	//echo $this->db->last_query();
	if($Query->num_rows()>0){
		return $Query->row_array();
	}
	return FALSE;		
}









}


