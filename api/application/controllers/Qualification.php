<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qualification extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Qualification_model');
		$this->load->model('Common_model');
	}



	

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getQualification_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QualificationID', 'QualificationID', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//print_r($this->Post); die();

		$RolesData = $this->Qualification_model->getQualification($this->EntityID,$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($RolesData)){
			$this->Return['Data'] = $RolesData['Data'];
		}	
	}

	/*

	Description: 	Use to add new Qualification

	URL: 			/api_admin/Qualification/add	

	*/

	public function addQualification_post()

	{

		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Qualification', 'Qualification', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

        //$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$QualificationStatus = $this->Qualification_model->addQualification($this->EntityID,$this->input->Post());

		//if($RolesData){

			if($QualificationStatus  == 1){
				$this->Return['ResponseCode'] 	=	200;
				$this->Return['Message']      	=	"New qualification Added successfully.";				
			}else  {
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This qualification name already exist.";
			}

		//}

	}




	/*

	Name: 			editRoles_post
	

	*/
	public function editQualification_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Qualification', 'Qualification', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->set_rules('QualificationID', 'QualificationID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Qualification_model->editQualification($this->EntityID, $this->input->post('QualificationID'), $this->input->Post());
	
		$this->Return['Data']= $this->Qualification_model->getQualification($this->EntityID, array("QualificationID"=>$this->input->post('QualificationID')));

	    $this->Return['Message']      	=	"Updated successfully."; 

	}


	public function deleteQualification_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('QualificationID', 'QualificationID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
	
		$this->Return['Data']= $this->Qualification_model->deleteQualification($this->EntityID, array("QualificationID"=>$this->input->post('QualificationID'),"StatusID"=>$this->input->post('StatusID')));

	    $this->Return['Message']      	=	"Deleted successfully."; 

	}
	
}