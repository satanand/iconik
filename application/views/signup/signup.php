<style type="text/css">
span.textbox {
      background-color: #FFF;
      color: #888;
      line-height:20px;
      height: 33px;
      padding: 8px;
      border: 1px  #c7c7c7 solid;
      font-size: 9pt;
      position: absolute;
}
    
span.textbox input {
      border: 0px;
      background-color: #FFF;
  }
</style>

<div class="container" ng-controller="PageController"> 
 <div id="logo" class="text-center"><img src="<?php echo API_URL;?>asset/img/emailer/output-onlinepngtools.png"></div>  
  <!-- Form -->
  <div class="col-12 col-sm-11 col-md-8 col-lg-6 col-xl-5 login-block" style="position: relative;">
    <h1 class="h3">Sign Up</h1>

     <p style="text-align: right"><em class="text-danger" style="font-size: 12px;">* </em>&nbsp;<span class="error"> Compulsory Fields</span></p>
    <br>

   
  <div class="row">
     <div class="col-sm-12 col-xs-12">      
        <form id="add_form" name="add_form" autocomplete="off" method="post" >
              <input name="UserTypeID" type="hidden" class="form-control" value="10" >
              <input name="Status" type="hidden" class="form-control" value="Pending" >
              <input name="DeviceType" type="hidden" class="form-control" value="Native" >
              <input type="hidden" name="Source" value="Direct"> 

           <div class="form-group">
                <label class="control-label">Institute Name <em class="text-danger" style="font-size: 12px;">*</em></label>
                <input name="FirstName" type="text" class="form-control" value="<?php echo $data['Name']; ?>" maxlength="50">
                  <input name="UserEmail" type="hidden" class="form-control" value="<?php echo $data['Email']; ?>" maxlength="50">

            </div>
              
          <div class="form-group">
            <label class="control-label">Email <em class="text-danger" style="font-size: 12px;">*</em></label>
            <input name="Email" type="text" id="email" class="form-control" value="<?php echo $data['Email']; ?>" maxlength="50">
          </div>

            <div class="form-group">
              <label class="control-label">Confirm Email <em class="text-danger" style="font-size: 12px;">*</em></label>
            <input name="ConfirmEmail" type="text" id="confemail" class="form-control" value="" oncopy="return false" onpaste="return false" maxlength="50">
            (Copy Paste not allowed)
            </div>
      
      
          <div class="form-group">
              <label class="control-label">Password <em class="text-danger" style="font-size: 12px;">*</em></label>
              <input name="Password" type="password"  class="form-control" value="" minlength="6" id="inputPassword">
              <div class="help-block">Minimum length 6</div>             
          </div>

            <div class="form-group">
              <label class="control-label">Confirm Password <em class="text-danger" style="font-size: 12px;">*</em></label>
              <input name="ConfirmPassword" type="password"  class="form-control"  id="inputPasswordConfirm">
            </div>

            <div class="form-group">
              <label class="control-label">Contact No. <em class="text-danger" style="font-size: 12px;">*</em></label>
              <!-- <input name="PhoneNumber" type="text" class="form-control integer" value=""  maxlength="10"> --><br>
              <span class="textbox"> 
              +91</span>
              <input type="text"  name="PhoneNumber" autofocus class="form-control integer" value=""  maxlength="10"/ style="margin-left: 38px;   width: 381px;">
          
            <span class="help-block">Contact Number i.e.(+91-988 *** 3456)</span>
          </div>
 
          <div class="form-group">
            <button type="button" class="btn btn-success btn-sm float-right" ng-disabled="addDataLoading" ng-click="addData()">Submit</button>
            <span class="float-left"><a href="signin" class="a">Sign in?</a></span>
          </div>
          <img src="asset/img/loader.svg" ng-if="addDataLoading" style="position: absolute;    top: 50%;    left: 50%;    transform: translate(-50%, -50%);">
         </form>
       </div>
   </div>
</div>

</div><!-- / container -->

<script type="text/javascript">
      function confirmEmail() {
          var email = document.getElementById("email").value;
          var confemail = document.getElementById("confemail").value;
          if(email != confemail) {
              alert('This email id is not registered with us!');
              return false;
      
          }else{
      return true;
          }
      
      }

    function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
    }
</script>