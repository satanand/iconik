<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignpaper extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Assignpaper_model');
	}


	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionBank/getQuestionBankStatistics
	Input (Sample JSON): 		
	*/
	public function getAssignPaperStatistics_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		if($this->input->post('SubjectGUID') != ""){
			$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}else if($this->input->post('CourseGUID') != ""){
			$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}

		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('SubjectGUID') != ""){			
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics($this->CategoryID);
		}else if($this->input->post('CourseGUID') != ""){
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics('',$this->CategoryID);
		}else{
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics();
		}
		
		if(!empty($QuestionBankData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $QuestionBankData;
		}	
	}


	/*
	Name: 			posts
	Description: 	Use to add new Question
	URL: 			/api/post	
	*/
	public function addQA_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('QuestionsLevel', 'Difficulty Level', 'required|in_list[Easy,Moderate,High]');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim|required|in_list[Multiple Choice,Logical Answer,Short Answer]');	
		$this->form_validation->set_rules('QuestionsMarks', 'Mark', 'trim|required');
		$this->form_validation->set_rules('QuestionContent', 'Enter Question', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|callback_validateEntityGUID[Media]');
		
		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 


		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = 1;
			$Answer = $this->Post['ShortAnswer'];
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = $this->Post['AnswerContent'];
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = "";
		}

		//echo $this->SessionUserID; echo '--'.$this->EntityID; die;

		$QAData = $this->Questionbank_model->addQA($this->SessionUserID, $this->EntityID, array(
			"SubjectID"	=>	$this->CategoryID,		
			"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
			"QuestionsLevel"	=>	$this->Post['QuestionsLevel'],
			"QuestionsType"	=>	$this->Post['QuestionsType'],
			"QuestionsMarks"	=>	$this->Post['QuestionsMarks'],
			"QuestionContent"	=>	$this->Post['QuestionContent'],
			"AnswerContent"		=>	$Answer,
			"CorrectAnswer"		=>	$CorrectAnswer,
			"Explanation"		=>	@$this->input->Post('Explanation')
		));

		if($QAData){

			if($this->Post['MediaGUID']){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $QAData['QtBankID']);
				}
			}
			

			$this->Return['Data']['QtBankGUID'] = $QAData['QtBankGUID'];

		}
	}


	/*
	Name: 			posts
	Description: 	Use to add new post
	URL: 			/api/post	
	*/
	public function editQA_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('QuestionsLevel', 'Difficulty Level', 'required|in_list[Easy,Moderate,High]');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim|required|in_list[Multiple Choice,Logical Answer,Short Answer]');	
		$this->form_validation->set_rules('QuestionsMarks', 'Mark', 'trim|required');
		$this->form_validation->set_rules('QuestionContent', 'Enter Question', 'trim|required');
		
		
		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 


		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = 1;
			$Answer = $this->Post['ShortAnswer'];
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = $this->Post['AnswerContent'];
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = "";
		}



		$QAData = $this->Questionbank_model->editQA($this->SessionUserID, $this->EntityID, array(
			"QtBankID"	=>	$this->QtBankID,
			"SubjectID"	=>	$this->CategoryID,		
			"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
			"QuestionsLevel"	=>	$this->Post['QuestionsLevel'],
			"QuestionsType"	=>	$this->Post['QuestionsType'],
			"QuestionsMarks"	=>	$this->Post['QuestionsMarks'],
			"QuestionContent"	=>	$this->Post['QuestionContent'],
			"AnswerContent"		=>	$Answer,
			"CorrectAnswer"		=>	$CorrectAnswer,
			"Explanation"		=>	@$this->input->Post('Explanation')
		));

		if($QAData){

			if($this->Post['MediaGUID']){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $this->QtBankID);
				}
			}

			$this->Return['Data']['QtBankGUID'] = $this->input->post('QtBankGUID');

		}
	}



	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/getPosts
	*/
	public function getQuestions_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		if($this->input->post('SubjectGUID') != ""){
			$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}else if($this->input->post('CourseGUID') != ""){
			$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}	
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'trim');
		$this->form_validation->set_rules('QuestionsLevel', 'Question Level', 'trim');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim');	
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		if($this->input->post('SubjectGUID') != ""){
			$SubjectID = $this->CategoryID;
			$CourseID = "";
		}else if($this->input->post('CourseGUID') != ""){
			$CourseID = $this->CategoryID;
			$SubjectID = "";
		}else{
			$CourseID = "";
			$SubjectID = "";
		}	


		$fields = array('QtBankID','QtBankGUID','CourseID','SubjectID','QuestionContent','QuestionsGroup','QuestionsLevel','QuestionsMarks','QuestionsType');

		$Questions=$this->Questionbank_model->getQuestions($fields,array(
				'SessionUserID'	=>	$this->SessionUserID,
				'EntityID'		=>	(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),
				'CourseID'		=>  $CourseID,
				'SubjectID'		=>	$SubjectID,
				'QuestionsGroup' =>	@$this->Post['QuestionsGroup'],
				'QuestionsLevel' =>	@$this->Post['QuestionsLevel'],
				'QuestionsType' =>	@$this->Post['QuestionsType'],
				'Filter'		=>	@$this->Post['Filter'],
				'PostType'     =>	@$this->Post['PostType'],
				'Keyword'		=>	@$this->Post['Keyword']
			), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}



	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/getPosts
	*/
	public function getQuestionAnswer_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Questions=$this->Questionbank_model->getQuestionAnswer($this->SessionUserID,(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtBankID,$this->input->post('courseID'),$this->input->post('subjectID'),$this->input->post('view'));

		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Post,QtBankID]');
		$this->form_validation->validation($this);  /* Run validation */	
		echo "fdsfds";	
		/* Validation - ends */
		if(!$this->Questionbank_model->deletePost($this->SessionUserID, $this->QtBankID)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}



	/*
	Name: 			posts
	Description: 	Use to add new post
	URL: 			/api/post	
	*/
	public function generateQuestionPaper_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('QuestionsLevel', 'Difficulty Level', 'required|in_list[Easy,Moderate,High]');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim|required|in_list[Multiple Choice,Logical Answer,Short Answer]');	
		$this->form_validation->set_rules('QuestionsMarks', 'Mark', 'trim|required');
		$this->form_validation->set_rules('QuestionContent', 'Enter Question', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|callback_validateEntityGUID[Media]');
		
		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 


		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = 1;
			$Answer = $this->Post['ShortAnswer'];
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = $this->Post['AnswerContent'];
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = "";
		}



		$QAData = $this->Questionbank_model->addQA($this->SessionUserID, $this->EntityID, array(
			"SubjectID"	=>	$this->CategoryID,		
			"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
			"QuestionsLevel"	=>	$this->Post['QuestionsLevel'],
			"QuestionsType"	=>	$this->Post['QuestionsType'],
			"QuestionsMarks"	=>	$this->Post['QuestionsMarks'],
			"QuestionContent"	=>	$this->Post['QuestionContent'],
			"AnswerContent"		=>	$Answer,
			"CorrectAnswer"		=>	$CorrectAnswer,
			"Explanation"		=>	@$this->input->Post('Explanation')
		));

		if($QAData){

			if($this->Post['MediaGUID']){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $QAData['QtBankID']);
				}
			}
			

			$this->Return['Data']['QtBankGUID'] = $QAData['QtBankGUID'];

		}
	}

}