<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utility_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}	



	/*
	Description: 	Use to get country list
	*/
	function getCountries(){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$Query = $this->db->query("SELECT CountryCode,CountryName,phonecode   FROM `set_location_country` ORDER BY CountryName ASC
		");
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			$Return['Data']['Records'] = $Query->result_array();
			return $Return;	
		}
		return FALSE;		
	}





	/*
	Description: 	Use to add ReferralCode
	*/
	function generateReferralCode($UserID=''){
		$ReferralCode =  random_string('alnum', 6);
		$this->db->insert('tbl_referral_codes', array_filter(array('UserID'=>$UserID, 'ReferralCode'=>$ReferralCode)));
		return $ReferralCode;
	}


/*
	Description: 	Use to get user from sentrifugo.
	*/
	function getCustomUsers() {
		$CRMDB = $this->load->database('crm',TRUE);
		$CRMDB->select('firstname,lastname,emailaddress,contactnumber,emppassword,emprole,employeeId');
		$CRMDB->from('main_users');
		$query = $CRMDB->get();
		return $query->result();
	}

	/*
	Description: 	Use to get user type from sentrifugo.
	*/
	function getCustomUserType() {
		$CRMDB = $this->load->database('crm',TRUE);
		$CRMDB->select('id,rolename');
		$CRMDB->from('main_roles');
		$query = $CRMDB->get();
		return $query->result();
	}
/*
	Description: 	Use to add user type from sentrifugo.
	*/
	public function insertUserType($table, $data) {
        $query = $this->db->insert($table, $data);
        if ($query) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
/*
	Description: 	Use to get user type from align hr.
	*/
    function checkUserType($id){
		$this->db->select('*');
		$this->db->from('tbl_users_type');
		$this->db->where('UserTypeID',$id);
		$query = $this->db->get();
		return $query->row();
	}
}


