<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}	

	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getContents($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/* Define section  */

		//print_r($Where); die;
		if(!empty($Where['SessionUserID'])){
			$InstituteID = $this->Common_model->getInstituteByEntity($Where['SessionUserID']);
		}
		
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select('P.PostID PostIDForUse, P.ParentPostID ParentPostIDForUse');
		$this->db->select($Field);
		$this->db->from('social_post P');
		$this->db->from('tbl_users U');
		$this->db->from('tbl_entity E');
		$this->db->from('tbl_entity EU');
		$this->db->where("E.CreatedByUserID",$InstituteID);
		$this->db->where("P.EntityID","U.UserID", FALSE);
		$this->db->where("P.PostID","E.EntityID", FALSE);
		$this->db->where("P.EntityID","EU.EntityID", FALSE);

		/* Filter - start */
		if(!empty($Where['Filter'])){
			if($Where['Filter']=='Saved'){
				$this->db->from('tbl_action A');
				$this->db->where("A.EntityID", $Where['SessionUserID']);
				$this->db->where("A.ToEntityID","P.PostID", FALSE);
				$this->db->where("A.Action", 'Saved');
			}
			elseif($Where['Filter']=='Liked'){
				$this->db->from('tbl_action A');
				$this->db->where("A.EntityID", $Where['SessionUserID']);
				$this->db->where("A.ToEntityID","P.PostID", FALSE);
				$this->db->where("A.Action", 'Liked');
			}
			elseif($Where['Filter']=='MyPosts'){
				$this->db->where("P.EntityID", $Where['SessionUserID']);
				$this->db->where("P.ToEntityID", $Where['SessionUserID']);
			}		
			elseif($Where['Filter']=='Popular'){
				$this->db->order_by('E.ViewCount','DESC');
			}
		}
		/* Filter - ends */	

		if(!empty($Where['Keyword'])){ /*search in post content*/
			$this->db->group_start();
			$this->db->where('MATCH (P.PostContent) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
			$this->db->or_where('MATCH (P.PostCaption) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);

			$this->db->or_like("U.FirstName", $Where['Keyword']);
			$this->db->or_like("U.LastName", $Where['Keyword']);
			$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);

			$this->db->or_like("EXISTS(SELECT SC.CategoryID from `set_categories` as SC, `tbl_entity_categories` as ES WHERE SC.CategoryName like '%".$Where['Keyword']."%' AND ES.EntityID=P.EntityID AND ES.CategoryID=SC.CategoryID)", FALSE);
			
			$this->db->group_end();


		}

		if(!empty($Where['ParentPostID'])){
			$this->db->where("P.ParentPostID",$Where['ParentPostID']);
		}

		if(!empty($Where['CategoryID'])){
			//$this->db->where("P.CategoryID",$Where['CategoryID']);
		}
		
		if(!empty($Where['PostType'])){
			$this->db->where("P.PostType",$Where['PostType']);
		}	


		if(!empty($Where['PostID'])){
			$this->db->where("P.PostID",$Where['PostID']);
		}

		if(!empty($Where['EntityGUID'])){
			$this->db->where("E.EntityGUID",$Where['EntityGUID']);
		}



		if(!empty($Where['SessionUserID'])){ /*skip for admin*/

			if(!empty($Where['EntityID'])){ /*viewing others wall*/
				$this->db->group_start();
				$this->db->where("P.ToEntityID",$Where['EntityID']);
				if(!empty($Where['Wall']) && $Where['Wall']=='Own'){/*Follow users posts*/
					$this->db->or_where("EXISTS(SELECT 1 FROM `social_subscribers` WHERE UserID=".$Where['SessionUserID']." AND ToEntityID=P.EntityID AND ACTION='Follow 'AND StatusID=2)", NULL, FALSE);
				}
				$this->db->group_end();
			}

			/* handling privacy - starts */
			$this->db->where("(CASE
				WHEN P.Privacy = 'Friends'
				THEN
				EXISTS(SELECT 1 FROM `social_subscribers` WHERE UserID=P.EntityID AND ToEntityID=".$Where['SessionUserID']." AND ACTION='Friend' AND StatusID=2)
				WHEN P.Privacy = 'Private'
				THEN
				P.`EntityID`=".$Where['SessionUserID']."
				WHEN P.Privacy = 'Public'
				THEN
				true
				ELSE
				false
				END
			)", NULL, FALSE);
			/* handling privacy - ends */
		}


		$this->db->order_by('P.PostID','DESC');
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				/*get attached media*/
                $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'File',"EntityID" => $Record['PostIDForUse']), TRUE);
                if(!empty($MediaData)){
                	$Record['Media_file'] = ($MediaData ? $MediaData['Data'] : array());
                	$Record['Media_file_count'] = count($Record['Media_file']['Records']);
                }else{
                	$Record['Media_file'] = array();
                	$Record['Media_file_count'] = 0;
                }
                

                $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"EntityID" => $Record['PostIDForUse']), TRUE);
                if(!empty($MediaData)){
                	$Record['Media_audio'] = ($MediaData ? $MediaData['Data'] : array());
                	$Record['Media_audio_count'] = count($Record['Media_audio']['Records']);
                }else{
                	$Record['Media_audio'] = array();
                	$Record['Media_audio_count'] = 0;
                }


                if(!empty($Record['PostVideoLink']) && $Record['PostVideoLink']!=NULL){
                	$Record['PostVideoLink']  = json_decode($Record['PostVideoLink']);
                	if(count($Record['PostVideoLink'])){
	                	$Record['PostVideoTitle']  = json_decode($Record['PostVideoTitle']);
	                	$Record['Media_video'] = array_combine($Record['PostVideoTitle'], $Record['PostVideoLink']);
	                	if(count($Record['Media_video']) > 0){
	                		$Record['Media_video_count'] = count($Record['Media_video']);
	                	}else{
	                		$Record['Media_video_count'] = 0;
	                	}	
	                }
                }else{
                	$Record['Media_video'] = array();
                	$Record['Media_video_count'] = 0;
                }
				
				/*get parent post if shared*/
				$Record['ParentPost'] =''; /*define return variable*/
				if(!empty($Record['ParentPostIDForUse'])){
                 $Record['ParentPost'] = $this->Post_model->getPosts('E.EntityGUID PostGUID,P.PostContent, P.Caption,P.PostVideoTitle,P.PostVideoLink,E.EntryDate,CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
						IF(U.ProfilePic = "","",CONCAT("' . BASE_URL . '",U.ProfilePic)) AS ProfilePic,', array('PostID' => $Record['ParentPostIDForUse'], 'SessionUserID' => $Where['SessionUserID'],));	
				}


				/*get parentcategory details*/
				$Record['ParentCategory'] = array();
				if(!empty($Record['ParentCategoryID'])){
					$this->load->model('Category_model');
						$Record['ParentCategory'] = $this->Category_model->getCategories('',array("CategoryID"=>$Record['ParentCategoryID']));
				}




				unset($Record['PostIDForUse']);
				unset($Record['ParentPostIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to add new post
	*/
	function addContent($EntityID, $ToEntityID, $Input=array()){
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$EntityGUID = get_guid();
		/* Add post to entity table and get EntityID. */
		$PostID = $this->Entity_model->addEntity($EntityGUID, array(
			"EntityTypeID"	=>	6,
			"UserID"		=>	$EntityID,
			"Privacy"		=>	@$Input["Privacy"],
			"StatusID"		=>	2,
			"Rating"		=>	$Input["Rating"]
		));

		$this->db->where('EntityID',$PostID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		/* Add post */
		$InsertData = array_filter(array(
			"PostID" 		=>	$PostID,
			"PostGUID" 		=>	$EntityGUID,			
			"ParentPostID" 	=>	@$Input["ParentPostID"],
			"PostType" 		=>	$Input["PostType"],		
			"EntityID" 		=>	$EntityID,
			"CategoryID" 	=>	@$Input["CategoryID"],
			"ToEntityID" 	=> 	$ToEntityID,
			"PostContent" 	=>	$Input["PostContent"],
			"PostCaption" 	=>	$Input["PostCaption"],
			"PostVideoTitle" => @$Input["PostVideoTitle"],
			"PostVideoLink"	=>	@$Input["PostVideoLink"],
			"CategoryGUIDs"	=>	@$Input["CategoryGUID"],
			"MediaGUIDs"	=>	@$Input["MediaGUIDs"]
		));
		$this->db->insert('social_post', $InsertData);
		//echo $this->db->last_query();

		/* Update entity shared count */
		if(!empty($Input["ParentPostID"])){
			$this->db->set('SharedCount', 'SharedCount+1', FALSE);
			$this->db->where('EntityID', $Input["ParentPostID"]);
			$this->db->limit(1);
			$this->db->update('tbl_entity');
		}

		/*add to category*/
		// $Input['CategoryGUIDs'] = json_decode($Input['CategoryGUIDs']);
		// if(!empty($Input['CategoryGUIDs'])){
		// 	/*Assign categories - starts*/
		// 	$this->load->model('Category_model');
		// 	foreach($Input['CategoryGUIDs'] as $CategoryGUID){
		// 		$CategoryGUID = str_replace("string:","",$CategoryGUID);
		// 		$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
		// 		if($CategoryData){
		// 			$InsertCategory[] = array('EntityID'=>$PostID, 'CategoryID'=>$CategoryData['CategoryID'],'ParentCategoryID'=>$Input["ParentCategoryID"]);
		// 		}
		// 	}
		// 	if(!empty($InsertCategory)){
		// 		$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
		// 	}
		// 	/*Assign categories - ends*/
		// }

		/*add to category*/
		//$Input['CategoryGUIDs'] = json_decode($Input['CategoryGUIDs']);
		if(!empty($Input['CategoryGUID'])){
			$this->load->model('Category_model');
			$CategoryData = $this->Category_model->getCategories('CategoryID,ParentCategoryID', array('CategoryGUID'=>$Input["CategoryGUID"]));
			if($CategoryData){
				$InsertCategory[] = array('EntityID'=>$PostID, 'CategoryID'=>$CategoryData['CategoryID']);
				if(!empty($InsertCategory)){
					$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
				}
			}
		}


		if(!empty($Input['PostVideoLink'])){
			$PostVideoTitle = json_decode($this->Post['PostVideoTitle']);
			$PostVideoLink = json_decode($this->Post['PostVideoLink']);
			$VideoEntityGUID = get_guid();

			
			/* Add post to entity table and get EntityID. */
			$VideoPostID = $this->Entity_model->addEntity($VideoEntityGUID, array(
				"EntityTypeID"	=>	9,
				"UserID"		=>	$EntityID,
				"Privacy"		=>	@$Input["Privacy"],
				"StatusID"		=>	2,
				"Rating"		=>	$Input["Rating"]
			));


			$this->db->where('EntityID',$VideoPostID);
			$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));
			
			//print_r($InsertVideo); die;
			foreach ($PostVideoLink as $key => $value) {
				$InsertVideo[] = array('VideoPostID'=>$VideoPostID,'VideoEntityGUID'=>$VideoEntityGUID,'EntityID'=>$PostID, 'PostVideoLink'=>$value,'PostVideoTitle'=>$PostVideoTitle[$key]);
				//print_r($InsertVideo); //die;
			}
			if(!empty($InsertVideo)){
				$data = $this->db->insert_batch('tbl_social_post_video', $InsertVideo); 
				//print_r($data);		
			}
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return array('PostID' => $PostID, 'PostGUID' => $EntityGUID);
	}


	/*
	Description: 	Use to update store.
	*/
	function editContent($PostID, $Input=array()){
		$UpdateArray = array_filter(array(
			"PostType" 		=>	$Input["PostType"],	
			"CategoryID" 	=>	@$Input["CategoryID"],
			"PostContent" 	=>	@$Input["PostContent"],
			"PostCaption" 	=>	@$Input["PostCaption"],
			"PostVideoTitle" => @$Input["PostVideoTitle"],
			"PostVideoLink"	=>	@$Input["PostVideoLink"],
			"CategoryGUIDs"	=>	@$Input["CategoryGUIDs"],
			"MediaGUIDs"	=>	@$Input["MediaGUIDs"]	
		));


		if(!empty($UpdateArray)){ /*Update product details*/
			$this->db->where('PostID', $PostID);
			$this->db->limit(1);
			$this->db->update('social_post', $UpdateArray);
		}

		// /*add to category*/
		// $Input['CategoryGUIDs'] = json_decode($Input['CategoryGUIDs']);
		// if(!empty($Input['CategoryGUIDs'])){
		// 	/*Assign categories - starts*/
		// 	$this->load->model('Category_model');
		// 	foreach($Input['CategoryGUIDs'] as $CategoryGUID){
		// 		$CategoryGUID = str_replace("string:","",$CategoryGUID);
		// 		$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
		// 		if($CategoryData){
		// 			$updateCategory = array('EntityID'=>$PostID, 'CategoryID'=>$CategoryData['CategoryID']);
		// 		}
		// 	}
		// 	if(!empty($updateCategory)){
		// 		$this->db->where('EntityID',$PostID);
		// 		$this->db->update('tbl_entity_categories',$updateCategory);
		// 	}
		// 	/*Assign categories - ends*/
		// }


		if(!empty($Input['CategoryGUID'])){
			$this->load->model('Category_model');
			$CategoryData = $this->Category_model->getCategories('CategoryID,ParentCategoryID', array('CategoryGUID'=>$Input["CategoryGUID"]));
			if($CategoryData){
				$updateCategory[] = array('EntityID'=>$PostID, 'CategoryID'=>$CategoryData['CategoryID'],'ParentCategoryID'=>$CategoryData["ParentCategoryID"]);
				if(!empty($updateCategory)){
					$this->db->where('EntityID',$PostID);
					$this->db->update('tbl_entity_categories',$updateCategory);
				}
			}
		}

		/*Update Status*/
		$this->Entity_model->updateEntityInfo($PostID);

		return array('PostID' => $PostID);
	}



	function getContentCount($EntityID, $PostType, $multiRecords=FALSE,  $PageNo=1, $PageSize=10, $SubjectID="", $CourseID=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		if(!empty($CourseID)){
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and CategoryID='".$CourseID."'");
		}else{
			//echo "select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2"; die;
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2");
		}
		
		//$query = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryTypeID = 3");

		$course = $query->result_array();

		$result = array(); 
		$new_result = array(); 
		$check_array = array();
		$check = 1; $audio_count = 0; $video_count = 0; $file_count = 0;
		if(count($course) > 0){
			//print_r($course);
			$p = 0;
			foreach ($course as $key => $value) {
				
					if(!empty($SubjectID)){
						$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CategoryID']."' and CategoryID = '".$SubjectID."'");
					}else{
						$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CategoryID']."'");
					}

					$SubCategoryData = $query->result_array();

					//print_r($SubCategoryData);

					$parentCategoryName = $value['CategoryName'];
					$parentCategoryGUID = $value['CategoryGUID'];

					foreach ($SubCategoryData as $k => $v) {

						if(!in_array($parentCategoryName, $check_array)){
							array_push($check_array, $parentCategoryName);
							$result[$parentCategoryName][$k]['ParentCategoryName'] = $parentCategoryName;
						}else{
							$result[$parentCategoryName][$k]['ParentCategoryName'] = "";	
						}
						$result[$parentCategoryName][$k]['ParentName'] = $parentCategoryName;
						$result[$parentCategoryName][$k]['parentCategoryGUID'] = $parentCategoryGUID;
						$result[$parentCategoryName][$k]['SubCategoryName'] = $v['CategoryName'];
						$result[$parentCategoryName][$k]['CategoryGUID'] = $v['CategoryGUID'];
						$result[$parentCategoryName][$k]['video_count'] = 0;
						$result[$parentCategoryName][$k]['audio_count'] = 0;
						$result[$parentCategoryName][$k]['text_file_count'] = 0;

						//$query = $this->db->query("SELECT es.EntityID, sc.CategoryName, sc.CategoryID, sc.CategoryGUID, sp.PostVideoLink, sp.PostGUID FROM ((tbl_entity_categories as es JOIN set_categories as sc ON es.CategoryID = sc.CategoryID) JOIN social_post as sp ON sp.PostID = es.EntityID) where es.CategoryID = ".$v['CategoryID']);

						//echo "SELECT es.EntityID FROM tbl_entity_categories as es where es.CategoryID = ".$v['CategoryID'];

						$query = $this->db->query("SELECT distinct(es.EntityID) FROM tbl_entity_categories as es where es.CategoryID = ".$v['CategoryID']);


						$subject = $query->result_array();

						if($query->num_rows() > 0){

							$EntityID = array_column($subject, 'EntityID');

							if(!empty($EntityID)){

								$query = $this->db->query("select count(tbl_social_post_video.VideoPostID) as count_video from tbl_social_post_video where EntityID in (".implode(",",$EntityID).")");

								$data = $query->result_array();

								$result[$parentCategoryName][$k]['video_count'] = $data[0]['count_video'];	
								

								$query = $this->db->query("select count(tbl_media.MediaID) as count_media from tbl_media where SectionID = 'Audio'  and EntityID in (".implode(",",$EntityID).")");

								$data = $query->result_array();

								$result[$parentCategoryName][$k]['audio_count'] = $data[0]['count_media'];

								$query = $this->db->query("select count(social_post.PostGUID) as count_PostGUID from social_post where PostType = '".$PostType."' and PostID in (".implode(",",$EntityID).") and PostContent != ''");
								$data = $query->result_array();
								$count_PostGUID = $data[0]['count_PostGUID'];

								//echo "select count(tbl_media.MediaID) as count_media from tbl_media where SectionID = 'File'  and EntityID in (".implode(",",$EntityID).")"; die;

								$query = $this->db->query("select count(tbl_media.MediaID) as count_media from tbl_media where SectionID = 'File'  and EntityID in (".implode(",",$EntityID).")");
								$data = $query->result_array();

								$result[$parentCategoryName][$k]['text_file_count'] = $count_PostGUID+$data[0]['count_media'];
							}
						}
						$p++;
						if(!empty($result[$parentCategoryName][$k])){
							array_push($new_result, $result[$parentCategoryName][$k]);
						}
					}
			}
			return $new_result;
		}
		return false;
	}




	function getContentByCategory($CategoryID, $PostType, $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
	//echo "SELECT es.EntityID FROM tbl_entity_categories as es JOIN set_categories as sc ON es.CategoryID = sc.CategoryID where sc.CategoryID = ".$CategoryID;		
		$query = $this->db->query("SELECT distinct(es.EntityID) FROM tbl_entity_categories as es where es.CategoryID = ".$CategoryID);
		$data = $query->result_array();
		$result = array(); $TotalCount = 0;
		if(count($data)){
			$i = 0; $j = 0; 
			foreach ($data as $k => $v) {
				// echo $v['EntityID'];
				// echo "<br>";
				$query = $this->db->query("select PostGUID,PostCaption,PostContent from social_post where PostType = '".$PostType."' and PostID = '".$v['EntityID']."'");

				$PostData = $query->result_array();
				$result[$k]['content'] = array();

				

				if($query->num_rows() > 0){
					$TotalCount = intval($TotalCount)+intval($query->num_rows());
					foreach ($PostData as $pk => $pv) {
						$arr = array();
						$PostGUID = $pv['PostGUID'];
						if(!empty($pv['PostContent'])){
							$arr['PostGUID'] = $pv['PostGUID'];
							$arr['PostCaption'] = $pv['PostCaption'];
							$arr['PostContent'] = $pv['PostContent'];
							array_push($result[$k]['content'], $arr);
						}
					}
				}

				if(empty($result[$k]['content'])){
					$result[$k]['content'] = 0;
				}

				if(!empty($PostGUID)){
					$result[$k]['PostGUID'] = $PostGUID;
				}


				$query = $this->db->query("select * from tbl_social_post_video where EntityID = '".$v['EntityID']."'");

				$PostVideo = $query->result_array();
				$result[$k]['media_video'] = array();

				

				if($query->num_rows() > 0){
					$TotalCount = intval($TotalCount)+intval($query->num_rows());
					foreach ($PostVideo as $pk => $pv) {
						$arr = array();
						if($pv['PostVideoLink'] != ""){
							$arr['VideoEntityGUID'] = $pv['VideoEntityGUID'];
							$arr['PostVideoLink'] = $pv['PostVideoLink'];
							$arr['PostVideoTitle'] = $pv['PostVideoTitle'];
							array_push($result[$k]['media_video'], $arr);
						}
					}
				}



				if(empty($result[$k]['media_video'])){
					$result[$k]['media_video'] = 0;
				}

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'File',"EntityID" => $v['EntityID']), TRUE);
				if($MediaData['Data']){
					$result[$k]['media_file'] = $MediaData['Data']['Records'];
					$TotalCount = intval($TotalCount)+intval(count($MediaData['Data']['Records']));
				}else{
					$result[$k]['media_file'] = 0;
				}

				

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"EntityID" => $v['EntityID']), TRUE);
				if($MediaData['Data']){
					$result[$k]['media_audio'] = $MediaData['Data']['Records'];
					$TotalCount = intval($TotalCount)+intval(count($MediaData['Data']['Records']));
				}else{
					$result[$k]['media_audio'] = 0;
				}

				

				$result[$k]['EntityID'] = 

				$i++;
				//print_r($result);
			}

			$result['TotalCount'] = $TotalCount;
			return $result;
		}else{
			return false;
		}
	}



	function getContentBySubject($CategoryID){
		//echo $CategoryID = 11456;
		//echo 'select CategoryID,CategoryGUID,CategoryName from set_categories where ParentCategoryID = '.$CategoryID; die;
		$query = $this->db->query('select CategoryID,CategoryGUID,CategoryName from set_categories where CategoryID = '.$CategoryID);
		$data = $query->result_array();
		$CategoryID = array_column($data, 'CategoryID');
		//echo "SELECT distinct(es.EntityID) FROM tbl_entity_categories as es where es.CategoryID IN ('".implode(',', $CategoryID)."')";

		$data = $query->result_array();
		$result = array();
		$i = 0; $j = 0; 
		if(count($data)){			
			foreach ($data as $k => $v) {
				//echo "SELECT distinct(es.EntityID) FROM tbl_entity_categories as es where es.CategoryID = (select CategoryID from set_categories where ParentCategoryID = '".$v['CategoryID']."')";
				$query = $this->db->query("SELECT distinct(es.EntityID) FROM tbl_entity_categories as es where es.CategoryID = '".$v['CategoryID']."'");
				$entityData = $query->result_array();

				if(count($entityData)){
					$result[$i]['SubCategoryName'] = $v['CategoryName'];
					$result[$i]['SubCategoryGUID'] = $v['CategoryGUID'];
					$manual_text = array(); $video = array(); $audio = array(); $text_file = array(); 
					foreach ($entityData as $key => $value) {
						//echo "select PostGUID,PostCaption,PostContent from social_post where PostType = '".$PostType."' and PostID = '".$value['EntityID']."'"; die;
						$query = $this->db->query("select PostGUID,PostCaption,PostContent from social_post where PostType = 'Content' and PostID = '".$value['EntityID']."'");

						$PostData = $query->result_array();

						if($query->num_rows() > 0){
							foreach ($PostData as $pk => $pv) {
								$arr = array();
								$PostGUID = $pv['PostGUID'];
								if(!empty($pv['PostContent'])){
									$arr['PostGUID'] = $pv['PostGUID'];
									$arr['PostCaption'] = $pv['PostCaption'];
									$arr['PostContent'] = $pv['PostContent'];
									array_push($manual_text, $arr);
								}
							}
						}

						
						//echo "select * from tbl_social_post_video where EntityID = '".$value['EntityID']."'";
						$query = $this->db->query("select * from tbl_social_post_video where EntityID = '".$value['EntityID']."'");

						$PostVideo = $query->result_array();

						if($query->num_rows() > 0){
							foreach ($PostVideo as $pk => $pv) {
								$arr = array();
								if($pv['PostVideoLink'] != ""){
									$arr['VideoEntityGUID'] = $pv['VideoEntityGUID'];
									$arr['PostVideoLink'] = $pv['PostVideoLink'];
									$arr['PostVideoTitle'] = $pv['PostVideoTitle'];
									array_push($video, $arr);
								}
							}
						}

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'File',"EntityID" => $value['EntityID']), TRUE);
						if($MediaData['Data']){	
							foreach ($MediaData['Data']['Records'] as $key => $media) {
								array_push($text_file, $media);
							}					
							
						}

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"EntityID" => $value['EntityID']), TRUE);
						if($MediaData['Data']){
							foreach ($MediaData['Data']['Records'] as $key => $media) {
								array_push($audio, $media);
							}	
						}					
					}
					//print_r($text_file);
					$result[$i]['manual_text'] = $manual_text;
					$result[$i]['video'] = $video;
					$result[$i]['text_file'] = $text_file;
					$result[$i]['audio'] = $audio;
					$i++;
				}else{
					$result[$i]['SubCategoryName'] = $v['CategoryName'];
					$result[$i]['SubCategoryGUID'] = $v['CategoryGUID'];
					$result[$i]['manual_text'] = [];
					$result[$i]['video'] = [];
					$result[$i]['text_file'] = [];
					$result[$i]['audio'] = [];
					$i++;
				}
				//print_r($result);
			}
			return $result;
		}else{
			return false;
		}
	}
		



	/*
	Description: 	Use to get Cetegories
	*/
	function getCategories($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select('C.CategoryGUID, C.CategoryID CategoryIDForUse, CT.CategoryTypeName, C.CategoryName');

		$this->db->select($Field);

		$this->db->select('

			CASE E.StatusID

			when "2" then "Active"

			when "6" then "Inactive"

			END as Status', false);



		$this->db->from('set_categories C');

		$this->db->from('set_categories_type CT');

		$this->db->from('tbl_entity E');



		$this->db->where('C.CategoryTypeID','CT.CategoryTypeID',FALSE);

		$this->db->where('C.CategoryID','E.EntityID',FALSE);



		if(!empty($Where['StoreID'])){

			$this->db->where("C.StoreID",$Where['StoreID']);

		}



		if(!empty($Where['CategoryID'])){

			$this->db->where("C.CategoryID",$Where['CategoryID']);

		}      



		if(!empty($Where['CategoryGUID'])){

			$this->db->where("E.EntityGUID",$Where['CategoryGUID']);

		}           



		if(!empty($Where['CategoryTypeID'])){

			$this->db->where("C.CategoryTypeID",$Where['CategoryTypeID']);

		}           



		if(!empty($Where['ParentCategoryID'])){

			$this->db->where("C.ParentCategoryID",$Where['ParentCategoryID']);

		}



		if(!empty($Where['ShowOnlyParent'])){

			$this->db->where("C.ParentCategoryID is NULL", NULL, FALSE);

		}



		/* Total records count only if want to get multiple records */

		if($multiRecords){ 

			$TempOBJ = clone $this->db;

			$TempQ = $TempOBJ->get();

			$Return['Data']['TotalRecords'] = $TempQ->num_rows();

			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/

		}else{

			$this->db->limit(1);

		}



		$this->db->order_by('E.MenuOrder','ASC');

		$Query = $this->db->get();	

		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){



				/*get attached media*/

				// $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Category',"EntityID" => $Record['CategoryIDForUse']), TRUE);

				// $Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());


				$query = $this->db->query("select EntityID from tbl_entity_categories where CategoryID = ".$Record['CategoryIDForUse']);

				$data = $query->result_array();

				$Record['Media'] = array();
				$audio = array(); $text_file = array(); $i = 0; $j = 0;
				foreach ($data as $key => $value) {
					//echo $value['EntityID'];
					if(isset($value['EntityID']) && !empty($value['EntityID'])){
						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"EntityID" => $value['EntityID']), TRUE);
						//print_r($MediaData);
						if(isset($MediaData) && !empty($MediaData['Data']['Records'])){
							array_push($audio, $MediaData['Data']['Records']);
						}

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'File',"EntityID" => $value['EntityID']), TRUE);
						//print_r($MediaData);
						if(isset($MediaData) && !empty($MediaData['Data']['Records'])){
							array_push($text_file, $MediaData['Data']['Records']);
						}
					}					
				}
				
				$Record['Media']['audio'] = ($audio ? $audio : new stdClass());
				$Record['Media']['text_file'] = ($text_file ? $text_file : new stdClass());

				/*get attached media*/

				//$Record['Media'] = json_encode($Record['Media']);



				$Record['SubCategories'] = array();

				$SubCategories = $this->getCategories('

					C.CategoryID,

					E.EntityGUID AS CategoryGUID,

					C.CategoryName',

					array("ParentCategoryID"=>$Record['CategoryIDForUse']),true,1,25

				);	



				if($SubCategories){

					$Record['SubCategories'] = $SubCategories['Data'];

				}



				unset($Record['CategoryIDForUse']);

				if(!$multiRecords){

					return $Record;

				}

				$Records[] = $Record;

			}

			$Return['Data']['Records'] = $Records;

			return $Return;

		}

		return FALSE;		

	}


	


	/*
	Description: 	Use to get Cetegories
	*/
	function getCategoriesTest($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		/*Additional fields to select*/
		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'CategoryID'				=>	'C.CategoryID',			
				'SubCategoryNames'			=>	'(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE ParentCategoryID=C.CategoryID) AS SubCategoryNames',
				'MenuOrder'					=>	'E.MenuOrder',
				'Rating'					=>	'E.Rating',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}


		$this->db->select('C.CategoryGUID, C.CategoryID CategoryIDForUse, C.CategoryName');
		$this->db->select($Field);
		$this->db->select('
			CASE E.StatusID
			when "2" then "Active"
			when "6" then "Inactive"
			END as Status', false);

		$this->db->from('set_categories C');
		$this->db->from('set_categories_type CT');
		$this->db->from('tbl_entity E');

		$this->db->where('C.CategoryTypeID','CT.CategoryTypeID',FALSE);
		$this->db->where('C.CategoryID','E.EntityID',FALSE);

		if(!empty($Where['StoreID'])){
			$this->db->where("C.StoreID",$Where['StoreID']);
		}

		if(!empty($Where['CategoryID'])){
			$this->db->where("C.CategoryID",$Where['CategoryID']);
		}      

		if(!empty($Where['CategoryGUID'])){
			$this->db->where("E.EntityGUID",$Where['CategoryGUID']);
		}           

		if(!empty($Where['CategoryTypeID'])){
			$this->db->where("C.CategoryTypeID",$Where['CategoryTypeID']);
		}           

		if(!empty($Where['ParentCategoryID'])){
			$this->db->where("C.ParentCategoryID",$Where['ParentCategoryID']);
		}

		if(!empty($Where['ShowOnlyParent'])){
			$this->db->where("C.ParentCategoryID is NULL", NULL, FALSE);
		}

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$this->db->order_by('E.MenuOrder','ASC');
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				/*get attached media*/
				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Category',"EntityID" => $Record['CategoryIDForUse']), TRUE);
				$Record['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

				$Record['SubCategories'] = new stdClass();
				$SubCategories = $this->getCategories('',array("ParentCategoryID"=>$Record['CategoryIDForUse']),true,1,25);	
				if($SubCategories){
					$Record['SubCategories'] = $SubCategories['Data'];
					// foreach ($Record['SubCategories']['Records'] as $ks => $vs) {
					// 	foreach ($vs['SubCategories']['Records'] as $k => $v) {
					// 		if(!empty($v['CategoryGUID'])){
					// 			$query = $this->db->query("select EntityID from tbl_entity_categories where CategoryID = (select CategoryID from set_categories where CategoryGUID = '".$v['CategoryGUID']."')");

					// 			$data = $query->result_array(); 

					// 			if(count($data) > 0){
					// 				$file = array(); $audio = array();
					// 				foreach ($data as $dk => $dv) {
					// 					//echo $dv['EntityID']; die;
					// 					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'File',"EntityID" => $dv['EntityID']), TRUE);
					// 					if(count($MediaData['Data']) > 0){
					// 						array_push($file, $MediaData['Data']['Records']);
					// 					}	

					// 					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Audio',"EntityID" => $dv['EntityID']), TRUE);
					// 					if(count($MediaData['Data']) > 0){
					// 						array_push($audio, $MediaData['Data']['Records']);
					// 					}										
					// 				}
					// 				$vs['SubCategories']['Records'][$k]['media_text_file'] = ($file ? $file : new stdClass());
					// 				$vs['SubCategories']['Records'][$k]['media_text_file'] = ($audio ? $audio : new stdClass());
					// 			}
					// 		}
					// 	}
					// }
					//print_r($Record['SubCategories']);
				}

				unset($Record['CategoryIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deleteVideo($VideoEntityGUID){
		$this->db->where('VideoEntityGUID', $VideoEntityGUID);
  		$this->db->delete('tbl_social_post_video');
  		return TRUE;
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deleteContent($UserID, $PostID){
		$PostData=$this->getContents('P.EntityID',array('PostID'=>$PostID, 'SessionUserID'=>$UserID));
		if(!empty($PostData) && $UserID==$PostData['EntityID']){
			$this->Entity_model->deleteEntity($PostID);
			return TRUE;
		}
		return FALSE;
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deleteTextContent($UserID, $PostID){
		$PostData=$this->getContents('P.EntityID',array('PostID'=>$PostID, 'SessionUserID'=>$UserID));
		if(!empty($PostData) && $UserID==$PostData['EntityID']){
			$this->db->where('PostID', $PostID);
    		$this->db->update('social_post', array("PostContent"=>"","PostCaption"=>""));
			return TRUE;
		}
		return FALSE;
	}



	/*
	Description: 	Use to delete post by owner
	*/
	function updateContentVideo($VideoEntityGUID, $update_arr){
		//$PostData=$this->getContents('P.EntityID',array('PostID'=>$PostID, 'SessionUserID'=>$UserID));
		if($VideoEntityGUID){
			$this->db->where('VideoEntityGUID', $VideoEntityGUID);
    		$this->db->update('tbl_social_post_video', $update_arr);
			return TRUE;
		}
		return FALSE;
	}


	 /*
	Description: 	Use to  DeteleMedia.
	*/
	function deleteMedia($MediaGUID){

		if(!empty($MediaGUID)){

			$this->db->where('MediaGUID', $MediaGUID);

			$this->db->delete('tbl_media');		
			
		}
		return TRUE;
	}
	
}