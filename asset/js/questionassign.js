app.controller('PageController', function ($scope, $http,$timeout){

    $scope.categoryDataList = [];

    $scope.data.SecondLevelPermission = [];

    /*----------------*/


    $scope.getFilterData = function (){
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
        $scope.getBatch();

        var SubjectGUID = $scope.getUrlParameter('CategoryGUID');

        $scope.SubjectGUID = SubjectGUID;

        var BatchGUID = $scope.getUrlParameter('BatchGUID');

        $scope.BatchGUID = BatchGUID;

        $scope.getSubjectByBatch(BatchGUID)
    }


    /*get subject*/

    $scope.getSubCategoryList = function (ParentCategoryGUID){

        //console.log(ParentCategoryGUID);

        $scope.subCategoryDataList = [];

        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID;

        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.subCategoryDataList = response.Data.Records; 

            } ////////console.log($scope.categoryDataList);

        });

        //console.log($scope.subCategoryDataList);

    }


    $scope.applyFilter1 = function (data)
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getFilterData();
        $scope.getBatchPapersList(1);
    }


    $scope.applyFilterQuiz = function (data)
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getQuizAssignStatistics(1);
    }

   

    $scope.getBatch = function(){

        var data = 'SessionKey='+SessionKey;   

        $scope.batch = [];     

        $http.post(API_URL+'batch/getBatch', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.batch = response.Data.Records; 

            } //////console.log($scope.categoryDataList);

        });

    }





    $scope.getSubjectByBatch = function(BatchGUID){

        $scope.BatchGUID = BatchGUID;

        var data = 'SessionKey='+SessionKey+'&BatchGUID='+BatchGUID;   

        $scope.subject = [];     

        $http.post(API_URL+'questionpaper/getSubjectByBatch', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.subject = response.Data.Records; 

            } console.log($scope.subject);

        });

    }


    $scope.getBatchByPaper = function(QtPaperGUID){

        $scope.QtPaperGUID = QtPaperGUID;

        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;   

        $scope.batch = [];     

        $http.post(API_URL+'questionpaper/getBatchByPaper', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.batch = response.Data.Records; 

            } console.log($scope.batch);

        });

    }



    /*list append*/

    $scope.getQuestionAssignStatistics = function (BatchGUID="",SubjectGUID="")

    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        $scope.getBatch();

        $scope.BatchGUID = BatchGUID;

        $scope.SubjectGUID = SubjectGUID;

        //if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.listLoading = true;

        $scope.data.dataList = [];

        var data = 'SessionKey='+SessionKey+'&BatchGUID='+BatchGUID+'&CategoryGUID='+SubjectGUID;

        $http.post(API_URL+'questionassign/getQuestionAssignStatistics', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataList.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;               

               }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort();  $scope.$apply(); }, 1000);

        });

    }



    // $scope.loadFormEdit = function (Position, QtPaperGUID, type="")

    // {
    //     $scope.getQuestionPaperlist();

    //     $scope.data.Position = Position;

    //     $scope.editAssignPaper = type;

    //     $scope.templateURLEdit = PATH_TEMPLATE+'questionpaper/edit_form.htm?'+Math.random();

    //     $scope.data.pageLoading = true;

    //     $scope.QtPaperGUID = QtPaperGUID;

    //     var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;

    //     $http.post(API_URL+'questionpaper/getAssignedTestByID', data, contentType).then(function(response) {

    //         var response = response.data;

    //         if(response.ResponseCode==200 && response.Data.length){ /* success case */

    //             $scope.data.pageLoading = false;

    //             $scope.formData = response.Data[0];  

    //             $scope.getSubCategoryList($scope.formData.CourseGUID);

    //             $scope.getAvailableQuestionOnEdit(QtPaperGUID,$scope.formData.SubjectGUID,$scope.formData.QuestionsGroupID);

    //             if($scope.formData.NegativeMarks) {

    //                $scope.formData.NegativeMarks = $scope.formData.NegativeMarks.split('.');

    //             }else{

    //                 $scope.formData.NegativeMarks[0] = 0;

    //                 $scope.formData.NegativeMarks[1] = 0;

    //             } 

    //             $('#edit_model').modal({show:true});

    //         }

    //     });

    // }






    /*list append*/

    $scope.getQuizAssignStatistics = function (check="")

    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        if(check!=1){
            $scope.getBatch();
        }
        

        //if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.listLoading = true;

        $scope.data.dataList = [];

        
        console.log($('#filterForm').serialize());

        var data = 'SessionKey='+SessionKey+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'questionassign/getQuizAssignStatistics', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataList.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;               

               }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort();  $scope.$apply(); }, 1000);

        });

    }



    $scope.getQuestionPaperlist = function (QuestionsGroup="")

    {

        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        var data = 'SessionKey='+SessionKey+'&QuestionsGroup='+QuestionsGroup;
        
        console.log(data);
        $scope.data.qplist = [];

        $http.post(API_URL+'questionpaper/getQuestionsPaperList', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) {

                   $scope.data.qplist.push(response.Data[i]);

                }

            }

        });

    }



    $scope.applyFilter = function ()
    {
       //$scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
       $scope.getTestResults(1,$scope.QtPaperGUID);
    }



    $scope.viewResultPaper = function ()
    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        $scope.data.listLoading = true;

        var ResultGUID = $scope.getUrlParameter('ResultGUID');

        $scope.ResultGUID = ResultGUID;

        var data = 'SessionKey='+SessionKey+'&CheckPaperData=Yes&ResultGUID='+$scope.ResultGUID;

        $http.post(API_URL+'questionpaper/getResultPaper', data, contentType).then(function(response) {

                var response = response.data;

                $scope.data.ResultsArr = [];

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;  

                    $scope.results =  response.Data[0];

                    //console.log($scope.results);

                   // results.ResultData.answer = results.ResultData.answer.replace(/((\[\s*)|(\s*\]))/g,"")

                    //results.ResultData.answer = results.ResultData.answer.substring(1, results.ResultData.answer .length-1);

                    //console.log(results.ResultData);

                    //$scope.TotalMarks = results.TotalMarks;

                   // $scope.TotalMarks = results.TotalMarks;
                   /*$scope.data.GivenAnsArr = [];
                    for (var i in $scope.results.ResultData) {

                       // console.log($scope.results.GivenAnswerID); 
                       $scope.data.ResultsArr.push($scope.results.ResultData[i]);
                       $scope.data.GivenAnsArr.push($scope.results.GivenAnswerID);
                      

                    }*/  
                     
                    $scope.data.listLoading = false; 

                }else{

                    $scope.data.noRecords = true;

                    $scope.data.listLoading = false; 

                }

                        

                $scope.data.listLoading = false;  

        });

    }




    $scope.getTestResults = function(check="",QtPaperGUID=""){
        
        
        if(check != 1){

            if ($scope.data.listLoading || $scope.data.noRecords) return;

        }

        $scope.data.listLoading = true;   

        if(QtPaperGUID.length > 0){
            $scope.QtPaperGUID = QtPaperGUID;

            $scope.data.dataList = [];

            $scope.data.pageNo = 1;
        }else{
            var QtPaperGUID = $scope.getUrlParameter('QtPaperGUID');

            $scope.QtPaperGUID = QtPaperGUID;
        }
        

        var data = 'SessionKey='+SessionKey+'&CheckPaperData=Yes&QtPaperGUID='+$scope.QtPaperGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        

        //console.log(check);

        $http.post(API_URL+'questionpaper/getTestResults', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataList.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;

                    $scope.data.listLoading = false; 

                }else{

                    $scope.data.noRecords = true;

                    $scope.data.listLoading = false; 

                }

                        

                $scope.data.listLoading = false;  

        });
    }





    $scope.getSubCategoryList = function (ParentCategoryGUID){

        console.log(ParentCategoryGUID);

        $scope.subCategoryDataList = [];

        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID;

        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.subCategoryDataList = response.Data.Records; 

            } //////console.log($scope.categoryDataList);

        });

        console.log($scope.subCategoryDataList);

    }





    $scope.getBatchPapersList = function (check="",BatchGUID="",SubjectGUID="")

    {

        //$scope.questionPaperList = 1; 

        if(check != 1){

            if ($scope.data.listLoading || $scope.data.noRecords) return;

        }else{
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }

        $scope.data.listLoading = true;             


        if(BatchGUID.length > 0 || SubjectGUID.length > 0){
            var BatchGUID = BatchGUID;

            $scope.BatchGUID = BatchGUID;

            var SubjectGUID = SubjectGUID;

            $scope.SubjectGUID = SubjectGUID;

            $scope.data.dataList = [];

            $scope.data.pageNo = 1;
        }
        

        var data = 'SessionKey='+SessionKey+'&SubjectGUID='+$scope.SubjectGUID+'&BatchGUID='+$scope.BatchGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        

        //console.log(check);

        $http.post(API_URL+'questionassign/getBatchPapersList', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataList.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;

                    $scope.data.listLoading = false; 

                }else{

                    $scope.data.noRecords = true;

                    $scope.data.listLoading = false; 

                }

                        

                $scope.data.listLoading = false;  

        });

    }



    





    /*load edit form*/

    $scope.loadViewresult = function (Position, QtAssignGUID)

    {

        console.log(PATH_TEMPLATE+'questionassign/view_form.htm');

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+'questionassign/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&QtAssignGUID='+QtAssignGUID;

        $http.post(API_URL+'questionassign/getResult', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length){ /* success case */                

                $scope.formData = response.Data[0];

                $('#view_model').modal({show:true}); 

                $scope.data.pageLoading = false;

            }else{                

                $scope.data.pageLoading = false;

                ErrorPopup("Not available for view!");

            }

        });

    }



     /*load add form*/

    $scope.loadAssignPaper1 = function (type="")

    {

        //alert(type);

        //if(QtPaperGUID.length){

            if(type == "Quiz"){
                $scope.getQuestionPaperlist(2);
                $scope.loadAssignPaperCheck = "Quiz";
            }else{
                $scope.getQuestionPaperlist();
                $scope.loadAssignPaperCheck = "Other";
            }
            

            $scope.getBatch();

           // $scope.getSubCategoryList(CourseGUID);

            // $scope.QtPaperGUID = QtPaperGUID;

            // $scope.SubjectGUID = SubjectGUID;

            $scope.templateURLAssign = PATH_TEMPLATE+'questionassign/add_form.htm?'+Math.random();

            $scope.data.pageLoading = true;   

            $('#assign_model').modal({show:true}); 

            $scope.data.pageLoading = false; 



            $timeout(function(){        

                var d = new Date();

                var month = d.getMonth();

                var day = d.getDate();

                var year = d.getFullYear();



                $("#Date").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d);
                
               // $("#StartTime").datetimepicker({format: 'H:i',minView: 2});

                $('#StartTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                 $('#ActivatedFromTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                 /*$('#ActivatedToTime').timepicker({
                    format: 'H:mm',minView: 1
                });*/

            }, 1000);

        //}        

    }


    $scope.loadFormEdit = function (Position, QtAssignGUID, QtPaperGUID, type, BatchID="")

    {

        if(type == "Quiz"){
            $scope.getQuestionPaperlist(2);
            $scope.loadAssignPaperCheck = "Quiz";
            $scope.BatchID = BatchID;
        }else{
            $scope.getQuestionPaperlist();
            $scope.loadAssignPaperCheck = "Other";
        }

        $scope.QtPaperGUID = QtPaperGUID;

        $scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE+'questionassign/edit_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&QtAssignGUID='+QtAssignGUID;
        

        $http.post(API_URL+'questionassign/getAssignedTestByID', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data[0];  

                $('#edit_model').modal({show:true});

                $timeout(function(){        

                    var d = new Date();

                    var month = d.getMonth();

                    var day = d.getDate();

                    var year = d.getFullYear();



                    $("#Date").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d);
                
               // $("#StartTime").datetimepicker({format: 'H:i',minView: 2});

                 $('#StartTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                 $('#EndTime').timepicker({
                    format: 'H:mm',minView: 1
                });


                $('#ActivatedFromTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                 $('#ActivatedToTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                }, 500);

            }

        });

    }



    $scope.loadFormDelete = function(Position,QtAssignGUID){
      
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL+'questionassign/delete', 'SessionKey='+SessionKey+'&QtAssignGUID='+QtAssignGUID, contentType).then(function(response) {
              
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }




    $scope.loadAssignQuiz = function (position,QtPaperGUID,CourseGUID,SubjectGUID)

    {

        if(QtPaperGUID.length){

            $scope.getBatch();

            $scope.getSubCategoryList(CourseGUID);

            $scope.QtPaperGUID = QtPaperGUID;

            $scope.SubjectGUID = SubjectGUID;

            $scope.templateURLAssign = PATH_TEMPLATE+'questionassign/assign_paper.htm?'+Math.random();

            $scope.data.pageLoading = true;   

            $('#assign_model').modal({show:true}); 

            $scope.data.pageLoading = false; 



            $timeout(function(){        

                var d = new Date();

                var month = d.getMonth();

                var day = d.getDate();

                var year = d.getFullYear();

                $("#Date").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d);
                
                $('#StartTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                 $('#ActivatedFromTime').timepicker({
                    format: 'H:mm',minView: 1
                });

                //$('#StartTime').datetimepicker({pickDate: false,format: 'dd-mm-yyyy H:i' }).datetimepicker('setStartDate',d);

                //$('input[name=EndTime]').datetimepicker({pickDate: false });

            }, 500);

        }        

    }





    $scope.addAssignPaper = function (check="")

    {
        $scope.addAssignPaperLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='assign_paper']").serialize();

        console.log(data);

        $http.post(API_URL+'questionpaper/addAssignPaper', data, contentType).then(function(response) {

            var response = response.data;
            $scope.addAssignPaperLoading = false;
            if(response.ResponseCode==200){

                SuccessPopup(response.Message);

                if(check != 'AssignQuiz'){
                    $scope.getQuestionAssignStatistics();
                }else{
                    $scope.data = angular.copy($scope.orig);
                    $scope.getQuizAssignStatistics();
                }

                $('#assign_model .close').click();

            }else{                

                ErrorPopup(response.Message);

            }

        });

    }


    $scope.editAssignPaper = function (check="")

    {
        $scope.editAssignPaperLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='assign_paper']").serialize();

        console.log(data);

        $http.post(API_URL+'questionassign/editAssignPaper', data, contentType).then(function(response) {

            var response = response.data;
            $scope.editAssignPaperLoading = false;
            if(response.ResponseCode==200){

                SuccessPopup(response.Message);

                $('#edit_model .close').click();

                if(check != 'AssignQuiz'){
                    $scope.getBatchPapersList(1);
                }else{
                    $scope.data = angular.copy($scope.orig);
                    $scope.getQuizAssignStatistics();
                }

                

            }else{                

                ErrorPopup(response.Message);

            }

        });

    }



    $scope.tableDatatoExcel = function()
    {       
        var data = 'SessionKey=' + SessionKey+'&QtPaperGUID='+$scope.QtPaperGUID+'&CheckPaperData=Yes';
        $scope.data.newDataList = [];
        $scope.data.tempList = [];
        alertify.confirm('Are you sure you want to export result data?', function() 
        {
            $scope.data.excelLoading = true;

            $http.post(API_URL + 'questionpaper/getTestResults', data, contentType).then(function(response) 
            {
                var response = response.data;
                if (response.ResponseCode == 200 && response.Data) 
                {
                    for (var i in response.Data) 
                    {
                       if(i == 0)
                       {
                           var QtPaperTitle = "Paper Title: " + response.Data[0]['CommonDetails'].QtPaperTitle;

                           var BatchName = "Batch Name: " + response.Data[0]['CommonDetails'].BatchName;

                           var SubjectName = "Subject Name: " + response.Data[0]['CommonDetails']['SubjectName'];

                           var TestDuration = "Test Duration: " + response.Data[0]['CommonDetails']['TestDuration'] + " Min.";
                           var heads = [QtPaperTitle, BatchName, SubjectName, TestDuration, "", "", "", "", "", ""];
                           $scope.data.newDataList.push(heads);


                           var heads = ["Student", "AttemptDate", "Total Question", "Attempted", "Skip Questions", "Correct Answers", "Wrong Answers", "Total Marks", "Passing %", "Obtained Marks"];                    
                            $scope.data.newDataList.push(heads);

                           
                        } 

                    
                        tempList = [response.Data[i]['FirstName'] + ' ' + response.Data[i]['LastName'],
                            response.Data[i]['AttemptDate'],
                            response.Data[i]['TotalQuestions'],
                            response.Data[i]['AttemptQuestions'],
                            response.Data[i]['SkipQuestions'],
                            response.Data[i]['CorrectAnswers'],
                            response.Data[i]['WrongAnswers'],
                            response.Data[i]['TotalMarks'],
                            response.Data[i]['PassingMarks'],
                            response.Data[i]['GainedMarks']
                        ];

                        $scope.data.newDataList.push(tempList);
                    }

                    $("#dvjson").excelexportjs({
                        containerid: "dvjson"
                           , datatype: 'json'
                           , dataset: $scope.data.newDataList
                           , columns: getColumns($scope.data.newDataList)     
                    }); 

                    $scope.data.excelLoading = false;      
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
    }



}); 





/* sortable - starts */

function tblsort() {



  var fixHelper = function(e, ui) {

    ui.children().each(function() {

        $(this).width($(this).width());

    });

    return ui;

}



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

        data: order,

        stop: function(response) {

            if (response.status == "success") {

                window.location.href = window.location.href;

            } else {

                alert('Some error occurred');

            }

        }

    });

}

/* sortable - ends */

}





function getSubCategoryList(BatchGUID,apiCall=""){

  angular.element(document.getElementById('question-body')).scope().getSubjectByBatch(BatchGUID); 

  if(apiCall.length > 0){

    angular.element(document.getElementById('question-body')).scope().getQuestionAssignStatistics(BatchGUID);  

  }  

}





function filterAssignPaperStatistics(CategoryGUID,apiCall){

  var BatchGUID = $("#Batch").val();

  angular.element(document.getElementById('question-body')).scope().getQuestionAssignStatistics(BatchGUID,CategoryGUID);  

}


function filterBatchPapersList(CategoryGUID){

  var BatchGUID = $("#Batch").val();

  angular.element(document.getElementById('question-body')).scope().getBatchPapersList(1,BatchGUID,CategoryGUID);  

}





function getQuizAssignStatistics(BatchGUID){

  angular.element(document.getElementById('question-body')).scope().getQuizAssignStatistics(BatchGUID);  

}


function getCategorySubjects(BatchGUID,apiCall=""){
  angular.element(document.getElementById('question-body')).scope().getSubjectByBatch(BatchGUID);  

  if(apiCall.length > 0){

    angular.element(document.getElementById('question-body')).scope().getBatchPapersList(1,BatchGUID,"");  

  }  
}

function getSubjectByBatch(BatchGUID){

    angular.element(document.getElementById('question-body')).scope().getSubjectByBatch(BatchGUID);  

}


function getBatchByPaper(QtPaperGUID){

    angular.element(document.getElementById('question-body')).scope().getBatchByPaper(QtPaperGUID);  

}


// function setEventEndDateTime(date){     
//     var d = new Date(date);
//     d.setMinutes(d.getMinutes() + 5);
//     $('#EndTime').datetimepicker({format: 'dd-mm-yyyy H:i',pickDate: false }).datetimepicker('setStartDate',d);
// }


function setEventEndDateTime(date){     
    var d = new Date(date);
    d.setMinutes(d.getMinutes() + 5);
    $('#EndTime').timepicker({format: 'H:mm',minView: 1});
    //$('#EndTime').timepicker({format: 'H:i',pickDate: false }).timepicker('setStartDate',d);
}


function setActivatedEndDateTime(date){     
    var d = new Date(date);
    d.setMinutes(d.getMinutes() + 5);
    $('#ActivatedToTime').timepicker({format: 'H:mm',minView: 1});
    //$('#EndTime').timepicker({format: 'H:i',pickDate: false }).timepicker('setStartDate',d);
}