<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}

	function getStudentStatistics($EntityID,$CourseID="",$BatchID=""){
		$MasterFranchisee = $this->Common_model->getMasterFranchiseeByEntityID($EntityID);
		// if($MasterFranchisee == 'no'){
		// 	$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		// }else{
		// 	$InstituteID = $EntityID;
		// }

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		// "select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity ON tbl_entity.EntityID =  set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and CategoryID='".$CourseID."' and tbl_entity.StatusID = 2";
		if(!empty($CourseID)){
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity ON tbl_entity.EntityID =  set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and CategoryID='".$CourseID."' and tbl_entity.StatusID = 2");
		}else{
			//echo "select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity ON tbl_entity.EntityID =  set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and tbl_entity.StatusID = 2";
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity ON tbl_entity.EntityID =  set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and tbl_entity.StatusID = 2");
		}

		$course = $query->result_array();

		$result = array(); 
		$new_result = array(); 
		$check_array = array();
		$check = 1; 
		if(count($course) > 0){
			//print_r($course);
			$p = 0;
			foreach ($course as $key => $value) {

					if(!empty($BatchID)){
						$query = $this->db->query("select BatchID,BatchGUID,BatchName from tbl_batch join tbl_entity ON tbl_entity.EntityID =  tbl_batch.BatchID where tbl_entity.InstituteID = ".$InstituteID."  and CourseID = ".$value['CategoryID']." and BatchID = ".$BatchID);
					}else{
						//echo "select BatchID,BatchGUID,BatchName from tbl_batch join tbl_entity ON tbl_entity.EntityID =  tbl_batch.BatchID where tbl_entity.InstituteID = ".$InstituteID." and tbl_entity.StatusID = 2 and CourseID = ".$value['CategoryID'];
						$query = $this->db->query("select BatchID,BatchGUID,BatchName from tbl_batch join tbl_entity ON tbl_entity.EntityID =  tbl_batch.BatchID where tbl_entity.InstituteID = ".$InstituteID." and CourseID = ".$value['CategoryID']);
					}

					$SubCategoryData = $query->result_array();

					//print_r($SubCategoryData);

					$parentCategoryName = $value['CategoryName'];
					$parentCategoryGUID = $value['CategoryGUID'];
					$CourseID = $value['CategoryID'];
					$total_student = 0;

					foreach ($SubCategoryData as $k => $v) {

						if(!in_array($parentCategoryName, $check_array)){
							array_push($check_array, $parentCategoryName);
							$result[$parentCategoryName][$k]['ParentCategoryName'] = $parentCategoryName;
						}else{
							$result[$parentCategoryName][$k]['ParentCategoryName'] = "";	
						}
						$result[$parentCategoryName][$k]['ParentName'] = $parentCategoryName;
						$result[$parentCategoryName][$k]['parentCategoryGUID'] = $parentCategoryGUID;
						$result[$parentCategoryName][$k]['BatchName'] = $v['BatchName'];
						$result[$parentCategoryName][$k]['BatchGUID'] = $v['BatchGUID'];
						$result[$parentCategoryName][$k]['student_count'] = 0;

						//echo "SELECT count(student.StudentID) as count_student FROM tbl_students as student join tbl_entity ON tbl_entity.EntityID =  student.StudentID where tbl_entity.InstituteID = ".$InstituteID." and student.CourseID = ".$value['CategoryID']." and student.BatchID = ".$v['BatchID'];

						//echo "SELECT count(student.StudentID) as count_student FROM tbl_students as student where student.CourseID = ".$value['CategoryID']." student.BatchID = ".$v['BatchID'];
						$query = $this->db->query("SELECT count(student.StudentID) as count_student FROM tbl_students as student join tbl_entity ON tbl_entity.EntityID =  student.StudentID where tbl_entity.InstituteID = ".$InstituteID." and tbl_entity.StatusID != 3 and student.CourseID = ".$value['CategoryID']." and student.BatchID = ".$v['BatchID']);

						$data = $query->result_array();

						$result[$parentCategoryName][$k]['count_student'] = $data[0]['count_student'];
						
						$total_student = $total_student+$data[0]['count_student'];

						if(!empty($result[$parentCategoryName][$k])){
							array_push($new_result, $result[$parentCategoryName][$k]);
						}

						$count_sub = $k+1;
						if($count_sub == count($SubCategoryData)){ 	//echo $count_sub; echo "<br>";
							$result[$parentCategoryName][$count_sub]['student_count'] = $total_student;
							$result[$parentCategoryName][$count_sub]['ParentName'] = $parentCategoryName;
							$result[$parentCategoryName][$count_sub]['parentCategoryGUID'] = $parentCategoryGUID;
							$result[$parentCategoryName][$count_sub]['BatchName'] = 'Total';
							$result[$parentCategoryName][$count_sub]['BatchGUID'] = $v['BatchGUID'];
							//print_r(expression)
							array_push($new_result, $result[$parentCategoryName][$count_sub]);
						}

						//print_r($new_result);
					}
			}
			return $new_result;
		}
		return false;
	
	}



	/*
	Description: 	ADD user to system.
	Procedures:
	1. Add user to user table and get UserID.
	2. Save login info to users_login table.
	3. Save User details to users_profile table.
	4. Genrate a Token for Email verification and save to tokens table.
	5. Send welcome Email to User with Token.
	*/
	function addUser($EntityID, $Input=array(), $UserTypeID, $SourceID, $StatusID=1){
		//print_r($Input); die;
		$this->db->trans_start();
		
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($Input['Email'])){
			$this->db->select("UserID,UserGUID,UserTypeID");
			$this->db->from("tbl_users");
			$this->db->where("Email",$Input['Email']);
			$this->db->limit(1);
			$Query = $this->db->get();
			$return = array();
			if($Query->num_rows()>0){
				$data = $Query->row();
				if($data->UserTypeID!=7){
					return TRUE;
				}else{
					$return['UserID'] = $data->UserID;
					$return['UserGUID'] = $data->UserGUID;
					return $return;
				}
			}
		}

		$EntityGUID = get_guid();

		$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>15,"UserID"=> $EntityID,"StatusID"=>$Input['StatusID']));

		$this->db->where('EntityID',$EntityID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		/* Add user to user table . */
		if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
			$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
			//unset($Input['PhoneNumber']);
		}
		$InsertData = array_filter(array(
			"UserID" 				=> 	$EntityID,
			"UserGUID" 				=> 	$EntityGUID,			
			"UserTypeID" 			=>	7,
			"StoreID" 				=>	@$Input['StoreID'],
			"FirstName" 			=> 	@ucfirst(strtolower($Input['FirstName'])),
			"MiddleName" 			=> 	@ucfirst(strtolower($Input['MiddleName'])),			
			"LastName" 				=> 	@ucfirst(strtolower($Input['LastName'])),
			"About" 				=>	@$Input['About'],
			"ProfilePic" 			=>	@$Input['ProfilePic'],
			"ProfileCoverPic" 		=>	@$Input['ProfileCoverPic'],			
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],
			"BirthDate" 			=>	@$Input['BirthDate'],

			"Address" 				=>	@$Input['Address'],
			"Address1" 				=>	@$Input['Address1'],
			"Postal" 				=>	@$Input['Postal'],	
			"CountryCode" 			=>	@$Input['CountryCode'],

			"StateName" 			=>	@$Input['StateName'],
			"CityName" 			=>	@$Input['CityName'],

			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"Latitude" 				=>	@$Input['Latitude'],
			"Longitude"				=>	@$Input['Longitude'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"PhoneNumberForChange" 	=>	@$Input['PhoneNumberForChange'],
			"Website" 				=>	@strtolower($Input['Website']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),
			"TwitterURL" 			=>	@strtolower($Input['TwitterURL']),
			"LinkedInURL"	=>	@strtolower($Input['LinkedInURL']),
			"ReferredByUserID" 		=>	@$Input['Referral']->UserID,
		));

		//print_r($InsertData);
		
		$this->db->insert('tbl_users', $InsertData);

		//echo $this->db->last_query(); die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}else{
			if (!empty($Input['Email'])) {

				/* Send welcome Email to User with Token. (only if source is Direct) */

				$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
		        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
		
		        if(!empty($Record['InstituteProfilePic'])){
			    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
			    }else{
			    	$MediaURL= "";
			    }

				$Token = $this->Recovery_model->generateToken($EntityID, 2);

				$content = $this->load->view('emailer/signup', array(

						"Name" => $Input['FirstName'].' '.$Input['LastName'],

						'Token' => $Token,

						'DeviceTypeID' => 1,

						"InstituteProfilePic"=>@$MediaURL,

						'Signature' => "Thanks, <br>".$InstituteData['FirstName']

					) , TRUE);

				

				sendMail(array(

					"From_name"  => $InstituteData['FirstName'],

					'emailTo' => $Input['Email'],

					'emailSubject' => "Welcome at " . $InstituteData['FirstName'],

					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

				));

			}

			$InsertDataa = array_filter(array(
				"UserID" 		=> $EntityID,
				"Password"		=> NULL,
				"SourceID"		=> 1,
				"EntryDate"		=> date("Y-m-d H:i:s")));

			$this->db->insert('tbl_users_login', $InsertDataa);
		}
		return array("UserID"=>$EntityID,"UserGUID"=>$EntityGUID);
	}


	function getStudents($EntityID,$where=array(),$multiRecords=FALSE,$PageNo=1, $PageSize=10){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('tbl_entity.StatusID,tbl_users.UserGUID,tbl_users.StateName,tbl_users.FirstName,tbl_users.LastName,tbl_students.Email,tbl_students.PhoneNumber,tbl_students.FeeID,tbl_students.Key,tbl_students.KeyStatusID,tbl_students.InstallmentID,tbl_students.InstallmentID,tbl_users.PhoneNumber,tbl_students.TotalFeeAmount,tbl_students.TotalFeeAmount,IF(tbl_users.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",tbl_users.ProfilePic)) AS ProfilePic');
		$this->db->from("tbl_students");
		$this->db->join('tbl_users', 'tbl_students.StudentID = tbl_users.UserID');
		$this->db->join('tbl_entity', 'tbl_users.UserID = tbl_entity.EntityID');
		$this->db->join('tbl_batch', 'tbl_batch.BatchID = tbl_students.BatchID','left');
		//$this->db->join('tbl_setting_fee', 'tbl_setting_fee.FeeID = tbl_students.FeeID','left');
		//$this->db->join("tbl_users ON tbl_students.StudentID = tbl_users.UserID join tbl_entity ON tbl_users.UserID = tbl_entity.EntityID");  
		if(!empty($where['CourseID'])){
			$this->db->where("tbl_students.CourseID",$where['CourseID']);
		}
		if(!empty($where['CategoryID'])){
			$this->db->where("tbl_students.CourseID",$where['CategoryID']);
		}
		if(!empty($where['BatchID'])){
			$this->db->where("tbl_students.BatchID",$where['BatchID']);
		}

		if(!empty($where['Keyword'])){
			$where['Keyword'] = trim($where['Keyword']);
			// if(validateEmail($Where['Keyword'])){
			// 	$where['Email'] = $where['Keyword'];
			// }
			// elseif(is_numeric($where['Keyword'])){
			// 	$where['PhoneNumber'] = $where['Keyword'];
			// }
			// else{
				$this->db->group_start();
				$this->db->like("tbl_users.FirstName", trim($where['Keyword']));
				$this->db->or_like("tbl_users.LastName", trim($where['Keyword']));
				$this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($where['Keyword'])), FALSE);
				$this->db->or_like("CONCAT_WS(' ',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($where['Keyword'])), FALSE);
				$this->db->or_like("tbl_users.PhoneNumber", trim($where['Keyword']));
				$this->db->or_like("tbl_users.Email", trim($where['Keyword']));
				$this->db->or_like("tbl_students.Key",$where['Keyword']);
				// $this->db->or_like("tbl_students.Key",$where['Keyword']);
				$this->db->or_like("tbl_batch.BatchName",$where['Keyword']);
				if( strpos( $where['Keyword'], "Inactive" ) !== false || strpos( $where['Keyword'], "inactive" ) !== false) {
	    			$this->db->or_where("tbl_students.KeyStatusID",1);
				}
				if( strpos( $where['Keyword'], "Active" ) !== false || strpos( $where['Keyword'], "active" ) !== false) {
	    			$this->db->or_where("tbl_students.KeyStatusID",2);
				}
				if( strpos( $where['Keyword'], "Not Assigned" ) !== false || strpos( $where['Keyword'], "not assigned" ) !== false) {
	    			$this->db->or_where("tbl_students.KeyStatusID",0);
				}
				$this->db->group_end();				
			//}

		}

		if(!empty($where['Email'])){
			$this->db->where("tbl_users.Email",$where['Email']);
		}

		if(!empty($where['PhoneNumber'])){
			$this->db->where("tbl_users.PhoneNumber",$where['PhoneNumber']);
		}

		// if(!empty($where['Keyword'])){
		// 	$this->db->group_start();
		// 	$this->db->like("tbl_users.FirstName",$where['Keyword']);
		// 	$this->db->or_like("tbl_students.Key",$where['Keyword']);
		// 	$this->db->or_like("tbl_users.LastName",$where['Keyword']);
		// 	$this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.LastName)", preg_replace('/\s+/', '', $where['Keyword']), FALSE);
		// 	$this->db->or_like("tbl_users.Email",$where['Keyword']);
		// 	$this->db->or_like("tbl_users.PhoneNumber",$where['Keyword']);	
		// 	if( strpos( $where['Keyword'], "Inactive" ) !== false) {
  //   			$this->db->or_where("tbl_students.KeyStatusID",1);
		// 	}
		// 	if( strpos( $where['Keyword'], "Active" ) !== false) {
  //   			$this->db->or_where("tbl_students.KeyStatusID",2);
		// 	}
		// 	$this->db->group_end();
		// }

		if(isset($where['StudentType']) && !empty($where['StudentType']) && $where['StudentType'] == 28)
		{
			$this->db->where("tbl_entity.StatusID = ".$where['StudentType']);
		}
		elseif(isset($where['StudentType']) && !empty($where['StudentType']) && $where['StudentType'] == 3)
		{
			$this->db->where("tbl_entity.StatusID = 3");
		}
		else
		{
			$this->db->where("tbl_entity.StatusID != 3 AND tbl_entity.StatusID != 28");
		}	

		$this->db->where("tbl_entity.InstituteID",$InstituteID);

		$this->db->order_by('tbl_students.StudentID','DESC');
		/* Total records count only if want to get multiple records */
		$PageSize = 30;
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get(); //echo $this->db->last_query();
		$StudentData = $Query->result_array();		
		$Records = array();
		if(!empty($StudentData)){ $i=0;
			foreach ($Query->result_array() as $record) 
			{
				$record['FeeStatusID'] = 0;
				if(!empty($record['FeeID'])){
					$sql = "SELECT f.FullDiscountFee, e.StatusID as FeeStatusID 
					FROM tbl_setting_fee f
					INNER JOIN tbl_entity e ON e.EntityID = f.FeeID
					WHERE f.FeeID = ".$record['FeeID']."
					LIMIT 1";

					$query = $this->db->query($sql);
					$FeeData = $query->result_array();
					$record['FullDiscountFee'] = $FeeData[0]['FullDiscountFee'];
					$record['FeeStatusID'] = $FeeData[0]['FeeStatusID'];
				}else{
					$record['FullDiscountFee'] = 0;
				}
				

				if(!empty($record['InstallmentID'])){
					//echo "select TotalFee,InstallmentAmount,NoOfInstallment from tbl_setting_installment where InstallmentID = ".$record['InstallmentID'];
					$query = $this->db->query("select TotalFee,InstallmentAmount,NoOfInstallment from tbl_setting_installment where InstallmentID = ".$record['InstallmentID']);
					$InstallmentData = $query->result_array();		
					if(!empty($InstallmentData)){
						$record['TotalFee'] = $InstallmentData[0]['TotalFee'];
						$record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
						$record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
					}else{
						$record['TotalFee'] = '-';
						$record['InstallmentAmount'] = '-';
						$record['NoOfInstallment'] = '-';
					}			
				}else{
					$record['TotalFee'] = '-';
					$record['InstallmentAmount'] = '-';
					$record['NoOfInstallment'] = '-';
				}	

				//print_r($record);
				//array_push(array, var)
				//$ddddd[$i] = $record;
				$i++; 
				array_push($Records, $record); 
			}
			//print_r($Records);
			return $Records;
		}else{
			return $Records;
		}
	}


	function getPendingProfile($EntityID,$where=array(),$multiRecords=FALSE,$PageNo=1, $PageSize=10){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('tbl_student_pending_profile.*, tbl_students.CourseID, tbl_students.BatchID, tbl_students.GuardianName as Current_GuardianName, tbl_students.GuardianAddress as Current_GuardianAddress, tbl_students.GuardianPhone as Current_GuardianPhone, tbl_students.GuardianEmail as Current_GuardianEmail, tbl_students.FathersName as Current_FathersName, tbl_students.MothersName as Current_MothersName, tbl_students.PermanentAddress as Current_PermanentAddress, tbl_students.FathersPhone as Current_FathersPhone, tbl_students.FathersEmail as Current_FathersEmail, tbl_users.FirstName as Current_FirstName, tbl_users.LastName as Current_LastName, tbl_users.Email as Current_Email, tbl_users.PhoneNumber as Current_PhoneNumber, tbl_users.Gender as Current_Gender, tbl_users.Address as Current_Address, tbl_users.CountryCode as Current_CountryCode, tbl_users.CityName as Current_CityName, tbl_users.StateName as Current_StateName, tbl_users.StateID as Current_StateID, tbl_users.ProfilePic as Current_ProfilePic, tbl_users.Postal as Current_Postal, tbl_users.FacebookURL as Current_FacebookURL, tbl_users.TwitterURL as Current_TwitterURL, tbl_users.LinkedInURL as Current_LinkedInURL, tbl_users.InstagramURL as Current_InstagramURL,set_categories.CategoryName,tbl_batch.BatchName');

		$this->db->from("tbl_student_pending_profile");
		$this->db->join("tbl_students", 'tbl_student_pending_profile.StudentID = tbl_students.StudentID','left');
		$this->db->join('tbl_users', 'tbl_student_pending_profile.StudentID = tbl_users.UserID','left');
		$this->db->join("set_categories", 'set_categories.CategoryID = tbl_students.CourseID','left');
		$this->db->join("tbl_batch", 'tbl_batch.BatchID = tbl_students.BatchID','left');

		if(!empty($where['CourseID'])){
			$this->db->where("tbl_students.CourseID",$where['CourseID']);
		}
		if(!empty($where['CategoryID'])){
			$this->db->where("tbl_students.CourseID",$where['CategoryID']);
		}
		if(!empty($where['BatchID'])){
			$this->db->where("tbl_students.BatchID",$where['BatchID']);
		}

		if(!empty($where['Approved']) && $where['Approved'] == 1){
			$this->db->where("ApprovedByID IS NOT NULL");
		}else{
			$this->db->where("ApprovedByID IS NULL");
		}

		if(!empty($where['Keyword'])){
			$where['Keyword'] = trim($where['Keyword']);
			$this->db->group_start();
			$this->db->like("tbl_users.FirstName", trim($where['Keyword']));
			$this->db->or_like("tbl_users.LastName", trim($where['Keyword']));
			$this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($where['Keyword'])), FALSE);
			$this->db->or_like("CONCAT_WS(' ',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($where['Keyword'])), FALSE);
			$this->db->or_like("tbl_users.PhoneNumber", trim($where['Keyword']));
			$this->db->or_like("tbl_users.Email", trim($where['Keyword']));
			$this->db->or_like("tbl_students.Key",$where['Keyword']);
			$this->db->group_end();
		}

		$this->db->order_by('tbl_student_pending_profile.PendingProfileID','DESC');
		/* Total records count only if want to get multiple records */
		$PageSize = 30;
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get(); //echo $this->db->last_query();
		$StudentData = $Query->result_array();		
		$Records = array();
		if(!empty($StudentData)){ $i=0;
			foreach ($Query->result_array() as $record) {	
				if(!empty($record['ApprovedOn'])){
					$record['ApprovedOn'] = date("d-m-Y h:i:s", strtotime($record['ApprovedOn']));
					$EntityData = $this->Common_model->getInstituteDetailByID($record['ApprovedByID'],"Email,UserID,FirstName,LastName,PhoneNumber");
					$record['ApprovedBy'] = $EntityData['FirstName']." ".$EntityData['LastName'];
					$record['ApprovedByEmail'] = $EntityData['Email'];
					$record['ApprovedByPhoneNumber'] = $EntityData['PhoneNumber'];		
				}else{
					$record['ApprovedBy'] = "";
					$record['ApprovedByEmail'] = "";
					$record['ApprovedByPhoneNumber'] = "";
				}
			    $record['EntryDate'] = date("d-m-Y h:i:s", strtotime($record['EntryDate']));	
				array_push($Records, $record); 
			}
			return $Records;
		}else{
			return $Records;
		}

	}


	function approveProfileUpdate($EntityID, $Where = array()){
		$sql = 'UPDATE tbl_students t1 
        INNER JOIN tbl_student_pending_profile t2 
             ON t1.StudentID = t2.StudentID 
		SET t1.GuardianName = IF(LENGTH("t2.GuardianName") > 0, t2.GuardianName, t1.GuardianName),
		t1.GuardianAddress = IF(LENGTH("t2.GuardianAddress") > 0, t2.GuardianAddress, t1.GuardianAddress), 
		t1.GuardianPhone = IF(LENGTH("t2.GuardianPhone") > 0, t2.GuardianPhone, t1.GuardianPhone), 
		t1.GuardianEmail = IF(LENGTH("t2.GuardianEmail") > 0, t2.GuardianEmail, t1.GuardianEmail),
		t1.FathersName = IF(LENGTH("t2.FathersName") > 0, t2.FathersName, t1.FathersName),
		t1.MothersName = IF(LENGTH("t2.MothersName") > 0, t2.MothersName, t1.MothersName),
		t1.PermanentAddress = IF(LENGTH("t2.PermanentAddress") > 0, t2.PermanentAddress, t1.PermanentAddress),
		t1.FathersPhone = IF(LENGTH("t2.FathersPhone") > 0, t2.FathersPhone, t1.FathersPhone),
		t1.FathersEmail = IF(LENGTH("t2.FathersEmail") > 0, t2.FathersEmail, t1.FathersEmail)
		WHERE t1.CourseID = "'.$Where['CourseID'].'" and t1.BatchID = "'.$Where['BatchID'].'" and t2.PendingProfileID = "'.$Where['PendingProfileID'].'"';
		$result1 = $this->db->query($sql);
		//print_r($result_array); 
		//die;

		$sql = 'UPDATE tbl_users t1 
        INNER JOIN tbl_student_pending_profile t2 
             ON t1.UserID = t2.StudentID
		SET t1.FirstName = IF(LENGTH("t2.FirstName") > 0, t2.FirstName, t1.FirstName),
		t1.LastName = IF(LENGTH("t2.LastName") > 0, t2.LastName, t1.LastName), t1.Email = IF(LENGTH("t2.Email") > 0, t2.Email, t1.Email), t1.PhoneNumber = IF(LENGTH("t2.PhoneNumber") > 0, t2.PhoneNumber, t1.PhoneNumber),  t1.Address = IF(LENGTH("t2.Address") > 0, t2.Address, t1.Address),  t1.CountryCode = IF(LENGTH("t2.CountryCode") > 0, t2.CountryCode, t1.CountryCode), t1.StateName = IF(LENGTH("t2.StateName") > 0, t2.StateName, t1.StateName), t1.CityName = IF(LENGTH("t2.CityName") > 0, t2.CityName, t1.CityName), t1.ProfilePic = IF(LENGTH("t2.ProfilePic") > 0, t2.ProfilePic, t1.ProfilePic), t1.Postal = IF(LENGTH("t2.Postal") > 0, t2.Postal, t1.Postal)  
		WHERE t1.UserID = "'.$Where['StudentID'].'" and t2.PendingProfileID = "'.$Where['PendingProfileID'].'"';
		$result2 = $this->db->query($sql);

		if($result1 == 1 && $result2 == 1){
			$arr = array("ApprovedOn" => date('Y-m-d h:i:s'), "ApprovedByID" => $EntityID);
			$this->db->where("PendingProfileID",$Where['PendingProfileID']);
			$this->db->update("tbl_student_pending_profile",$arr);

			$EntityData = $this->Common_model->getInstituteDetailByID($EntityID,"Email,UserID,FirstName,LastName,PhoneNumber");

			$StudentData = $this->Common_model->getInstituteDetailByID($Where['StudentID'],"Email,UserID,FirstName,LastName,PhoneNumber");

			/* Send Email to student for update profile approval. */
			$EmailText = $EntityData['FirstName']." ".$EntityData['LastName']." has approved  request to update profile.";
			
			$content = $this->load->view('emailer/common', 
					array("Name" => $StudentData['FirstName'],'EmailText' => $EmailText) , TRUE);
			sendMail(array(
				'emailTo' => $StudentData['Email'],
				'emailSubject' => "Approved Profile Update Request",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			/* Send SMS to show student update profile status. */
			if(!empty($StudentData['PhoneNumber'])){
				sendSMS(array(
				 	'PhoneNumber' 	=> $StudentData['PhoneNumber'],			
				 	'Message'		=> "Your profile update request is approved by ".$EntityData['FirstName']." ".$EntityData['LastName']." and profile is updated successfully."
				));
			}


			//echo $this->db->last_query(); die;
			return TRUE;
		}else{
			return FALSE;
		}

	}


	function getStudentByID($EntityID,$UserID="",$CategoryID="",$BatchID=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('tbl_users.UserID, tbl_entity.StatusID, tbl_users.UserGUID, tbl_users.FirstName,tbl_users.LastName,tbl_users.Address,tbl_users.StateName,tbl_users.CityName,tbl_users.Postal,tbl_users.PhoneNumber,tbl_users.LinkedInURL,tbl_users.FacebookURL,tbl_users.Email, tbl_students.GuardianName,tbl_students.GuardianAddress,tbl_students.GuardianPhone, tbl_students.GuardianEmail, tbl_students.FathersName, tbl_students.MothersName, tbl_students.PermanentAddress, tbl_students.FathersPhone, tbl_students.FathersEmail,tbl_students.CourseID,tbl_students.BatchID,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.FeeStatusID,tbl_students.TotalFeeAmount,tbl_students.TotalFeeInstallments,tbl_students.InstallmentAmount, tbl_students.Remarks,IF(tbl_users.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",tbl_users.ProfilePic)) AS ProfilePic, tbl_students.DroppingDate, tbl_students.DroppingReason, tbl_students.AmountBalance, tbl_students.PaymentType, tbl_students.ChequeDate, tbl_students.ChequeNumberOrTransaction, tbl_students.BankOrOther');
		$this->db->from("tbl_students");
		$this->db->join("tbl_users","tbl_students.StudentID = tbl_users.UserID");
		$this->db->join("tbl_entity","tbl_users.UserID = tbl_entity.EntityID");
		if(!empty($InstituteID)){
			$this->db->where("InstituteID",$InstituteID);
		}
		if(!empty($CategoryID)){
			$this->db->where("CourseID",$CategoryID);
		}
		if(!empty($BatchID)){
			$this->db->where("BatchID",$BatchID);
		}
		if(!empty($UserID)){
			$this->db->where("tbl_users.UserID",$UserID);
			$this->db->limit(1);
		}
		$Query =  $this->db->get(); //echo $this->db->last_query(); die;
		//$StudentData = $Query->result_array(); //print_r($StudentData);
		$Records = array();
		if($Query->num_rows() > 0){
			foreach ($Query->result_array() as $record) 
			{
				$record['DueDates'] = array();				
				
				if(!isset($UserID) || empty($UserID)) $UserID = $record['UserID'];


				$sql = "SELECT date_format(DueDate, '%d-%m-%Y') as DueDate FROM tbl_students_feeduedate WHERE StudentID = ".$UserID;
				$query1 = $this->db->query($sql);								
				if($query1->num_rows() > 0)
				{
					$DueDates = $query1->result_array();
					if(isset($DueDates) && !empty($DueDates))
					{
						$record['DueDates'] = $DueDates;
					}
				}	

				if(!empty($record['CourseID']))
				{
					$query = $this->db->query("select CategoryGUID,CategoryName from set_categories where CategoryID = ".$record['CourseID']);
					$CategoryData = $query->result_array();
					$record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
					$record['CourseName'] = $CategoryData[0]['CategoryName'];
				}


				if(!empty($record['BatchID']))
				{
					$query = $this->db->query("select BatchName from tbl_batch where BatchID = ".$record['BatchID']);
					$BatchName = $query->result_array();
					$record['BatchName'] = $BatchName[0]['BatchName'];
				}


				$FBID = 0;
				$record['TotalFee'] = 0;                
                $record['NoOfInstallment'] = 0;                                   
                if(!empty($record['FeeID']))
                {
                    $FBID = $record['FeeID'];

                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();
                    $record['TotalFee'] = $FeeData[0]['FullDiscountFee'];
                    $record['NoOfInstallment'] = 1;
                    $record['InstallmentAmount'] = $record['TotalFee'];

                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment, FeeID
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {
                        $record['TotalFee'] = $InstallmentData[0]['TotalFee'];
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                        $record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];

                        $FBID = $InstallmentData[0]['FeeID'];;
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['InstallmentAmount'] = 0;
                }

                $record['IsPaidAnyFee'] = 0;
                $record['PaidEMICount'] = 0;
                $record['RemainingEMI'] = 0;
                $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$UserID. " AND CourseID = '".$CategoryID."' AND BatchID = '".$BatchID."'");
                $SumData = $query->result_array();  
                if(!empty($SumData) && $SumData[0]['FeeCollectionID'] != 0)
                {
                    $record['PaidEMICount'] = $SumData[0]['FeeCollectionID'];
                    //$record['RemainingEMI'] = $record['PaidEMICount'] - count($SumData);                    
                }
                $record['RemainingEMI'] = $record['NoOfInstallment'] - $record['PaidEMICount'];
                    
                

                $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$UserID. " AND CourseID = '".$CategoryID."' AND BatchID = '".$BatchID."'");
                $SumData = $query->result_array(); 
                if(!empty($SumData) && $SumData[0]['PaidAmount'] > 0){
                    $record['PaidAmount'] = $SumData[0]['PaidAmount'];
                }else{
                    $record['PaidAmount'] = 0;
                } 
                $record['RemainingAmount'] = (int)$record['TotalFee'] - (int)$record['PaidAmount'];


                
                $record['PaidFeeHistory'] = array();
                $query = $this->db->query("select * from tbl_fee_collection where StudentID = ".$UserID. " AND CourseID = '".$CategoryID."' AND BatchID = '".$BatchID."'");
                $SumData = $query->result_array();  
                if(isset($SumData) && !empty($SumData))
                {
                    foreach($SumData as $arr)
                    {
                    	if(isset($arr['Denomination']) && !empty($arr['Denomination']) && $arr['PaymentMode'] == "Cash")
                    	$arr['Denomination'] = json_decode($arr['Denomination']);
                    
                    	$record['PaidFeeHistory'][] = $arr;                 
                    }

                    $record['IsPaidAnyFee'] = 1;
                }

				
				if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
                    $record['Fee Status'] = "Unpaid";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Fee Status'] = "Paid";
                }else{
                    $record['Fee Status'] = "Unpaid";
                }
 
				if(!empty($record['StatusID']) && $record['StatusID'] == 1){
					$record['Email Verification'] = "Pending";
				}else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
					$record['Email Verification'] = "Done";
				}
				

				
				$record['FBList'] = array();
				if($FBID > 0)
				{
					$this->load->model('Fee_model');
					$record['FBList'] = $this->Fee_model->getFeeBreakupDetails($FBID, $InstituteID);
				}	

				//print_r($record);
				array_push($Records, $record);
			}
			//print_r($Records);
			return $Records;
		}else{
			return $Records;
		}
	}







	function addStudentOnly($UserID,$UserGUID,$Input=array()){
		$InsertDatas = array_filter(array(
			"StudentID" => 	$UserID,
			"StudentGUID" => $UserGUID,
			"CourseID" =>	@$Input['CategoryID'],
			"BatchID" =>	@$Input['BatchID'],	
			"Email" =>	@strtolower($Input['Email']),
			"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),
			"GuardianName" 	=>	@$Input['GuardianName'],
			"GuardianAddress"  =>	@$Input['GuardianAddress'],
			"GuardianPhone"  =>	@$Input['GuardianPhone'],
			"GuardianEmail"  =>	@$Input['GuardianEmail'],
			"FathersName"  =>	@$Input['FathersName'],
			"MothersName"  =>	@$Input['MothersName'],
			"PermanentAddress"  =>	@$Input['PermanentAddress'],
			"FathersPhone"  =>	@$Input['FathersPhone'],
			"FathersEmail"  =>	@$Input['FathersEmail'],
			"Remarks" =>	@$Input['Remarks']));
		$this->db->insert('tbl_students', $InsertDatas);
		//echo $this->db->last_query();
		$StudentID = $this->db->insert_id();
		if(!empty($StudentID)){
			return $StudentID;
		}else{
			return FALSE;
		}
	}


	function importStudents($EntityID,$Input=array(), $UserTypeID, $SourceID, $StatusID=1){
		//$this->db->trans_start();
		$EntityGUID = get_guid();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		/* Add user to entity table and get EntityID. */
		$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>15,"UserID"=> $EntityID,"StatusID"=>$Input['StatusID']));

		$this->db->where('EntityID',$EntityID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		/* Add user to user table . */
		if(!empty($EntityID)){
			if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
				$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
				unset($Input['PhoneNumber']);
			}
			$InsertData = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"UserGUID" 				=> 	$EntityGUID,			
				"UserTypeID" 			=>	7,
				"FirstName" 			=> 	@ucfirst(strtolower($Input['FirstName'])),		
				"LastName" 				=> 	@ucfirst(strtolower($Input['LastName'])),
				"Address" 				=>	@$Input['Address'],
				"Postal" 				=>	@$Input['Postal'],	
				"StateName" 			=>	@$Input['StateName'],
				"CityName" 			=>	@$Input['CityName'],
				"LinkedInURL" 			=>	@strtolower($Input['LinkedInURL']),
				"FacebookURL" 			=>	@strtolower($Input['FacebookURL'])
			));

			
			$this->db->insert('tbl_users', $InsertData);
			//echo $this->db->last_query();
			$UserID = $this->db->insert_id();

			if(!empty($UserID)){
				$InsertDatas = array_filter(array(
					"StudentID" => 	$EntityID,
					"StudentGUID" => $EntityGUID,
					"CourseID" =>	@$Input['CategoryID'],
					"BatchID" =>	@$Input['BatchID'],
					"FeeID" =>	@$Input['FeeID'],			
					"Email" =>	@strtolower($Input['Email']),
					"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),
					"GuardianName" 	=>	@$Input['GuardianName'],
					"GuardianAddress"  =>	@$Input['GuardianAddress'],
					"GuardianPhone"  =>	@$Input['GuardianPhone'],
					"GuardianEmail"  =>	@$Input['GuardianEmail'],
					"FathersName"  =>	@$Input['FathersName'],
					"MothersName"  =>	@$Input['MothersName'],
					"PermanentAddress"  =>	@$Input['PermanentAddress'],
					"FathersPhone"  =>	@$Input['FathersPhone'],
					"FathersEmail"  =>	@$Input['FathersEmail'],
					"Remarks" =>	@$Input['Remarks']));
				$this->db->insert('tbl_students', $InsertDatas);
				$StudentID = $this->db->insert_id();
				//echo $this->db->last_query();
				/* Save login info to users_login table. */
				$InsertDataa = array_filter(array(
					"UserID" 		=> $EntityID,
					"Password"		=> md5(($SourceID=='1' ? $Input['Password'] : $Input['SourceGUID'])),
					"SourceID"		=> $SourceID,
					"EntryDate"		=> date("Y-m-d H:i:s")));

				$this->db->insert('tbl_users_login', $InsertDataa);

				/*save user settings*/
				$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));

				// $this->db->trans_complete();
				// if ($this->db->trans_status() === FALSE)
				// {
				// 	return FALSE;
				// }
				return $EntityID;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	

	function getCourses($EntityID, $CategoryTypeID){
		/*Additional fields to select*/
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select("CategoryName,CategoryGUID,CategoryID,Duration");
		$this->db->from("set_categories");
		$this->db->join("tbl_entity as E", "E.EntityID = set_categories.CategoryID");
		$this->db->join("tbl_entity as EP", "EP.EntityID = set_categories.ParentCategoryID");
		$this->db->where("E.StatusID", 2);
		$this->db->where("EP.StatusID", 2);
		$this->db->where("EP.InstituteID", $InstituteID);
		$this->db->where("CategoryTypeID", $CategoryTypeID);

		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$course = $query->result_array();
			return 	$course;
		}
	}


	function getFeePlans($EntityID, $CategoryID){
		/*Additional fields to select*/
		//echo "select * from tbl_setting_fee where CourseID = ".$CategoryID;
		$query = $this->db->query("select * from tbl_setting_fee where CourseID = ".$CategoryID);
		$record = $query->result_array();
		$records['installment'] = array(); $records['fee'] = array();
		if(count($query->result_array()) > 0){
			foreach ($query->result_array() as $record) {
				$query = $this->db->query("select * from  tbl_setting_installment  where FeeID = ".$record['FeeID']);
				$installment = $query->result_array();
				if(!empty($installment)){
					foreach ($installment as $key => $value) {
						//print_r($value);
						array_push($records['installment'], $value);
					}
				}
				array_push($records['fee'], $record);
			}
		}
		return $records;
	}



	function addStudent($EntityID,$Input=array(), $UserTypeID, $SourceID, $StatusID=1, $CategoryID ,$FeeID, $BatchID){
		$this->db->trans_start();
		if(!empty($Input['PhoneNumber'])){
			$this->db->select('PhoneNumber');
			$this->db->from('tbl_users');
			$this->db->where('PhoneNumber',$Input['PhoneNumber']);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				return 'PhoneNumber Exist';
				die;
			}
		}
		if(!empty($Input['Email'])){
			$this->db->select("UserID,UserGUID,UserTypeID");
			$this->db->from("tbl_users");
			$this->db->join("tbl_entity","tbl_entity.EntityID = tbl_users.UserID");
			$this->db->where("Email",$Input['Email']);
			$this->db->where("tbl_entity.StatusID",2);
			$this->db->limit(1);
			$Query = $this->db->get();
			if($Query->num_rows()>0){
				$data = $Query->row();
				if($data->UserTypeID!=7){
					return FALSE;
				}else{
					if($FeeID!=""){
						$InstallmentID = 0;
					}else{
						$InstallmentID = $Input['FeeGUID'];
					}
					$EntityID = $data->UserID;
					$EntityGUID = $data->UserGUID;
					$InsertDatas = array_filter(array(
						"StudentID" => 	$EntityID,
						"StudentGUID" => $EntityGUID,			
						"CourseID" =>	$CategoryID,
						"BatchID" =>	$BatchID,
						"FeeID" =>	@$FeeID,
						"InstallmentID" => $InstallmentID,
						"FeeStatusID" => @$Input['FeeStatusID'],
						"TotalFeeAmount" =>	@$Input['TotalFeeAmount'],
						"InstallmentAmount" =>	@$Input['InstallmentAmount'],
						"TotalFeeInstallments" =>	@$Input['TotalFeeInstallments'],						
						"Email" =>	@strtolower($Input['Email']),
						"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),
						"GuardianName" 	=>	@$Input['GuardianName'],
						"GuardianAddress"  =>	@$Input['GuardianAddress'],
						"GuardianPhone"  =>	@$Input['GuardianPhone'],
						"GuardianEmail"  =>	@$Input['GuardianEmail'],
						"FathersName"  =>	@$Input['FathersName'],
						"MothersName"  =>	@$Input['MothersName'],
						"PermanentAddress"  =>	@$Input['PermanentAddress'],
						"FathersPhone"  =>	@$Input['FathersPhone'],
						"FathersEmail"  =>	@$Input['FathersEmail'],
						"Remarks" =>	@$Input['Remarks']));
					$this->db->insert('tbl_students', $InsertDatas);
					$StudentsID = $this->db->insert_id();



					//Insert record for due date-----------------------------
					$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
					if(isset($Input['duedates']) && !empty($Input['duedates']))
					{						
						foreach($Input['duedates'] as $d)
						{
							$InsertData = array(
							"InstituteID" 		=>	$InstituteID,
							"CourseID"			=>	$CategoryID,
							"BatchID" 			=>	$BatchID,
							"StudentID" 		=>	$EntityID,
							"DueDate"			=>	date("Y-m-d", strtotime($d)),
							"SaveDate"			=> date("Y-m-d H:i:s")
							);
							$this->db->insert('tbl_students_feeduedate', $InsertData);
						}	
					}
					
					//------------------------------------------------------------

					$this->db->trans_complete();
					if ($this->db->trans_status() === FALSE)
					{
						return FALSE;
					}
					return $EntityID;
				}
			}else{

				$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

				$InstituteID = $EntityID;
				$EntityGUID = get_guid();

				/* Add user to entity table and get EntityID. */
				$InsertData = array_filter(array(
					"EntityGUID" 		=>	$EntityGUID,
					"EntityTypeID"		=>	15,
					"CreatedByUserID" 	=>	$EntityID,
					"EntryDate" 		=>	date("Y-m-d H:i:s"),
					"StatusID"			=>	1
				));
				$this->db->insert('tbl_entity', $InsertData); //echo $this->db->last_query();
				$EntityID = $this->db->insert_id();
				

				$this->db->where('EntityID',$EntityID);
				$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));
				//c4ca4238a0b923820dcc509a6f75849b
				//c4ca4238a0b923820dcc509a6f75849b
				/* Add user to user table . */
				// if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
				// 	$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
				// 	unset($Input['PhoneNumber']);
				// }

				if(!empty($Input['StateID'])){
					$StateName = $this->Common_model->getStatesNameById($Input['StateID']);
				}else{
					$StateName = "";
				}

				$InsertData = array_filter(array(
					"UserID" 				=> 	$EntityID,
					"UserGUID" 				=> 	$EntityGUID,			
					"UserTypeID" 			=>	7,
					"FirstName" 			=> 	@ucfirst(strtolower($Input['FirstName'])),		
					"LastName" 				=> 	@ucfirst(strtolower($Input['LastName'])),
					"Email" 				=>	@strtolower($Input['Email']),
					"Address" 				=>	@$Input['Address'],
					"Postal" 				=>	@$Input['Postal'],	
					"StateName" 			=>	$StateName,
					"CityName" 			=>	@$Input['CityName'],
					"PhoneNumber" 			=>	@$Input['PhoneNumber'],
					"LinkedInURL" 			=>	@strtolower($Input['LinkedInURL']),
					"FacebookURL" 			=>	@strtolower($Input['FacebookURL'])
				));

				
				$this->db->insert('tbl_users', $InsertData);


			$InsertDataa = array(
			"UserID" 		=> $EntityID,
			"Password"		=> md5($this->Users_model->random_strings(6)),
			"SourceID"		=> 1,
			"EntryDate"		=> date("Y-m-d H:i:s"));
			$this->db->insert('tbl_users_login', $InsertDataa);






				if($FeeID!=""){
					$InstallmentID = 0;
				}else{
					$InstallmentID = $Input['FeeGUID'];
				}

				$InsertDatas = array_filter(array(
					"StudentID" => 	$EntityID,
					"StudentGUID" => $EntityGUID,			
					"CourseID" =>	$CategoryID,
					"BatchID" =>	$BatchID,
					"FeeID" =>	@$FeeID,
					"FeeStatusID" => @$Input['FeeStatusID'],
					"InstallmentID" => $InstallmentID,
					"TotalFeeAmount" =>	@$Input['TotalFeeAmount'],
					"InstallmentAmount" =>	@$Input['InstallmentAmount'],
					"TotalFeeInstallments" =>	@$Input['TotalFeeInstallments'],					
					"Email" =>	@strtolower($Input['Email']),
					"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),
					"GuardianName" 	=>	@$Input['GuardianName'],
					"GuardianAddress"  =>	@$Input['GuardianAddress'],
					"GuardianPhone"  =>	@$Input['GuardianPhone'],
					"GuardianEmail"  =>	@$Input['GuardianEmail'],
					"FathersName"  =>	@$Input['FathersName'],
					"MothersName"  =>	@$Input['MothersName'],
					"PermanentAddress"  =>	@$Input['PermanentAddress'],
					"FathersPhone"  =>	@$Input['FathersPhone'],
					"FathersEmail"  =>	@$Input['FathersEmail'],
					"Remarks" =>	@$Input['Remarks']));
				$this->db->insert('tbl_students', $InsertDatas);
				$StudentsID = $this->db->insert_id();

				
				//Insert record for due date-----------------------------
				$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
				if(isset($Input['duedates']) && !empty($Input['duedates']))
				{					
					foreach($Input['duedates'] as $d)
					{
						$InsertData = array(
						"InstituteID" 		=>	$InstituteID,
						"CourseID"			=>	$CategoryID,
						"BatchID" 			=>	$BatchID,
						"StudentID" 		=>	$EntityID,
						"DueDate"			=>	date("Y-m-d", strtotime($d)),
						"SaveDate"			=> date("Y-m-d H:i:s")
						);
						$this->db->insert('tbl_students_feeduedate', $InsertData);
					}	
				}				
				//------------------------------------------------------------


				if (!empty($Input['Email'])) {

					// Send welcome Email to User with Token. (only if source is Direct)

					$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName,ProfilePic");

					$instProfilePic = $InstituteData['ProfilePic'];

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
			        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
					
			        if(!empty($instProfilePic) && isset($instProfilePic))
			        {				    	
				    	$MediaURL= BASE_URL."uploads/profile/picture/".$instProfilePic;
				    }
				    /*elseif(!empty($Record['InstituteProfilePic']))
			        {				    	
				    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
				    }*/
				    else
				    {
				    	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
				    }

					$Token = $this->Recovery_model->generateToken($EntityID, 2);

					$content = $this->load->view('emailer/signup', array(

							"Name" => $Input['FirstName'].' '.$Input['LastName'],

							'Token' => $Token,

							'DeviceTypeID' => 1,

							"InstituteProfilePic"=>@$MediaURL,

							'Signature' => "Thanks, <br>".$InstituteData['FirstName']

						) , TRUE);

					sendMail(array(

						"From_name"  => $InstituteData['FirstName'],

						'emailTo' => $Input['Email'],

						'emailSubject' => "Welcome at " . $InstituteData['FirstName'],

						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

					));

				}

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return FALSE;
				}
				return $EntityID;
			}
		}

		
	}


	function editStudent($EntityID,$UserID,$Input=array(), $UserTypeID, $SourceID, $StatusID=1, $CategoryID ,$FeeID, $BatchID){
		$this->db->trans_start();
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		if(!empty($Input['PhoneNumber'])){
			$this->db->select('PhoneNumber');
			$this->db->from('tbl_users');
			$this->db->where('PhoneNumber',$Input['PhoneNumber']);
			$this->db->where('UserID != '.$UserID);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				return 'PhoneNumber Exist';
				die;
			}
		}


		$this->db->select('Email');
		$this->db->from('tbl_users');
		$this->db->where('UserID', $UserID);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$UserData = $query->result_array()[0];
		}


		if(!empty($Input['Email']))
		{
			if($UserData['Email']!=$Input['Email'])
			{
				$Input['EmailForChange'] = $Input['Email'];			
				/* Genrate a Token for Email verification and save to tokens table. */
				//$this->load->model('Recovery_model');

				$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName,ProfilePic");

				$instProfilePic = $InstituteData['ProfilePic'];

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
		        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
		
		        if(!empty($instProfilePic) && isset($instProfilePic))
			    {				    	
				  	$MediaURL= BASE_URL."uploads/profile/picture/".$instProfilePic;
				}
		        /*elseif(!empty($Record['InstituteProfilePic']))
		        {
			    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
			    }*/
			    else
			    {
			    	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
			    }

				$Token = $this->Recovery_model->generateToken($UserID, 2);	

				$content = $this->load->view('emailer/signup', 
						array("Name" => $Input['FirstName'],'Token' => $Token,"InstituteProfilePic"=>@$MediaURL,
							'Signature' => "Thanks, <br>".$InstituteData['FirstName']) , TRUE);

				sendMail(array(
					"From_name"  => $InstituteData['FirstName'],
					'emailTo' => $Input['EmailForChange'],
					'emailSubject' => "Verify Your Email",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));
				unset($UpdateArray['Email']);
			}
		}


		if(!empty($Input['StateID'])){
			$StateName = $this->Common_model->getStatesNameById($Input['StateID']);
		}else{
			$StateName = "";
		}

		//print_r($UpdateArray);

		

		//echo $this->db->last_query();

		/* Add user to user table . */
		/*if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
			$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
			unset($Input['PhoneNumber']);
		}*/

		$UpdateData = array_filter(array(
			"FirstName" 			=> 	@ucfirst(($Input['FirstName'])),		
			"LastName" 				=> 	@ucfirst(($Input['LastName'])),
			"Email" 				=>	@strtolower($Input['Email']),
			"Address" 				=>	@$Input['Address'],
			"Postal" 				=>	@$Input['Postal'],	
			"StateName" 			=>	$StateName,
			"CityName" 			=>	@$Input['CityName'],
			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"LinkedInURL" 			=>	@strtolower($Input['LinkedInURL']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL'])
		));

		$this->db->where('UserID',$UserID);
		$this->db->update('tbl_users', $UpdateData);

		if($FeeID!=""){
			$InstallmentID = NULL;
			$Input['TotalFeeAmount'] = 0;
			$Input['TotalFeeInstallments'] = 0;
			$Input['InstallmentAmount'] = 0;
		}else if($Input['FeeGUID'] == 'Customized'){
			$InstallmentID = NULL;
			$FeeID = NULL;
		}else{
			$InstallmentID = $Input['FeeGUID'];
			$FeeID = NULL;
			$Input['TotalFeeAmount'] = NULL;
			$Input['TotalFeeInstallments'] = NULL;
			$Input['InstallmentAmount'] = NULL;
		}

		if(empty($Input['FathersPhone']) || $Input['FathersPhone'] == 0){
			$Input['FathersPhone'] = NULL;
		}

		if(empty($Input['GuardianPhone']) || $Input['GuardianPhone'] == 0){
			$Input['GuardianPhone'] = NULL;
		}

		$UpdateDatas = array(		
			"CourseID" =>	$CategoryID,
			"BatchID" =>	$BatchID,
			"FeeID" => $FeeID,
			"FeeStatusID" => @$Input['FeeStatusID'],
			"InstallmentID" => $InstallmentID,
			"TotalFeeAmount" =>	@$Input['TotalFeeAmount'],
			"TotalFeeInstallments" =>	@$Input['TotalFeeInstallments'],
			"InstallmentAmount" =>	@$Input['InstallmentAmount'],					
			"Email" =>	@strtolower($Input['Email']),
			"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),
			"GuardianName" 	=>	@$Input['GuardianName'],
			"GuardianAddress"  =>	@$Input['GuardianAddress'],
			"GuardianPhone"  =>	@$Input['GuardianPhone'],
			"GuardianEmail"  =>	@$Input['GuardianEmail'],
			"FathersName"  =>	@$Input['FathersName'],
			"MothersName"  =>	@$Input['MothersName'],
			"PermanentAddress"  =>	@$Input['PermanentAddress'],
			"FathersPhone"  =>	@$Input['FathersPhone'],
			"FathersEmail"  =>	@$Input['FathersEmail'],
			"Remarks" =>	@$Input['Remarks']);

		

		$this->db->where('StudentID',$UserID);
		$this->db->update('tbl_students', $UpdateDatas);


		//Insert record for due date-----------------------------		
		if(isset($Input['duedates']) && !empty($Input['duedates']))
		{					
			$this->db->where("InstituteID", $InstituteID);
			$this->db->where("CourseID", $CategoryID);
			$this->db->where("BatchID", $BatchID);
			$this->db->where("StudentID", $UserID);
			$this->db->delete("tbl_students_feeduedate");

			foreach($Input['duedates'] as $d)
			{
				$InsertData = array(
				"InstituteID" 		=>	$InstituteID,
				"CourseID"			=>	$CategoryID,
				"BatchID" 			=>	$BatchID,
				"StudentID" 		=>	$UserID,
				"DueDate"			=>	date("Y-m-d", strtotime($d)),
				"SaveDate"			=> date("Y-m-d H:i:s")
				);
				$this->db->insert('tbl_students_feeduedate', $InsertData);
			}	
		}				
		//------------------------------------------------------------

		//echo $this->db->last_query(); die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return $EntityID;
	}


	function delete($EntityID, $UserID, $KeyStatusID){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->where("EntityID",$UserID);
		$this->db->where("InstituteID",$InstituteID);
		$this->db->update("tbl_entity",array("StatusID"=>3));

		if($KeyStatusID == 1){
			$this->db->select("Key");
			$this->db->from("tbl_students");
			$this->db->where("StudentID",$UserID);
			$this->db->order_by("StudentID","DESC");
			$this->db->limit(1);
			$data = $this->db->get();
			if($data->num_rows() > 0){
				$key = $data->result_array();
			    $key = base64_decode($key[0]['Key']);// die;
				if(!empty($key)){
					$this->db->where("KeyName",$key);
					$this->db->where("AssignedInstitute",$InstituteID);
					$this->db->update("set_keys",array("UsedStatus"=>NULL));
					//echo $this->db->last_query();
				}
			}
		}else if($KeyStatusID == 6){
			$this->db->select("Key");
			$this->db->from("tbl_students");
			$this->db->where("StudentID",$UserID);
			$this->db->order_by("StudentID","DESC");
			$this->db->limit(1);
			$data = $this->db->get();
			if($data->num_rows() > 0){
				$key = $data->result_array();
			    $key = base64_decode($key[0]['Key']); //die;
				if(!empty($key)){
					$this->db->where("KeyName",$key);
					$this->db->where("AssignedInstitute",$InstituteID);
					$this->db->update("set_keys",array("RenewStatus"=>1));
					//echo $this->db->last_query();
				}
			}
		}
		return TRUE;
	}


	function saveImage($EntityID, $UserID, $Input)
	{
		$this->db->where('UserID',$UserID);
		$this->db->update('tbl_users',array("ProfilePic"=>$Input['MediaName']));
		//echo $this->db->last_query();
		return TRUE;
	}


	/*----------------------------------------------------------------------------------------
    Scholarship Test Section
    ----------------------------------------------------------------------------------------*/
	function getScholarshipTests($Input = array())
	{
		//$UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		$UserID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$result = array();

		$QuestionsGroup = 5;		
			
		/*$sql = "SELECT qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qp.StartDateTime as StartTime, qp.EndDateTime as EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, s.CategoryName as SubjectName, c.CategoryName as CourseName
		FROM tbl_question_paper as qp
		INNER JOIN set_categories s ON s.CategoryID = qp.SubjectID AND s.CategoryTypeID = 3 
		INNER JOIN set_categories c ON c.CategoryID = qp.CourseID AND c.CategoryTypeID = 2		
		WHERE qp.QuestionsGroup = ".$QuestionsGroup." AND DATE(qp.StartDateTime) >= CURDATE()
		ORDER BY qp.StartDateTime, qp.QtPaperTitle
		";*/

		$append = "";
		$keyword = trim($Input['filterTest']);
		if(isset($keyword) && !empty($keyword))
		{
			$append = " AND (qp.QtPaperTitle LIKE '%$keyword%' OR				
				u.FirstName LIKE '%$keyword%' OR
				u.LastName LIKE '%$keyword%'
			) ";
		}		
		
		$sql = "SELECT qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, u.FirstName, u.LastName, DATE_FORMAT(qp.ScheduleDate,'%d-%m-%Y') as ScheduleDate, qp.Duration, DATE_FORMAT(qp.TestStartTime,'%h:%i %p') as TestStartTime, qp.ScheduleType, DATE_FORMAT(qp.TestActivatedFrom,'%h:%i %p') as TestActivatedFrom, DATE_FORMAT(qp.TestActivatedTo,'%h:%i %p') as TestActivatedTo
		FROM tbl_question_paper as qp
		INNER JOIN tbl_users u ON u.UserID = qp.EntityID AND u.UserTypeID = 10	
		WHERE qp.QuestionsGroup = ".$QuestionsGroup." AND DATE(qp.ScheduleDate) >= CURDATE() 
		AND qp.QtPaperID NOT IN (SELECT ResultID FROM tbl_test_results WHERE StudentID = '".$UserID."')
		AND qp.QtPaperID NOT IN (SELECT QtPaperID FROM tbl_scholarship_applicants WHERE StudentID = '".$UserID."' AND Status != 'pending' AND OrderID > 0)
		$append
		ORDER BY qp.StartDateTime, qp.QtPaperTitle";

		$query = $this->db->query($sql);		

		$data = $query->result_array();

		if(count($data) > 0)
		{
			foreach ($data as $key => $value) 
			{
				$QtBankIDsArr = array();
				$QtBankIDsArr[] = ($value['EasyLevelQuestionsID']) ? $value['EasyLevelQuestionsID'] : 0;
				$QtBankIDsArr[] = ($value['HighLevelQuestionsID']) ? $value['HighLevelQuestionsID']: 0;
				$QtBankIDsArr[] = ($value['ModerateLevelQuestionsID']) ? $value['ModerateLevelQuestionsID']: 0;

				$QtBankIDsArr =  array_unique($QtBankIDsArr);

				$QtBankIDs = implode(",", $QtBankIDsArr);
				if(isset($value['QuestionsID']) && !empty($value['QuestionsID']))
				{
					$QtBankIDs = $QtBankIDs.",".$value['QuestionsID'];
				}					

				if($QtBankIDs)
				{
					$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

					if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows())
					{
						$QuestionsMarks = $QuestionsMarks->result_array();

						$result[$key]['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
					}
					else
					{
						$result[$key]['TotalMarks'] = 0;
					}
				}
				else
				{
					$result[$key]['TotalMarks'] = 0;
				}

				//Extra Condition Code For API------------------------------------------
				if(!isset($result[$key]['TotalMarks']) || empty($result[$key]['TotalMarks']))
				{
					$result[$key]['TotalMarks'] = 0;
				}

				
				$result[$key]['QtAssignGUID'] = $value['QtAssignGUID'];
				$result[$key]['QtPaperTitle'] = $value['QtPaperTitle'];
				$result[$key]['NegativeMarks'] = $value['NegativeMarks'];
				$result[$key]['PassingMarks'] = $value['PassingMarks'];
				$result[$key]['QuestionsGroup'] = $value['QuestionsGroup'];
				$result[$key]['TotalQuestions'] = $value['TotalQuestions'];
				$result[$key]['QtPaperGUID'] = $value['QtPaperGUID'];					
				
				$result[$key]['ScheduleDate'] = $value['ScheduleDate'];				
				$result[$key]['InstituteName'] = $value['FirstName']." ".$value['LastName'];
				$result[$key]['QtPaperID'] = $value['QtPaperID'];
				$result[$key]['TestDuration'] = $value['Duration'];

				if($value['ScheduleType'] == 2)
				{
					$result[$key]['StartTime'] = $value['TestStartTime'];					
				}
				else
				{
					$result[$key]['StartTime'] = $value['TestActivatedFrom']." - ".$value['TestActivatedTo'];
				}
				
			}
		}
		

		return $result;
	}


	function getAppliedScholarshipTests($Input = array())
	{
		$UserID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$result = array();

		$QuestionsGroup = 5;

		$append = "";
		$keyword = trim($Input['filterTest']);
		if(isset($keyword) && !empty($keyword))
		{
			$append = " AND (qp.QtPaperTitle LIKE '%$keyword%' OR				
				u.FirstName LIKE '%$keyword%' OR
				u.LastName LIKE '%$keyword%' OR
				a.KeyName LIKE '%$keyword%'
			) ";
		}

		$sql = "SELECT qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, u.FirstName, u.LastName, DATE_FORMAT(qp.ScheduleDate,'%d-%m-%Y') as ScheduleDate, qp.Duration, DATE_FORMAT(qp.TestStartTime,'%h:%i %p') as TestStartTime, DATE_FORMAT(qp.TestActivatedFrom,'%h:%i %p') as TestActivatedFrom, DATE_FORMAT(qp.TestActivatedTo,'%h:%i %p') as TestActivatedTo, qp.ScheduleType, a.*, rst.ResultGUID
		FROM tbl_question_paper as qp
		INNER JOIN tbl_users u ON u.UserID = qp.EntityID AND u.UserTypeID = 10	
		INNER JOIN tbl_scholarship_applicants a ON a.QtPaperID = qp.QtPaperID AND a.StudentID = '".$UserID."' AND a.Status IN('success','Success') AND a.OrderID > 0
		LEFT JOIN tbl_test_results rst ON rst.QtPaperID = a.QtPaperID AND a.StudentID = rst.StudentID AND rst.StudentID = '".$UserID."'
		WHERE qp.QuestionsGroup = ".$QuestionsGroup." $append
		ORDER BY qp.ScheduleDate, qp.TestStartTime, qp.QtPaperTitle";

		/*$sql = "SELECT qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qp.StartDateTime as StartTime, qp.EndDateTime as EndTime, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, u.FirstName as InstituteName, a.PaidAmount, DATE_FORMAT(a.PaymentDate, '%d-%M-%Y') as PaymentDate
		FROM tbl_question_paper as qp
		INNER JOIN tbl_users u ON u.UserID = qp.EntityID AND u.UserTypeID = 10	
		INNER JOIN tbl_scholarship_applicants a ON a.QtPaperID = qp.QtPaperID AND a.StudentID = '".$UserID."' AND a.Status != 'pending' AND a.OrderID > 0
		WHERE qp.QuestionsGroup = ".$QuestionsGroup."
		AND qp.QtPaperID NOT IN (SELECT ResultID FROM tbl_test_results WHERE StudentID = '".$UserID."')		
		$append
		ORDER BY qp.StartDateTime, qp.QtPaperTitle";*/

		$query = $this->db->query($sql);		

		$data = $query->result_array();

		if(count($data) > 0)
		{
			foreach ($data as $key => $value) 
			{
				$QtBankIDsArr = array();
				$QtBankIDsArr[] = ($value['EasyLevelQuestionsID']) ? $value['EasyLevelQuestionsID'] : 0;
				$QtBankIDsArr[] = ($value['HighLevelQuestionsID']) ? $value['HighLevelQuestionsID']: 0;
				$QtBankIDsArr[] = ($value['ModerateLevelQuestionsID']) ? $value['ModerateLevelQuestionsID']: 0;

				$QtBankIDsArr =  array_unique($QtBankIDsArr);

				$QtBankIDs = implode(",", $QtBankIDsArr);
				if(isset($value['QuestionsID']) && !empty($value['QuestionsID']))
				{
					$QtBankIDs = $QtBankIDs.",".$value['QuestionsID'];
				}					

				if($QtBankIDs)
				{
					$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

					if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows())
					{
						$QuestionsMarks = $QuestionsMarks->result_array();

						$result[$key]['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
					}
					else
					{
						$result[$key]['TotalMarks'] = 0;
					}
				}
				else
				{
					$result[$key]['TotalMarks'] = 0;
				}

				//Extra Condition Code For API------------------------------------------
				if(!isset($result[$key]['TotalMarks']) || empty($result[$key]['TotalMarks']))
				{
					$result[$key]['TotalMarks'] = 0;
				}

				
				$result[$key]['QtAssignGUID'] = $value['QtAssignGUID'];
				$result[$key]['QtPaperTitle'] = $value['QtPaperTitle'];
				$result[$key]['NegativeMarks'] = $value['NegativeMarks'];
				$result[$key]['PassingMarks'] = $value['PassingMarks'];
				$result[$key]['QuestionsGroup'] = $value['QuestionsGroup'];
				$result[$key]['TotalQuestions'] = $value['TotalQuestions'];
				$result[$key]['QtPaperGUID'] = $value['QtPaperGUID'];					
				
				$result[$key]['ScheduleDate'] = $value['ScheduleDate'];								
				$result[$key]['InstituteName'] = $value['FirstName']." ".$value['LastName'];
				$result[$key]['QtPaperID'] = $value['QtPaperID'];
				$result[$key]['TestDuration'] = $value['Duration'];

				$result[$key]['ValidUpto'] = $value['ValidUpto'];
				$result[$key]['KeyNames'] = $value['KeyName'];
				$result[$key]['PaymentDate'] = date("d-M-Y H:i", strtotime($value['PaymentDate']));
				$result[$key]['ResultGUID'] = $value['ResultGUID'];

				
				if($value['ScheduleType'] == 2)
				{
					$result[$key]['StartTime'] = $value['TestStartTime'];					
				}
				else
				{
					$result[$key]['StartTime'] = $value['TestActivatedFrom']." - ".$value['TestActivatedTo'];
				}
			}
		}
		

		return $result;
	}

 
	function applyForScholarshipTest($StudentID, $QtPaperID, $isSave = 0)
	{
		$chk = true;

		$sql = "SELECT ApplyID
		FROM tbl_scholarship_applicants
		WHERE StudentID = '$StudentID' AND QtPaperID = '$QtPaperID' AND Status != 'pending' AND OrderID > 0
		LIMIT 1	";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows() > 0)
		{
			$chk = false;
		} 


		if($isSave == 1)
		{
			$this->db->trans_start();

			$insertArr = array("QtPaperID"=>$QtPaperID,
			"StudentID"=>$StudentID,
			"ApplyDate"=>date('Y-m-d h:i:s'),
			"PaidAmount"=>300
			);

			$this->db->insert('tbl_scholarship_applicants', $insertArr);

			$ApplyID = $this->db->insert_id();

			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}

			return array("ApplyID" => $ApplyID, "StudentID" => $StudentID, "QtPaperID" => $QtPaperID);
		}

		return $chk;
	}


	function isAttemptTest($StudentID, $QtPaperID)
	{
		$sql = "SELECT ResultID
		FROM tbl_test_results
		WHERE StudentID = '$StudentID' AND QtPaperID = '$QtPaperID'
		LIMIT 1	";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows() > 0)
		{
			return true;
		}

		return false;
	}


	function getScholarshipPayments($EntityID, $where = array(), $multiRecords=FALSE,$PageNo=1, $PageSize=10){
		//$query = $this->db->query("select * from tbl_scholarship_applicants where StudentID = ".$EntityID);
		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);
		//print_r($where); die;
		$this->db->select("sa.*,qp.QtPaperTitle,CONCAT_WS(' ',u.FirstName,u.LastName) FullName,u.Email,u.PhoneNumber");
		$this->db->from("tbl_scholarship_applicants sa");
		$this->db->join("tbl_question_paper qp","qp.QtPaperID = sa.QtPaperID", "left");
		$this->db->join("tbl_users u","u.UserID = sa.StudentID", "left");

		if($UserTypeID!=1){
			$this->db->where("sa.StudentID",$EntityID);
		}

		if(!empty($where['Keyword'])){
			$this->db->group_start();
			$this->db->like("qp.QtPaperTitle",$where['Keyword']);
			$this->db->or_like("sa.PaidAmount",$where['Keyword']);
			$this->db->or_like("sa.ApplyDate",$where['Keyword']);
			$this->db->or_like("sa.PaymentDate",$where['Keyword']);
			$this->db->or_like("sa.TrackingID",$where['Keyword']);
			$this->db->or_like("sa.PaymentType",$where['Keyword']);
			$this->db->or_like("sa.Status",$where['Keyword']);
			$this->db->group_end();
		}

		if(!empty($where['filterKeyword'])){
			$this->db->group_start();
			$this->db->like("qp.QtPaperTitle",$where['filterKeyword']);
			$this->db->or_like("sa.PaidAmount",$where['filterKeyword']);
			$this->db->or_like("sa.ApplyDate",$where['filterKeyword']);
			$this->db->or_like("sa.PaymentDate",$where['filterKeyword']);
			$this->db->or_like("sa.TrackingID",$where['filterKeyword']);
			$this->db->or_like("sa.PaymentType",$where['filterKeyword']);
			$this->db->or_like("sa.Status",$where['filterKeyword']);
			$this->db->or_like("u.FirstName",$where['filterKeyword']);
			$this->db->or_like("u.LastName",$where['filterKeyword']);
			$this->db->or_like("u.Email",$where['filterKeyword']);
			$this->db->or_like("u.PhoneNumber",$where['filterKeyword']);
			$this->db->group_end();
		}


		if(!empty($where['ApplyDate'])){
			$this->db->like("sa.ApplyDate",date("Y-m-d", strtotime($where['ApplyDate'])));
		}

		if(!empty($where['PaymentDate'])){
			$this->db->like("sa.PaymentDate",date("Y-m-d", strtotime($where['PaymentDate'])));
		}


		if(isset($where['filterFromDate']) && !empty($where['filterFromDate']) && isset($where['filterToDate']) && !empty($where['filterToDate']))
		{
			$this->db->where("DATE(sa.ApplyDate) >= '".$where['filterFromDate']."' AND DATE(e.ApplyDate) <= '".$where['filterToDate']."'");
		}else if(isset($where['filterFromDate']) && !empty($where['filterFromDate']) && empty($where['filterToDate'])){
			$this->db->like("sa.ApplyDate",date("Y-m-d", strtotime($where['filterFromDate'])));
		}else if(isset($where['filterToDate']) && !empty($where['filterToDate']) && empty($where['filterFromDate'])){
			$this->db->like("sa.ApplyDate",date("Y-m-d", strtotime($where['filterToDate'])));
		}

		$PageSize = 25;
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get();  //print_r($where); die;
		//echo $this->db->last_query(); die;
		$records = array();
		if($Query->num_rows() > 0){
			foreach ($Query->result_array() as $record) {

				$this->db->select('CONCAT_WS(" ",u.FirstName,u.LastName) FullName,u.Email,u.PhoneNumber, IF(u.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",u.ProfilePic)) AS ProfilePic');
				$this->db->from("tbl_users u");
				$this->db->join("tbl_entity e","u.UserID = e.InstituteID", "left");
				$this->db->where("e.EntityID",$record['QtPaperID']);
				$this->db->limit(1);
				$Query1 = $this->db->get();

				if($Query1->num_rows() > 0){
					$record['InstituteDetail'] = $Query1->result_array()[0];
				}

				$record['ApplyDate'] = date("d-m-Y h:i", strtotime($record['ApplyDate']));
				$record['PaymentDate'] = date("d-m-Y h:i", strtotime($record['ApplyDate']));
				$records[] = $record;

			}
		}
		return $records;
	}


	function savePaymentForScholarshipTest($StudentID, $QtPaperID, $Inputs = array())
	{
		$ApplyID = $Inputs['ApplyID'];

		$sql = "SELECT qp.QtPaperTitle, u.FirstName, u.LastName, u.PhoneNumber, u.PhoneNumberForChange, u.Email, u.EmailForChange, qp.ScheduleDate, DATE_FORMAT(qp.TestActivatedFrom,'%h:%i %p') as TestActivatedFrom, DATE_FORMAT(qp.TestActivatedTo,'%h:%i %p') as TestActivatedTo, DATE_FORMAT(qp.TestStartTime,'%h:%i %p') as TestStartTime, qp.Duration, qp.ScheduleType
		FROM tbl_question_paper as qp
		INNER JOIN tbl_users u ON u.UserID = qp.EntityID AND u.UserTypeID = 10	
		INNER JOIN tbl_scholarship_applicants a ON a.QtPaperID = qp.QtPaperID AND a.StudentID = '".$StudentID."' AND a.ApplyID = '".$ApplyID."'
		WHERE qp.QtPaperID = '".$QtPaperID."' AND a.ApplyID = '".$ApplyID."'
		LIMIT 1
		"; 

		$query = $this->db->query($sql);		

		$result_data = $query->result_array();
		if(isset($result_data) && !empty($result_data))
		{
			if(!isset($result_data[0]['Email']) || empty($result_data[0]['Email']))
				$result_data[0]['Email'] = $result_data[0]['EmailForChange'];

			if(!isset($result_data[0]['PhoneNumber']) || empty($result_data[0]['PhoneNumber']))
				$result_data[0]['PhoneNumber'] = $result_data[0]['PhoneNumberForChange'];

			$instEmail = $result_data[0]['Email'];
			$instName = $result_data[0]['FirstName']." ".$result_data[0]['LastName'];
			$instPhoneNumber = $result_data[0]['PhoneNumber'];
			$instScheduleDate = date("d-M-Y", strtotime($result_data[0]['ScheduleDate']));
			$instDuration = $result_data[0]['Duration'];

			if($result_data[0]['ScheduleType'] == 2)
			{
				$instTestStartTime = $result_data[0]['TestStartTime'];
			}
			else
			{
				$instTestStartTime = $result_data[0]['TestActivatedFrom']." - ".$result_data[0]['TestActivatedTo'];
			}			

			$ValidUpto = date("Y-m-d", strtotime($instScheduleDate." +7 DAY"));

			$this->load->model('Keys_model');
			$GeneratedKeyName = "ST#".$this->Keys_model->random_strings(6);
			

			$this->db->select("ApplyID");
			$this->db->from("tbl_scholarship_applicants");
			$this->db->where("KeyName = '".$GeneratedKeyName."' ");
			$this->db->limit(1);
			$userLoginKey = $this->db->get();
			if($userLoginKey->num_rows() > 0)
			{
				$GeneratedKeyName = "ST#".time().$StudentID;				
			}
			
			$GeneratedKeyNameMD = md5($GeneratedKeyName);
			
			$PaymentDate = date("Y-m-d H:i:s"); 

			$Data = array("PaidAmount" => $Inputs['PaidAmount'],		
			"PaymentDate" => $PaymentDate, 
			"OrderID" => $Inputs['OrderID'],
			"TrackingID" => $Inputs['TrackingID'],
			"PaymentType" => $Inputs['PaymentType'],
			"KeyName" => $GeneratedKeyName,
			"ValidUpto" => $ValidUpto,
			"Status" => $Inputs['OrderStatus']);
			

			$this->db->where('ApplyID', $ApplyID);		
			$this->db->where('StudentID', $StudentID);
			$this->db->update('tbl_scholarship_applicants', $Data);			
			
			

			$InsertDataa = array(
			"UserID" 		=> $StudentID,
			"Password"		=> $GeneratedKeyNameMD,
			"SourceID"		=> 6,
			"EntryDate"		=> date("Y-m-d H:i:s"));
			$this->db->insert('tbl_users_login', $InsertDataa);
			
			
			$sql = "SELECT u.FirstName, u.LastName, u.PhoneNumber, u.PhoneNumberForChange, u.Email, u.EmailForChange
			FROM tbl_scholarship_applicants as a
			INNER JOIN tbl_users u ON u.UserID = a.StudentID			
			WHERE a.QtPaperID = '".$QtPaperID."' AND a.ApplyID = '".$ApplyID."'
			LIMIT 1
			"; 
			$query = $this->db->query($sql);
			$StudentData = $query->result_array();
			
			//$StudentData = $this->Common_model->getInstituteDetailByID($StudentID,"Email,EmailForChange,FirstName,LastName,PhoneNumber,PhoneNumberForChange");
			
			if(!isset($StudentData[0]['Email']) || empty($StudentData[0]['Email']))
				$StudentData[0]['Email'] = $StudentData[0]['EmailForChange'];

			if(!isset($StudentData[0]['PhoneNumber']) || empty($StudentData[0]['PhoneNumber']))
				$StudentData[0]['PhoneNumber'] = $StudentData[0]['PhoneNumberForChange'];

			$studentEmail = $StudentData[0]['Email'];
			$studentPhoneNumber = $StudentData[0]['PhoneNumber'];
			$Inputs['PaymentDate'] = date("d-M-Y", strtotime($PaymentDate));

			$ValidUpto = date("d-m-Y", strtotime($ValidUpto));
			if(!empty($studentPhoneNumber))
			{	
				$msg = "Download 'ourapp' https://play.google.com/store/search?q=ourapp&hl=en. Use key: ".$GeneratedKeyName." to login. Key valid till ".$ValidUpto.". Happy learning! Iconik";

				sendSMS(array(
				 	'PhoneNumber' 	=> $studentPhoneNumber,			
				 	'Message'		=> $msg
				));
			}


			//Sending email to Student---------------------------------------------------
			$EmailText = "<p>You successfully applied and purchase the scholarship test of <b>".strtoupper($instName)."</b></p>";

			$EmailText .= "<p>Scholarship Test Name: <b>".ucfirst($result_data[0]['QtPaperTitle'])."</b></p>";
			
			$EmailText .= "<p>Test Time: ".$instTestStartTime."<br/>Duration: ".$instDuration." Hrs<br/></p>";
						
			$EmailText .= "<p>To give test from mobile app, Download 'ourapp' https://play.google.com/store/search?q=ourapp&hl=en. Use key: ".$GeneratedKeyName." to login. Key valid till ".$ValidUpto.".</p>";

			$EmailText .= "<p>Purchase details are mentioned below.<br/>";

			$EmailText .= "Order ID: ".$Inputs['OrderID']."<br/>";

			$EmailText .= "Transaction ID: ".$Inputs['TrackingID']."<br/>";

			$EmailText .= "Amount : ".$Inputs['PaidAmount']."<br/>";

			$EmailText .= "Payment Date : ".$Inputs['PaymentDate']."<br/></p>";
			$content = $EmailText;
			sendMail(array(
			'emailTo'       => $studentEmail,         
			'emailSubject'  => "Purchasing Details Of Scholarship Test - Iconik",
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));


			//Sending email to Institute---------------------------------------------------
			$EmailText = "<p>New user is applied for ".ucfirst($result_data[0]['QtPaperTitle'])." scholarship test";

			$EmailText .= "<p>Scholarship Test Name: ".ucfirst($result_data[0]['QtPaperTitle'])."<b></b></p>";

			$EmailText .= "<p>User details are as follows:</p>";

			$EmailText .= "<p>User Name: ".ucwords($StudentData[0]['FirstName'].' '.$StudentData[0]['LastName'])."</p>";
			$EmailText .= "<p>User Email: ".$studentEmail."</p>";
			$EmailText .= "<p>Mobile: ".$studentPhoneNumber."</p>";

			$EmailText .= "<p>Order ID: ".$Inputs['OrderID']."</p>";		

			$EmailText .= "<p>Applied Date : ".$Inputs['PaymentDate']."</p>";
			
			$content = $EmailText;

			sendMail(array(
			'emailTo'       => $instEmail,         
			'emailSubject'  => "New User Applied For Scholarship Test - Iconik",
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));
		}
		
		return true;	
	}


	function getQuestionAnswerList($EntityID,$QtPaperID,$fields)
	{		
		$StudentID = $EntityID;

		if($this-> applyForScholarshipTest($StudentID, $QtPaperID))
		{
			return 'not_apply'; die;
		}

		if($this-> isAttemptTest($StudentID, $QtPaperID))
		{
			return 'attempt'; die;
		}


		$Return = array('Data' => array('Records' => array()));
		
		$this->db->select(implode(',', $fields));
		$this->db->from('tbl_question_paper');
		$this->db->where('QtPaperID',$QtPaperID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$QuestionsLevelEasy = array();

					if($Record['QuestionsLevelEasy'] > 0)
					{
						if(!empty($Record['EasyLevelQuestionsID']))
						{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['EasyLevelQuestionsID'],',').")");
						}
						else
						{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.SubjectID = '".$Record['SubjectID']."' and QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%' and QB.QuestionsLevel = 'Easy' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2 ORDER BY rand() limit ".$Record['QuestionsLevelEasy']);
						}
						
						$QuestionsLevelEasy = $course->result_array(); 
						foreach ($QuestionsLevelEasy as $key => $value) 
						{
							$QuestionsLevelEasy[$key]['QuestionContent'] = htmlspecialchars(trim(strip_tags($value['QuestionContent'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$QuestionsLevelEasy[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."'");
							$answer = $answer->result_array();

							if($value['QuestionsType'] == 'Short Answer'){ 
								foreach ($answer as $ka => $va) {
									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(strip_tags($va['AnswerContent'])));
								}
							}

							$QuestionsLevelEasy[$key]['answer'] = $answer;
						}

						$ids = array_column($QuestionsLevelEasy, 'QtBankID');

						if(!empty($ids)){
							$this->db->where('QtPaperID', $QtPaperID);
							$this->db->update('tbl_question_paper', array('EasyLevelQuestionsID' => implode(',', $ids)));
						}
					}

					$QuestionsLevelModerate = array();
					if($Record['QuestionsLevelModerate'] > 0)
					{
						if(!empty($Record['ModerateLevelQuestionsID']))
						{
							//echo "select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['ModerateLevelQuestionsID'],',').")";
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['ModerateLevelQuestionsID'],',').")");
						}
						else
						{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.SubjectID = '".$Record['SubjectID']."' and QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%' and QB.QuestionsLevel = 'Moderate' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2  ORDER BY rand() limit ".$Record['QuestionsLevelModerate']);
						}
						$QuestionsLevelModerate = $course->result_array(); 
						
						foreach ($QuestionsLevelModerate as $key => $value) 
						{
							$QuestionsLevelModerate[$key]['QuestionContent'] = htmlspecialchars(trim(strip_tags($value['QuestionContent'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$QuestionsLevelModerate[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());


							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."'");
							$answer = $answer->result_array();						

							if($value['QuestionsType'] == 'Short Answer'){ 
								foreach ($answer as $ka => $va) {
									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(strip_tags($va['AnswerContent'])));
								}
							}

							$QuestionsLevelModerate[$key]['answer'] = $answer;
						}

						$ids = array_column($QuestionsLevelModerate, 'QtBankID');

						if(!empty($ids)){
							$this->db->where('QtPaperID', $QtPaperID);
							$this->db->update('tbl_question_paper', array('ModerateLevelQuestionsID' => implode(',', $ids)));
						}
					}

					$QuestionsLevelHigh = array();
					if($Record['QuestionsLevelHigh'] > 0)
					{
						if(!empty($Record['HighLevelQuestionsID']))
						{
							//echo "select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['HighLevelQuestionsID'],',').")";
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['HighLevelQuestionsID'],',').")");
						}
						else
						{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.SubjectID = '".$Record['SubjectID']."' and QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%'  and QB.QuestionsLevel = 'High' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2 ORDER BY rand() limit ".$Record['QuestionsLevelHigh']);
						}
						$QuestionsLevelHigh = $course->result_array(); 
						foreach ($QuestionsLevelHigh as $key => $value) {
							$QuestionsLevelHigh[$key]['QuestionContent'] = htmlspecialchars(trim(strip_tags($value['QuestionContent'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$QuestionsLevelHigh[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."'");
							$answer = $answer->result_array();						

							if($value['QuestionsType'] == 'Short Answer')
							{ 
								foreach ($answer as $ka => $va) 
								{
									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(strip_tags($va['AnswerContent'])));
								}
							}

							$QuestionsLevelHigh[$key]['answer'] = $answer;
						}

						$ids = array_column($QuestionsLevelHigh, 'QtBankID');

						if(!empty($ids)){
							$this->db->where('QtPaperID', $QtPaperID);
							$this->db->update('tbl_question_paper', array('HighLevelQuestionsID' => implode(',', $ids)));
						}
					}

					$QA_data = array_merge($QuestionsLevelEasy,$QuestionsLevelModerate,$QuestionsLevelHigh);

					$ids = array_column($QA_data, 'QtBankID');

					if(!empty($ids)){
						$this->db->where('QtPaperID', $QtPaperID);
						$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $ids)));
					}
				//}

				$Record['question_answer'] = $QA_data;
				$Record['TotalQuestions'] = count($Record['question_answer']);
				$Record['Total_marks'] = array_sum(array_column($Record['question_answer'], 'QuestionsMarks'));
			}
			$Return['Data'] = $Record;
			return $Return;
		}
		return FALSE;		
	}


	function revokeLoan($EntityID,$LoanID){
		$this->db->where("StudentID", $EntityID);
		$this->db->where("LoanID", $LoanID);
		$this->db->delete("tbl_apply_for_loan");
		return TRUE;
	}



	function studentByIDForDrop($EntityID,$UserID="",$CategoryID="",$BatchID=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('tbl_entity.StatusID, tbl_users.UserGUID, tbl_users.FirstName,tbl_users.LastName,tbl_users.Address,tbl_users.StateName,tbl_users.CityName,tbl_users.Postal,tbl_users.PhoneNumber,tbl_users.LinkedInURL,tbl_users.FacebookURL,tbl_users.Email, tbl_students.GuardianName,tbl_students.GuardianAddress,tbl_students.GuardianPhone, tbl_students.GuardianEmail, tbl_students.FathersName, tbl_students.MothersName, tbl_students.PermanentAddress, tbl_students.FathersPhone, tbl_students.FathersEmail,tbl_students.CourseID,tbl_students.BatchID,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.FeeStatusID,tbl_students.TotalFeeAmount,tbl_students.TotalFeeInstallments,tbl_students.InstallmentAmount, tbl_students.Remarks,IF(tbl_users.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",tbl_users.ProfilePic)) AS ProfilePic');
		$this->db->from("tbl_students");
		$this->db->join("tbl_users","tbl_students.StudentID = tbl_users.UserID");
		$this->db->join("tbl_entity","tbl_users.UserID = tbl_entity.EntityID");
		if(!empty($InstituteID)){
			$this->db->where("InstituteID",$InstituteID);
		}
		if(!empty($CategoryID)){
			$this->db->where("CourseID",$CategoryID);
		}
		if(!empty($BatchID)){
			$this->db->where("BatchID",$BatchID);
		}
		
		$this->db->where("tbl_users.UserID",$UserID);
		$this->db->limit(1);

		$Query =  $this->db->get(); //echo $this->db->last_query(); die;
		//$StudentData = $Query->result_array(); //print_r($StudentData);
		$Records = array();
		if($Query->num_rows() > 0){
			foreach ($Query->result_array() as $record) 
			{				
				if(!empty($record['CourseID']))
				{
					$query = $this->db->query("select CategoryGUID,CategoryName from set_categories where CategoryID = ".$record['CourseID']);
					$CategoryData = $query->result_array();
					$record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
					$record['CourseName'] = $CategoryData[0]['CategoryName'];
				}


				if(!empty($record['BatchID']))
				{
					$sql = "SELECT BatchName, DATE_FORMAT(StartDate, '%d-%M-%Y') as StartDate, DATE_FORMAT(DATE_ADD(StartDate, INTERVAL Duration MONTH), '%d-%M-%Y') as EndDate 
					FROM tbl_batch 
					WHERE BatchID = ".$record['BatchID']."
					LIMIT 1
					";

					$query = $this->db->query($sql);
					$BatchName = $query->result_array();
					$record['BatchName'] = $BatchName[0]['BatchName'];
					$record['StartDate'] = $BatchName[0]['StartDate'];
					$record['EndDate'] = $BatchName[0]['EndDate'];
				}


				$FBID = 0;
				$record['TotalFee'] = 0;                
                $record['NoOfInstallment'] = 0;                                   
                if(!empty($record['FeeID']))
                {
                    $FBID = $record['FeeID'];

                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();
                    $record['TotalFee'] = $FeeData[0]['FullDiscountFee'];
                    $record['NoOfInstallment'] = 1;
                    $record['InstallmentAmount'] = $record['TotalFee'];

                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment, FeeID
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {
                        $record['TotalFee'] = $InstallmentData[0]['TotalFee'];
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                        $record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];

                        $FBID = $InstallmentData[0]['FeeID'];;
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['InstallmentAmount'] = 0;
                }

                $record['IsPaidAnyFee'] = 0;
                $record['PaidEMICount'] = 0;
                $record['RemainingEMI'] = 0;
                $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$UserID. " AND CourseID = '".$CategoryID."' AND BatchID = '".$BatchID."'");
                $SumData = $query->result_array();  
                if(!empty($SumData) && $SumData[0]['FeeCollectionID'] != 0)
                {
                    $record['PaidEMICount'] = $SumData[0]['FeeCollectionID'];
                    //$record['RemainingEMI'] = $record['PaidEMICount'] - count($SumData);                    
                }
                $record['RemainingEMI'] = $record['NoOfInstallment'] - $record['PaidEMICount'];
                    
                

                $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$UserID. " AND CourseID = '".$CategoryID."' AND BatchID = '".$BatchID."'");
                $SumData = $query->result_array(); 
                if(!empty($SumData) && $SumData[0]['PaidAmount'] > 0){
                    $record['PaidAmount'] = $SumData[0]['PaidAmount'];
                }else{
                    $record['PaidAmount'] = 0;
                } 
                $record['RemainingAmount'] = (int)$record['TotalFee'] - (int)$record['PaidAmount'];


                
                				
				if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
                    $record['Fee Status'] = "Unpaid";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Fee Status'] = "Paid";
                }else{
                    $record['Fee Status'] = "Unpaid";
                } 
							

				
				$record['FBList'] = array();
				if($FBID > 0)
				{
					$this->load->model('Fee_model');
					$record['FBList'] = $this->Fee_model->getFeeBreakupDetails($FBID, $InstituteID);
					$record['SystemRefundAmount'] = 0;
					if(isset($record['FBList']) && !empty($record['FBList']))
					{
						$rt = 0; $fbarr = array();
						foreach($record['FBList'] as $arr)
						{
							$arr['RefundAmount'] = "";
							if(isset($arr['RefundPercent']) && !empty($arr['RefundPercent']) && isset($arr['NetAmount']) && !empty($arr['NetAmount']))
							{
								$rm = round( ($arr['NetAmount'] * $arr['RefundPercent']) / 100 );
								$rt = $rt + $rm;
								$arr['RefundAmount'] = $rm;
							}

							$fbarr[] = $arr;
						}

						$record['SystemRefundAmount'] = round($rt);
						$record['FBList'] = array();
						$record['FBList'] = $fbarr;
					}
				}	

				//print_r($record);
				array_push($Records, $record);
			}
			//print_r($Records);
			return $Records;
		}else{
			return $Records;
		}
	}


	function dropStudent($EntityID, $StudentID, $Input = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $BreakTypeID = $Input['breakup_head_id'];

        $this->db->trans_start();

        
        $sql = "SELECT s.StudentID
        FROM tbl_students s
        INNER JOIN tbl_entity e ON s.StudentID = e.EntityID AND e.InstituteID = $InstituteID AND e.StatusID = 2
        WHERE s.StudentID = $StudentID AND e.StatusID = 2
        LIMIT 1";
        $Query = $this->db->query($sql);                       
        if($Query->num_rows() <= 0)
        {
           return 'NotExist'; die;           
        }
             
        
        if($Input['do_payment_type'] == "cheque")
		{
			$do1 = $Input['do_cheque_number'];
			$do2 = $Input['do_cheque_bank'];
			$do3 = date("Y-m-d", strtotime($Input['do_cheque_date']));
		}
		elseif($Input['do_payment_type'] == "online")
		{			
			$do1 = $Input['do_online_transactionid'];			
			$do2 = $Input['do_online_details'];		
			$do3 = "";
		}

        
        $UpdateData = array(                           
        "DroppingDate" => date("Y-m-d", strtotime(@$Input['do_date'])),
        "DroppingReason" => @$Input['do_reason'],
        "AmountBalance" => @$Input['do_amount_refund'],
        "PaymentType" => @$Input['do_payment_type'],        
        "ChequeNumberOrTransaction" => $do1,
        "BankOrOther" => $do2,
        "ChequeDate" => $do3,
        "DropoutBy" => $this->EntityID,
    	"SystemCalculatedRefund" => $Input['do_system_amount_refund']);

        $this->db->where('StudentID', $StudentID);
        $this->db->update('tbl_students', $UpdateData);        
        $ID = $this->db->affected_rows();         

        if($ID > 0)
        {
        	$UpdateData = array("StatusID" => 28);
	        $this->db->where('EntityID', $StudentID);
	        $this->db->where('InstituteID', $InstituteID);
	        $this->db->update('tbl_entity', $UpdateData);
        }


        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }

        return $ID;     
    }



    function editStudentApp($EntityID,$Input=array()) 
    {
		$UserID = $EntityID;

		$this->db->trans_start();
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		if(!empty($Input['PhoneNumber']))
		{
			$this->db->select('PhoneNumber');
			$this->db->from('tbl_users');
			$this->db->where('PhoneNumber',$Input['PhoneNumber']);
			$this->db->where('UserID != '.$UserID);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				return 'PhoneNumber Exist';
				die;
			}
		}


		$this->db->select('Email');
		$this->db->from('tbl_users');
		$this->db->where('UserID', $UserID);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$UserData = $query->result_array()[0];
		}


		if(!empty($Input['Email'])){
			if($UserData['Email']!=$Input['Email']){
				$Input['EmailForChange'] = $Input['Email'];			
				/* Genrate a Token for Email verification and save to tokens table. */
				$this->load->model('Recovery_model');
				$this->load->model('Media_model');

				$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
		        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
		
		        if(!empty($Record['InstituteProfilePic'])){
			    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
			    }else{
			    	$MediaURL= "";
			    }
			    
				$Token = $this->Recovery_model->generateToken($UserID, 2);	

				$content = $this->load->view('emailer/signup', 
						array("Name" => $Input['FirstName'],'Token' => $Token,"InstituteProfilePic"=>@$MediaURL,
							'Signature' => "Thanks, <br>".$InstituteData['FirstName']) , TRUE);

				sendMail(array(
					"From_name"  => $InstituteData['FirstName'],
					'emailTo' => $Input['EmailForChange'],
					'emailSubject' => "Verify Your Email",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));
				unset($UpdateArray['Email']);
			}
		}


		/*if(!empty($Input['StateID'])){
			$StateName = $this->Common_model->getStatesNameById($Input['StateID']);
		}else{
			$StateName = "";
		}*/

		
		

		$UpdateData = array_filter(array(
			"FirstName" 			=> 	@ucfirst(($Input['FirstName'])),		
			"LastName" 				=> 	@ucfirst(($Input['LastName'])),
			"Email" 				=>	@strtolower($Input['Email']),
			"Address" 				=>	@$Input['Address'],
			"Postal" 				=>	@$Input['Postal'],	
			/*"StateName" 			=>	$StateName,
			"CityName" 			=>	@$Input['CityName'],*/
			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"LinkedInURL" 			=>	@strtolower($Input['LinkedInURL']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL'])
		));

		if(isset($Input['MediaName']) && !empty($Input['MediaName']))
		{
			$UpdateData["ProfilePic"] = $Input['MediaName'];
		}

		$this->db->where('UserID',$UserID);
		$this->db->update('tbl_users', $UpdateData);

		

		if(empty($Input['FathersPhone']) || $Input['FathersPhone'] == 0){
			$Input['FathersPhone'] = NULL;
		}

		if(empty($Input['GuardianPhone']) || $Input['GuardianPhone'] == 0){
			$Input['GuardianPhone'] = NULL;
		}

		$UpdateDatas = array(	
									
			"Email" =>	@strtolower($Input['Email']),
			"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),
			"GuardianName" 	=>	@$Input['GuardianName'],
			"GuardianAddress"  =>	@$Input['GuardianAddress'],
			"GuardianPhone"  =>	@$Input['GuardianPhone'],
			"GuardianEmail"  =>	@$Input['GuardianEmail'],
			"FathersName"  =>	@$Input['FathersName'],
			"MothersName"  =>	@$Input['MothersName'],
			"PermanentAddress"  =>	@$Input['PermanentAddress'],
			"FathersPhone"  =>	@$Input['FathersPhone'],
			"FathersEmail"  =>	@$Input['FathersEmail'],
			"Remarks" =>	@$Input['Remarks']);

		//print_r($UpdateDatas); die;

		$this->db->where('StudentID',$UserID);
		$this->db->update('tbl_students', $UpdateDatas);


		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return $EntityID;
	}


	function getUnAssignBatchStudent($EntityID)
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);				
		
		$sql = "SELECT s.StudentID, u.FirstName, u.LastName, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile
        FROM tbl_students s
        INNER JOIN tbl_entity e ON s.StudentID = e.EntityID AND e.InstituteID = $InstituteID AND e.StatusID = 2
        INNER JOIN tbl_users u ON s.StudentID = u.UserID AND u.UserTypeID = 7
        WHERE e.StatusID = 2 AND (s.CourseID IS NULL OR s.CourseID = '' OR s.CourseID <= 0 OR s.BatchID <= 0 OR s.BatchID IS NULL OR s.BatchID = '' ) AND s.ConvertOrderStatus IN ('Success', 'success') AND s.ConvertTrackingID IS NOT NULL AND s.ConvertDate IS NOT NULL
        ORDER BY u.FirstName, u.LastName, s.StudentID DESC
        ";

		$Query = $this->db->query($sql);  
		
		if($Query->num_rows() > 0)
		{
			return $Query->result_array();
		}	

		return array();
	}


	function BulkAssignBatchSave($EntityID, $CourseID, $BatchID, $Input = array())
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $StudentIDArr = $Input['BASChkBox'];
        $StudentIDs = implode(",", $StudentIDArr);

        $this->db->trans_start();
        
        $sql = "UPDATE tbl_students
        SET CourseID = '$CourseID', BatchID = '$BatchID'       
        WHERE StudentID IN($StudentIDs) AND (CourseID IS NULL OR CourseID = '' OR CourseID <= 0 OR BatchID <= 0 OR BatchID IS NULL OR BatchID = '' ) AND ConvertOrderStatus IN ('Success', 'success') AND ConvertTrackingID IS NOT NULL AND ConvertDate IS NOT NULL ";
        
        $Query = $this->db->query($sql);          
       
        $ID = $this->db->affected_rows();

        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            return FALSE;
        }

        return $ID;     
    }



    function getAppliedScholarshipTestsAPI($Input = array())
	{
		$UserID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$result = array();

		$QuestionsGroup = 5;
		$STKey = $Input['STKey'];

		$sql = "SELECT qp.QtPaperTitle, qp.QuestionsGroup, qp.QtPaperGUID, qp.QtPaperID, qp.NegativeMarks, qp.PassingMarks, qp.TotalQuestions, qp.EasyLevelQuestionsID, qp.HighLevelQuestionsID, qp.ModerateLevelQuestionsID, qp.QuestionsID, u.FirstName, u.LastName, DATE_FORMAT(qp.ScheduleDate,'%d-%m-%Y') as ScheduleDate, qp.Duration, DATE_FORMAT(qp.TestStartTime,'%h:%i %p') as TestStartTime, DATE_FORMAT(qp.TestActivatedFrom,'%h:%i %p') as TestActivatedFrom, DATE_FORMAT(qp.TestActivatedTo,'%h:%i %p') as TestActivatedTo, qp.ScheduleType, a.*, rst.ResultGUID, qp.EntityID
		FROM tbl_question_paper as qp
		INNER JOIN tbl_users u ON u.UserID = qp.EntityID AND u.UserTypeID = 10	
		INNER JOIN tbl_scholarship_applicants a ON a.QtPaperID = qp.QtPaperID AND a.StudentID = '".$UserID."' AND a.Status IN('success','Success') AND a.OrderID > 0 AND a.KeyName = '".$STKey."'
		LEFT JOIN tbl_test_results rst ON rst.QtPaperID = a.QtPaperID AND a.StudentID = rst.StudentID AND rst.StudentID = '".$UserID."'
		WHERE qp.QuestionsGroup = ".$QuestionsGroup." AND a.KeyName = '".$STKey."'
		LIMIT 1
		";

		$query = $this->db->query($sql);		

		$data = $query->result_array();

		if(count($data) > 0)
		{
			$this->load->model('Questionpaper_model');

			foreach ($data as $key => $value) 
			{
				$EntityIDForInst = $value['EntityID'];
				$QtPaperIDForInst = $value['QtPaperID'];

				$QtBankIDsArr = array();
				$QtBankIDsArr[] = ($value['EasyLevelQuestionsID']) ? $value['EasyLevelQuestionsID'] : 0;
				$QtBankIDsArr[] = ($value['HighLevelQuestionsID']) ? $value['HighLevelQuestionsID']: 0;
				$QtBankIDsArr[] = ($value['ModerateLevelQuestionsID']) ? $value['ModerateLevelQuestionsID']: 0;

				$QtBankIDsArr =  array_unique($QtBankIDsArr);

				$QtBankIDs = implode(",", $QtBankIDsArr);
				if(isset($value['QuestionsID']) && !empty($value['QuestionsID']))
				{
					$QtBankIDs = $QtBankIDs.",".$value['QuestionsID'];
				}					

				if($QtBankIDs)
				{
					$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

					if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows())
					{
						$QuestionsMarks = $QuestionsMarks->result_array();

						$result[$key]['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
					}
					else
					{
						$result[$key]['TotalMarks'] = 0;
					}
				}
				else
				{
					$result[$key]['TotalMarks'] = 0;
				}

				//Extra Condition Code For API------------------------------------------
				if(!isset($result[$key]['TotalMarks']) || empty($result[$key]['TotalMarks']))
				{
					$result[$key]['TotalMarks'] = 0;
				}

				
				$result[$key]['QtAssignGUID'] = $value['QtAssignGUID'];
				$result[$key]['QtPaperTitle'] = $value['QtPaperTitle'];
				$result[$key]['NegativeMarks'] = $value['NegativeMarks'];
				$result[$key]['PassingMarks'] = $value['PassingMarks'];
				$result[$key]['QuestionsGroup'] = $value['QuestionsGroup'];
				$result[$key]['TotalQuestions'] = $value['TotalQuestions'];
				$result[$key]['QtPaperGUID'] = $value['QtPaperGUID'];					
				
				$result[$key]['ScheduleDate'] = $value['ScheduleDate'];								
				$result[$key]['InstituteName'] = $value['FirstName']." ".$value['LastName'];
				$result[$key]['QtPaperID'] = $value['QtPaperID'];
				$result[$key]['TestDuration'] = $value['Duration'];
				

				$result[$key]['ValidUpto'] = $value['ValidUpto'];
				$result[$key]['KeyNames'] = $value['KeyName'];
				$result[$key]['PaymentDate'] = date("d-M-Y H:i", strtotime($value['PaymentDate']));
				$result[$key]['ResultGUID'] = $value['ResultGUID'];
				if(isset($value['ResultGUID']) && !empty($value['ResultGUID']))
				{
					$result[$key]['TestGiven'] = "1";
				}
				else
				{
					$result[$key]['TestGiven'] = "0";
				}
				
				$result[$key]['ScheduleType'] = $value['ScheduleType'];
				if($value['ScheduleType'] == 2)
				{
					$result[$key]['TestStartTime'] = $value['TestStartTime'];
					$result[$key]['TestActivatedFrom'] = $result[$key]['TestActivatedTo'] = "";				
				}
				else
				{
					$result[$key]['TestActivatedFrom'] = $value['TestActivatedFrom'];
					$result[$key]['TestActivatedTo'] = $value['TestActivatedTo'];
					$result[$key]['TestStartTime'] = "";
				}

				$result[$key]['viewPaper'] = $this->Questionpaper_model->viewPaper($EntityIDForInst, $QtPaperIDForInst);
			}
		}
		

		return $result;
	}

}