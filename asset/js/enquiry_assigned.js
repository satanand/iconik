app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.Users = [];

    
    $( "#filterFromDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterToDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#filterToDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterFromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });



    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentParentCategoryGUID = ParentCategoryGUID;

    $scope.CategoryTypeID = $scope.getUrlParameter('CategoryTypeID');

    $scope.APIURL = API_URL + "upload/image";
    /*----------------*/

    $scope.getFilterData = function ()

    {
        /*$scope.getSecondLevelPermission();
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();

        $http.post(API_URL+'admin/category/getFilterData', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ 

             $scope.filterData =  response.Data;

             $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 300);          

         }

        });*/

    }





    /*list*/

    $scope.applyFilter = function ()

    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        $scope.getList();

    }





    /*list append*/

    $scope.getList = function ()
    {
        if ($scope.data.listLoading || $scope.data.noRecords) return;        

        $scope.data.listLoading = true;

        if($scope.data.Users.length <= 0)
        $scope.data.Users = [];

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
     

        $http.post(API_URL+'enquiry/getAssignedEnquiryCount', data, contentType).then(function(response) 
        {
                var response = response.data;

                if(response.ResponseCode==200 && response.Data.Content)
                { 
                    $scope.data.dataList = response.Data.Content;

                    if($scope.data.Users <= 0)
                    $scope.data.Users = response.Data.Users;
                }
                else
                {

                    $scope.data.noRecords = true;
                }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }


  


    /*load Enquiry By Status Popup*/

    $scope.loadEnquiryByStatusPopup = function (SourceID, StatusID)
    {
        $scope.addDataLoading = true;

        $scope.data.listLoading = true;

        $scope.SourceID = SourceID;

        $scope.StatusID = StatusID;

        $scope.data.dataListDetails = [];

        $scope.templateURLAdd = PATH_TEMPLATE+'enquiry/enquiry_details_by_status_list.htm?'+Math.random();

        var data = 'SessionKey='+SessionKey+'&SourceID='+SourceID+'&StatusID='+StatusID;

        $http.post(API_URL+'enquiry/loadEnquiryByStatus', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            { /* success case */                

                for (var i in response.Data.Records) 
                {

                    $scope.data.dataListDetails.push(response.Data.Records[i]);

                }

                $('#add_model').modal({show:true});
                     
                $scope.addDataLoading = false;

                $scope.data.listLoading = false;
            }
            
            $scope.addDataLoading = false;         

       });

    }


}); 


function search_records()
{
    angular.element(document.getElementById('content-body')).scope().getList();
}


function clear_search_records()
{    
    $("#filterStatus, #filterSource, #filterAssignedToUser, #filterFromDate, #filterToDate").val("");    

    search_records();
}


/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

            type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }
}

/*function ParentcategoryCheck(CatType){
    if(CatType == 'Stream'){
        $("#SelectParentCategory").hide();
        $("#category-name-label").text("Stream Name");
        $("#select-label-parent-category").text("");
        $("#course_duration").hide();
    }else if(CatType == 'Course'){
        $("#SelectParentCategory").show();
        $("#category-name-label").text("Course Name");
        $("#select-label-parent-category").text("Select Stream");
        $("#course_duration").show();
        $("#course_duration").val("");
    }else if(CatType == 'Subject'){
        $("#category-name-label").text("Subject Name");
        $("#SelectParentCategory").show();
        $("#select-label-parent-category").text("Select Course");
        $("#course_duration").hide();
    }else{
       $("#category-name-label").text("Name");
       $("#SelectParentCategory").hide();
       $("#select-label-parent-category").text("Select Parent"); 
       $("#course_duration").hide();
    }
}*/
/* sortable - ends */