<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobprovider_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to get Jobs
	*/
	function getJobs($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10)
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(j.StartDate) >= '$FilterStartDate' AND DATE(j.EndDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (j.JobTitle LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR 
			j.EmployementType LIKE '%$Keyword%' OR 
			j.Experience LIKE '%$Keyword%' OR 
			j.Qualification LIKE '%$Keyword%' OR 
			j.Salary LIKE '%$Keyword%' OR
			c.name LIKE '%$Keyword%' OR
			s.StateName LIKE '%$Keyword%'

			) ";
		}

		/*if(isset($Where['FilterEmployementType']) && !empty($Where['FilterEmployementType']))
		{
			$FilterEmployementType = trim($Where['FilterEmployementType']);
			
			$append .= " AND (j.EmployementType = '$Keyword') ";
		}*/


		$sql = "SELECT j.*, DATE_FORMAT(j.StartDate, '%d-%m-%Y') as StartDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, s.StateName, c.name as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, COUNT(ja.ApplyID) as Responses
		FROM tbl_post_job j
		INNER JOIN set_location_states s ON j.JobLocationState = s.State_id
		INNER JOIN cities c ON j.JobLocationCity = c.id
		LEFT JOIN tbl_post_job_applications ja ON ja.PostJobID = j.PostJobID
		WHERE j.StatusID != 3 AND j.InstituteID = '$InstituteID' $append
		GROUP BY j.PostJobID
		ORDER BY j.PostJobID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function add($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		$PartTimeJobGUID = get_guid();

		$InsertData = array_filter(array(
			"PartTimeJobGUID"=>$PartTimeJobGUID,
			"EntityID"=>$EntityID,
			"Position"=>$Input['Position'],
			"Description"=>$Input['Description'],
			"EndDate"=>date("Y-m-d",strtotime($Input['EndDate'])),
			"PartTimeJobState"=>$Input['PartTimeJobState'],
			"PartTimeJobCity"=>$Input['PartTimeJobCity'],
			"StatusID"=>$Input['JobStatus'],
			"Salary"=>$Input['Salary'],
			"EntryDate"=>date("Y-m-d H:i:s"),
			"ModifyDate"=>date("Y-m-d H:i:s")
		));
		
		$PostJobID = $this->db->insert('tbl_part_time_job', $InsertData);

		$part_time_job_id = $this->db->insert_id();
		if($part_time_job_id > 0)
		{
			$InsertData = array(
			"CronFor"=>"part_time_job",
			"TargetIds"=>$part_time_job_id,
			"SentType"=>'sms',
			"InsertDate"=>date("Y-m-d H:i:s"),
			"UpdateDate"=>date("Y-m-d H:i:s")
			);
		
			$cronJobsID = $this->db->insert('cronjobs', $InsertData);
		}


		$StateName = $this->Common_model->getStatesNameById($Input['PartTimeJobState']);

		$CityName = $this->Common_model->getCityNameById($Input['PartTimeJobCity']);

		$ProviderInfo = $this->Common_model->getInstituteDetailByID($EntityID,"FirstName,LastName,Email,PhoneNumberForChange,PhoneNumber");

		

		//Sending email to Job Provider---------------------------------------------------
		$EmailText = "<p>Hi, ".$ProviderInfo['FirstName']."</p>";

		$EmailText .= "<p>Thank you for registering & posting manpower requirements with Iconik! - we hope your experience was seamless and hassle free.Below are the details of the job you have posted.</p>";

		$EmailText .= "<p>Position: <b>".ucfirst($Input['Position'])."</b></p>";

		$EmailText .= "<p>Description: <b>".ucfirst($Input['Description'])."</b></p>";

		$EmailText .= "<p>Entry Date: <b>".ucfirst($Input['Description'])."</b></p>";

		$EmailText .= "<p>End Date: <b>".ucfirst($Input['Description'])."</b></p>";

		$EmailText .= "<p>Salary: <b>".ucfirst($Input['Description'])."</b></p>";

		$EmailText .= "<p>Job Location State: <b>".ucfirst($StateName)."</b></p>";

		$EmailText .= "<p>Job Location City: <b>".ucfirst($CityName)."</b></p>";

		$EmailText .= "<p>We hope you get an appropriate manpower. Write to contact@iconik.in if you have any questions about the service.</p>";

		$EmailTextFooter = "<p>Thank you, </p>";
		$EmailTextFooter .= "<p>Support Team</p>";
		$EmailTextFooter .= SIGNATURE;
		
		$content = $this->load->view('emailer/job_provider',array("EmailText"=>$EmailText,"Signature"=>$EmailTextFooter),TRUE);

		sendMail(array(
		'emailTo'       => $ProviderInfo['Email'],         
		'emailSubject'  => "Thank you for posting your manpower requirements with Iconik.",
		'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
		));

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function edit($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$UpdateData = array_filter(array(
			"Position"=>$Input['Position'],
			"Description"=>$Input['Description'],
			"EndDate"=>date("Y-m-d",strtotime($Input['EndDate'])),
			"PartTimeJobState"=>$Input['PartTimeJobState'],
			"PartTimeJobCity"=>$Input['PartTimeJobCity'],
			"StatusID"=>$Input['JobStatus'],
			"Salary"=>$Input['Salary'],
			"ModifyDate"=>date("Y-m-d H:i:s")
		));
		
		$this->db->where('PartTimeJobGUID', $Input['PartTimeJobGUID']);
		$this->db->update('tbl_part_time_job', $UpdateData);

		$this->db->trans_complete();
 
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function delete($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->where('PartTimeJobGUID', $Input['PartTimeJobGUID']);
		$this->db->where('EntityID', $EntityID);
		$this->db->update('tbl_part_time_job', array("StatusID" => 3));
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}

 
	
	/*-------------------------------------------------------------
	Section For Faculty
	-------------------------------------------------------------*/
	function getPartTimeJobs($EntityID="", $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(j.EntryDate) >= '$FilterStartDate' AND DATE(j.EndDate) <= '$FilterEndDate') ";
		}else if(isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate'])){
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));
			$append .= " AND  DATE(j.EndDate) = '$FilterEndDate' ";
			
		}else if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate'])){
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$append .= " AND  DATE(j.EntryDate) = '$FilterStartDate' ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.Position LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR 
			j.Salary LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			c.name LIKE '%$Keyword%' OR
			s.StateName LIKE '%$Keyword%'
			) ";

			if(strtolower($Keyword) == 'open'){
				$append .= " OR j.StatusID = 2 ";	
			}

			if(strtolower($Keyword) == 'closed' || strtolower($Keyword) == 'close'){
				$append .= " OR j.StatusID = 4 ";	
			}
		}


		if(!empty($EntityID) && $this->UserTypeID == 82)
		{
			$append .= " AND j.EntityID = '$EntityID' ";
		}
		elseif(!empty($EntityID) && ($this->UserTypeID == 88))
		{
			$append .= " AND j.PartTimeJobID NOT IN(SELECT ja.PartTimeJobID FROM tbl_parttime_job_applications ja WHERE ja.StudentID = '$EntityID')";
		}

		if(!empty($Where['ForStudents']) && $Where['ForStudents'] == 1){
			$append .= " AND (DATE(j.EntryDate) <= CURDATE() AND DATE(j.EndDate) >= CURDATE())"; 
		}

		//echo $append; 


		$sql = "SELECT j.*,j.Position as Title, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, j.PartTimeJobState as StateName, j.PartTimeJobCity as CityName, DATE_FORMAT(j.ModifyDate, '%d-%m-%Y') as ModifyDate, u.FirstName as ProviderName,u.Email as ProviderEmail,u.PhoneNumber as ProviderPhoneNumber, '' as ApplyID
		FROM tbl_part_time_job j
		INNER JOIN tbl_users u ON u.UserID = j.EntityID
		/*INNER JOIN set_location_states s ON j.PartTimeJobState = s.State_id
		INNER JOIN cities c ON j.PartTimeJobCity = c.id	*/			
		WHERE j.StatusID != 3 $append ORDER BY j.PartTimeJobID DESC LIMIT $PageNo, $PageSize";

		$Query = $this->db->query($sql);  $ShortList = 0; //echo $this->db->last_query(); die;
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();
			$testarr = $Query->result_array();
			foreach($testarr as $Where)
			{
				if(!empty($Where['ShortListedByUserID']) && strpos(",", $Where['ShortListedByUserID']) !== false){
					$ShortListedByUserID = explode(",", $Where['ShortListedByUserID']);
					if(in_array($EntityID, $ShortListedByUserID)){
						$Where['ShortList'] = 1;
						$ShortList++;
					}else{
						$Where['ShortList'] = 0;
					}
				}else if(!empty($Where['ShortListedByUserID']) && $Where['ShortListedByUserID'] == $EntityID){
					$Where['ShortList'] = 1;
					$ShortList++;
				}else{
					$Where['ShortList'] = 0;
				}

				if(!empty($EntityID)){
					$this->db->select("ApplyID");
					$this->db->from("tbl_parttime_job_applications");
					$this->db->where("StudentID",$EntityID);
					$this->db->where("PartTimeJobID",$Where['PartTimeJobID']);
					$Query = $this->db->get();	
					if($Query->num_rows() > 0){
						$Where['Applied'] = 1;
					}else{
						$Where['Applied'] = 0;
					}
				}else{
					$Where['Applied'] = 0;
				}

				if(!empty($EntityID)){
					$this->db->select("ApplyID");
					$this->db->from("tbl_parttime_job_applications");
					$this->db->where("PartTimeJobID",$Where['PartTimeJobID']);
					$Query = $this->db->get();	
					if($Query->num_rows() > 0){
						$Where['Applicants_count'] = $Query->num_rows();
					}else{
						$Where['Applicants_count'] = 0;
					}
				}else{
					$Where['Applicants_count'] = 0;
				}

				$arr[] = $Where;	
				//echo $i;			
			}
			//print_r($arr);
			$Return['Data']['ShortListCount'] = $ShortList;
			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function applyForJob($EntityID, $Input)
	{
		// $AppID = 0;

		$PartTimeJobID = $Input['PartTimeJobID'];

		// if(!empty($Input['MediaGUID']))
		// {
			$this->db->select('*');
			$this->db->from('tbl_part_time_job');
			$this->db->where('PartTimeJobID',$PartTimeJobID);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$JobData = $que->result_array();
				$JobData = $JobData[0];
				$ProviderData = $this->Common_model->getInstituteDetailByID($JobData['EntityID'],"Email,FirstName,LastName,PhoneNumberForChange");
			}
		//}

		if($Input['Type'] == 1){

			$InsertData = array(
			"PartTimeJobID"=>$PartTimeJobID,
			"StudentID"=>$EntityID,
			"ApplyDate"=>date("Y-m-d H:i:s")
			);
			
			$this->db->insert('tbl_parttime_job_applications', $InsertData);
			
			$AppID = $this->db->insert_id();

			if($AppID > 0){
				$StudentData = $this->Common_model->getInstituteDetailByID($EntityID,"Email,FirstName,LastName,PhoneNumberForChange,CityName,StateName,Address");
			
				//Sending email to Student---------------------------------------------------
				$EmailText = "<p>Dear ".$StudentData['FirstName']." ".$StudentData['LastName'].",</p>";

				$EmailText .= "<p>Your application for the position of  <b>".ucfirst($JobData['Position'])."</b> has been sent to <b>".ucfirst($ProviderData['FirstName'])."</b>.</p>";

				$EmailText .= "<p>The company will contact you directly once the application is received & shortlisted. Write to contact@iconik.in if you have any questions about the service.</p>";	

				$EmailTextFooter = "";				
				$content = $this->load->view('emailer/job_provider',array("EmailText"=>$EmailText),TRUE);

				sendMail(array(
				'emailTo'       => $StudentData['Email'],         
				'emailSubject'  => "Iconik: Your application for the position of ".ucfirst($JobData['Position'])." has been sent to the employer.",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));




				//Sending email to provider---------------------------------------------------
				$EmailText = "<p>Hi ".$ProviderData['FirstName'].",</p>";

				$EmailText .= "<p>Iconik: You have received an application for the position of <b>".ucfirst($JobData['Position'])."</b> from the student with details as follows.</p>";


				$EmailText .= "<p>Student Name: <b>".ucfirst($StudentData['FirstName']." ".$StudentData['LastName'])."</b></p>";

				if(!empty($StudentData['StateName']) && !empty($StudentData['CityName']) && !empty($StudentData['Address'])){
					$EmailText .= "<p>Student Address: <b>".ucfirst($Input['Address']).", ".ucfirst($Input['CityName']).", ".ucfirst($Input['StateName'])."</b></p>";
				}

				//$EmailText .= "<p>Student Address: <b>".ucfirst($Input['Description'])."</b></p>";

				$EmailText .= "<p>Student Email ID: <b>".ucfirst($StudentData['Email'])."</b></p>";

				if(!empty($StudentData['PhoneNumber'])){
					$EmailText .= "<p>Student Contact No.: <b>".ucfirst($StudentData['PhoneNumber'])."</b></p>";
				}else{
					$EmailText .= "<p>Student Contact No.: <b>".ucfirst($StudentData['PhoneNumberForChange'])."</b></p>";
				}
				

				$EmailText .= "<p>Application Date: <b>".date("d-m-Y h:i:s")."</b></p>";
				$EmailText .= "<p>You can contact the student directly. Write to contact@iconik.in if you have any questions about the service. Happy Hiring!</p>";
				$EmailTextFooter = "";				
				$content = $this->load->view('emailer/job_provider',array("EmailText"=>$EmailText),TRUE);
				sendMail(array(
				'emailTo'       => $ProviderData['Email'],         
				'emailSubject'  => "Iconik: You have received an application for the position of ".ucfirst($JobData['Position']).".",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));
			}

			return $AppID;

		}else{
			
			$this->db->where("PartTimeJobID",$PartTimeJobID);
			$this->db->where("StudentID",$EntityID);
			$this->db->delete('tbl_parttime_job_applications');
			$this->db->limit(1);
			//echo $this->db->last_query(); die;
			return TRUE;

		}
		


		// $InsertData = array("ApplyID"=>$AppID,
		// "LogDate"=>date("Y-m-d H:i:s"),
		// "LogStatus"=>10);		
		// $this->db->insert('tbl_post_job_applications_log', $InsertData);

		// $this->db->trans_complete();

		// return $AppID;
	}


	function getAppliedJobs($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(ja.ApplyDate) >= '$FilterStartDate' AND DATE(ja.ApplyDate) <= '$FilterEndDate') ";

		}else if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && empty($Where['FilterEndDate']))
		{

			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$append .= " AND DATE(ja.ApplyDate) = '$FilterStartDate' ";
		}else if(isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']) && empty($Where['FilterStartDate']))
		{

			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));
			$append .= " AND DATE(ja.ApplyDate) = '$FilterEndDate' ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.Position LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR  
			j.Salary LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			u.Email LIKE '%$Keyword%' OR
			u.PhoneNumber LIKE '%$Keyword%' OR
			j.PartTimeJobCity LIKE '%$Keyword%' OR
			j.PartTimeJobState LIKE '%$Keyword%'
			) ";
		}


		$sql = "SELECT j.*,  DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, j.PartTimeJobState as StateName, j.PartTimeJobCity as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, u.FirstName as ProviderName,u.Email as ProviderEmail,u.PhoneNumber as ProviderPhoneNumber, ja.ApplyID, DATE_FORMAT(ja.ApplyDate, '%d-%m-%Y') as ApplyDate
		FROM  tbl_part_time_job j
		INNER JOIN tbl_users u ON u.UserID = j.EntityID			
		INNER JOIN tbl_parttime_job_applications ja ON ja.PartTimeJobID = j.PartTimeJobID AND ja.StudentID = '$EntityID' $append
		ORDER BY ja.ApplyID DESC
		LIMIT $PageNo, $PageSize";

		$Query = $this->db->query($sql);  //echo $this->db->last_query(); die;
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();		
			foreach($Query->result_array() as $Where)
			{
				$arr[] = $Where;
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getShortListedJobs($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			//$append .= " AND (DATE(ja.ApplyDate) >= '$FilterStartDate' AND DATE(ja.ApplyDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.Position LIKE '%$Keyword%' OR 
			j.Description LIKE '%$Keyword%' OR  
			j.Salary LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			j.PartTimeJobState LIKE '%$Keyword%' OR
			j.PartTimeJobCity LIKE '%$Keyword%'
			) ";
		}


		$sql = "SELECT j.*,  DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, j.PartTimeJobState as StateName, j.PartTimeJobCity as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, u.FirstName as ProviderName,u.Email as ProviderEmail,u.PhoneNumber as ProviderPhoneNumber
		FROM  tbl_part_time_job j
		INNER JOIN tbl_users u ON u.UserID = j.EntityID			
		Where FIND_IN_SET('".$EntityID."',ShortListedByUserID) $append
		ORDER BY j.PartTimeJobID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);  $ShortList = 0;
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$arr[] = $Where;				
			}
			$Return['Data']['ShortListCount'] = $ShortList;
			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getApplicants($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		//print_r($Where);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		$UserTypeID = $this->Common_model->getUserTypeByEntityID($this->EntityID);
		
		$FilterStatus = $Where['FilterStatus'];

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(ja.ApplyDate) >= '$FilterStartDate' AND DATE(j.EndDate) <= '$FilterEndDate') ";
		}else if(isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate'])){
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));
			$append .= " AND  DATE(j.EndDate) = '$FilterEndDate' ";
			
		}else if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate'])){
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$append .= " AND  DATE(ja.ApplyDate) = '$FilterStartDate' ";
		}

		if(isset($Where['FilterKeyword']) && !empty($Where['FilterKeyword']))
		{
			$Keyword = trim($Where['FilterKeyword']);

			$append .= " AND (j.Position LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			u.LastName LIKE '%$Keyword%' OR
			u.Email LIKE '%$Keyword%' OR
			u.PhoneNumber LIKE '%$Keyword%' OR
			u.PhoneNumberForChange LIKE '%$Keyword%' OR
			j.PartTimeJobState LIKE '%$Keyword%' OR
			j.PartTimeJobCity LIKE '%$Keyword%'
			) ";			
		}

		if(!empty($Where['PartTimeJobID'])){
			$PartTimeJobID = $Where['PartTimeJobID'];
			$append .= " AND j.PartTimeJobID = '$PartTimeJobID' ";
		} 


		if($UserTypeID == 1){
			$sql = "SELECT ja.*, j.*, DATE_FORMAT(j.ModifyDate, '%d-%m-%Y %H:%i') as ModifyDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, j.PartTimeJobState as StateName, j.PartTimeJobCity as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, CONCAT_WS(' ',u.FirstName,u.LastName) as FullName, u.Email, u.PhoneNumber, u.PhoneNumberForChange, DATE_FORMAT(ja.ApplyDate, '%d-%m-%Y %H:%i') as ApplyDate
			FROM tbl_part_time_job j				
			INNER JOIN tbl_parttime_job_applications ja ON ja.PartTimeJobID = j.PartTimeJobID 
			INNER JOIN tbl_users u ON u.UserID = ja.StudentID			
			WHERE 1 = 1  $append
			ORDER BY ja.ApplyID
			LIMIT $PageNo, $PageSize
			";
		}else{
			$sql = "SELECT ja.*, j.*, DATE_FORMAT(j.ModifyDate, '%d-%m-%Y %H:%i') as ModifyDate, DATE_FORMAT(j.EndDate, '%d-%m-%Y') as EndDate, j.PartTimeJobState as StateName, j.PartTimeJobCity as CityName, DATE_FORMAT(j.EntryDate, '%d-%m-%Y') as EntryDate, u.FirstName, u.LastName, u.Email, u.PhoneNumber, u.PhoneNumberForChange, DATE_FORMAT(ja.ApplyDate, '%d-%m-%Y %H:%i') as ApplyDate
			FROM tbl_part_time_job j				
			INNER JOIN tbl_parttime_job_applications ja ON ja.PartTimeJobID = j.PartTimeJobID 
			INNER JOIN tbl_users u ON u.UserID = ja.StudentID			
			WHERE j.EntityID = '$EntityID'  $append
			ORDER BY ja.ApplyID
			LIMIT $PageNo, $PageSize
			";
		}

		

		$Query = $this->db->query($sql); //echo $this->db->last_query(); die;

		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function setShortlistJobs($EntityID, $Input)
	{
		if($Input['Type'] == 0)
		{			
			$sql = $this->db->query("UPDATE  tbl_part_time_job SET  ShortListedByUserID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', ShortListedByUserID, ','), ',".$EntityID.",', ','))	WHERE FIND_IN_SET('".$EntityID."',ShortListedByUserID) AND  `PartTimeJobID` = '".$Input['PartTimeJobID']."'");

		}
		else
		{
			$sql = "SELECT ShortListedByUserID FROM tbl_part_time_job WHERE `PartTimeJobID` = '".$Input['PartTimeJobID']."' AND ( ShortListedByUserID IS NOT NULL AND ShortListedByUserID != '' )";

			$data = $this->db->query($sql);
			
			if($data->num_rows() > 0)
			{
				$sql = $this->db->query("UPDATE tbl_part_time_job SET `ShortListedByUserID` = CONCAT(`ShortListedByUserID`, ',".$EntityID."') WHERE `PartTimeJobID` = '".$Input['PartTimeJobID']."'");
			}
			else
			{
				$sql = $this->db->query("UPDATE tbl_part_time_job SET `ShortListedByUserID` = ".$EntityID." WHERE `PartTimeJobID` = '".$Input['PartTimeJobID']."'");
			}			
		}

		return TRUE;
	}


	function uploadLatestResume($EntityID, $Input)
	{
		$ApplyID = $Input['ApplyID'];

		$sql = "SELECT m.MediaName
		FROM tbl_post_job_applications ja
		INNER JOIN tbl_media m ON ja.MediaID = m.MediaID
		WHERE ja.ApplyID = '$ApplyID'
		LIMIT 1	";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows() <= 0)
		{
			return -1;
		}

		$mediaObj 	= $Query->result_array();
		$MediaName 	= $mediaObj[0]['MediaName']; 

		$MediaID = 0;
		if(!empty($Input['MediaGUID']))
		{
			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
			$this->db->limit(1);
			$que = $this->db->get();			
			if($que->num_rows() > 0)
			{
				$mediaID = $que->result_array();
				$MediaID = $mediaID[0]['MediaID'];
			}
		}


		$UpdateData = array(
		"IsResumeUpdated"=>1,		
		"MediaID"=>$MediaID,
		"ModifyDate"=>date("Y-m-d H:i:s"));
		$this->db->where('ApplyID', $ApplyID);
		$this->db->update('tbl_post_job_applications', $UpdateData);


		$InsertData = array("ApplyID"=>$ApplyID,
		"LogDate"=>date("Y-m-d H:i:s"),
		"LogStatus"=>27);		
		$this->db->insert('tbl_post_job_applications_log', $InsertData);

		$this->db->trans_complete();

		return $ApplyID;
	}

}