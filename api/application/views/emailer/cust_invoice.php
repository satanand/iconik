<style type="text/css">
.table td, .table tr, .table th
{
	border: 1px solid #ccc !important;
}

b
{
	font-size: 12px !important;
}
</style>

<table border="0" width="100%">
<tr>
<td colspan="2" align="center"><b>INVOICE</b></td>
</tr>
</table>

<br/>


<table width="100%" border="0" style="border-collapse: collapse; border:0px none !important;">
<tr>
	<td align="left"><b style="font-size: 12px;">ORDER ID:</b> <?php echo str_pad($content['Data']['Order']['OrderID'], 7, "0", STR_PAD_LEFT);?></th>
	<td align="right">
		<b style="font-size: 12px;">ORDER DATE:</b> <?php echo $content['Data']['Order']['OrderDate'];?><br/>
		<b style="font-size: 12px;">ORDER STATUS:</b> <?php echo ucfirst($content['Data']['Order']['OrderStatus']);?>
	</td>   
	
</tr>	

<tr>
	<td align="left"><br/><b style="font-size: 12px;">CUSTOMER DETAILS</b><br/>
	<?php echo $content['Data']['Order']['CustomerName'];?><br/>
	<?php echo $content['Data']['Order']['CustomerEmail'];?><br/>
	<?php echo $content['Data']['Order']['CustomerMobile'];?>		
	</td>
	

	<td align="right"><b style="font-size: 12px;">DELIVERY DETAILS</b><br/>
	<?php echo $content['Data']['Order']['BillingAddress'];?><br/>
	<?php echo $content['Data']['Order']['BillingCity'];?>, 
	<?php echo $content['Data']['Order']['BillingState'];?>, 
	<?php echo $content['Data']['Order']['BillingZipCode'];?>		
	</td>	
</tr>

	
<tr>
	<td colspan="2" align="left"><br/><b style="font-size: 12px;">AMOUNT:</b> <?php echo $content['Data']['Order']['NetTotal'];?>		
	</td>    
</tr>
</table>


<hr/>

<div style="width: 100%; overflow: auto;">
<table width="100%" border="1" style="border-collapse: collapse;" cellpadding="5" cellspacing="5">
<thead>
<tr>	   
    <th>Product Name</th>
    <th>Description (first 100 words)</th>    
    <th>Institute Name</th>
    <th>Unit Price</th>
    <th>Ordered Qty</th>
    <th>Amount</th>
    <th>Discount Amount</th>    
    <th>Amount</th>
</tr>
</thead>

<tbody id="tabledivbody">
<?php
if(isset($content['Data']['OrderDetails']) && !empty($content['Data']['OrderDetails']))
{
	$subtotal = $distotal = $shptotal = $gndtotal = 0;
	foreach($content['Data']['OrderDetails'] as $arr)
	{
		if(isset($arr['Amount']) && !empty($arr['Amount']))
		$subtotal = $subtotal + $arr['Amount'];

		if(isset($arr['DiscountAmount']) && !empty($arr['DiscountAmount']))
		$distotal = $distotal + $arr['DiscountAmount'];

		if(isset($arr['ShippingCost']) && !empty($arr['ShippingCost']))
		$shptotal = $shptotal + $arr['ShippingCost'];

	?>	
		<tr>	
			<td><?php echo $arr['ProductName'];?></td>	

			<td><?php echo substr($arr['ProductDesc'], 0, 100);?></td> 

			<td><?php echo ucwords($arr['FirstName']." ".$arr['LastName']);?></td>

			<td align="center"><?php echo $arr['ProductPrice'];?></td>

			<td align="center"><?php echo $arr['OrderedQty'];?></td>

			<td align="center"><?php echo $arr['Amount'];?></td>	

			<td align="center"><?php echo $arr['DiscountAmount'];?></td>			

			<td align="center"><?php echo $arr['Amount'];?></td>
		</tr>
<?php
	}
?>
		
		<tr>
			<td colspan="5" align="right"><i><b style="font-size: 12px;">TOTAL</b></i></td>	
			<td align="center"><i><b style="font-size: 12px;"><?php echo $subtotal;?></b></i></td>
			<td align="center"><i><b style="font-size: 12px;"><?php echo $distotal;?></b></i></td>
			<td align="center"><i><b style="font-size: 12px;"><?php echo $subtotal;?></b></i></td>
		</tr>

		<tr>
			<td colspan="8" align="right">&nbsp;</td>	
		</tr>

		<tr>
			<td colspan="7" align="right" valign="top"><b style="font-size: 12px;">SUMMARY</b></td>			

			<td>
				<table class="table" width="100%" border="1" style="border-collapse: collapse;" cellpadding="5" cellspacing="5">
					<tr>
						<td><b style="font-size: 12px;">TOTAL AMOUNT</b></td>			

						<td><b><?php echo $subtotal;?></b></td>	
					</tr>
					

					<tr>	
						<td><b style="font-size: 12px;">TOTAL DISCOUNT</b></td>

						<td><b><?php echo $distotal;?></b></td>
					</tr>
					

					<tr>	
						<td><b style="font-size: 12px;">TOTAL SHIPPING</b></td>

						<td><b><?php echo $shptotal;?></b></td>
					</tr>


					<tr>	
						<td><b style="font-size: 12px;">GRAND TOTAL</b></td>

						<td><b><?php echo $content['Data']['Order']['NetTotal'];?></b></td>
					</tr>					
				</table>
			</td>	
		</tr>

		
<?php
}
?>			
</tbody>
</table>
</div>