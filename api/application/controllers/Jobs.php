<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends API_Controller_Secure
{
	function __construct()

	{

		parent::__construct();

		$this->load->model('Jobs_model');

	}



	/*

	Description: 	Use to add new job

	URL: 			/api_admin/jobs/add	

	*/

	public function add_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('JobTitle', 'Job Title','trim|required');
		$this->form_validation->set_rules('Description', 'Description','trim|required');
		$this->form_validation->set_rules('EmployementType', 'Employement Type','trim|required');
		$this->form_validation->set_rules('StartDate', 'Start Date','trim|required');
		$this->form_validation->set_rules('EndDate', 'End Date','trim|required');
		$this->form_validation->set_rules('Experience', 'Experience','trim|required');
		$this->form_validation->set_rules('Qualification', 'Qualification','trim|required');
		$this->form_validation->set_rules('JobLocationState', 'Job Location State','trim|required');
		$this->form_validation->set_rules('JobLocationCity', 'Job Location City','trim|required');
		$this->form_validation->set_rules('JobStatus', 'Job Status','trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobs_model->add($this->EntityID, $this->Post);

		if($JobsData == 1)
		{
			$this->Return['Message']      	=	"New Job Posted Successfully."; 
		}
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"sorry! Some error occured.";			
		}
	}


	/*

	Description: 	Use to get jobs list

	URL: 			/api/jobs/getJobs	

	*/

	public function getJobs_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Qualification', 'Qualification', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getJobs($this->EntityID, $this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*

	Description: 	Use to edit job

	URL: 			/api_admin/jobs/edit	

	*/

	public function edit_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PostJobGUID', 'PostJobGUID', 'trim|required');
		$this->form_validation->set_rules('JobTitle', 'Job Title','trim|required');
		$this->form_validation->set_rules('Description', 'Description','trim|required');
		$this->form_validation->set_rules('EmployementType', 'Employement Type','trim|required');
		$this->form_validation->set_rules('StartDate', 'Start Date','trim|required');
		$this->form_validation->set_rules('EndDate', 'End Date','trim|required');
		$this->form_validation->set_rules('Experience', 'Experience','trim|required');
		$this->form_validation->set_rules('Qualification', 'Qualification','trim|required');
		$this->form_validation->set_rules('JobLocationState', 'Job Location State','trim|required');
		$this->form_validation->set_rules('JobLocationCity', 'Job Location City','trim|required');
		$this->form_validation->set_rules('JobStatus', 'Job Status','trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobs_model->edit($this->EntityID, $this->Post);

		if($JobsData == 1)
		{
			$this->Return['Message']      	=	"Job Updated Successfully.";
		} 
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";			
		}
	}

	/*

	Description: 	Use to delete job

	URL: 			/api_admin/jobs/delete	

	*/

	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PostJobGUID', 'PostJobGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobs_model->delete($this->EntityID, $this->Post);

		//echo $NewsData;

		if($JobsData == 1){
			$this->Return['Message']      	=	"Job Deleted Successfully.";

		} else {
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";
			
		}
	}


	/*
	Description: 	Use to get Applicants for particular Job
	URL: 			/api/jobs/getApplicants	
	*/

	public function getApplicants_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PostJobID', 'PostJobID', 'trim|integer|required');		
			
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getApplicants($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	public function saveFacultyStatus_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('PostJobIDForStatus', 'Job ID','trim|required');		
		$this->form_validation->validation($this);
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Status = $this->Jobs_model->saveFacultyStatus($this->EntityID, $this->Post);

		/*if($Status == "Empty")
		{
			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Please select status for atleast one record."; 
		}
		else*/if($Status == "Error") 
		{
            $this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Sorry! Some error occured.";			
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;

			$this->Return['Message']      	=	"Status updated successfully.";
		}
	}



	public function addFixInterview_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ApplyID', 'Applicant','trim|required');
		$this->form_validation->set_rules('InterviewType', 'Interview Type','trim|required');
		$this->form_validation->validation($this);		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$JobsData = $this->Jobs_model->addFixInterview($this->EntityID, $this->Post);

		if($JobsData == -1)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Re-Scheduled interview date should be greater than last interview date.";
		}
		if($JobsData == -2)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Personal interview date should be greater than telephonic interview date.";
		}
		elseif($JobsData)
		{
			$this->Return['Message']      	=	"Updated successfully."; 
		}
		else 
		{
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"sorry! Some error occured.";			
		}
	}


	public function getJobLog_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ApplyID', 'ApplyID', 'trim|integer|required');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getJobLog($this->EntityID, $this->Post);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}



	/*-------------------------------------------------------------
	Section For Faculty Market Place
	-------------------------------------------------------------*/
	/*
	Description: 	Use to get jobs posted by institutes list
	URL: 			/api/jobs/getPostedJobs
	*/
	public function getPostedJobs_post()
	{
		//$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|required|callback_validateEntityGUID');
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getPostedJobs($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*
	Description: 	Use to apply For Job by Faculty
	URL: 			/api/jobs/applyForJob
	*/
	public function applyForJob_post() 
	{	
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('PostJobID', 'PostJobID', 'trim|required');
		
		$this->form_validation->set_rules('app_add_qualification', 'Qualification', 'trim|required');
		$this->form_validation->set_rules('aap_add_experience', 'Experience', 'trim|required');
		$this->form_validation->set_rules('aap_add_age', 'Age', 'trim|required');
		$this->form_validation->set_rules('aap_add_note_to_employer', 'Note To Employer', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'Resume', 'trim|required');

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Status = $this->Jobs_model->applyForJob($this->EntityID, $this->Post);

		if($Status == -1)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You already applied for selected job.";
			$this->Return['Data']      		=	array(); 
		}
		elseif($Status == 0)
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured.";
			$this->Return['Data']      		=	array(); 
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Message']      	=	"Successfully applied for job.";
			$this->Return['Data']      		=	array();
		}
	}


	/*
	Description: 	Use to get Applied Jobs by Faculty
	URL: 			/api/jobs/getAppliedJobs
	*/
	public function getAppliedJobs_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getAppliedJobs($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}



	/*
	Description: 	Use to get ShortListed Jobs by Faculty
	URL: 			/api/jobs/getShortListedJobs
	*/
	public function getShortListedJobs_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getShortListedJobs($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*
	Description: 	Use to set Shortlist Jobs by faculty
	URL: 			/api/jobs/setShortlistJobs
	*/
	public function setShortlistJobs_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');	
		$this->form_validation->set_rules('PostJobID', 'Job ID', 'trim|required');
		$this->form_validation->set_rules('Type', 'Type', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->setShortlistJobs($this->EntityID, $this->Post);

		if($JobsData)
		{
			$this->Return['Message']      	=	"Successfully done"; 
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message']      =	"Sorry! Some error occured"; 
		}
	}


	


 	public function uploadLatestResume_post() 
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ApplyID', 'ApplyID', 'trim|required');		
		$this->form_validation->set_rules('MediaGUID', 'Resume', 'trim|required');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Status = $this->Jobs_model->uploadLatestResume($this->EntityID, $this->Post);

		if($Status == -1)
		{			
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Your application not found.";
			$this->Return['Data']      		=	array(); 
		}
		else
		{
			$this->Return['ResponseCode'] 	=	200;
			$this->Return['Message']      	=	"Resume updated successfully.";
			$this->Return['Data']      		=	array();
		}
	}
	


}