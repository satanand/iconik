<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends API_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Page_model');
		$this->load->model('Institute_model');
		$this->load->model('Adda_model');
		$this->load->model('Staff_model');
		$this->load->model('Event_model');
		$this->load->model('Gallery_model');
		$this->load->model('Students_model');
		$this->load->model('Jobs_model');
		$this->load->model('Jobprovider_model');
		$this->load->model('Askquestion_model');
	}


	/*
	Description: 	Use to get Get single event details.
	URL: 			/api/Event/getEventDetails
	Input (Sample JSON): 		
	*/
	public function getEventDetails_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->set_rules('Type', 'Type','trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		//$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if(!empty($this->input->post('Type'))){
			$EventParticipantsData = $this->Event_model->getEventParticipants("", array("EventID"=>$this->EventID,"UserID" => $this->EntityID));
		}else{
			$EventParticipantsData = $this->Event_model->getEventParticipants("", array("EventID"=>$this->EventID));
		}	

		if(!empty($this->input->post('Type'))){
			$EventActivityData = $this->Event_model->getEventActivity("", array("EventID"=>$this->EventID,"UserID" => $this->EntityID));
		}else{
			$EventActivityData = $this->Event_model->getEventActivity("", array("EventID"=>$this->EventID)); 

		}		

		//$EventActivityData = $this->Event_model->getEventActivity($this->EntityID, array("EventID"=>$this->EventID)); 

		$EventDetails = $this->Event_model->getEventByID("",array("EventID"=>$this->EventID));

		if(!empty($EventDetails)){
			$data['EventDetails'] = $EventDetails;
		}else{
			$data['EventDetails'] = [];
		}
		

		if(!empty($EventActivityData)){
			$data['EventActivities'] = $EventActivityData['Data'];
		}else{
			$data['EventActivities'] = [];
		}

		if(!empty($EventParticipantsData)){
			$data['EventParticipants'] = $EventParticipantsData['Data'];
		}else{
			$data['EventParticipants'] = [];
		}
		
		if(!empty($data)){
			$this->Return['Data'] = $data;
		}	
	}


	/*
	Name: 			Subscribe
	Description: 	Use to store subscribed users email from MP
	*/
	public function Subscribe_post(){
		$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_message('valid_email', 'Please enter valid email.');
		$this->form_validation->validation($this);

		$data = $this->Page_model->Subscribe($this->input->post());

		if($data == -1){
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! This email address already exist.";
		}else if($data > 0){
			$this->Return['Message'] = "Thank you for your subscription.";
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! Please try later";
		}	
	}

	/*
	Name: 			getPage
	Description: 	Use to get page data.
	*/
	public function getPage_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PageGUID', 'PageGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$PageData = $this->Page_model->getPage('*',
			array("PageGUID" => $this->Post['PageGUID'])
		);
		if($PageData){
			$PageData['Content'] = htmlentities($PageData['Content']);
			$this->Return['Data'] = $PageData;
		}	
	}



	/*
	Name: 			savePage
	Description: 	Use to update page data.	
	*/
	public function savePage_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('PageGUID', 'PageGUID', 'trim|required');
		$this->form_validation->set_rules('Title', 'Page Title', 'trim');
		$this->form_validation->set_rules('Content', 'Content', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$this->Page_model->editPage($this->Post['PageGUID'], array('Title'=>$this->Post['Title'], 'Content'=>$this->Post['Content']));

		$this->Return['Message']      	=	"Updated successfully."; 
	}



	public function getStatesList_post()
	{
		$this->form_validation->set_rules('CountryCode', 'CountryCode', 'trim');
		$this->form_validation->validation($this);  /* Run validation */	
		$StatesList = $this->Common_model->getStatesList($this->input->post('CountryCode'));
		
		if(!empty($StatesList)){
			$this->Return['Data'] = $StatesList;
		}	
	}


	public function getCountriesList_post()
	{
	
		$CountriesList = $this->Common_model->getCountriesList();
		
		if(!empty($CountriesList)){
			$this->Return['Data'] = $CountriesList;
		}	
	}


	public function getCitiesList_post()
	{		
		$this->form_validation->set_rules('State_id', 'State_id', 'trim|required');
		$this->form_validation->validation($this);

		//print_r($this->input->post()); die;

		$CitiesList = $this->Common_model->getCitiesList($this->input->post('State_id'));
		
		if(!empty($CitiesList)){
			$this->Return['Data'] = $CitiesList;
		}	
	}

	public function getCitiesByStateName_post()
	{
		$this->form_validation->set_rules('StateName', 'StateName', 'trim|required');
		$this->form_validation->validation($this);

		//print_r($this->input->post()); die;

		$CitiesList = $this->Common_model->getCitiesListByStateName($this->input->post('StateName'));
		
		if(!empty($CitiesList)){
			$this->Return['Data'] = $CitiesList;
		}	
	}



	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	public function getInstitutes_post()
	{
		/* Validation section */
		//$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('StateName', 'StateName', 'trim');
		$this->form_validation->set_rules('CityName', 'CityName', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		/*$UsersData = $this->Institute_model->getAllInstitutes($this->EntityID,'RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);*/

		$UsersData = $this->Institute_model->getAllInstitutesSearch($this->EntityID,$this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);
		
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}		


	/*

	Name: 			getPosts

	Description: 	Use to get list of student adda post.

	URL: 			/api/post/getPosts

	*/

	public function getPosts_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('PostGUID', 'PostGUID', 'trim');

		$this->form_validation->set_rules('PostType', 'PostType', 'trim|in_list[Post,Reply]');

		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$Posts=$this->Adda_model->getPosts('', $this->Post, TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		//print_r($Posts);

		if($Posts)
		{
			$this->Return['Data'] = $Posts['Data'];
		}
		else
		{
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message']   =	""; 
			$this->Return['Data'] = [];
		}

	}

	/*

	Name: 			getProfile

	Description: 	Use to get user profile info.

	URL: 			/api/user/getProfile

	*/

	public function getProfile_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */

		$formData=$this->Users_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array("UserID"=>$this->UserID));
        
       	$this->Return['Data'] = $formData;
        

	}

	public function getCourse_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */

		$Data = $this->Staff_model->getCourse($this->UserID);
        
        if(!empty($Data)){
        	$this->Return['Data'] = $Data;	
        }
	}



	public function getEvent_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */

		$Data = $this->Event_model->getEvent("",array("UserID"=>$this->UserID),TRUE);
        //print_r($Data);
       	
        if(!empty($Data)){
        	$this->Return['Data'] = $Data['Data'];	
        }

	}


	public function getPhotosInAlbum_post(){
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('Category', 'Category','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
	    $GalleryData = $this->Gallery_model->getPhotosInAlbum($this->UserID, $this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData['Data'];
		}	
	}

	
	/*----------------------------------------------------------------------------------------
    Scholarship Test Section
    ----------------------------------------------------------------------------------------*/
	public function getScholarshipTests_post()
	{
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->validation($this); 
		
		$Data = $this->Students_model->getScholarshipTests($this->Post);
		
		if($Data)
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $Data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found.";
			$this->Return['Data'] = array();
		}	
	}

	/*-------------------------------------------------------------
	Section For Faculty Market Place
	-------------------------------------------------------------*/
	/*
	Description: 	Use to get jobs posted by institutes list
	URL: 			/api/jobs/getPostedJobs
	*/
	public function getPostedJobs_post()
	{
		//$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|required|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		//$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$JobsData = $this->Jobs_model->getPostedJobs("", $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}


	/*-------------------------------------------------------------
	Section For student of Market Place
	-------------------------------------------------------------*/
	/*
	Description: 	Use to get jobs posted by job provider
	URL: 			/api/jobs/getPartTimeJobs
	*/
	public function getPartTimeJobs_post()
	{		
		$this->form_validation->set_rules('ForStudents', 'ForStudents', 'trim');
		$this->form_validation->set_rules('FilterKeyword', 'FilterKeyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$JobsData = $this->Jobprovider_model->getPartTimeJobs("", $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($JobsData)
		{
			$this->Return['Data']      	=	$JobsData['Data']; 
		}
	}

	/*-------------------------------------------------------------
    Ask A Question - For Every One Global Section
    -------------------------------------------------------------*/
    public function getAAQList_post()
    {
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim');

		$this->form_validation->validation($this);  /* Run validation */

		$data = $this->Askquestion_model->getAAQList("", $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = ""; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message'] = "No record found."; 
		}
	}

	public function getAAQReplyOnly_post()
    {
		
		$this->form_validation->set_rules('AAQID', 'Question Id', 'trim|required');
		$this->form_validation->set_rules('filterReply', 'Search Word', 'trim');

		$this->form_validation->validation($this);  /* Run validation */	

		$data = $this->Askquestion_model->getAAQReplyOnly("", $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = ""; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] =	500;
			$this->Return['Message'] = "No record found."; 
		}
	}

	public function getAAQReply_post()
    {
		$this->form_validation->set_rules('AAQID', 'Question Id', 'trim|required');
		$this->form_validation->set_rules('filterReply', 'Search Word', 'trim');

		$this->form_validation->validation($this);  /* Run validation */	

		$data = $this->Askquestion_model->getAAQReply("", $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = ""; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] =	500;
			$this->Return['Message'] = "No record found."; 
		}  
	}


	/*
	Description: 	Use to get Get photo gallery by album id.
	URL: 			/api/page/getPhotosGallery
	Input (Sample JSON): 		
	*/
	public function getPhotosGallery_post()
	{
		//$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$this->form_validation->set_rules('AlbumID', 'AlbumID','trim|required');
		$this->form_validation->set_rules('AlbumName', 'AlbumName','trim|required');
		$this->form_validation->validation($this); 
    
	    $GalleryData = $this->Gallery_model->getPhotosGallery("",$this->Post);

		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData['Data'];
		}	
	}


	public function BAEnquiry_post()
	{		
		$this->form_validation->set_rules('FullName', 'Full Name','trim|required');
		$this->form_validation->set_rules('Email', 'Email','trim|required|valid_email');
		$this->form_validation->set_rules('MobileNumber', 'Mobile Number','trim|required|min_length[10]|max_length[10]');
		
		$this->form_validation->validation($this);
		
		$Data = $this->Page_model->BAEnquiry($this->Post);

		if($Data === "Exist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You already downloaded the brochure using same email id.";
		}
		else 
		{
            $this->Return['ResponseCode'] 	=	200;
            $this->Return['Message']      	=	"Brochure has been sent to your email id. Please check your inbox."; 
		}
	}


	/*public function addStudentDirect_post()
	{
		
		$this->form_validation->set_rules('Course', 'Course', 'trim|required');
		$this->form_validation->set_rules('Batch', 'Batch','trim|required');		

		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');
		$this->form_validation->set_rules('LastName', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('PhoneNumber', 'Mobile Number', 'trim|required|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('Address', 'Personal Address', 'trim|required');
		$this->form_validation->set_rules('StateID', 'State Name', 'trim|required');
		$this->form_validation->set_rules('CityName', 'City Name', 'trim|required');		
		
		$this->form_validation->set_rules('FathersName', 'Fathers Name', 'trim|required');
		$this->form_validation->set_rules('MothersName', 'Mothers Name', 'trim|required');
		$this->form_validation->set_rules('FathersPhone', 'Fathers/Mothers Mobile', 'trim|required|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('FathersEmail', 'Fathers/Mothers Email', 'trim|required|valid_email');
					
		$this->form_validation->validation($this);
		
		$UserID = $this->Page_model->addStudentDirect($this->Post);

		if($UserID == "PhoneNumber Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This mobile number is already exist.";
		}
		elseif($UserID == "Email Exist")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "This email already exist.";
		}
		elseif(isset($UserID) && !empty($UserID)) 
		{
			$this->Return['Data'] = 1;

			$this->Return['Message'] = "Saved successfully.";
		}
		else 
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";
		}
	}*/


}