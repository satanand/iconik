<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4" id="top-heading">Manage Churn Enquiry</h1>
</header>
<?php //$this->load->view('includes/breadcrumb'); ?>

<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body"><!-- Body -->


<nav aria-label="breadcrumb" >
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>  
		<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>enquiry">Manage Enquiry</a></li>
		<li class="breadcrumb-item">Churned</li> 
	</ol> 
</nav>


<!-- Top container -->

<div class="clearfix mt-2 mb-2">
	<form class="" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<span class="h5">Total records: {{data.dataList.length}}</span>
				</div>
			</div>
		</div>
		
		<div class="row">	
			<div class="col-md-2">
				<div class="form-group">
					<select name="filterStatus" id="filterStatus" class="form-control">
						<option value="">Select Status</option>

						<option value="4">Closed</option>

						<option value="5">Dropped</option>
					</select>
				</div>
			</div>			


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterSource" id="filterSource" class="form-control">

						<option value="">Select Source</option>

						<option value="Reference">Reference</option>

						<option value="Website">Website</option>

						<option value="App">App</option>

						<option value="Walkin">Walk-in</option>

						<option value="Advertisement">Advertisement</option>

					</select>
				</div>
			</div>


			<div class="col-md-2">
				<div class="form-group">
					<select name="filterAssignedToUser" id="filterAssignedToUser" class="form-control">

						<option value="">Select Assigned To</option>

						<option ng-repeat="xx in data.Users" value="{{xx.UserID}}">{{xx.FirstName}}&nbsp;{{xx.LastName}}&nbsp;({{xx.UserTypeName}})</option>

					</select>
				</div>
			</div>


		
			<div class="col-md-3">
				<div class="form-group">
					<div id="universal_search"> 
						<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Type and search" style="" autocomplete="off">
						
					</div>				 	 
			    </div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
		</div>
	</form>			

	
</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 
		

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width:80px;" class="text-center">ID</th>

					<th style="width: 200px;">Name</th>

					<th style="width: 100px;">Mobile</th>

					<th style="width: 100px;">Interested In</th>

					<th style="width: 100px;">Assigned To</th>

					<th style="width: 100px;">Total Calls</th>

					<th style="width: 100px;">Last Remarks</th>					

					<th style="width: 100px;">Last Call Date</th>

					<th style="width: 100px;" class="text-center">Status</th>

					<th style="width: 100px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">

				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.EnquiryID}}">

					<td>{{ (key + 1)}}</td>

					<td>{{row.EnquiryPersonName}}<br/>({{row.EnquirySource}})</td>					

					<td>{{row.EnquiryMobile}}<br/>{{row.EnquiryEmail}}</td>

					<td>{{row.EnquiryInterestedIn}}</td>

					<td>{{row.AssignedTo}}</td>

					<td>{{row.totalCalls}}</td>

					<td>{{row.LastRemarks}}</td>					

					<td>{{row.LastCallDate}}</td> 

					<td>{{row.CurrentStatus}}</td>

					<td class="text-center">					
						
						<a class="glyphicon glyphicon-plus" href="" ng-if="((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" ng-click="loadFormAssign(key, row.EnquiryGUID)" title="Re-Assign Enquiry"></a>
					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->


	<!-- Re Assign Modal -->

	<div class="modal fade" id="assign_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Re-Assign Enquiry</h3>  	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAssign"></div>

			</div>

		</div>

	</div>





</div><!-- Body/ -->