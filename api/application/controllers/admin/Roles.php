<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Roles extends API_Controller_Secure

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('Category_model');
		$this->load->model('Roles_model');

	}

	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function add_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		//$this->form_validation->set_rules('UserTypeName', 'Roles Type', 'trim|required|callback_validateUserTypeName');

	
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */



		$RolesData = $this->Roles_model->addRoles($this->SessionUserID,$this->Post['UserTypeName']);

		if($RolesData){

			if($RolesData == 'EXIST'){
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This staff role name already exist.";
			}else{

				$this->Return['Message']      	=	"New staff role added successfully."; 
			}

		}

	}


	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function editRoles_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');

		$this->form_validation->set_rules('UserTypeName', 'UserTypeName', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Roles_model->editRoles($this->input->Post('UserTypeID'),$this->input->Post('UserTypeName'));
	
		$this->Return['Data']= $this->Roles_model->getRolesd($this->input->Post('UserTypeID'),array());

	    $this->Return['Message']      	=	"Roles updated successfully."; 

		}

	/* permission */
	public function PermissionRoles_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');

		//$this->form_validation->set_rules('ModuleID', 'ModuleID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
         
        $this->Roles_model->permissiondel($this->input->Post('UserTypeID'));
		
		$checkbox1=$this->input->Post('ModuleID');
		//print_r($checkbox1);

		foreach($checkbox1 as $chk1)  
        {  

       	 $this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),$chk1); 

		}
	
		//$this->Return['Data']= $this->Roles_model->getRolesd($this->EntityID,array());
		$this->Return['Data']= $this->Roles_model->getRolesd($this->input->Post('UserTypeID'),array());

	    $this->Return['Message']      	=	"Roles updated successfully."; 

	}



	


	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function editCategory_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('CategoryName', 'Category Name', 'trim|required');

		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');



		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->Category_model->editCategory($this->CategoryID, array('CategoryName'=>$this->Post['CategoryName'], 'StatusID'=>$this->StatusID));



		/* check for media present - associate media with this Post */

		if(!empty($this->Post['MediaGUIDs'])){

			$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);

			foreach($MediaGUIDsArray as $MediaGUID){

				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));

				if ($MediaData){

					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $this->CategoryID);

				}

			}

		}

		/* check for media present - associate media with this Post - ends */





		$CategoryData = $this->Category_model->getCategories('',

			array("CategoryID"=>$this->CategoryID));

			$this->Return['Data'] = $CategoryData;

			$this->Return['Message']      	=	"Category updated successfully."; 

		}





	/*

	Description: 	use to get list of filters

	URL: 			/api_admin/entity/getFilterData	

	*/

	public function getFilterData_post()

	{

		/* Validation section */

		//$this->form_validation->set_rules('ParentCategoryGUID', 'Parent Category', 'trim|callback_validateEntityGUID[Category,ParentCategoryID]');

		//$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */





		$CategoryTypes = $this->Roles_model->getRolesTypes();
    
		if($CategoryTypes){

			$Return['RolesTypes'] = $CategoryTypes['Data']['Records'];			

		}

		$this->Return['Data'] = $Return;

	}


	/*

	Description: 	use to get list of filters

	URL: 			/api_admin/entity/getFilterData	

	*/

	public function getSecondLevelPermission_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('ModuleID', 'ModuleID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */


		$SecondLevelPermission = $this->Roles_model->getSecondLevelPermission($this->input->post('ModuleID'));
    
		if($SecondLevelPermission){

			$this->Return['Data'] = $SecondLevelPermission;	

		}

		

	}

}

