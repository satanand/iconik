app.controller('PageController', function($scope, $http, $timeout) {
    $scope.data.pageSize = 100;
    $scope.data.dataList = [];
    $scope.data.filterDataA = [];
    $scope.data.filterDataC = [];
    $scope.data.CategoryData = [];
    $scope.data.SecondLevelPermission = [];

    $scope.APIURL = API_URL + "upload/image";
    $scope.APIURL_FILE = API_URL + "upload/file";
   

    /*----------------*/
    $scope.getIframe = function(url) {
        var a = $scope.createVideo(url, 200, 150);
        $(".iframe:last").html(a);
    }

    $scope.getFilterData = function(Category = "") {

        /*if (!Category) {
          $scope.getList(Category);
        }*/
         if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
        var data = 'SessionKey=' + SessionKey;
        $scope.data.filterDataA = [];
        $scope.data.filterDataC = [];
        $http.post(API_URL + 'Library/getFilterData', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data) {
                /* success case */
                if (response.Data.Category) {
                    for (var i in response.Data.Category.Data.Records) {
                        console.log(response.Data.Category.Data.Records[i]);
                        $scope.data.filterDataC.push(response.Data.Category.Data.Records[i]);
                    }
                }

                if (response.Data.Author) {
                    for (var i in response.Data.Author) {

                        $scope.data.filterDataA.push(response.Data.Author[i]);
                    }
                }

                console.log($scope.data.filterDataC);



                $timeout(function() {

                    $("select.chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8
                    }).trigger("chosen:updated");

                }, 300);
            }

        });

        // $scope.getUserData();

    }


    /*get user NAme*/
    $scope.getUserData = function(BookGUID = "")

    {

        var data = 'SessionKey=' + SessionKey + '&BookGUID=' + BookGUID;

        $scope.data.UserData = [];

        $http.post(API_URL + 'Library/getUserData', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data) {
                /* success case */


                for (var i in response.Data.Records) {

                    $scope.data.UserData.push(response.Data.Records[i]);
                }

                //console.log($scope.data.UserData);

                /*$timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 500);  */
            }

        });


    }
    /*delete*/

    $scope.loadFormBookDelete = function(Position, BookGUID) {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'library/delete', 'SessionKey=' + SessionKey + '&BookGUID=' + BookGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


    /*get set category book*/
    $scope.getCategory = function()

    {

        var data = 'SessionKey=' + SessionKey;
        $scope.data.CategoryData = [];
        $http.post(API_URL + 'Library/getCategory', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */


                for (var i in response.Data.Records) {

                    $scope.data.CategoryData.push(response.Data.Records[i]);
                }


                /*
           $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 200);*/
            }

        });


    }


    /*list*/



    $scope.applyFilter = function() {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getFilterData();
        $scope.getList();
    }

    $scope.applytinymce = function() {
        tinymce.init({
            selector: '#Synopsis',
            setup: function(editor) {
                editor.on('keyup', function(e) {
                    console.log('edited. Contents: ' + editor.getContent());
                    formDataCheck(editor.getContent());
                });
            }
        });
    }


    /*list append*/



    $scope.getList = function(check, Category = "", Author = "", FormType = "", Keyword = "") {
        if (check != 1) {

            if ($scope.data.listLoading || $scope.data.noRecords) return;

            $scope.data.pageSize = 20;


        } else {
            $scope.Category = Category;
            $scope.Author = Author;
            $scope.FormType = FormType;
            $scope.Keyword = Keyword;
            $scope.data.pageNo = 1;
            $scope.data.dataList = [];
        }


        $scope.data.listLoading = true;
        var data = 'SessionKey=' + SessionKey + '&Category=' + Category + '&Author=' + Author + '&FormType=' + FormType + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize()+ '&Keyword=' + Keyword;


        console.log(data);

        $http.post(API_URL + 'Library/getLibrary', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }
                //  console.log($scope.data.dataList);
                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;
            }
            $scope.data.listLoading = false;

            // setTimeout(function(){ tblsort(); }, 1000);

        });

    }


    /*load add form*/
    $scope.loadFormAdd = function(Position) {
        $scope.formType = "books";
        $scope.getCategory();

        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();

        $('#add_model').modal({
            show: true
        });

        $timeout(function() {

            tinymce.init({
                selector: '#Synopsis'
            });
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 700);

    }

    /*load add form*/
    $scope.loadFormAddD = function(Position) {
        $scope.formType = "digital";
        $scope.getCategory();

        $scope.templateURLAddd = PATH_TEMPLATE + module + '/addd_form.htm?' + Math.random();

        $('#addd_model').modal({
            show: true
        });

        $timeout(function() {

            tinymce.init({
                selector: '#Synopsis'
            });
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 700);

    }


    $scope.getIframe = function(url) {
        var a = $scope.createVideo(url, 200, 150);
        $(".iframe:last").html(a);
    }


    $scope.loadContentView = function(ContentType, Title, URL_Data) {

        $scope.data.pageLoading = true;
        $scope.ContentType = ContentType;
        $scope.URL_Data = URL_Data;
        $scope.Title = Title;

        $scope.templateURLView = PATH_TEMPLATE + module + '/view_media.htm?' + Math.random();
        $('#view_media').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }

    /*load add form*/
    $scope.loadFormAddCategory = function(Position) {
        $('#add_model .close').click();
        $('#edits_model .close').click();
        $("#addd_model .close").click();
        $scope.templateURLCategory = PATH_TEMPLATE + module + '/addcategory_form.htm?' + Math.random();

        $('#addcategory_model').modal({
            show: true
        });

        $timeout(function() {
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 200);
    }


    $scope.cancelAddActivity = function() {
       // alert($scope.formType);
        if ($scope.formType == 'books') {
            $('#add_model').modal({
                show: true
            }); // console.log($scope.Categories);
            $("#Synopsis").show();
            tinymce.remove('#Synopsis');
            $timeout(function() {
                tinymce.init({
                    selector: '#Synopsis'
                });
                $(".chosen-select").chosen({
                    width: '100%',
                    "disable_search_threshold": 8,
                    "placeholder_text_multiple": "Please Select",
                }).trigger("chosen:updated");
            }, 1000);
        } else if ($scope.formType == 'digital') {
            $('#addd_model').modal({
                show: true
            });
            $timeout(function() {
                tinymce.init({
                    selector: '#Synopsis'
                });
                $(".chosen-select").chosen({
                    width: '100%',
                    "disable_search_threshold": 8,
                    "placeholder_text_multiple": "Please Select",
                }).trigger("chosen:updated");
            }, 1000);
        } else if ($scope.formType == 'EditDigital') {
            $('#edits_model').modal({
                show: true
            });
            $timeout(function() {
                tinymce.init({
                    selector: '#Synopsis'
                });
                $(".chosen-select").chosen({
                    width: '100%',
                    "disable_search_threshold": 8,
                    "placeholder_text_multiple": "Please Select",
                }).trigger("chosen:updated");
            }, 1000);
        } else if ($scope.formType == 'EditBooks') {
            $('#edits_model').modal({
                show: true
            });
            $timeout(function() {
                tinymce.init({
                    selector: '#Synopsis'
                });
                $(".chosen-select").chosen({
                    width: '100%',
                    "disable_search_threshold": 8,
                    "placeholder_text_multiple": "Please Select",
                }).trigger("chosen:updated");
            }, 1000);
        }
    }

    /*add category book*/

    $scope.addCategoryData = function() {

        var category = $('.Category').val(); //alert(category);

        $scope.addDataLoadingc = true;


        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='category_form']").serialize();

        $http.post(API_URL + 'library/addcategory', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.applyFilter();

                $scope.category = category;
            $timeout(function() {
                $('#addcategory_model .close').click();

                if ($scope.formType == 'books') {
                    $('#add_model').modal({
                        show: true
                    }); // console.log($scope.Categories);
                    $("#Synopsis").show();
                    tinymce.remove('#Synopsis');
                    $timeout(function() {
                        tinymce.init({
                            selector: '#Synopsis'
                        });
                        $(".chosen-select").chosen({
                            width: '100%',
                            "disable_search_threshold": 8,
                            "placeholder_text_multiple": "Please Select",
                        }).trigger("chosen:updated");
                    }, 1000);
                } else if ($scope.formType == 'digital') {
                    $('#addd_model').modal({
                        show: true
                    });
                    $timeout(function() {
                        tinymce.init({
                            selector: '#Synopsis'
                        });
                        $(".chosen-select").chosen({
                            width: '100%',
                            "disable_search_threshold": 8,
                            "placeholder_text_multiple": "Please Select",
                        }).trigger("chosen:updated");
                    }, 1000);
                }
            }, 300);


            } else {

                ErrorPopup(response.Message);

            }

            $scope.addDataLoadingc = false;

        });

    }



    /*load Book View*/



    $scope.loadFormView = function(Position, BookGUID, FormType) {

        $scope.data.Position = Position;

        if (FormType == 'Book') {

            $scope.templateURLView = PATH_TEMPLATE + module + '/view_form.htm?' + Math.random();
        } else {

            $scope.templateURLView = PATH_TEMPLATE + module + '/viewd_form.htm?' + Math.random();
        }
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'Library/getLibraryBy', 'SessionKey=' + SessionKey + '&BookGUID=' + BookGUID + '&FormType=' + FormType, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
                $scope.DigitalType = $scope.formData.DigitalType;
                $scope.MediaURL = $scope.formData.MediaURL;



                if ($scope.DigitalType == 'File') {
                    $scope.fileExtension = $scope.MediaURL.replace(/^.*\./, '');
                }
                // console.log($scope.fileExtension);
                $('#view_model').modal({
                    show: true
                });


                $timeout(function() {
                    tinymce.init({
                        selector: '#Synopsis'
                    });

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 700);

            }

        });

    }
    /*load edit form*/



    $scope.loadFormEdit = function(Position, BookGUID, FormType) {

        $scope.data.Position = Position;
        if (FormType == 'Book') {
            $scope.formType = "EditBooks"
            $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
        } else {
            $scope.formType = "EditDigital"
            $scope.templateURLEdit = PATH_TEMPLATE + module + '/editd_form.htm?' + Math.random();
        }

        $scope.data.pageLoading = true;
        $scope.getCategory();
        $http.post(API_URL + 'Library/getLibraryBy', 'SessionKey=' + SessionKey + '&BookGUID=' + BookGUID + '&FormType=' + FormType, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#edits_model').modal({
                    show: true,
                    backdrop: 'static',
                    keyboard: false
                });


                $timeout(function() {
                    tinymce.init({
                        selector: '#Synopsis'
                    });

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 700);

            }

        });

    }




    /*load loadFormIssueBook form*/



    $scope.loadFormIssueBook = function(Position, BookGUID, FormType) {

        $scope.templateURLIssueBook = PATH_TEMPLATE + module + '/issuebook.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $scope.data.BookGUID = BookGUID;
        $scope.getUserData(BookGUID);

        //$http.post(API_URL+'Library/getmodules', data, contentType).then(function(response) {

        $http.post(API_URL + 'Library/getLibraryBy', 'SessionKey=' + SessionKey + '&BookGUID=' + BookGUID + '&FormType=' + FormType, contentType).then(function(response) {

            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                $scope.data.pageLoading = false;
                //$scope.formData = response.Data.Records;

                $scope.formData = response.Data
                var d = new Date();
                $('#issunebook_model').modal({
                    show: true
                });
                $timeout(function() {
                    $("#IssueDate").datetimepicker({
                        format: 'dd-mm-yyyy',
                        minView: 2
                    }).datetimepicker('setEndDate', d);
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 700);

            }

        });
    }


    /*load permission form*/


    $scope.getBookDataByKey = function(key) {
        var date = $scope.formData[key].IssueDate;
        var date = new Date(date);
        var d = new Date(); //alert(date); //alert(d);
        $("#ReturnDate").datetimepicker({
            format: 'dd-mm-yyyy',
            minView: 2
        }).datetimepicker('setStartDate', date).datetimepicker('setEndDate', d);
    }



    $scope.loadFormReturnBook = function(Position, BookGUID, FormType) {

        $scope.templateURLReturnBook = PATH_TEMPLATE + module + '/returnbook.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $scope.data.BookGUID = BookGUID;

        //$http.post(API_URL+'Library/getmodules', data, contentType).then(function(response) {
        //$scope.getUserData();
        $scope.formData = [];
        $http.post(API_URL + 'Library/getReturnBook', 'SessionKey=' + SessionKey + '&BookGUID=' + BookGUID + '&FormType=' + FormType, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;
                //$scope.formData = response.Data.Records;

                for (var i in response.Data) {

                    $scope.formData.push(response.Data[i]);

                }

                // $scope.formData = response.Data

                $('#returnbook_model').modal({
                    show: true
                });
                var d = new Date();
                $timeout(function() {
                    //$("#ReturnDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d);

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 700);

            }

        });
    }

    /*add data*/



    $scope.addData = function() {
        $scope.addDataLoading = true;

        tinyMCE.triggerSave();
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();
        $http.post(API_URL + 'Library/add', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.applyFilter();

                $timeout(function() {

                    $('#add_model .close').click();

                }, 200);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }


    $scope.addDataD = function() {
        $scope.addDataLoading = true;

        tinyMCE.triggerSave();
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='addd_form']").serialize();
        $http.post(API_URL + 'Library/addd', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.applyFilter();

                $timeout(function() {

                    $('#addd_model .close').click();

                }, 200);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }
    /*edit data*/


    $scope.editData = function(Position) {
        $scope.editDataLoading = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();
        $http.post(API_URL + 'Library/editLibrary', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;
                $timeout(function() {
                    $('#edits_model .close').click();
                }, 200);
            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });

    }

    $scope.editDatad = function(Position) {
        $scope.editDataLoading = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='editd_form']").serialize();
        $http.post(API_URL + 'Library/editLibraryd', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;
                 $timeout(function() {
                    $('#edits_model .close').click();
                 }, 200);
            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });

    } /*PermissionData data*/



    $scope.IssueBookData = function(Position) {

        $scope.PermissionDataLoading = true;

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='issuebook_form']").serialize();

        $http.post(API_URL + 'Library/AddIssueBook', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */
                //$scope.applyFilter();
                SuccessPopup(response.Message);

                window.location.href = window.location.href;
                /*$scope.data.dataList[$scope.data.Position] = response.Data;
                $timeout(function() {
                    $('.modal-header .close').click();
                }, 200);*/
            } else {
                ErrorPopup(response.Message);
            }
            $scope.PermissionDataLoading = false;
        });

    }

    $scope.ReturnBookData = function(Position) {

        $scope.PermissionDataLoading = true;

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='returnbook_form']").serialize();

        $http.post(API_URL + 'Library/EditReturnBook', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */
                //$scope.applyFilter();
                SuccessPopup(response.Message);

                window.location.href = window.location.href;

                /*$scope.data.dataList[$scope.data.Position] = response.Data;
                $timeout(function() {
                    $('.modal-header .close').click();
                }, 200);*/
            } else {
                ErrorPopup(response.Message);
            }
            $scope.PermissionDataLoading = false;
        });

    }


});



$("#add_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});

$("#edits_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});


function getFilterCategory(Category) {

    // alert(Category);

    //  angular.element(document.getElementById('content-body')).scope().getList(ParentCategoryGUID,CategoryGUID); 

    angular.element(document.getElementById('content-bodys')).scope().getList(1, Category, '');

    // angular.element(document.getElementById('content-bodys')).scope().getFilterData(Category);  

}

function getFilterAuthor(Author) {

    //alert(Category);
    var Category = $("#Category").val();
    angular.element(document.getElementById('content-bodys')).scope().getList(1, Category, Author);

}

function getFilterFormType(FormType) {
    // var FormType = $("#FormType").val();
    angular.element(document.getElementById('content-bodys')).scope().getList(1, '', '', FormType);
}


$(document).on('change', '#fileInputs', function() {
    var target = $(this).data('target');
    var croptarget = $(this).data('croptarget');

    var mediaGUID = $(this).data('targetinput');
    var progressBar = $('.progressBars'),
        bar = $('.progressBars .bars'),
        percent = $('.progressBars .percents');
    $(this).parent().ajaxForm({
        data: {
            SessionKey: SessionKey
        },
        dataType: 'json',
        beforeSend: function() {
            progressBar.fadeIn();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function(obj, statusText, xhr, $form) {
            if (obj.ResponseCode == 200) {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
                $(target).prop("src", obj.Data.MediaURL);
                //$("input[name='File']").val(obj.Data.MediaGUID);
                $(mediaGUID).val(obj.Data.MediaGUID);

                if (obj.Data.ImageCropper == "Yes") {
                    $(croptarget).show();

                }

            } else {
                ErrorPopup(obj.Message);
            }
        },
        complete: function(xhr) {
            progressBars.fadeOut();
            $('#fileInputs').val(mediaGUID);


        }
    }).submit();

});

$(document).on('change', '#fileInputAudio', function() {
    var target = $(this).data('target');
    var croptarget = $(this).data('croptarget');

    var mediaGUID = $(this).data('targetinput');
    var progressBar = $('.progressBarA'),
        bar = $('.progressBarA .bars'),
        percent = $('.progressBarA .percents');
    $(this).parent().ajaxForm({
        data: {
            SessionKey: SessionKey
        },
        dataType: 'json',
        beforeSend: function() {
            progressBar.fadeIn();
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function(obj, statusText, xhr, $form) {
            if (obj.ResponseCode == 200) {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
                $(target).prop("src", obj.Data.MediaURL);
                //$("input[name='File']").val(obj.Data.MediaGUID);
                $(mediaGUID).val(obj.Data.MediaGUID);

                if (obj.Data.ImageCropper == "Yes") {
                    $(croptarget).show();

                }

            } else {
                ErrorPopup(obj.Message);
            }
        },
        complete: function(xhr) {
            progressBarA.fadeOut();
            $('#fileInputAudio').val(mediaGUID);


        }
    }).submit();

});


function search_records()
{    
    var Category = $("#Category").val();
    var Author = $("#Author").val();
    var Keyword = $("#Keyword").val();
    var FormType = $("#FormType").val();

    angular.element(document.getElementById('content-bodys')).scope().getList(1, Category, Author, FormType, Keyword);
}


function clear_search_records()
{    
    $("#Category, #Author, #Keyword, #FormType").val("");    

    search_records();
}