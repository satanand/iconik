<style type="text/css">
.glyphicon {
    position: relative;
    top: -4px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    font-size: 20px;
    padding: 4px;
}
</style>
<header class="panel-heading" > <h1 class="h4">Manage Fee</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" id="fee-body" ng-controller="PageController" ng-init="getFilterData()"><!-- Body -->

	<!-- Top container -->
	<div class="row clearfix mt-2 mb-2">
		<div class="col-md-2">
			<span class="float-left records d-none d-sm-block">
				<span class="h5">Total records: {{data.dataList.length}}</span>
			</span>
		</div>
	</div>

	<div class="row clearfix mt-2 mb-2">	
		<div class="col-md-2">
		 <div class="form-group">
		    <select class="form-control" onchange="FilterAndGetSubCategoryList(this.value,'')" name="ParentCat" id="Courses">
		       <option value="{{data.ParentCategoryGUID}}">Select Stream</option>
		       <option ng-repeat="row in categoryDataList" value="{{row.CategoryGUID}}" ng-if="row.Status == 'Active'" ng-selected="row.CategoryGUID == ParentCategoryGUID">{{row.CategoryName}}</option>
		    </select>
		 </div>
		</div>
		<div class="col-md-2">
		 <div class="form-group">
		    <div id="subcategory_section">
		       <select id="subject" name="CategoryGUIDs" class="form-control">
		          <option value="">Select Course</option>
		          <option ng-repeat="row in CoursesDataList" value="{{row.CategoryGUID}}" ng-if="row.Status == 'Active'"  ng-selected="CategoryGUID == row.CategoryGUID">{{row.CategoryName}}</option>
		       </select>
		    </div>
		 </div>
		</div>

		<div class="col-md-4">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>		


	      <div class="col-md-4 float-right">
			<div class="float-right">
				<button class="btn btn-success btn-sm" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Fee</button>

				<button class="btn btn-success btn-sm" ng-click="loadFormFeeBreakup();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Manage Fee Breakup</button>
			</div>
		</div>
	</div>
	<!-- Top container/ -->
	<!-- Data table -->
	<div> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		<table class="table table-striped table-hover" ng-init="getList()">
			<!-- table heading -->
			<thead>
				<tr>
					
					<th style="width: 200px;">Stream</th>
					<th style="width: 100px;">Course</th>
					<th style="width: 100px;">Duration (Month)</th>
					<th style="width: 100px;">Amount (<i class="fa fa-rupee"></i>)</th>
					<th style="width: 150px;" class="text-center">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row" ng-repeat="(key, row) in data.dataList">
					<td>{{row.ParentCategoryName}}</td>
					<td>{{row.SubCategoryName}}</td>
					<td>
						<span ng-if="row.Feedata.length">{{row.Feedata[0].NoMonth}}</span>
						<span  ng-if="!row.Feedata.length">{{row.Duration}}</span>
					</td>
					<td>
						<span ng-if="row.Feedata.length && row.Feedata[0].FullAmount != ''">{{row.Feedata[0].FullAmount}}</span>
						<span ng-if="row.Feedata.length && row.Feedata[0].FullAmount == ''">0</span>
						<span  ng-if="!row.Feedata.length">0</span>
					</td>
					
		
					<td class="text-center" style="text-align: center;" ng-if="row.Feedata.length && row.Feedata[0].FullAmount != 0.00">
						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.Feedata[0].FeeGUID)" data-toggle="tooltip" title="View"></a>
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.Feedata[0].FeeGUID)" data-toggle="tooltip" title="Edit"></a>
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.Feedata[0].FeeGUID)" title="Delete"></a>						
					</td>
					<td class="text-center" style="text-align: center;" ng-if="!row.Feedata.length || row.Feedata[0].FullAmount == 0.00">
						<a ng-if="UserTypeID == 10 || UserTypeID == 1 || (data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" class="glyphicon glyphicon-plus" href="" ng-click="LoadSelectedCategoryFormAdd(row.parentCategoryGUID, row.CategoryGUID)" data-toggle="tooltip" title="Add fee for {{row.SubCategoryName}} course"></a>	
									
					</td>
			
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="data.noRecords">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->

	<!-- add Modal -->
	<div class="modal fade" id="add_model" ng-init="getFilterData()">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Course <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>



	<!-- edit Modal -->
	<div class="modal fade" id="edit_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Course <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLEdit"></div>
			</div>
		</div>
	</div>

	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Course <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>

	<!-- delete Modal -->
	<div class="modal fade" id="delete_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Delete Course <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div ng-include="templateURLDelete"></div>
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>


	<div class="modal fade" id="breakup_name_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Manage Breakup</h3>     	
					<button type="button" class="close" ng-click="showParentFee();" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLBreakup"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="fee_breakup_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Manage Fee Breakup</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLFeeBreakup"></div>
			</div>
		</div>
	</div>



</div><!-- Body/ -->