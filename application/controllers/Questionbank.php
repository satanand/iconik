<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Questionbank extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js?123',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',			

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Question Bank" => "",
			"Question Repository" => "",
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionBank/Questionbank_list');

		$this->load->view('includes/footer');

	}



	public function questions_list()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/questionbank.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Question Repository" => base_url()."questionbank",
			"Questions" => ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('questionBank/Questions_list');

		$this->load->view('includes/footer');

	}

}