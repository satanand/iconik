<link href='<?php echo base_url();?>asset/fullcalendar/css/fullcalendar.css' rel='stylesheet' />
<link href="<?php echo base_url();?>asset/fullcalendar/css/bootstrapValidator.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>asset/fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
<!-- Custom css  -->
<link href="<?php echo base_url();?>asset/fullcalendar/css/custom.css" rel="stylesheet" />
<script src='<?php echo base_url();?>asset/fullcalendar/js/moment.min.js'></script>
<script src="<?php echo base_url();?>asset/fullcalendar/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/fullcalendar/js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>asset/fullcalendar/js/fullcalendar.min.js"></script>
<script src='<?php echo base_url();?>asset/fullcalendar/js/bootstrap-colorpicker.min.js'></script>
<header class="panel-heading">
   <h1 class="h4">Manage Time Scheduling</h1>
</header>
<style type="text/css">
  .fc-day-number.fc-other-month{
    opacity: 1.3!important; 
  }
  .fc-day-number.fc-past{
    opacity: 0.3!important; 
  }
</style>
<?php $this->load->view('includes/breadcrumb'); ?>
<input type="hidden" id="FacultyIDs" value="">
<div >
<!-- Body -->
<div class="row"> 
  <div class="col-md-9 offset-md-1">
   <select class="form-control"  name="UserID" id='Facultys' style="width:250px">
      <option value="">Search Faculty</option>
      <option value="All">All Faculty</option>
   </select>
  </div>
  <div class="col-md-2">
    <button type="button" class="btn btn-info btn-sm ml-1" data-toggle="modal" data-target="#myModal">Note</button>
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="">Note</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>          
          </div>
          <div class="modal-body">
            <p>1. To schedule a Batch you first need to create Course, Faculty, Batch, Subject and need to link each other.</p>
            <p>2. You can schedule time for upcoming days & not past days.</p>
          </div>
          <div style="padding: 10px 15px 10px 15px;">
            <button style="float: right;" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>
<img src="asset/img/loader.svg" id="loader"  style="display:none; position: absolute; left: 50%;    transform: translate(-50%, -50%);">
<div class="container">   
   <!-- Notification -->
   <div class="alert"></div>
   <div class="row clearfix">
      <div class="col-md-12 column">
         <div id='calendar'></div>
      </div>
   </div>
</div>
<div class="modal fade" id="batchfaculty">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
         </div>
         <div class="modal-body">
            <!--  <div class="error"></div> -->
            <form class="form-horizontal" id="crud-form">
               <input type="hidden" id="start">
               <input type="hidden" id="end">
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label" for="title">Course <span class="text-danger">*</span></label>
                        <select id="CourseID" name="CourseID" class="form-control">
                           <option value="">Please Select</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label" for="title">Faculty <span class="text-danger">*</span></label>
                        <select id="FacultyID" name="FacultyID" class="form-control">
                           <option></option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label" for="title">Batch <span class="text-danger">*</span></label>
                        <select id="BatchID" name="BatchID" class="form-control">
                           <option> </option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label" for="title">Subject <span class="text-danger">*</span></label>
                        <select id="SubjectID" name="SubjectID" class="form-control">
                           <option> </option>
                        </select>
                     </div>
                  </div>
               </div>


               <!-- <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                     <label class="control-label" for="title">Subject <span class="text-danger">*</span></label>
                    <input type="text" name="title" id="title" value="" class="form-control" />
                  
                     </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                     <label class="control-label" for="color">Color</label>
                     
                      <div id="cp8" data-format="alias" class="input-group colorpicker-component">
                         <input type="text" name="color" id="color" value="primary" class="form-control" />
                         <span class="input-group-addon"><i style="width: 20px; height: 30px;"></i></span></div>
                         <span class="help-block">Click to pick a color</span>
                         
                  
                     </div>
                  </div>
                  
                  </div> -->

              <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label" for="title">Date <span id="text_danger" class="text-danger">*</span></label>
                        <input type="text" id="forDates" name="forDates" class="form-control input-md" placeholder="Select scheduling dates" readonly>
                        <span style="color:red; font-size: 10px;">Click outside the date window for closing it.</span>
                     </div>
                  </div>                 
               </div>    


               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label" for="title">Start Time <span class="text-danger">*</span></label>
                        <input type="text" id="inTime" name="StartTime" class="form-control input-md" placeholder="00:00">
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label" for="title">End Time <span class="text-danger">*</span></label>
                        <input type="text" id="outTime" name="EndTime" class="form-control input-md" placeholder="00:00">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="form-group">
                        <label class="control-label" for="description">Description</label>
                        <textarea class="form-control" id="description" name="description"></textarea>
                     </div>
                  </div>
            </form>
            </div>
            <div class="modal-footer">
               <button type="button" id="CancelBtn" class="btn btn-default" data-dismiss="modal">Cancel</button>

               <img src="./asset/img/ajax-loader.gif" id="addDataLoading"  style="position: absolute;    top: 90%;    left: 50%;    transform: translate(-50%, -50%); display: none;">
            </div>
         </div>
      </div>
   </div>
</div>

<link rel="stylesheet" type="text/css" href="./asset/plugins/multiple_dates_picker/jquery-ui.multidatespicker.css">
<script src="./asset/plugins/multiple_dates_picker/jquery-ui.multidatespicker.js"> </script>
<script type="text/javascript">
var multidate;
$(function()
{
    multidate = $("#forDates").multiDatesPicker({
      dateFormat: "dd-mm-yy",
      minDate: 0,
      numberOfMonths:3,
      
      onSelect: function() {
        $(this).data('datepicker').inline = true;                               
    },
    onClose: function() {
        $(this).data('datepicker').inline = false;
    }
    });
});  

function setMultiDate(d)
{
  $("#text_danger").show();

  multidate.multiDatesPicker('addDates', d);
}

function resetMultiDate(d)
{
  multidate.multiDatesPicker('resetDates');

  setMultiDate(d);
}  

function clearMultiDate()
{
  multidate.multiDatesPicker('resetDates');
  $("#text_danger").hide();
} 
</script>