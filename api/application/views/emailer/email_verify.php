<?php if(empty($Error)){?>
<h2 style="margin-bottom:20px; font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; ">Hi <?php echo $UserData['FirstName'];?>,</h2>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Your account has been successfully verified.</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Thank you for registering with <span><?php if(!empty($InstituteName)){ echo $InstituteName; }else { echo SITE_NAME; }?></span>.</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; "><a href="https://iconik.in/">Go to Home Page</a></p>

<?php }else{ ?>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; "><?php echo $Error ;?></p>

<?php } ?>