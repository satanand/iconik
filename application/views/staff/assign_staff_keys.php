<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<style>
#BtnTotalStudent {
    position: fixed;
    bottom: 20px;
    right: 135px;
    z-index: 99;
    font-size: 15px;
    border: none;
    outline: none;
    background-color: gray;
    color: white;
    cursor: pointer;
    padding: 10px;
    border-radius: 4px;
    top: 290px;
    height: 40px;
}

#myBtn:hover {
  background-color: #555;
}
</style>
<header class="panel-heading">
	<h1 class="h4">Assign Keys To Staff</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="staff-body" ng-init="getRoles()"><!-- Body -->

<div class="row clearfix mt-2 mb-2">
<div class="col-md-9">
	<span class="float-left records d-none d-sm-block">
		<span class="h5">Total Records: {{data.totalRecords}}</span>
	</span>
</div>
</div>

	<div class="clearfix mt-2 mb-2">
		<form id="filterForm" role="form" autocomplete="off" > <!--ng-submit="applyFilterStudents()" -->
			<input type="hidden" name="PageType" value="AssignKeys">
		   <div class="row" style="margin-left: 0px;">		   	  
		      <div class="col-md-2">
		         <div class="form-group">
		         	<label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label>
		            <select class="form-control"  name="UserTypeID" ng-model="role" id="UserTypeID" ng-model="UserTypeID">
						<option value="">Select Role</option>
						<option ng-repeat="rows in data.Roles" value="{{rows.UserTypeID}}" 	ng-selected="UserTypeID==rows.UserTypeID">{{rows.UserTypeName}}</option>
					</select>
		         </div>
		      </div>


		      <div class="col-md-2">
		      	<div class="form-group">
		      		<label class="filter-col" ></label>				 		
                    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Enter search word" style="" autocomplete="off">                        			 	
				</div>
			  </div>


		      <div class="col-md-2">
		         <div class="form-group">
		            <div id="subcategory_section">
		            	<label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label>
		               <select  id="Validity"  name="Validity" class="form-control" ng-model="Validity" ng-change="availableKeys(Validity)">

								<option value="">Select Validity</option>

								<option value="3">3 Month</option>

								<option value="6">6 Month</option>

								<option value="12">12 Month</option>

								<option value="24">24 Month</option>

						</select> 
		            </div>
		         </div>
		      </div>
		      <div class="col-md-2">

					<div class="form-group">

						<label class="filter-col" for="ParentCategory" id="select-label-parent-category" style="margin-top: -25px;">Available Keys</label>

						<input  type="hidden" class="form-control" value="{{availableData.AvailableKeys}}" id="availableKeys">

						<input  type="number" class="form-control availableKeys" value="{{availableData.AvailableKeys}}" id="availableKeysTxt" disabled="disabled" style="height: 40px;margin-top: 0px;width: 80px;">

					</div>

				</div>

				<div class="col-md-2">
					<div class="form-group">
						<label class="filter-col" ></label>	<br/>
						<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records_keys();"><?php echo SEARCH_BTN;?></button>&nbsp;
					<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records_keys();"><?php echo CLEAR_SEARCH_BTN;?></button>
					</div>
				</div>

		   </div>
	   </form>
	</div>

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">


	<!-- Data table -->
	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

	
		<!-- data table -->
		<p style="float: right; font-size: 11px; color: red;">You can assign key to faculty only.</p>
		<table class="table table-striped table-hover"  id="tableToExport">
			<!-- table heading -->
			<thead>
				<tr>
					<th style="width: 100px; text-align: center;">Staff</th>
					<th style="width: 100px; text-align: left;"><input type="checkbox" name="all value="all" id="checkAll" onchange="selectAllStudent()">&nbsp;&nbsp;Select All</th>
					<th style="width: 100px; text-align: center;">Key</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>				
				<tr scope="row" ng-repeat="(key, row) in data.dataList">
					<td style="text-align: center;">{{row.FirstName}} {{row.LastName}} ({{row.UserTypeName}})</td>
					
					<td style="text-align: left;">
						<input type="checkbox" name="UserGUID[]" value="{{row.UserGUID}}" class="SelectStudent" onchange="checkLength()" ng-if="row.UserTypeID == 11">

						<input type="checkbox" disabled="" ng-if="row.UserTypeID != 11">
					</td>
					
					<td style="text-align: left;"><img src="asset/img/loader.svg" ng-if="assignDataLoading" style="position: absolute;    top: 80%;    left: 50%;    transform: translate(-50%, -50%);">
						<div ng-show="$last"><a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0"  class="btn btn-success btn-sm ml-1" ng-disabled="assignDataLoading"  id="assignKeyBtn"  ng-click="AssignKey()">Assign Keys</a></div></td>
				</tr>
			</tbody>
		</table>
		<button id="BtnTotalStudent"  style="display: block;">Selected Staff 0</button>
		<!-- no record -->
		<p class="no-records text-center" ng-if="!data.dataList.length">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
</div><!-- Body/ -->

<script type="text/javascript">
	
</script>