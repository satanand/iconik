<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>

<header class="panel-heading">
	<h1 class="h4">My Orders</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="order-body"><!-- Body -->

	<div class="clearfix mt-2 mb-2">

		<div class="float-right mr-2">

	   		<a class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" href="<?php echo base_url(); ?>bulksms">Bulk SMS</a>

		</div>

	</div>





	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<span class="float-left records hidden-sm-down">

			<span ng-if="data.dataList.length" class="h5">Total records: {{data.dataList.length}}</span>

		</span>

	</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getSMSOrders(0,'buy_now')" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 150px;" >Order ID</th>

					<th style="width: 150px;">Order Date</th>

					<th style="width: 150px;" ng-if="UserTypeID==1">Ordered By</th>

					<!-- <th style="width: 80px;" class="text-center">Amount (<i class="fa fa-rupee"></i>)</th> -->

					

					<th style="width: 150px;" class="text-center">Payment  Method</th>

					<th >Payment Status</th>

					<th style="width: 150px;" class="text-center">Stock Received</th>

					<th style="width: 100px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row" ng-repeat="(key, row) in data.dataList">

					

					<td ng-if="row.OrderID.length == 1">{{row.OrderIDPrefix}}00{{row.OrderID}}</td>

					<td  ng-if="row.OrderID.length == 2">{{row.OrderIDPrefix}}0{{row.OrderID}}</td>

					<td ng-if="row.OrderID.length > 2">{{row.OrderIDPrefix}}{{row.OrderID.length}}</td>

					<td >{{row.EntryDate}}</td>

					<td ng-if="UserTypeID==1">{{row.FirstName}} {{row.LastName}}</td>

					<!-- <td class="text-center">{{row.OrderPrice}}</td> -->
					

					
					<td class="text-center">{{row.PaymentMethod}}</td>

					<td ng-if="row.OrderStatus == 'Offline' && row.PaymentStatus == 'Unpaid'">Partly Payment ({{row.TotalPaidAmount}})&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a></td>

					<td ng-if="row.OrderStatus == 'Offline' && row.PaymentStatus == 'Paid'">Paid ({{row.TotalPaidAmount}})</td>

					<td ng-if="row.OrderStatus == 'Online'">Paid ({{row.TotalPaidAmount}})</td>

					<td ng-if="row.OrderStatus == ''">Unpaid ({{row.OrderPrice}})&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a></td>
					
					<!-- <td class="text-center" ng-if="row.StatusID == 1 && row.PaymentStatus == 'Unpaid'">Unpaid <button class="btn btn-success btn-sm ml-3 float-right ng-scope" ng-click="payNow(key,row.OrderGUID)">Pay Now</button></td>
 -->

					<td class="text-center" ng-if="row.StatusID==2 && row.PaymentStatus == 'Paid'">Paid</td>

					<td class="text-center">No</td>
					
<!-- 
					<td class="text-center"><span ng-class="{Pending:'text-danger', Delivered:'text-success',Deleted:'text-danger',Blocked:'text-danger'}[row.Status]">{{row.Status}}</span></td>  -->

					<td class="text-center">

						<a class="glyphicon glyphicon-eye-open" ng-click="loadFormView(key, row.OrderGUID)"  data-toggle="tooltip" title="view order"></a>
<!-- 
						<div class="dropdown">

							<button class="btn btn-secondary  btn-sm action" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8230;</button>

							<div class="dropdown-menu dropdown-menu-left">

								<a class="dropdown-item" href="" ng-click="loadFormView(key, row.OrderGUID)">View</a>

							</div>

						</div> -->

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->



	<div id="myModal" class="modal fade myModal{{key+1}}">
        <div class="modal-dialog modal-lg">              
          <div class="modal-content" style="top:100px;">

            <div class="modal-header">
             <!--  <button type="button" class="close" ng-click="loadFormView(key,POrderGUID)"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>  -->
             
             <h3 class="modal-title h5">Deposite Slip</h3>     	
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="loadFormView(key,POrderGUID)"><span aria-hidden="true">&times;</span></button>                 
            </div>

            <div class="modal-body">
            	<img  src="{{MediaURL}}" class="img-responsive" style="height: 500px; width:500px;">
            </div>
          </div>
        </div>
    </div>

	<div class="modal fade" id="add_model">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Order</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_payment">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Make Payment</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLPayment"></div>

			</div>

		</div>

	</div>



	<!-- view Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Order</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<div ng-include="templateURLView"></div>

				<!-- /form -->

			</div>

		</div>

	</div>

</div><!-- Body/ -->