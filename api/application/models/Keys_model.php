<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Keys_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}
	


	function random_strings($length_of_string) { 

	    // String of all alphanumeric character 
		$str_result = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz'; 

	    // Shufle the $str_result and returns substring 
	    // of specified length 
		return substr(str_shuffle($str_result), 0, $length_of_string); 
	} 



	function updateAllotedKeys($EntityID,$Input=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$validity = array();
		foreach ($Input['Keys'] as $key => $value) {	
			//echo $key;
			if($key == 0){
				$limitFrom = 0;
				$limitTo = $value;
			}else{
				$limitFrom = $limitTo+$limitFrom;
				$limitTo = $value;
			}

			//echo $limitFrom; die;
			$this->db->select('KeyID');	
			$this->db->from('set_keys');
			$this->db->where('AssignedInstitute',$InstituteID);
			$this->db->where('UsedStatus IS NULL');			
			$this->db->limit($limitTo, $limitFrom);
			$query = $this->db->get();
			$KeyID = $query->result_array();
			//echo $this->db->last_query();
			//print_r($KeyID);
			if(count($KeyID) > 0){
				$KeyID = array_column($KeyID, 'KeyID');
				//echo count($KeyID); die;
				$KeyID = implode(',', $KeyID);
				//print_r($KeyID); die;
				//echo "UPDATE set_keys SET Validity = ".$Input['Validity'][$key]." WHERE KeyID  IN ('".$KeyID."')"; die;
				$this->db->query("UPDATE set_keys SET Validity = ".$Input['Validity'][$key]." WHERE KeyID IN (".$KeyID.")");
			}
			
			
		}

		return TRUE;
	}


	/* to generate key */
	function permute($v,$l,$count,$c="23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz"){
		$j=0; $data1 = array();
		for($t='',$cl=strlen($c),$s=array_fill(0,$l,0),$i=pow($cl,$l);$a=0,$i--;) {

			for($t='';$a<$l;$t.=$c[$s[$a++]]);
				for(;$a--&&++$s[$a]==$cl;$s[$a]=0);

					$this->db->select('KeyName');
				$this->db->from('set_keys');
				$this->db->where('KeyName',$v.''.$this->random_strings(4).''.$t);
				$this->db->limit(1);
				$Query = $this->db->get();
			//echo $this->db->last_query(); die;
				if($Query->num_rows() == 0){
					$key = $v.''.$this->random_strings(4).''.$t;
					array_push($data1, $key);
					if($j==$count){ return $data1; break; }
					$j++; 
				}		    

			};
		}


		function checkKeyStatus($UserID,$Input){
			$this->db->select('KeyStatusID');
			$this->db->from('tbl_students');
			$this->db->where('Key',$Input['Key']);
			$this->db->where('StudentID',$UserID);
			$Query = $this->db->get();
		//echo $this->db->query(); die;
			if($Query->num_rows()>0){
				return $StatusID = $Query->result_array();
			}else{
				return FALSE;
			}
		}



		function activateKey($UserID,$Input){
			$this->db->select("Key");
			if($Input['UserTypeID'] == 7){
				$this->db->from("tbl_students");
			}else{
				$this->db->from("tbl_staff_assign_keys");
			}		
			$this->db->where("Key",$Input['Key']);
			$Query = $this->db->get();
			if($Query->num_rows()>0){
				$date = date("Y-m-d H:i:s");
				$date = strtotime(date("Y-m-d H:i:s", strtotime($date)) . " +".$Input['Validity']." month");
				$ExpiredOn = date("Y-m-d H:i:s",$date);

				if($Input['UserTypeID'] == 7)
				{
					$this->db->where("StudentID",$UserID);
					$this->db->where("Key",$Input['Key']);
					$this->db->where("ExpiredOn is NULL");
					$this->db->update("tbl_students",array("ActivatedOn"=>date("Y-m-d H:i:s"),"ExpiredOn"=>$ExpiredOn,"KeyStatusID"=>2));
					$afftectedRows = $this->db->affected_rows();

					$StudentData = $this->Common_model->getInstituteDetailByID($UserID,"Email,UserID,FirstName,LastName");

					$this->db->select("UserID");
					$this->db->from("tbl_users_login");
					$this->db->where("SourceID = 1");
					$this->db->where("Password != '' OR Password IS NOT NULL");
					$this->db->where("UserID = ".$UserID);
					$this->db->limit("1");
					$query = $this->db->get();
					if($query->num_rows() > 0)
					{

						/*$password = $this->Users_model->random_strings(6);
						$passwordMD5 = md5($password);
						$sql = "UPDATE tbl_users_login SET Password = '$passwordMD5' WHERE UserID = '$UserID' AND SourceID = 1 LIMIT 1";
						$this->Users_model->updateBySql($sql);
						$Password = "Password: $password";*/
						$Password = "Use your current password for login.";
					}	
					else
					{
						$Password = $this->random_strings(5);
						$UpdateArray = array_filter(array(
							"Password" 	=>	md5($Password),
							"EntryDate" =>	date("Y-m-d H:i:s"),
							"UserID"  => $UserID,
							"SourceID" => 1
						));
						$this->db->insert('tbl_users_login', $UpdateArray);
					}	


					$this->load->model('Recovery_model');
					$content = $this->load->view('emailer/adduser',array("Name" =>  
						$StudentData['FirstName'].' '.$StudentData['LastName'], 'Password' => $Password, "Email"=>$StudentData['Email'], "Type" => "Student"),TRUE);

					sendMail(array(
						'emailTo' 		=> $StudentData['Email'],			
						'emailSubject'	=> $StudentData['FirstName']." ".$StudentData['LastName']."  Welcome to Iconik.",
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
					));

				}else{
					$this->db->where("StaffID",$UserID);
					$this->db->where("Key",$Input['Key']);
					$this->db->where("ExpiredOn is NULL");
					$this->db->update("tbl_staff_assign_keys",array("ActivatedOn"=>date("Y-m-d H:i:s"),"ExpiredOn"=>$ExpiredOn,"KeyStatusID"=>2));
					$afftectedRows = $this->db->affected_rows();
				}

				if($afftectedRows){
					return array("ActivatedOn"=>date("Y-m-d H:i:s"),"ExpiredOn"=>$ExpiredOn);
				}else{
					return "Already activated";
				}
			}else{
				return FALSE;
			}
		}


		function addKeys($EntityID,$Input){
			$this->db->trans_start();
			if(!empty($Input['KeyCount'])){
				$permuted_str = $this->permute($Input['Validity'],3,$Input['KeyCount']);

				for ($i=0; $i < $Input['KeyCount']; $i++) { 

					$InsertData = array_filter(array(
						"KeyName" 		=>	$permuted_str[$i],
						"Validity" 		=>	$Input['Validity'],	
						"EntryDate" 	=>	date("Y-m-d H:i:s"),
						"CreatedByUser" => $EntityID,
						"UsedStatus" => 0
					));

				//print_r($InsertData); die();

					$this->db->insert('set_keys', $InsertData);

				//echo $this->db->last_query(); die;
				}
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return TRUE;
		}


		function availableKeys($EntityID,$validity=""){
		//print_r($validity);
			if(empty($validity)){
				$validity = array(3,6,9,12,24);
			}else{
				$validity = array($validity);
			}

			/*Additional fields to select*/
			$query = $this->db->query("select UserTypeID from tbl_users where UserID = ".$EntityID);
			$Users = $query->result_array();
			$UserTypeID = $Users[0]['UserTypeID'];

			if($UserTypeID == 1){
				foreach ($validity as $key => $value) {
				//echo "select count(KeyName) as KeyName from set_keys where CreatedByUser = ".$EntityID." and Validity = ".$value." and AssignedInstitute IS NULL";
					$query = $this->db->query("select count(KeyName) as KeyName from set_keys where CreatedByUser = ".$EntityID." and Validity = ".$value." and AssignedInstitute IS NULL");
					$keys = $query->result_array();
					$data[$key]['Validity'] = $value;
					$data[$key]['AvailableKeys'] = $keys[0]['KeyName'];
				}
			}else if($UserTypeID == 10){
				foreach ($validity as $key => $value) {
					$query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute like '%".$EntityID."%' and Validity = ".$value." and UsedStatus IS NULL");
					$keys = $query->result_array();
					$data[$key]['Validity'] = $value;
					$data[$key]['AvailableKeys'] = $keys[0]['KeyName'];
				}
			}
			return $data;
		}



		function allotKeys($EntityID,$Input,$UserID){
			$this->db->trans_start();
			if(!empty($Input['KeyCount'])){		
				$query = $this->db->query("select KeyID from set_keys where Validity = ".$Input['Validity']." and AssignedInstitute IS NULL ORDER BY RAND() limit ".$Input['KeyCount']);
				$keysID = $query->result_array();
			//print_r($keysID); die;
				if(count($keysID) > 0){
					foreach ($keysID as $key => $value) {
						$updateArr = array("AssignedInstitute"=>$UserID);
						$this->db->where("KeyID",$value['KeyID']);
						$this->db->update("set_keys",$updateArr);
					}
				}
			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return TRUE;
		}






		function allotKeysToMasterFranchisee($EntityID,$UserID){
			$this->db->trans_start();
			$Validity = array(03,06,12,24); 
			$this->db->select("TotalKeys");
			$this->db->from("set_franchisee_default_keys");
			$this->db->order_by("DefaultKeysID");
			$this->db->limit("1");
			$query = $this->db->get();
			if($query->num_rows() > 0){
			$keysCount = $query->result_array(); //echo print_r($keysCount);
			$KeysPerValidity = $keysCount[0]['TotalKeys']/4;
			foreach ($Validity as $key => $value) {
				$permuted_str = $this->permute($value,3,$KeysPerValidity);
				for ($i=0; $i < $KeysPerValidity; $i++) { 

					$InsertData = array_filter(array(
						"KeyName" 		=>	$permuted_str[$i],
						"Validity" 		=>	$value,	
						"EntryDate" 	=>	date("Y-m-d H:i:s"),
						"CreatedByUser" => $EntityID,
						"UsedStatus" => 0,
						"AssignedInstitute" => $UserID
					));

					$this->db->insert('set_keys', $InsertData);
				}
			}
		}
		//if(!empty($Input['KeyCount'])){		

		//}
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}



	function setFranchiseeDefaultKeys($EntityID,$TotalKeys){
		if(!empty($TotalKeys)){
			$this->db->insert("set_franchisee_default_keys",array('TotalKeys'=>$TotalKeys,'EntryDate'=>date("Y-m-d H:i:s")));
		}else{
			return FALSE;
		}
	}




	function assignKeysToStudent($EntityID,$UserGUID,$Validity,$AvailableKeys){		
		$UserGUID = json_decode($UserGUID,TRUE);
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		//print_r($UserGUID); die;
		if(!empty($UserGUID)){
			foreach ($UserGUID as $key => $value) {

				//echo $value; die;

				$UserID = $this->Common_model->getUserIdByGUID($value);
				$StudentEmail = $this->Common_model->getUserEmailByID($UserID);
				$UserTypeID = $this->Common_model->getUserTypeByEntityID($UserID);
				$InstituteEmail = $this->Common_model->getUserEmailByID($InstituteID);


				//echo $this->UserID; die;
				//echo "select KeyName,KeyID,Validity from set_keys where AssignedInstitute = ".$InstituteID." and Validity = ".$Validity." and UsedStatus IS NULL limit 1"; die;
				$query = $this->db->query("select KeyName,KeyID,Validity from set_keys where AssignedInstitute = ".$InstituteID." and Validity = ".$Validity." and (UsedStatus IS NULL OR RenewStatus = 2)  limit 1");

				$Keydata = $query->row_array();

				//print_r($KeyName); die;
				if(!empty($Keydata)){
					$KeyName = $Keydata['KeyName'];

					if(!empty($KeyName)){

						

						if($UserTypeID == 7){
							$updateArr = array("Key"=>$KeyName,"Validity"=>$Keydata['Validity'],"KeyStatusID"=>1,"KeyAssignedOn"=>date("Y-m-d H:i:s"));
							$StudentID = $this->Common_model->getUserIdByGUID($value);
							$this->db->where("StudentID",$StudentID);
							$this->db->update("tbl_students",$updateArr);
						}else{
							$StaffID = $this->Common_model->getUserIdByGUID($value);
							$InsertArr = array("StaffID"=>$StaffID,"StaffGUID"=>$value,"Key"=>$KeyName,"Validity"=>$Keydata['Validity'],"KeyStatusID"=>1,"KeyAssignedOn"=>date("Y-m-d H:i:s"));
							$this->db->insert("tbl_staff_assign_keys",$InsertArr);
						}
						
						//echo $this->db->last_query(); die;

						$updateArr = array("UsedStatus"=>1);
						$this->db->where("KeyID",$Keydata['KeyID']);
						$this->db->update("set_keys",$updateArr);

						$this->db->select("UserID");
						$this->db->from("tbl_users_login");
						$this->db->where("UserID", $UserID);
						$this->db->limit(1);
						$userLogin = $this->db->get();
						//echo $this->db->last_query(); echo $userLogin->num_rows(); die;

						if($userLogin->num_rows() > 0){
							if($UserTypeID != 7){
								$InsertDataa = array_filter(array(
									"UserID" 		=> $UserID,
									"Password"		=> md5($KeyName),
									"SourceID"		=> 6,
									"EntryDate"		=> date("Y-m-d H:i:s")));
								$this->db->insert('tbl_users_login', $InsertDataa);
							}else{
								
								$skey = md5($KeyName);
								$InsertDataa = array_filter(array(
									"Password"		=> $skey,
									"SourceID"		=> 6,
									"EntryDate"		=> date("Y-m-d H:i:s")));
								$this->db->where("UserID", $UserID);
								$this->db->update('tbl_users_login', $InsertDataa);


								//Parent Key Assign-------------------------
								if($UserTypeID == 7 || $UserTypeID == 88)
								{
									$pkey = md5("P#".$KeyName);
									$InsertDataa = array_filter(array(
										"Password"		=> $pkey,
										"SourceID"		=> 6,
										"EntryDate"		=> date("Y-m-d H:i:s")));
									$this->db->where_not_in("Password", array($skey));
									$this->db->where("UserID", $UserID);
									$this->db->update('tbl_users_login', $InsertDataa);
								}	
							}
							
							//echo $this->db->last_query(); die;
						}else{
							$InsertDataa = array_filter(array(
								"UserID" 		=> $UserID,
								"Password"		=> md5($KeyName),
								"SourceID"		=> 6,
								"EntryDate"		=> date("Y-m-d H:i:s")));
							$this->db->insert('tbl_users_login', $InsertDataa);

							
							//Parent Key Assign-------------------------
							if($UserTypeID == 7 || $UserTypeID == 88)
							{
								$InsertDataa = array_filter(array(
									"UserID" 		=> $UserID,
									"Password"		=> md5("P#".$KeyName),
									"SourceID"		=> 6,
									"EntryDate"		=> date("Y-m-d H:i:s")));
								$this->db->insert('tbl_users_login', $InsertDataa);
							}	
						}	

						$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
						$Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

						if(!empty($Record['InstituteProfilePic'])){
							$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
						}else{
							$MediaURL= "";
						}			

						
					    //Use the key below to activate your account on Iconik Mobile App.  App Key: 
						//$EmailText = "A key is assigned to you, using this key you will be able to access the Iconik APP. Use the key ".base64_encode($KeyName). " to continue with Iconik APP";

						$EmailText = "A key is assigned to you, using this key you will be able to access the Iconik APP. Use the key below to activate your account on Iconik Mobile App.<br>App Key ".$KeyName;

						$content = $this->load->view('emailer/assign_key',array("Name" => 'There', 'EmailText' => $EmailText, "InstituteProfilePic"=>@$MediaURL,'Signature' => "Thanks and Regards, <br>".$InstituteData['FirstName']),TRUE);

						$SendMail = sendMail(array(

							"From_name"  => $InstituteData['FirstName'],

							'emailTo' 		=> $StudentEmail,

							'emailSubject'	=> "App Key",

							'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

						));


						//Send email to parent-------------------------------------------
						if($UserTypeID == 7 || $UserTypeID == 88)
						{
							$this->db->select("FathersName,FathersEmail,FathersPhone");
							$this->db->from("tbl_students");
							$this->db->where("StudentID", $UserID);
							$this->db->limit(1);
							$query = $this->db->get();
							$StudentParentDetails = $query->result_array();

							$EmailText = "A key is assigned to you, using this key you will be able to access the Iconik APP. Use the key below to activate your account on Iconik Mobile App.<br>App Key P#".$KeyName;

							$content = $this->load->view('emailer/assign_key',array("Name" => $StudentParentDetails[0]['FathersName'], 'EmailText' => $EmailText, "InstituteProfilePic"=>@$MediaURL,'Signature' => "Thanks and Regards, <br>".$InstituteData['FirstName']),TRUE);

							$SendMail = sendMail(array(
								"From_name"  => $InstituteData['FirstName'],
								'emailTo' 		=> $StudentParentDetails[0]['FathersEmail'],
								'emailSubject'	=> "App Key",
								'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
							));
						}



						if($key+1 == count($UserGUID)){
							$total = $key+1;
							$EmailText = "The Keys are assigned to ".$total." users successfully...";
							
							$content = $this->load->view('emailer/assign_key',array("Name" => $InstituteData['FirstName'], 'EmailText' => $EmailText, 'Signature' => ""),TRUE);

							$SendMail = sendMail(array(
								'emailTo' 		=> $InstituteEmail,

								'emailSubject'	=> "App Keys Assigned",

								'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
							));

							return $key+1;
						}

					}else{
						return $key+1;
					}
				}else{
					return FALSE;
				}
			}
		}
	}

	

	
	/*
	Description: 	Use to get Cetegories
	*/
	function getKeysStatisctics($EntityID){
		$validity = array(3,6,12,24);
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		/*Additional fields to select*/
		$query = $this->db->query("select UserTypeID from tbl_users where UserID = ".$EntityID);
		$Users = $query->result_array();
		$UserTypeID = $Users[0]['UserTypeID'];
		$data[0]['UserTypeID'] = $Users[0]['UserTypeID'];
		if($UserTypeID == 1){
			foreach ($validity as $key => $value) {
				$query = $this->db->query("select count(KeyName) as KeyName from set_keys where  Validity = ".$value);
				$keys = $query->result_array();
				$data[$key]['Validity'] = $value;
				$data[$key]['TotalKeys'] = $keys[0]['KeyName'];			
				$query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute IS NOT NULL  and Validity = ".$value);
				$keys = $query->result_array();
				$data[$key]['UsedKeys'] = $keys[0]['KeyName'];
				$data[$key]['AvailableKeys'] = $data[$key]['TotalKeys']-$data[$key]['UsedKeys'];
			}
		}else{
			foreach ($validity as $key => $value) {
				$query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute = ".$InstituteID." and Validity = ".$value);
				$keys = $query->result_array();
				$data[$key]['Validity'] = $value;
				$data[$key]['TotalKeys'] = $keys[0]['KeyName'];			
				$query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute = ".$InstituteID." and UsedStatus = 1 and Validity = ".$value);
				//echo $this->db->last_query(); die;
				$keys = $query->result_array();
				$data[$key]['UsedKeys'] = $keys[0]['KeyName'];
				$data[$key]['AvailableKeys'] = $data[$key]['TotalKeys']-$data[$key]['UsedKeys'];
			}
		}

		$new_data = array();

		$new_data['records'] = $data;

		$AvailableKeys = array_column($data, 'AvailableKeys');
		$new_data['TotalAvailableKeys'] = array_sum($AvailableKeys);

		$AvailableKeys = array_filter(array_column($data, 'AvailableKeys'));
		if(empty($AvailableKeys)){
			$new_data['Allot_keys'] = 0;
		}else{
			$new_data['Allot_keys'] = 1;
		}
		return $new_data;
	}


	function getStudents($EntityID,$Where="",$multiRecords=FALSE,$PageNo=1, $PageSize=10){
		//print_r($Where); die;

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select("tbl_entity.StatusID,tbl_users.UserGUID,tbl_users.FirstName,tbl_users.LastName,tbl_students.Email,tbl_students.PhoneNumber,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.InstallmentID");
		$this->db->from("tbl_students");
		$this->db->join("tbl_users","tbl_students.StudentID = tbl_users.UserID");
		$this->db->join("tbl_entity","tbl_users.UserID = tbl_entity.EntityID");

		if(!empty($InstituteID)){
			$this->db->where("tbl_entity.InstituteID",$InstituteID);
		}

		if(!empty($Where['CourseID'])){
			$this->db->where("tbl_students.CourseID",$Where['CourseID']);
		}

		if(!empty($Where['BatchID'])){
			$this->db->where("tbl_students.BatchID",$Where['BatchID']);
		}
		//echo "InstituteID=".$InstituteID;
		$this->db->where("tbl_entity.InstituteID",$InstituteID);

		//$this->db->where("tbl_entity.StatusID",2);

		//$this->db->where("tbl_students.StatusID",2);

		//$this->db->where("tbl_students.Key = ''");


		$this->db->group_start();
		$this->db->where("tbl_students.Key = '' OR tbl_students.Key IS NULL");
		$this->db->or_where("tbl_students.KeyStatusID",0);
		$this->db->or_where("tbl_students.KeyStatusID",6);
		$this->db->group_end();

		//echo $Where['Keyword'];

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			if(validateEmail($Where['Keyword'])){
				$Where['Email'] = $Where['Keyword'];
			}
			elseif(is_numeric($where['Keyword'])){
				$Where['PhoneNumber'] = $Where['Keyword'];
			}
			else{
				$this->db->group_start();
				$this->db->like("tbl_users.FirstName", trim($Where['Keyword']));
				$this->db->or_like("tbl_users.LastName", trim($Where['Keyword']));
				$this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
				$this->db->or_like("CONCAT_WS(' ',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
				$this->db->group_end();
			}

		}

		if(!empty($Where['Email'])){
			$this->db->where("tbl_users.Email",$Where['Email']);
		}

		if(!empty($Where['PhoneNumber'])){
			$this->db->where("tbl_users.PhoneNumber",$Where['PhoneNumber']);
		}

		
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$query = $this->db->get();

		//echo $this->db->last_query(); die;

		if($query->num_rows() > 0){
			$StudentData = $query->result_array();
			$Records = array();
			if(!empty($StudentData)){
				foreach ($StudentData as $record) {
					if(!empty($record['FeeID'])){
						$query = $this->db->query("select FullDiscountFee from tbl_setting_fee where FeeID = ".$record['FeeID']);
						$FeeData = $query->result_array();
						$record['FullDiscountFee'] = $FeeData[0]['FullDiscountFee'];
					}else{
						$record['FullDiscountFee'] = 0;
					}
					

					if(!empty($record['InstallmentID'])){
						//echo "select TotalFee,InstallmentAmount,NoOfInstallment from tbl_setting_installment where InstallmentID = ".$record['InstallmentID'];
						$query = $this->db->query("select TotalFee,InstallmentAmount,NoOfInstallment from tbl_setting_installment where InstallmentID = ".$record['InstallmentID']);
						$InstallmentData = $query->result_array();					
					}

					if(!empty($InstallmentData)){
						$record['TotalFee'] = $InstallmentData[0]['TotalFee'];
						$record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
						$record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
					}else{
						$record['TotalFee'] = '-';
						$record['InstallmentAmount'] = '-';
						$record['NoOfInstallment'] = '-';
					}

					//print_r($record);
					array_push($Records, $record);
				}
				return $Records;
			}else{
				return $Records;
			}
		}
		
	}



	function UsedKeysDetail($EntityID,$Where=array(),$multiRecords=FALSE,$PageNo=1, $PageSize=10){

		//print_r($Where); die;
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('S.KeyAssignedOn,S.Key,S.Validity,S.KeyStatusID,S.ActivatedOn,S.ExpiredOn,U.UserGUID,U.FirstName,U.LastName,B.BatchName,C.CategoryName');

		$this->db->from("tbl_students S");
		$this->db->join("tbl_users U","S.StudentID = U.UserID",'left');
		$this->db->join("tbl_entity E","S.StudentID = E.EntityID",'left');
		$this->db->join("tbl_batch B","S.BatchID = B.BatchID",'left');
		$this->db->join("set_categories C","S.CourseID = C.CategoryID",'left');
		//$this->db->where("K.AssignedInstitute",$InstituteID);
		$this->db->where("S.Key IS NOT NULL");

		// $this->db->from('tbl_students S');
		// $this->db->from('tbl_users U');
		// $this->db->from('set_keys K');
		// $this->db->from('tbl_batch B');
		// $this->db->from('set_categories C');

		// $this->db->where("S.StudentID","U.UserID", FALSE);
		// $this->db->where("S.BatchID","B.BatchID", FALSE);
		// $this->db->where("S.CourseID","C.CategoryID", FALSE);

		$this->db->where("E.InstituteID",$InstituteID);

		if(!empty($Where['Validity'])){
			$this->db->where("S.Validity",$Where['Validity']);
		}

		if(!empty($Where['BatchID'])){
			$this->db->where("S.BatchID",$Where['BatchID']);
		}

		if(!empty($Where['CategoryID'])){
			$this->db->where("S.CourseID",$Where['CategoryID']);
		}

		if(!empty($Where['Keyword']))
		{
			$this->db->group_start();
			$this->db->like("U.FirstName", trim($Where['Keyword']));
			$this->db->or_like("U.LastName", trim($Where['Keyword']));
			$this->db->or_like("CONCAT_WS(' ',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
			$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
			$this->db->or_like("S.Key",$Where['Keyword']);
			$this->db->or_like("U.Email",$Where['Keyword']);
			$this->db->or_like("U.PhoneNumber",$Where['Keyword']);
			$this->db->or_like("B.BatchName",$Where['Keyword']);
			$this->db->or_like("C.CategoryName",$Where['Keyword']);						
			$this->db->or_where("S.ActivatedOn",$Where['Keyword']);
			$this->db->or_where("S.ExpiredOn",$Where['Keyword']);
			$this->db->or_where("S.KeyAssignedOn",$Where['Keyword']);
			$this->db->or_where("S.Validity",$Where['Keyword']);
			if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
				$this->db->or_where("S.KeyStatusID",1);
			}
			if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
				$this->db->or_where("S.KeyStatusID",2);
			}
			if( strpos( $Where['Keyword'], "Not Assigned" ) !== false || strpos( $Where['Keyword'], "not assigned" ) !== false) {
				$this->db->or_where("S.KeyStatusID",0);
			}
			$this->db->group_end();
		}


		$this->db->order_by('S.KeyAssignedOn','DESC');
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get(); //echo $this->db->last_query();
		$StudentData = $Query->result_array();		
		$Records = array();
		if(!empty($StudentData)){ $i=0;
			foreach ($Query->result_array() as $record) {

				if(!empty($record['KeyAssignedOn']))	{
					$record['KeyAssignedOn'] = date("d-m-Y h:i:s", strtotime($record['KeyAssignedOn']));
				}

				if(!empty($record['ActivatedOn']))	{
					$record['ActivatedOn'] = date("d-m-Y h:i:s", strtotime($record['ActivatedOn']));
				}

				if(!empty($record['ExpiredOn']))	{
					$record['ExpiredOn'] = date("d-m-Y h:i:s", strtotime($record['ExpiredOn']));
				}

				$record['Type'] = "Student";

				array_push($Records, $record); 
			}
			return $Records;
		}else{
			return $Records;
		}

	}


	function UsedStaffKeysDetail($EntityID,$Where=array(),$multiRecords=FALSE,$PageNo=1, $PageSize=10){

		//print_r($Where); die;
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('S.KeyAssignedOn,S.Key,S.Validity,S.KeyStatusID,S.ActivatedOn,S.ExpiredOn,U.UserGUID,U.FirstName,U.LastName,B.BatchName,C.CategoryName');

		$this->db->from("tbl_staff_assign_keys S");
		$this->db->join("tbl_users U","S.StaffID = U.UserID",'left');
		$this->db->join("tbl_entity E","S.StaffID = E.EntityID",'left');
		$this->db->join("tbl_batchbyfaculty BF","S.StaffID = BF.FacultyID",'left');
		$this->db->join("tbl_batch B","BF.BatchID = B.BatchID");
		$this->db->join("set_categories C","B.CourseID = C.CategoryID",'left');
		//$this->db->where("K.AssignedInstitute",$InstituteID);
		$this->db->where("S.Key IS NOT NULL");

		// $this->db->from('tbl_students S');
		// $this->db->from('tbl_users U');
		// $this->db->from('set_keys K');
		// $this->db->from('tbl_batch B');
		// $this->db->from('set_categories C');

		$this->db->where("S.StaffID","U.UserID", FALSE);
		//$this->db->where("S.BatchID","B.BatchID", FALSE);
		//$this->db->where("S.CourseID","C.CategoryID", FALSE);

		$this->db->where("E.InstituteID",$InstituteID);

		if(!empty($Where['Validity'])){
			$this->db->where("S.Validity",$Where['Validity']);
		}

		if(!empty($Where['BatchID'])){
			$this->db->where("B.BatchID",$Where['BatchID']);
		}

		if(!empty($Where['CategoryID'])){
			$this->db->where("B.CourseID",$Where['CategoryID']);
		}

		if(!empty($Where['Keyword']))
		{
			$this->db->group_start();
			$this->db->like("U.FirstName", trim($Where['Keyword']));
			$this->db->or_like("U.LastName", trim($Where['Keyword']));
			$this->db->or_like("CONCAT_WS(' ',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
			$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
			$this->db->or_like("S.Key",$Where['Keyword']);
			$this->db->or_like("U.Email",$Where['Keyword']);
			$this->db->or_like("U.PhoneNumber",$Where['Keyword']);
			$this->db->or_like("B.BatchName",$Where['Keyword']);
			$this->db->or_like("C.CategoryName",$Where['Keyword']);						
			$this->db->or_where("S.ActivatedOn",$Where['Keyword']);
			$this->db->or_where("S.ExpiredOn",$Where['Keyword']);
			$this->db->or_where("S.KeyAssignedOn",$Where['Keyword']);
			$this->db->or_where("S.Validity",$Where['Keyword']);
			if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
				$this->db->or_where("S.KeyStatusID",1);
			}
			if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
				$this->db->or_where("S.KeyStatusID",2);
			}
			if( strpos( $Where['Keyword'], "Not Assigned" ) !== false || strpos( $Where['Keyword'], "not assigned" ) !== false) {
				$this->db->or_where("S.KeyStatusID",0);
			}
			$this->db->group_end();
		}


		$this->db->order_by('S.KeyAssignedOn','DESC');
		$this->db->group_by('U.UserID');
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get(); //echo $this->db->last_query();
		$StudentData = $Query->result_array();		
		$Records = array();
		if(!empty($StudentData)){ $i=0;
			foreach ($Query->result_array() as $record) {

				if(!empty($record['KeyAssignedOn']))	{
					$record['KeyAssignedOn'] = date("d-m-Y h:i:s", strtotime($record['KeyAssignedOn']));
				}

				if(!empty($record['ActivatedOn']))	{
					$record['ActivatedOn'] = date("d-m-Y h:i:s", strtotime($record['ActivatedOn']));
				}

				if(!empty($record['ExpiredOn']))	{
					$record['ExpiredOn'] = date("d-m-Y h:i:s", strtotime($record['ExpiredOn']));
				}

				$record['Type'] = "Staff";

				array_push($Records, $record); 
			}
			return $Records;
		}else{
			return $Records;
		}

	}


	function UnassignKey($EntityID,$UserID)
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($UserID);

		$this->db->select("Key,KeyStatusID");
		$this->db->from("tbl_students");
		$this->db->where("StudentID",$UserID);
		$this->db->where("KeyStatusID",1);
		$this->db->limit(1);
		$data = $this->db->get();
		if($data->num_rows() > 0)
		{
			$data = $data->result_array();
			$Key = $data[0]['Key'];

			$this->db->where("StudentID",$UserID);
			$this->db->update("tbl_students",array("KeyStatusID" => 0,"Key" => NULL, "KeyAssignedOn" => NULL, "ActivatedOn" => NULL, "ExpiredOn" => NULL));

			$this->db->where("AssignedInstitute",$InstituteID);
			$this->db->where("KeyName",$Key);
			$this->db->where("UsedStatus",1);
			$this->db->update("set_keys",array("UsedStatus" => NULL));//038EIB002

			$this->db->where('userID', $UserID);
			$this->db->where('SourceID', 6);
			$this->db->delete('tbl_users_login'); 
		}
	}


	function UnassignStaffKey($EntityID,$StaffID){
		$InstituteID = $this->Common_model->getInstituteByEntity($StaffID);

		$this->db->select("Key,KeyStatusID");
		$this->db->from("tbl_staff_assign_keys");
		$this->db->where("StaffID",$StaffID);
		$this->db->where("KeyStatusID",1);
		$this->db->limit(1);
		$data = $this->db->get();
		if($data->num_rows() > 0)
		{
			$data = $data->result_array();
			$Key = $data[0]['Key'];

			$this->db->where("StaffID",$StaffID);
			$this->db->delete("tbl_staff_assign_keys");

			$this->db->where("AssignedInstitute",$InstituteID);
			$this->db->where("KeyName",$Key);
			$this->db->where("UsedStatus",1);
			$this->db->update("set_keys",array("UsedStatus" => NULL));

			//echo $this->db->last_query(); die;
		}
	}

}