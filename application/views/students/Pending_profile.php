<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<style>
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input { 
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}
</style>
<header class="panel-heading">
	<h1 class="h4">Manage Students</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="student-body" ng-init="getFilterData(1)"><!-- Body -->
	<!-- <form id="filterForm" role="form" autocomplete="off"> -->
		<form class="" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter(1)">
		<div class="row" style="margin-top: 20px;">	
			<div class="col-md-2">
		      	<div class="form-group">
			      <span class="float-left records hidden-sm-down">
					<span class="h5">Total records: {{data.dataLists.length}}</span>
				  </span>
				</div>
		  	</div>	 
		  	
			<div class="col-md-2">
				<!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
				<select class="form-control" onchange="filterPendingProfileList(this.value,'')" name="CategoryGUID" id="Courses">
	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
	               <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}" ng-selected="CourseGUID==row.CategoryGUID">{{row.CategoryName}}</option>
	            </select>
			</div>
			<div class="col-md-2">
				<!-- <label for="inputName" class="control-label mb-10">Select Subject</label> -->
				<div id="subcategory_section" class="subcategory_section">
	               <select id="subject" name="BatchGUID" onchange="filterPendingProfileList('',this.value)"  class="form-control">
	                  <option value="">Select Batch</option>
	                  <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}" ng-selected="BatchGUID==row.BatchGUID">{{row.BatchName}}</option>
	               </select>
	            </div>
			</div>
			<div class="col-md-2">
				<!-- <label for="inputName" class="control-label mb-10">Select Subject</label> -->
				<div id="subcategory_section" class="subcategory_section">
	               <select id="Approved" name="Approved" onchange="applyFilter(1)"  class="form-control">
	                  <option value="0" selected="selected">Pending Profiles</option>
	                  <option value="1">Approved Profiles</option>
	               </select>
	            </div>
			</div>
			<div class="col-md-4">				
					<div id="universal_search">
                        <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="width: 45%;" autocomplete="off" onkeyup="SearchTextClear(this.value)" >
                        <!-- <span class="glyphicon glyphicon-search" ng-click="applyFilter(1)" data-toggle="tooltip" title="Search"></span> -->
                        <input type="button" class="btn btn-info btn-sm" value="Apply Filter" name="Submit" ng-click="applyFilter(1)" style="width: 40%; margin-left: 10px;">
                        <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
                    </div>				
			</div>
			
		</div>	
		</form>											
	<!-- </form> -->


	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">
	<input type="hidden" name="CategoryGUID" id="CategoryGUID">
	<input type="hidden" name="QuestionsLevel" id="QuestionsLevel">
	<input type="hidden" name="QuestionsGroup" id="QuestionsGroup">
	<input type="hidden" name="QuestionsType" id="QuestionsType">


	<!-- Data table -->
	<div class="table-responsive block_pad_md" style="margin-top: 45px;" infinite-scroll="getPendingProfile()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
		
		<!-- data table -->
		<table class="table table-striped table-hover">
			<!-- table heading -->
			<thead>
				<tr ng-if="Approved == 0">
					<th style="width: 100px; text-align: center;">Name</th>
					<th style="width: 100px; text-align: center;">Course</th>
					<th style="width: 100px; text-align: center;">Batch</th>
					<th style="width: 100px; text-align: center;">Email</th>
					<th style="width: 100px; text-align: center;">Contact Number</th>
					<th style="width: 100px; text-align: center;">Entry Date</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
				<tr ng-if="Approved == 1">
					<th style="width: 100px; text-align: center;">Name</th>
					<th style="width: 100px; text-align: center;">Course</th>
					<th style="width: 100px; text-align: center;">Batch</th>
					<th style="width: 100px; text-align: center;">Entry Date</th>
					<th style="width: 100px; text-align: center;">Approved By</th>
					<th style="width: 100px; text-align: center;">Approved Date</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr ng-if="Approved == 0" scope="row"  ng-repeat="(key, row) in data.dataLists">
					<td style="text-align: center;">{{row.Current_FirstName}} {{row.Current_LastName}}</td>
					<td style="text-align: center;">{{row.CategoryName}}</td>
					<td style="text-align: center;">{{row.BatchName}}</td>
					<td style="text-align: center;">{{row.Current_Email}}</td>
					<td style="text-align: center;">{{row.Current_PhoneNumber}}</td>
					<td style="text-align: center;">{{row.EntryDate}}</td>					
					<td class="text-center" style="text-align: center;">
						<a  class="glyphicon glyphicon-eye-open" href="" ng-click="loadPendingProfileView(key,row)" title="View"></a>
					</td>
				</tr>
				<tr ng-if="Approved == 1" scope="row"  ng-repeat="(key, row) in data.dataLists">
					<td style="text-align: center;">{{row.Current_FirstName}} {{row.Current_LastName}}</td>
					<td style="text-align: center;">{{row.CategoryName}}</td>
					<td style="text-align: center;">{{row.BatchName}}</td>
					<td style="text-align: center;">{{row.EntryDate}}</td>	

					<td style="text-align: center;"><div class="content float-left"><strong class="ng-binding">{{row.ApprovedBy}}</strong> <!-- ngIf: row.Email --><div ng-if="row.Email" class="ng-scope"><a href="mailto:{{row.ApprovedByEmail}}" target="_top" class="ng-binding">{{row.ApprovedByEmail}}</a></div><!-- end ngIf: row.Email --><!-- ngIf: !row.Email --> </div></td>
					<td style="text-align: center;">{{row.ApprovedOn}}</td>	
					<td class="text-center" style="text-align: center;">
						<a  class="glyphicon glyphicon-eye-open" href="" ng-click="loadPendingProfileView(key,row)" title="View"></a>
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="!data.dataLists.length">
			<span ng-if="data.dataLists.length">No more records found.</span>
			<span ng-if="!data.dataLists.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->

	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Student Details</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>

</div><!-- Body/ -->

<script type="text/javascript">
	function test(status){
		alert(status);
	}
</script>