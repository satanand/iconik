<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to get Get Permitted Modules.
	*/
	function getPermittedModules($UserTypeID,$UserID){
		$InstituteID = $this->Common_model->getInstituteByEntity($UserID);

		$this->db->select('AM.*');
		$this->db->from('admin_user_type_permission AUTP');
		$this->db->from('admin_modules AM');
		$this->db->where("AUTP.ModuleID","AM.ModuleID", FALSE);
		$this->db->where("AUTP.UserTypeID",$UserTypeID);		
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			$data = $Query->result_array();
			foreach($data as $Record){
				//print_r($Record); die;
				$this->db->select("*");
				$this->db->from("admin_second_level_permission");
				$this->db->where("ModuleID",$Record['ModuleID']);
				$this->db->where("InstituteID",$InstituteID);
				$this->db->limit(3);
				$QUE = $this->db->get();
				if($QUE->num_rows() > 0){
					$SecondPermission = $QUE->result_array();
					foreach ($SecondPermission as $key => $value) {
						$Record["Action"][$key] =  $value['Action'];
					}
				}

			 	$Records[] = $Record;
			}

			return $Records;
		}
		return FALSE;		
	}


	/*------------------------------*/
	/*------------------------------*/	
	function getMenu($UserTypeID, $ParentControlID=NULL) {
		$this->db->select('*');
		$this->db->from('admin_control C');
		$this->db->join('admin_modules M', 'C.ModuleID=M.ModuleID', 'left');
		$this->db->or_group_start(); //this will start grouping

		$this->db->where('C.ModuleID IS NULL', NULL, FALSE);
		$this->db->where('C.ModuleID IS NULL', NULL, FALSE);
		$this->db->or_where("C.ModuleID IN (SELECT ModuleID FROM admin_user_type_permission WHERE UserTypeID=$UserTypeID )", NULL, FALSE);
    	$this->db->group_end(); //this will end grouping


    	$this->db->where('C.ParentControlID',$ParentControlID);
    	$this->db->order_by('C.Sort','ASC');
    	$query = $this->db->get();
		//echo 	$this->db->last_query();
		//exit;
    	$return = array();				
    	if($query->num_rows()>0){

    		foreach($query->result_array() as $value){
    			if(empty($value['ModuleName'])){
    				$value['ChildMenu']=$this->getMenu($UserTypeID, $value['ControlID']);
    				if(empty($value['ChildMenu'])){
    					unset($value);
    				}
    			}
    			if(!empty($value)){
    				$data[] = $value;
    			}
    		}

    	}
    	return @$data;
    }








}

