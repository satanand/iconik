<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keys extends API_Controller_Secure
{
	function __construct()

	{

		parent::__construct();

		$this->load->model('Keys_model');

		$this->load->model('Users_model');

		$this->load->model('Common_model');

	}
	



	public function updateAllotedKeys_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required');

		$this->form_validation->set_rules('Validity[]', 'Validity', 'trim|required');

		$this->form_validation->set_rules('Keys[]', 'Keys', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->post['TotalPrice'] == $this->post['TotalAvailableKeys']){
			
			$this->Keys_model->updateAllotedKeys($this->EntityID, $this->input->post());

		}else{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message']      	=	"Total keys should be equal of available keys."; 
		}
	}


	/*

	Name: 			keyVerification

	Description: 	Student Delete session if key is not wrong

	URL: 			/api/keys/signout/

	*/

	public function checkKeyStatus_post()

	{

		/* Validation section */	
		// $this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required');

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->set_rules('Key', 'Key', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		//print_r($this->UserID); die;		

		/* Validation - ends */
		$InstituteData = $this->Keys_model->checkKeyStatus($this->UserID,$this->input->post());
		//$this->Return['Data']  =	$InstituteData; 
		if($InstituteData){
			$this->Return['KeyStatusID']  =	$InstituteData[0]['KeyStatusID']; 
		}else{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message']      	=	"Sorry! Some error occured."; 
		}

	}


	/*

	Name: 			keyVerification

	Description: 	Student Delete session if key is not wrong

	URL: 			/api/keys/signout/

	*/

	public function keyVerification_post()

	{

		/* Validation section */	
		// $this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required');

		$this->form_validation->set_rules('FullName', 'FullName', 'trim|required');

		$this->form_validation->set_rules('Email', 'Email', 'trim|required');

		$this->form_validation->set_rules('InstituteID', 'InstituteID', 'trim|required');

		$this->form_validation->set_rules('Key', 'Key', 'trim|required');

		$this->form_validation->set_rules('WrongField', 'WrongField', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		$InstituteData = $this->Users_model->getUsers('FirstName,LastName,Email',array("UserID" => $this->input->Post('InstituteID')));
		//print_r($InstituteData);
		//if(!empty($InstituteData)){

		
			
		$content = $this->load->view('emailer/InformToInstitute', array(
					"InstituteName" => 	$InstituteData['FirstName'],
					"StudentName" 	=> 	$this->input->Post('FullName'),
					"StudentEmail" 	=> 	$this->input->Post('Email'),
					'Message' 		=> 	"Student ".$StudentData['FullName']." is getting wrong information for field ".$this->input->Post('WrongField')." after trying to login with the key"." ".$this->input->Post('Key').".",
				) , TRUE);
		
			sendMail(array(
				'emailTo' => $InstituteData['Email'],
				'emailSubject' => "App access with wrong key",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));
		//}

		if($this->Post['StatusID'] == 7){
			$this->Users_model->deleteSession($this->Post['SessionKey']);
		}

		//$this->Users_model->deleteSession($this->Post['SessionKey']);/* Delete session */

	}



	/*

	Name: 			activateKey

	Description: 	Activate key

	URL: 			/api/keys/signout/

	*/

	public function activateKey_post()

	{

		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	

		if(!empty($this->input->post('UserTypeID')) && $this->input->post('UserTypeID') == 11){
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		}else{
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[Students,UserID]');
		}		

		$this->form_validation->set_rules('Key', 'Key', 'trim|required');

		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		//print_r($this->post['Validity']); die;	

		/* Validation - ends */
		$InstituteData = $this->Keys_model->activateKey($this->UserID,$this->input->post());
		
		if($InstituteData == "Already activated"){
			
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message']   =  "This key is already activated."; 

		}else if($InstituteData){

			$this->Return['Data']  =	$InstituteData; 

		}else{

			$this->Return['ResponseCode'] = 500;

			$this->Return['Message']   =  "You are passing wrong key."; 
		}
	}

	/*

	Description: 	Use to add new keys

	URL: 			/api/keys/add	

	*/

	public function add_post()

	{

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');
		$this->form_validation->set_rules('KeyCount', 'Keys Count', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Keys_model->addKeys($this->EntityID, $this->Post);

		if($KeysData){

			$this->Return['Message']      	=	"Keys added successfully."; 
		}

	}



	/*

	Description: 	Use to add new keys

	URL: 			/api/keys/add	

	*/

	public function getUser_post()

	{

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//$UsersData = $this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,UserTypeID,UserTypeName,Username',array("UserID" => $this->SessionUserID);

		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,UserTypeID,UserTypeName,Username',array("UserTypeID" => 10));

	}



	/*

	Description: 	Use to add new keys

	URL: 			/api/keys/add	

	*/

	public function allotKeys_post()

	{

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('KeyCount', 'Keys Count', 'trim|required');
		$this->form_validation->set_rules('AvailableKeys', 'Keys Count', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		//print_r($this->post[]); die;

		if($this->input->post('KeyCount') > $this->input->post('AvailableKeys')){
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message']      	=	"Keys count is greater than available keys."; 
		}else{

			$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

			$allotKeys = $this->Keys_model->allotKeys($this->EntityID, $this->Post, $this->UserID);

			if($allotKeys){

				$this->Return['Message']      	=	"Keys alloted successfully."; 
			}
		}

	}



	/*

	Description: 	Use to add new keys

	URL: 			/api/keys/add	

	*/

	public function availableKeys_post()

	{

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('validity', 'validity', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$allotKeys = $this->Keys_model->availableKeys($this->EntityID, $this->Post['validity']);

		if($allotKeys){

			$this->Return['Data']      	=	$allotKeys; 
		}

	}


	


	/*

	Name: 			getKeysStatisctics

	Description: 	Use to get keys dashboard.

	URL: 			api/Keys/getKeysStatisctics/	

	*/

	public function getKeysStatisctics_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Keys_model->getKeysStatisctics($this->EntityID);

		$this->Return['Data'] = $data;
	}





	/*

	Description: 	use to get list of filters

	URL: 			/api_admin/entity/getFilterData	

	*/

	public function getBatchByCourse_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('InstituteGUID', 'InstituteGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */


		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);


		$batchData = $this->Common_model->getBatchByCourse($this->EntityID,$this->CategoryID);

		if($batchData){

			$this->Return['Data'] = $batchData;	

		}

	}


	/*
	Description: 	Use to get Get student list.
	URL: 			/api/key/getStudents where key is null
	Input (Sample JSON): 		
	*/
	public function getStudents_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		//$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[batch,BatchID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword');
		$this->form_validation->validation($this);  /* Run validation */		
		

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		$StudentData = $this->Keys_model->getStudents($EntityID,array("CourseID"=>@$this->CategoryID,"BatchID"=>@$this->BatchID,"Keyword"=>@$this->input->post('Keyword')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}


	/*
	Description: 	Use to  assign key to students.
	URL: 			/api/key/assign keys to student
	Input (Sample JSON): 		
	*/
	public function assignKeysToStudent_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');
		$this->form_validation->set_rules('AvailableKeys', 'AvailableKeys', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */
		//print_r($this->input->post('UserGUID')); die;  assignKeysToStudent

		$StudentData = $this->Keys_model->assignKeysToStudent((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->input->post('UserGUID'),$this->input->post('Validity'),$this->input->post('AvailableKeys'));
		if($StudentData){
			$this->Return['Message'] = "keys are assigned to ".$StudentData." students";
			$this->Return['Data'] = $StudentData;
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! keys are not available";
		}
	}


	/*
	Description: 	Use to  assign key to staff.
	URL: 			/api/key/assign keys to staff
	Input (Sample JSON): 		
	*/
	public function assignKeysToStaff_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Validity', 'Validity', 'trim|required');
		$this->form_validation->set_rules('AvailableKeys', 'AvailableKeys', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */
		//print_r($this->input->post('UserGUID')); die;  assignKeysToStudent

		$StudentData = $this->Keys_model->assignKeysToStudent((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->input->post('UserGUID'),$this->input->post('Validity'),$this->input->post('AvailableKeys'));
		if($StudentData){
			$this->Return['Message'] = "keys are assigned to ".$StudentData." staff";
			$this->Return['Data'] = $StudentData;
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Sorry! keys are not available";
		}
	}


	


	/*
	Description: 	Use to  set default keys to students.
	URL: 			/api/key/assign keys to student
	Input (Sample JSON): 		
	*/
	public function setFranchiseeDefaultKeys_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('TotalKeys', 'TotalKeys', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */
		//print_r($this->input->post('UserGUID')); die;  assignKeysToStudent
		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
		$Data = $this->Keys_model->setFranchiseeDefaultKeys($EntityID,$this->input->post('TotalKeys'));
		if($Data){
			
		}
	}


	/*
	Description: 	Use to get used keys with student detail.
	URL: 			/api/keys/UsedKeysDetail
	Input (Sample JSON): 		
	*/
	public function UsedKeysDetail_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('Type', 'Type', 'trim');
		$this->form_validation->set_rules('Validity', 'Validity', 'trim');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);

		if(!empty($this->input->post('Type')) && $this->input->post('Type') == "Staff")	{			
			$StudentData = $this->Keys_model->UsedStaffKeysDetail($EntityID,array("Validity"=>$this->input->post("Validity"),"BatchID"=>@$this->BatchID,"CategoryID"=>@$this->CategoryID,"Keyword"=>@$this->input->post('Keyword')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		}else{
			$StudentData = $this->Keys_model->UsedKeysDetail($EntityID,array("Validity"=>$this->input->post("Validity"),"BatchID"=>@$this->BatchID,"CategoryID"=>@$this->CategoryID,"Keyword"=>@$this->input->post('Keyword')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		}
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}



	/*
	Description: 	Use to get used keys with student detail.
	URL: 			/api/keys/UsedKeysDetail
	Input (Sample JSON): 		
	*/
	public function UnassignKey_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		if($this->input->post('Type') == "Student"){
			$this->form_validation->set_rules('StudentGUID', 'StudentGUID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		}else{
			$this->form_validation->set_rules('StudentGUID', 'StudentGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		}		
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);	

		if($this->input->post('Type') == "Student"){
			$StudentData = $this->Keys_model->UnassignKey($EntityID,$this->UserID);
		}else{
			$StudentData = $this->Keys_model->UnassignStaffKey($EntityID,$this->UserID);
		}
		//print_r($StudentData);
		
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}
}