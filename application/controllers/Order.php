<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js'

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"My Orders" => ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('order/order_list', $load['breadcrumb']);

		$this->load->view('includes/footer');

	}



	public function list()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css'

		);

		$load['js']=array(

			'asset/js/order.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js'

		);



		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"My Business" => "",
			"Manage Order" => ""
		);


		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('order/customer_order_list',$load['breadcrumb']);

		$this->load->view('includes/footer');

	}



	public function online()

	{
		//echo "fdsjflkjsldk"; die;

		//print_r($this->input->post()); die;

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',

			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',

		);

		$load['js']=array(

			'asset/js/order.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js'

		);			


		$load['data'] = $this->input->post();

		if($this->input->post("merchant_param3") == "SMS Credits"){
			$load['breadcrumb']=array(
				"Dashboard" => base_url()."dashboard",
				"Bulk SMS" => base_url()."bulksms",
				"Payment Confirmation" => ""
			);
		}else{
			$load['breadcrumb']=array(
				"Dashboard" => base_url()."dashboard",
				"Order Keys" => base_url()."order",
				"Payment Confirmation" => ""
			);
		}

		$load['heading'] = "Payment Confirmation (Online via CCavenue)";



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('order/payment_continue', $load);

		$this->load->view('includes/footer');

	}


	public function online_payment(){
		if(!empty($this->Post)){



			$JSON = json_encode(array(



				"Username" 		=> @$this->Post['Username'],



				"Password" 		=> @$this->Post['Password'],



				"Source" 		=> 'Direct',



				"DeviceType" 	=> 'Native'



			));



			$Response = APICall(API_URL.'admin/signin', $JSON); /* call API and get response */



			if($Response['ResponseCode'] == 200){ /*check for admin type user*/

				//print_r($Response['Data']); die;

				$this->session->set_userdata('UserData',$Response['Data']); /* Set data in PHP session */

				//print_r($this->session->userdata('UserData')); die;

			}


			//redirect(base_url().'dashboard');
			response($Response);



			exit;



		}







		/* load view */



		$load['js']=array(



			'asset/js/signin.js'



		);	



		$this->load->view('includes/header',$load);



		$this->load->view('signin/signin');



		$this->load->view('includes/footer');
	}



}

