<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function GetStudentAccount($EntityID)
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select("tbl_students.*,tbl_users.Email,tbl_users.PhoneNumber,tbl_users.PhoneNumberForChange,tbl_users.FirstName,tbl_users.LastName,tbl_users.Address,tbl_users.CountryCode,tbl_users.CityName,tbl_users.StateName as State,tbl_users.Postal,CO.CountryName, tbl_users.FacebookURL, tbl_users.TwitterURL, tbl_users.GoogleURL, tbl_users.InstagramURL, tbl_users.LinkedInURL, tbl_users.WhatsApp");
		$this->db->select('IF(tbl_users.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",tbl_users.ProfilePic)) AS ProfilePic,CONCAT_WS(" ",tbl_users.FirstName,tbl_users.LastName) FullName');
		$this->db->from("tbl_students");
		$this->db->join("tbl_users","tbl_students.StudentID = tbl_users.UserID");
		$this->db->join("tbl_entity","tbl_users.UserID = tbl_entity.EntityID");
		$this->db->join('set_location_country CO', 'tbl_users.CountryCode = CO.CountryCode', 'left');
		if(!empty($InstituteID)){
			$this->db->where("InstituteID",$InstituteID);
		}
		if(!empty($CategoryID)){
			$this->db->where("CourseID",$CategoryID);
		}
		if(!empty($BatchID)){
			$this->db->where("BatchID",$BatchID);
		}
		if(!empty($EntityID)){
			$this->db->where("tbl_users.UserID",$EntityID);
			$this->db->limit(1);
		}
		$Query =  $this->db->get(); //echo $this->db->last_query(); die;
		//$StudentData = $Query->result_array(); //print_r($StudentData);
		$Records = array();
		if($Query->num_rows() > 0){
			foreach ($Query->result_array() as $record) 
			{
				if(!empty($record['FeeID']))
				{
					$query = $this->db->query("select FullDiscountFee from tbl_setting_fee where FeeID = ".$record['FeeID']);
					$FeeData = $query->result_array();
					$record['FullDiscountFee'] = $FeeData[0]['FullDiscountFee'];
				}
				else
				{
					$record['FullDiscountFee'] = 0;
				}
				
				if(!empty($record['CourseID']))
				{
					$query = $this->db->query("select CategoryGUID,CategoryName,Duration from set_categories where CategoryID = ".$record['CourseID']);
					$CategoryData = $query->result_array();
					$record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
					$record['CourseName'] = $CategoryData[0]['CategoryName'];
					$record['CourseDuration'] = $CategoryData[0]['Duration'];
				}

				if(!empty($record['CourseID']))
				{
					$query12 = $this->db->query("select CategoryGUID,CategoryName,CategoryID from set_categories where ParentCategoryID = ".$record['CourseID']);

					if($query12->num_rows() > 0){
						$CategoryData = $query12->result_array();
						$subjects = array();

						foreach ($CategoryData as $subject) {
							//print_r($subject);
							//echo "SELECT CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.UserID,U.PhoneNumber,U.Email FROM tbl_user_jobs as UJ join tbl_users as U ON U.UserID = UJ.UserID WHERE FIND_IN_SET('".$subject['CategoryID']."',UJ.SubjectID)";
							$where = "FIND_IN_SET('".$subject['CategoryID']."', UJ.SubjectID)";  
							$this->db->select('CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.UserID,U.PhoneNumber,U.Email');
							$this->db->from('tbl_user_jobs as UJ');
							$this->db->join('tbl_users as U','U.UserID = UJ.UserID');
							$this->db->where($where);
							$query13 =  $this->db->get();
							//echo $this->db->last_query(); die;
							
							if($query13->num_rows() > 0){
								$FacultyData = $query13->result_array();
								$subject['Faculty'] = $FacultyData;
							}else{
								$subject['Faculty'] = array();
							}

							$subjects[] = $subject;
						}

						$record['SubjectName'] = $subjects;
					}					
				}


				if(!empty($record['BatchID']))
				{
					$query = $this->db->query("select BatchName from tbl_batch where BatchID = ".$record['BatchID']);
					$BatchName = $query->result_array();
					$record['BatchName'] = $BatchName[0]['BatchName'];

					// $this->db->select('CONCAT_WS(" ",u.FirstName,u.LastName) FullName,u.UserID,u.PhoneNumber,u.Email');
			  //       $this->db->from('tbl_batchbyfaculty bf');
			  //       $this->db->join('tbl_users u', 'u.UserID = bf.FacultyID', 'left');
			  //       $this->db->where('BatchID',$record['BatchID']);
			        
			  //       $query = $this->db->get();
			  //       //echo $this->db->last_query(); 
			  //       if ($query->num_rows() > 0) {
			  //       	$record['Faculty'] = $query->result_array();
			  //       }
				}

				if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
					$record['Fee Status'] = "Unpaid";
				}else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 5){
					$record['Fee Status'] = "Paid";
				}else{
					$record['Fee Status'] = "Unpaid";
				}

				if(!empty($record['StatusID']) && $record['StatusID'] == 1){
					$record['Email Verification'] = "Pending";
				}else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
					$record['Email Verification'] = "Done";
				}

				$this->db->select("PaymentMode,Amount,TotalFee,RemainingAmount,	PaymentDate,	Denomination, ChequeNumber, ChequeDate, BankName, 	PaymentType, TransactionID, MediaID");
				$this->db->from("tbl_fee_collection");
				$this->db->where("StudentID",$record['StudentID']);
				$this->db->where("CourseID",$record['CourseID']);
				$data = $this->db->get(); $arrFee = array();
				if($data->num_rows() >  0){
					foreach ($data->result_array() as $FeeDetails) {
						if(!empty($FeeDetails['MediaID'])){
							$this->load->model('Media_model');
							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'OfflinePayment',"MediaID" => $FeeDetails['MediaID']),TRUE);
							$FeeDetails['MediaURL'] = $Record['MediaID']['Records'][0]['MediaURL'];
						}else{
							$FeeDetails['MediaURL'] = "";
						}
						$arrFee[] = $FeeDetails;
					}
					$record['PaidFeeDetails'] = $arrFee;
				}else{
					$record['PaidFeeDetails'] = [];
				}	
				

				if(!empty($record['InstallmentID']))
				{
					$query = $this->db->query("select TotalFee,InstallmentAmount,NoOfInstallment from tbl_setting_installment where InstallmentID = ".$record['InstallmentID']);
					$InstallmentData = $query->result_array();	
					if(!empty($InstallmentData))
					{
						$record['TotalFee'] = $InstallmentData[0]['TotalFee'];
						$record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
						$record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
					}				
				}
				elseif(!empty($record['FeeID']))
				{
					$query = $this->db->query("select FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
					$FeeData = $query->result_array();
					$record['TotalFee'] = $FeeData[0]['FullAmount'];
					$record['InstallmentAmount'] = '-';
					$record['NoOfInstallment'] = '-';
				}
				elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['TotalFee'] = 0;
                    $record['InstallmentAmount'] = 0;                                    
                	$record['NoOfInstallment'] = 0; 
                }


				$query = $this->db->query("SELECT DueDateID, date_format(DueDate, '%d-%M-%Y') as DueDate, date_format(ReceivedDate, '%d-%m-%Y') as ReceivedDate, IsPaid 
                    FROM tbl_students_feeduedate 
                    WHERE StudentID = ".$EntityID." AND InstituteID = '".$InstituteID."'" );
                $DueDates = $query->result_array();
                $record['DueDates'] = array();
                if(isset($DueDates) && !empty($DueDates))
                {
                    $record['DueDates'] = $DueDates;
                }



				//print_r($record);
				array_push($Records, $record);
			}
			//print_r($Records);
			return $Records;
		}else{
			return $Records;
		}
	}


	function CheckUserSessionExist($UserID){		
		$this->db->select('UserID');
		$this->db->from('tbl_users_session');
		$this->db->where('UserID', $UserID);
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return 2;
		}else{
			return 1;
		}
	}


	function CheckUserEmailExist($Email,$NotUserType="",$UserTypeID=""){
		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);
		$InstituteID = $this->getInstituteByEntity($EntityID);
		if(!empty($InstituteID)){
			$this->db->select('tbl_users.Email');
			$this->db->from('tbl_users');
			$this->db->join('tbl_entity','tbl_entity.EntityID = tbl_users.UserID');
			$this->db->where('tbl_entity.InstituteID', $InstituteID);
			$this->db->where('tbl_users.Email',$Email);

			if(!empty($NotUserType)){
				$this->db->where('tbl_users.UserTypeID NOT IN ('.$NotUserType.')');
			}


			if(!empty($UserTypeID)){
				$this->db->where('tbl_users.UserTypeID',$UserTypeID);
			}
			
			$this->db->limit(1);
			$query = $this->db->get();

			// echo $this->db->last_query();
			// die;
			if($query->num_rows() > 0){
				return 2;
			}else{
				return 1;
			}
		}
	}	
	
	/*
	Description: 	Use to Save POST input to DB
	*/
	function addInputLog($Response){
		if(!API_SAVE_LOG){
			return TRUE;
		}
		@$this->db->insert('log_api', array(
			'URL' 		=> current_url(),
			'RawData'	=> @file_get_contents("php://input"),
			'DataJ'		=> json_encode(array_merge(array("API" => $this->classFirstSegment = $this->uri->segment(2)), $this->Post, $_FILES)),
			'Response'	=> json_encode($Response)
		));
	}


	/*
	Description: 	Use to get EntityTypeID by EntityTypeName
	*/
	function getEntityTypeID($EntityTypeName){
		if(empty($EntityTypeName)){return FALSE;}
		$this->db->select('EntityTypeID');
		$this->db->from('tbl_entity_type');
		$this->db->where('EntityTypeName',$EntityTypeName);
		$this->db->limit(1);
		$Query = $this->db->get();		
		if($Query->num_rows()>0){
			return $Query->row()->EntityTypeID;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get SectionID by SectionID
	*/
	function getSection($SectionID){
		if(empty($SectionID)){return FALSE;}
		$this->db->select('*');
		$this->db->from('tbl_media_sections');
		$this->db->where('SectionID',$SectionID);
		$this->db->limit(1);
		$Query = $this->db->get();		
		if($Query->num_rows()>0){
			return $Query->row_array();
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get DeviceTypeID by DeviceTypeName
	*/
	function getDeviceTypeID($DeviceTypeName){
		if(empty($DeviceTypeName)){return FALSE;}
		$this->db->select('DeviceTypeID');
		$this->db->from('set_device_type');
		$this->db->where('DeviceTypeName',$DeviceTypeName);
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			return $Query->row()->DeviceTypeID;
		}else{
			return FALSE;
		}
	}
	/*
	Description: 	Use to get SourceID by SourceName
	*/
	function getSourceID($SourceName){
		if(empty($SourceName)){return FALSE;}
		$this->db->select('SourceID');
		$this->db->from('set_source');
		$this->db->where('SourceName',$SourceName);
		$this->db->limit(1);
		$Query = $this->db->get();		
		if($Query->num_rows()>0){
			return $Query->row()->SourceID;
		}else{
			return FALSE;
		}
	}

	/*
	Description: 	Use to get SourceID by SourceName
	*/
	function getStatusID($Status){
		if(empty($Status)){return FALSE;}
		$Query = $this->db->query("SELECT `StatusID` FROM `set_status` WHERE FIND_IN_SET('".$Status."',StatusName) LIMIT 1");
		if($Query->num_rows()>0){
			return $Query->row()->StatusID;
		}else{
			return FALSE;
		}
	}



     /*
	Description: 	Use to get ReferralCode
	*/
	function getReferralCode($ReferralCode){
		if(empty($ReferralCode)){return FALSE;}
		$this->db->select('ReferralCodeID, UserID');
		$this->db->from('tbl_referral_codes');
		$this->db->where('ReferralCode',$ReferralCode);
		$this->db->limit(1);
		$Query = $this->db->get();		
		if($Query->num_rows()>0){
			return $Query->row();
		}else{
			return FALSE;
		}
	}


     /*
	Description: 	Use to get EntityID by MenuGUID
	*/
	function getCategoryTypeName($CategoryTypeName){
		if(empty($CategoryTypeName)){return FALSE;}
		$this->db->select('CategoryTypeID');
		$this->db->from('set_categories_type');
		$this->db->where('CategoryTypeName',$CategoryTypeName);
		$this->db->limit(1);
		$Query = $this->db->get();		
		if($Query->num_rows()>0){
			return $Query->row()->CategoryTypeID;
		}else{
			return FALSE;
		}
	}



	/*
	Description: 	Use to get category name by id
	*/
	function getCategoryNameByID($CategoryID){
		if(empty($CategoryID)){return FALSE;}
		$this->db->select('CategoryName');
		$this->db->from('set_categories');
		$this->db->where('CategoryID',$CategoryID);
		$this->db->limit(1);
		$Query = $this->db->get();		
		if($Query->num_rows()>0){
			return $Query->row()->CategoryName;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get Student Count By Baych ID
	*/
	function getStudentCountByBatchID($BatchID){
		if(empty($BatchID)){return FALSE;}
		$count = 0;	

		$sql = "SELECT count(StudentID) as total
		FROM tbl_students a
		WHERE a.BatchID = '$BatchID'";

		$Query = $this->db->query($sql);	
					
		if($Query->num_rows() > 0)
		{
			$ID = $Query->result();

			$count = $ID[0]-> total;
		}

		return $count;	
	}


	/*
	Description: 	Use to get Student Count By Course ID
	*/
	function getStudentCount($CourseID, $Where=array()){
		$this->db->select('count(StudentID) as total');
        $this->db->from("tbl_students a");

        if(!empty($CourseID)){
            $this->db->where("a.CourseID", $CourseID);
        }

        if(!empty($Where['BatchID'])){
            $this->db->where("a.BatchID", $Where['BatchID']);
        }

        $Query = $this->db->get();

        //echo $this->db->last_query();	
					
		if($Query->num_rows() > 0)
		{
			$ID = $Query->result();

			$count = $ID[0]->total;
		}

		return $count;	
	}




	/*
	Description: 	Use to get Batch by Course
	*/
	function getBatchByCourse($EntityID="",$CourseID=""){
		//if(empty($CourseID)){return FALSE;}
		$InstituteID = $this->getInstituteByEntity($EntityID);
		$this->db->select('BatchGUID,BatchName,BatchID,date_format(StartDate,"%d-%m-%Y") as StartDate');
		$this->db->from('tbl_batch');
		if(!empty($EntityID)){			
			$this->db->join("tbl_entity","tbl_entity.EntityID = tbl_batch.BatchID");
			$this->db->where('tbl_entity.InstituteID',$InstituteID);
		}	
		if(!empty($CourseID)){
			$this->db->where('CourseID',$CourseID);
		}		
		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			return $Query->result_array();
		}else{
			return FALSE;
		}
	}
	

	/*
	Description: 	Use to get institute id by entity id
	*/
	function getInstituteByEntity($EntityID){
		if(empty($EntityID)){return FALSE;}
		$this->db->select('InstituteID');
		$this->db->from('tbl_entity');
		if(!empty($EntityID)){
				$this->db->where('EntityID',$EntityID);
			}

		if(!empty($EntityGUID)){
				$this->db->where('EntityGUID',$EntityGUID);
			}

		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->InstituteID;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}	

	/*
	Description: 	Use to get institute id by entity id
	*/
	function getInstituteByEntityGUID($EntityGUID){
		if(empty($EntityGUID)){return FALSE;}
		$this->db->select('InstituteID');
		$this->db->from('tbl_entity');
		if(!empty($EntityGUID)){
			$this->db->where('EntityGUID',$EntityGUID);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->InstituteID;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get institute id by entity id
	*/
	function joinInstitute($EntityID){
		if(empty($EntityID)){return FALSE;}
		$this->db->select('InstituteID');
		$this->db->from('tbl_entity');
		$this->db->where('EntityID',$EntityID);
		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->InstituteID;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get country list
	*/
	function getCountriesList($CountryCode=""){
		$this->db->select('CountryCode,CountryName');
		$this->db->from('set_location_country');
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			return $Query->result_array();
		}else{
			return FALSE;
		}
	}



	/*
	Description: 	Use to get states list
	*/
	function getStatesList($CountryCode=""){
		$this->db->select('State_id,StateName');
		$this->db->from('set_location_states');
		if(!empty($CountryCode)){
			$this->db->where("CountryCode",$CountryCode);
		}else{
			$this->db->where("CountryCode","IN");
		}
		$this->db->order_by("StateName", "asc");
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		if($Query->num_rows()>0){
			return $Query->result_array();
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get states name by id
	*/
	function getStatesNameById($stateID){
		$this->db->select('StateName');
		$this->db->from('set_location_states');
		$this->db->where("State_id",$stateID);
		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->StateName;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get city name by id
	*/
	function getCityNameById($cityID){
		$this->db->select('name');
		$this->db->from('cities');
		$this->db->where("id",$cityID);
		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->name;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}



	/*
	Description: 	Use to get cities list
	*/
	function getCitiesList($stateID=""){
		$this->db->select('id,name');
		$this->db->from('cities');
		if(!empty($stateID)){
			$this->db->where("state_id",$stateID);
		}	
		$this->db->order_by("name", "asc");	
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			return $Query->result_array();
		}else{
			return FALSE;
		}
	}


	


	/*
	Description: 	Use to get states list
	*/
	function getUserTypeByEntityID($EntityID){
		$this->db->select('UserTypeID');
		$this->db->from('tbl_users');
		$this->db->where("UserID",$EntityID);
		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->UserTypeID;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}



	/*
	Description: 	Use to get states list
	*/
	function getMasterFranchiseeByEntityID($EntityID){
		$this->db->select('MasterFranchisee');
		$this->db->from('tbl_users');
		$this->db->where("UserID",$EntityID);
		$Query = $this->db->get();	
		//echo $this->db->last_query();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->MasterFranchisee;
			//$instituteID->InstituteID;
		}else{
			return FALSE;
		}
	}


	


	/*
	Description: 	Use to get user id by UserGUID
	*/
	function getUserIdByGUID($UserGUID=""){
		$this->db->select('UserID');
		$this->db->from('tbl_users');
		$this->db->where("UserGUID",$UserGUID);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->UserID;
		}else{
			return FALSE;
		}
	}



	/*
	Description: 	Use to get user id by UserGUID
	*/
	function getUserEmailByID($UserID=""){
		$this->db->select('Email');
		$this->db->from('tbl_users');
		$this->db->where("UserID",$UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->Email;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get user id by UserGUID
	*/
	function checkEmailExist($UserEmail){
		$this->db->select('Email');
		$this->db->from('tbl_users');
		$this->db->where("Email",$UserEmail);
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			$data = $Query->row();
			return $data->Email;
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get user id by UserGUID
	*/
	function getInstituteDetailByID($UserID="",$Fields=""){
		$this->db->select($Fields);
		$this->db->from('tbl_users');
		$this->db->where("UserID",$UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			$data = $Query->result_array();
			return $data[0];
		}else{
			return FALSE;
		}
	}


	/*
	Description: 	Use to get student detail by id
	*/
	function getStudentDetailByID($Fields="",$StudentID="",$Where=array()){
		$this->db->select($Fields);
		$this->db->from("tbl_students");
		$this->db->join("tbl_users","tbl_students.StudentID = tbl_users.UserID");
		$this->db->join("tbl_entity","tbl_users.UserID = tbl_entity.EntityID");
		
		if(!empty($Where['CourseID'])){
			$this->db->where("CourseID",$CategoryID);
		}
		if(!empty($Where['BatchID'])){
			$this->db->where("BatchID",$BatchID);
		}
		if(!empty($StudentID)){
			$this->db->where("tbl_users.UserID",$StudentID);
			$this->db->limit(1);
		}
		$Query =  $this->db->get(); 

		$Records = array();
		if($Query->num_rows() > 0){
			$data = $Query->result_array();
			return $data[0];
		}
	}
		


	/*
	Description: 	Use to get cities list
	*/
	function getCitiesListByStateName($stateName){
		$this->db->select('id,name');
		$this->db->from('cities');
		if(!empty($stateName)){
			$this->db->where("state_id = (select state_id from set_location_states where StateName = '".$stateName."')");
		}	
		$this->db->order_by("name", "asc");		
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		if($Query->num_rows()>0){
			return $Query->result_array();
		}else{
			return FALSE;
		}
	}

	/*
	Description: 	Use to get Duration
	*/
	function getDuration($CategoryGUID="",$CategoryID=""){
		$this->db->select('Duration');
		$this->db->from('set_categories');
		if(!empty($CategoryGUID)){
			$this->db->where("CategoryGUID",$CategoryGUID);
		}
		if(!empty($CategoryID)){
			$this->db->where("CategoryID",$CategoryID);
		}		
		$Query = $this->db->get();	
		/*echo $this->db->last_query();
		die();*/
		if($Query->num_rows()>0){
			return $Query->result_array();
		}else{
			return FALSE;
		}
	}



	/*Read the data from DB */
	function getMySchecdules($EntityID,$FacultyID="",$SubjectID="",$Inputs = array())
	{
	 	$InstituteID = $this->getInstituteByEntity($EntityID);
	 	$where = "";

	 	if(!empty($FacultyID)){
	 		$where .= " AND es.FacultyID = ".$FacultyID;
	 	}

	 	if(!empty($SubjectID)){
	 		$where .= " AND es.SubjectID = ".$SubjectID;
	 	}

 
	 	$append = "";
	 	if(isset($Inputs['BatchDate']) && !empty($Inputs['BatchDate']))
	 	{
	 		$append = " AND ( DATE(es.start) <= '".$Inputs['BatchDate']."' AND DATE(es.end) >= '".$Inputs['BatchDate']."' )";
	 	}

	
		$sql = "SELECT es.*, CONCAT_WS('',U.FirstName,U.LastName) FullName,U.UserID,B.BatchID,B.BatchName,C.CategoryName,CC.CategoryName as CourseName FROM set_timescheduling es join tbl_entity E on es.id=E.EntityID join tbl_batch B on es.BatchID=B.BatchID join tbl_users U on es.FacultyID=U.UserID join set_categories C on C.CategoryID = es.SubjectID join set_categories CC on es.CourseID = CC.CategoryID WHERE E.InstituteID = 
		".$InstituteID." ".$where." $append AND es.BatchID = (select BatchID from tbl_students where StudentID = '".$EntityID."')  ORDER BY es.start DESC";

		return $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();
	}


	function ContactUs($EntityID,$Input){
		$InstituteID = $this->getInstituteByEntity($EntityID);
		$InstituteData = $this->getInstituteDetailByID($InstituteID,"Email,FirstName");

		$EmailText = "Student ".$Input['Name']." wants to contact you with the following details, which is mentioned below.";

		$content = $this->load->view('emailer/contact',array(
				"EmailText" => $EmailText,
				"InstituteName" => trim($InstituteData['FirstName']),
				"Name" => trim($Input['Name']),
				"Email" => trim($Input['Email']), 
				"PhoneNumber" => trim($Input['PhoneNumber']), 
				"Message" => trim($Input['Message']),
				"Signature"=>""),
			TRUE);

		$SendMail = sendMail(array(
			//'From_email'    => $InstituteEmail,

			'emailTo' 	=> $InstituteData['Email'],			

			'emailSubject'	=> $Input['Subject'],

			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

		));


		$EmailText = "Your message has been sent successfully to Institute with the following details, which is mentioned below.";
		$content = $this->load->view('emailer/contact',array(
				"EmailText" => $EmailText,
				"InstituteName" => trim($InstituteData['FirstName']),
				"Name" => trim($Input['Name']),
				"Email" => trim($Input['Email']), 
				"PhoneNumber" => trim($Input['PhoneNumber']), 
				"Message" => trim($Input['Message']),
				"Signature"=>""),
			TRUE);
		$SendMail = sendMail(array(
			//'From_email'    => $InstituteEmail,

			'emailTo' 	=> trim($Input['Email']),			

			'emailSubject'	=> $Input['Subject'],

			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

		));
	}


	function RatingByStudent($EntityID,$Input){
		$Insert = array(
			"FacultyRating" => @$Input['FacultyRating'],
			"CourseRating" =>  @$Input['CourseRating'],
			"FacilityRating" =>  @$Input['FacilityRating'],
			"TeachingRating" =>  @$Input['TeachingRating'],
			"EnviornmentRating" =>  @$Input['EnviornmentRating'],
			"Comment" =>  @$Input['Comment']
		);

		$this->db->where("StudentID",$EntityID);
		$this->db->update("tbl_students",$Insert);
		return true;
	}


	function GetRatingByStudentID($EntityID,$Input=""){
		$this->db->select("FacultyRating,CourseRating,FacilityRating,TeachingRating,EnviornmentRating,Comment");
		$this->db->from("tbl_students");
		$this->db->where("StudentID",$EntityID);
		$Query = $this->db->get();
		if($Query->num_rows() > 0){
			$data = $Query->result_array();
			return $data[0];
		}else{
			return FALSE;
		}
	}


	function GetBatchmates($EntityID, $multiRecords=FALSE, $PageNo=1, $PageSize=10){
		$this->db->select('CONCAT_WS(" ",u.FirstName,u.LastName) FullName,u.UserID,u.PhoneNumber,u.Email,IF(u.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",u.ProfilePic)) AS ProfilePic');
        $this->db->from('tbl_students s');
        $this->db->from('tbl_users u');

		$this->db->where("s.StudentID","u.UserID", FALSE);

        $this->db->where("s.BatchID = (select BatchID from tbl_students where StudentID = '".$EntityID."' limit 1)");
        $this->db->where('s.StudentID != '.$EntityID);
        /* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
        	foreach($query->result_array() as $Record){	
				
				if(!$multiRecords){
					return $Record;
				}

				$Records[] = $Record;
			}
			$Return['Data'] = $Records;
			return $Return;
        }
	}


	function getAAQSubjects()
	{  
		$data = array();

		$sql = "SELECT SubjectGUID, SubjectName
		FROM tbl_subjects
		WHERE StatusID = 2 
		ORDER BY SubjectName		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$data[] = $Where;				
			}

			return $data;
		}

		return FALSE;	
	}


	function getAAQRelatedTo()
	{  
		$data = array();

		$sql = "SELECT RelatedToGUID, RelatedToName
		FROM tbl_related_to
		WHERE StatusID = 2 
		ORDER BY RelatedToName	
		";

		$Query = $this->db->query($sql); 

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$data[] = $Where;				
			}

			return $data;
		}

		return FALSE;	
	}


	/*
	Description: 	Get Birth Days List of Institute Employee
	*/
	function GetBirthDaysList($EntityID)
	{  
		$data = array();

		$InstituteID = $this->getInstituteByEntity($EntityID);

		$sql = "SELECT u.FirstName, u.LastName, u.EmailForChange, u.BirthDate, u.PhoneNumberForChange, u.CityName, u.StateName, ut.UserTypeName, IF(u.ProfilePic IS NULL,CONCAT('".PROFILE_PICTURE_URL."','default.jpg'),CONCAT('".PROFILE_PICTURE_URL."',u.ProfilePic)) AS ProfilePic 
		FROM tbl_entity e 
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		INNER JOIN tbl_users_type ut ON ut.UserTypeID = u.UserTypeID
		WHERE e.StatusID = 2 AND e.InstituteID = $InstituteID AND u.UserTypeID != 10 AND DATE(u.BirthDate) = CURDATE() AND u.BirthDate IS NOT NULL
		ORDER BY u.FirstName, u.LastName	
		";

		$Query = $this->db->query($sql); 

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$data[] = $Where;				
			}		
		}

		return $data;	
	}


	/*
	Description: 	Get Pending Profile of Institute Employee
	*/
	function GetPendingProfile($EntityID)
	{  
		$data['Total'] = 0;

		$InstituteID = $this->getInstituteByEntity($EntityID);

		$sql = "SELECT p.PendingProfileID 
		FROM tbl_student_pending_profile p 
		INNER JOIN tbl_entity e ON e.EntityID = p.StudentID AND e.StatusID = 2 AND e.InstituteID = $InstituteID
		INNER JOIN tbl_students s ON s.StudentID = p.StudentID
		WHERE p.ApprovedByID IS NULL
		";

		$Query = $this->db->query($sql); 

		if($Query->num_rows()>0)
		{
			$data['Total'] = $Query->num_rows();		
		}

		return $data;	
	}


	function getSubCategoryName($CategoryID){
   	    $this->db->select('C.CategoryName');
		$this->db->from('set_categories C');
		$this->db->join('tbl_entity E','E.EntityID=C.CategoryID','left');
		//$this->db->where("C.CategoryTypeID",3);
		$this->db->where("C.ParentCategoryID",$CategoryID);
		//$this->db->where("E.InstituteID",$InstituteID);
		$this->db->where("E.StatusID!=",6);
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		
		if($Query->result_array()>0){

			foreach($Query->result_array() as $Record){				
			 	$Records[] = $Record['CategoryName'];
			}

			return $Records;
		}
		return FALSE;
	}


	function assignStudentToInstitute($EntityID, $Where = array())
	{
		$UserID = $Where['UserID'];
		$UserGUID = $Where['UserGUID'];
		$InstituteID = $Where['InstituteID'];
		
		$PaidAmount = $Where['PaidAmount'];
		$PaymentDate = $Where['PaymentDate'];
		$OrderStatus = $Where['OrderStatus'];
		$TransactionID = $Where['TrackingID'];	

		$this->db->trans_start();

		$UpdateData = array("UserTypeID"=>7);		
		$this->db->where('UserID', $UserID);
		$this->db->update('tbl_users', $UpdateData);
		$this->db->limit(1);
		$id = $this->db->affected_rows();


		$UpdateData = array("InstituteID"=>$InstituteID);		
		$this->db->where('EntityID', $UserID);
		$this->db->update('tbl_entity', $UpdateData);
		$this->db->limit(1);


		/*$this->db->where('UserID', $UserID);
		$this->db->where('SourceID', 1);
		$this->db->delete('tbl_users_login');*/


		$InsertData = array(
		"StudentID"=>$UserID,			
		"StudentGUID"=>$UserGUID,
		"ConvertPaidAmount"=>$PaidAmount,
		"ConvertOrderStatus"=>$OrderStatus,
		"ConvertTrackingID"=>$TransactionID,					
		"ConvertDate"=>date("Y-m-d H:i:s")
		);
		$this->db->insert('tbl_students', $InsertData);

		$this->db->trans_complete();

		if($id > 0)
		{
			//Send sms to student--------------------------	
			$sql = "SELECT IF(PhoneNumber IS NULL, PhoneNumberForChange, PhoneNumber) as Mobile
			FROM tbl_users				
			WHERE UserID = '".$UserID."'	
			LIMIT 1 ";

			$Query = $this->db->query($sql);
			if($Query->num_rows()>0)
			{
				$arr = $Query->result_array();

				$Mobile = $arr[0]['Mobile'];		
				
				$InstituteID = str_pad($InstituteID, 5, "0");
				$msg = "The transaction (ID: $TransactionID) is success. You are now a registered student of the institute with code $InstituteID. Happy Learning!! Team Iconik";

				sendSMS(array(
					'PhoneNumber' => $Mobile,
					'Text' => $msg
				));
			}						
			
		}	

		return $id;	
	}


	/*
	Description: 	Use to get user id by UserGUID
	*/
	/*function getFatherIdByStudentId($StudentID="")
	{
		$sql = "SELECT s.FatherID
		FROM tbl_students s 
		INNER JOIN tbl_entity e ON e.EntityID = s.FatherID 
		WHERE s.StudentID = '$StudentID'
		LIMIT 1";		
		
		$Query = $this->db->query($sql)	
		
		if($Query->num_rows()>0)
		{
			$data = $Query->row();
			
			return $data->FatherID;
		}
		else
		{
			return FALSE;
		}
	}*/

}


