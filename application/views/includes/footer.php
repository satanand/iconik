</section>
</div> <!-- container/ -->
</div> <!-- mainFrame/ -->

<div class="extension_popup">
    <div class="modal fade" id="extension_popup">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title h5">{{file_title}}</h3>
                    <button type="button" class="close" data-dismiss="modal" ng-click="extension_popup_close()" aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                </div>
                <div class="modal-body" style="height: 400px;">
                    <input type="hidden" name="PostGUID" id="PostGUID">
                    <div id="iframe-info"></div>    
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="extension_popup_close()">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="library_popup">
    <div class="modal fade" id="library_popup">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title h5">{{file_title}}</h3>
                    <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>                    
                </div>
                <div class="modal-body" style="height: 400px;">
                    <input type="hidden" name="PostGUID" id="PostGUID">
                    <div id="iframes-info"></div>    
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="changePassword_modal" >
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title h5">Change Password</h3>     	
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<!-- Filter form -->
			<form id="changePassword_form" role="form" name="changePassword_form" autocomplete="off" class="ng-pristine ng-valid">
				<div class="modal-body">
					<div class="form-area">

						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
                                    <label class="filter-col" for="Validity">Current Password</label>
									<input type="password" name="CurrentPassword" class="form-control"  placeholder="Current Password">
								</div>
							</div>
							<div class="col-md-8">
								<div class="form-group">
                                    <label class="filter-col" for="Validity">New Password</label>
									<input type="password" name="Password" class="form-control" placeholder="New Password">
								</div>
							</div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="filter-col" for="Validity">Confirm Password</label>
                                    <input type="password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password">
                                </div>
                            </div>
						</div>

					</div> <!-- form-area /-->
				</div> <!-- modal-body /-->

				<div class="modal-footer">
                    <img src="asset/img/loader.svg" ng-if="changeCP" style="position: absolute;    top: 50%;    left: 50%;    transform: translate(-50%, -50%);">
					<button type="submit" class="btn btn-success btn-sm"  ng-disabled="changeCP" ng-click="changePassword()">Submit</button>
				</div>

			</form>
			<!-- Filter form/ -->
		</div>
	</div>
</div>


<?php $this->load->view('includes/error_popup'); ?>

<!-- Page Loader -->
<!-- <div ng-if="data.pageLoading" class="text-center page-loader"><span>Loading&#8230;</span></div> -->
<div ng-if="data.pageLoading" id="PageControllerLoader" class="text-center">
    <img src="./asset/img/iconik-loader.svg" style="position: absolute;    top: 50%;    left: 50%;    transform: translate(-50%, -50%);">
</div>


<!-- FOOTER -->
<?php if(!empty($this->session->userdata('UserData'))){ ?>
<footer style="position: inherit;background: #FFFFFF;">
	<div class="container-fluid fixed-bottom" style="text-align: center;">
		<p class="text-muted  small mb-0" style="font-size: 12px;padding-bottom: 15px;">Powered by <a href="<?php echo BASE_URL; ?>" target="_blank" style="color: #0DBCDA;"><img src="http://cp.iconik.in/api/asset/img/emailer/Teknoviq-logo.png" style="height: 25px;width: 55px;"></a></p>
	</div>
</footer> 
<?php }else{ ?>
<footer style="position: inherit;">
    <div class="container-fluid fixed-bottom" style="text-align: center;">
        <p class="text-muted  small mb-0" style="font-size: 12px;padding-bottom: 15px;">Powered by <a href="<?php echo BASE_URL; ?>" target="_blank" style="color: #0DBCDA;"><img src="<?php echo BASE_URL; ?>api/asset/img/emailer/Teknoviq-logo.png" style="height: 25px;width: 55px;"></a>.</p>
    </div>
</footer>
<?php } ?>


<!-- BOOTSTRAP JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- App.js -->
<script src="asset/js/app.js"></script>
<!-- Alertify JS -->
<script src="asset/plugins/alertify/alertify.min.js"></script>
<!-- Other Plugins JS -->
<?php if(!empty($js)){foreach($js as $value){ ?>
<script src="<?php echo $value; ?>"></script>
<?php }} ?>

<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script>
$(document).ready(function(){
  $(".menu-toggel").click(function(){
	$(".navigation").toggleClass("sidebar_hide");
  });

  $(".menu-toggel").click(function(){
	$(".main-navbar").toggleClass("sidebar_left");
  });

    $(".menu-toggel").click(function(){
	$(this).toggleClass("menu_icon");
  });

});
</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142484832-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-142484832-2');
</script>





<style type="text/css">
.email_error, .mobile_error, .length_error
{
    font-size: 12px;
    color: red;
}    
</style>
<script type="text/javascript">
function maxCharactersLimit(obj, len)
{
    $(obj).parent().find(".length_error").remove();

    var content = $(obj).val();

    var clen = content.length;

    if(clen > len)
    {        
        $(obj).parent().append("<span class='length_error'>Only "+len+" characters are allowed.</span>");
        
        $(obj).val(content.substr(0,len));

        return false;       
    }    
    
    return true;
} 


function validateEmail(obj)
{
    $(obj).parent().find(".email_error").remove();

    var email = $(obj).val();

    if(email != "")
    {
        var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
      
        if(!re.test(email))
        {
            $(obj).parent().append("<span class='email_error'>Please enter valid email.</span>");
            return false;
        }
    }    
    
    return true;
}    

function validateMobile(obj)
{
    $(obj).parent().find(".mobile_error").remove();

    var mobile = $(obj).val();

    if(mobile != "")
    {
        var re = /^([0-9]{10})$/;
      
        if(!re.test(mobile))
        {
            $(obj).parent().append("<span class='mobile_error' style='margin-top:5px;'>Please enter valid mobile number.</span>");
            return false;
        }
    }    
    
    return true;
}


$(window).load(function() 
{
    $("#mainFrame").show();

    setTimeout(function()
    {
        $("#PageControllerLoader").hide(); 

        $("footer").find(".container-fluid").removeClass("fixed-bottom");

    }, 300);



    if (SessionKey.length > 0) 
    {
         var idleState = false;
        var idleTimer = null;
        $('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () 
        {
            clearTimeout(idleTimer);
            if (idleState == true) 
            { 
                $("body").css('background-color','#fff');            
            }
            idleState = false;
            idleTimer = setTimeout(function () 
            { 
                idleState = true; 
                alert("Due to inactivity, Current login session is expired, please signin to again access your account.");
                window.location.href = "<?php echo BASE_URL;?>signin/signout";               
                
            }, (30 * 60 * 1000));
        });

        $("body").trigger("mousemove");
    }
   


});
</script>
<!-- <img src="./asset/img/iconik-loader.svg" id="PageControllerLoader" style="position: absolute;    top: 50%;    left: 50%;    transform: translate(-50%, -50%);"> -->
</body>
</html>