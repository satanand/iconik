app.controller('PageController', function ($scope, $http,$timeout){


    $scope.data.dataList = [];
    $scope.UserData =[];
    $scope.data.SecondLevelPermission = [];
    
    /*----------------*/

     $scope.getFilterData = function ()
    {
         $scope.getStates();
         if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
        
    }
    
    $scope.applyFilter = function (){

        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        $scope.getList();

        $scope.getFilterData();

    }


    /*list append*/

    $scope.getList = function ()

    {

        if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&InstituteGUID='+InstituteGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'branch/getBranch', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.dataList.push(response.Data.Records[i]);

               }

             $scope.data.pageNo++;               

         }else{

            $scope.data.noRecords = true;

        }

        $scope.data.listLoading = false;


    });

    }



    $scope.getStates = function(){

        var data = 'SessionKey='+SessionKey;

        $scope.data.states = [];

        $http.post(API_URL+'students/getStatesList', data, contentType).then(function(response) {
            var response = response.data;
            
            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.states = response.Data;

                var option = "";

                for (var i in $scope.data.states) {

                  option += '<option value="'+$scope.data.states[i].State_id+'">'+$scope.data.states[i].StateName+'</option>';
               }


               $scope.StateOptions = option;


            }
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

        });

    }





    $scope.getCities = function(State_id){
        $scope.data.cities = [];
        var data = 'SessionKey='+SessionKey+'&State_id='+State_id;
       
        $http.post(API_URL+'students/getCitiesList', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.cities.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }



    /*load add form*/

    $scope.loadFormAdd = function (Position)

    {
        $scope.data.Position = Position;
    
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
       
        $('#add_model').modal({show:true});


        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

    }


    /*load edit form*/

    $scope.loadFormEdit = function (Position, EntityGUID)

    {

        $scope.data.Position = Position;
   
        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();

        $scope.data.pageLoading = true;
   
        $http.post(API_URL+'branch/getEditBranch','SessionKey='+SessionKey+'&EntityGUID='+EntityGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
                // var option = '<option value="">Please Select</option>';
                // for (var i in $scope.data.states) {
                //    var selected = '';
                //   if($scope.formData.StateID == $scope.data.states[i].State_id){
                //     selected = 'selected';
                //   }

                //   option += '<option value="'+$scope.data.states[i].State_id+'" '+selected+'>'+$scope.data.states[i].StateName+'</option>';
                // }

                // $("#State").html(option);
                // $scope.StateOptions = option;

                //console.log($scope.StateOptions);
                
                $scope.getCities($scope.formData.StateID);

                $('#edits_model').modal({show:true});

                $timeout(function(){            

                  // $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

                  // $("#State").val($scope.formData.StateID);

                  
               }, 800);

            }

        });

    }

      $scope.loadFormView = function (Position, UserID)
       {

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&UserID='+UserID+'&IsAdmin=Yes&Params=RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType';
       
        $http.post(API_URL+'branch/getProfile',data , contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;

                $scope.getCities($scope.formData.State_id);

             $('#View_model').modal({show:true});


                 $timeout(function(){    
                  /*  $("#StartDates,#EndDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});
                    tinymce.init({selector: '#EventDescription' });*/
                  // $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
               
               
               }, 700);

            }

        });

    }

    /*load edit loadFormInactive*/

    $scope.loadFormInactive = function (Position, EntityGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLActive = PATH_TEMPLATE+module+'/active_form.htm?'+Math.random();

          $scope.data.pageLoading = true;
        
          $http.post(API_URL+'branch/getEditBranch','SessionKey='+SessionKey+'&EntityGUID='+EntityGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
        

        $('#active_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 600);
           }

        });
  }  

  /*load edit loadFormActive*/

    $scope.loadFormActive = function (Position, EntityGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLInactive = PATH_TEMPLATE+module+'/inactive_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'branch/getEditBranch','SessionKey='+SessionKey+'&EntityGUID='+EntityGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
        

        $('#inactive_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

           }

        });
     } 

     /*load edit loadFormActive*/

    $scope.loadFormEmail = function (Position, EntityGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLEmail = PATH_TEMPLATE+module+'/email_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'branch/getEditBranch','SessionKey='+SessionKey+'&EntityGUID='+EntityGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
        

        $('#email_model').modal({show:true});

        $timeout(function(){            

           //$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

           }

        });
     }

     
    /*add data*/

    $scope.addData = function ()

    {

        $scope.addDataLoading = true;

       // console.log(InstituteGUID); 

        var data = 'SessionKey='+SessionKey+'&InstituteGUID='+InstituteGUID+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'branch/add', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.data.dataList.push(response.Data);
                 $scope.applyFilter();
                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }





    /*edit data*/

    $scope.editData = function ()

    {

        $scope.editDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&InstituteGUID='+InstituteGUID+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'branch/editBranch', data, contentType).then(function(response) {

            var response = response.data;



            if(response.ResponseCode==200){ /* success case */               

              SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;
                 $scope.applyFilter();
                  $timeout(function(){ 
                $('#edits_model .close').click();
                 }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }  


    /*edit data*/

    $scope.emailData = function ()

    {

        $scope.editDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&InstituteGUID='+InstituteGUID+'&'+$("form[name='email_form']").serialize();

        $http.post(API_URL+'branch/editEmailBranch', data, contentType).then(function(response) {

        var response = response.data;

       if(response.ResponseCode==200){ /* success case */               

              SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;
                 $scope.applyFilter();
                  $timeout(function(){ 
                $('.modal-header .close').click();
                 }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }  

    /*Inactive data*/

    $scope.Inactive = function ()

    {

        //$scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='active_form']").serialize();

        $http.post(API_URL+'branch/Inactive', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

              SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;
                 $('.modal-header .close').click();

            }else{

                ErrorPopup(response.Message);

            }

              $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);         

        });

    }  

    /*Active data*/

    $scope.Active = function ()

    {

        //$scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='inactive_form']").serialize();

        $http.post(API_URL+'branch/Active', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

              SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;
                 $('.modal-header .close').click();

            }else{

                ErrorPopup(response.Message);

            }

              $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);          

        });

    } 

   
   $scope.loadFormDelete = function(Position,UserID)
    {
      
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL+'branch/delete', 'SessionKey='+SessionKey+'&UserID='+UserID, contentType).then(function(response) {
              
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
       
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }




}); 



/* sortable - starts */

function tblsort() {



  var fixHelper = function(e, ui) {

    ui.children().each(function() {

        $(this).width($(this).width());

    });

    return ui;

}



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

        data: order,

        stop: function(response) {

            if (response.status == "success") {

                window.location.href = window.location.href;

            } else {

                alert('Some error occurred');

            }

        }

    });

}







}

function getCities(state_id){
  angular.element(document.getElementById('branches-body')).scope().getCities(state_id);  
}
/* sortable - ends */