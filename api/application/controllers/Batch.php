<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Batch extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('Batch_model');
		$this->load->model('Entity_model');
		$this->load->model('Common_model');
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getBatch_post() 
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('BatchID', 'BatchID', 'trim');
		$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');
		$this->form_validation->set_rules('CategoryID', 'CategoryID', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		
		 
	  	$BatchData = $this->Batch_model->getBatch($this->input->post('BatchID'),$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		
		if(!empty($BatchData)){
			$this->Return['Data'] = $BatchData['Data'];
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/batch/getBatchsAssign
	Input (Sample JSON): 		
	*/
	public function getBatchcreate_post()
	{
		$this->form_validation->set_rules('FacultyID', 'FacultyID', 'trim|required');
		$this->form_validation->validation($this); 
		//echo $this->input->post('BatchID'); die();
		 
	  	$BatchUserData = $this->Batch_model->getBatchcreate($this->input->post('FacultyID'));
		//print_r($BatchData); die();
		if(!empty($BatchUserData)){
			$this->Return['Data'] = $BatchUserData;
		}	
	}
	/*
	Description: 	Use to get Get single category.
	URL: 			/api/batch/getBatchsAssign
	Input (Sample JSON): 		
	*/
	public function getBatchsAssign_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('AsID', 'AsID', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		//echo $this->input->post('BatchID'); die();
		 
	  	$BatchUserData = $this->Batch_model->getBatchsAssign($this->input->post('AsID'));
		//print_r($BatchData); die();
		if(!empty($BatchUserData)){
			$this->Return['Data'] = $BatchUserData;
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/rolse/module
	Input (Sample JSON): 		
	*/
	public function getmodules_post()
	{
		$BatchDataModule = $this->Batch_model->getmodules($this->input->post('UserTypeID'));

		if(!empty($BatchDataModule)){
			$this->Return['Data'] = $BatchDataModule['Data'];
		}	
	}



	/*

	Description: 	Use to add new category

	URL: 			/api_admin/batch/add	

	*/

	public function add_post()

	{  /* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		//$this->form_validation->set_rules('InstituteID', 'InstituteID', 'trim|callback_validateEntityGUID');
		
		$this->form_validation->set_rules('CategoryGUID', 'Select Course','trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchName', 'Batch Name', 'trim|required');
		$this->form_validation->set_rules('Duration', 'Duration', 'trim|required');
		$this->form_validation->set_rules('StartDate', 'Start Date', 'trim|required|callback_validateDate');
		
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$BatchData = $this->Batch_model->addBatch(array('BatchName'=>$this->Post['BatchName'],'Duration'=>$this->Post['Duration'],'StartDate'=>$this->Post['StartDate'],'CourseID'=>$this->CategoryID));
			//echo $BatchData; 
	      if($BatchData == 1){

	      	$this->Return['Message']      	=	"New Batch added successfully."; 
				
			}else{

				
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Batch Name already exist.";
			}


	}

	/*

	Description: 	Use to add new category

	URL: 			/api_admin/addasaining/add	

	*/

	public function addasaining_post()

	{  /* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('CategoryID', 'Select Course', 'trim|required');
		$this->form_validation->set_rules('BatchID', 'Batch Name','trim|required');
		$this->form_validation->set_rules('FacultyID', 'Select Staff','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$BatchData = $this->Batch_model->addBatchAsaining($this->Post);
		if($BatchData==1){

				$this->Return['Message']      	=	"Batch Assigned successfully."; 

		}else{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Already Assigned Batch."; 
		
		}
	}
	/* /user/editBatchs/	*/

	public function editBatchs_post()
    {   
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('BatchName', 'BatchName', 'trim|required');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|required');
		$this->form_validation->set_rules('Duration', 'Duration', 'trim|required');
		$this->form_validation->set_rules('LastDuration', 'LastDuration', 'trim|required');  
		$this->form_validation->set_rules('DuratioExtendMonth', 'DuratioExtendMonth', 'trim');
		$this->form_validation->set_rules('StartDate', 'StartDate', 'trim|required|callback_validateDate');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID','trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$BatchData = $this->Batch_model->editBatch(array('BatchGUID'=>$this->Post['BatchGUID'],'BatchName'=>$this->Post['BatchName'],'Duration'=>$this->Post['Duration'],'LastDuration'=>$this->post['LastDuration'],'DuratioExtendMonth'=>$this->post['DuratioExtendMonth'],'StartDate'=>$this->Post['StartDate'],'CourseID'=>$this->CategoryID));

		

		if($BatchData == 1){
			$this->Return['Data'] = $this->Batch_model->getBatchd($this->input->Post('BatchGUID'),array());
			$this->Return['Message']      	=	"Batch updated successfully."; 

		}else{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"This Batch Name already exist.";
		}

	}	



	public function editAssignData_post()
    {   
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('AsID', 'AsID', 'trim|required');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('FacultyID', 'FacultyID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$Editdata= $this->Batch_model->editAssignData(array('AsID'=>$this->input->post('AsID'),'FacultyID'=>$this->input->post('FacultyID'),'BatchID'=>$this->BatchID));


		$this->Return['Data'] = $this->Batch_model->getBatchsAssign($this->input->post('AsID'),array(),TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

	
		$this->Return['Message']      	=	"Batch Assign updated successfully."; 
	}


	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategory
	Input (Sample JSON): 		
	*/
	public function getBatchs_post()
	{
		/* Validation section */
		//$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CategoryData = $this->Batch_model->getBatchByID($this->input->post('BatchGUID'));
		//print_r($CategoryData); die();
		if(!empty($CategoryData)){
			$this->Return['Data'] = $CategoryData;
		}	
	}

	/*Name: 			editBatch_post*/

	public function editBatch_post()
     {  /* Validation section */
        $this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');
		$this->form_validation->set_rules('UserTypeName', 'UserTypeName', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$data = $this->Batch_model->editBatch($this->input->Post('UserTypeID'),$this->input->Post('UserTypeName'));
		$this->Return['Data']= $this->Batch_model->getBatch($this->EntityID,array());
		$this->Return['Message']      	=	"Batch updated successfully."; 

		}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories		
	*/
	public function getContentCategories_post()
	{
		$ContentCategoryData = $this->Category_model->getContentCategories();
		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/batch/getCategories
	
	*/
	public function getCategories_post()
	{   
		
		$CategoryData = $this->Batch_model->geCategories();
		//print_r($CategoryData);
		if(!empty($CategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $CategoryData;
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/batch/getDuration
	
	*/
	public function getDuration_post()
	{   
		//echo $this->input->Post('CategoryGUID'); die();
		$Durationdata = $this->Common_model->getDuration($this->input->Post('CategoryGUID'));
		if(!empty($Durationdata)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $Durationdata;
		}	
	}	


	public function getFaculty_post()
	{
		$this->form_validation->set_rules('CourseID', 'CourseID', 'trim|required');
		$this->form_validation->validation($this);

		$DataFaculty = $this->Batch_model->getFaculty($this->input->Post('CourseID'));
		//print_r($CategoryData);
		if(!empty($DataFaculty)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $DataFaculty;
		}	
	}


	public function delete_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->validation($this);  /* Run validation */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$DataFaculty = $this->Batch_model->deleteBatch($this->EntityID, $this->BatchID);	

		// if($DataFaculty){
		// 	//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
		// 	$this->Return['Data'] = $DataFaculty;
		// }	
	}


	public function Assingdelete_post()  
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('AsID', 'AsID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */
		$DataFaculty = $this->Batch_model->deleteAssingBatch($this->input->Post('AsID'));	

		// if($DataFaculty){
		// 	//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
		// 	$this->Return['Data'] = $DataFaculty;
		// }	
	}


	public function getStudentsList_post()
	{
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID','trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$StudentsList = $this->Batch_model->getStudentsList($this->EntityID,@$this->BatchID,@$this->Post['EntryDate']);	

		if($StudentsList){
			$this->Return['Data'] = $StudentsList;
		}	
	}



	public function getAttendanceData_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('StartDate', 'StartDate','trim');
		$this->form_validation->set_rules('EndDate', 'EndDate','trim');
		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$AttendanceData = $this->Batch_model->getAttendanceData($this->EntityID,$this->input->post());	

		if(count($AttendanceData) > 0){
			$this->Return['Data'] = $AttendanceData;
		}else{
			$this->Return['Data'] = [];
			$this->Return['Message'] = "Data not found";
		}	
	}



	public function getAttendanceCountByBatch_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('BatchID', 'BatchID','trim');
		$this->form_validation->set_rules('StartDate', 'StartDate','trim');
		$this->form_validation->set_rules('EndDate', 'EndDate','trim');
		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$AttendanceData = $this->Batch_model->getAttendanceCountByBatch($this->EntityID,$this->input->post());	

		if($AttendanceData){
			$this->Return['Data'] = $AttendanceData;
		}else{
			$this->Return['Data'] = 0;
			$this->Return['Message'] = "Data not found";
		}	
	}


	public function getAttendanceDetailByBatch_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('BatchID', 'BatchID','trim');
		$this->form_validation->set_rules('EntryDate', 'EntryDate','trim');
		$this->form_validation->set_rules('Status', 'Status','trim');
		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$AttendanceData = $this->Batch_model->getAttendanceDetailByBatch($this->EntityID,$this->input->post());	

		if($AttendanceData){
			$this->Return['Data'] = $AttendanceData;
		}else{
			$this->Return['Data'] = 0;
			$this->Return['Message'] = "Data not found";
		}	
	}




	public function SetAttendance_post()
	{
		$this->form_validation->set_rules('BatchID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('EntryDate', 'EntryDate','trim|required');
		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$AttendanceStatus = $this->Batch_model->SetAttendanceBulk($this->EntityID, $this->BatchID, $this->Post);	
		
		if($AttendanceStatus == 1)
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Please select atleast one student.";
		}
		else
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "Attendance saved successfully.";
		}	
	}


	public function SetAttendanceByFaculty_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('BatchID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('EntryDate', 'EntryDate','trim|required');
		$this->form_validation->set_rules('uid', 'Attendance','trim|required');
		$this->form_validation->validation($this);  /* Run validation */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$AttendanceStatus = $this->Batch_model->SetAttendanceByFaculty($this->EntityID, $this->BatchID, $this->Post);	
		
		if($AttendanceStatus == 1)
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Please select atleast one student.";
		}
		else
		{
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "Attendance saved successfully.";
		}	
	}

}