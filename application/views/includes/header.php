<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="no-cache">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php echo SITE_NAME;?></title>
	<base href="<?php echo base_url(); ?>">
	<!-- FONT -->
	<link href='https://fonts.googleapis.com/css?family=Noto+Sans|Roboto:300,400,500|Poppins:100,200,300,400,500,600' rel='stylesheet'>
	<!-- BOOTSTRAP CSS -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> 
	<!-- <link rel="stylesheet"  href="https://blackrockdigital.github.io/startbootstrap-sb-admin-2/vendor/bootstrap/css/bootstrap.min.css">
 -->	<!-- Alertify CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>asset/plugins/alertify/css/alertify.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>asset/plugins/alertify/css/themes/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">
	<!-- Plugins CSS -->
	<?php  //print_r($this->session->userdata('UserData')); die; 
	if(!empty($css)){foreach($css as $value){  ?>
		<link rel="stylesheet" href="<?php echo $value; ?>" />
	<?php }} ?>
	<!-- CUSTOM CSS -->
	<link rel="stylesheet" href="asset/css/app.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="asset/css/custom.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="asset/css/glyphicon.css" type="text/css" media="screen" />

	<script>
		var BASE_URL = "<?php echo base_url(); ?>";
		var API_URL = "<?php echo API_URL; ?>";
		var PATH_TEMPLATE = "<?php echo PATH_TEMPLATE; ?>";
		var UserGUID, UserTypeID, ParentCategoryGUID, MasterFranchisee = '', FromEmail = '', FromName = '';

		<?php if($this->session->userdata('UserData')){?>
			var module = "<?php echo $this->ModuleData['ModuleName'];?>";
			var SessionKey = "<?php echo $this->SessionKey; ?>";
			var UserGUID = "<?php echo $this->session->userdata('UserData')['UserGUID']; ?>";
			console.log(UserGUID);
			var UserTypeID = "<?php echo $this->UserTypeID; ?>";
			<?php //if(!empty($this->InstituteGUID)){ ?>
				var InstituteGUID = "<?php echo $this->session->userdata('UserData')['InstituteGUID']; ?>";
				var InstituteID = "<?php echo $this->session->userdata('UserData')['InstituteID']; ?>";
				var InstituteName = "<?php echo $this->session->userdata('UserData')['InstituteName'];?>";
			<?php //} ?>

			MasterFranchisee = "<?php echo $this->session->userdata('UserData')['MasterFranchisee']; ?>";
			FranchiseeAssignCount = "<?php echo $this->session->userdata('UserData')['FranchiseeAssignCount']; ?>";
			FromEmail = "<?php echo $this->session->userdata('UserData')['Email']; ?>";
			FromName = "<?php echo $this->session->userdata('UserData')['FirstName']; ?>";
			
			<?php if(!empty($_GET['ParentCategoryGUID'])){ ?>
				var ParentCategoryGUID = "<?php echo $_GET['ParentCategoryGUID']; ?>";
			<?php } ?>
			var CurrentCategoryGUID = "";
			<?php if(!empty($_GET['CategoryGUID'])){ ?>
				var CurrentCategoryGUID = "<?php echo $_GET['CategoryGUID']; ?>";
			<?php } ?>

			<?php if(!empty($this->session->userdata('UserData')['InstituteProfilePic'])){ ?>
				var InstituteProfilePic = "<?php echo $this->session->userdata('UserData')['InstituteProfilePic']; ?>";
			<?php }else{ ?>
				var InstituteProfilePic = "<?php echo base_url(); ?>/api/asset/img/emailer/logo.png";
			<?php } ?>

			
		<?php } ?>
	</script>

	<!-- Jquery -->
	<script src="<?php echo base_url(); ?>asset/plugins/common/jquery-2.2.4.min.js"></script>


	<script src="<?php echo base_url(); ?>asset/plugins/common/jquery-ui.min.js"></script>

	<!-- AngularJS -->
	<script src="<?php echo base_url(); ?>asset/plugins/common/angular.min.js"></script>
	<script src="asset/plugins/ng-infinite-scroll.min.js"></script>

	<script src="<?php echo base_url(); ?>asset/plugins/common/angular-sanitize.js"></script>

	<!-- <script type="text/javascript">
	  var verifyCallback = function(response) {
        console.log(response);
      };
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6Lf3RagUAAAAAPxZcgz-2ftBEazFzDU5la12vhy9',
          'callback' : verifyCallback,
          'theme' : 'dark'
        });
      };
    </script>  -->   
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<style type="text/css">
		iframe{
			width: 100%!important;
		}
		.g-recaptcha div{
			width: 100%!important;
		}
		.grecaptcha-badge{
			visibility: collapse !important;  
		}
		
		#PageControllerLoader { display: block; }
	</style>
</head>
<body ng-app="myApp" ng-controller="MainController">