<?php
  $MasterFranchisee = $this->session->userdata('UserData')['MasterFranchisee'];
  $basename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
   if( strpos( $_SERVER['REQUEST_URI'], "dashboard" ) == false ) {
      
  }else{
    ?>
    <style type="text/css">
      .block {
          display: block!important;
          background-color: #F3F3F3!important;
          border-radius: 0px!important;
          box-shadow: none!important;
          overflow: hidden!important;
          color: #41A6E0;
      }
    </style>
    <?php 
  }
?>
<style type="text/css">
  .glyphicon {
      position: relative;
      top: -20px;
      display: inline-block;
      font-family: 'Glyphicons Halflings';
      font-style: normal;
      font-weight: normal;
      line-height: 1;
      -webkit-font-smoothing: antialiased;
      font-size: 20px;
      padding: 4px;
      /*color: #FFF;*/
  }
  .glyphicon-align-justify:before{
    color: #FFF;
  }
</style>
<script type="text/javascript">
  var currentModule = '<?php echo  $this->ModuleData['ModuleName']; ?>';
  var currentModuleID = '<?php echo  $this->ModuleData['ModuleID']; ?>';
</script>
<div class="top_header d-flex">
  <div class="col-md-3 offset-md-9 offset-sm-10 offset-xs-10 d-flex justify-content-around">
        <div class="notification dropdown">
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <?php if($this->session->userdata('UserData')['Notification']['Data']['Records']){
                  echo '<a class="nav-link dropdown-toggle text-white" href="#" id="navbardrop" data-toggle="dropdown" ng-click="NotificationMarkAllRead()">  <i class="fa fa-bell" aria-hidden="true"></i> </a>';
                }else{

                }

                ?>
                
                
                <div class="dropdown-menu dropdown-menu-right notify-list">
                    <ul  class="list-unstyled">
                       <li><div class="dropdown-title"> You have new notification </div></li>
                   
                        <div class="notif-center p-3">
                          <?php if(!empty($this->session->userdata('UserData')['Notification'])){
                              foreach ($this->session->userdata('UserData')['Notification']['Data']['Records'] as $key => $value) {
                                if(!empty($value['ProfilePic'])){
                                  $ProfilePic = $value['ProfilePic'];
                                }else{
                                  $ProfilePic = '<i class="fa fa-user-plus"></i>';
                                }
                                $t1 = StrToTime ( $value['EntryDate'] );
                                $t2 = StrToTime ( date("Y-m-d H:i:s") );
                                

                                $delta_T = ($t2 - $t1);
                                $day = round(($delta_T % 604800) / 86400); 
                                $hours = round((($delta_T % 604800) % 86400) / 3600); 
                                $minutes = round(((($delta_T % 604800) % 86400) % 3600) / 60); 
                                $sec = round((((($delta_T % 604800) % 86400) % 3600) % 60));

                                if($day != 0){
                                  $ago = $day.' day ago';
                                }else if($hours == 0){
                                  $ago = $minutes.' minutes ago';
                                }else{
                                  $ago = $hours.' hours ago';
                                }
                                // $ago = round((strtotime($value['EntryDate']) - strtotime(date("Y-m-d H:i:s"))/(60*60));
                                echo '<a href="#" >
                                  <div class="notif-icon bg-primary"> '.$ProfilePic.' </div>
                                  <div class="notif-content pl-3">
                                    <p class="mb-0"> '.$value['NotificationText'].' </p>
                                    <span class="time">Before '.$ago.' </span> 
                                  </div>
                                </a>';
                             } 
                          } ?>

                         <!--  <a href="#">
                            <div class="notif-icon bg-primary"> <i class="fa fa-user-plus"></i> </div>
                            <div class="notif-content pl-3">
                              <p class="mb-0"> New user registered </p>
                              <span class="time">5 minutes ago</span> 
                            </div>
                          </a>

                          <a href="#"  class="mb-0">
                            <div class="notif-icon bg-primary"> <i class="fa fa-user-plus"></i> </div>
                            <div class="notif-content pl-3">
                              <p class="mb-0"> New user registered </p>
                              <span class="time">5 minutes ago</span> 
                            </div>
                          </a> -->
                        </div>

                    </ul>
                </div>
              </li>
            </ul>
        </div>

      <div class="dropdown">
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <span style="margin-right: 5px;position: inherit;top: -25px;"> Welcome <?php echo $this->session->userdata('UserData')['FullName']; ?> </span>
            <a class="glyphicon glyphicon-align-justify" href="#" id="navbardrop" data-toggle="dropdown" style="top: -20px;"></a>
            <div class="dropdown-menu dropdown-menu-right" data-display="static" style="top:10px!important;">
              <a class="dropdown-item right-dropdown-item" href="<?php echo base_url().'profile'; ?>"  >Profile</a>
              <a class="dropdown-item right-dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#changePassword_modal"  >Change Password</a>
            
              <a class="dropdown-item right-dropdown-item" href="<?php echo base_url().'signin/signout/'.$this->SessionData['SessionKey'];?>">Sign Out</a>
            </div>
          </li>
        </ul>
      </div> 
  </div>
</div> 
<div class="main-navbar">
    <nav class="navbar navbar-expand-sm fixed-top navigation">
      <div class="container-fluid">
        <div class="navbar-header"> 

          <?php if(!empty($this->session->userdata('UserData')['InstituteProfilePic'])){ 
            $InstituteProfilePic = $this->session->userdata('UserData')['InstituteProfilePic'];
           }else{
            $InstituteProfilePic = API_URL."asset/img/emailer/logo.png";
           } ?>

        <a class="navbar-brand" href="#"><img src="<?php echo $InstituteProfilePic ?>" style="height:64px"></a>

      </div>

    <!-- navigation -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
        <?php foreach($this->Menu as $Value)
        { 
            if($Value['ModuleName'] == "mark_attendance") continue;

          if($basename == "dashboard" && $Value['ControlName'] != "Manage Courses"  && $Value['ControlName'] != "Manage Staff"  && $Value['ControlName'] != "Communications" && $Value['ControlName'] != "Enquiry Handling"  && $Value['ControlName'] != "Manage Library" && $Value['ControlName'] != "Manage Branch"  && $Value['ControlName'] != "Post Jobs" && $this->session->userdata('UserData')['UserTypeID'] != 1){
            continue;
          }
          else if($Value['ControlName'] == 'Manage Profile' || ($Value['ControlName'] == 'My Business' && $this->session->userdata('UserData')['UserTypeID'] != 1)){
             continue;
          }
          else if(($this->session->userdata('UserData')['MasterFranchisee'] == 'no' && $this->session->userdata('UserData')['UserTypeID'] != 1) && ($Value['ControlName'] == 'Manage Order' || $Value['ControlName'] == 'Inquiry Handling')) {
            continue;
          }  ?>
          <?php if(empty($Value['ChildMenu'])){ ?>            
            <?php if($this->ModuleData['ModuleName'] == "content/details_list" && $Value['ControlName'] == "Manage Content"){ ?>
              <li class="nav-item ">        
                <a class="nav-link  active" href="<?php echo base_url().$Value['ModuleName'];?>"><i class="<?php echo @$Value['ModuleIcon']; ?>"></i><?php echo $Value['ControlName'];?></a>
              </li> 
            <?php }else{ ?>
              <li class="nav-item ">        
                <a class="nav-link  <?php if($Value['ModuleName']==$this->ModuleData['ModuleName']){echo "active";} ?>" href="<?php echo base_url().$Value['ModuleName'];?>"><i class="<?php echo @$Value['ModuleIcon']; ?>"></i><?php echo $Value['ControlName'];?></a>
              </li>
            <?php } ?>                   
          <?php }else{ ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              <i class="<?php echo @$Value['ModuleIcon']; ?>"></i><?php echo $Value['ControlName'];?>
            </a>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
              <?php foreach($Value['ChildMenu'] as $Value0){
                if($basename == "dashboard" && $Value['ControlName'] == "Communications" && ($Value0['ModuleName'] == "bulksms" || $Value0['ModuleName'] == "bulkemail")){
                    continue;
                } 
                if($this->session->userdata('UserData')['UserTypeID'] == 1 && $Value0['ControlName'] == 'Order Keys') {
                continue;
                }
                if($this->session->userdata('UserData')['UserTypeID'] == 1 && $Value0['ControlName'] == 'Allocate') {
                continue;
                }
                if($this->session->userdata('UserData')['UserTypeID'] == 1 && $Value0['ControlName'] == 'Allocate Keys') {
                continue;
                }
                // if(($this->session->userdata('UserData')['MasterFranchisee'] == 'no' && $this->session->userdata('UserData')['UserTypeID'] != 1) && $Value0['ControlName'] == 'Pricing' ) {
                // continue;
                // }  ?>
                <?php if(($this->ModuleData['ModuleName'] == "students/students_list" && $Value0['ControlName'] == "Students") || ($this->ModuleData['ModuleName'] == "keys/used" && $Value0['ControlName'] == "Inventory") || ($this->ModuleData['ModuleName'] == "questionbank/questions_list" && $Value0['ControlName'] == "Question Repository") || ($this->ModuleData['ModuleName'] == "questionpaper/list" && $Value0['ControlName'] == "Question Paper")  || ($this->ModuleData['ModuleName'] == "questionpaper/paper" && $Value0['ControlName'] == "Question Paper")  || ($this->ModuleData['ModuleName'] == "questionassign/list" && $Value0['ControlName'] == "Assign Test")  || ($this->ModuleData['ModuleName'] == "questionassign/results" && $Value0['ControlName'] == "Assign Test")  || ($this->ModuleData['ModuleName'] == "questionassign/result_paper" && $Value0['ControlName'] == "Assign Test")  || ($this->ModuleData['ModuleName'] == "questionassign/quiz" && $Value0['ControlName'] == "Assign Quiz")  || ($this->ModuleData['ModuleName'] == "bulksms/orders" && $Value0['ControlName'] == "Bulk SMS")  || ($this->ModuleData['ModuleName'] == "bulksms/orders" && $Value0['ControlName'] == "Bulk SMS")){ ?>
                <li><a class="dropdown-item active" href="<?php echo base_url().$Value0['ModuleName'];?>"><?php echo $Value0['ControlName'];?></a></li>
              <?php }else{ ?>
                <li><a class="dropdown-item <?php if($Value0['ModuleName']==$this->ModuleData['ModuleName']){echo "active";} ?>" href="<?php echo base_url().$Value0['ModuleName'];?>"><?php echo $Value0['ControlName'];?></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>

      <?php } ?>
    </ul>
    </div>


    <!-- /navigation -->

    <div class="attr-nav w-100"> 
      <!-- Right nav -->

    <!--   <div class="dropdown">
        <div class="nav-item">

          <a class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ng-click="getNotifications();">
            <i class="fa fa-bell-o fa-lg text-white" ></i>
            <span class="badge" ng-show="data.notificationCount>0">{{data.notificationCount}}</span>
          </a>

          <ul class="dropdown-menu notification_menu">
            <li class="text-light bg-dark px-2 py-2">Notifications</li>

            <li class="notification-box text-center" ng-if="!notificationList">
              <a class="dropdown-item" href="javascript:void(0)">
              <div class="row">
                <div class="not-txt">
                  No new notifications.
                </div>
              </div>
            </a>
          </li>

          <li class="notification-box" ng-repeat="(key, row) in notificationList">
            <a class="dropdown-item" href="./order" target="_self">
            <div class="row">
              <div class="not-txt">
                {{row.NotificationText}}
              </div>
              <div class="not-date"><small class="text-warning">{{row.EntryDate}}</small></div>
            </div>
          </a>
        </li>

      </ul>
    </div>
    </div> -->
    </div>
    </div>
    </nav>

<?php
$blockdiv = "none";
if($basename == "timescheduling")
{
    $blockdiv = "block";
}    
?>
    <div id="mainFrame" style="display: <?php echo $blockdiv;?>">
      <div class="container-fluid">
        <section class="block">