<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Roles_model');
		$this->load->model('Staff_model');
		$this->load->model('Users_model');
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
		$this->load->model('Recovery_model');
		$this->load->library("excel");
	}
  

	/*
	Description: 	Use to get Import Data.
	URL: 			/api/Staff/importStaff
	Input (Sample JSON): 		
	*/
	public function importStaff_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserTypeID', 'Role', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$MediaURL = explode("/", $this->input->post('MediaURL'));
		$mediaName = end($MediaURL);

		$staffData = $this->excel->excel_to_array("uploads/staff/".$mediaName);
		$arr_keys = array_keys($staffData[0]);

		// print_r($arr_keys);
		// print_r($questionData); 
		// echo $arr_keys[0];
		// echo $questionData[0][$arr_keys[0]];
		// die;

		$target_keys = array("FIRST NAME","LAST NAME","EMAIL","ADMIN ACCESS","APP ACCESS","CONTACT NO.","GENDER","DATE OF BIRTH","ADDRESS","ZIP CODE");

		//echo $target_keys[1];
		
		foreach ($target_keys as $key => $value) {
			//echo strpos($arr_keys[$key], $value); die;
			if (strpos($arr_keys[$key], $value) !== false) { 
				//echo $arr_keys[$key]."<br>".$value;
				
			}else{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	'Sorry! This file format is not correct. Download excel to get format to add students.';
				die;
			}
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$PostData = array();
		for ($i=0; $i < count($staffData); $i++) {
			$PostData['FirstName'] = $staffData[$i][$arr_keys[0]];
			$PostData['LastName'] = $staffData[$i][$arr_keys[1]];
			$PostData['Email'] = $staffData[$i][$arr_keys[2]];

			if($staffData[$i][$arr_keys[3]] == 1){
				$PostData['Access'] = 'Yes';
			}else{
				$PostData['Access'] = 'No';
			}

			if($staffData[$i][$arr_keys[4]] == 1){
				$PostData['AppAccess'] = 'Yes';
			}else{
				$PostData['AppAccess'] = 'No';
			}			
			
			$PostData['PhoneNumber'] = $staffData[$i][$arr_keys[5]];

			if($staffData[$i][$arr_keys[5]] == 1){
				$PostData['Gender'] = 'Male';
			}else{
				$PostData['Gender'] = 'Female';
			}	

			$PostData['BirthDate'] = $staffData[$i][$arr_keys[7]];
			$PostData['Address'] = $staffData[$i][$arr_keys[8]];
			$PostData['CityCode'] = $staffData[$i][$arr_keys[9]];
			//print_r($PostData); die;
			$UserData = $this->Staff_model->addUser($this->EntityID,$PostData, $this->input->post('UserTypeID'), 1, 1);

			if(!$UserData){
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Phone number and email already exist.";  
			}
		}

		$this->Return['Message']      	=	count($staffData)." Staff members are added successfully.";
	}



	public function index_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim');
		//$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->set_rules('PageType', 'PageType', 'trim');
		$this->form_validation->set_rules('AppAccess', 'AppAccess', 'trim');   	
		$this->form_validation->set_rules('AdminAccess', 'AdminAccess', 'trim');	
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$UsersData=$this->Staff_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic,MaritalStatus,Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		//$UsersData=$this->Staff_model->getUsers('',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		//print_r($UsersData); die;
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}	
	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	public function getProfile_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim');
		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */

		$UserData=$this->Staff_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array('UserID'=>$this->Post['UserID']));

		if($UserData){

			$this->Return['Data'] = $UserData;

		}	

	}
	/*
	Name: 			getCourse
	Description: 	Use to get course list.
	URL: 			/staff/getCourse
	*/
	public function getCourse_post()
    {   
    	$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$CourseData=$this->Staff_model->getCourse($InstituteID);

		if($CourseData){

			$this->Return['Data'] = $CourseData;

		}	

	}

	public function getSubject_post()
    {   
    	$this->form_validation->set_rules('CategoryID', 'CategoryID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		$SubjectData=$this->Staff_model->getSubject($this->input->Post('CategoryID'));

		if($SubjectData){

			$this->Return['Data'] = $SubjectData['Data'];

		}	

	}

	/*
	Name: 			getModule
	Description: 	Use to get module.
	URL: 			/api_admin/users/geRoles
	*/
	public function getusertype_post()
	{
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$UserTypeData = $this->Staff_model->getusertype($this->EntityID);

		if(!empty($UserTypeData)){
			$this->Return['Data'] = $UserTypeData['Data'];
		}
	}


	/*
	Description: 	Use to update user profile info.
	URL: 			/api_admin/entity/changeStatus/	
	*/
	public function changeStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->Entity_model->updateEntityInfo($this->UserID, array("StatusID"=>$this->StatusID));
		$this->Return['Data']=$this->Staff_model->getUsers('FirstName,LastName,Email,ProfilePic,Status',array("UserID" => $this->UserID));
		$this->Return['Message']  =	"Status has been changed.";
	}



	/*
	Name: 			updateUserInfo
	Description: 	Use to update user profile info.
	URL: 			/api_admin/updateUserInfo/	
	*/
	public function updateUserInfo_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');
		$this->form_validation->set_rules('LastName', 'Last Name', 'trim|required');
		if($this->input->Post('Access')=='Yes'){
			$this->form_validation->set_rules('Email', 'Email', 'trim|required');
			$this->form_validation->set_rules('PhoneNumber', 'Contact No', 'trim|required|min_length[10]|max_length[10]',array('is_unique'=>'Already Used Phone Number'));
			$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required', array("required"=>"Role is required"));
			$this->form_validation->set_rules('AppAccess', 'App Access','trim|required');
			if($this->input->Post('UserTypeID') == 11){
				$this->form_validation->set_rules('CourseID[]', 'Course','trim|required');
				$this->form_validation->set_rules('SubjectID[]', 'Subject','trim|required');		
			}
		}else{			
			$this->form_validation->set_rules('Email', 'Email', 'trim|required');
			$this->form_validation->set_rules('PhoneNumber', 'Contact No', 'trim|required|min_length[10]|max_length[10]',array('is_unique'=>'Already Used Phone Number'));
			$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required', array("required"=>"Role is required"));
			$this->form_validation->set_rules('AppAccess', 'App Access','trim|required');
			if($this->input->Post('UserTypeID') == 11){
				$this->form_validation->set_rules('CourseID[]', 'Course','trim|required');
				$this->form_validation->set_rules('SubjectID[]', 'Subject','trim|required');		
			}	

			if($this->input->post('type') == 'PersonalDetails'){
				/*Personal Details*/
				$this->form_validation->set_rules('Gender', 'Gender','trim|required');
				$this->form_validation->set_rules('BirthDate', 'Date of Birth','trim|required');
				$this->form_validation->set_rules('MaritalStatus', 'Marital Status','trim|required');
				if($this->input->Post('MaritalStatus')=='married'){
					$this->form_validation->set_rules('AnniversaryDate', 'Anniversary Date','trim|required');
				}			
				$this->form_validation->set_rules('Address', 'Address','trim|required');
				$this->form_validation->set_rules('StateID', 'State Name','trim|required');
				$this->form_validation->set_rules('CityName', 'City Name','trim|required');			
				$this->form_validation->set_rules('CityCode', 'ZIP Code','trim|required');
			}
			else if($this->input->post('type') == 'EmergencyDetails'){
				/*Emergency Details*/
				$this->form_validation->set_rules('Emergency', 'Emergency','trim|required');
				$this->form_validation->set_rules('RelationWithEmergency', 'Relation With Emergency','trim');
				$this->form_validation->set_rules('EmergencyPhoneNumber', 'Emergency PhoneNumber','trim');
				$this->form_validation->set_rules('EmergencyGender', 'Emergency Gender','trim');
				$this->form_validation->set_rules('EmergencyAddress', 'Emergency Address','trim');
				$this->form_validation->set_rules('EmergencyState', 'Emergency State','trim');
				$this->form_validation->set_rules('EmergencyCityName', 'Emergency City Name','trim');
				$this->form_validation->set_rules('EmergencyCityCode', 'Emergency City Code','trim');
			}
			else if($this->input->post('type') == 'NomineeDetails'){
				/*nominee*/
				$this->form_validation->set_rules('Nominee', 'Nominee','trim|required');
				$this->form_validation->set_rules('RelationWithNominee', 'Relation With Nominee','trim');
				$this->form_validation->set_rules('NomineePhoneNumber', 'Nominee Phone Number','trim');
				$this->form_validation->set_rules('NomineeGender', 'Nominee Gender','trim');
				$this->form_validation->set_rules('NomineeAddress', 'Nominee Address','trim');
				$this->form_validation->set_rules('NomineeState', 'Nominee State','trim');
				$this->form_validation->set_rules('NomineeCityName', 'Nominee City Name','trim');
				$this->form_validation->set_rules('NomineeCityCode', 'Nominee City Code','trim');
			}
			else if($this->input->post('type') == 'Qualification'){
				/*Eduction Qualification*/
				$this->form_validation->set_rules('Qualification[]', 'Qualification','trim|required');
				$this->form_validation->set_rules('Specialization[]', 'Specialization','trim|required');
				$this->form_validation->set_rules('Univercity[]', 'University','trim|required');
				$this->form_validation->set_rules('PassingYear[]', 'PassingYear','trim|required');
				$this->form_validation->set_rules('Percentage[]', 'Percentage','trim|required');
			}
			else if($this->input->post('type') == 'JobsDetails'){
				/* job Profile */
				$this->form_validation->set_rules('Job', 'Job','trim|required');
				$this->form_validation->set_rules('Department', 'Department','trim|required');
				$this->form_validation->set_rules('JobDesignation', 'Job Designation','trim|required');
				$this->form_validation->set_rules('ReportingManager', 'Reporting Manager','trim|required');
				$this->form_validation->set_rules('JoiningDate', 'Joining Date','trim|required');
				$this->form_validation->set_rules('AppraisalDate', 'AppraisalDate','trim|required');
			}
			else if($this->input->post('type') == 'WorkExperience'){
				/* Work Experience */
				$this->form_validation->set_rules('Employer[]', 'Employer','trim');
				$this->form_validation->set_rules('Industries[]', 'Industries','trim');
				$this->form_validation->set_rules('ExpDesignation[]', 'ExpDesignation','trim');
				$this->form_validation->set_rules('JobProfile[]', 'Job Profile','trim');
				$this->form_validation->set_rules('From[]', 'From','trim');
				$this->form_validation->set_rules('To[]', 'To','trim');
			}
			else if($this->input->post('type') == 'SocialMedia'){
				/* Social info and course subject */
				$this->form_validation->set_rules('FacebookURL', 'FacebookURL','trim');
				$this->form_validation->set_rules('TwitterURL', 'TwitterURL','trim');
				$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL','trim');
				$this->form_validation->set_rules('GoogleURL', 'GoogleURL','trim');

				if($this->input->post('FacebookURL') != "")
				{
					if(!validate_input_url($this->input->post('FacebookURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid Facebook URL.";
						die;
					}
				}

				if($this->input->post('TwitterURL') != "")
				{
					if(!validate_input_url($this->input->post('TwitterURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid Twitter URL.";
						die;
					}
				}
				
				if($this->input->post('LinkedInURL') != "")
				{
					if(!validate_input_url($this->input->post('LinkedInURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid LinkedIn URL.";
						die;
					}
				}

				if($this->input->post('GoogleURL') != "")
				{
					if(!validate_input_url($this->input->post('GoogleURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid Google URL.";
						die;
					}
				}
			}
		}

		
		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');	
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');	
		$this->form_validation->set_rules('MediaName','MediaName','trim');
		$this->form_validation->set_rules('UserID','User ID','trim');		
		$this->form_validation->validation($this);  	
		

		$this->Staff_model->updateUserInfo($this->input->Post('UserID'), array_merge($this->Post, array("SkipPhoneNoVerification"=>true)));

		$this->Return['Data'] = $this->Staff_model->getUsers('StatusID,Status,ProfilePic,Email,Username,Gender,BirthDate,PhoneNumber,UserTypeName,RegisteredOn,LastLoginDate',array("UserID" => $this->input->Post('UserID')));
		$this->Return['Message']  =	"Successfully updated."; 	
	}

	public function delete_post()
	{
		/* Validation section */
		$data=$this->Staff_model->delete($this->input->Post('UserID'));

	if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
	}

	/*
	Name: 			add
	Description: 	Use to register user to system.
	URL: 			/api_admin/users/add/
	*/
	public function add_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FirstName', 'First Name', 'trim|required');
		$this->form_validation->set_rules('LastName', 'Last Name', 'trim|required');
		if($this->input->Post('Access')=='Yes'){
			$this->form_validation->set_rules('Email', 'Email', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail');
			$this->form_validation->set_rules('PhoneNumber', 'Contact No', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]',array('is_unique'=>'Already Used Phone Number'));
			$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required', array("required"=>"Role is required"));
			$this->form_validation->set_rules('AppAccess', 'App Access','trim|required');
			if($this->input->Post('UserTypeID') == 11){
				$this->form_validation->set_rules('CourseID[]', 'Course','trim|required');
				$this->form_validation->set_rules('SubjectID[]', 'Subject','trim|required');		
			}
		}else{			
			$this->form_validation->set_rules('Email', 'Email', 'trim|required');
			$this->form_validation->set_rules('PhoneNumber', 'Contact No', 'trim|required|is_unique[tbl_users.PhoneNumber]|min_length[10]|max_length[10]',array('is_unique'=>'Already Used Phone Number'));
			$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required', array("required"=>"Role is required"));
			$this->form_validation->set_rules('AppAccess', 'App Access','trim|required');
			if($this->input->Post('UserTypeID') == 11){
				$this->form_validation->set_rules('CourseID[]', 'Course','trim|required');
				$this->form_validation->set_rules('SubjectID[]', 'Subject','trim|required');		
			}	

			if($this->input->post('type') == 'PersonalDetails'){
				/*Personal Details*/
				$this->form_validation->set_rules('Gender', 'Gender','trim|required');
				$this->form_validation->set_rules('BirthDate', 'Date of Birth','trim|required');
				$this->form_validation->set_rules('MaritalStatus', 'Marital Status','trim|required');
				if($this->input->Post('MaritalStatus')=='married'){
					$this->form_validation->set_rules('AnniversaryDate', 'Anniversary Date','trim|required');
				}			
				$this->form_validation->set_rules('Address', 'Address','trim|required');
				$this->form_validation->set_rules('StateID', 'State Name','trim|required');
				$this->form_validation->set_rules('CityName', 'City Name','trim|required');			
				$this->form_validation->set_rules('CityCode', 'ZIP Code','trim|required');
			}
			else if($this->input->post('type') == 'EmergencyDetails'){
				/*Emergency Details*/
				$this->form_validation->set_rules('Emergency', 'Emergency','trim|required');
				$this->form_validation->set_rules('RelationWithEmergency', 'Relation With Emergency','trim');
				$this->form_validation->set_rules('EmergencyPhoneNumber', 'Emergency PhoneNumber','trim');
				$this->form_validation->set_rules('EmergencyGender', 'Emergency Gender','trim');
				$this->form_validation->set_rules('EmergencyAddress', 'Emergency Address','trim');
				$this->form_validation->set_rules('EmergencyState', 'Emergency State','trim');
				$this->form_validation->set_rules('EmergencyCityName', 'Emergency City Name','trim');
				$this->form_validation->set_rules('EmergencyCityCode', 'Emergency City Code','trim');
			}
			else if($this->input->post('type') == 'NomineeDetails'){
				/*nominee*/
				$this->form_validation->set_rules('Nominee', 'Nominee','trim|required');
				$this->form_validation->set_rules('RelationWithNominee', 'Relation With Nominee','trim');
				$this->form_validation->set_rules('NomineePhoneNumber', 'Nominee Phone Number','trim');
				$this->form_validation->set_rules('NomineeGender', 'Nominee Gender','trim');
				$this->form_validation->set_rules('NomineeAddress', 'Nominee Address','trim');
				$this->form_validation->set_rules('NomineeState', 'Nominee State','trim');
				$this->form_validation->set_rules('NomineeCityName', 'Nominee City Name','trim');
				$this->form_validation->set_rules('NomineeCityCode', 'Nominee City Code','trim');
			}
			else if($this->input->post('type') == 'Qualification'){
				/*Eduction Qualification*/
				$this->form_validation->set_rules('Qualification[]', 'Qualification','trim|required');
				$this->form_validation->set_rules('Specialization[]', 'Specialization','trim|required');
				$this->form_validation->set_rules('Univercity[]', 'University','trim|required');
				$this->form_validation->set_rules('PassingYear[]', 'PassingYear','trim|required');
				$this->form_validation->set_rules('Percentage[]', 'Percentage','trim|required');
			}
			else if($this->input->post('type') == 'JobsDetails'){
				/* job Profile */
				$this->form_validation->set_rules('Job', 'Job','trim|required');
				$this->form_validation->set_rules('Department', 'Department','trim|required');
				$this->form_validation->set_rules('JobDesignation', 'Job Designation','trim|required');
				$this->form_validation->set_rules('ReportingManager', 'Reporting Manager','trim|required');
				$this->form_validation->set_rules('JoiningDate', 'Joining Date','trim|required');
				$this->form_validation->set_rules('AppraisalDate', 'AppraisalDate','trim|required');
			}
			else if($this->input->post('type') == 'WorkExperience'){
				/* Work Experience */
				$this->form_validation->set_rules('Employer[]', 'Employer','trim');
				$this->form_validation->set_rules('Industries[]', 'Industries','trim');
				$this->form_validation->set_rules('ExpDesignation[]', 'ExpDesignation','trim');
				$this->form_validation->set_rules('JobProfile[]', 'Job Profile','trim');
				$this->form_validation->set_rules('From[]', 'From','trim');
				$this->form_validation->set_rules('To[]', 'To','trim');
			}
			else if($this->input->post('type') == 'SocialMedia')
			{
				/* Social info and course subject */
				$this->form_validation->set_rules('FacebookURL', 'FacebookURL','trim');
				$this->form_validation->set_rules('TwitterURL', 'TwitterURL','trim');
				$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL','trim');
				$this->form_validation->set_rules('GoogleURL', 'GoogleURL','trim');

				if($this->input->post('FacebookURL') != "")
				{
					if(!validate_input_url($this->input->post('FacebookURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid Facebook URL.";
						die;
					}
				}

				if($this->input->post('TwitterURL') != "")
				{
					if(!validate_input_url($this->input->post('TwitterURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid Twitter URL.";
						die;
					}
				}
				
				if($this->input->post('LinkedInURL') != "")
				{
					if(!validate_input_url($this->input->post('LinkedInURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid LinkedIn URL.";
						die;
					}
				}

				if($this->input->post('GoogleURL') != "")
				{
					if(!validate_input_url($this->input->post('GoogleURL')))
					{
						$this->Return['ResponseCode'] 	=	500;
						$this->Return['Message']      	=	"Please enter valid Google URL.";
						die;
					}
				}
			}
		}

		
		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');	
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');	
		$this->form_validation->set_rules('MediaName','MediaName','trim');
		$this->form_validation->set_rules('UserID','User ID','trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		//echo $this->SourceID; die();
		$this->Post['UserTypeName'] = "Staff";

		if(!empty($this->input->post('UserID')) && $this->input->post('UserID') > 0){
			$this->Staff_model->updateUserInfo($this->input->Post('UserID'), array_merge($this->Post, array("SkipPhoneNoVerification"=>true)));
		}else{
			$UserID = $this->Staff_model->addUser((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->Post, $this->Post['UserTypeID'], $this->SourceID, ($this->Post['Source'] != 'Direct' ? '2' : '1')); 
		}
	
		if(!empty($UserID)){
			$UserData = $this->Staff_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
				'UserID' => $UserID
			));
			$this->Return['Data'] = $UserData;
		}else{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"An error occurred, please try again later.";  
		}
	}

	
}