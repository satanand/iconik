<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('News_model');
		$this->load->model('Utility_model');
		$this->load->model('Entity_model');
		$this->load->model('Walloffame_model');
	}


/*get News*/
	public function getNews_post()
	{	$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('type', 'type', 'trim');
		$this->form_validation->set_rules('NewsDates', 'NewsDates','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
		$NewsData = $this->News_model->getNews('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if(!empty($NewsData)){
			$this->Return['Data'] = $NewsData['Data'];
		}	
	}


	/*get News by id*/
	public function getNewsByID_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		//$this->form_validation->set_rules('NewsGUID', 'NewsGUID','trim|required|callback_validateEntityGUID[News,NewsID]');
		$this->form_validation->validation($this); 


		$NewsData = $this->News_model->getNewsByID($this->input->Post('NewsGUID'),$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if(!empty($NewsData)){
			$this->Return['Data'] = $NewsData;
		}	
	}




	/*

	Description: 	Use to add new book

	URL: 			/api_admin/book/add	

	*/

	public function add_post()
	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('NewsName', 'News Name', 'trim|required');
		$this->form_validation->set_rules('NewsDate', 'News Date', 'trim|required|callback_validateDate');
		$this->form_validation->set_rules('NewsDescription', 'News Description', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'File','trim');
		$this->form_validation->validation($this);  /* Run validation */		


		$NewsData = $this->News_model->addNews($this->Post);

		//echo $NewsData;

			if($NewsData == 1){

				$this->Return['Message']      	=	"New News Add Successfully."; 

			} else {
                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This News Title Name Already Exist.";
				
			}


	}

		/*

	Name: 			editNews_post
	

	*/
	public function editNews_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
	     $this->form_validation->set_rules('NewsGUID', 'NewsGUID', 'trim|required');
	     $this->form_validation->set_rules('NewsName', 'News Name', 'trim|required');
		 $this->form_validation->set_rules('NewsDate', 'News Date', 'trim|required|callback_validateDate');
		 $this->form_validation->set_rules('NewsDescription', 'News Description', 'trim');
		 $this->form_validation->set_rules('MediaGUID', 'MediaGUID','trim');
		  $this->form_validation->set_rules('MediaID', 'MediaID', 'trim');
		
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */


		if(!empty($this->Post['MediaGUID'])){

			$this->Walloffame_model->delete($this->input->Post('NewsGUID'));

		}

		$this->News_model->editNews($this->Post);
	
		$this->Return['Data']= $this->News_model->getNews('',array("NewsGUID"=>$this->input->Post('NewsGUID')));

		$this->Return['Message']      	=	"News updated successfully."; 

		}

	public function delete_post()
	{
		/* Validation section */
		//echo $this->input->Post('NewsGUID'); die();
		
		$data=$this->News_model->delete($this->input->Post('NewsGUID'));



		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
	}



}