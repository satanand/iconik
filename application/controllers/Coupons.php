<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coupons extends Admin_Controller_Secure 
{

	public function index()
	{
		$load['css']=array(
			'asset/plugins/select2/dist/css/select2.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',
			'asset/plugins/select2/dist/js/select2.full.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',			
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);		

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Discounts" => ""
		);		


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('coupons/coupons_list');
		$this->load->view('includes/footer');
	}







}

