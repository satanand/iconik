<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	function getGalleryStatistics($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select("*");
		$this->db->from("set_gallery_album");
		$this->db->where("InstituteID",$InstituteID);
		
		if(!empty($Where['Keyword'])){
			$this->db->group_start();
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->like("AlbumName", $Where['Keyword']);
			$this->db->group_end();				
		}

		if(!empty($Where['Category'])){
			$this->db->group_start();
			$this->db->like('AlbumName',$Where['Category']);
			$this->db->group_end();
		}

		$this->db->order_by("AlbumID","DESC");		

		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();

		//echo $this->db->last_query(); die;

		$Records = array();



		if($Query->num_rows() > 0){
			$data = $Query->result_array();
			foreach ($data as $Record) {
				$query = $this->db->query("select count(MediaID) as count_MediaID from tbl_media where SectionID = 'gallery' and CategoryID like '%".$Record['AlbumName']."%' and StatusID = 2");
				$data = $query->result_array();
				$Record['pictures_count'] = $data[0]['count_MediaID'];
				array_push($Records, $Record);
			}
			return $Records;
		}
		return false;
	}

	


	/*
	Description: 	Use to add new category
	*/
	function add($Input=array()){


		$WallofGUID = get_guid();


        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);


			if(!empty($Input['Category'])){

				$Categorys=implode(',',$Input['Category']);

			}else{
				$Categorys='';
			}

			/* Add Gallery */
			$UpdateArray = array_filter(array(
				
				"MediaCaption"=>@$Input['MediaCaption'],
				"UserID"=>$InstituteID,
				"CategoryID"=>$Categorys,
				"InstituteID"=>$InstituteID,
				
			));
			if(!empty($Input['MediaGUIDe'])){

			$this->db->where('MediaGUID', $Input['MediaGUIDe']);
			
			}

			
			if(!empty($Input['MediaGUIDs'])){

			$this->db->where('MediaGUID', $Input['MediaGUIDs']);

			}

			$data=$this->db->update('tbl_media', $UpdateArray);

			$UpdateArrays = array_filter(array(
			
				"InstituteID"=>$InstituteID
				
			));

			if(!empty($Input['MediaGUIDs'])){
			   $this->db->where('EntityGUID', $Input['MediaGUIDs']);
			}
			if(!empty($Input['MediaGUIDe'])){
			   $this->db->where('EntityGUID', $Input['MediaGUIDe']);
			}

			$data=$this->db->update('tbl_entity', $UpdateArrays);
			//echo $this->db->last_query();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return TRUE;
		}
	



	/*
	Description: 	Use to update Gallery.
	*/
	function editgallery($Input=array()){
		
		if (!empty($Input['Category'])) {
			$Cetegories = implode(',', $Input['Category']);
		}else{
			$Cetegories = '';
		}

		$UpdateArray = array_filter(array(				
			"MediaCaption"=>@$Input['MediaCaption'],
			"CategoryID"=>@$Cetegories					
		));

		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('MediaGUID', $Input['MediaGUID']);
			//$this->db->limit(1);
			$data=$this->db->update('tbl_media', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		
		return TRUE;
	}


    /*
	Description: 	Use to  DeteleMedia.
	*/
	function delete($MediaGUID){

		if(!empty($MediaGUID)){

			$UpdateData = array_filter(array(

				"StatusID"  =>1
				
			));

			$this->db->where('MediaGUID', $MediaGUID);

			$this->db->update('tbl_media',$UpdateData);
			
			
		}
		return TRUE;
	}


	/*
	Description: 	Use to  DeteleMedia.
	*/
	function DeteleMedia($MediaGUID){

		if(!empty($MediaGUID)){
			$this->db->where('MediaGUID', $MediaGUID);
			$data=$this->db->delete('tbl_media');
		}
		return TRUE;
	}

    
    /*
	Description: 	Use to get Cetegories
	*/
	function getCategory($EventGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('AlbumID,AlbumName,EntryDate,ModifyDate');

		$this->db->from('set_gallery_album');

		if(!empty($Where['AlbumID'])){

			$this->db->where('AlbumID',$Where['AlbumID']);
		}

		if(!empty($Where['CategoryName'])){

			$this->db->where('AlbumName',$Where['CategoryName']);
		}

		$this->db->where('InstituteID',$InstituteID);
		$this->db->order_by('AlbumName','ASC');
	

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}
    
    /*add category */
	function addcategory($data=array()){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->db->select('AlbumID');

	    $this->db->from('set_gallery_album');
	    
	    $this->db->where('LOWER(`AlbumName`)="'.strtolower($data['Category']).'"');

         $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

			$InsertData = array_filter(array(

				"AlbumName"  =>	$data['Category'],
				"InstituteID"  => $InstituteID,	
				"EntryDate" => date("Y-m-d H:i:s")
			));

			$this->db->insert('set_gallery_album', $InsertData);

		}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}

	
		return TRUE;
	}	


	
	/*
	Description: 	Use to get Cetegories
	*/
	function getgalleryBy($MediaGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$this->db->select('*');
		$this->db->from('tbl_media');
		$this->db->where('StatusID',2);
		if(!empty($Where['MediaGUID'])){
			$this->db->where('MediaGUID',$Where['MediaGUID']);
		}		
		$this->db->order_by('MediaCaption','ASC');
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Gallery',"MediaID" => $Record['MediaID']),TRUE);
			$Record['MediaID'] = ($MediaData ? $MediaData['Data'] : new stdClass());
			$Record['MediaThumbURL'] = $Record['MediaID']['Records'][0]['MediaThumbURL'];
			$Record['MediaURL'] = $Record['MediaID']['Records'][0]['MediaURL'];

			if(!$multiRecords){

					return $Record;
				}
			$Records[] = $Record;
			}
			
			return $Records;
		}
		return FALSE;		
	}


	function getPhotosGallery($EntityID, $Where){ //print_r($Where); die;
		//$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select("AlbumName");
		$this->db->from('set_gallery_album');
		$this->db->Where('AlbumID',$Where['AlbumID']);

		if(!empty($InstituteID)){
			$this->db->where('InstituteID',$InstituteID);
		}

		$this->db->limit(1);
		$data = $this->db->get();

		if($data->num_rows() > 0){
			$data = $data->result_array();
			$AlbumName = $data[0]['AlbumName'];
		}

		$this->db->select('*');		
		$this->db->from('tbl_media b');
		if(!empty($InstituteID)){
			$this->db->where('b.InstituteID',$InstituteID);
		}
		$this->db->where('b.SectionID','Gallery');
		$this->db->where('b.StatusID',2);
		
		if(!empty($Where['AlbumName'])){
			$this->db->like("b.CategoryID",$Where['AlbumName']);
		}		

		$this->db->order_by("b.MediaID","DESC");

		$Query = $this->db->get();	
       // echo $this->db->last_query(); die();
		
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

				if(!empty($Record['MediaID'])){

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Gallery',"MediaID" => $Record['MediaID']),TRUE);
					$MediaDatas = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaDatas['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaDatas['Records'][0]['MediaURL'];
					$Record['MediaCaption'] = $MediaDatas['Records'][0]['MediaCaption'];
				}
				
				$Records[] = $Record;

			}

			$Return['Data']['Records'] = $Records;

			//print_r($Return); die();
			return $Return;
		}
		return FALSE;	
	}


	function getPhotosInAlbum($EntityID, $Where = array()){ //print_r($Where); die;
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select("*");
		$this->db->from("set_gallery_album");
		$this->db->where("InstituteID",$InstituteID);
		
		if(!empty($Where['Keyword'])){
			$this->db->group_start();
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->like("AlbumName", $Where['Keyword']);
			$this->db->group_end();				
		}

		if(!empty($Where['Category'])){
			$this->db->group_start();
			$this->db->like('AlbumName',$Where['Category']);
			$this->db->group_end();
		}

		$this->db->order_by("AlbumID","DESC");		

		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get();

		//echo $this->db->last_query(); die;

		$Records = array();

		if($Query->num_rows() > 0){
			$data = $Query->result_array();
			$Media_list = array();
			foreach ($data as $Record) {
				$this->db->select('*');		
				$this->db->from('tbl_media b');
				$this->db->where('b.InstituteID',$InstituteID);
				$this->db->where('b.SectionID','Gallery');
				$this->db->where('b.StatusID',2);
				
				//if(!empty($Where['AlbumName'])){
					$this->db->like("b.CategoryID",$Record['AlbumName']);
				//}		

				$this->db->order_by("b.MediaID","DESC");

				$Query = $this->db->get();	
		       // echo $this->db->last_query(); die();
				
				if($Query->num_rows()>0){
					$i = 0;
					foreach($Query->result_array() as $Media){
						if(!empty($Media['MediaID'])){
							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Gallery',"MediaID" => $Media['MediaID']),TRUE);
							$MediaDatas = ($MediaData ? $MediaData['Data'] : new stdClass());
							$Media_list[$i]['MediaThumbURL'] = $MediaDatas['Records'][0]['MediaThumbURL'];
							$Media_list[$i]['MediaURL'] = $MediaDatas['Records'][0]['MediaURL'];
							$Media_list[$i]['MediaCaption'] = $MediaDatas['Records'][0]['MediaCaption'];
							$i++;
						}
					}					
				}

				$Record['Media'] = $Media_list;
				$Records[] = $Record;

				// $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Gallery',"MediaID" => $Record['MediaID']),TRUE);
				// $Record['MediaID'] = ($MediaData ? $MediaData['Data'] : new stdClass());
				// $Record['MediaThumbURL'] = $Record['MediaID']['Records'][0]['MediaThumbURL'];
				// $Record['MediaURL'] = $Record['MediaID']['Records'][0]['MediaURL'];

				// $query = $this->db->query("select count(MediaID) as count_MediaID from tbl_media where SectionID = 'gallery' and CategoryID like '%".$Record['AlbumName']."%' and StatusID = 2");
				// $data = $query->result_array();
				// $Record['pictures_count'] = $data[0]['count_MediaID'];
				// array_push($Records, $Record);
			}
			$Return['Data']['Records'] = $Records;

			//print_r($Return); die();
			return $Return;
		}
		return false;	
	}

	
	/*
	Description: 	Use to get Gallery
	*/
	function getgallery($MediaGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('*');		
		$this->db->from('tbl_media b');
		//$this->db->join('tbl_entity e','e.EntityID=b.MediaID','left');
		//$this->db->join('tbl_users u','u.UserID = b.UserID','left');
		$this->db->where('b.InstituteID',$InstituteID);
		$this->db->where('b.SectionID','Gallery');
		$this->db->where('b.StatusID',2);

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->like("b.MediaCaption", $Where['Keyword']);				
		}
		if(!empty($Where['Category'])){
			$this->db->group_start();
			$this->db->like('b.CategoryID',$Where['Category']);
			$this->db->group_end();
		}
		if(!empty($Where['MediaGUID'])){

			$this->db->where('b.MediaGUID',$Where['MediaGUID']);
		}
		if(!empty($MediaGUID)){

			$this->db->where('b.MediaGUID',$MediaGUID);
		}

		$this->db->order_by("b.MediaID","DESC");

		

		//$this->db->order_by('MediaID','DESC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
        //echo $this->db->last_query(); die();
		
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

				 if(!empty($Record['MediaID'])){

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Gallery',"MediaID" => $Record['MediaID']),TRUE);
					$MediaDatas = ($MediaData ? $MediaData['Data'] : new stdClass());
					$Record['MediaThumbURL'] = $MediaDatas['Records'][0]['MediaThumbURL'];
					$Record['MediaURL'] = $MediaDatas['Records'][0]['MediaURL'];
					$Record['MediaCaption'] = $MediaDatas['Records'][0]['MediaCaption'];
				}
				if(!$multiRecords){

					return $Record;
				}
				
				$Records[] = $Record;

			}

			$Return['Data']['Records'] = $Records;

			//print_r($Return); die();
			return $Return;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to get Gallery
	*/
	function getgallery1($MediaGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Gallery',"MediaID" => $Record['MediaID']),TRUE);
				$Record['MediaData'] = ($MediaData ? $MediaData['Data'] : new stdClass());
				$Record['MediaThumbURL'] = $Record['MediaData']['Records'][0]['MediaThumbURL'];
				$Record['MediaURL'] = $Record['MediaData']['Records'][0]['MediaURL'];
		
		return FALSE;		
	}


	/*
	Description: 	Use to get Gallery
	*/
	function getYears($InstituteID){

		$this->db->select('Distinct(Year)');		
		$this->db->from('tbl_media b');
		$this->db->join('tbl_entity e','e.EntityID=b.MediaID');
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('b.SectionID','Gallery');
		$this->db->order_by('Year','DESC');
		$Query = $this->db->get();	
		
		if($Query->num_rows()>0){
			return $Query->result_array();
		}

		return FALSE;		
	}



	function EditAlbum($Input)
	{
		$UpdateArray = array_filter(array(				
			"AlbumName"=>@$Input['AlbumName']
		));
		$this->db->where("AlbumID",$Input['AlbumID']);
		$this->db->update("set_gallery_album",$UpdateArray);
		return TRUE;
	}


	function DeleteAlbum($Input)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->where("AlbumID",$Input['AlbumID']);
		$this->db->where("InstituteID",$InstituteID);
		$this->db->delete("set_gallery_album");

		$this->db->where("CategoryID",$Input['AlbumName']);
		$this->db->where("InstituteID",$InstituteID);
		$this->db->delete("tbl_media");
		return TRUE;
	}

}