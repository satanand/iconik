<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fee extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js'

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Courses" => base_url()."category",
			"Manage Fee" => ""
		);		




		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('fee/fee_list');

		$this->load->view('includes/footer');

	}



	public function collection()
	{

		$load['css'] = array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',

			'asset/plugins/select2/dist/css/select2.min.css',
		);
		
		$load['js'] = array(
			'asset/js/feecollection.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',		
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
			'asset/js/excelexportjs.js',

			'asset/plugins/select2/dist/js/select2.min.js',

		);		

		$load['breadcrumb'] = array(
			"Dashboard" => base_url()."dashboard",
			"Manage Courses" => base_url()."category",
			"Fee Collection" => ""
		);	

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('fee/collection');

		$this->load->view('includes/footer');

	}


	public function reminders()
	{
		$load['css'] = array(
			'asset/plugins/chosen/chosen.min.css',			
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',			
		);
		
		$load['js'] = array(
			'asset/js/fee.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',						
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);		

		$load['breadcrumb'] = array(
			"Dashboard" => base_url()."dashboard",
			"Manage Courses" => base_url()."category",
			"Fee Reminders" => ""
		);	

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('fee/reminders');

		$this->load->view('includes/footer');

	}

}

