app.controller('PageController', function($scope, $http, $timeout) {

    $scope.data.pageSize = 100;

    $scope.data.dataLists = [];
    $scope.data.dataList = [];
    $scope.data.batchList = [];
    $scope.data.categoryList = [];
    $scope.CourseDuration = "";

    //$scope.data.batchLists = [];

    $scope.CategoryDataList = [];

    // $scope.data.dataFaculty = [];
    // $scope.data.dataBatch=[];
    $scope.UserType;
 

    /*$scope.$watch('search', function(CourseID)
       { 
        
          $scope.data = angular.copy($scope.orig);
        
         $scope.getList(CourseID);
       });*/
    /*----------------*/

    /*list*/
    $scope.getFilterData = function() {
        $scope.getCategoryList();
        $scope.getSecondLevelPermission();
        var BatchID = $scope.getUrlParameter('BatchID');
        if (BatchID != undefined) {

            $scope.type = "facultys";

        } else {

            $scope.type = "faculty";
        }

    }



    $scope.applyFilter = function()

    {

        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getCategoryList();
        $scope.getList();


    }




    /*list append*/

    $scope.getList = function(CategoryID = "", check = "")

    {
        if (check != 1) {

            if ($scope.data.listLoading || $scope.data.noRecords) return;

        } else {

            $scope.data.pageNo = 1;
            $scope.data.dataList = [];

        }

        $scope.CategoryID = "";


        $scope.data.pageSize = 20;


        $scope.data.listLoading = true;

        var BatchID = $scope.getUrlParameter('BatchID');

        if (BatchID != undefined) {

            $scope.type = "facultys";

        } else {

            $scope.type = "faculty";
        }

        //alert(type);

        var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID + '&BatchID=' + BatchID + '&CourseID=' + CategoryID + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize();


        $http.post(API_URL + 'batch/getBatch', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }

                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;

            }


            $scope.data.listLoading = false;

            //setTimeout(function(){ tblsort(); }, 1000);

        });

    }

    /*load add form*/

    $scope.loadFormAdd = function(Position)

    {
        $scope.data.Position = Position;
        $scope.data.dataLists = [];
        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();

        $scope.data.pageLoading = true;

        $('#add_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;

        $timeout(function() {
            $("#StartDate").datepicker({
                format: 'dd-mm-yyyy',
                minView: 2
            });
            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 600);

    }


    $scope.loadFormAttendance = function()
    {       
        $scope.data.dataLists = [];

        $scope.getStudents();

        $scope.templateURLAttendance = PATH_TEMPLATE + module + '/attendance_form.htm?' + Math.random();

        //console.log($scope.templateURLAttendance);

        $scope.data.pageLoading = true;

       // $scope.date = new Date();

        var d = new Date($.now());
        var m = (d.getMonth() + 1); if(m <= 9) m = "0" + m;
        $scope.date = d.getDate()+"-"+m+"-"+d.getFullYear();

        $('#attendance_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }



    /*load add Asaining Batch*/

    $scope.loadFormAddAs = function(Position) {
        $scope.data.dataFaculty = [];
        $scope.data.dataBatch = [];
        $scope.data.Position = Position;
        $scope.templateURLAddAs = PATH_TEMPLATE + module + '/addAssign_form.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#addasaining_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;

        $timeout(function() {

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 300);

    }


    /*load edit form*/

    $scope.loadFormEdit = function(Position, BatchGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
$('#edits_model').modal({
                    show: true
                });

        $scope.data.pageLoading = true;

        //$scope.getCategoryList();

        $http.post(API_URL + 'batch/getBatchs', 'SessionKey=' + SessionKey + '&BatchGUID=' + BatchGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                
                $scope.formData = response.Data;

                $scope.Duration = $scope.formData.Duration;

                $scope.CourseDuration = $scope.formData.CourseDuration;

                

                //$timeout(function() {
                    $("#StartDate").datepicker({
                        format: 'dd-mm-yyyy',
                        minView: 2
                    });
                    /*$(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                    var CategoryGUID = $("#CategoryGUID").val();*/
                   // $scope.getDuration(CategoryGUID);
                //}, 1000);

            }

        });

    }
    /*load view form*/

    $scope.loadFormView = function(Position, BatchGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE + module + '/view_form.htm?' + Math.random();

        $scope.data.pageLoading = true;

        //$scope.getCategoryList();

        $http.post(API_URL + 'batch/getBatchs', 'SessionKey=' + SessionKey + '&BatchGUID=' + BatchGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;

                $('#view_model').modal({
                    show: true
                });

                $timeout(function() {
                    $("#StartDate").datepicker({
                        format: 'dd-mm-yyyy',
                        minView: 2
                    });
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 600);

            }

        });

    }

    /*load edit Assign form*/

    $scope.loadFormAssingEdit = function(Position, AsID, UserType, UserGUID)

    {

        $scope.data.Position = Position;



        $scope.templateURLAssignEdit = PATH_TEMPLATE + module + '/editAssign_form.htm?' + Math.random();


        $scope.data.pageLoading = true;

        $scope.getBatch();

        $http.post(API_URL + 'batch/getBatchsAssign', 'SessionKey=' + SessionKey + '&AsID=' + AsID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */



                $scope.formData = response.Data;



                $scope.getFaculty($scope.formData.CategoryID);

                $scope.getBatch($scope.formData.CategoryID);

                $scope.CategoryID = $scope.formData.CategoryID;

                $scope.FacultyID = $scope.formData.UserID;

                $scope.data.pageLoading = false;

                console.log($scope.formData.UserID);

                $('#editAssign_model').modal({
                    show: true
                });

                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 500);

            }

        });

    }

    /*load view Assign form*/

    $scope.loadFormAssingView = function(Position, AsID, UserType, UserGUID)

    {

        $scope.data.Position = Position;



        $scope.templateURLAssignView = PATH_TEMPLATE + module + '/viewAssign_form.htm?' + Math.random();


        $scope.data.pageLoading = true;

        //$scope.getBatch();

        $http.post(API_URL + 'batch/getBatchsAssign', 'SessionKey=' + SessionKey + '&AsID=' + AsID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */



                $scope.formData = response.Data;



                //$scope.getFaculty($scope.formData.CategoryID);

                //$scope.getBatch($scope.formData.CategoryID);

                $scope.CategoryID = $scope.formData.CategoryID;

                $scope.FacultyID = $scope.formData.UserID;

                $scope.data.pageLoading = false;

                console.log($scope.formData.UserID);

                $('#viewAssign_model').modal({
                    show: true
                });

                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 500);

            }

        });

    }


    /* category get select box */
    $scope.getCategoryList = function() {

        $scope.data.categoryList = [];
        var data = 'SessionKey=' + SessionKey + '&' + $('#filterPanel form').serialize();

        $http.post(API_URL + 'batch/getCategories', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data) {

                for (var i in response.Data) {

                    $scope.data.categoryList.push(response.Data[i]);

                }

                console.log($scope.data.categoryList);
                $timeout(function() {

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 200);
            }
        });
    }

    /* get Duration get select box */
    $scope.getDuration = function(CategoryGUID) {
        //alert(CategoryGUID);

        var data = 'SessionKey=' + SessionKey + '&CategoryGUID=' + CategoryGUID;

        $http.post(API_URL + 'batch/getDuration', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200) {


               // $scope.formData = response.Data;

                $scope.Duration = response.Data[0].Duration;
                 $scope.CourseDuration = response.Data[0].Duration;
                //console.log($scope.Duration);

                $timeout(function() {

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 200);
            }
        });
    }

    /* category get select box */
    $scope.getFaculty = function(CourseID) {

        // alert(UserType);
        //$scope.CategoryDataList = [];
        $scope.data.dataFaculty = [];
        var data = 'SessionKey=' + SessionKey + '&CourseID=' + CourseID;

        $http.post(API_URL + 'batch/getFaculty', data, contentType).then(function(response) {
            var response = response.data;
            //console.log(response);
            if (response.ResponseCode == 200 && response.Data) {

                for (var i in response.Data) {

                    $scope.data.dataFaculty.push(response.Data[i]);

                }
                $timeout(function() {

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 200);
            }
        });
    }


    /* category get select box */
    $scope.getStudents = function(BatchGUID="") {
        $scope.data.Students = [];
        var data = 'SessionKey=' + SessionKey + '&BatchGUID=' + BatchGUID;

        $http.post(API_URL + 'batch/getStudentsList', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200 && response.Data) {
                for (var i in response.Data) {
                    $scope.data.Students.push(response.Data[i]);
                }
            }
        });
    }


    $scope.SetAttendance = function () //(UserID,AttendanceStatus,date,BatchID)
    {       
        /*date = $("#EntryDate").val();
        var data = 'SessionKey=' + SessionKey + '&BatchID=' + BatchID + '&UserID=' + UserID + '&AttendanceStatus=' + AttendanceStatus + '&EntryDate=' + date;

        $http.post(API_URL + 'batch/SetAttendance', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
               SuccessPopup(response.Message); 
            }
            else
            {
               ErrorPopup(response.Message);
               $(".radio"+UserID).prop('checked', false);
            }
        });*/

        $scope.loadeSubmit = true;
        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_attendance_form']").serialize();

        $http.post(API_URL+'batch/SetAttendance', data, contentType).then(function(response) 
        {
            var response = response.data;
            $scope.loadeSubmit = false;
            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);

                $timeout(function()
                {
                   $('#attendance_model .close').click();

                   window.location.href = BASE_URL + "/batch";

               }, 3000);
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.data.pageLoading = false;
        });


    }

    /* category get select box */
    $scope.getBatch = function(CourseID) {

        //$scope.CategoryDataList = [];
        $scope.data.dataBatch = [];

        var data = 'SessionKey=' + SessionKey + '&CourseID=' + CourseID;

        $http.post(API_URL + 'batch/getBatch', data, contentType).then(function(response) {
            
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {

                for (var i in response.Data.Records) {

                    $scope.data.dataBatch.push(response.Data.Records[i]);

                }

                console.log($scope.data.dataBatch);

                $timeout(function() {

                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 200);

            }
        });
    }

    /*add data*/

    $scope.addData = function(actType)

    {

        $scope.addDataLoading = true;

        // console.log(InstituteGUID); 

        var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID + '&' + $("form[name='add_form']").serialize();

        $http.post(API_URL + 'batch/add', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) 
            {
                /* success case */

                SuccessPopup(response.Message);

                if(actType == 1)
                {
                    $("#BatchName, #StartDate").val('');
                }
                else
                {
                    $scope.applyFilter();

                    $timeout(function() {

                        $('#add_model .close').click();

                    }, 200);
                }    

            } else {

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;

        });

    }

    /*addasainingData data*/

    $scope.addasainingData = function()

    {

        $scope.addDataLoading = true;

        // console.log(InstituteGUID); 

        var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID + '&' + $("form[name='addasaining_form']").serialize();

        $http.post(API_URL + 'batch/addasaining', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.applyFilter();

                $timeout(function() {

                    $('#addasaining_model .close').click();

                }, 200);

            } else {

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;

        });

    }




    /*edit data*/

    $scope.editData = function(Position)

    {
        var Duration = $("#Duration").val();
        var DuratioExtendMonth = $("#DuratioExtendMonth").val();
        var CategoryGUID = $("#CategoryGUID").val();
        $scope.getDuration(CategoryGUID);

        //37===0===36
        //console.log(Duration+"==="+DuratioExtendMonth+"==="+$scope.CourseDuration);
        if(parseInt(Duration) > parseInt($scope.CourseDuration)+1 && DuratioExtendMonth > 0){
            alert("You have already extended duration.");
            $("#Duration").val($scope.Duration);
            return false;
        }else if(parseInt(Duration) > parseInt($scope.CourseDuration)+1 && DuratioExtendMonth == 0){
            alert("You can extend duration only by 1 month.");
            $("#Duration").val($scope.Duration);
            return false;
        }else{
            
            //  return false;
            $scope.editDataLoading = true;

            var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID + '&' + $("form[name='edit_form']").serialize();

            $http.post(API_URL + 'batch/editBatchs', data, contentType).then(function(response) {

                var response = response.data;

                if (response.ResponseCode == 200) {
                    /* success case */

                    SuccessPopup(response.Message);

                   $scope.data.dataList[$scope.data.Position] = response.Data;

                   //$scope.getList($scope.CategoryID,1);

                    $('#edits_model .close').click();

                } else {

                    ErrorPopup(response.Message);

                }

                $scope.editDataLoading = false;

            });
        }
    }

    /*edit Assign data*/

    $scope.editAssignData = function(Position)

    {

        $scope.editDataLoading = true;

        var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID + '&' + $("form[name='editAssign_form']").serialize();

        $http.post(API_URL + 'batch/editAssignData', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;

                $('#editAssign .close').click();

            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

        });

    }


    /*delete*/
    $scope.loadFormDelete = function(Position, BatchGUID) {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'batch/delete', 'SessionKey=' + SessionKey + '&BatchGUID=' + BatchGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }

    /*delete*/
    $scope.loadFormAssingDelete = function(Position, AsID) {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to Unassign?', function() {

            $http.post(API_URL + 'batch/Assingdelete', 'SessionKey=' + SessionKey + '&AsID=' + AsID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }

});


function getCategoryLists(CategoryID) 
{
    angular.element(document.getElementById('content-body')).scope().getList(CategoryID, 1);
}


function search_records()
{
    angular.element(document.getElementById('content-body')).scope().getList("", 1);
}


function clear_search_records()
{    
    $("#CategoryID, #Keyword").val("");    

    search_records();
}

/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



    $(".table-sortable tbody").sortable({

        placeholder: 'tr_placeholder',

        helper: fixHelper,

        cursor: "move",

        tolerance: 'pointer',

        axis: 'y',

        dropOnEmpty: false,

        update: function(event, ui) {

            sendOrderToServer();

        }

    }).disableSelection();

    $(".table-sortable thead").disableSelection();




    function sendOrderToServer() {

        var order = 'SessionKey=' + SessionKey + '&' + $("#tabledivbody").sortable("serialize");

        $.ajax({

            type: "POST",
            dataType: "json",
            url: API_URL + 'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }


}
/* sortable - ends */

function getDuration(CategoryGUID){
    angular.element(document.getElementById('content-body')).scope().getDuration(CategoryGUID);  
}