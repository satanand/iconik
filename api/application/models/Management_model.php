<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
		$this->load->model('Entity_model');
	}


	/*
	Description: 	Use to get Cetegories
	*/
	function getCategories($EntityID='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		/*Additional fields to select*/

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID); 
		$Records = array();
		
		$this->db->select('C.CategoryGUID, C.CategoryTypeID, C.CategoryID, C.CategoryName, C.Duration, E.EntryDate, E.ModifiedDate, E.StatusID');

		$this->db->from('set_categories C');
		$this->db->join("tbl_entity E", "E.EntityGUID = C.CategoryGUID");
		$this->db->where("E.InstituteID",$InstituteID);

		if(!empty($Where['CategoryID'])){
			$this->db->where("C.CategoryID",$Where['CategoryID']);
		}      

		if(!empty($Where['CategoryGUID'])){
			$this->db->where("E.EntityGUID",$Where['CategoryGUID']);
		}           

		if(!empty($Where['CategoryTypeID'])){
			$this->db->where("C.CategoryTypeID",$Where['CategoryTypeID']);
		}           

		if(!empty($Where['ParentCategoryID'])){
			$this->db->where("C.ParentCategoryID",$Where['ParentCategoryID']);
		}


		if(!empty($Where['Keyword'])){
			
			$this->db->group_start();
			$this->db->like("C.CategoryName", trim($Where['Keyword']));
			$this->db->or_like("C.Duration", trim($Where['Keyword']));
			$this->db->or_like('CT.CategoryTypeName',trim($Where['Keyword']));
			if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
    			$this->db->or_where("E.StatusID",6);
			}
			else if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
    			$this->db->or_where("E.StatusID",2);
			}
			$this->db->group_end();

		}

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get(); 
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$this->db->order_by('C.CategoryID','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query(); 
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	/* get batch data*/
    function getFacultyByBatch($EntityID, $Where = array(), $multiRecords = FALSE, $PageNo = 1, $PageSize = 10)
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        
       // if (!empty($Where['BatchID'])) {
            
            $this->db->select('CONCAT_WS(" ",u.FirstName,u.LastName) FullName,u.UserID');
            $this->db->from('tbl_batch B');
            $this->db->join('tbl_batchbyfaculty bf', 'bf.BatchID = B.BatchID', 'left');
            $this->db->join('tbl_users u', 'u.UserID = bf.FacultyID', 'left');
            $this->db->where("bf.InstituteID", $InstituteID);
            
            //$this->db->where("B.StatusID != 3");
            $this->db->where("bf.AssignStatus != 3");

            if (!empty($Where['BatchID'])) {
               $this->db->where("bf.BatchID", $Where['BatchID']);
            }
            
            if (!empty($Where['CourseID'])) {
                $this->db->where("B.CourseID", $Where['CourseID']);
            }

            if(isset($Where['CategoryID']) && !empty($Where['CategoryID']))
            {
                $this->db->where("B.CourseID", $Where['CategoryID']);
            }

            if (!empty($Where['Keyword'])) {
                $this->db->group_start();

                $this->db->like("u.FirstName", $Where['Keyword']);
                $this->db->or_like("u.LastName", $Where['Keyword']);
                $this->db->or_like("CONCAT_WS('',u.FirstName,u.Middlename,u.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
                $this->db->or_like("B.BatchName", $Where['Keyword']);
               $this->db->group_end();
            }
           
            $this->db->order_by('bf.AsID', 'DESC');
            
      // }
        
        if ($multiRecords) {
            $TempOBJ                        = clone $this->db;
            $TempQ                          = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize));
            /*for pagination*/
        }

        $Query = $this->db->get();
        //echo $this->db->last_query(); die();
        if ($Query->num_rows() > 0) {
            foreach ($Query->result_array() as $Record) {
                $Records[] = $Record;
            }
            $Return['Data']['Records'] = $Records;
            
            return $Return;
        }
        return FALSE;
    }


    /* get batch data*/
    function getInstituteBatches($EntityID, $Where = array(), $multiRecords = FALSE, $PageNo = 1, $PageSize = 10)
    {
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('B.BatchName,B.BatchID,B.BatchGUID,B.StartDate, B.Duration ,u.UserID, c.CategoryName as CourseName, c.CategoryID, c.ParentCategoryID');
        $this->db->from('tbl_batch B');
        $this->db->join('tbl_entity e', 'e.EntityID = B.BatchID', 'left');
        $this->db->join('tbl_batchbyfaculty bf',  'bf.BatchID = B.BatchID', 'left');
        $this->db->join('tbl_users u', 'u.UserID = bf.FacultyID', 'left');
        $this->db->join('set_categories c', 'c.CategoryID = B.CourseID', 'left');

        if (!empty($Where['StreamID'])) {        	
            $this->db->where("c.ParentCategoryID", $Where['StreamID']);
        }

        $this->db->where("e.InstituteID", $InstituteID);

        if (!empty($Where['BatchID'])) {
            $this->db->where("B.BatchID", $Where['BatchID']);
        }

        //$this->db->where("B.StatusID != 3");
        //$this->db->where("bf.AssignStatus != 3");
        
        if (!empty($Where['CourseID'])) {
            $this->db->where("B.CourseID", $Where['CourseID']);
        }

        if(isset($Where['FacultyID']) && !empty($Where['FacultyID']))
        {
            $this->db->where("bf.FacultyID", $Where['FacultyID']);
        }

        if (!empty($Where['Keyword'])) {
            $this->db->group_start();

            $this->db->like("u.FirstName", $Where['Keyword']);
            $this->db->or_like("u.LastName", $Where['Keyword']);
            $this->db->or_like("CONCAT_WS('',u.FirstName,u.Middlename,u.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("B.BatchName", $Where['Keyword']);
            $this->db->group_end();
        }
       	$this->db->group_by('B.BatchID');
        $this->db->order_by('bf.AsID', 'DESC');
        
        if ($multiRecords) {
            $TempOBJ                        = clone $this->db;
            $TempQ                          = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize));
            /*for pagination*/
        }

        $Query = $this->db->get();
       // echo $this->db->last_query(); //die();
        if ($Query->num_rows() > 0) {
            foreach ($Query->result_array() as $Record) {        
            	$this->db->select('CONCAT_WS(" ",u.FirstName,u.LastName) FullName');
	            $this->db->from('tbl_batchbyfaculty bf');
	            $this->db->join('tbl_users u', 'u.UserID = bf.FacultyID', 'left');
	            $this->db->where('bf.BatchID', $Record['BatchID']);
	            $this->db->where("bf.InstituteID", $InstituteID);  
	            $FacultyQuery = $this->db->get();
	            //echo $this->db->last_query(); 
	            if($FacultyQuery->num_rows() > 0){
	            	$FacultyArr = $FacultyQuery->result_array();
	            	$FacultyArr = array_column($FacultyArr, "FullName");
	            	$Record['FacultyName'] = implode(", ", $FacultyArr);
	            }else{
	            	$Record['FacultyName'] = "";
	            } 

            	$Record['StreamName'] = $this->Common_model->getCategoryNameByID($Record['ParentCategoryID']);


                $Subjects = $this->Common_model->getSubCategoryName($Record['CategoryID']);

                if(!empty($Subjects)){
                    $Record['SubjectName'] = implode(", ", $Subjects);
                }else{
                    $Record['SubjectName'] = "";
                }
                

                $Record['NoOfStudents'] = $this->Common_model->getStudentCountByBatchID($Record['BatchID']);


                $TimeQuery = "SELECT es.StartTime, es.EndTime, es.start as StartDate, es.end as EndDate FROM set_timescheduling es  WHERE  es.BatchID = '".$Record['BatchID']."' ORDER BY id DESC";

                $TimeQuery = $this->db->query($TimeQuery);

                //echo "SELECT es.StartTime, es.EndTime, es.start as StartDate, es.end as EndDate FROM set_timescheduling es  WHERE  es.BatchID = '".$Record['BatchID']."' ORDER BY id DESC";

                if($TimeQuery->num_rows() > 0){
                    $TimeArr = $TimeQuery->result_array();
                    $Record['Timing'] = $TimeArr;
                }else{
                    $Record['Timing'] = "";
                } 

                
                $Records[] = $Record;
            }
            $Return['Data']['Records'] = $Records;
            
            return $Return;
        }
        return FALSE;
    }


    function getCoursesWithStudentCount($EntityID, $Input=array())
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID); 
        $Records = array();
        
        $this->db->select('C.CategoryGUID, C.CategoryID, C.CategoryName');
        $this->db->from('set_categories C');
        $this->db->join("tbl_entity E", "E.EntityGUID = C.CategoryGUID");
        $this->db->where("E.InstituteID",$InstituteID);


        if(!empty($Input['StreamID'])){
            $this->db->where("C.ParentCategoryID", $Input['StreamID']);
        }

        if(!empty($Input['CourseID'])){
            $this->db->where("C.CategoryID", $Input['CourseID']);
        }

        $Query = $this->db->get();
        
        if($Query->num_rows() > 0)
        {
            foreach ($Query->result_array() as $Record) 
            { 
                $Record['NoOfStudents'] = $this->Common_model->getStudentCount($Record['CategoryID'],$Input);
                
                $Records[] = $Record;
            }
            $Return['Data']['Records'] = $Records;
            
            return $Return;
        }
        return FALSE;        
    }


    function getInstituteStudents($EntityID, $Where=array())
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $this->db->select('tbl_entity.StatusID,tbl_users.UserID, tbl_users.UserGUID, tbl_users.FirstName,tbl_users.LastName,tbl_users.Address,tbl_users.StateName,tbl_users.CityName,tbl_users.Postal,tbl_users.PhoneNumber,tbl_users.LinkedInURL,tbl_users.FacebookURL,tbl_users.Email, tbl_students.GuardianName,tbl_students.GuardianAddress,tbl_students.GuardianPhone, tbl_students.GuardianEmail, tbl_students.FathersName, tbl_students.MothersName, tbl_students.PermanentAddress, tbl_students.FathersPhone, tbl_students.FathersEmail,tbl_students.CourseID,tbl_students.BatchID,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.FeeStatusID,tbl_students.TotalFeeAmount,tbl_students.TotalFeeInstallments,tbl_students.InstallmentAmount, tbl_students.Remarks,tbl_students.Key,tbl_students.Validity,tbl_students.KeyAssignedOn,tbl_students.ActivatedOn,tbl_students.ExpiredOn,IF(tbl_users.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",tbl_users.ProfilePic)) AS ProfilePic,set_categories.CategoryID');
        $this->db->from("tbl_students");
        $this->db->join("tbl_users","tbl_students.StudentID = tbl_users.UserID");
        $this->db->join("tbl_entity","tbl_users.UserID = tbl_entity.EntityID");
        $this->db->join("set_categories","set_categories.CategoryID = tbl_students.CourseID");
        
        //if(!empty($InstituteID)){
        $this->db->where("tbl_entity.InstituteID",$InstituteID);
        //}

        if(!empty($Where['StreamID'])){
            $this->db->where("set_categories.ParentCategoryID",$Where['StreamID']);
        }
        if(!empty($Where['CourseID'])){
            $this->db->where("tbl_students.CourseID",$Where['CourseID']);
        }
        if(!empty($Where['BatchID'])){
            $this->db->where("tbl_students.BatchID",$Where['BatchID']);
        }
        if(!empty($UserID)){
            $this->db->where("tbl_users.UserID",$UserID);
            $this->db->limit(1);
        }
        $Query =  $this->db->get(); //echo $this->db->last_query(); die;
        //$StudentData = $Query->result_array(); //print_r($StudentData);
        $Records = array();
        if($Query->num_rows() > 0){
            foreach ($Query->result_array() as $record) 
            {              

                if(!empty($record['CourseID']))
                {
                    $query = $this->db->query("select CategoryGUID,CategoryName from set_categories where CategoryID = ".$record['CourseID']);
                    $CategoryData = $query->result_array();
                    $record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
                    $record['CourseName'] = $CategoryData[0]['CategoryName'];
                }

                if(!empty($record['BatchID']))
                {
                    $query = $this->db->query("select BatchName from tbl_batch where BatchID = ".$record['BatchID']);
                    $BatchName = $query->result_array();
                    $record['BatchName'] = $BatchName[0]['BatchName'];
                }

                $record['TotalFee'] = 0;                
                $record['NoOfInstallment'] = 0;                                   
                if(!empty($record['FeeID']))
                {
                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();

                    if(!empty($FeeData)){
                        $record['TotalFee'] = $FeeData[0]['FullDiscountFee'];
                        $record['NoOfInstallment'] = 1;
                        $record['InstallmentAmount'] = $record['TotalFee'];   
                    }else{
                        $record['TotalFee'] = 0;
                        $record['NoOfInstallment'] = 0;
                        $record['InstallmentAmount'] = 0; 
                    }                                      
                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {
                        $record['TotalFee'] = $InstallmentData[0]['TotalFee'];
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                        $record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['InstallmentAmount'] = 0;
                }

                $record['IsPaidAnyFee'] = 0;
                $record['PaidEMICount'] = 0;
                $record['RemainingEMI'] = 0;
                $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$record['UserID']. " AND CourseID = '".$record['CourseID']."' AND BatchID = '".$record['BatchID']."'");

                $SumDataquery = $this->db->query($query);   
                if($SumDataquery !== FALSE && $SumDataquery->num_rows() > 0)
                {
                    $SumData = $SumDataquery->result_array();
                    if(!empty($SumData)){
                        $record['PaidEMICount'] = $SumData[0]['FeeCollectionID'];
                    }                    
                    //$record['RemainingEMI'] = $record['PaidEMICount'] - count($SumData);                    
                }
                $record['RemainingEMI'] = $record['NoOfInstallment'] - $record['PaidEMICount'];
                    
                $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$record['UserID']. " AND CourseID = '".$record['CourseID']."' AND BatchID = '".$record['BatchID']."'");

                $SumDataquery = $this->db->query($query);  
                if($SumDataquery !== FALSE && $SumDataquery->num_rows() > 0)
                {
                    $SumData = $SumDataquery->result_array();                    
                    if(!empty($SumData) && $SumData[0]['PaidAmount'] > 0){
                        $record['PaidAmount'] = $SumData[0]['PaidAmount'];
                    }else{
                        $record['PaidAmount'] = 0;
                    } 
                }else{
                    $record['PaidAmount'] = 0;
                }

                $record['RemainingAmount'] = (int)$record['TotalFee'] - (int)$record['PaidAmount'];
    
                $record['PaidFeeHistory'] = array();

                $query = $this->db->query("select * from tbl_fee_collection where StudentID = ".$record['UserID']. " AND CourseID = '".$record['CourseID']."' AND BatchID = '".$record['BatchID']."'");
                 
                $SumDataquery = $this->db->query($query);
                if($SumDataquery->num_rows() > 0){
                    $SumData = $SumDataquery->result_array();   
                    if(isset($SumData) && !empty($SumData))
                    {
                        foreach($SumData as $arr)
                        {
                            if(isset($arr['Denomination']) && !empty($arr['Denomination']) && $arr['PaymentMode'] == "Cash")
                            $arr['Denomination'] = json_decode($arr['Denomination']);
                        
                            $record['PaidFeeHistory'][] = $arr;                 
                        }

                        $record['IsPaidAnyFee'] = 1;
                    }
                }
                

                
                if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
                    $record['Fee Status'] = "Unpaid";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Fee Status'] = "Paid";
                }else{
                    $record['Fee Status'] = "Unpaid";
                }

                if(!empty($record['StatusID']) && $record['StatusID'] == 1){
                    $record['Email Verification'] = "Pending";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Email Verification'] = "Done";
                }

                unset($record['CategoryGUID'], $record['FeeID'], $record['BatchID'], $record['CourseID']);
                unset($record['CategoryGUID']);
                array_push($Records, $record);
            }
            //print_r($Records);
            return $Records;
        }else{
            return $Records;
        }
    }


    function getTimeScheduling($EntityID, $Where=array())
    {    
        $sql = "SELECT es.*, CONCAT_WS(' ',U.FirstName,U.LastName) FacultyName, U.UserID, B.BatchID, B.BatchName, B.Duration,SS.CategoryName as StreamName, SC.CategoryName as CourseName, GROUP_CONCAT(SJ.CategoryName) as SubjectName 
        FROM set_timescheduling es 
        join tbl_entity E on es.id = E.EntityID 
        join tbl_batch B on es.BatchID = B.BatchID 
        join tbl_users U on es.FacultyID = U.UserID 
        join set_categories SC on SC.CategoryID = B.CourseID 
        join set_categories SS on SS.CategoryID = SC.ParentCategoryID 
        join set_categories SJ on find_in_set(SJ.CategoryID,es.SubjectID)";
        $sql .= "WHERE E.InstituteID = '".$EntityID."'";

        if(!empty($Where['ToDate']) && !empty($Where['FromDate'])){
            $sql .= ' AND es.start between "'.date("Y-m-d", strtotime($Where['ToDate'])).'" and "'.date("Y-m-d", strtotime($Where['FromDate'])).'" ';
        }else if(!empty($Where['ToDate'])){
            $sql .= ' AND es.start = "'.date("Y-m-d", strtotime($Where['ToDate'])).'" ';
        }else if(!empty($Where['FromDate'])){
            $sql .= ' AND es.start = "'.date("Y-m-d", strtotime($Where['FromDate'])).'" ';
        }

        if(!empty($Where['StreamID'])){
            $sql .= " AND SC.ParentCategoryID = '".$Where['StreamID']."'";   
        }

        if(!empty($Where['CourseID'])){
            $sql .= " AND SC.CategoryID = '".$Where['CourseID']."'";   
        }

        if(!empty($Where['BatchID'])){
            $sql .= " AND B.BatchID = '".$Where['BatchID']."'";   
        }

        if(!empty($Where['FacultyID'])){
            $sql .= " AND es.FacultyID = '".$Where['FacultyID']."'";   
        }

        $sql .= 'GROUP BY es.id 
            ORDER BY es.start ASC';

        //echo $sql;

        $Query = $this->db->query($sql);

       // print_r($Query);

        $Records = array();

       // echo $Query->num_rows();
    
        if($Query->num_rows() > 0)
        {
            foreach ($Query->result_array() as $Record) { 
                $Record['NoOfStudents'] = $this->Common_model->getStudentCount($Record['CourseID'],array("BatchID"=>$Record['BatchID']));
                
                $Records[] = $Record;

                //print_r($Records);
            }
            //$Return['Data']['Records'] = $Records;

            //print_r($Records);
            
            return $Records;
        }
    }

    function getKeysInventory($EntityID, $Where=array()){
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $validity = array(3,6,12,24);
        /*Additional fields to select*/
        $query = $this->db->query("select UserTypeID from tbl_users where UserID = ".$InstituteID);
        $Users = $query->result_array();
        $UserTypeID = $Users[0]['UserTypeID'];
        $data[0]['UserTypeID'] = $Users[0]['UserTypeID'];
        if($UserTypeID == 1){
            foreach ($validity as $key => $value) {
                $query = $this->db->query("select count(KeyName) as KeyName from set_keys where  Validity = ".$value);
                $keys = $query->result_array();
                $data[$key]['Validity'] = $value;
                $data[$key]['TotalKeys'] = $keys[0]['KeyName'];         
                $query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute IS NOT NULL  and Validity = ".$value);
                $keys = $query->result_array();
                $data[$key]['UsedKeys'] = $keys[0]['KeyName'];
                $data[$key]['AvailableKeys'] = $data[$key]['TotalKeys']-$data[$key]['UsedKeys'];
            }
        }else{
            foreach ($validity as $key => $value) {
                $query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute = ".$InstituteID." and Validity = ".$value);
                $keys = $query->result_array();
                $data[$key]['Validity'] = $value;
                $data[$key]['TotalKeys'] = $keys[0]['KeyName'];         
                $query = $this->db->query("select count(KeyName) as KeyName from set_keys where AssignedInstitute = ".$InstituteID." and UsedStatus = 1 and Validity = ".$value);
                $keys = $query->result_array();
                $data[$key]['UsedKeys'] = $keys[0]['KeyName'];
                $data[$key]['AvailableKeys'] = $data[$key]['TotalKeys']-$data[$key]['UsedKeys'];
            }
        }

        $new_data = array();

        $new_data['records'] = $data;

        $AvailableKeys = array_column($data, 'AvailableKeys');
        $new_data['TotalAvailableKeys'] = array_sum($AvailableKeys);

        $AvailableKeys = array_filter(array_column($data, 'AvailableKeys'));
        if(empty($AvailableKeys)){
            $new_data['Allot_keys'] = 0;
        }else{
            $new_data['Allot_keys'] = 1;
        }
        return $new_data;
    }
    
    function AssignKeysDetail($EntityID,$Where=array(),$multiRecords=FALSE,$PageNo=1, $PageSize=10){

        //print_r($Where); die;
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

        $this->db->select('S.KeyAssignedOn,S.Key,S.Validity,S.KeyStatusID,S.ActivatedOn,S.ExpiredOn,U.UserGUID,U.FirstName,U.LastName,B.BatchName,C.CategoryName as CourseName');

        $this->db->from("tbl_students S");
        $this->db->join("tbl_users U","S.StudentID = U.UserID",'left');
        $this->db->join("tbl_entity E","S.StudentID = E.EntityID",'left');
        $this->db->join("tbl_batch B","S.BatchID = B.BatchID",'left');
        $this->db->join("set_categories C","S.CourseID = C.CategoryID",'left');
        //$this->db->where("K.AssignedInstitute",$InstituteID);
        $this->db->where("S.Key IS NOT NULL");

        // $this->db->from('tbl_students S');
        // $this->db->from('tbl_users U');
        // $this->db->from('set_keys K');
        // $this->db->from('tbl_batch B');
        // $this->db->from('set_categories C');

        $this->db->where("S.StudentID","U.UserID", FALSE);
        $this->db->where("S.BatchID","B.BatchID", FALSE);
        $this->db->where("S.CourseID","C.CategoryID", FALSE);

        $this->db->where("E.InstituteID",$InstituteID);

        if(!empty($Where['StreamID'])){
            $this->db->where("C.ParentCategoryID",$Where['StreamID']);
        }

        if(!empty($Where['Validity'])){
            $this->db->where("S.Validity",$Where['Validity']);
        }

        if(!empty($Where['BatchID'])){
            $this->db->where("S.BatchID",$Where['BatchID']);
        }

        if(!empty($Where['CourseID'])){
            $this->db->where("S.CourseID",$Where['CourseID']);
        }

        if(!empty($Where['Keyword']))
        {
            $this->db->group_start();
            $this->db->like("U.FirstName", trim($Where['Keyword']));
            $this->db->or_like("U.LastName", trim($Where['Keyword']));
            $this->db->or_like("CONCAT_WS(' ',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
            $this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', ' ', trim($Where['Keyword'])), FALSE);
            $this->db->or_like("S.Key",$Where['Keyword']);
            $this->db->or_like("U.Email",$Where['Keyword']);
            $this->db->or_like("U.PhoneNumber",$Where['Keyword']);
            $this->db->or_like("B.BatchName",$Where['Keyword']);
            $this->db->or_like("C.CategoryName",$Where['Keyword']);                     
            $this->db->or_where("S.ActivatedOn",$Where['Keyword']);
            $this->db->or_where("S.ExpiredOn",$Where['Keyword']);
            $this->db->or_where("S.KeyAssignedOn",$Where['Keyword']);
            $this->db->or_where("S.Validity",$Where['Keyword']);
            if( strpos( $Where['Keyword'], "Inactive" ) !== false || strpos( $Where['Keyword'], "inactive" ) !== false) {
                $this->db->or_where("S.KeyStatusID",1);
            }
            if( strpos( $Where['Keyword'], "Active" ) !== false || strpos( $Where['Keyword'], "active" ) !== false) {
                $this->db->or_where("S.KeyStatusID",2);
            }
            if( strpos( $Where['Keyword'], "Not Assigned" ) !== false || strpos( $Where['Keyword'], "not assigned" ) !== false) {
                $this->db->or_where("S.KeyStatusID",0);
            }
            $this->db->group_end();
        }


        $this->db->order_by('S.KeyAssignedOn','DESC');
        /* Total records count only if want to get multiple records */
        if($multiRecords){ 
            $TempOBJ = clone $this->db;
            $TempQ = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
        }

        $Query = $this->db->get(); //echo $this->db->last_query();
        $StudentData = $Query->result_array();      
        $Records = array();
        if(!empty($StudentData)){ $i=0;
            foreach ($Query->result_array() as $record) {

                if(!empty($record['KeyAssignedOn']))    {
                    $record['KeyAssignedOn'] = date("d-m-Y h:i:s", strtotime($record['KeyAssignedOn']));
                }

                if(!empty($record['ActivatedOn']))  {
                    $record['ActivatedOn'] = date("d-m-Y h:i:s", strtotime($record['ActivatedOn']));
                }

                if(!empty($record['ExpiredOn']))    {
                    $record['ExpiredOn'] = date("d-m-Y h:i:s", strtotime($record['ExpiredOn']));
                }

                array_push($Records, $record); 
            }
            return $Records;
        }else{
            return $Records;
        }

    }



    function getEnquiryList($Where=array(), $PageNo=1, $PageSize=10)
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
        

        $PageNo = ($PageNo -1) * $PageSize;

        //Filter the records based on search filters applied----------------------------
        $filter = "";

        if(!empty($Where['filterStatus']) && $Where['filterStatus'] == 2)
        {           
            $filter .= " AND e.EnquiryAssignToUser > 0 ";
        }
        elseif(!empty($Where['filterStatus']) && $Where['filterStatus'] == 1)
        {
            $filter .= " AND e.EnquiryAssignToUser <= 0 ";
        }

        if(!empty($Where['filterSource']))
        {
            $filterSource = $Where['filterSource'];
            $filter .= " AND e.EnquirySource = '".$filterSource."' ";
        }

        if(!empty($Where['filterAssignedToUser']))
        {           
            $filterAssignedToUser = $Where['filterAssignedToUser'];
            $filter .= " AND e.EnquiryAssignToUser = ".$filterAssignedToUser;
        }

        if(!empty($Where['filterInterestedIn']))
        {
            $filterInterestedIn = $Where['filterInterestedIn'];
            $filter .= " AND e.EnquiryInterestedIn = '".$filterInterestedIn."' ";
        }

        if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
        {
            $filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
            $filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

            $filter .= " AND (DATE(e.EnquiryDate) >= '$filterFromDate' AND DATE(e.EnquiryDate) <= '$filterToDate' ) ";
        }

        
        $sql = "SELECT e.EnquiryGUID,   e.EnquiryPersonName,    e.EnquiryMobile,    e.EnquiryEmail,     e.EnquiryInterestedIn,  e.EnquiryRemarks,   e.EnquirySource, DATE_FORMAT(e.EnquiryDate, '%d-%M-%Y') as EnquiryDate, e.EnquiryAssignToUser,  s.EnquiryStatusName, CONCAT(u.FirstName, ' ', u.LastName) as AssignedToUser
        FROM tbl_enquiry e
        INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
        LEFT JOIN tbl_users u ON e.EnquiryAssignToUser = u.UserID
        WHERE e.StatusID = 2 AND e.EnquiryInstituteID = '$InstituteID' $filter              
        ORDER BY e.EnquiryID
        LIMIT $PageNo, $PageSize
        ";          

        $Query = $this->db->query($sql);

        $Records = array();
        
        if($Query->num_rows()>0)
        {
            $Return['Data']['TotalRecords'] = $Query->num_rows();

            foreach($Query->result_array() as $Record)
            {
                $Records[] = $Record;
            }

            $Return['Data']['Records'] = $Records;

            return $Return;
        }

        return FALSE;       
    }


    function AAQStudentList($EntityID, $Where=array())
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        
        $PageNo = $Where['PageNo'];
        $PageSize = $Where['PageSize'];

        $PageNo = ($PageNo - 1) * $PageSize;

        $filter = "";
        if(!empty($Where['StreamID']))
        {
            $filter .= " AND c1.CategoryID = ".$Where['StreamID'];
        }
        if(!empty($Where['CourseID']))
        {
            $filter .= " AND c.CategoryID = ".$Where['CourseID'];
        }
        if(!empty($Where['BatchID']))
        {
            $filter .= " AND b.BatchID = ".$Where['BatchID'];
        }


        $sql = "SELECT * FROM
        (
        SELECT u.StateName as State, u.CityName as City, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `Student Name`, s.Email, s.PhoneNumber as Mobile, s.FeeID, s.InstallmentID, u.PhoneNumber, s.TotalFeeAmount, b.BatchName as `Batch Details`, DATE_FORMAT(e.EntryDate, '%d-%M-%Y') as RegistrationDate, c.CategoryName as Course, c1.CategoryName as Stream, s.Key, DATE_FORMAT(s.KeyAssignedOn, '%d-%M-%Y') as `Assigned Date`, DATE_FORMAT(s.ActivatedOn, '%d-%M-%Y') as `Activation Date`, DATE_FORMAT(s.ExpiredOn, '%d-%M-%Y') as `Expiry Date`, IF(s.KeyStatusID = 1, 'Inactive', IF(s.KeyStatusID = 2, 'Active', 'Not Assigned')) as Status, DATE(s.KeyAssignedOn) as AssignedDate, DATE(s.ActivatedOn) as ActivationDate, DATE(s.ExpiredOn) as ExpiryDate, IF(u.ProfilePic IS NULL,CONCAT('".BASE_URL."','uploads/profile/picture/','default.jpg'),CONCAT('".BASE_URL."','uploads/profile/picture/',u.ProfilePic)) AS ProfilePic, e.EntityID as StudentID
        FROM tbl_students s
        INNER JOIN tbl_users u ON s.StudentID = u.UserID
        INNER JOIN tbl_entity e ON u.UserID = e.EntityID AND e.StatusID != 3 AND e.InstituteID = $InstituteID
        INNER JOIN tbl_batch b ON b.BatchID = s.BatchID
        INNER JOIN set_categories c ON c.CategoryID = s.CourseID AND c.CategoryTypeID = 2
        INNER JOIN set_categories c1 ON c.CategoryID = s.CourseID AND c.ParentCategoryID = c1.CategoryID AND c1.CategoryTypeID = 1      
        WHERE e.InstituteID = $InstituteID $filter
        ORDER BY c1.CategoryName, c.CategoryName, u.FirstName
        ) as temp
        WHERE 1 = 1
        LIMIT $PageNo, $PageSize ";        
        
        $Query = $this->db->query($sql);

        $Records = array();       
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                $Records[] = $Record;
            }           
        }

        return $Records;
    }


    function AAQStudentQuestionList($EntityID, $Inputs = array())
    {  
        $data = $arr = array(); 

        //$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        
        $PageNo = 1; $PageSize = 50;
        if(isset($Inputs['PageNo']) && !empty($Inputs['PageNo']))
        {
            $PageNo = $Inputs['PageNo'];
        }
        if(isset($Inputs['PageSize']) && !empty($Inputs['PageSize']))
        {
            $PageSize = $Inputs['PageSize'];
        }

        $PageNo = ($PageNo - 1) * $PageSize;

        $StudentID = $Inputs['StudentID'];

        $Records = array();

        $sql = "SELECT a.QueryContent, a.MediaID, a.QueryID, a.QueryCaption, a.EntryDate
        FROM tbl_ask_a_question a              
        INNER JOIN tbl_entity e ON a.EntityID = e.EntityID AND e.StatusID = 2 AND e.InstituteID = $InstituteID
        WHERE a.EntityID = $StudentID AND a.QueryType = 'Query'
        ORDER BY a.QueryID
        LIMIT $PageNo, $PageSize        
        ";

        $Query = $this->db->query($sql);

        if($Query->num_rows()>0)
        {   
            $this->load->model('Media_model');
            foreach($Query->result_array() as $Record)
            {
                $Record['MediaURL'] = $Record["MediaExt"] = "";
                if($Record['MediaID'] > 0)            
                {               
                    $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AskQuestion',"MediaID" => $Record['MediaID']),TRUE);
                    $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                    $Record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
                    $Record["MediaExt"] = pathinfo($MediaID['Records'][0]['MediaURL'], PATHINFO_EXTENSION);
                }


                $Records[] = $Record;                
            }            
        }

        return $Records;   
    }


    function AAQFacultyReplyList($EntityID, $Inputs = array())
    {
        //$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        
        $PageNo = 1; $PageSize = 50;
        if(isset($Inputs['PageNo']) && !empty($Inputs['PageNo']))
        {
            $PageNo = $Inputs['PageNo'];
        }
        if(isset($Inputs['PageSize']) && !empty($Inputs['PageSize']))
        {
            $PageSize = $Inputs['PageSize'];
        }

        $PageNo = ($PageNo - 1) * $PageSize;
        
        $QuestionID = $Inputs['QuestionID'];

        $Records = array();       

        $sql = "SELECT a.QueryContent, a.MediaID, a.QueryID, a.QueryCaption, a.EntryDate, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as fullname, IF(u.ProfilePic IS NULL, CONCAT('".BASE_URL."','uploads/profile/picture/','default.jpg'), CONCAT('".BASE_URL."','uploads/profile/picture/',u.ProfilePic)) AS ProfilePic
        FROM tbl_ask_a_question a              
        INNER JOIN tbl_entity e ON a.EntityID = e.EntityID AND e.StatusID = 2
        INNER JOIN tbl_users u ON u.UserID = a.EntityID AND u.UserTypeID IN(11,90) 
        WHERE a.ParentQueryID = $QuestionID AND a.QueryType = 'Reply'
        ORDER BY a.QueryID
        LIMIT $PageNo, $PageSize         
        ";

        $Query = $this->db->query($sql);

        if($Query->num_rows()>0)
        {   
            $this->load->model('Media_model');
            foreach($Query->result_array() as $Record)
            {
                $Record['MediaURL'] = $Record["MediaExt"] = "";
                if($Record['MediaID'] > 0)            
                {               
                    $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'AskQuestion',"MediaID" => $Record['MediaID']),TRUE);
                    $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                    $Record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];
                    $Record["MediaExt"] = pathinfo($MediaID['Records'][0]['MediaURL'], PATHINFO_EXTENSION);
                }

                $Records[] = $Record;                
            }            
        }

        return $Records;   
    }


    function AnnoucementList($EntityID, $Where=array(), $multiRecords=FALSE,$PageNo=1, $PageSize=10)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        
        $PageNo = $Where['PageNo'];
        $PageSize = $Where['PageSize'];

        $PageNo = ($PageNo - 1) * $PageSize;

        $filter = "";
        
        if(!empty($Where['BatchID']))
        {
            $filter .= " AND (FIND_IN_SET (' ".$Where['BatchID']."', a.AnnoucementBatchID) OR FIND_IN_SET ('".$Where['BatchID']."', a.AnnoucementBatchID)) ";
        }

        if(!empty($Where['FacultyID']))
        {
            $filter .= " AND a.AnnoucementFacultyID = ".$Where['FacultyID'];
        }

        if(!empty($Where['startDate']) && !empty($Where['endDate']))
        {
            $filter .= " AND a.AnnoucementCreatedDate BETWEEN '".$Where['startDate']."' AND '".$Where['endDate']."' ";
        }else if(!empty($Where['endDate']))
        {
            $filter .= " AND a.AnnoucementCreatedDate like '%".$Where['endDate']."%' ";
        }else if(!empty($Where['startDate']))
        {
            $filter .= " AND a.AnnoucementCreatedDate like '%".$Where['startDate']."%' ";
        }

        $LIMIT = "";

        if(!empty($PageNo) && !empty($PageSize))
        {
            $LIMIT = " LIMIT $PageNo, $PageSize ";
        }


        /*$sql = "SELECT * FROM
        (
        SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `FacultyName`, b.BatchName as `BatchName`, c.CategoryName as Course, c1.CategoryName as Stream, a.AnnoucementMessage, DATE_FORMAT(a.AnnoucementForDate, '%d-%M-%Y') as `AnnoucementDate`, DATE_FORMAT(a.AnnoucementForDate, '%H-%i-%s') as `AnnoucementTime`
        FROM tbl_annoucement a
        INNER JOIN tbl_users u ON a.AnnoucementFacultyID = u.UserID        
        INNER JOIN tbl_batch b ON b.BatchID = a.AnnoucementBatchID
        INNER JOIN set_categories c ON c.CategoryID = b.CourseID AND c.CategoryTypeID = 2
        INNER JOIN set_categories c1 ON c.CategoryID = b.CourseID AND c.ParentCategoryID = c1.CategoryID AND c1.CategoryTypeID = 1      
        WHERE a.AnnoucementInstituteID = $InstituteID $filter
        ORDER BY u.FirstName
        ) as temp
        WHERE 1 = 1
        LIMIT $PageNo, $PageSize ";*/   

       $sql = "SELECT * FROM
        (
        SELECT a.AnnoucementID, CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as `FacultyName`, a.AnnoucementMessage, DATE_FORMAT(a.AnnoucementForDate, '%d-%M-%Y') as `AnnoucementDate`, DATE_FORMAT(a.AnnoucementForDate, '%i:%i:%s') as `AnnoucementTime`, a.AnnoucementBatchID
        FROM tbl_annoucement a
        INNER JOIN tbl_users u ON a.AnnoucementFacultyID = u.UserID                   
        WHERE a.AnnoucementInstituteID = $InstituteID $filter        
        ORDER BY u.FirstName, u.LastName
        ) as temp
        WHERE 1 = 1
        $LIMIT";     
        
        $Query = $this->db->query($sql);

        $Records = array();       
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                if(isset($Record['AnnoucementBatchID']))
                {
                    $str = "";
                    $arr = $this-> getBatchName($Record['AnnoucementBatchID']);
                    if(isset($arr) && !empty($arr))
                        $str = implode(",", $arr);
                    
                    $Record['BatchName'] = $str;
                }

                $Records[] = $Record;
            }           
        }

        return $Records;
    }


    function getBatchName($Ids)
    {
        $sql = "SELECT BatchName 
        FROM tbl_batch
        WHERE BatchID IN ($Ids)
        ORDER BY BatchName";     
        
        $Query = $this->db->query($sql);

        $Records = array();       
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                $Records[] = $Record['BatchName'];
            }           
        }

        return $Records;
    }

    function AnnoucementDetail($EntityID, $Where=array())
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        
        $PageNo = $Where['PageNo'];
        $PageSize = $Where['PageSize'];

        $PageNo = ($PageNo - 1) * $PageSize;
        
        $AnnoucementID = $Where['AnnoucementID'];
        
        $sql = "SELECT AnnoucementStudentID 
        FROM tbl_annoucement        
        WHERE AnnoucementID = '$AnnoucementID' AND AnnoucementInstituteID = '$InstituteID'
        LIMIT 1";     
        
        $Query = $this->db->query($sql);

        $Records = array();       
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                if(isset($Record['AnnoucementStudentID']))
                {                    
                    $str = "";
                    
                    $arr = $this-> getStudentName($Record['AnnoucementStudentID']);
                    
                    if(isset($arr) && !empty($arr))
                        $str = implode(",", $arr);
                    
                    $Record['StudentName'] = $str;
                }

                unset($Record['AnnoucementStudentID']);

                $Records[] = $Record;
            }           
        }

        return $Records;
    }


    function getStudentName($Ids)
    {
        $sql = "SELECT u.FirstName, u.LastName
        FROM tbl_students s
        INNER JOIN tbl_users u ON u.UserID = s.StudentID
        WHERE u.UserID IN ($Ids)
        ORDER BY u.FirstName, u.LastName";     
        
        $Query = $this->db->query($sql);

        $Records = array();       
        
        if($Query->num_rows()>0)
        {
            foreach($Query->result_array() as $Record)
            {                
                $Records[] = trim($Record['FirstName']).' '.trim($Record['LastName']);
            }           
        }

        return $Records;
    }


    /* get fee collection*/
    function getFeeCollections($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15)
    {    
       // print_r($Where); 
       //date_column BETWEEN >= '2014-01-10' AND '2015-01-01';   
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $this->db->select('tbl_users.UserID, tbl_students.CourseID,tbl_students.BatchID,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.FeeStatusID,tbl_students.TotalFeeAmount,tbl_students.TotalFeeInstallments,tbl_students.InstallmentAmount,tbl_fee_collection.*,tbl_users.UserGUID,tbl_users.FirstName,tbl_users.LastName,CONCAT(U.FirstName, " ", IF(U.LastName IS NOT NULL, U.LastName,"")) as fullname');
        $this->db->from("tbl_fee_collection");
        $this->db->join("tbl_students","tbl_fee_collection.StudentID = tbl_students.StudentID",'left');
        $this->db->join("tbl_users","tbl_users.UserID = tbl_students.StudentID",'left');
        $this->db->join("tbl_users as U","U.UserID = tbl_fee_collection.UserID",'left');
        $this->db->join("set_categories as SC","SC.CategoryID = tbl_fee_collection.CourseID",'left');


        if(!empty($Where['Keyword'])){ /*search in post content*/
            $this->db->group_start();
            $this->db->like("tbl_fee_collection.Amount", $Where['Keyword']);
            $this->db->or_like("tbl_fee_collection.PaymentDate", $Where['Keyword']);
            $this->db->or_like("tbl_fee_collection.PaymentMode", $Where['Keyword']);
            $this->db->or_like("tbl_fee_collection.TransactionID", $Where['Keyword']);
            $this->db->or_like("tbl_users.FirstName", $Where['Keyword']);
            $this->db->or_like("tbl_users.LastName", $Where['Keyword']);
            $this->db->or_like("U.FirstName", $Where['Keyword']);
            $this->db->or_like("U.LastName", $Where['Keyword']);
            $this->db->or_like("EntryDate", $Where['Keyword']);
            $this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("CONCAT_WS(' ',tbl_users.FirstName,tbl_users.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("CONCAT_WS('',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("CONCAT_WS(' ',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->group_end();
        }


        $this->db->where("tbl_fee_collection.InstituteID",$InstituteID);        


        if(!empty($Where['StreamID'])){
            $this->db->where("SC.ParentCategoryID",$Where['StreamID']);
        }
        if(!empty($Where['CourseID'])){
            $this->db->where("tbl_students.CourseID",$Where['CourseID']);
        }
        if(!empty($Where['BatchID'])){
            $this->db->where("tbl_students.BatchID",$Where['BatchID']);
        }
        if(!empty($Where['FeeCollectionID'])){
            $this->db->where("FeeCollectionID",$Where['FeeCollectionID']);
        }
        if(!empty($Where['startDate']) && !empty($Where['endDate'])){
            $Where['startDate'] = date("Y-m-d",strtotime($Where['startDate']));
            $Where['endDate'] = date("Y-m-d",strtotime($Where['endDate']));
            $this->db->where("PaymentDate BETWEEN '".$Where['startDate']."' AND '".$Where['endDate']."'");
        }else if(!empty($Where['startDate'])){
            $Where['startDate'] = date("Y-m-d",strtotime($Where['startDate']));
            $this->db->where("PaymentDate",$Where['startDate']);
        }else if(!empty($Where['endDate'])){
            $Where['endDate'] = date("Y-m-d",strtotime($Where['endDate']));
            $this->db->where("PaymentDate",$Where['endDate']);
        }
        $this->db->order_by("FeeCollectionID","DESC");
        if($multiRecords){ 
            $TempOBJ = clone $this->db;
            $TempQ = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
        }
        $Query =  $this->db->get(); //echo $this->db->last_query(); die;
        //$StudentData = $Query->result_array(); //print_r($StudentData);
        $Records = array(); $balance = 0;
        if($Query->num_rows() > 0)
        {
            foreach ($Query->result_array() as $record) 
            {
                $UserID = $record['PaymentDate'];

                $record['PaymentDate'] = date("d-m-Y",strtotime($record['PaymentDate']));
                $record['ChequeDate'] = date("d-m-Y",strtotime($record['ChequeDate']));

                if(!empty($record['CourseID'])){
                    $query = $this->db->query("select CategoryGUID,CategoryName from set_categories where CategoryID = ".$record['CourseID']);
                    $CategoryData = $query->result_array();
                    $record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
                    $record['CourseName'] = $CategoryData[0]['CategoryName'];
                }


                if(!empty($record['BatchID'])){
                    $query = $this->db->query("select BatchName from tbl_batch where BatchID = ".$record['BatchID']);
                    $BatchName = $query->result_array();
                    $record['BatchName'] = $BatchName[0]['BatchName'];
                }

                if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
                    $record['Fee Status'] = "Unpaid";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Fee Status'] = "Paid";
                }else{
                    $record['Fee Status'] = "Unpaid";
                }    


                $record['TotalFee'] = 0;                
                $record['NoOfInstallment'] = 0;                                   
                if(!empty($record['FeeID']))
                {
                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();
                    $record['TotalFee'] = $FeeData[0]['FullDiscountFee'];
                    $record['NoOfInstallment'] = 1;
                    $record['InstallmentAmount'] = $record['TotalFee'];                     
                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {
                        $record['TotalFee'] = $InstallmentData[0]['TotalFee'];
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                        $record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['InstallmentAmount'] = 0;
                }
                


                $record['PaidEMICount'] = 0;
                $record['RemainingEMI'] = 0;
                if(!empty($Where['FeeCollectionID'])){
                     $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$record['StudentID']." and FeeCollectionID <= ".$Where['FeeCollectionID']);
                }else{
                     $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$record['StudentID']);
                }
                $SumData = $query->result_array();  
                if(!empty($SumData))
                {
                    $record['PaidEMICount'] = $SumData[0]['FeeCollectionID'];                    
                }
                $record['RemainingEMI'] = $record['NoOfInstallment'] - $record['PaidEMICount'];  



                if(!empty($Where['FeeCollectionID'])){
                     $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$record['StudentID']." and FeeCollectionID <= ".$Where['FeeCollectionID']);
                }else{
                     $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$record['StudentID']);
                }                
                $SumData = $query->result_array();  
                if(!empty($SumData)){
                    $record['PaidAmount'] = $SumData[0]['PaidAmount'];
                }else{
                    $record['PaidAmount'] = 0;
                } 

                $record['RemainingAmount'] = (int)$record['TotalFee'] - (int)$record['PaidAmount'];                  
                $record['Denomination'] = json_decode($record['Denomination']);

                $StudentIDchk = $record['StudentID'];
                $query = $this->db->query("SELECT DueDateID, date_format(DueDate, '%d-%M-%Y') as DueDate, date_format(ReceivedDate, '%d-%m-%Y') as ReceivedDate, IsPaid 
                    FROM tbl_students_feeduedate 
                    WHERE StudentID = ".$StudentIDchk." AND InstituteID = '".$InstituteID."'" );
                $DueDates = $query->result_array();
                $record['DueDates'] = array();
                if(isset($DueDates) && !empty($DueDates))
                {
                    $record['DueDates'] = $DueDates;
                }



                if(!empty($record['MediaID']) && $record['MediaID'] != NULL && $record['PaymentMode'] == "Cheque")
                {
                    $this->load->model('Media_model');
                    $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'OfflinePayment',"MediaID" => $record['MediaID']),TRUE);
                    $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                    $record['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
                    $record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];                    
                }else{
                    $record['MediaThumbURL'] = "";
                    $record['MediaURL'] = "";
                }

                $balance = $record['Amount'] + $balance;
                $record['balance'] = $balance;
                //print_r($record);
                array_push($Records, $record);
            }
            //print_r($Records);
            return $Records;
        }else{
            return $Records;
        }
    }
}