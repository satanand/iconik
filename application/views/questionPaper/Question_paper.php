<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<header class="panel-heading">
	<h1 class="h4">Manage Question Paper</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getList()"><!-- Body -->
	<div class="clearfix mt-2 mb-2">
	   <div class="row float-left ml-6" style="width:80%;">
	      <div class="col-md-4" style="margin-left: 35px;">
	         <div class="form-group">
	            <select class="form-control" onchange="getCategorySubjects(this.value,'getQuestionPaperStatistics')" name="ParentCat" id="Courses">
	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
	               <option ng-repeat="row in categoryDataList" value="{{$index}}">{{row.CategoryName}}</option>
	            </select>
	         </div>
	      </div>
	      <div class="col-md-4">
	         <div class="form-group">
	            <div id="subcategory_section">
	               <select id="subject" name="CategoryGUIDs" onchange="filterQuestionBankStatistics(this.value)"  class="form-control">
	                  <option value="">Select Subject</option>
	               </select>
	            </div>
	         </div>
	      </div>
	   </div>
	   <div class="float-right mr-2">		
			<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Generate Question Paper</button>
		</div>
	</div>

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">


	<!-- Data table -->
	<div class="table-responsive block_pad_md"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		<table class="table table-striped table-hover">
			<!-- table heading -->
			<thead>
				<tr>
					<th style="width: 250px;">Course</th>
					<th >Subject</th>
					<th style="width: 150px;">Question Paper</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody ng-repeat="(key, row) in data.dataList">
				<tr scope="row"  ng-if="row.SubCategoryName == 'Total'" style="background: #EAECEE;">
					<td>{{row.ParentCategoryName}}</td>
					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.SubCategoryName}}</b></td>
					<td ng-if="row.SubCategoryName != 'Total'">{{row.SubCategoryName}}</td>
					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.question_count}}</b></td>
					<td ng-if="row.SubCategoryName != 'Total'">{{row.question_count}}</td>
					<td class="text-center" style="text-align: center;">
						<!-- <a class="glyphicon glyphicon-edit" href="<?php //echo base_url().'questionbank/questions_list/?parentCategoryGUID={{row.parentCategoryGUID}}&CategoryGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Questions list"></a> -->
					</td>
				</tr>
				<tr scope="row" ng-if="row.SubCategoryName != 'Total'">
					<td >{{row.ParentCategoryName}}</td>
					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.SubCategoryName}}</b></td>
					<td ng-if="row.SubCategoryName != 'Total'">{{row.SubCategoryName}}</td>
					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.question_count}}</b></td>
					<td ng-if="row.SubCategoryName != 'Total'">{{row.question_count}}</td>
					<td class="text-center" style="text-align: center;">
						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'questionpaper/list/?parentCategoryGUID={{row.parentCategoryGUID}}&CategoryGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Questions Paper"></a>
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="!data.dataList.length">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->




	<!-- add Modal -->
	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Generate Question Paper</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>
</div><!-- Body/ -->