<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<header class="panel-heading"> <h2 class="h4" style="text-align: left;">Manage Gallery</h2> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body"><!-- Body -->



	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">
		<div class="row">
			<div class="col-md-4">
				<span class="float-left records d-none d-sm-block">

					<span class="h5">Total Records: {{data.dataList.length}}</span>

				</span>
			</div>
		</div>

		<div class="row">
 		
				<!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
			
	   
		<!-- <div class="col-md-4">
			 <form class="form-inline" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()">
			 	<div id="universal_search">
	                <input type="text" class="form-control" name="Keyword" placeholder="Type and search album" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
	                <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
	                <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
	            </div>
			</form>
		</div> --> 
		<div class="col-md-6">
			<select class="form-control" name="Category" id="Category" onchange="getCategorybyList(this.value)">
				<option value="">All Album</option>
				<option ng-repeat="(key, rowc) in data.CategoryData" value="{{rowc.AlbumName}}">{{rowc.AlbumName}}</option>
			</select>
		</div>
        <div class="col-md-6" style="float:right;">
        	<div class="float-right">
	         <button class="btn btn-success btn-sm" ng-click="loadFormAddCategory();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add New Album</button>
	         <button class="btn btn-success btn-sm" style="margin-left: 10px;" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Entry</button>
           </div>
        </div>
		

	</div>
		
	
</div>
           

	<!-- Top container/ -->




	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getGalleryStatistics()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>
					<th >Album</th>
					<!-- <th style="width: 300px;">Album</th> -->
					<th style="width: 200px;">Images</th>
					
					<th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>



			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">

					
					<td>
						<strong>{{row.AlbumName}}</strong><br>
						<a  class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.AlbumID)" data-toggle="tooltip" title="Edit" data-toggle="tooltip" title="View"></a>

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEditAlbum(key, row.AlbumID)" data-toggle="tooltip" title="Edit" data-toggle="tooltip" title="Edit"></a>

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="DeleteAlbum(key, row.AlbumID, row.AlbumName)" data-toggle="tooltip" title="Delete" data-toggle="tooltip" title="Delete"></a>
					</td>
					<td>
						<strong>{{row.pictures_count}}</strong>
					</td>
				
					<!-- <td>
						<img src="{{row.MediaThumbURL}}">
					</td> -->
					
					<td class="text-center" style="text-align: center;">
						<a  class="glyphicon glyphicon-eye-open" href="" ng-click="loadPhotoGallery(key, row.AlbumID, row.AlbumName)"  data-toggle="tooltip" title="View"></a>

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-plus" href="" ng-click="loadFormAddPhoto(row.AlbumID, row.AlbumName)"  data-toggle="tooltip" title="Add photos in album"></a>

						<!-- <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormMediaDelete(key, row.MediaGUID)" data-toggle="tooltip" title="Delete" data-toggle="tooltip" title="Delete"></a> -->
					</td>

				</tr>
			</tbody>

		</table>

		<!-- <div class="jcarousel-wrapper">
	        <div class="jcarousel" data-jcarousel="true">
	            <ul style="left: 0px; top: 0px;">
	                <li ng-repeat="photo in photos"><img src="{{photo.src}}" width="600" height="400" alt=""></li>
	            </ul>
	        </div>

	        <p class="photo-credits">
	            Photos by <a href="http://www.mw-fotografie.de/">Marc Wiegelmann</a>
	        </p>

	        <a href="http://127.0.0.1/public_html/gallery/#" class="jcarousel-control-prev inactive" data-jcarouselcontrol="true">‹</a>
	        <a href="http://127.0.0.1/public_html/gallery/#" class="jcarousel-control-next" data-jcarouselcontrol="true">›</a>
	        
	        <p class="jcarousel-pagination" data-jcarouselpagination="true"><a href="https://sorgalla.com/jcarousel/examples/basic/#1" class="active">1</a><a href="https://sorgalla.com/jcarousel/examples/basic/#2">2</a><a href="https://sorgalla.com/jcarousel/examples/basic/#3">3</a><a href="https://sorgalla.com/jcarousel/examples/basic/#4">4</a><a href="https://sorgalla.com/jcarousel/examples/basic/#5">5</a><a href="https://sorgalla.com/jcarousel/examples/basic/#6">6</a></p>
	        
	    </div> -->

		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->

	<!-- add Album Category -->

	<div class="modal fade" id="addcategory_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add New Album</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="category_form" name="category_form" autocomplete="off" ng-include="templateURLCategory">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

		<!-- add Modal -->

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Entry</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Entry</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				
				<!-- /form -->
			</div>
		</div>
		</div><!-- Body/ -->

		<!-- view Modal -->
		<div class="modal fade" id="view_model">
		   <div class="modal-dialog modal-md" role="document">
		      <div class="modal-content">
		         <div class="modal-header">
		            <h3 class="modal-title h5">View Entry</h3>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		         </div>
		         <!-- form -->
		         <form id="view_form" name="view_form" autocomplete="off" ng-include="templateURLView">
		         </form>
		         <!-- /form -->
		      </div>
		   </div>
		</div>


		<!-- view Modal -->
		<div class="modal fade" id="view_gallery_model">
		   <div class="modal-dialog modal-md" role="document" style="width: 1000px; max-width: 1000px;">
		      <div class="modal-content">
		         <div class="modal-header">
		            <h3 class="modal-title h5">View Gallery</h3>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		         </div>
		         <!-- form -->
		         <div  ng-include="templateURLViewGallery"></div>
		         <!-- /form -->
		      </div>
		   </div>
		</div>

		<!-- edit Modal -->
		<div class="modal fade" id="edit_album_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Edit Entry</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<!-- <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEditAlbum">
					</form> -->
					<div  ng-include="templateURLEditAlbum"></div>
					<!-- /form -->
				</div>
			</div>
		</div><!-- Body/ -->

</div>





