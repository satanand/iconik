<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Library extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Library_model');
		$this->load->model('Walloffame_model');
		$this->load->model('Utility_model');
	}



	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getFilterData_post()
	{	
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
		$LibraryDataC = $this->Library_model->getCategory('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		$LibraryDataA = $this->Library_model->getAuthor('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

	
		$this->Return['Data']['Category'] = $LibraryDataC;
		$this->Return['Data']['Author'] = $LibraryDataA;	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getLibrary_post()
	{
		$this->form_validation->set_rules('Category', 'Category','trim');
		$this->form_validation->set_rules('Author', 'Author','trim');
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('FormType', 'FormType','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
		$LibraryData = $this->Library_model->getLibrary('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		//print_r($LibraryData);

		if(!empty($LibraryData)){
			$this->Return['Data'] = $LibraryData['Data'];
		}	
	}




	/*

	Description: 	Use to add new book

	URL: 			/api_admin/book/add	

	*/

	public function add_post()
	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Title', 'Title', 'trim|required');

		 $this->form_validation->set_rules('Category', 'Category', 'trim|required');
		 $this->form_validation->set_rules('Author', 'Author', 'trim|required');
		 $this->form_validation->set_rules('AccessionNumber', 'Accession Number', 'trim|required');
		 
		 $this->form_validation->set_rules('Synopsis', 'Synopsis', 'trim');
		 $this->form_validation->set_rules('FormType', 'FormType', 'trim');
		 $this->form_validation->set_rules('TotalBook', 'TotalBook', 'trim|required');
	
		 $this->form_validation->set_rules('MediaGUIDBook', 'Book Icon','trim');
		 //$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');
		$this->form_validation->validation($this);  /* Run validation */	

		 
		$LibraryData = $this->Library_model->addLibrary($this->Post);

		//echo $LibraryData;

			if($LibraryData == 1){

				$this->Return['Message']      	=	"New Books added successfully."; 

			} else {
                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Books Accession Number Already Exist.";
				
			}


	}

	public function addd_post()
	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Title', 'Title', 'trim|required');
		 $this->form_validation->set_rules('Category', 'Category', 'trim|required');
		 $this->form_validation->set_rules('Synopsis', 'Synopsis', 'trim');
		 $this->form_validation->set_rules('FormType', 'FormType', 'trim');
		 $this->form_validation->set_rules('DigitalType', 'DigitalType', 'trim');
		 $this->form_validation->set_rules('AudioGUID', 'AudioGUID', 'trim');
		 $this->form_validation->set_rules('VedioURL', 'VedioURL', 'trim');
		 $this->form_validation->set_rules('MediaGUID', 'File','trim');
		 $this->form_validation->set_rules('MediaGUIDBook', 'Book Icon','trim');
		 //$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');
		$this->form_validation->validation($this);  /* Run validation */	

		
		    

	
		$LibraryData = $this->Library_model->addLibrary($this->Post);

		//echo $LibraryData;

			if($LibraryData == 1){

				$this->Return['Message']      	=	"New Books added successfully."; 

			} else {
                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Books Accession Number Already Exist.";
				
			}


	}

		public function delete_post()
	{
		/* Validation section */
		//$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required|callback_validateEntityGUID[Media, MediaID]');
		//$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		//echo $this->input->Post('MediaGUID'); die();
		$data=$this->Library_model->delete($this->input->Post('BookGUID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
	}

	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function addcategory_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Category', 'Category', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		
		$LibraryData = $this->Library_model->addcategory($this->Post);

		//echo $LibraryData;

			if($LibraryData == 1){

				$this->Return['Message']      	=	"Add New Book Category successfully."; 

			} else {

                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Books Category Name Already Exist.";
				
			}


	}



	public function getCategory_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CategoryData = $this->Library_model->getCategory('',array());
		if(!empty($CategoryData)){
			$this->Return['Data'] = $CategoryData['Data'];
		}	
	}

	/*
	Description: 	Use to get Get single .
	URL: 			/api/getLibraryBy/getLibraryBy
	Input (Sample JSON): 		
	*/
	public function getLibraryBy_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('BookGUID', 'BookGUID','trim|required');
		$this->form_validation->set_rules('FormType', 'FormType','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$LibraryData = $this->Library_model->getLibraryBy('',array("BookGUID"=>$this->input->post('BookGUID'),"FormType"=>$this->input->post('FormType')));
		if(!empty($LibraryData)){
			$this->Return['Data'] = $LibraryData;
		}	
	}

	/*getReturnBook*/

	public function getReturnBook_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('BookGUID', 'BookGUID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$LibraryData = $this->Library_model->getReturnBook('',array("BookGUID"=>$this->input->post('BookGUID')));
		if(!empty($LibraryData)){
			$this->Return['Data'] = $LibraryData;
		}	
	}

	/*

	Name: 			editLibrary_post
	

	*/
	public function editLibrary_post()
    {
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Title', 'Title', 'trim|required');
		$this->form_validation->set_rules('Category', 'Category', 'trim|required');
		$this->form_validation->set_rules('Author', 'Author', 'trim|required');
		$this->form_validation->set_rules('AccessionNumber', 'AccessionNumber', 'trim|required');
        $this->form_validation->set_rules('BookGUID', 'BookGUID', 'trim|required');
		$this->form_validation->set_rules('Synopsis', 'Synopsis', 'trim');
		 $this->form_validation->set_rules('TotalBook', 'TotalBook', 'trim|required');

		$this->form_validation->set_rules('AudioGUID', 'AudioGUID', 'trim');

		$this->form_validation->set_rules('VedioURL', 'VedioURL', 'trim');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');

		$this->form_validation->set_rules('Audio', 'Audio', 'trim');
		

		$this->form_validation->set_rules('MediaID', 'MediaID', 'trim');
		$this->form_validation->set_rules('MediaIDBook', 'MediaIDBook', 'trim');
		$this->form_validation->set_rules('MediaGUIDBook', 'MediaGUIDBook', 'trim');
		$this->form_validation->set_rules('FormType', 'FormType', 'trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
			if(!empty($this->Post['MediaGUID'])){

				$this->Library_model->DeteleMedia($this->input->Post('MediaID'));
			}
			if(!empty($this->Post['MediaGUIDBook'])){

				$this->Library_model->DeteleMedia($this->input->Post('MediaIDBook'));
			}
			if(!empty($this->Post['AudioGUID'])){

				$this->Library_model->DeteleMedia($this->input->Post('Audio'));
			}
		
		$LibraryData = $this->Library_model->editLibrary($this->Post);

		if($LibraryData == 1){

			$this->Return['Data']= $this->Library_model->getLibrary('',array("BookGUID"=>$this->input->Post('BookGUID')));

	    	$this->Return['Message']      	=	"Library updated successfully."; 

		} else {
            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"This Books Accession Number Already Exist.";			
		}
	}	

	public function editLibraryd_post()
    {
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Title', 'Title', 'trim|required');
		$this->form_validation->set_rules('Category', 'Category', 'trim|required');
		$this->form_validation->set_rules('BookGUID', 'BookGUID', 'trim|required');
		$this->form_validation->set_rules('Synopsis', 'Synopsis', 'trim');
		$this->form_validation->set_rules('AudioGUID', 'AudioGUID', 'trim');
		$this->form_validation->set_rules('VedioURL', 'VedioURL', 'trim');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		$this->form_validation->set_rules('Audio', 'Audio', 'trim');
		$this->form_validation->set_rules('MediaID', 'MediaID', 'trim');
		$this->form_validation->set_rules('MediaIDBook', 'MediaIDBook', 'trim');
		$this->form_validation->set_rules('MediaGUIDBook', 'MediaGUIDBook', 'trim');
		$this->form_validation->set_rules('DigitalType', 'DigitalType', 'trim');
		$this->form_validation->set_rules('FormType', 'FormType', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
			if(!empty($this->Post['MediaGUID'])){

				$this->Library_model->DeteleMedia($this->input->Post('MediaID'));
			}
			if(!empty($this->Post['MediaGUIDBook'])){

				$this->Library_model->DeteleMedia($this->input->Post('MediaIDBook'));
			}
			if(!empty($this->Post['AudioGUID'])){

				$this->Library_model->DeteleMedia($this->input->Post('Audio'));
			}
		
		$this->Library_model->editLibrary($this->Post);
	
		$this->Return['Data']= $this->Library_model->getLibrary('',array("BookGUID"=>$this->input->Post('BookGUID')));

	    $this->Return['Message']      	=	"Library updated successfully."; 

	}


	/* permission */
	public function AddIssueBook_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');

		$this->form_validation->set_rules('BookID', 'BookID', 'trim|required');

		$this->form_validation->set_rules('IssueDate', 'IssueDate', 'trim|required|callback_validateDate');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
         
      //echo $this->Post['UserID']; die();
       	$this->Library_model->AddIssueBook($this->Post); 

		
		$this->Return['Data']= $this->Library_model->getLibrary('',array("BookGUID"=>$this->input->Post('BookGUID')));

	    $this->Return['Message']      	=	"Library updated successfully."; 
	
		}
		/* EditReturnBook */
  public function EditReturnBook_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');

		$this->form_validation->set_rules('BookID', 'BookID', 'trim|required');

		$this->form_validation->set_rules('ReturnDate', 'ReturnDate', 'trim|required|callback_validateDate');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
         
     //echo $this->Post['UserID']; die();
       	$this->Library_model->EditReturnBook($this->Post); 

		
		$this->Return['Data']= $this->Library_model->getLibrary('',array("BookGUID"=>$this->input->Post('BookGUID')));

	    $this->Return['Message']      	=	"Library updated successfully."; 
	
		}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getContentCategories_post()
	{
		$ContentCategoryData = $this->Category_model->getContentCategories();
		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}

     /*getUserData*/
	public function getUserData_post()
	{
		$this->form_validation->set_rules('keyvalue', 'keyvalue', 'trim');
		$this->form_validation->set_rules('BookGUID', 'BookGUID', 'trim');
		$this->form_validation->validation($this);
		$UsersData = $this->Library_model->getUserData($this->Post);
		if(!empty($UsersData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $UsersData['Data'];
		}	
	}
}