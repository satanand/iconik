<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulkemail extends Admin_Controller_Secure
{
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		// $load['css']=array(
		// 	'asset/plugins/chosen/chosen.min.css',
		// 	'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
		// );
		
		$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',	
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js'
		);	

		$load['breadcrumb'] = array(
			"Dashboard" => base_url()."dashboard",
			"Communication" => "",
			"Bulk Email" =>  ""
		);


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('bulkemail/bulkemail');
		$this->load->view('includes/footer');
	}


	public function signature()
	{
		// $load['css']=array(
		// 	'asset/plugins/chosen/chosen.min.css',
		// 	'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
		// );
		
		$load['js']=array(
			'asset/js/bulkemail.js',	
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.3/jquery.tinymce.min.js'
		);

		$load['breadcrumb'] = array(
			"Dashboard" => base_url()."dashboard",
			"Communication" => "",
			"Manage Signature" =>  ""
		);	


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('bulkemail/signature');
		$this->load->view('includes/footer');
	}


}