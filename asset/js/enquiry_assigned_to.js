app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentParentCategoryGUID = ParentCategoryGUID;

    $scope.CategoryTypeID = $scope.getUrlParameter('CategoryTypeID');

    $scope.APIURL = API_URL + "upload/image";
    /*----------------*/

    $( "#filterFromDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterToDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#filterToDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterFromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    $scope.getFilterData = function ()

    {
        /*$scope.getSecondLevelPermission();
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();

        $http.post(API_URL+'admin/category/getFilterData', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ 

             $scope.filterData =  response.Data;

             $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 300);          

         }

        });*/

    }





    /*list*/

    $scope.applyFilter = function ()

    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        $scope.getList(0);

    }





    /*list append*/

    $scope.getList = function (pgfrm)
    {        
        if(pgfrm == 1)
        {            
            $scope.data.dataList = [];
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        } 

        $scope.data.listLoading = true;       
        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
     

        $http.post(API_URL+'enquiry/getEnquiryAssignedToList', data, contentType).then(function(response) 
        {
                var response = response.data;

                if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data.Records) 
                    {

                     $scope.data.dataList.push(response.Data.Records[i]);

                    }

                     $scope.data.pageNo++;               

                }else{

                    $scope.data.noRecords = true;
                }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }



    /*load add form*/

    $scope.loadFormAdd = function (Position, EnquiryGUID)
    {
        $scope.data.Position = Position;

        $scope.data.dataCallListDetails = [];

        $scope.templateURLAdd = PATH_TEMPLATE+module+'/call_add_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'enquiry/getEnquiryOfAssignedUser', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data.EnquiryData;

                $scope.LoginUser = response.LoginUser;

                if(response.CallData.Data.Records)
                {
                    for (var i in response.CallData.Data.Records) 
                    {
                        $scope.data.dataCallListDetails.push(response.CallData.Data.Records[i]);
                    }
                }    
                
                $('#add_model').modal({show:true});

                $timeout(function(){   

                    $( "#FollowUpDate" ).datepicker({
                      dateFormat: "dd-mm-yy",
                      changeMonth: true,
                      minDate:0
                    });         

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }

        });

    }

    
    /*add data*/

    $scope.addData = function ()
    {
        $scope.editDataLoading = true;        

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'enquiry/addCallData', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);  

                $scope.applyFilter();              

                $timeout(function()
                {            

                   $('#add_model .close').click();

                }, 200);

            }
            else
            {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }




    /*load view form*/

    $scope.loadFormView = function (Position, EnquiryGUID)

    {
        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+module+'/view_call_history.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $scope.data.dataListDetails = [];

        $http.post(API_URL+'enquiry/getCallLogHistoryOfUser', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;
                $scope.LoginUser = response.LoginUser;

                    for (var i in response.Data.Records) 
                    {

                        $scope.data.dataListDetails.push(response.Data.Records[i]);

                    }
                    

                $('#view_model').modal({show:true});
                $scope.data.pageLoading = false;
            }   

        });

    }


    $scope.loadFormAssigned = function ()
    {        
        window.location.href = BASE_URL + "enquiry/assigned";
    }



}); 



function toggle_call_log()
{
    $("#CallListDetails").toggle();
}


function search_records()
{
    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function clear_search_records()
{    
    $("#filterStatus, #filterSource, #filterFromDate, #filterToDate, #Keyword").val("");    

    search_records();
}

/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

            type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }
}
/*
function ParentcategoryCheck(CatType){
    if(CatType == 'Stream'){
        $("#SelectParentCategory").hide();
        $("#category-name-label").text("Stream Name");
        $("#select-label-parent-category").text("");
        $("#course_duration").hide();
    }else if(CatType == 'Course'){
        $("#SelectParentCategory").show();
        $("#category-name-label").text("Course Name");
        $("#select-label-parent-category").text("Select Stream");
        $("#course_duration").show();
        $("#course_duration").val("");
    }else if(CatType == 'Subject'){
        $("#category-name-label").text("Subject Name");
        $("#SelectParentCategory").show();
        $("#select-label-parent-category").text("Select Course");
        $("#course_duration").hide();
    }else{
       $("#category-name-label").text("Name");
       $("#SelectParentCategory").hide();
       $("#select-label-parent-category").text("Select Parent"); 
       $("#course_duration").hide();
    }
}*/
/* sortable - ends */