app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.Users = [];

    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentParentCategoryGUID = ParentCategoryGUID;

    $scope.CategoryTypeID = $scope.getUrlParameter('CategoryTypeID');

    $scope.APIURL = API_URL + "upload/image";
    /*----------------*/

    $scope.getFilterData = function ()
    {
        $scope.AssignedToUsersList();

        /*$scope.getSecondLevelPermission();
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();

        $http.post(API_URL+'admin/category/getFilterData', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ 

             $scope.filterData =  response.Data;

             $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 300);          

         }

        });*/

    }





    /*list*/

    $scope.applyFilter = function ()

    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        

        $scope.getList(0);

    }


    /*------------------------------------------------------------------
    Load master filters
    -----------------------------------------------------------------*/
    $scope.AssignedToUsersList = function ()
    {
        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'enquiry/getAssignedToUsersList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Users)
            {
                if($scope.data.Users.length <= 0)
                {
                    for (var i in response.Users) 
                    {
                        $scope.data.Users.push(response.Users[i]);
                    }
                }
            }
        });
    }


    /*list append*/

    $scope.getList = function (IsSet)
    {
        if(IsSet == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }

        $scope.data.listLoading = true;

        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'enquiry/getChurnedEnquiryList', data, contentType).then(function(response) 
        {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.Records)
                { /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data.Records) 
                    {

                     $scope.data.dataList.push(response.Data.Records[i]);

                    }

                     $scope.data.pageNo++;         

                }
                else
                {

                    $scope.data.noRecords = true;
                }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }   



    /*load add Assign Enquiry*/
    $scope.loadFormAssign = function (Position, EnquiryGUID)
    {
        $scope.data.Position = Position;

        $scope.templateURLAssign = PATH_TEMPLATE+'enquiry/assign_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'enquiry/ReassignEnquiry', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;

                $('#assign_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }
            else
            {
                $scope.data.pageLoading = false;

                ErrorPopup(response.Message);
            } 
        });

    }
    

    $scope.assignData = function ()
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='assign_form']").serialize();

        $http.post(API_URL+'enquiry/addReAssignEnquiry', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);                

                $timeout(function(){            

                   $('#assign_model .close').click();

               }, 200);

            }
            else
            {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }

}); 





/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

            type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }
}
/*
function ParentcategoryCheck(CatType){
    if(CatType == 'Stream'){
        $("#SelectParentCategory").hide();
        $("#category-name-label").text("Stream Name");
        $("#select-label-parent-category").text("");
        $("#course_duration").hide();
    }else if(CatType == 'Course'){
        $("#SelectParentCategory").show();
        $("#category-name-label").text("Course Name");
        $("#select-label-parent-category").text("Select Stream");
        $("#course_duration").show();
        $("#course_duration").val("");
    }else if(CatType == 'Subject'){
        $("#category-name-label").text("Subject Name");
        $("#SelectParentCategory").show();
        $("#select-label-parent-category").text("Select Course");
        $("#course_duration").hide();
    }else{
       $("#category-name-label").text("Name");
       $("#SelectParentCategory").hide();
       $("#select-label-parent-category").text("Select Parent"); 
       $("#course_duration").hide();
    }
}*/
/* sortable - ends */

function search_records()
{
    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function clear_search_records()
{    
    $("#filterStatus, #filterSource, #filterAssignedToUser, #Keyword").val("");    

    search_records();
}