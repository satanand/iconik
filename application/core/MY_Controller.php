<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_Controller extends CI_Controller {
	public function __construct()
	{
		
		parent::__construct();
		// echo "sadfasdf";die;
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$Input = file_get_contents("php://input");
			$this->Post =  json_decode($Input,1);
			if(empty($this->Post)){
				parse_str($Input,$this->Post);
			}
		}
		//if ($this->session->userdata('UserData')) {redirect(base_url().'dashboard');exit();}		
	}
}

class Admin_Controller extends Main_Controller {
	public function __construct()
	{
		
		parent::__construct();
		if ($this->session->userdata('UserData')) {redirect(base_url().'dashboard');exit();}	
	}
}

class Admin_Controller_Secure extends Main_Controller {
	public function __construct()
	{
		parent::__construct();
		/* Ensure already Signed in */
		if (empty($this->session->userdata('UserData'))) {
			redirect(base_url()); exit;
		}
		$this->SessionData 		=	$this->session->userdata('UserData'); 
		//$this->ModuleData 		= 	ValidateUserAccess($this->SessionData['PermittedModules'],$this->uri->segment(1));
		$this->ModuleData		= 	ValidateUserAccess($this->SessionData['PermittedModules'],$this->uri->uri_string);
		$this->SessionKey		=	$this->SessionData['SessionKey'];	
		$this->InstituteGUID	=	$this->SessionData['InstituteGUID'];	
		$this->UserGUID			=	$this->SessionData['UserGUID'];
		$this->Menu				=	$this->SessionData['Menu'];
		$this->StoreGUID		=	$this->SessionData['StoreGUID'];
		$this->UserTypeID 		= 	$this->SessionData['UserTypeID'];
	}	
}