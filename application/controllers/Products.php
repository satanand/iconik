<?php      
defined('BASEPATH') OR exit('No direct script access allowed');
class Products extends Admin_Controller_Secure 
{
	
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);		
		

		$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',			
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);		

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Products" => ""
		);		


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('products/products_list');
		$this->load->view('includes/footer');
	}


	public function orders()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/products.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',			
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);		

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Orders" => ""
		);		


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('products/orders_list');
		$this->load->view('includes/footer');
	}


	public function generateInvoice()
	{
		$data['content'] = array();
		
		if(!empty($_GET['oid']) && isset($_SESSION['UserData']['SessionKey']) && !empty($_SESSION['UserData']['SessionKey']))
		{
			$this->load->helper('custom_helper');

			$_POST['SessionKey'] = $_SESSION['UserData']['SessionKey'];
			$_POST['OrderID'] = $_GET['oid'];

			$JSON = json_encode($_POST);
			
			$Response = APICall(API_URL.'products/getInvoiceDetails', $JSON);
			$data['content'] = $Response;
			
			$report_title = "Invoice";
			$header_title = $data['report_title'] = ucwords($report_title);			
			$data['session_data'] = $_SESSION['UserData'];

			if($Response['ResponseCode'] == 200)
			{
				$html = $this->load->view('products/invoice_pdf', $data, true);
				
				$pdfFilePath = $report_title.$data['content']['Data']['Order']['CustomerName'].".pdf";
		        
		        $this->load->library('m_pdf');
		        
		 		$this->m_pdf->pdf->autoScriptToLang = true;
				$this->m_pdf->pdf->baseScript = 1;
				$this->m_pdf->pdf->autoVietnamese = true;
				$this->m_pdf->pdf->autoLangToFont = true;

		 		$this->m_pdf->pdf->setFooter($header_title." (".'{PAGENO}'.")");
		       	
		        $this->m_pdf->pdf->WriteHTML($html);		 
		        
		        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
			}
			else
			{
				$msg = $Response['Message'];
				echo "<script> alert('".$msg."'); window.history.go(-1); </script>";
				die;
			}
		}
	
	}
	
}