
app.controller('PageController', function($scope, $http, $timeout) 
{
    $( "#FilterStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    $scope.APIURL = API_URL + "upload/image";

    $scope.data.qualification = [];

    $scope.data.states = [];



    $scope.getFilterData = function()
    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }    
    }


    $scope.applyFilter = function() 
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList(0);
        $scope.getFilterData();
    }

    $scope.getProductCategoryList = function ()
    {
        $scope.data.CategorySubCategoryList = [];       
        
        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'products/getCategoryList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.CategorySubCategoryList = response.Data.Records.CategorySubCategory;
                /*for (var i in response.Data.Records.CategorySubCategory) 
                {
                    $scope.data.CategorySubCategoryList.push(response.Data.Records.CategorySubCategory[i]);
                } */              
            }
        });
    }


    $scope.getList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'products/getCoupons', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                
                $scope.data.pageNo++;
            }
            else
            {
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });

    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) 
    {
        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add.htm?' + Math.random();

        $('#add_model').modal({
            show: true
        });

        

        //$scope.getProducts();

        $scope.getProductCategoryList();

        $timeout(function() {

            $( "#ExpiryDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              minDate:0
            });

            $("#ProductsInclude, #ProductsCategory").select2();

        }, 1000);
        
    }


    $scope.addData = function() 
    {
        $scope.addDataLoading = true;
        
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();

        $http.post(API_URL + 'products/coupons_add', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
                
                $timeout(function() 
                {
                    window.location.href = window.location.href;

                }, 1000);
            } 
            else 
            {
                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }


    $scope.editData = function() 
    {
        $scope.addDataLoading = true;
        
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();

        $http.post(API_URL + 'products/coupons_edit', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
               
                $timeout(function() 
                {
                    window.location.href = window.location.href;
                }, 1000);
            } 
            else 
            {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;
        });
    }



    $scope.loadFormDelete = function(Position, CouponsID) 
    {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL + 'products/coupons_delete', 'SessionKey=' + SessionKey + '&CouponsID=' + CouponsID, contentType).then(function(response) 
            {
                var response = response.data;
                if (response.ResponseCode == 200) 
                {
                    /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.applyFilter();
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


    $scope.loadFormView = function(Position, ProductID, Data) 
    {
        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLView = PATH_TEMPLATE + module + '/view.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#View_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }

    


    $scope.loadFormEdit = function(Position, ProductID, Data) 
    {
        $scope.data.Position = Position;
        $scope.formData = Data;        

        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#edits_model').modal({
            show: true
        });

        
        $scope.getProductCategoryList();

        $timeout(function() 
        {
            $( "#ExpiryDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              minDate:0
            });

            $("#ProductsInclude, #ProductsCategory").select2();
            //console.log($scope.formData.ProductsInclude);
            
            var PXArr = $scope.formData.ProductsCategory.split(",");
            $(PXArr).each(function(ii, vv)
            {
                $("#ProductsCategory option[value='"+vv+"']").prop('selected' , true);
                //$("#ProductsCategory").trigger("change");                  
            });
            $("#ProductsCategory").trigger("change");                  
                                  
            

        }, 1000);


        $timeout(function() 
        {
            var PXArr1 = $scope.formData.ProductsInclude.split(",");
            $(PXArr1).each(function(ii, vv)
            {
                
                $("#ProductsInclude option[value='"+vv+"']").prop('selected' , true);
                $("#ProductsInclude").trigger("change");  
            }); 

        }, 5000);    

        $scope.data.pageLoading = false;
    }


    $scope.getProducts = function (check=0)
    {        
        var cat = $("#ProductsCategory").val();
        if(cat == "" || cat <= 0)
        {
            //alert("Please select product subcategory first.");
            //return false;
            cat = 0;
        }

        $scope.ProductsSubCategory = cat;
        
        $scope.data.ProductsBySubCategoryList = [];
        var data = 'SessionKey='+SessionKey+'&ProductsSubCategory='+cat;

        $http.post(API_URL+'products/getProductsBySubCategory', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data)
            {
                for (var i in response.Data) 
                {
                    $scope.data.ProductsBySubCategoryList.push(response.Data[i]);
                }                
            }
        });

    }


});



function clear_filters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword").val('');

    angular.element(document.getElementById('content-body')).scope().getList(1);
}