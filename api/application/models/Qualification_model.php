<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Qualification_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to add new Qualification
	*/
	function addQualification($EntityID, $Input=""){

		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);


		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);

		//$this->db->trans_start();

		$EntityGUID = get_guid();

		$this->db->select('QualificationID');
		$this->db->from('set_qualification');
		$this->db->where('LOWER(`Qualification`)="'.strtolower($Input['Qualification']).'"');
        //$this->db->where('InstituteID',$InstituteID);
        $this->db->where('StatusID !=', 3);
	   
	    $query = $this->db->get();

	    //echo $this->db->last_query(); 
	    //echo $query->num_rows(); die;
	    if ( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

	    	/* Add Roles */
			$InsertData = array_filter(array(
				"Qualification"  =>	$Input['Qualification'],
				"Description" 		=>	@$Input['Description'],	
				"UserID" 		=>	$EntityID,	
				"InstituteID" 		=>	$InstituteID,
				"EntryDate"  => date("Y-m-d H:i:s")
			));
			
			$this->db->insert('set_qualification', $InsertData);

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return TRUE;
		}
	}



	/*
	Description: 	Use to update roles.
	*/
	function editQualification($EntityID, $QualificationID, $Input){
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('QualificationID');
		$this->db->from('set_qualification');
		$this->db->where('LOWER(`Qualification`)="'.strtolower($Input['Qualification']).'"');
        //$this->db->where('InstituteID',$InstituteID);
        $this->db->where('QualificationID != '.$QualificationID);
        $this->db->where('StatusID != 3');
	   
	    $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if ( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{
			$UpdateArray = array_filter(array(
				"Qualification"  =>	$Input['Qualification'],
				"Description" 		=>	@$Input['Description'],	
				"UserID" 		=>	$EntityID,	
				"InstituteID" 		=>	$InstituteID,
				"ModifyDate"  => date("Y-m-d H:i:s")
			));

			if(!empty($UpdateArray)){
				$this->db->where('QualificationID', $QualificationID);
				$data=$this->db->update('set_qualification', $UpdateArray);
			}

			
			return TRUE;
		}
	}

	
	/*
	Description: 	Use to get Cetegories
	*/
	function getQualification($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('*');
		$this->db->from('set_qualification');
		$this->db->where('QualificationID NOT IN (select QualificationID from tbl_delete_qualification where InstituteID = '.$InstituteID.')');
		//$this->db->where("InstituteID",$InstituteID);

		if(!empty($Where['QualificationID'])){
			$this->db->where("QualificationID",$Where['QualificationID']);
		}

		if(isset($InstituteID) && !empty($InstituteID))
		{
			//$where = " InstituteID = $InstituteID OR InstituteID = '' OR InstituteID IS NULL ";
			//$this->db->where($where);
		}

		$this->db->where("StatusID",2);

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->group_start();
			$this->db->like("Qualification", $Where['Keyword']);
			$this->db->or_like("Description", $Where['Keyword']);
			$this->db->or_like("ModifyDate", $Where['Keyword']);	
			$this->db->or_like("EntryDate", $Where['Keyword']);	
			$this->db->group_end();
		}

		$this->db->order_by('QualificationID','DESC');

		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die;
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				
				if($Record['EntryDate'] != NULL){
					$Record['EntryDate'] = date("d-m-Y H:i:s", strtotime($Record['EntryDate']));
				}else{
					$Record['EntryDate'] = "";
				}

				if($Record['ModifyDate'] != NULL){
					$Record['ModifyDate'] = date("d-m-Y H:i:s", strtotime($Record['ModifyDate']));
				}else{
					$Record['ModifyDate'] = "";
				}				
				
				if(!$multiRecords){
					return $Record;
				}
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	


	




	function deleteQualification($EntityID, $Input){
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);

		$DeleteArray = array_filter(array(
			"QualificationID" =>	$Input['QualificationID'],
			"InstituteID"  => $InstituteID
		));

		if(!empty($DeleteArray)){
			$data=$this->db->insert('tbl_delete_qualification', $DeleteArray);
		}

		
		return TRUE;
	}


}