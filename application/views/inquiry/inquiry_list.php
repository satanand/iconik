<header class="panel-heading" > <h1 class="h4">Manage Inquiry</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body"><!-- Body -->
   <!-- Top container -->
   <br>
   <br>
   <br>

   <h6>Under Development.....</h6>
	<!-- Top container/ -->
	<!-- Data table -->

	

	 
	   <!-- add Modal -->

		<div class="modal fade" id="add_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Create <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>
	   <!-- add Asaining Modal -->

		<div class="modal fade" id="addasaining_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5"> Assign <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="addasaining_form" name="addasaining_form" autocomplete="off" ng-include="templateURLAddAs">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>

	   <!-- add Modal -->

		<div class="modal fade" id="edit_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>
      <!-- assing -->
		<div class="modal fade" id="editAssign_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Edit Assignment </h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="editAssign_form" name="editAssign_form" autocomplete="off" ng-include="templateURLAssignEdit">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>

         <!-- permission form -->

		<div class="modal fade" id="permission_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Permission <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="permission_form" name="permission_form" autocomplete="off" ng-include="templateURLPermission" method="POST">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>

</div><!-- Body/ -->







