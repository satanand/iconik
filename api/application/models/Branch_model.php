<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}
		/*
	Description: 	Use to add new category
	*/
	function addBranch($data=array()){
		//print_r($data); die;
		

		$EntityGUID = get_guid();
        $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$EntityID=$this->Entity_model->addEntitys($EntityGUID,array('UserID'=>$this->UserID,"EntityTypeID"=>1,'InstituteID'=>$InstituteID,"StatusID"=>1));
	    	
		   $this->Entity_model->updateEntityInfo($EntityID, array("StatusID"=>1,'InstituteID'=>$EntityID));
			/* Add Branch */
			$InsertData = array_filter(array(
				"UserID"        =>	$EntityID,
				"UserGUID"      =>	$EntityGUID,
				"ParentUserID" => $this->EntityID,
				"FirstName"     =>	trim($data['FirstName']),	
				"UserTypeID" 	=>	10,	
				"Email" 	    =>	$data['Email'],	
				"Address" 		=>	$data['Address'],	
				"CityName" 		=>	$data['CityName'],	
				"StateID" 	    =>	$data['StateName'],	
				"CountryCode" 	=>	$data['CountryCode'],
				"CityCode" 	    =>	$data['CityCode'],
				"Postal"    => $data['CityCode']
				
			));

			//print_r($InsertData); die();

			$this->db->insert('tbl_users', $InsertData);
			// echo $this->db->last_query();
			// echo $insert_id = $this->db->insert_id(); die;

			if($EntityID > 0){
				$branchData = $this->getBranchByID($EntityGUID);

				if(!empty($data['Email'])){

					$InstituteData = $this->Common_model->getInstituteDetailByID($this->EntityID,"Email,UserID,FirstName");

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $this->EntityID),TRUE);
			        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
			
			        if(!empty($Record['InstituteProfilePic'])){
				    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
				    }else{
				    	$MediaURL= "";
				    }

	                $this->load->model('Recovery_model');
	               // $InstituteEmail = $this->Common_model->getUserEmailByID($this->UserID);
					$EmailText = "As discussed, please find the registration link below to register for using the Iconik Platform";

					$content = $this->load->view('emailer/invitation_email',array("Name" => trim($data['FirstName']), "InstituteProfilePic"=>@$MediaURL,"UseID"=>$EntityID,"Signature"=>"Thanks,<br>".$InstituteData['FirstName'],"EmailText"=>$EmailText,"Email"=>trim($data['Email'])),TRUE);

					$SendMail = sendMail(array(
						//'From_email'    => $InstituteEmail,

						"From_name"  => $InstituteData['FirstName'],

						'emailTo' 		=> $data['Email'],			

						'emailSubject'	=>$data['FirstName'].", Invitation to Register",

						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

					));

					
				}
				if($SendMail==FALSE){

					return FALSE;

				}else{

				   return $branchData;
				}
				
			}else{
				return FALSE;
			}
		
	}

	

	
/* get branch data*/
	function getBranch($EntityGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('U.UserID,U.UserGUID,U.FirstName,U.Address,U.CityName,U.CityCode,U.Postal,U.StateID,U.Email,E.EntityGUID,E.StatusID,S.StateName');
		$this->db->from('tbl_users U');
		$this->db->join('tbl_entity E', 'E.EntityGUID = U.UserGUID');
        $this->db->join("set_location_states S",'S.State_id=U.StateID');
        $this->db->where("U.ParentUserID",$InstituteID);
        $this->db->where("U.UserID != ".$this->EntityID);
        $this->db->where("U.ParentUserID!=",'');
        $this->db->where("E.EntityTypeID",1);
        $this->db->where("E.StatusID!=",3);

        if(!empty($EntityGUID)){
        	$this->db->where("U.UserGUID",$EntityGUID);
        }

        if(!empty($Where['Keyword'])){

			$Where['Keyword'] = trim($Where['Keyword']);
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.Email", $Where['Keyword']);
				$this->db->or_like("U.Address", $Where['Keyword']);
				$this->db->or_like("U.CityName", $Where['Keyword']);
				$this->db->or_like("U.CityCode", $Where['Keyword']);
				$this->db->or_like("U.Postal", $Where['Keyword']);
				$this->db->or_like("S.StateName", $Where['Keyword']);
				$this->db->group_end();	
						
			}

		$this->db->order_by('E.EntityID','DESC');

		 if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	

	

    /*edit get Branch data*/
	function getBranchByID($EntityGUID){

		$this->db->select('InstituteName,UserID,UserGUID,FirstName,LastName,Email,Address,CityName,StateName,StateID,CityCode,ParentUserID');
		$this->db->from('tbl_users');
		$this->db->limit(1);
		if(!empty($EntityGUID)){
			$this->db->where("UserGUID",$EntityGUID);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				//echo $Record['ParentUserID']; die();
		$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $Record['ParentUserID']),TRUE);
			$Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
			if(!empty($Record['InstituteProfilePic'])){
				$Record['MediaThumbURL'] = $Record['InstituteProfilePic']['Records'][0]['MediaThumbURL'];
				$Record['MediaURL'] = $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
			}else{
				$Record['MediaThumbURL'] = "";
				$Record['MediaURL'] = "";
			}
			
				$Records = $Record;
			}
          return $Records;
		}
		return FALSE;		
	}

    /*
	Description: 	Use to update Branch.
	*/
	function editBranch($data){
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$UpdateArray = array_filter(array(
				"ParentUserID" => $this->EntityID,
				"InstituteName" =>	$data['BranchName'],
				"FirstName" 	=>	$data['FirstName'],
				"LastName" 		=>	$data['LastName'],		
				"Email" 	    =>	$data['Email'],	
				"Address" 		=>	$data['Address'],	
				"CityName" 		=>	$data['CityName'],	
				"StateID" 	    =>	$data['StateName'],	
				"CountryCode" 	=>	$data['CountryCode'],
				"CityCode" 	    =>	$data['CityCode'],	
				"Postal"    => $data['CityCode']
				
			));

		if(!empty($UpdateArray)){
			$this->db->where('UserGUID', $data['UserGUID']);
			$this->db->limit(1);
			$data=$this->db->update('tbl_users', $UpdateArray);
		}

		return TRUE;
	}

	
	/*
	Description: 	Use to update Branch.
	*/
	function editEmailBranch($data){

       $UserGUID= $data['UserGUID'];
        $UserID= $data['UserID'];
        $Email=$data['Email'];
        $Name= $data['FirstName']; 
        $MediaURL=$data['MediaURL']; 

		$this->db->select('UserID');
		$this->db->from('tbl_users');
		$this->db->where("Email",$Email);
		$this->db->where("UserID != ".$UserID);
		$this->db->limit(1);
		$Query = $this->db->get();
		//echo $this->db->last_query(); die();
		if($Query->num_rows() > 0){

			return 'exist';

		}else{

			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$data = $this->db->update('tbl_users', array("Email"=>$Email));

	               
	                $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

					$InstituteData = $this->Common_model->getInstituteDetailByID($this->EntityID,"Email,UserID,FirstName");
					
					$EmailText = "As discussed, please find the registration link below to register for using the Iconik Platform";
					
					$content = $this->load->view('emailer/invitation_email',array("Name" => $Name,"InstituteProfilePic"=>@$MediaURL,"UserID"=>$UserID,"EmailText"=>$EmailText,"Email"=>$Email),TRUE);

					$SendMail = sendMail(array(
						//'From_email'    => $InstituteEmail,

						'From_name'    => $InstituteData['FirstName'],

						'emailTo' 		=> $Email,			

						'emailSubject'	=> $Name .", Invitation to Register",

						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

					));


			if($SendMail == 1){
				return 1;
			}else{
				return 2;
			}	
		}

	}

	/*
	Description: 	Use to update Inactive.
	*/
	function Inactive($EntityGUID){

		$UpdateArray = array_filter(array(

				"StatusID" =>3
			));

		if(!empty($UpdateArray)){
			$this->db->where('EntityGUID', $EntityGUID);
			$this->db->limit(1);
			$data=$this->db->update('tbl_entity', $UpdateArray);
		}

		return TRUE;
	}
	/*
	Description: 	Use to update Active.
	*/
	function Active($EntityGUID){

		$UpdateArray = array_filter(array(

				"StatusID" => 1
			));

		if(!empty($UpdateArray)){
			$this->db->where('EntityGUID', $EntityGUID);
			$this->db->limit(1);
			//$this->db->last_query(); die();
			$data=$this->db->update('tbl_entity', $UpdateArray);
		}

		return TRUE;
	}

	/*
	Description: 	Instituted to delete.
	*/
	function delete($UserID){

		$UpdateArray = array_filter(array(

				"StatusID" => 3
			));

		if(!empty($UpdateArray)){
			$this->db->where('EntityID', $UserID);
			$this->db->limit(1);
			$data=$this->db->update('tbl_entity', $UpdateArray);
		}

		return TRUE;
	}
/* get branch data*/
	function getBranchd($EntityGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('U.InstituteName,U.UserID,U.UserGUID,U.UserName,U.Email,E.EntityGUID,E.StatusID');
		$this->db->from('tbl_users U');
		$this->db->join('tbl_entity E', 'E.EntityGUID = U.UserGUID');
        $this->db->where("E.InstituteID",$InstituteID);
        $this->db->where("E.CreatedByUserID",$this->UserID);
        $this->db->where("E.EntityTypeID",18);
        
        if(!empty($EntityGUID)){
        	$this->db->where("U.UserGUID",$EntityGUID);
        }
		$this->db->order_by('InstituteName','ASC');

		 if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$Query = $this->db->get();	

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				if(!$multiRecords){
					return $Record;
				}
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}


	function deleteBatch($EntityID, $BatchID){
		$this->db->where("BatchID",$BatchID);
		$this->db->update("tbl_batch",array("StatusID"=>3));
	}

}