<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Roles_model');
		$this->load->model('Recovery_model');
		$this->load->model('Users_model');
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
	}



	/*
	Description: 	Use to broadcast message.
	URL: 			/api_admin/users/broadcast/	
	*/
	public function broadcast_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Title', 'Title', 'trim|required');
		$this->form_validation->set_rules('Message', 'Title', 'trim|required');
		$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim'); /* Media GUIDs */
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

/*		$EntityGUID = get_guid();
		// Add to entity table and get EntityID.
		$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>11, "UserID"=>$this->SessionUserID, "StatusID"=>2));

		//check for media present - associate media with this Post
		if(!empty($this->Post['MediaGUIDs'])){
			$MediaGUIDsArray = explode(",", $this->Post['MediaGUIDs']);
			foreach($MediaGUIDsArray as $MediaGUID){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $EntityID);
				}
			}
		}*/
		/* check for media present - associate media with this Post - ends */

		$UsersData=$this->Users_model->getUsers('
			U.UserID,	
			U.Username			
			',array('AdminUsers' =>'No'), TRUE, 1, 1000);
		if($UsersData){
			$NotificationText 		= $this->Post['Title'];
			$NotificationMessage 	= $this->Post['Message'];
			foreach($UsersData['Data']['Records'] as $Value){
				/* send notification - starts */
				$this->Notification_model->addNotification('broadcast', $NotificationText, $this->SessionUserID, $Value['UserID'], '' , $NotificationMessage);
				/* send notification - ends */
			}
		}

		$this->Return['Message'] = 'Message broadcasted.';
	}

	/*
	Name: 			getUsers
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getProfile
	*/
	public function index_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.($this->UserTypeID==4 ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$UsersData=$this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee,OwnerName,Achivements,ScholarshipTest,Classrooms,Library,TeachingAids,HostelFacility,NumberOfRooms,StudentPerRooms,MessFacility',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}	
	/*
	Name: 			getUsersStaff
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getUsersStaff
	*/
	public function Staff_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.($this->UserTypeID==4 ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$UsersData=$this->Users_model->getUserss('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}	
	/*
	Name: 			getprofilr
	Description: 	Use to get users list.
	URL: 			/api_admin/users/getprofile
	*/
	public function profile_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.($this->UserTypeID==4 ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('AdminUsers', 'AdminUsers', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$UsersData=$this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType',$this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($UsersData){
			$this->Return['Data'] = $UsersData['Data'];
		}
	}

	/*
	Name: 			getModule
	Description: 	Use to get module.
	URL: 			/api_admin/users/geRoles
	*/
	public function getusertype_post()
	{
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$UserTypeData = $this->Users_model->getusertype($this->EntityID);

		if(!empty($UserTypeData)){
			$this->Return['Data'] = $UserTypeData['Data'];
		}
	}


	/*
	Description: 	Use to update user profile info.
	URL: 			/api_admin/entity/changeStatus/	
	*/
	public function changeStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->Entity_model->updateEntityInfo($this->UserID, array("StatusID"=>$this->StatusID));
		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,Status,StatusID',array("UserID" => $this->UserID));
		$this->Return['Message']  =	"Status has been changed.";
	}


	/*
	Description: 	Use to update user details as pan and bank details.
	URL: 			/api_admin/entity/changeVerificationStatus/	
	*/
	public function changeVerificationStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('VetificationType', 'VetificationType', 'trim|required');
		if($this->Post['VetificationType']=='PAN'){
			$this->form_validation->set_rules('PanStatus', 'PanStatus', 'trim|required|callback_validateStatus');	
		}
		if($this->Post['VetificationType']=='BANK'){
			$this->form_validation->set_rules('BankStatus', 'BankStatus', 'trim|required|callback_validateStatus');
		}
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		if($this->Post['VetificationType']=='PAN' && !empty($this->Post['PanStatus'])){
			$UpdateData = array("PanStatus"=>$this->StatusID);
		}
		if($this->Post['VetificationType']=='BANK' && !empty($this->Post['BankStatus'])){
			$UpdateData = array("BankStatus"=>$this->StatusID);
		}
		$this->Users_model->updateUserInfo($this->UserID, $UpdateData );
		$this->Return['Data']=$this->Users_model->getUsers('FirstName,LastName,Email,ProfilePic,Status,PanStatus,BankStatus',array("UserID" => $this->UserID));
		$this->Return['Message']      	=	"Status has been changed.";
	}




	/*
	Name: 			updateUserInfo
	Description: 	Use to update user profile info.
	URL: 			/api_admin/updateUserInfo/	
	*/
	public function updateUserProfile_post() 
	{
		/* Validation section */
		if(empty($this->input->post('UserTypeID'))){
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		}

		if($this->input->post('UserTypeID') == 7 && !empty($this->input->post('UserGUID'))){
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[Students,UserID]');
		}else if(!empty($this->input->post('UserGUID'))){
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		}
		//$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');

		$this->form_validation->set_rules('FirstName', 'Name', 'trim|required');
		$this->form_validation->set_rules('LastName', 'LastName', 'trim');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim');

		if($this->input->post('Type') == "MarketPlace"){
			$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('PhoneNumber', 'Phone Number|required','trim');
			$this->form_validation->set_rules('Address', 'Address','trim');
			$this->form_validation->set_rules('CountryCode', 'Country','trim');  
			$this->form_validation->set_rules('StateName', 'State','trim');
			$this->form_validation->set_rules('CityName', 'City','trim');  
			$this->form_validation->set_rules('Postal', 'Zip Code','trim');
			if($this->input->post('UserTypeID') == 7){
				$this->form_validation->set_rules('FathersName', 'Fathers Name','trim|required');
				$this->form_validation->set_rules('MothersName', 'Mothers Name','trim|required');
				$this->form_validation->set_rules('PermanentAddress', 'Permanent Address','trim|required');
				$this->form_validation->set_rules('FathersEmail', 'Fathers Email','trim|valid_email|required');
				$this->form_validation->set_rules('FathersPhone', 'Fathers Phone','trim|required');

				
				$this->form_validation->set_rules('GuardianName', 'Guardian Name','trim');
				$this->form_validation->set_rules('GuardianAddress', 'Guardian Address','trim');
				$this->form_validation->set_rules('GuardianEmail', 'Guardian Email','trim|valid_email');
				$this->form_validation->set_rules('GuardianPhone', 'Guardian Phone','trim');

				$this->form_validation->set_rules('Note', 'Note','trim');
			}
		}else{
			
			
			$this->form_validation->set_rules('Address', 'Address','trim|required');
			$this->form_validation->set_rules('CountryCode', 'Country Name','trim|required');  
			$this->form_validation->set_rules('StateID', 'State Name','trim|required');
			$this->form_validation->set_rules('CityName', 'City Name','trim|required');
			$this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('PhoneNumber', 'Mobile Number','trim|required');
		}
		
		//$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');
		
		$this->form_validation->set_rules('Gender', 'Gender','trim');
		$this->form_validation->set_rules('BirthDate', 'BirthDate','trim|callback_validateDate');
		$this->form_validation->set_rules('MaritalStatus', 'MaritalStatus','trim');
		$this->form_validation->set_rules('StateID', 'StateID','trim');
		$this->form_validation->set_rules('CityCode', 'CityCode','trim');
		$this->form_validation->set_rules('Course', 'Course','trim');
		$this->form_validation->set_rules('Institute', 'Institute','trim');
		$this->form_validation->set_rules('Specialization', 'Specialization','trim');
		$this->form_validation->set_rules('Year', 'Year','trim');
		$this->form_validation->set_rules('Percentage', 'Percentage','trim');
		$this->form_validation->set_rules('Applicable', 'Applicable','trim');
		$this->form_validation->set_rules('Reportto', 'Reportto','trim');
		$this->form_validation->set_rules('Designation', 'Designation','trim');
		$this->form_validation->set_rules('ReportingManager', 'ReportingManager','trim');
		$this->form_validation->set_rules('JoiningDate', 'JoiningDate','trim');
		$this->form_validation->set_rules('AppraisalDate', 'AppraisalDate','trim');
		$this->form_validation->set_rules('Employer', 'Employer','trim');
		$this->form_validation->set_rules('Industry', 'Industry','trim');
		$this->form_validation->set_rules('JobProfile', 'JobProfile','trim');
		$this->form_validation->set_rules('Experience', 'Experience','trim');
		$this->form_validation->set_rules('FacebookURL', 'FacebookURL','trim');
		$this->form_validation->set_rules('TwitterURL', 'TwitterURL','trim');
		$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL','trim');
		$this->form_validation->set_rules('GoogleURL', 'GoogleURL','trim');

		$this->form_validation->set_rules('OwnerName[]', 'OwnerName','trim');
		$this->form_validation->set_rules('Achivements', 'Achivements','trim');
		$this->form_validation->set_rules('ScholarshipTest', 'ScholarshipTest','trim');
		$this->form_validation->set_rules('Classrooms', 'Classrooms','trim');
		$this->form_validation->set_rules('Library', 'Library','trim');
		$this->form_validation->set_rules('TeachingAids[]', 'TeachingAids','trim');
		$this->form_validation->set_rules('HostelFacility', 'HostelFacility','trim');
		$this->form_validation->set_rules('NumberOfRooms', 'NumberOfRooms','trim');
		$this->form_validation->set_rules('StudentPerRooms', 'StudentPerRooms','trim');
		$this->form_validation->set_rules('MessFacility', 'MessFacility','trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('Type') == "MarketPlace" && $this->input->post('UserTypeID') == 7){
			//print_r($this->Post); die;
			$chk = $this->Users_model->addStudentInfo($this->UserID, array_merge($this->Post, array("StatusID"=>@$this->StatusID, "SkipPhoneNoVerification"=>true)));

			if(!empty($this->input->post('MediaName'))){
				$this->Users_model->updateUserInfo($this->SessionUserID, array($this->input->post('Section')=>$this->input->post('MediaName')));
			}

			if($chk == 2){
				$this->Return['Message']  =	"Updated Successfully.";
			}else{
				$this->Return['Message']  =	"An Email has been send to institute for profile update approval request.";
			}
			 	

		}else{
			
			if($this->input->post('Website') != "")
			{
				if(!validate_input_url($this->input->post('UrlType').$this->input->post('Website')))
				{
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message']      	=	"Please enter valid Website URL.";
					die;
				}
			}


			if($this->input->post('FacebookURL') != "")
			{
				if(!validate_input_url($this->input->post('FacebookURL')))
				{
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message']      	=	"Please enter valid Facebook URL.";
					die;
				}
			}

			if($this->input->post('TwitterURL') != "")
			{
				if(!validate_input_url($this->input->post('TwitterURL')))
				{
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message']      	=	"Please enter valid Twitter URL.";
					die;
				}
			}
			
			if($this->input->post('LinkedInURL') != "")
			{
				if(!validate_input_url($this->input->post('LinkedInURL')))
				{
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message']      	=	"Please enter valid LinkedIn URL.";
					die;
				}
			}

			if($this->input->post('GoogleURL') != "")
			{
				if(!validate_input_url($this->input->post('GoogleURL')))
				{
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message']      	=	"Please enter valid Google URL.";
					die;
				}
			}	


			$this->Users_model->updateUserInfo($this->UserID, array_merge($this->Post, array("StatusID"=>@$this->StatusID, "SkipPhoneNoVerification"=>true)));

			if(!empty($this->input->post('MediaName'))){
				$this->Users_model->updateUserInfo($this->SessionUserID, array($this->input->post('Section')=>$this->input->post('MediaName')));
			}
			

			$this->Return['Data'] = $this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email,EmailForChange,Username, ProfilePic, Gender, BirthDate, PhoneNumber,PhoneNumberForChange, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee',array("UserID" => $this->UserID));

			if(!empty($this->Return['Data']['EmailForChange'])){

				$Email=$this->input->Post('EmailForChange'); 

			}else{

				$Email=$this->Return['Data']['Email'];
			}

			if(!empty($this->Return['Data']['PhoneNumberForChange'])){
				$PhoneNo=$this->Return['Data']['PhoneNumberForChange'];
			}else{
				$PhoneNo=$this->Return['Data']['PhoneNumber'];
			}



			if(trim($this->input->Post('Email'))!==trim($Email) &&  $this->input->Post('Email') != ""){

			$this->Return['Message']  =	"An Email/SMS has been send for verifiction of new email id /mobile No."; 	

		   }elseif($this->input->Post('PhoneNumber')!=$PhoneNo  && $this->input->Post('PhoneNumber') != ""){

		   	$this->Return['Message']  =	"An Email/SMS has been send for verifiction of new email id /mobile No.";
		   	
		   }else{

		   	$this->Return['Message']  =	"Successfully updated."; 

		   }	
		}
	}




	/*
	Name: 			updateUserInfo
	Description: 	Use to update user profile info.
	URL: 			/api_admin/updateUserInfo/	
	*/
	public function updateUserInfo_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required|callback_validateEntityGUID[User,UserID]');
		//$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');
		$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|required');
		$this->form_validation->set_rules('LastName', 'LastName', 'trim|required');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');
		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email');
		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');
		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber','trim');
		$this->form_validation->set_rules('Gender', 'Gender','trim');
		$this->form_validation->set_rules('BirthDate', 'BirthDate','trim|callback_validateDate');
		$this->form_validation->set_rules('MaritalStatus', 'MaritalStatus','trim');
		$this->form_validation->set_rules('Address', 'Address','trim');
		$this->form_validation->set_rules('CityName', 'CityName','trim');
		$this->form_validation->set_rules('StateName', 'StateName','trim');
		$this->form_validation->set_rules('CityCode', 'CityCode','trim');
		$this->form_validation->set_rules('Course', 'Course','trim');
		$this->form_validation->set_rules('Institute', 'Institute','trim');
		$this->form_validation->set_rules('Specialization', 'Specialization','trim');
		$this->form_validation->set_rules('Year', 'Year','trim');
		$this->form_validation->set_rules('Percentage', 'Percentage','trim');
		$this->form_validation->set_rules('Applicable', 'Applicable','trim');
		$this->form_validation->set_rules('Reportto', 'Reportto','trim');
		$this->form_validation->set_rules('Designation', 'Designation','trim');
		$this->form_validation->set_rules('ReportingManager', 'ReportingManager','trim');
		$this->form_validation->set_rules('JoiningDate', 'JoiningDate','trim');
		$this->form_validation->set_rules('AppraisalDate', 'AppraisalDate','trim');
		$this->form_validation->set_rules('Employer', 'Employer','trim');
		$this->form_validation->set_rules('Industry', 'Industry','trim');
		$this->form_validation->set_rules('JobProfile', 'JobProfile','trim');
		$this->form_validation->set_rules('Experience', 'Experience','trim');
		$this->form_validation->set_rules('FacebookURL', 'FacebookURL','trim');
		$this->form_validation->set_rules('TwitterURL', 'TwitterURL','trim');
		$this->form_validation->set_rules('LinkedInURL', 'LinkedInURL','trim');
		$this->form_validation->set_rules('GoogleURL', 'GoogleURL','trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->Users_model->updateUserInfo($this->UserID, array_merge($this->Post, array("StatusID"=>@$this->StatusID, "SkipPhoneNoVerification"=>true)));
		$this->Return['Data'] = $this->Users_model->getUsers('RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,MasterFranchisee',array("UserID" => $this->UserID));
		$this->Return['Message']  =	"Successfully updated."; 	
	}



	/*
	Name: 			add
	Description: 	Use to register user to system.
	URL: 			/api_admin/users/add/
	*/
	public function add_post()
	{
		/* Validation section */
		//$this->form_validation->set_rules('Email', 'Email','trim|required|valid_email|callback_validateEmail');
		$this->form_validation->set_rules('Password', 'Password', 'trim'.(empty($this->Post['Source']) || $this->Post['Source']=='Direct' ? '|required' : ''));
		$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|required');
		$this->form_validation->set_rules('LastName', 'LastName', 'trim');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');
		$this->form_validation->set_rules('Gender', 'Gender', 'trim|required');
		//$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[3,4]');
		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|callback_validatePhoneNumber|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');	
		//$this->form_validation->set_rules('Status', 'Status', 'trim|required|callback_validateStatus');

		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim|callback_validateEntityGUID[Store,StoreID]');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$UserID = $this->Users_model->addUser($this->Post, $this->Post['UserTypeID'], $this->SourceID, $this->StatusID); 

	   /*if(!empty($this->input->Post('Email'))){	
				
				$this->Recovery_model->recovery($this->input->Post('Email'));
		  }*/
		  if (!empty($this->Post['Email'])) {
				/* Send welcome Email to User with Token. (only if source is Direct) */
				$Token = ($this->Post['Source'] == 'Direct' ? $this->Recovery_model->generateToken($UserID, 2) : '');

				//$Tokens = $this->Recovery_model->generateToken($UserID,1);

				$content = $this->load->view('emailer/signup', array(
						"Name" => @$this->Post['FirstName'],
						'Token' => $Token						
					) , TRUE);

				sendMail(array(
					'emailTo' => $this->Post['Email'],
					'emailSubject' => "Thank you for registering at " . SITE_NAME,
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));
			}
				/*referal code generate*/
			$this->Utility_model->generateReferralCode($UserID);
			/* Send welcome notification */
			$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);
			/* get user data */
			$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(
				'UserID' => $UserID
			));
			/* create session only if source is not Direct and account treated as Verified. */
			$UserData['SessionKey'] = '';

			$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(
				"IPAddress" => @$this->Post['IPAddress'],
				"SourceID" => $this->SourceID,
				"DeviceTypeID" => $this->DeviceTypeID,
				"DeviceGUID" => @$this->Post['DeviceGUID'],
				"DeviceToken" => @$this->Post['DeviceToken']
			));

		if(!$UserID){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"An error occurred, please try again later.";  
		}else{


			
			return true;
		}
	}


	
	
}