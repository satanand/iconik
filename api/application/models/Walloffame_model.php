<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Walloffame_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to add new category
	*/
	function addWallOfFame($Input=array()){

		$this->db->select('MediaID');

	    $this->db->from('tbl_media');
	    
	    $this->db->where('LOWER(`MediaCaption`)="'.strtolower($Input['MediaCaption']).'"');
	     $this->db->where('InstituteID',$InstituteID);
	     $this->db->where('SectionID','WallOfFame');
         $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{


		$WallofGUID = get_guid();


        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		    $WallofGUID = get_guid();
		    
			$originalDate = $Input['NewsDate'];

			$newDate = date("Y-m-d", strtotime($originalDate));
			$Year = date("Y", strtotime($originalDate));
			/* Add WallOfFame */
			$UpdateArray = array_filter(array(
				"MediaContent"=>@$Input['MediaContent'],
				"NewsDate"=>@$newDate,
				"Year"=>@$Year,
				"MediaCaption"=>@$Input['MediaCaption'],
				"UserID"=>@$Input['Name'],
				"InstituteID"=>@$InstituteID
			));
			if(!empty($Input['MediaGUIDe'])){

			$this->db->where('MediaGUID', $Input['MediaGUIDe']);
			
			}

			if(!empty($Input['MediaGUIDs'])){

			$this->db->where('MediaGUID', $Input['MediaGUIDs']);

			}
			$data=$this->db->update('tbl_media', $UpdateArray);

			$UpdateArrays = array_filter(array(

				"InstituteID"=>$InstituteID
				
			));

			if(!empty($Input['MediaGUIDs'])){
			   $this->db->where('EntityGUID', $Input['MediaGUIDs']);
			}
			if(!empty($Input['MediaGUIDe'])){
			   $this->db->where('EntityGUID', $Input['MediaGUIDe']);
			}

			$data=$this->db->update('tbl_entity', $UpdateArrays);
			//echo $this->db->last_query();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return TRUE;
		}
	}



	/*
	Description: 	Use to update WallOfFame.
	*/
	function editWalloffame($Input=array()){
		$originalDate = $Input['NewsDate'];
			$newDate = date("Y-m-d", strtotime($originalDate));
		$Year = date("Y", strtotime($originalDate));
		$UpdateArray = array_filter(array(

				"MediaContent"=>@$Input['MediaContent'],
				"NewsDate"=>@$newDate,
				"Year"=>@$Year,
				"MediaCaption"=>@$Input['MediaCaption'],
				"UserID"=>@$Input['Name'],
				
			));

		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('MediaGUID', $Input['MediaGUID']);
			//$this->db->limit(1);
			$data=$this->db->update('tbl_media', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		
		return TRUE;
	}


	

	/*
	Description: 	Use to  DeteleMedia.
	*/
	function delete($MediaGUID){

		if(!empty($MediaGUID)){
			$this->db->where('MediaGUID', $MediaGUID);
			$data=$this->db->delete('tbl_media');
		}
		return TRUE;
	}




	
	/*
	Description: 	Use to get Cetegories
	*/
	function getwalloffameBy($MediaGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){


		$this->db->select("*,DATE_FORMAT(NewsDate,'%d-%m-%Y') NewsDate");
		$this->db->from('tbl_media');
		if(!empty($Where['MediaGUID'])){
			$this->db->where('MediaGUID',$Where['MediaGUID']);
		}
		$this->db->where('StatusID',2);
		$this->db->order_by('MediaCaption','ASC');
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			$Userdata =	$this->Common_model->getInstituteDetailByID($Record['UserID'],'FirstName,LastName');
			$Record['UserName'] = $Userdata['FirstName']." ".$Userdata['LastName'];

			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'WallOfFame',"MediaID" => $Record['MediaID']),TRUE);
			$Record['MediaID'] = ($MediaData ? $MediaData['Data'] : new stdClass());

			//$Record['MediaID'] = ($MediaData ? $MediaData['Data'] : new stdClass());
			$Record['MediaThumbURL'] = $Record['MediaID']['Records'][0]['MediaThumbURL'];
			$Record['MediaURL'] = $Record['MediaID']['Records'][0]['MediaURL'];

			if(!$multiRecords){

					return $Record;
				}
			$Records[] = $Record;
			}
			
			return $Records;
		}
		return FALSE;		
	}

	
	/*
	Description: 	Use to get WallOfFame
	*/
	function getwalloffame($MediaGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

	

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('b.*,u.FirstName,u.LastName,ut.UserTypeName');		
		$this->db->from('tbl_media as b');
		//$this->db->join('tbl_entity e','e.EntityID=b.MediaID');
		$this->db->join('tbl_users as u','u.UserID = b.UserID');
		$this->db->join('tbl_users_type as ut','ut.UserTypeID = u.UserTypeID');

		$this->db->where('b.InstituteID',$InstituteID);
		$this->db->where('b.SectionID','WallOfFame');
		$this->db->where('b.StatusID',2);
		//echo $Where['Keyword']; die();

		if(!empty($Where['Keyword'])){

			//$Where['Keyword'] = trim($Where['Keyword']);

			$this->db->group_start();

			$this->db->like("u.FirstName", $Where['Keyword']);
			$this->db->or_like("u.LastName", $Where['Keyword']);	
			$this->db->or_like("u.Middlename", $Where['Keyword']);

			//$this->db->or_like("CONCAT_WS('',u.FirstName,u.Middlename,u.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);

			$this->db->or_like("b.MediaCaption", $Where['Keyword']);

			$this->db->or_like("b.Year", $Where['Keyword']);
			
			//$this->db->or_like("b.MediaContent", $Where['Keyword']);

			$this->db->group_end();
				
		}

		if(!empty($Where['MediaGUID'])){

			$this->db->where('b.MediaGUID',$Where['MediaGUID']);
		}

		if(!empty($Where['Year']))
		{
			$this->db->where('b.Year',$Where['Year']);
		}
		else
		{
			$this->db->where('b.Year', date("Y"));
		}

		/*if(!empty($Where['Month'])){

			$this->db->where('MONTH(NewsDate)',$Where['Month']);
		}*/

		$this->db->order_by('NewsDate','DESC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//print_r($Query);
		//echo $this->db->last_query();  die();
		
		if($Query->num_rows()>0)
		{
			$temp = array();

			$i=0;
			foreach($Query->result_array() as $Record)
			{
			 	//$Record['MediaContent'] = substr($Record['MediaContent'], 0, 400).'...<a href="JavaScript:Void(0);" onclick="loadFormView('.$i.', '.$Record["MediaGUID"].')" data-toggle="tooltip" title="View">Read more</a>';

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'WallOfFame',"MediaID" => $Record['MediaID']),TRUE);
				$Record['MediaID'] = ($MediaData ? $MediaData['Data'] : new stdClass());
				$Record['MediaThumbURL'] = $Record['MediaID']['Records'][0]['MediaThumbURL'];
				$Record['MediaURL'] = $Record['MediaID']['Records'][0]['MediaURL'];
				if(!$multiRecords){

					return $Record;
				}
				$Record['FullName'] = $Record['FirstName']." ".$Record['LastName'];
				
				
				$month = date("m", strtotime($Record['NewsDate']));

				$temp[$month][] = $Record;
				
				$i++;
			}

			$Records = $temp;

			$Return['Data']['Records'] = $Records;

			//print_r($Return); die();
			return $Return;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to get WallOfFame
	*/
	function getYears($InstituteID){

		$this->db->select('Distinct(Year)');		
		$this->db->from('tbl_media b');
		$this->db->where('b.InstituteID',$InstituteID);
		$this->db->where('b.SectionID','WallOfFame');
		$this->db->order_by('Year','DESC');
		$Query = $this->db->get();	

		//print_r($Query);
		//echo $this->db->last_query(); 
		
		if($Query->num_rows()>0){
			return $Query->result_array();
		}

		return FALSE;		
	}

}