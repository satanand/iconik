<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feecollection extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Feecollection_model');
		$this->load->model('Common_model');
	}


	/*
	Description: 	Use to get Get student fee details.
	URL: 			/api/Feecollection/getStudentFeeDetail
	Input (Sample JSON): 		
	*/
	public function getStudentFeeDetail_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[Students,UserID]');	
		$this->form_validation->validation($this); 
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//print_r($this->Post); die();

		$StudentFeeDetail = $this->Feecollection_model->getStudentFeeDetail($this->EntityID,$this->UserID);

		if(!empty($StudentFeeDetail)){
			$this->Return['Data'] = $StudentFeeDetail;
		}	
	}

	/*

	Description: 	Use to add new fee collection

	URL: 			/api/Feecollection/addCollection	

	*/

	public function addCollection_post()

	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'Student Name', 'trim|required|callback_validateEntityGUID[Students,UserID]');   	
		$this->form_validation->set_rules('PaymentMode', 'Payment Mode', 'trim|required');
		$this->form_validation->set_rules('RemainingAmount', 'Remaining Amount', 'trim');
		$this->form_validation->set_rules('TotalFee', 'Total Fee', 'trim');
		if($this->input->post('PaymentMode') == "Cash")
		{
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim|required');
			//$this->form_validation->set_rules('Notes[]', 'Denomination', 'trim|required');
		}
		else if($this->input->post('PaymentMode') == "Cheque"){
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('ChequeNumber', 'Cheque Number', 'trim|required');
			$this->form_validation->set_rules('ChequeDate', 'Cheque Date', 'trim|required');
			$this->form_validation->set_rules('BankName', 'Bank Name', 'trim|required');
			$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');	
		}
		else if($this->input->post('PaymentMode') == "Online"){
			$this->form_validation->set_rules('PaymentType', 'Payment Type', 'trim|required');
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');			
			$this->form_validation->set_rules('TransactionID', 'Transaction ID', 'trim|required');	
		}
		
		$this->form_validation->validation($this);  /* Run validation */		
		
		$duedates = $this->Post['duedates'];
		if(isset($duedates) && !empty($duedates))
		{
			foreach($duedates as $d)
			{
				if(!isset($d) || empty($d))
				{
					$this->Return['ResponseCode'] = 500;

					$this->Return['Message'] = "Please select Due date.";

					die;
				}
			}
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Due date is required.";

			die;
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

        //$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$CollectionStatus = $this->Feecollection_model->addCollection($this->EntityID,$this->UserID,$this->input->Post());
		
		if($CollectionStatus == 1){
			$this->Return['Message']  =	"New Collection Added successfully.";
		}

	}


	/*
	Description: 	Use to get Get student list.
	URL: 			/api/feecollection/getStudents
	Input (Sample JSON): 		
	*/
	public function getStudents_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('Keyword', 'Keyword');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (empty($this->EntityID) ? $this->SessionUserID : $this->EntityID);	

		$StudentData = $this->Feecollection_model->getStudents($EntityID,array("BatchID"=>@$this->BatchID,"CategoryID"=>@$this->CategoryID,"Keyword"=>@$this->input->post('Keyword')));
		if(!empty($StudentData)){
			$this->Return['Data'] = $StudentData;
		}	
	}


	/*

	Description: 	Use to add new fee collection

	URL: 			/api/Feecollection/editCollection	

	*/

	public function editCollection_post()

	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserGUID', 'Student Name', 'trim|required|callback_validateEntityGUID[Students,UserID]');	
		$this->form_validation->set_rules('FeeCollectionID', 'Fee collection ID','trim|required');
		$this->form_validation->set_rules('PaymentMode', 'Payment Mode', 'trim|required');
		if($this->input->post('PaymentMode') == "Cash"){
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('PaymentDate', 'Payment Date', 'trim|required');
			//$this->form_validation->set_rules('Determination', 'Determination', 'trim|required');
		}
		else if($this->input->post('PaymentMode') == "Cheque"){
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');
			$this->form_validation->set_rules('ChequeNumber', 'Cheque Number', 'trim|required');
			$this->form_validation->set_rules('ChequeDate', 'Cheque Date', 'trim|required');
			$this->form_validation->set_rules('BankName', 'Bank Name', 'trim|required');
			$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');	
		}
		else if($this->input->post('PaymentMode') == "Online"){
			$this->form_validation->set_rules('PaymentType', 'Payment Type', 'trim|required');
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required');			
			$this->form_validation->set_rules('TransactionID', 'Transaction ID', 'trim|required');	
		}
		
		$this->form_validation->validation($this);  /* Run validation */		

		$duedates = $this->Post['duedates'];
		if(isset($duedates) && !empty($duedates))
		{
			foreach($duedates as $d)
			{
				if(!isset($d) || empty($d))
				{
					$this->Return['ResponseCode'] = 500;

					$this->Return['Message'] = "Please select Due date.";

					die;
				}
			}
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Due date is required.";

			die;
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

        //$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$CollectionStatus = $this->Feecollection_model->editCollection($this->EntityID,$this->UserID,$this->input->Post());

		if($CollectionStatus == 1){
			$this->Return['Message']  =	"New Collection Added successfully.";
		}

	}




	/*
	Name: 			getFeeCollections
	Description: 	Use to get list of collection.
	URL: 			/api/FeeCollections/getFeeCollections
	*/
	public function getFeeCollections_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FeeCollectionID', 'Fee collection ID','trim');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID','trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('startDate', 'startDate', 'trim');
		$this->form_validation->set_rules('endDate', 'endDate', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID :$this->SessionUserID);
		
		$FeeCategoryData = $this->Feecollection_model->getFeeCollections($this->EntityID,
			array(
				"CategoryID"=>$this->CategoryID, 
				"BatchID"=>$this->BatchID, 
				"FeeCollectionID"=>$this->input->post("FeeCollectionID"),
				"startDate"=>$this->input->post("startDate"),
				"endDate"=>$this->input->post("endDate"),
				"Keyword"=>$this->input->post("Keyword")
			), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);	
		
		if(!empty($FeeCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $FeeCategoryData;
		}	
	}



	/*
	Name: 			deleteCollections
	Description: 	Use to delete list of collection.
	URL: 			/api/FeeCollections/delete
	*/
	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('FeeCollectionID', 'Fee collection ID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		//echo $this->input->post('FeecollectionID');
		$data=$this->Feecollection_model->delete($this->input->post('FeeCollectionID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
		
	}




	/*

	Name: 			editRoles_post
	

	*/
	public function editDepartment_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('DepartmentName', 'DepartmentName', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->set_rules('DepartmentID', 'DepartmentID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Department_model->editDepartment($this->EntityID, $this->input->post('DepartmentID'), $this->input->Post());
	
		$this->Return['Data'] = $this->Department_model->getDepartment($this->EntityID, array("DepartmentID"=>$this->input->post('DepartmentID')));

	    $this->Return['Message']  =	"Updated successfully."; 

	}


	public function deleteDepartment_post()
	{
		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('DepartmentID', 'DepartmentID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
	
		$this->Return['Data']= $this->Department_model->deleteDepartment($this->EntityID, array("DepartmentID"=>$this->input->post('DepartmentID'),"StatusID"=>$this->input->post('StatusID')));

	    $this->Return['Message']      	=	"Deleted successfully."; 

	}
	
}