<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('Cronjob_model');
	}

	
	public function triggerSMS()
	{
		$Response = array();

		$chk = $this->Cronjob_model->triggerSMS();      	
		
		if($chk)
		{
			$Response['ResponseCode'] 	=	500;

			$Response['Message']      	=	"Error";
		}		
		else
		{
			$Response['ResponseCode'] 	=	200;

			$Response['Message']      	=	"Sent Successfully.";
		}

		return $Response;
	}


	public function triggerEmail()
	{
		$Response = array();

		$chk = $this->Cronjob_model->triggerEmail();      	
		
		if($chk)
		{
			$Response['ResponseCode'] 	=	500;

			$Response['Message']      	=	"Error";
		}		
		else
		{
			$Response['ResponseCode'] 	=	200;

			$Response['Message']      	=	"Sent Successfully.";
		}

		return $Response;
	}
	


	public function partTimeJobs()
	{
		$Response = array();

		$chk = $this->Cronjob_model->partTimeJobs();      	
		
		if($chk)
		{
			$Response['ResponseCode'] 	=	500;

			$Response['Message']      	=	"Error";
		}		
		else
		{
			$Response['ResponseCode'] 	=	200;

			$Response['Message']      	=	"Moved Successfully.";
		}

		return $Response;
	}



	public function beforeDueDate()
	{
		$Response = array();

		$chk = $this->Cronjob_model->beforeDueDate();      	
		
		if($chk)
		{
			$Response['ResponseCode'] 	=	500;

			$Response['Message']      	=	"Error";

			$Response['Data'] 	=	array();
		}		
		else
		{
			$Response['ResponseCode'] 	=	200;

			$Response['Message']      	=	"Moved Successfully.";

			$Response['Data'] 	=	$chk;
		}

		return $Response;
	}
	


	public function afterDueDate()
	{
		$Response = array();

		$chk = $this->Cronjob_model->afterDueDate();      	
		
		if($chk)
		{
			$Response['ResponseCode'] 	=	500;

			$Response['Message']      	=	"Error";
		}		
		else
		{
			$Response['ResponseCode'] 	=	200;

			$Response['Message']      	=	"Moved Successfully.";
		}

		return $Response;
	}


	public function scholarshipTestEdited()
	{
		$Response = array();

		$chk = $this->Cronjob_model->scholarshipTestEdited();      	
		
		if($chk)
		{
			$Response['ResponseCode'] 	=	500;

			$Response['Message']      	=	"Error";
		}		
		else
		{
			$Response['ResponseCode'] 	=	200;

			$Response['Message']      	=	"Saved Successfully.";
		}

		return $Response;
	}
}