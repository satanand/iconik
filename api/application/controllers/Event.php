<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Event_model');
		$this->load->model('Walloffame_model');
		$this->load->model('Utility_model');
		$this->load->model('Entity_model');
	}


	/*get Event*/
	public function getEvent_post()
	{
		$this->form_validation->set_rules('Category', 'Category','trim');
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
		$EventData = $this->Event_model->getEvent('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if(!empty($EventData)){
			$this->Return['Data'] = $EventData['Data'];
		}	
	}




	/*

	Description: 	Use to add new book

	URL: 			/api_admin/book/add	

	*/

	public function add_post()
	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EventName', 'Event Name', 'trim|required');
		 //$this->form_validation->set_rules('CategoryID', 'Category', 'trim|required');
		 $this->form_validation->set_rules('EventStartDateTime', ' Start Date', 'trim|required|callback_validateDate');
		 $this->form_validation->set_rules('EventEndDateTime', 'End Date', 'trim|required|callback_validateDate');

		 $this->form_validation->set_rules('EventDescription', 'Event Description', 'trim');
		 $this->form_validation->set_rules('Address', 'Venue', 'trim|required');
		 $this->form_validation->set_rules('MediaGUID', 'MediaGUID','trim');
		 //$this->form_validation->set_rules('MediaGUIDs', 'MediaGUIDs', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		if(strtotime($this->input->post('EventStartDateTime')) == strtotime($this->input->post('EventEndDateTime'))){
			//return array("exist","Event start  and end time can not be same.");
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Event start and end time can not be same.";
		}else if(strtotime($this->input->post('EventStartDateTime')) > strtotime($this->input->post('EventEndDateTime'))){
			//return array("exist","End time should be ahead of start time.");
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Event end time should be ahead of start time.";
		}/*else if(strtotime($this->input->post('EventStartDateTime')) < strtotime(date("Y-m-d H:i:s"))){
		
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Event start time should be ahead of current time.";
		}*/else{
			$EventData = $this->Event_model->addEvent($this->Post);

			//echo $EventData;

			if($EventData == 1){

				$this->Return['Message']      	=	"New Event Add Successfully."; 

			} else {
                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Event Title Name Already Exist.";
				
			}
		}

	}

	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function addcategory_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Category', 'Category', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		
		$EventData = $this->Event_model->addcategory($this->Post);

		//echo $EventData;

		if($EventData == 1){

			$this->Return['Message']      	=	"Add New Activity successfully."; 

		} else {

            $this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"This Event Activity Name Already Exist.";
			
		}


	}



	public function getCategory_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CategoryData = $this->Event_model->getCategory('',array());
		if(!empty($CategoryData)){
			$this->Return['Data'] = $CategoryData['Data'];
		}	
	}

	/*
	Description: 	Use to get Get single .
	URL: 			/api/getEventByID/getEventByID
	Input (Sample JSON): 		
	*/
	public function getEventByID_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$EventData = $this->Event_model->getEventByID('',array("EventGUID"=>$this->input->post('EventGUID')));
		if(!empty($EventData)){
			$this->Return['Data'] = $EventData;
		}	
	}
	

	/*getReturnEvent*/

	public function getReturnEvent_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$EventData = $this->Event_model->getReturnEvent('',array("EventGUID"=>$this->input->post('EventGUID')));
		if(!empty($EventData)){
			$this->Return['Data'] = $EventData;
		}	
	}

	/*

	Name: 			editEvent_post
	

	*/
	public function editEvent_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EventName', 'Event Name', 'trim|required');
		 //$this->form_validation->set_rules('CategoryID', 'CategoryID', 'trim|required');
		  $this->form_validation->set_rules('Address', 'Venue', 'trim|required');
		 $this->form_validation->set_rules('EventStartDateTime', ' Start Date', 'trim|required|callback_validateDate');
		 $this->form_validation->set_rules('EventEndDateTime', 'End Date', 'trim|required|callback_validateDate');
		 $this->form_validation->set_rules('EventDescription', 'Event Description', 'trim');
		 $this->form_validation->set_rules('MediaGUID', 'MediaGUID','trim');
		 $this->form_validation->set_rules('MediaID', 'MediaID', 'trim');


		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|callback_validateEntityGUID[Event,EventID]');

		$this->form_validation->validation($this);  /* Run validation */	

		//echo $this->input->Post('EventGUID'); die();
		/* Validation - ends */

		if(strtotime($this->input->post('EventStartDateTime')) == strtotime($this->input->post('EventEndDateTime'))){
			//return array("exist","Event start  and end time can not be same.");
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Event start and end time can not be same.";
		}else if(strtotime($this->input->post('EventStartDateTime')) > strtotime($this->input->post('EventEndDateTime'))){
			//return array("exist","End time should be ahead of start time.");
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Event end time should be ahead of start time.";
		}/*else if(strtotime($this->input->post('EventStartDateTime')) < strtotime(date("Y-m-d H:i:s"))){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Event start time should be ahead of current time.";
		}*/else{


				if(!empty($this->Post['MediaGUID'])){

					$this->Event_model->DeteleMedia($this->input->Post('EventGUID'));
			     }
			     
				$this->Event_model->editEvent($this->Post);
			
				$this->Return['Data']= $this->Event_model->getEvent('',array("EventGUID"=>$this->input->Post('EventGUID')));

				
			
				
			    $this->Return['Message']      	=	"Event updated successfully."; 
			}

	}

		


	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getContentCategories_post()
	{
		$ContentCategoryData = $this->Category_model->getContentCategories();
		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}


	public function addParticipants_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EventActivityID', 'EventActivityID', 'trim|required');
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|required|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//print_r($this->input->post()); die;

		$data = $this->Event_model->addParticipants($this->EntityID,$this->EventID,$this->input->post('EventActivityID'));
		//print_r($data); die;
		if($data == "added"){
			$this->Return['Data'] = $data;
			$this->Return['Message']      	=	"Added Successfully."; 			
		}else{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You have already applied for the same Activity."; 
		}
	}
	/*delete*/

	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|required|callback_validateEntityGUID[Event, EventID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$data=$this->Event_model->delete($this->input->Post('EventGUID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
	}


	public function getStaffList_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$data=$this->Event_model->getStaffList($this->EntityID);

		if($data){
			$this->Return['Data']      	=	$data; 
		}
	}

	


	public function ManageActivity_post()
	{
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|required|callback_validateEntityGUID[Event,EventID]');

		$this->form_validation->set_rules('ActivityName', 'ActivityName', 'trim|required');
		$this->form_validation->set_rules('Accountaibility', 'Accountaibility', 'trim|required');
		$this->form_validation->set_rules('Coordinator', 'Coordinator', 'trim|required');
		$this->form_validation->set_rules('hour', 'Hour', 'trim|required');
		$this->form_validation->set_rules('minute', 'Minute', 'trim|required');
		$this->form_validation->set_rules('Task[]', 'Task', 'trim|required');
		$this->form_validation->set_rules('ResponsiblePerson[]', 'ResponsiblePerson', 'trim|required');
		$this->form_validation->set_rules('CompletionDate[]', 'CompletionDate', 'trim|required');
		$this->form_validation->validation($this);

		$data = $this->Event_model->ManageActivity($this->EventID, $this->input->Post());

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured."; 

		}else{

			$this->Return['Message']      	=	"Added Successfully"; 
		}
	}



	public function EditActivity_post()
	{
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|required|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->set_rules('EventActivityID', 'EventActivityID', 'trim|required');
		$this->form_validation->set_rules('ActivityName', 'ActivityName', 'trim|required');
		$this->form_validation->set_rules('Accountaibility', 'Accountaibility', 'trim|required');
		$this->form_validation->set_rules('Coordinator', 'Coordinator', 'trim|required');
		$this->form_validation->set_rules('hour', 'Hour', 'trim|required');
		$this->form_validation->set_rules('minute', 'Minute', 'trim|required');
		$this->form_validation->set_rules('Task[]', 'Task', 'trim|required');
		$this->form_validation->set_rules('ResponsiblePerson[]', 'ResponsiblePerson', 'trim|required');
		$this->form_validation->set_rules('CompletionDate[]', 'CompletionDate', 'trim|required');
		$this->form_validation->validation($this);

		$data = $this->Event_model->EditActivity($this->EventID, $this->input->Post());

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured."; 

		}else{

			$this->Return['Message']      	=	"Updated Successfully"; 
		}
	}


	public function getEventActivity_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$EventData = $this->Event_model->getEventActivity($this->EntityID, array("EventID"=>$this->EventID));

		if(!empty($EventData)){
			$this->Return['Data'] = $EventData['Data'];
		}else{
			$this->Return['Data'] = [];
		}	
	}


	public function getEventParticipants_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->set_rules('EventActivityID', 'EventActivityID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$EventData = $this->Event_model->getEventParticipants($this->EntityID, array("EventID"=>$this->EventID,"EventActivityID"=>$this->input->post('EventActivityID')));

		if(!$EventData && $EventData == false){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Data'] = [];
		}else{
			$this->Return['Data'] = $EventData['Data'];
		}
	}


	public function ActivityDelete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->set_rules('EventActivityID', 'EventActivityID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$EventData = $this->Event_model->ActivityDelete($this->EntityID, array("EventID"=>$this->EventID,"EventActivityID"=>$this->input->post('EventActivityID')));	
	}

	public function SetParticipantsPermission_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');
		$this->form_validation->set_rules('EventActivityID', 'EventActivityID', 'trim|required');
		$this->form_validation->set_rules('Permission', 'Permission', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$EventData = $this->Event_model->SetParticipantsPermission($this->EntityID, $this->input->post());

		if($EventData){
			$this->Return['Message']      	=	"Updated Successfully"; 
		}	
	}


	/*
	Description: 	Use to get Get single event details.
	URL: 			/api/Event/getEventDetails
	Input (Sample JSON): 		
	*/
	public function getEventDetails_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EventGUID', 'EventGUID', 'trim|callback_validateEntityGUID[Event,EventID]');
		$this->form_validation->set_rules('Type', 'Type','trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if(!empty($this->input->post('Type'))){
			$EventParticipantsData = $this->Event_model->getEventParticipants($this->EntityID, array("EventID"=>$this->EventID,"UserID" => $this->EntityID));
		}else{
			$EventParticipantsData = $this->Event_model->getEventParticipants($this->EntityID, array("EventID"=>$this->EventID));
		}	

		if(!empty($this->input->post('Type'))){
			$EventActivityData = $this->Event_model->getEventActivity($this->EntityID, array("EventID"=>$this->EventID,"UserID" => $this->EntityID));
		}else{
			$EventActivityData = $this->Event_model->getEventActivity($this->EntityID, array("EventID"=>$this->EventID)); 

		}		

		//$EventActivityData = $this->Event_model->getEventActivity($this->EntityID, array("EventID"=>$this->EventID)); 

		$EventDetails = $this->Event_model->getEventByID($this->EntityID,array("EventID"=>$this->EventID));

		if(!empty($EventDetails)){
			$data['EventDetails'] = $EventDetails;
		}else{
			$data['EventDetails'] = [];
		}
		

		if(!empty($EventActivityData)){
			$data['EventActivities'] = $EventActivityData['Data'];
		}else{
			$data['EventActivities'] = [];
		}

		if(!empty($EventParticipantsData)){
			$data['EventParticipants'] = $EventParticipantsData['Data'];
		}else{
			$data['EventParticipants'] = [];
		}
		
		if(!empty($data)){
			$this->Return['Data'] = $data;
		}	
	}

}