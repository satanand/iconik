<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends API_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Store_model');
	}


	/*
	Description: Use to locate order.
	URL: /api/store/trackOrder
	*/

	public function trackOrder_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim|required|callback_validateOrderGUID');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$OrderData=$this->Store_model->trackOrders(array('UserID'=>$this->SessionUserID, 'OrderID'=>$this->OrderID));
		if($OrderData){
			$this->Return['Data'] = $OrderData;
		}
	}


	/*
	Name: booking
	Description: Use for table booking in the restaurant.
	URL: /api/store/booking
	*/

	public function booking_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('BookingOn', 'BookingOn', 'trim|required|callback_validateDate');
		$this->form_validation->set_rules('NumberOfPeople', 'NumberOfPeople', 'trim|numeric|required');
		$this->form_validation->validation($this);  /* Run validation */
		/* Validation - ends */

		$BookingID = $this->Store_model->booking($this->Post, $this->SessionUserID, @$this->StoreID);
		if(!$BookingID){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You have already booked this slot.";  
		}else{
			$this->Return['Message']      	=	"Your request is submitted, we will get back to you shortly.";  
		}

	}

	/*
	Name: 			getBooking
	Description: 	Use to get Get list of bookings.
	URL: 			/api/store/getBooking	
	*/
	public function getBookings_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$BookingsData = $this->Store_model->getBookings('
			BookingOn,
			NumberOfPeople,
			EntryDate
			',
			array("UserID"=>$this->SessionUserID),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($BookingsData){
			$this->Return['Data'] = $BookingsData['Data'];
		}	
	}


	/*
	Name: 			getCoupons
	Description: 	Use to get Get list of Coupons.
	URL: 			/api/store/getCoupons	
	*/
	public function getCoupons_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CouponData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			E.EntryDate,
			E.StatusID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate
			',
			array("StoreID"=>@$this->StoreID, "StatusID"=>2),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($CouponData){
			$this->Return['Data'] = $CouponData['Data'];
		}	
	}



	/*
	Name: 			getCoupons
	Description: 	Use to get Get list of Coupons.
	URL: 			/api/store/getCoupons	
	*/
	public function getCoupon_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('CouponGUID', 'CouponGUID','trim|required|callback_validateEntityGUID[Coupon,CouponID]');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CouponData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			E.EntryDate,
			E.StatusID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate
			',
			array("CouponID"=>$this->CouponID));
		if($CouponData){
			$this->Return['Data'] = $CouponData;
		}	
	}



	/*
	Name: 			validateCoupon
	Description: 	Use to get detail of Coupon.
	URL: 			/api/store/validateCoupon	
	*/
	public function validateCoupon_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('CouponCode', 'CouponCode', 'trim'.(empty($this->Post['CouponGUID']) ? '|required' : '').'|callback_validateCoupon');
		$this->form_validation->set_rules('CouponGUID', 'CouponGUID', 'trim|callback_validateEntityGUID[Coupon,CouponID]');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CouponsData = $this->Store_model->getCoupons('
			E.EntityGUID AS CouponGUID,
			C.CouponTitle,
			C.CouponDescription,		
			C.CouponCode,
			C.CouponType,
			C.CouponValue,
			C.CouponValueLimit,
			C.CouponValidTillDate
			',
			array("CouponID" => $this->CouponID)
		);
		if($CouponsData){
			$this->Return['Data'] = $CouponsData;
		}	
	}


	/*
	Name: 			getStores
	Description: 	Use to get Get list of Stores.
	URL: 			/api/store/getStores	
	*/
	public function getStores_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Veg,NonVeg]');
		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');
		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$ProductsData = $this->Store_model->getStores('
			E.EntityGUID AS StoreGUID,
			E.Rating,
			ST.StoreName,
			ST.Location,		
			ST.IsVeg,
			ST.EstimatedDeliTime,
			ST.MinOrderPrice,
			(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=ST.StoreID)) AS CategoryNames',
			array(
				"Filter"	=>	@$this->Post['Filter'],
				'Keyword'	=>	@$this->Post['Keyword'],
				'Latitude'	=>	@$this->Post['Latitude'],
				'Longitude'	=>	@$this->Post['Longitude']
			),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($ProductsData){
			$this->Return['Data'] = $ProductsData['Data'];
		}	
	}

	/*
	Description: 	Use to get Get single Stores.
	URL: 			/api/store/getStore
	*/
	public function getStore_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID', 'trim|required|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$StoreData = $this->Store_model->getStores('
			E.EntityGUID AS StoreGUID,
			ESO.EntityGUID StoreOwnerGUID,
			E.Rating,
			ST.StoreName,
			ST.Location,		
			ST.IsVeg,
			ST.EstimatedDeliTime,
			ST.MinOrderPrice,
			(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=ST.StoreID)) AS CategoryNames',
			array("StoreID"=>$this->StoreID));
		if($StoreData){
			$this->Return['Data'] = $StoreData;
		}	
	}




	/*
	Description: 	Use to add new product.
	URL: 			/api/store/addProduct/	
	*/
	public function addProduct_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('ProductSKU', 'Product SKU', 'trim');
		$this->form_validation->set_rules('ProductName', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('ProductRegPrice', 'ProductRegPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('ProductBuyPrice', 'ProductBuyPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('CurrencyType', 'CurrencyType', 'trim');
		$this->form_validation->set_rules('ProductDesc', 'ProductDesc', 'trim');
		$this->form_validation->set_rules('ProductShortDesc', 'ProductShortDesc', 'trim');
		$this->form_validation->set_rules('Tags', 'Tags', 'trim');
		$this->form_validation->set_rules('ProductInStock', 'ProductInStock', 'trim|integer');
		$this->form_validation->set_rules('ProductBackorders', 'ProductBackorders', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('SellVia', 'SellVia', 'trim');
		$this->form_validation->set_rules('Negotiable', 'Negotiable', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('Location', 'Location', 'trim');
		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');
		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');
		$this->form_validation->set_rules('IsVeg', 'IsVeg', 'trim|in_list[Yes,No]');

		$this->form_validation->set_rules('ListingType', 'ListingType', 'trim|in_list[Free,Featured,Urgent,Highlight]');

		$this->form_validation->set_rules('ProductPrepTime', 'ProductPrepTime', 'trim');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$ProductData = $this->Store_model->addProduct($this->SessionUserID, array_merge($this->Post, array("StoreID"=>@$this->StoreID)),2);
		if($ProductData){
			$this->Return['Data']['ProductGUID'] = $ProductData['ProductGUID'];
			$this->Return['Message']      	=	"Product added successfully."; 
		}


	}




	/*
	Description: 	Use to update product info.
	URL: 			/api/store/editProduct/	
	*/
	public function editProduct_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('ProductGUID', 'ProductGUID', 'trim|required|callback_validateEntityGUID[Product,ProductID]');
		$this->form_validation->set_rules('ProductSKU', 'Product SKU', 'trim');
		$this->form_validation->set_rules('ProductName', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('ProductRegPrice', 'ProductRegPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('ProductBuyPrice', 'ProductBuyPrice', 'trim|required|numeric');
		$this->form_validation->set_rules('CurrencyType', 'CurrencyType', 'trim');
		$this->form_validation->set_rules('ProductDesc', 'ProductDesc', 'trim');
		$this->form_validation->set_rules('ProductShortDesc', 'ProductShortDesc', 'trim');
		$this->form_validation->set_rules('Tags', 'Tags', 'trim');
		$this->form_validation->set_rules('ProductInStock', 'ProductInStock', 'trim|integer');
		$this->form_validation->set_rules('ProductBackorders', 'ProductBackorders', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('SellVia', 'SellVia', 'trim');
		$this->form_validation->set_rules('Negotiable', 'Negotiable', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('Location', 'Location', 'trim');
		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');
		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');
		$this->form_validation->set_rules('IsVeg', 'IsVeg', 'trim|in_list[Yes,No]');
		$this->form_validation->set_rules('ProductPrepTime', 'ProductPrepTime', 'trim');
		$this->form_validation->set_rules('Status', 'Status', 'trim|callback_validateStatus');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$this->Store_model->editProduct($this->ProductID, array_merge($this->Post,array("StatusID"=>@$this->StatusID)), $this->SessionUserID);
		
		$ProductData = $this->Store_model->getProducts('
			E.EntityGUID ProductGUID,
			E.Rating,
			P.ProductSKU,
			P.ProductName,
			P.ProductRegPrice,		
			P.ProductBuyPrice,
			P.PercentageOff,
			P.CurrencyType,
			P.ProductShortDesc ProductDesc,
			P.Tags,
			P.ProductInStock,
			P.ProductBackorders,
			P.SellVia,
			P.Negotiable,
			E.Location,
			P.IsVeg,
			P.ProductPrepTime',
			array("ProductID"=>$this->ProductID));
		$this->Return['Data'] = $ProductData;
		$this->Return['Message']      	=	"Product updated successfully."; 
	}



	/*
	Name: 			getCategoryProducts
	Description: 	Use to get Get product List under the Category.
	URL: 			/api/store/getCategoryProducts
	Input (Sample JSON): 		
	*/
	public function getCategoryProducts_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('CategoryTypeName', 'CategoryTypeName', 'trim|required|callback_validateCategoryTypeName');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$this->load->model('Category_model');
		$CategoryData = $this->Category_model->getCategoryProducts('
			C.CategoryID,
			E.EntityGUID AS CategoryGUID,
			C.CategoryName',
			array("CategoryTypeID"=>$this->CategoryTypeID, "StoreID"=>@$this->StoreID)
		);
		if(!empty($CategoryData)){
			$this->Return['Data']['Records'] = $CategoryData;
		}	
	}




	/*
	Name: 			getProducts
	Description: 	Use to get Get list of products.
	URL: 			/api/store/getProducts	
	*/
	public function getProducts_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');

		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Veg,NonVeg,Free,Featured,Urgent,Highlight]');

		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');
		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');
		$this->form_validation->set_rules('Radius', 'Radius', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		$Query = '
		CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
		IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,
		U.Username,
		E.EntryDate,	
		EU.EntityGUID AS UserGUID,			
		E.EntityGUID AS ProductGUID,
		E.Rating,
		E.LikedCount,
		E.ViewCount,
		P.ProductSKU,
		P.ProductName,
		P.ProductRegPrice,		
		P.ProductBuyPrice,
		P.PercentageOff,
		P.CurrencyType,	
		P.ProductDesc,
		P.Tags,		
		P.ProductInStock,
		P.ProductBackorders,
		P.SellVia,
		P.Negotiable,
		E.Latitude,
		E.Longitude,
		P.IsVeg,
		P.ListingType,
		P.ProductPrepTime,
		(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=2 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryNames,
		"No" as IsLiked,
		';
		if(!empty($this->SessionUserID)){
			$Query.= '
			/* Check self liked or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.ProductID AND Action="Liked"), "Yes", "No") AS IsLiked,
			';
		}

		$ProductsData = $this->Store_model->getProducts($Query,
			array(
				"CategoryID"	=>	@$this->CategoryID,
				'Keyword'		=>	@$this->Post['Keyword'],
				"Filter"		=>	@$this->Post['Filter'],
				'Latitude'		=>	@$this->Post['Latitude'],
				'Longitude'		=>	@$this->Post['Longitude'],
				'StoreID'		=>	@$this->StoreID,
				"StatusID"		=>	2
			),
			TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']
		);
		if($ProductsData){
			$this->Return['Data'] = $ProductsData['Data'];
		}	
	}

	/*
	Name: 			getProduct
	Description: 	Use to get Get single product info.
	URL: 			/api/store/getProduct	
	*/
	public function getProduct_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('ProductGUID', 'ProductGUID', 'trim|required|callback_validateEntityGUID[Product,ProductID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Query = '
		CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
		U.Username,
		IF(U.ProfilePic IS NULL,CONCAT("'.PROFILE_PICTURE_URL.'","default.jpg"),CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,
		U.Username,	
		EU.EntityGUID AS UserGUID,	
		EU.Rating UserRating,	
		EU.EntryDate UserEntryDate,
		(SELECT COUNT(1) FROM  social_subscribers WHERE social_subscribers.ToEntityID =U.UserID and Action="Follow") FollowersCount,
		(SELECT COUNT(1) FROM  social_subscribers WHERE social_subscribers.UserID =U.UserID and Action="Follow") FollowingCount,

		(SELECT COUNT(P.`ProductID`) FROM `ecom_products` P, `tbl_entity` E	WHERE P.`ProductID`=E.`EntityID`
		AND E.`CreatedByUserID`= U.UserID AND E.`StatusID`=2) SellerListedProductsCount,

		0 SellerSoldProductsCount,

		E.EntryDate,
		E.EntityGUID AS ProductGUID,
		E.Rating,
		E.LikedCount,
		E.ViewCount,
		P.ProductSKU,
		P.ProductName,
		P.ProductRegPrice,		
		P.ProductBuyPrice,
		P.PercentageOff,
		P.CurrencyType,	
		P.ProductDesc,
		P.Tags,		
		P.ProductInStock,
		P.ProductBackorders,
		P.SellVia,
		P.Negotiable,
		P.IsVeg,
		P.ListingType,
		P.ProductPrepTime,
		(SELECT GROUP_CONCAT(CategoryName SEPARATOR ", ") FROM set_categories WHERE set_categories.CategoryTypeID=2 AND CategoryID IN(SELECT EC.CategoryID FROM `tbl_entity_categories` EC  WHERE EC.EntityID=P.ProductID)) AS CategoryNames,
		"No" as IsLiked,
		';
		if(!empty($this->SessionUserID)){
			$Query.= '
			IF(E.CreatedByUserID='.$this->SessionUserID.',"Yes", "No") IsOwner,

			/* Check self liked or not */
			IF(EXISTS(SELECT 1 FROM tbl_action WHERE EntityID='.$this->SessionUserID.' AND ToEntityID=P.ProductID AND Action="Liked"), "Yes", "No") AS IsLiked,
			';
		}

		$ProductData = $this->Store_model->getProducts($Query,
			array("ProductID"=>$this->ProductID)
		);
		if($ProductData){
			if(!empty($this->SessionUserID)){
				$this->Entity_model->addViewCount($this->SessionUserID, $this->ProductID);
			}
			$this->Return['Data'] = $ProductData;
		}	
	}



	/*
	Name: placeOrder
	Description: Use to add new order to system.
	URL: /api/store/placeOrder
	*/

	public function placeOrder_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StoreGUID', 'StoreGUID','trim'.(MULTISTORE ? '|required' : '').'|callback_validateEntityGUID[Store,StoreID]');

		$this->form_validation->set_rules('Recipient[FirstName]', 'FirstName', 'trim|required');
		$this->form_validation->set_rules('Recipient[LastName]', 'LastName', 'trim');


		$this->form_validation->set_rules('FromDateTime', 'FromDateTime', 'trim|callback_validateDate');
		$this->form_validation->set_rules('ToDateTime', 'ToDateTime', 'trim|callback_validateDate');
		$this->form_validation->set_rules('Note', 'Note', 'trim');

		$this->form_validation->set_rules('DeliveryPlace[Address]', 'Address', 'trim|required');
		$this->form_validation->set_rules('DeliveryPlace[Address1]', 'Address1', 'trim');
		$this->form_validation->set_rules('DeliveryPlace[City]', 'City', 'trim');
		$this->form_validation->set_rules('DeliveryPlace[PhoneNumber]', 'Mobile number', 'trim|required');

		$this->form_validation->set_rules('Products[]', 'Products', 'trim|required');

		$this->form_validation->set_rules('DeliveryType', 'DeliveryType', 'trim|required|in_list[Delivery,Pickup]');
		$this->form_validation->set_rules('PaymentMode', 'PaymentMode', 'trim|required|in_list[Online,Wallet,COD]');
		$this->form_validation->set_rules('PaymentGateway', 'PaymentGateway', 'trim|in_list[PayPal,Paytm]');
		$this->form_validation->set_rules('TransactionID', 'TransactionID', 'trim');

		$this->form_validation->set_rules('RewardPoints', 'RewardPoints', 'trim|integer|callback_validateRewardPoints');
		$this->form_validation->set_rules('CouponCode', 'CouponCode', 'trim|callback_validateCoupon');

		if(!empty($this->Post['Products']) && is_array($this->Post['Products'])){
			foreach($this->Post['Products'] as $Key=>$Value){
				$this->form_validation->set_rules('Products['.$Key.'][ProductGUID]', 'ProductGUID', 'trim|required|callback_validateEntityGUID[Product,ProductID]');
				$this->form_validation->set_rules('Products['.$Key.'][Quantity]', 'Quantity', 'trim|numeric');

				/*Get product details to check in stock - start*/
				$ProductData = $this->Store_model->getProducts('P.ProductName, P.ProductInStock, P.ProductBackorders',array("ProductGUID"=>$Value['ProductGUID']));
				if(($ProductData['ProductInStock']==0 || $Value['Quantity']>$ProductData['ProductInStock']) && $ProductData['ProductBackorders']=='No'){
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message']      	=	$ProductData['ProductName']." Out-of-Stock.";
					exit;
				}
				/*Get product details to check in stock - ends*/

			}	
		}

		$this->form_validation->validation($this);  /* Run validation */
		/* Validation - ends */

		$Order = $this->Store_model->addOrder($this->Post, $this->SessionUserID, @$this->StoreID, @$this->CouponID);
		if(!$Order){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"An error occurred, please try again later.";  
		}
		else{

			/* notification to store owner - starts */
			if(!empty($this->StoreID)){
				$StoreData = $this->Store_model->getStores('ST.UserID',array("StoreID"=>$this->StoreID));
				$Message = "A new order ".$Order['OrderID']." has been placed - ".SITE_NAME;
				/*Push*/
				$this->Notification_model->addNotification('placeed_order', $Message, $StoreData['UserID'], $StoreData['UserID'], $Order['OrderGUID']);

				/*SMS*/
				$UserData=$this->Users_model->getUsers('U.PhoneNumber',array('UserID'=>$StoreData['UserID']));
				if(!empty($UserData['PhoneNumber'])){
					sendSMS(array(
						'PhoneNumber' 	=> $UserData['PhoneNumber'],			
						'Text'			=> $Message
					));
				}

			}
			/* notification to store owner - ends */

			if($this->Post['PaymentMode']=='COD'){
				/*send order confirmation to customer*/
				$this->Store_model->orderConfirmation($this->SessionUserID, $Order['OrderGUID']);	
			}

			$this->Return['Message'] = "Order placed successfully. Placed order cannot be canceled.";
			$this->Return['Data'] = array('OrderGUID' => $Order['OrderGUID']);
		}
	}



	/*
	Name: confirmOrder
	Description: Use to change order status as confirmed.
	URL: /api/store/confirmOrder
	*/

	public function confirmOrder_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim|required|callback_validateOrderGUID');
		$this->form_validation->set_rules('TransactionID', 'TransactionID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */
		/* Validation - ends */

		/* change order status to Confirmed */
		$updateStaus = $this->Store_model->updateOrder($this->OrderID, array_merge($this->Post,array("StatusID"=>2)));
		if(!$updateStaus){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"An error occurred, please try again later.";  
		}
		else{
			/*send order confirmation to customer*/
			$this->Store_model->orderConfirmation($this->SessionUserID, $this->Post['OrderGUID']);
			$this->Return['Message'] = "Order placed successfully.";
		}
	}



	/*
	Name: confirmDelivery
	Description: Use to change order status as delivered.
	URL: /api/store/confirmOrder
	*/

	public function confirmDelivery_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim|callback_validateOrderGUID');
		$this->form_validation->validation($this);  /* Run validation */
		/* Validation - ends */
		/* get order details - starts */
		$OrderData=$this->Store_model->getOrders('O.UserID, CONCAT_WS(" ",O.FirstName,O.LastName) Name, O.Address, O.Address1, O.City, O.Postal, O.PhoneNumber',array('OrderID'=>$this->OrderID));


		/* change order status to Delivered */
		$updateStaus = $this->Store_model->updateOrder($this->OrderID, array("StatusID"=>5));
		if(!$updateStaus){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"An error occurred, please try again later.";  
		}
		else{
			/* change order assigned status */
			$this->Store_model->updateOrderAssigned($this->SessionUserID, $this->OrderID,
				array("StatusID"=>5,"OnRoute"=>'No')
			);

			/*add reward points*/
			$this->Users_model->updateRewardPoints(
				array("TransactionID" => $this->OrderID, "TransactionDetails" => ''),
				$OrderData['UserID'], 100, 'Add', 'Purchase');


			/* Send order delivered email to user - starts */
			$UserData=$this->Users_model->getUsers('U.FirstName, U.Email',array('UserID'=>$OrderData['UserID']));
			if(!empty($UserData['Email'])){
				/*set variables (used to send with email)*/
				$DeliveryAddress =
				$OrderData['Name'].'<br>'.
				$OrderData['Address'].'<br>'.
				$OrderData['Address1'].'<br>'.
				$OrderData['City'].' '.(!empty($OrderData['Postal']) ? '- '.$OrderData['Postal'] : '').'<br>'.
				'Contact No. '.$OrderData['PhoneNumber'];
				/* get order details - ends */

				$content = $this->load->view('emailer/placed_order',array("Name" =>  $UserData['FirstName'], "OrderID" => $this->OrderID),TRUE);

				sendMail(array(
					'emailTo' 		=> $UserData['Email'],			
					'emailSubject'	=> "Order Confirmation - Your Order with ".SITE_NAME." [".$this->OrderID."] has been successfully delivered!",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE)) 
				));
			}
			/* Send order delivered email to user - ends */

			/* send notification on like - starts */
			$NotificationText = "Your order ".$this->OrderID." has been successfully delivered!";
			$this->Notification_model->addNotification('confirm_delivery', $NotificationText, $this->SessionUserID, $OrderData['UserID'], $this->Post['OrderGUID']);
			/* send notification on like - ends */

			$this->Return['Message'] = "Order delivered successfully.";
		}
	}


	/*
	Name: updateOrderAssigned
	Description: Use to update assigned order.
	URL: /api/store/updateOrderAssigned
	*/

	public function updateOrderAssigned_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim|callback_validateOrderGUID');
		$this->form_validation->set_rules('Status', 'Status', 'trim|in_list[Accept,Reject]');
		$this->form_validation->set_rules('OnRoute', 'OnRoute', 'trim|in_list[Yes,No]');

		$this->form_validation->validation($this);  /* Run validation */
		/* Validation - ends */
		/* get order details - starts */
		$OrderData=$this->Store_model->getOrders('O.UserID',array('OrderID'=>$this->OrderID));



		if(!empty($this->Post['Status'])){
			$StatusID = ($this->Post['Status']=='Accept' ? '2' : '3');	

			if($StatusID==2){
				/* send notification on Accept - starts */
				$NotificationText = "Your order ".$this->OrderID." has been accepted by the delivery person.";
				$this->Notification_model->addNotification('accept_order', $NotificationText, $this->SessionUserID, $OrderData['UserID'], $this->Post['OrderGUID']);
				/* send notification on Accept - ends */
			}

		}

		/* change order assigned status */
		if($this->Store_model->updateOrderAssigned($this->SessionUserID, $this->OrderID,
			array(
				"StatusID"	=>	@$StatusID,
				"OnRoute"	=>	@$this->Post['OnRoute']
			)
		)){
			$this->Return['Message'] = ($this->Post['Status']=='Accept' ? 'Order accepted.' : 'Order rejected.');
		}

		if(!empty($this->Post['OnRoute']) && $this->Post['OnRoute']=='Yes'){
			/* send notification on like - starts */
			$NotificationText = "Your order ".$this->OrderID." is on its way.";
			$this->Notification_model->addNotification('on_delivery', $NotificationText, $this->SessionUserID, $OrderData['UserID'], $this->Post['OrderGUID']);
			/* send notification on like - ends */
		}

	}




	/*
	Name: getOrders
	Description: Use to get order list.
	URL: /api/store/getOrders
	*/

	public function getOrders_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|required|in_list[Processing,Past,Assigned]');
		$this->form_validation->set_rules('Status', 'Status', 'trim|in_list[Pending,Accepted,Delivered]');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Orders=$this->Store_model->getOrders('
			O.OrderID,
			O.OrderGUID,
			O.OrderPrice,
			O.FirstName,
			O.LastName,
			O.Address,
			O.Address1,
			O.City,
			O.PhoneNumber,
			O.DeliveryType,
			O.PaymentMode,
			O.PaymentGateway,
			O.TransactionID,
			O.EntryDate,
			(SELECT GROUP_CONCAT(P.ProductName SEPARATOR ", ") FROM `ecom_products` P, `ecom_order_details` OD WHERE P.ProductID=OD.ProductID AND OD.OrderID=O.OrderID) AS ProcuctNames,

			(SELECT IF (((SELECT OnRoute FROM `ecom_order_assigned` WHERE OrderID=O.OrderID AND OnRoute="Yes") IS NOT NULL), "Yes", "No")) AS OnRoute


		',array(($this->Post['Filter']=='Assigned' ? 'ToUserID': 'UserID'/*Order assigned to user*/)=>$this->SessionUserID, 'Filter'=>$this->Post['Filter'], 'Status'=>@$this->Post['Status']), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($Orders){
			$this->Return['Data'] = $Orders['Data'];
		}
	}






	/* Validations */
	function validateRewardPoints($RewardPoints)
	{	
		if(empty($RewardPoints)){
			return TRUE;
		}
		$UserData=$this->Users_model->getUsers('U.RewardPoints',array('UserID'=>$this->SessionUserID));
		if($UserData['RewardPoints']>=$RewardPoints){
			return TRUE;
		}
		$this->form_validation->set_message('validateRewardPoints', "You don't have much reward points to spend.");  
		return FALSE;
	}

	function validateCoupon($CouponCode)
	{	
		if(empty($CouponCode)){
			return TRUE;
		}
		$CouponData = $this->Store_model->getCoupons('CouponID, CouponValidTillDate',array("CouponCode"=>$CouponCode,"StatusID"=>2));
		if($CouponData){
			if(!empty($CouponData['CouponID']) && strtotime($CouponData['CouponValidTillDate'])<time()){
				$this->form_validation->set_message('validateCoupon', 'Coupon expired.');  
				return FALSE;
			}
			$this->CouponID = $CouponData['CouponID'];
			return TRUE;
		}
		$this->form_validation->set_message('validateCoupon', 'Invalid {field}.');  
		return FALSE;
	}




}
