<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Assign Paper</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getFilterData()"><!-- Body -->
	<div class="row" style="margin-top: 10px;">		 
		<div class="col-md-10">
	      	<div class="form-group">
		      <span class="float-left records hidden-sm-down">
				<span class="h5">Total records: {{data.dataList.length}}</span>
			  </span>
			 </div>
		</div>
		<div class="col-md-2" style="    margin-top: 0px; float: right;">

			<label for="inputName" class="control-label mb-10"></label>

			<!-- <button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadAssignPaper1();">Assign Question Paper</button> -->

		</div>	
	</div>
	<form id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter1()">
		<div class="clearfix mt-4 mb-2">

		   <div class="row float-left ml-6" style="width:80%;">

		      <div class="col-md-3">

		         <div class="form-group">

		            <select class="form-control" onchange="getCategorySubjects(this.value,'getBatchPapersList')" name="ParentCat" id="Batch">

		               <option value="">Select Batch</option>

		               <option ng-repeat="row in batch" value="{{row.BatchGUID}}" ng-selected="row.BatchGUID = BatchGUID">{{row.BatchName}}</option>
		            </select>

		         </div>

		      </div>

		      <div class="col-md-3">

		         <div class="form-group">

		            <div id="subcategory_section">

		               <select id="subject" name="CategoryGUIDs" onchange="filterBatchPapersList(this.value)"  class="form-control">

		                  <option value="">Select Subject</option>

		                  <option ng-repeat="row in subject" value="{{row.CategoryGUID}}" ng-selected="row.CategoryGUID == SubjectGUID">{{row.CategoryName}}</option>

		               </select>

		            </div>

		         </div>

		      </div>
		      <div class="col-md-3">

				<div class="multiselect">

					<!-- <label for="inputName" class="control-label mb-10">Question Group</label> -->

				    <select class="form-control" name="QuestionsGroup" ng-model="QuestionsGroup" ng-change="applyFilter1(QuestionsGroup)">

							<option value="">All Question Group</option>

							<option value="1">Practice Test</option>

							<option value="2">Quiz</option>

							<option value="3">Assessment</option>

							<option value="4">Contest</option>

							<option value="5">Scholarship Test</option>

					</select>

				</div>

			</div>
		      <div class="col-md-3">

					<div class="form-group">
					 	 <div id="universal_search">
	                        <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
	                        <span class="glyphicon glyphicon-search" ng-click="applyFilter1()" data-toggle="tooltip" title="Search"></span>
	                        <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
	                    </div>
					</div>

			  </div>

		   </div>

		  <!--  <div class="float-right mr-2">		

				<a class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" href="<?php echo base_url().'questionassign/quiz/'; ?>">Quiz Statistics</a>

		   </div> -->

		</div>
	</form>

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">

	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">

	<input type="hidden" name="CategoryGUID" id="CategoryGUID">

	<input type="hidden" name="QuestionsLevel" id="QuestionsLevel">

	<input type="hidden" name="QuestionsGroup" id="QuestionsGroup">

	<input type="hidden" name="QuestionsType" id="QuestionsType">





	<!-- Data table -->

	<div class="table-responsive block_pad_md" style="margin-top: 0px;" infinite-scroll="getBatchPapersList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 7px;">S.No</th>

					<th style="width: 150px;">Paper Title</th>					

					<th style="width: 100px; ">Paper Group</th>

					<th style="width: 100px;">Assign Date</th> 
					<th style="width: 100px;">Test Date</th>

					<th style="width: 130px; text-align: center;">Activation From/<br/>To Time</th>

					<th style="width: 120px; text-align: center;">Start Time/<br/>End Time</th>					

					<th style="width: 70px; text-align: center;">Duration</th>

					<th style="width: 50px; text-align: center;">Total Students</th>

					<th style="width: 50px; text-align: center;">Total Marks</th>

					<th style="width: 75px; text-align: center;">Passing Marks</th>

					<th style="width: 150px; text-align: center;">Result/Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row"  ng-repeat="(key, row) in data.dataList">

					<td>{{key+1}}</td>

					<td >{{row.QtPaperTitle}}</td>

					<td >{{row.QuestionsGroup}}</td>

					<td >{{row.AssignedDate}}</td>

					<td >{{row.TestDateDis}}</td>

					<td style="text-align: center;">{{row.ActivatedFrom}}/<br/>{{row.ActivatedTo}}</td>

					<td style="text-align: center;">{{row.StartTime}}<br/>{{row.EndTime}}</td>

					<td style="text-align: center;">{{row.Duration}} Min.</td>

					<td style="text-align: center;" ng-if="!row.TotalStudents">0</td>

					<td style="text-align: center;" ng-if="row.TotalStudents">{{row.TotalStudents}}</td>



					<td style="text-align: center;" ng-if="!row.TotalMarks">0</td>

					<td style="text-align: center;" ng-if="row.TotalMarks">{{row.TotalMarks}}</td>  


					<td style="text-align: center;" ng-if="row.PassingMarks">{{row.PassingMarks}}%</td>

					<!-- <td style="text-align: center;">{{row.TotalAttendedBy}}</td>

					<td style="text-align: center;">{{row.TotalFailedStudents}}</td>

					<td style="text-align: center;">{{row.TotalPassedStudents}}</td> -->

					<td class="text-center" style="text-align: center;">

						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'questionassign/results/?QtPaperGUID={{row.QtPaperGUID}}'; ?>" data-toggle="tooltip" title="View Results"></a> 

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-if="row.EditStatus == 'No'" class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEdit(key,row.QtAssignGUID, row.QtPaperGUID, 'test')" data-toggle="tooltip" title="Edit"></a>						

						<!-- <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-if="row.EditStatus == 'No'" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key,row.QtAssignGUID)" data-toggle="tooltip" title="Delete"></a> -->

						
						<a class="glyphicon glyphicon-export" href="<?php echo base_url().'questionassign/result_export_pdf?QtPaperGUID={{row.QtPaperGUID}}'; ?>"  data-toggle="tooltip" title="Export Paper Result In PDF"></a> 


						<!-- <a class="glyphicon glyphicon-repeat" href="" ng-click="loadAssignPaper1(key,row.QtPaperGUID)" data-toggle="tooltip" title="Assign Question Paper"></a> -->

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Generate Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<!-- add Modal -->

	<div class="modal fade" id="assign_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Assign Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAssign"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_subject">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Subjcet</h3>     	

					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="subjectTemplateURLAdd"></div>

			</div>

		</div>

	</div>











	<!-- edit Modal -->

	<div class="modal fade" id="edit_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit Assigned Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>





	<!-- edit Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Question Details</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>

</div><!-- Body/ -->