<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Keys extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"Inventory" => ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('keys/keys_statistics');

		$this->load->view('includes/footer');

	}





	public function assign()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/keys.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"Assign Keys" => ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('keys/assign_keys');

		$this->load->view('includes/footer');

	}


	public function used()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/keys.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"Inventory" => base_url()."keys",
			"Details of used keys" => ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('keys/used_keys_list');

		$this->load->view('includes/footer');

	}

}

