<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Gallery_model');
	}


	public function getPhotosInAlbum_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('Category', 'Category','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
    
	    $GalleryData = $this->Gallery_model->getPhotosInAlbum($this->EntityID, $this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData['Data'];
		}	
	}


	public function getGalleryStatistics_post(){
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('Category', 'Category','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
	    $GalleryData = $this->Gallery_model->getGalleryStatistics($this->EntityID, $this->Post, TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData;
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getgallerys_post()
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('Category', 'Category','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
	  $GalleryData = $this->Gallery_model->getgallery('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		//$GalleryData = $this->Media_model->getMedia('MediaCaption',array("SectionID" => 'Gallery',"InstituteID"=>$InstituteID),TRUE);

		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData['Data'];
		}	
	}




	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function add_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('MediaCaption', 'Title', 'trim|required');
		$this->form_validation->set_rules('Category[]', 'Album', 'trim|required');
		$this->form_validation->set_rules('MediaGUIDs', 'Please Upload Image', 'trim|required');

		
		$this->form_validation->validation($this);  /* Run validation */		

		//echo $this->Post['MediaGUIDs'];
		
		$GalleryData = $this->Gallery_model->add($this->Post);

		//echo $GalleryData;

			if($GalleryData == 1){

				$this->Return['Message']      	=	"New Image added successfully."; 

			} else {

                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Media Title Name Already Exist.";
				
			}


	}

	public function getCategory_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('AlbumID', 'AlbumID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CategoryData = $this->Gallery_model->getCategory('',array("AlbumID"=>$this->input->post('AlbumID')));
		if(!empty($CategoryData)){
			$this->Return['Data'] = $CategoryData['Data'];
		}	
	}

	public function addcategory_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Category', 'Album Name', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		
		$GalleryData = $this->Gallery_model->addcategory($this->Post);

		//echo $GalleryData;

			if($GalleryData == 1){

				$this->Return['Message']      	=	"New Album added successfully."; 

			} else {

                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Album Name Already Exist.";
				
			}


	}



	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategory
	Input (Sample JSON): 		
	*/
	public function getgalleryBy_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$GalleryData = $this->Gallery_model->getgalleryBy('',array("MediaGUID"=>$this->input->post('MediaGUID')));
		//$GalleryData = $this->Media_model->getMedia('MediaCaption',array("SectionID" => 'Gallery',"MediaGUID" => $Record['MediaGUID']),TRUE);
		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData;
		}	
	}

	public function delete_post()
	{
		/* Validation section */
		//$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required|callback_validateEntityGUID[Media, MediaID]');
		//$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		//echo $this->input->Post('MediaGUID'); die();
		$data=$this->Gallery_model->delete($this->input->Post('MediaGUID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
	}



	/*

	Name: 			editWallOfFame_post
	

	*/
	public function editgallery_post()

	{

		/* Validation section */
        $this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('MediaCaption', 'Title', 'trim|required');
		$this->form_validation->set_rules('Category[]', 'Album', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required');
		$this->form_validation->set_rules('MediaGUIDe', 'MediaGUIDe', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		if(!empty($this->Post['MediaGUIDe'])){

			$this->Gallery_model->add($this->Post);
			$this->Gallery_model->DeteleMedia($this->input->Post('MediaGUID'));
			$this->Return['Data']= $this->Gallery_model->getgallery('',array("MediaGUID"=>$this->input->Post('MediaGUIDe')));
			
		}else{

			$this->Gallery_model->editgallery($this->Post);
			$this->Return['Data']= $this->Gallery_model->getgallery('',array("MediaGUID"=>$this->input->Post('MediaGUID')));
		}
	
		

	    $this->Return['Message']      	=	"Gallery Image updated successfully."; 

	}



	/*

	Name: 			EditAlbum_post
	

	*/
	public function EditAlbum_post()

	{

		/* Validation section */
        $this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('AlbumName', 'AlbumName', 'trim|required');
		$this->form_validation->set_rules('AlbumID', 'AlbumID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->Gallery_model->EditAlbum($this->Post);
		

	    $this->Return['Message']      	=	"Album updated successfully."; 

	}


	/*

	Name: 			DeleteAlbum_post
	

	*/
	public function DeleteAlbum_post()

	{

		/* Validation section */
        $this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
        $this->form_validation->set_rules('AlbumName', 'AlbumName', 'trim|required');
		$this->form_validation->set_rules('AlbumID', 'AlbumID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->Gallery_model->DeleteAlbum($this->Post);
		

	    $this->Return['Message']      	=	"Album deleted successfully."; 

	}


	/*
	Description: 	Use to get Get photo gallery by album id.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getPhotosGallery_post()
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$this->form_validation->set_rules('AlbumID', 'AlbumID','trim|required');
		$this->form_validation->set_rules('AlbumName', 'AlbumName','trim|required');
		$this->form_validation->validation($this); 
    
	    $GalleryData = $this->Gallery_model->getPhotosGallery($this->EntityID,$this->Post);

		if(!empty($GalleryData)){
			$this->Return['Data'] = $GalleryData['Data'];
		}	
	}
}