<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Order extends API_Controller_Secure

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('Recovery_model');

		$this->load->model('Notification_model');

		$this->load->model('Utility_model');

		$this->load->model('Order_model');

		$this->load->model('Keys_model');

		$this->load->model('Users_model');

		$this->load->model('Common_model');

	}







	/*

	Description: 	Use to get keys price.

	URL: 			/api/order/getKeysPrice

	Input (Sample JSON): 		

	*/

	public function getKeysPrice_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->getKeysPrice($this->EntityID);



		if($KeysData){



			$this->Return['Data']      	=	$KeysData; 

		}

	}




	/*

	Description: 	Use to get orders.

	URL: 			/api/order/getOrders

	Input (Sample JSON): 		

	*/

	public function getOrders_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->set_rules('OrderGUID','OrderGUID','trim');

		$this->form_validation->set_rules('OrderFor','OrderFor','trim');

		$this->form_validation->set_rules('PaymentStatus','PaymentStatus','trim');

		$this->form_validation->set_rules('PaymentMethod','PaymentMethod','trim');

		$this->form_validation->set_rules('OrderStatusID','OrderStatusID','trim');

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');

		$this->form_validation->set_rules('menu_type','menu_type','trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->getOrders($this->EntityID, array(
			"OrderGUID" => $this->input->post('OrderGUID'),
			"OrderFor" => @$this->input->post('OrderFor'),
			"PaymentStatus" => @$this->input->post('PaymentStatus'),
			"PaymentMethod" => @$this->input->post('PaymentMethod'),
			"Keyword" => @$this->input->post('Keyword'),
			"UserID" => @$this->UserID,
			"OrderStatusID" => @$this->input->post('OrderStatusID')
		), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize'], $this->input->post('menu_type'));



		if($KeysData){

			$this->Return['Data']      	=	$KeysData; 

		}

	}



	/*

	Description: 	Use to get orders.

	URL: 			/api/order/getOrders

	Input (Sample JSON): 		

	*/

	public function getSMSOrders_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->set_rules('OrderGUID','OrderGUID','trim');

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');

		//$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim');

		$this->form_validation->set_rules('menu_type','menu_type','trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->getSMSOrders($this->EntityID,array("OrderGUID"=>$this->input->post('OrderGUID'),"UserID"=>@$this->UserID),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize'], $this->input->post('menu_type'));



		if($KeysData){

			$this->Return['Data']      	=	$KeysData; 

		}

	}





	/*

	Description: 	Use to add keys order.

	URL: 			/api/order/add

	Input (Sample JSON): 		

	*/

	public function add_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('Validity[]', 'Validity', 'trim|required');

		$this->form_validation->set_rules('OrderKeys[]', 'KEYS COUNT', 'trim|required');

		$this->form_validation->set_rules('Price[]', 'KEYS PRICE', 'trim|required');

		$this->form_validation->set_rules('TotalPrice', 'TOTAL AMOUNT', 'trim|required');

		$this->form_validation->set_rules('OrderGUID', 'OrderGUID', 'trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$OrderData = $this->Order_model->addOrder($this->Post,$this->EntityID);

		if($OrderData){
			$this->Return['Data']  =	$OrderData; 
		}

	}



	public function addSMSCreditOrder_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ChequeAmount', 'ChequeAmount', 'trim|required|greater_than[0]');
		$this->form_validation->set_rules('OrderFor', 'OrderFor', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$OrderData = $this->Order_model->addSMSCreditOrder($this->Post,$this->EntityID);
		$this->Return['Data']  =	$OrderData; 
		//$this->Post['OrderGUID'] = $OrderData['OrderGUID'];
	}



	/*

	Description: 	Use to save payment details

	URL: 			/api/order/makePayment

	Input (Sample JSON): 		

	*/

	public function makePayment_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		if($this->input->post('OrderFor') != "SMS Credits"){
			$this->form_validation->set_rules('OrderGUID', 'Order ID', 'trim|required');
		}

		$this->form_validation->set_rules('PaymentType', 'Payment Type', 'trim|required');

		if($this->input->post('PaymentType') == "Offline"){
			$this->form_validation->set_rules('ChequeNumber', 'Cheque Number', 'required');
			$this->form_validation->set_rules('DrawnBank', 'Cheque Drawn On', 'trim|required');
			$this->form_validation->set_rules('ChequeDate', 'Cheque Date', 'trim|required');
			$this->form_validation->set_rules('ChequeAmount', 'Amount', 'trim|required|greater_than[0]');
			$this->form_validation->set_rules('DepositedDate', 'Deposited On', 'trim|required');
		}else if($this->input->post('PaymentType') == "Online"){
			$this->form_validation->set_rules('TransactionNumber', 'Transaction Number', 'trim|required');
			$this->form_validation->set_rules('Amount', 'Amount', 'trim|required|greater_than[0]');
			$this->form_validation->set_rules('DepositedOn', 'Deposited On', 'trim|required');
			$this->form_validation->set_rules('OrderStatusID', 'OrderStatusID', 'trim|required');
		}

		$this->form_validation->set_rules('OrderFor', 'OrderFor', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */

		//print_r($this->Post);		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('OrderFor') == "SMS Credits"){
			$OrderData = $this->Order_model->addSMSCreditOrder($this->Post,$this->EntityID);
			$this->Post['OrderGUID'] = $OrderData['OrderGUID'];
		}

		if($this->input->post('PaymentType') == "Online"){
			$OrderData = $this->Order_model->makePayment($this->EntityID,$this->Post,$this->input->post('OrderStatusID'));
		}else{
			$OrderData = $this->Order_model->makePayment($this->EntityID,$this->Post,1);
		}

		

		if($OrderData){
			$this->Return['Data']  =	$OrderData; 
			$this->Return['Message']  =	"Payment has been done successfully.";
		}

	}


	/*

	Description: 	Use to allot  orderd keys.

	URL: 			/api/order/allotOrderedKeys

	Input (Sample JSON): 		

	*/

	public function allotOrderedKeys_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('OrderGUID','OrderGUID','trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->allotOrderedKeys($this->EntityID,array("OrderGUID"=>$this->input->post('OrderGUID')));



		if($KeysData){

			$this->Return['Data']      	=	$KeysData; 

		}

	}



	/*

	Description: 	Use to unallot  orderd keys.

	URL: 			/api/order/unallotOrderedKeys

	Input (Sample JSON): 		

	*/

	public function unallotOrderedKeys_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('OrderGUID','OrderGUID','trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->unallotOrderedKeys($this->EntityID,array("OrderGUID"=>$this->input->post('OrderGUID')));



		if($KeysData){

			$this->Return['Data']      	=	$KeysData; 

		}

	}

	


	/*

	Description: 	Use to allot  orderd sms.

	URL: 			/api/order/allotOrderedSMS

	Input (Sample JSON): 		

	*/

	public function allotOrderedSMS_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('OrderGUID','OrderGUID','trim|required');

		$this->form_validation->set_rules('SMSCredits','SMSCredits','trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->allotOrderedSMS($this->EntityID,array("OrderGUID"=>$this->input->post('OrderGUID'),"SMSCredits"=>$this->input->post('SMSCredits')));



		if($KeysData){

			$this->Return['Data']      	=	$KeysData; 

		}

	}


	public function verifyOrder_post()
	{
		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('OrderGUID','OrderGUID','trim|required');

		$this->form_validation->set_rules('PaymentGUID','PaymentGUID','trim|required');

		$this->form_validation->set_rules('CreditedOn','CreditedOn','trim|required');

		$this->form_validation->set_rules('TransactionNumber','TransactionNumber','trim|required');  

		$this->form_validation->set_rules('OrderFor','OrderFor','trim|required'); 

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);



		$KeysData = $this->Order_model->verifyOrder($this->EntityID,array("PaymentGUID"=>$this->input->post('PaymentGUID'),"OrderGUID"=>$this->input->post('OrderGUID'),"CreditedOn"=>$this->input->post('CreditedOn'),"TransactionNumber"=>$this->input->post('TransactionNumber'), "OrderFor" =>$this->input->post('OrderFor')));



		if(!$KeysData){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured"; 
		}
	}

}