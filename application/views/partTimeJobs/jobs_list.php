<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}

	#ui-id-1
	{
		z-index: 9999 !important;
		overflow-x: hidden;
		overflow-y: auto;
		max-height: 150px;
	}
</style>  
<header class="panel-heading"> <h1 class="h4">Manage Part Time Jobs</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData();" id="content-body"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<span class="h5">Total Jobs: {{data.dataList.length}}</span>
				</div>
			</div>

			<!-- <div class="col-md-6">
				<div class="form-group">
					<div class="float-right">			

						<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 87 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Post New Job</button>
					</div>
				</div>
			</div> -->
		</div>


		<form id="filterForm" role="form" autocomplete="off">
		<div class="row">		
		

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterStartDate" type="text" placeholder="Job Start Date" id="FilterStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterEndDate" type="text" placeholder="Job End Date" id="FilterEndDate" class="form-control">
			</div>
		</div>

		<!-- <div class="col-md-2">
			<div class="form-group">
				<select class="form-control" ng-change="getList('',1);" name="FilterEmployementType" id="FilterEmployementType">
					<option value="">Please Select</option>					
					<option value="Fulltime">Fulltime</option>
					<option value="Partime">Partime</option>
					<option value="Contractual">Contractual</option>
				</select>
			</div>
		</div> -->			

		<div class="col-md-3">
			<div class="form-group">			 	
	        	<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Search by word" >
	    	</div>
		</div>

		
		<div class="col-md-3">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" ng-click="getList(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="RemoveJobFilters();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>
		

		

	</div>
	</form>		

	</div>

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading --> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 100px;">Position</th>					
					<th style="width: 200px;">Job Provider</th>
					<th style="width: 70px;">Job Location</th> 
					<th style="width: 100px;">Salary</th>
					<th style="width: 100px;">Post Date/ Valid Till</th>
					<th style="width: 150px;" class="text-center">Applicants</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.PostJobGUID}}">

					
					<td><strong>{{row.Title}}</strong></td>			

					<td>{{row.ProviderName}}<br/><label style="font-size: 12px">{{row.ProviderEmail}}</label><br/><label style="font-size: 12px">{{row.ProviderPhoneNumber}}</label></td>

					<td>{{row.CityName}}, {{row.StateName}}</td>

					<td><label class="fa fa-rupee"></label> {{row.Salary}}&nbsp;per month</td>

					<td>{{row.EntryDate | date}} / {{row.EndDate | date}}</td> 	
								

					<td class="text-center" style="text-align: center;">

						<a  href="<?php echo base_url()."parttimejobs/applicants_list?PartTimeJobID={{row.PartTimeJobID}}"; ?>" ng-click="viewPartTimeJobs(key, row.PartTimeJobID, row)" data-toggle="tooltip" title="View Job Details">{{row.Applicants_count}}</a>
						

						<!-- <a class="glyphicon glyphicon-eye-open" href="" ng-click="viewPartTimeJobs(key, row.PartTimeJobID, row)" data-toggle="tooltip" title="View Job Details"></a>&nbsp;&nbsp;			
						
						<a ng-if="row.Applied != 1" class="glyphicon glyphicon-plus" href="" ng-click="ApplyForPartTimeJob(row.PartTimeJobID, row, 1);" data-toggle="tooltip" title="Apply For Job"></a> -->

						<!-- <a ng-if="row.Applied == 1" class="glyphicon glyphicon-plus" href="" ng-click="ApplyForPartTimeJob(row.PartTimeJobID, row, 0);" data-toggle="tooltip" title="Apply For Job" ng-if="row.ApplyID <= 0 || row.ApplyID == ''"></a> -->

						<!-- <a class="glyphicon glyphicon-plus" href="" ng-click="ApplyForJobPopup(row.PostJobID, row);" data-toggle="tooltip" title="Apply For Job"></a> -->
							
					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->



	
	<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Job</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Post Job</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Job</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>



</div><!-- Body/ -->