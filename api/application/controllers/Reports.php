<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('Reports_model');
	}

	public function generateReport_post()
	{		
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('filterReportType', 'Report Type', 'trim|required');
		$this->form_validation->set_rules('filterFromDate', 'From Date', 'trim|required');
		$this->form_validation->set_rules('filterToDate', 'To Date', 'trim|required');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');				
		$this->form_validation->validation($this);  /* Run validation */		

		$Data = $this->Reports_model->generateReport($this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);		
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}


	public function searchHistory_post()
	{		
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);  /* Run validation */		

		$Data = $this->Reports_model->searchHistory($this->Post);		
		
		if(!empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}



	public function saveSearch_post()
	{		
		if(!preg_match('/^[a-zA-Z0-9\s]+$/', $_POST['SearchName']) && $_POST['SearchName'] != "")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Only aplhabets are allowed in Search Name.";

			die;
		}

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('SearchName', 'Search Name', 'trim|required');
		$this->form_validation->set_rules('filterReportType', 'Report Type', 'trim|required');
		$this->form_validation->set_rules('filterFromDate', 'From Date', 'trim|required');
		$this->form_validation->set_rules('filterToDate', 'To Date', 'trim|required');		
		$this->form_validation->validation($this);		

		$SID = $this->Reports_model->saveSearch($this->Post);

		if($SID)
		{		
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Search saved successfully.";
		}		
	}




	public function deleteSearch_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('did', 'Search ID', 'trim|required');	
		$this->form_validation->validation($this);

		$Data = $this->Reports_model->deleteSearch($this->Post['did']);		
		
		if($Data > 0)
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Deleted successfully.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Oops! something went wrong. Please try again.";
		}
	}


	public function dailyRegistrations_post()
	{		
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->validation($this);		

		$Data = $this->Reports_model->dailyRegistrations();		
		
		if(isset($Data) && !empty($Data))
		{
			$this->Return['Data'] = $Data['Data'];
		}	
	}

}