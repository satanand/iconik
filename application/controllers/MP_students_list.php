<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<style>
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	.switch input { 
	  opacity: 0;
	  width: 0;
	  height: 0;
	}

	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}
</style>
<header class="panel-heading">
	<h1 class="h4">Manage Students</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="student-body" ng-init="getMPStudents(1)"><!-- Body -->
	<!-- <form id="filterForm" role="form" autocomplete="off"> -->
		<div class="row" style="margin-top: 20px;">	
			<div class="col-md-2">
		      	<div class="form-group">
			      <span class="float-left records hidden-sm-down">
					<span class="h5">Total records: {{data.dataLists.length}}</span>
				  </span>
				</div>
		  	</div>	 
			<div class="col-md-2">
				<!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
				<select class="form-control" onchange="filterStudentsList(this.value,'')" name="ParentCat" id="Courses">
	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
	               <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}" ng-selected="CourseGUID==row.CategoryGUID">{{row.CategoryName}}</option>
	            </select>
			</div>
			<div class="col-md-2">
				<!-- <label for="inputName" class="control-label mb-10">Select Subject</label> -->
				<div id="subcategory_section" class="subcategory_section">
	               <select id="subject" name="CategoryGUIDs" onchange="filterStudentsList('',this.value)"  class="form-control">
	                  <option value="">Select Batch</option>
	                  <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}" ng-selected="BatchGUID==row.BatchGUID">{{row.BatchName}}</option>
	               </select>
	            </div>
			</div>
			<div class="col-md-2">
				<form class="" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()">
					<div id="universal_search">
                        <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
                        <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
                        <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
                    </div>
				</form>
			</div>
			<div class="col-md-4 float-right">
				<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd('FromList');" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Student</button>
				<button ng-click="tableDatatoExcel()"  class="btn btn-success btn-sm ml-1 float-right" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Export Data to Excel</button>
				<span id="dvjson"></span>	
			</div>
		</div>												
	<!-- </form> -->


	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">
	<input type="hidden" name="CategoryGUID" id="CategoryGUID">
	<input type="hidden" name="QuestionsLevel" id="QuestionsLevel">
	<input type="hidden" name="QuestionsGroup" id="QuestionsGroup">
	<input type="hidden" name="QuestionsType" id="QuestionsType">


	<!-- Data table -->
	<div class="table-responsive block_pad_md" style="margin-top: 45px;" infinite-scroll="getStudentsList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
		
		<!-- data table -->
		<table class="table table-striped table-hover">
			<!-- table heading -->
			<thead>
				<tr>
					<th style="width: 100px; text-align: center;">Name</th>
					<th style="width: 100px; text-align: center;">Email</th>
					<th style="width: 100px; text-align: center;">Contact Number</th>
					<th style="width: 100px; text-align: center;">Fee</th>
					<th style="width: 100px; text-align: center;">Key</th>
					<th style="width: 100px; text-align: center;">Key Status</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row"  ng-repeat="(key, row) in data.dataLists">
					<td style="text-align: center;">{{row.FirstName | capitalizeFirstLetter}} {{row.LastName}}</td>
					<td style="text-align: center;">{{row.Email}}</td>
					<td style="text-align: center;">{{row.PhoneNumber}}</td>
					<td style="text-align: center;">

					<label ng-if="row.TotalFeeAmount==0 && row.TotalFee > 0">{{row.TotalFee}}<label ng-if="row.FeeStatusID == 3">(Old)</label></label>
					
					<label ng-if="row.TotalFeeAmount==0 && row.TotalFee == '-' && row.FullDiscountFee > 0">{{row.FullDiscountFee}}<label ng-if="row.FeeStatusID == 3">(Old)</label></label>

					<label ng-if="row.TotalFeeAmount > 0">{{row.TotalFeeAmount}}<label ng-if="row.FeeStatusID == 3">(Old)</label></label>

					<label ng-if="row.TotalFeeAmount=='' && row.TotalFee == '-' && row.FullDiscountFee == 0">-</label>

					<label ng-if="row.TotalFeeAmount==0 && row.TotalFee == '-' && row.FullDiscountFee == 0">-</label>

					
					</td>

					<td style="text-align: center;" ng-if="row.Key">{{row.Key}}</td>
					<td style="text-align: center;" ng-if="!row.Key.length">-</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==2" class="text-success">Active</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==1" class="text-danger">Inactive</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==6" class="text-danger">Expired</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==0" class="text-danger">Not Assigned</td>
					<td class="text-center" style="text-align: center;">						
						<a  class="glyphicon glyphicon-upload" href="" ng-click="loadImageForm(key,row.UserGUID,row.ProfilePic)" title="Upload Image"></a>
						<a  class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key,row.UserGUID)" title="View"></a>
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key,row.UserGUID,row.StateName)" title="Edit"></a>
						<!-- <a class="glyphicon glyphicon-plus" href="" ng-click="loadFormAdd(key, row.PostGUID)"></a> -->
						<a  class="glyphicon glyphicon-trash" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" href="" ng-click="loadFormDelete(key,row.UserGUID,row.KeyStatusID)"></a>
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="!data.dataLists.length">
			<span ng-if="data.dataLists.length">No more records found.</span>
			<span ng-if="!data.dataLists.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->




	<!-- add Modal -->
	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Student</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="add_subject">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Subjcet</h3>     	
					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="subjectTemplateURLAdd"></div>
			</div>
		</div>
	</div>



	<div class="modal fade" id="upload_image">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Upload Image</h3>     	
					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="uploadTemplateURLAdd"></div>
			</div>
		</div>
	</div>





	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Student Details</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLEdit"></div>
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Student Details</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>


	<!-- delete Modal -->
	<div class="modal fade" id="delete_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Delete Student</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
</div><!-- Body/ -->

<script type="text/javascript">
	function test(status){
		alert(status);
	}
</script>