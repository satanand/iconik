<h2 style="margin-bottom:20px; font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; "><?php echo $Name;?>,</h2>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Welcome to Iconik.<br/>
Please click on the link to set your password and enter the OTP provided to set your password</p>

<p style="font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">
<a href="<?php echo ADMIN_BASE_URL.'recovery/reset' ?>">You can also copy and paste this link in your browser:</a>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">One time password: <?php echo $Token; ?></p>