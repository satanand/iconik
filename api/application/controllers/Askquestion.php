<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Askquestion extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Askquestion_model');
		$this->load->model('Entity_model');
	} 


	/*
		getFacultyList
	*/

	public function getFacultyList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Results = $this->Askquestion_model->getFacultyList($this->EntityID);

		if($Results){

			$this->Return['Data'] = $Results;
		}
	}


	/*

	Name: 			add ask a question

	Description: 	Use to add ask a question

	URL: 			/api/Askquestion/add

	*/
	public function add_post()
	{

		//print_r($this->input->post()); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		if(!empty($this->input->post('UserTypeID')) && $this->input->post('UserTypeID') == 11){
			$this->form_validation->set_rules('UserGUID', 'Student ID', 'trim|callback_validateEntityGUID[Students,UserID]');			
		}else{
			$this->form_validation->set_rules('UserGUID', 'Faculty', 'trim|required|callback_validateEntityGUID[User,UserID]');
		}
		
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		$this->form_validation->set_rules('QueryType', 'QueryType', 'trim|required|in_list[Query,Reply]');	
		$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->set_rules('QueryCaption', 'QueryCaption', 'trim');
		$this->form_validation->set_rules('QueryContent', 'Question', 'trim|required');
		$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');
		// $this->form_validation->set_rules('File', 'File', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->addQuery($this->EntityID, $this->UserID, $this->QueryID, $this->input->post());

		if($KeysData){

			$this->Return['Message']      	=	$this->input->post('QueryType')." added successfully."; 
		}
	}



	/*

	Name: 			edit ask a question

	Description: 	Use to edit ask a question

	URL: 			/api/Askquestion/edit

	*/
	public function edit_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		if(!empty($this->input->post('UserTypeID')) && $this->input->post('UserTypeID') == 11){
			//echo "11";
			$this->form_validation->set_rules('UserGUID', 'Student ID', 'trim|callback_validateEntityGUID[Students,UserID]');
		}else{
			$this->form_validation->set_rules('UserGUID', 'Faculty', 'trim|callback_validateEntityGUID[User,UserID]');
		}
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		$this->form_validation->set_rules('QueryType', 'QueryType', 'trim|required|in_list[Query,Reply]');	
		$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|required|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->set_rules('QueryCaption', 'QueryCaption', 'trim');
		$this->form_validation->set_rules('QueryContent', 'Question', 'trim|required');
		$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');
		// $this->form_validation->set_rules('File', 'File', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->editQuery($this->EntityID, $this->UserID, $this->QueryID, $this->input->post());

		if($KeysData){

			$this->Return['Message']      	=	$this->input->post('QueryType')." updated successfully."; 
		}
	}


	/*

	Name: 			edit ask a question

	Description: 	Use to edit ask a question

	URL: 			/api/Askquestion/edit

	*/
	public function delete_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|required|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->deleteQuery($this->EntityID,$this->QueryID);

		if($KeysData){

			$this->Return['Message']      	=	"Query deleted successfully."; 
		}
	}



	/*

	Name: 			getPosts

	Description: 	Use to get list of post.

	URL: 			/api/post/getPosts

	*/

	public function getQueries_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		if(!empty($this->input->post('ParentQueryGUID'))){
			$this->form_validation->set_rules('ParentQueryGUID', 'ParentQueryGUID', 'trim|callback_validateEntityGUID[Ask a Question,QueryID]');
		}else{
			$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|callback_validateEntityGUID[Ask a Question,QueryID]');
		}

		//$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('QueryType', 'QueryType', 'trim|required|in_list[Query,Reply]');

		$this->form_validation->set_rules('ReqType', 'ReqType', 'trim');	
		
		//$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');

		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->set_rules('CourseID', 'CourseID', 'trim');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */



		$Posts=$this->Askquestion_model->getQueries('

			EU.EntityGUID UserGUID,

			E.EntityGUID QueryGUID,

			P.QueryID,

			P.LikedByUserID,

			P.QueryContent,

			P.QueryCaption,
 
			P.MediaID,

			P.EntryDate,

			CONCAT_WS(" ",U.FirstName,U.LastName) FacultyName,

			U.UserGUID  AS FacultyGUID,

			CONCAT_WS(" ",US.FirstName,US.LastName) StudentName,

			US.UserGUID AS StudentGUID,

			IF(P.EntityID='.$this->SessionUserID.',"Yes", "No") AS IsMyPost



			',array(

				'SessionUserID'	=>	$this->SessionUserID,

				'EntityID'		=>	(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),

				'QueryID'		=>	@$this->QueryID,

				'ParentQueryID'	=>	@$this->QueryID,

				'QueryType'     =>	@$this->Post['QueryType'],

				'Keyword'		=>	@$this->Post['Keyword'],

				'CourseID'   =>	@$this->Post['CourseID'],

				'UserTypeID'   =>	@$this->Post['UserTypeID'],

				'ReqType' => @$this->Post['ReqType']


			), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Posts)
		{
			$this->Return['Data'] = $Posts['Data'];
		}
		else
		{
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message']   =	""; 
			$this->Return['Data'] = [];
		}

	}


	public function setLikeDislike_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|required|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->set_rules('Liked', 'Liked', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Status = $this->Askquestion_model->setLikeDislike($EntityID,$this->QueryID,$this->input->post('Liked'));


		if($Status >= 0)
		{
			$this->Return['Data']['LikedByUserID'] = $Status;
			$this->Return['Message']      	=	"Updated successfully."; 
		}else{
			$this->Return['ResponseCode']   =	500; 
		}

	}



	/*-------------------------------------------------------------
    Ask A Question - For Every One Global Section
    -------------------------------------------------------------*/
    public function getAAQList_post()
    {
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		//$this->form_validation->set_rules('QueryGUID', 'QueryGUID', 'trim|required|callback_validateEntityGUID[Ask a Question,QueryID]');
		$this->form_validation->set_rules('Type', 'Type', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim');

		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Askquestion_model->getAAQList($EntityID, $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = ""; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message'] = "No record found."; 
		}
	}


	
	public function addAAQ_post()  
	{
		//$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|required|callback_validateEntityGUID');
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('aaq_add_subject', 'Question Subject', 'trim|required');
		$this->form_validation->set_rules('aaq_add_relatedto', 'Question Related To', 'trim|required');
		$this->form_validation->set_rules('aaq_add_question', 'Question', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');		

		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->addAAQ($this->EntityID, $this->input->post());

		if($KeysData)
		{
			$this->Return['ResponseCode']   =	200;
			$this->Return['Message']      	=	"Question added successfully."; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message'] = "Invalid post content. Please try again."; 
		}
	}


	public function getAAQReply_post()
    {
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Type', 'Type', 'trim');
		$this->form_validation->set_rules('AAQID', 'Question Id', 'trim|required');
		$this->form_validation->set_rules('filterReply', 'Search Word', 'trim');

		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Askquestion_model->getAAQReply($EntityID, $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = ""; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] =	500;
			$this->Return['Message'] = "No record found."; 
		}  
	}
 
	public function getAAQReplyOnly_post()
    {
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('Type', 'Type', 'trim');
		$this->form_validation->set_rules('AAQID', 'Question Id', 'trim|required');
		$this->form_validation->set_rules('filterReply', 'Search Word', 'trim');

		$this->form_validation->validation($this);  /* Run validation */	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Askquestion_model->getAAQReplyOnly($EntityID, $this->Post);

		if(!empty($data))
		{
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = ""; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] =	500;
			$this->Return['Message'] = "No record found."; 
		}
	}


	public function addAAQReply_post()   
	{
		//echo "<pre>"; print_r($_POST); die;
		//$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|required|callback_validateEntityGUID');
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('AAQID', 'Question ID', 'trim|required');
		$this->form_validation->set_rules('aaq_reply_content', 'Reply', 'trim|required');
		
		$this->form_validation->set_rules('aaq_reply_subject', 'Subject', 'trim');		
		$this->form_validation->set_rules('aaq_reply_related_to', 'Related To', 'trim');		
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');


		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$KeysData = $this->Askquestion_model->addAAQReply($this->EntityID, $this->input->post());

		if($KeysData)
		{
			$this->Return['ResponseCode']   =	200;
			$this->Return['Message']      	=	"Reply added successfully."; 
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode']   =	500;
			$this->Return['Message'] = "Invalid reply content. Please try again."; 
		}
	}


	public function AAQSetLikeDislike_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('aaqID', 'aaqID', 'trim|required');
		$this->form_validation->set_rules('Liked', 'Liked', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */

		//print_r($this->input->post()); die;	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Status = $this->Askquestion_model->AAQSetLikeDislike($EntityID,$this->input->post('aaqID'),$this->input->post('Liked'));

		if($Status >= 0)
		{
			$this->Return['Data']['LikedByUserID'] = $Status;
			if($this->input->post("Liked") == 0){
				$this->Return['Message']      	=	"Disliked successfully."; 
			}else{
				$this->Return['Message']      	=	"Liked successfully."; 
			}			
		}else{
			$this->Return['ResponseCode']   =	500; 
		}
	}


	public function setRating_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('aaqID', 'aaqID', 'trim|required');
		$this->form_validation->set_rules('rating', 'Rating', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */

		//print_r($this->input->post()); die;	

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$Status = $this->Askquestion_model->setRating($EntityID,$this->input->post('aaqID'),$this->input->post('rating'));

		if($Status)
		{
			$this->Return['Message']      	=	"Rating Updated successfully."; 
		}else{
			$this->Return['ResponseCode']   =	500; 
			$this->Return['Message']      	=	"Sorry! Some error occured, Please try later."; 
		}
	}

}