app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.Users = [];
    $scope.data.InterestedIn = [];

    $( "#filterFromDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterToDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#filterToDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterFromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentParentCategoryGUID = ParentCategoryGUID;

    $scope.CategoryTypeID = $scope.getUrlParameter('CategoryTypeID');

    $scope.APIURL = API_URL + "upload/image";
    /*----------------*/

    
    /*list*/

    $scope.applyFilter = function ()

    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        
        $scope.getList(0);

    }


    /*------------------------------------------------------------------
    Load master filters
    -----------------------------------------------------------------*/
    /*$scope.InterestedInList = function ()
    {
        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'enquiry/getInterestedInList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.InterestedIn)
            {
                if($scope.data.InterestedIn.length <= 0)
                {
                    for (var i in response.InterestedIn) 
                    {
                        $scope.data.InterestedIn.push(response.InterestedIn[i]);
                    }
                }
            }
        });
    }*/




    /*list append*/

    $scope.getList = function (IsSet)
    {
        if(IsSet == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }    

        $scope.data.listLoading = true;        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
     

        $http.post(API_URL+'students/getScholarshipPayments', data, contentType).then(function(response) 
        {
            var response = response.data;

            //$scope.data.listLoading = false;

            console.log(response.Data.length);
            
            if(response.ResponseCode == 200 && response.Data.length > 0)
            { /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) 
                {
                    $scope.data.dataList.push(response.Data[i]);
                }                 
                
                $scope.data.pageNo++;   

                $scope.data.listLoading = false;            
                
            }
            else
            {

                $scope.data.noRecords = true;

                $scope.data.listLoading = false;
            }           

            

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }



    /*load view form*/

    $scope.loadFormView = function (Position, EnquiryGUID)

    {
        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'enquiry/getEnquiry', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#view_model').modal({show:true});

            }

        });

    }



}); 




/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

            type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }
}


function search_records()
{
    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function clear_search_records()
{    
    $("#filterKeyword, #filterFromDate, #filterToDate").val("");    

    search_records();
}
