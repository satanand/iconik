app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.pageSize = 15;

    $scope.availableData = [];

    $scope.data.pageNo = 1;

    //$scope.data.batch = [];
    /*----------------*/

    $scope.availableKeys = function (validity)
    {
        var data = 'SessionKey='+SessionKey+'&validity='+validity;

        $http.post(API_URL+'keys/availableKeys', data, contentType).then(function(response) {

            var response = response.data;

                if(response.ResponseCode==200 && response.Data)
                { /* success case */

                    $scope.availableData =  response.Data[0]; 
                    console.log($scope.availableData);

                    $timeout(function(){

                    $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

                    }, 300);
                }
                else
                {
                    $scope.availableData.AvailableKeys =  ""; 
                }

        });

    }



    $scope.getUser = function ()

    {
        var data = 'SessionKey='+SessionKey;
        $scope.Institute = [];
        $http.post(API_URL + 'institute/getInstitutes', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200 && response.Data.Records) { /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) {
                    $scope.Institute.push(response.Data.Records[i]);
                }
                
            }
        });
    }


    $scope.getBatchByCourse = function(CourseGUID){
        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;
        $scope.data.batch = [];
        $http.post(API_URL+'keys/getBatchByCourse', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */
                for (var i in response.Data) {
                    $scope.data.batch.push(response.Data[i]);
                }
                //$scope.data.batch.push(response.Data);

                console.log($scope.data.batch);
            }
        });
    }


    $scope.getCourses = function(){
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
        var data = 'SessionKey='+SessionKey+'&CategoryTypeName=Course';
        $scope.data.course = [];
        $http.post(API_URL+'students/getCourses', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */
                $scope.data.course = response.Data;
            }
        });
    }

    /*list*/

    $scope.applyFilter = function ()
    {

       // $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        $scope.UsedKeysDetail(1,$scope.Validity);

    }


    $scope.applyFilterStudents = function ()

    {

       // $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        $scope.getStudentsList(1,$scope.CourseGUID,$scope.BatchGUID);

    }



    $scope.getStudentsList = function (check="",CourseGUID="",BatchGUID="")
    { 
        // alert(1);   

         if(check!=1){
            if ($scope.data.listLoading || $scope.data.noRecords) return;
         }else{
            $scope.data.dataList = [];
            $scope.data.pageNo = 0;
         }

         $scope.CourseGUID = CourseGUID;
         $scope.BatchGUID = BatchGUID;
        

        $scope.data.listLoading = true;

        $scope.data.pageSize = 20;
        
        
        
        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID+'&BatchGUID='+BatchGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        console.log(data);
        $http.post(API_URL+'keys/getStudents', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    for (var i in response.Data) {
                       $scope.data.dataList.push(response.Data[i]);
                    }
                    $scope.data.pageNo++;
                }else{
                    $scope.data.noRecords = true;
                   // ////console.log(data.dataList.length);
                }
                        
                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);
        });
    }



    $scope.UnassignKey = function(Position,StudentGUID,Type){
        $scope.data.Position = Position;
        $scope.data.listLoading = true;
        if(StudentGUID.length){
            alertify.confirm('Are you sure you want to unassign the key?', function() {
                var data = 'SessionKey='+SessionKey+'&StudentGUID='+StudentGUID+'&Type='+Type;
               
                $http.post(API_URL+'keys/UnassignKey', data, contentType).then(function(response) {
                        var response = response.data;
                        if(response.ResponseCode==200){ /* success case */
                            SuccessPopup(response.Message);
                            $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                            $scope.data.totalRecords--;                               
                        }else{
                           SuccessPopup(response.Message);
                        }
                                
                        $scope.data.listLoading = false;   //console.log($scope.data.pageNo);
                });
            }).set('labels', {
                ok: 'Yes',
                cancel: 'No'
            });
        }else{
            ErrorPopup("Student is not available to unassign the key.");
        }
        $scope.data.listLoading = false;
    }




    $scope.UsedKeysDetail = function (check="",Validity="")
    { 
        // alert(1);   

         if(check!=1){
            if ($scope.data.listLoading || $scope.data.noRecords) return;
            var Validity = $scope.getUrlParameter('Validity');
            $scope.Validity = Validity;
         }else{
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
            $scope.Validity = Validity;
         }

         
        

        $scope.data.listLoading = true;

        $scope.data.pageSize = 20;
        
        
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize()+'&Validity='+$scope.Validity;
        console.log(data);
        $http.post(API_URL+'keys/UsedKeysDetail', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    for (var i in response.Data) {
                       $scope.data.dataList.push(response.Data[i]);
                    }
                    $scope.data.pageNo++;
                }else{
                    $scope.data.noRecords = true;
                   // ////console.log(data.dataList.length);
                }
                        
                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);
        });
    }





    /*list append*/

    $scope.getKeysStatisctics = function ()

    {
        //$scope.getUser();
        $scope.getSecondLevelPermission();

        if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.listLoading = true;

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'keys/getKeysStatisctics', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.dataList = response.Data.records;

                $scope.data.Allot_keys = response.Data.Allot_keys;
                
                $scope.data.TotalAvailableKeys = response.Data.TotalAvailableKeys;

                console.log($scope.data.Allot_keys);

                $scope.data.pageNo++;               

             }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }





    /*load add form*/

    $scope.loadFormAdd = function (Position, CategoryGUID)

    {

        $scope.templateURLAdd = PATH_TEMPLATE+'keys/add_form.htm?'+Math.random();

        $('#add_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

    }



    $scope.LoadUpdateKeys = function ()
    {
        $scope.templateURLUpdateKeys = PATH_TEMPLATE+'keys/update_alloted_keys.htm?'+Math.random();

        $('#edit_keys_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);
    }



    $scope.loadFormAllot = function ()

    {
        $scope.getUser();

        $scope.availableData.AvailableKeys = 0;

        $scope.templateURLAllot = PATH_TEMPLATE+'keys/allot_keys_form.htm?'+Math.random();

        $('#allot_key_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);
    }


    $scope.loadFranchiseeDefaultKey = function ()

    {
        $scope.getUser();

        $scope.availableData.AvailableKeys = 0;

        $scope.templateURLSetFranchiseeKeys = PATH_TEMPLATE+'keys/default_keys_form.htm?'+Math.random();

        $('#franchisee_default_keys_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);
    }


    /*add data*/

    $scope.addData = function ()

    {

        $scope.addDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'keys/add', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.getKeysStatisctics();

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }


    $scope.AssignKey = function(){
        $scope.assignDataLoading = true;        
        var UserGUID = $("input[name='UserGUID[]']:checkbox:checked").map(function() {
            if($(this).val()){
                return $(this).val();
            }
        }).get();

       // return false;

       // alert(UserGUID.length);

        if(UserGUID.length > 0){
            var Course = $("#Courses").val();
            var Batch = $("#Batch").val();
            var Validity = $("#Validity").val();
            var AvailableKeys = $("#availableKeys").val();
            //alert(Validity.length);
            if(Validity.length == 0){
                ErrorPopup("Please select keys validity to assign keys.");
                $scope.assignDataLoading = false;      
            }else if(AvailableKeys.length == 0){
                ErrorPopup("Keys are not available to assign keys.");
                $scope.assignDataLoading = false;      
            }else{


                $scope.addDataLoading = true;

                var data = 'SessionKey='+SessionKey+'&CategoryGUID='+Course+'&BatchGUID='+Batch+'&Validity='+Validity+'&AvailableKeys='+AvailableKeys+'&UserGUID='+JSON.stringify(UserGUID);
                //console.log(data); return false;
                $http.post(API_URL+'keys/assignKeysToStudent', data, contentType).then(function(response) {

                    var response = response.data;

                    if(response.ResponseCode==200){ /* success case */               

                        SuccessPopup(response.Message);

                        $scope.assignDataLoading = false;  

                        var availableKeys = $("#availableKeys").val();

                        var availableKeys = parseInt(availableKeys) - parseInt(response.Data);
                        if (availableKeys > 0 || availableKeys == 0) {
                           $("#availableKeys").val(availableKeys);
                            $(".availableKeys").val(availableKeys);
                        }



                        $scope.getStudentsList(1,$scope.CourseGUID,$scope.BatchGUID);

                        $timeout(function(){            

                           $('.modal-header .close').click();

                       }, 200);

                    }else{

                        ErrorPopup(response.Message);

                    }

                    $scope.assignDataLoading = false;      

                });
            }
        }else{
            ErrorPopup("Please select student to assign keys.");
            $scope.assignDataLoading = false;   
        }
       // $scope.assignDataLoading = false;   
    }


    $scope.allotKeys = function ()

    {

        $scope.addDataLoading = true;  //$scope.AvailableKeys

        var data = 'SessionKey='+SessionKey+'&AvailableKeys='+$scope.availableData.AvailableKeys+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'keys/allotKeys', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.getKeysStatisctics();

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }


    $scope.setFranchiseeDefaultKeys = function ()

    {

        $scope.addDataLoading = true;  //$scope.AvailableKeys

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'keys/setFranchiseeDefaultKeys', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }



    $scope.updateAllotedKeys = function ()

    {

        var TotalAvailableKeys = $("#TotalAvailableKeys").val();
        var TotalPrice = $("#TotalPrice").val();
        if(TotalPrice == TotalAvailableKeys){
            //$("input[name='sport']:checked")
            // var Validity = $("input[name='Validity']:checked")
            //     .map(function() {
            //         if($(this).val()){
            //             return $(this).val();
            //         }
            //     }).get();
            // console.log(Validity);
            // var OrderKeys = $("input[name='OrderKeys[]']")
            //     .map(function() {
            //         if($(this).val()){
            //             return $(this).val();
            //         }
            //     }).get();

            // var Price = $("input[name='Price[]']")
            //     .map(function() {
            //         if($(this).val()){
            //             return $(this).val();
            //         }
            //     }).get();

            // var TotalPrice = $("#TotalPrice").val();
            // var OrderGUID = "";
            // if($scope.OrderGUID){
            //     OrderGUID = $scope.formData.OrderGUID;
            // }
            var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

            $http.post(API_URL+'keys/updateAllotedKeys', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200){ /* success case */

                    SuccessPopup(response.Message);

                    $scope.data.pageLoading = false;

                    $scope.formData = response.Data;

                    $('.modal-header .close').click();

                    $scope.templateURLPayment = PATH_TEMPLATE+'keys/payment_form.htm?'+Math.random();

                    $('#add_payment').modal({show:true});

                    $timeout(function(){            

                       $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

                   }, 200);

                }else{

                    ErrorPopup(response.Message);
                }

            });

        }else{

            ErrorPopup('Total keys should be equal of available keys.');

        }    
    }

}); 









/* sortable - starts */

function tblsort() {



  var fixHelper = function(e, ui) {

    ui.children().each(function() {

        $(this).width($(this).width());

    });

    return ui;

}



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

        data: order,

        stop: function(response) {

            if (response.status == "success") {

                window.location.href = window.location.href;

            } else {

                alert('Some error occurred');

            }

        }

    });

}

}


function filterStudentsList(CourseGUID="",BatchGUID=""){ 

$("#BtnTotalStudent").text("Selected Students 0");   
    if(CourseGUID.length > 0){ //alert(CourseGUID.length);
        angular.element(document.getElementById('keys-body')).scope().getBatchByCourse(CourseGUID); 
    }
}

function filterUsedKeysList(CourseGUID){    
    angular.element(document.getElementById('keys-body')).scope().getBatchByCourse(CourseGUID); 
    angular.element(document.getElementById('keys-body')).scope().applyFilter(); 
}

function selectAllStudent(){ 
    if($('#checkAll').prop("checked") == false){
        $('.SelectStudent').prop('checked', false);
    }else{
        $('.SelectStudent').prop('checked', true);
    }
    var length = $('input.SelectStudent:checked').length; 
    $("#BtnTotalStudent").text("Selected Students "+length);
}


function  checkLength() {
    var length = $('input.SelectStudent:checked').length;
    $("#BtnTotalStudent").text("Selected Students "+length);
}





function updateTotalKeys(){
    var TotalAvailableKeys = $("#TotalAvailableKeys").val();

    var key3 = $("#keys3").val();
    if(!(key3.length)){
        key3 = 0;
    }
    var key6 = $("#keys6").val();
    if(!(key6.length)){
        key6 = 0;
    }
    var key12 = $("#keys12").val();
    if(!(key12.length)){
        key12 = 0;
    }
    var key24 = $("#keys24").val();
    if(!(key24.length)){
        key24 = 0;
    }
    console.log(key3+','+key6+','+key12+','+key24);
    var total = parseInt(key3)+parseInt(key6)+parseInt(key12)+parseInt(key24);
    if(TotalAvailableKeys < total){
        $("#TotalPrice").val(total);
        $(".TotalPrice").val(total);
        alert("Sorry! You have only "+TotalAvailableKeys+" keys are available.");
    }else{
        $("#TotalPrice").val(total);
        $(".TotalPrice").val(total);
    }    
}
/* sortable - ends */


function search_records()
{
    var CourseGUID = $("#Courses").val();

    var BatchGUID = $("#Batch").val();

    angular.element(document.getElementById('keys-body')).scope().getStudentsList(1,CourseGUID,BatchGUID);
}

function applyFilter(type)
{
    angular.element(document.getElementById('keys-body')).scope().applyFilter();
}


function clear_search_records()
{    
    $("#Courses, #Batch, #Keyword").val("");    

    $("#Batch").find("option:gt(0)").remove();

    search_records();
}