<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Pricing_model extends CI_Model

{

	public function __construct()

	{

		parent::__construct();	

	}





	function random_strings($length_of_string) { 

	  

	    // String of all alphanumeric character 

	    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 

	  

	    // Shufle the $str_result and returns substring 

	    // of specified length 

	    return substr(str_shuffle($str_result), 0, $length_of_string); 

	} 





	function addPricing($EntityID,$Input){

		$this->db->trans_start();	



		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);



		if($UserTypeID!=1){

			$EntityID = $this->Common_model->getInstituteByEntity($EntityID);

		}

		if($Input['Type'] == 'QSP'){
			$QSP = 1;
			$RSP = 0;
		}else if($Input['Type'] == 'RSP'){
			$QSP = 0;
			$RSP = 1;
		}



		$InsertData = array_filter(array(

			"Validity" 	=>	$Input['Validity'],

			"Price" 	=>	$Input['Price'],		

			"EntryDate" =>	date("Y-m-d H:i:s"),

			"QSP" => $QSP,

			"RSP" => $RSP

		));



		$this->db->insert('set_keys_price', $InsertData);

		$PriceID = $this->db->insert_id();

			

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)

		{

			return FALSE;

		}

		return $data['PriceID'] = $PriceID;

	}



	



	

	/*

	Description: 	Use to get pricing list

	*/

	function getPricing($EntityID,$Where="",$multiRecords=FALSE,  $PageNo=1, $PageSize=15){

		//print_r($Where);

		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);



		if($UserTypeID!=1){

			$EntityID = $this->Common_model->getInstituteByEntity($EntityID);

		}



		$this->db->select('*');

		$this->db->from('set_keys_price');



		if(!empty($Where['PriceID'])){

			$this->db->where('PriceID',$Where['PriceID']);

		}


		if(!empty($Where['type'])){

			$this->db->where($Where['type'],1);

		}

 
		//$this->db->where('InstituteID',$EntityID);



		$this->db->order_by("PriceID","ASC");	


		$Query = $this->db->get();

		//echo $this->db->last_query(); die;


		$Records = $arr = array();
		if($Query->num_rows()>0)
		{
			foreach ($Query->result_array() as $record) 
			{
				$arr[$record['Validity']][$Where['type']] = $record['Price'];
				$arr[$record['Validity']]['EntryDate'] = date("d-M-Y",strtotime($record['EntryDate']));	
			}

			foreach($arr as $k => $v)
			{
				$Records[] = array("Validity" => $k, "Price" => $v[$Where['type']], "EntryDate" => $v['EntryDate']);
			}
		}



		return $Records;

	}



}