<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionbank extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Questionbank_model');
		$this->load->model('Common_model');
		$this->load->library("excel");
	}



	/*
	Description: 	Use to get Import Data.
	URL: 			/api/Questionbank/importQuestions
	Input (Sample JSON): 		
	*/
	function escape_string($data)
	{
        $result = array();
        foreach($data as $row){
            $result[] = str_replace('"', '', $row);
        }
        return $result;
    }

    function removeCH($str)
	{
		$str = preg_replace('/[\x00-\x1F\x7F-\xFF]/', ' ', $str);
		$str = str_replace('"', " ", $str);
		$str = str_replace("'", " ", $str);		
		$str = str_replace('"', " ", $str);
		$str = str_replace("'", " ", $str);
		$str = str_replace('"', ' ', $str);
		$str = str_replace("'", ' ', $str);	

		$str = str_replace("NULL", 'NULLs', $str);
		$str = str_replace("Null", 'Nulls', $str);
		
		$str = addslashes($str);		
		$str = htmlentities($str, ENT_QUOTES | ENT_IGNORE, "UTF-8");				

		return $str;
	}

	public function importQuestions_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->validation($this);	
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$MediaURL = explode("/", $this->input->post('MediaURL'));
		$mediaName = end($MediaURL);

		
		if(!isset($this->Post['TopicID']) || empty($this->Post['TopicID'])) $this->Post['TopicID'] = 0;


		
		$csvFile = fopen("uploads/question_bank/question/".$mediaName, 'r');
		//echo "<pre>";

		$csvData = array();
        $i = 1; $qc = 0;
        while(($row = fgetcsv($csvFile, 1024)) !== FALSE)
        {
            if(isset($row) && !empty($row))
            {                           
                //echo "<pre>"; print_r($row);

                if(	(isset($row[0]) && !empty($row[0]) && ($row[0] == 1 || $row[0] == 2 || $row[0] == 3) ) && 
                	(isset($row[1]) && !empty($row[1])) && 
                	($row[1] == 1 || $row[1] == 2) 		&& 
                	(isset($row[2]) && !empty($row[2])) && 
                	(isset($row[3]) && !empty($row[3])) &&
                	(isset($row[4]) && !empty($row[4])) &&
                	(isset($row[5]) && !empty($row[5])) &&
                	(isset($row[8]) && !empty($row[8])) )                	
                {
                	
                	$QuestionsLevel = $CorrectAnswer = $QuestionsType = "";

                	$processIt = 0;


                	if($row[0] == 2)
					{
						$QuestionsLevel = "Moderate";
					}
					else if($row[0] == 3)
					{
						$QuestionsLevel = "High";
					}
					else
					{
						$QuestionsLevel = "Easy";
					}
						

					//Multiple Choice Question-------------------------
                	if($row[1] == 1 && 
                	isset($row[4]) && !empty($row[4]) &&
                	isset($row[5]) && !empty($row[5]) &&
                	isset($row[6]) && !empty($row[6]) &&
                	isset($row[7]) && !empty($row[7]) &&
                	($row[8] == 1 || $row[8] == 2 || $row[8] == 3 || $row[8] == 4)
                	)
                	{
                		$QuestionsType = "Multiple Choice";

                		$row[4] = $this->removeCH($row[4]);
                		$row[5] = $this->removeCH($row[5]);
                		$row[6] = $this->removeCH($row[6]);
                		$row[7] = $this->removeCH($row[7]);

                		$Answer = array($row[4], $row[5], $row[6], $row[7]);
                		if($row[8] == 1)
                		{
							$CorrectAnswer = $row[4];
						}
						else if($row[8] == 2)
						{
							$CorrectAnswer = $row[5];
						}
						else if($row[8] == 3)
						{
							$CorrectAnswer = $row[6];
						}
						else if($row[8] == 4)
						{
							$CorrectAnswer = $row[7];
						}

						$processIt = 1;

                	}
                	//Logical Question-------------------------------
                	elseif($row[1] == 2 && ($row[8] == 1 || $row[8] == 2))
                	{
                		$QuestionsType = "Logical Answer";
                		$Answer = array("True","False");

                		if($row[8] == 1)
                		{
							$CorrectAnswer = 'True';
						}
						elseif($row[8] == 2) 
						{
							$CorrectAnswer = 'False';
						}

                		$processIt = 1;
                	}

                	if($processIt == 1)
                	{
	                	/*echo "<br/><br/>..........................................................";
	                	echo "<br/><pre>";
	                	echo $row[3];*/

	                	$row[3] = $this->removeCH($row[3]);
                		
	                	/*echo "<br/><br/>";
	                	echo $row[3];*/
	                	

	                	$QAData = $this->Questionbank_model->addQA($this->SessionUserID, $this->EntityID, array(
							"SubjectID"	=>	$this->CategoryID,		
							"TopicID" 			=>	$this->Post['TopicID'],
							"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
							"QuestionsLevel"	=>	$QuestionsLevel,
							"QuestionsType"		=>	$QuestionsType,
							"QuestionsMarks"	=>	$row[2],
							"QuestionContent"	=>	$row[3],
							"AnswerContent"		=>	$Answer,
							"CorrectAnswer"		=>	$CorrectAnswer,
							"Explanation"		=>	$row[9]
						));

						if(isset($QAData) && !empty($QAData))
						{
							//echo "<br/>.......SAVED.........";
						}
						else
						{
							//echo "<br/>.......NOT SAVED.........";
						}

						$qc++;
	                }	

                }                
            }
        }

        // Close opened CSV file
        fclose($csvFile);		

		$this->Return['Message']      	=	$qc." Questions are added successfully.";
	}


	public function importQuestionsBK_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim|required', array("required" => "Upload file is required."));
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$MediaURL = explode("/", $this->input->post('MediaURL'));
		$mediaName = end($MediaURL);

		$questionData = $this->excel->excel_to_array("uploads/question_bank/question/".$mediaName);
		$arr_keys = array_keys($questionData[0]);
		
		//echo "<pre>"; print_r($questionData); die;

		$target_keys = array("Difficulty Level","Questions Type","Marks","Question Content","Choice 1 of Multiple Choice ","Choice 2 of Multiple Choice ","Choice 3 of Multiple Choice","Choice 4 of Multiple Choice","Correct Answer ","Explanation");
		
		
		foreach ($target_keys as $key => $value) 
		{			
			if (strpos($arr_keys[$key], $value) !== false) 
			{				
			}
			else
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	'Sorry! This file format is not correct. Download excel to get format to add students.';
				die;
			}
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$qc = 0;
		for ($i=0; $i < count($questionData); $i++) 
		{
			if($questionData[$i][$arr_keys[0]] == 1)
			{
				$QuestionsLevel = "Easy";
			}
			else if($questionData[$i][$arr_keys[0]] == 2)
			{
				$QuestionsLevel = "Moderate";
			}
			else if($questionData[$i][$arr_keys[0]] == 3)
			{
				$QuestionsLevel = "High";
			}


			$donot_process = 1;
			if($questionData[$i][$arr_keys[1]] == 1)
			{
				if($questionData[$i][$arr_keys[4]] == "" && $questionData[$i][$arr_keys[5]] == "" && $questionData[$i][$arr_keys[6]] == "" && $questionData[$i][$arr_keys[7]] == "")
				{
					$donot_process = 0;
				}
			}
			else if($questionData[$i][$arr_keys[1]] == 2)
			{
				if($questionData[$i][$arr_keys[8]] == "")
				{
					$donot_process = 0;
				}
			}
			else if($questionData[$i][$arr_keys[1]] == 3)
			{
				if($questionData[$i][$arr_keys[4]] == "")
				{
					$donot_process = 0;
				}
			}

			$questionData[$i][$arr_keys[3]] = trim($questionData[$i][$arr_keys[3]]);

			if($donot_process == 1 && $questionData[$i][$arr_keys[3]] != "")
			{
				
				if($questionData[$i][$arr_keys[1]] == 1)
				{
					$QuestionsType = "Multiple Choice";
					$Answer = array($questionData[$i][$arr_keys[4]],$questionData[$i][$arr_keys[5]],$questionData[$i][$arr_keys[6]],$questionData[$i][$arr_keys[7]]);
					$Answer = ($Answer);

					if($questionData[$i][$arr_keys[8]] == 1){
						$CorrectAnswer = $questionData[$i][$arr_keys[4]];
					}else if($questionData[$i][$arr_keys[8]] == 2){
						$CorrectAnswer = $questionData[$i][$arr_keys[5]];
					}else if($questionData[$i][$arr_keys[8]] == 3){
						$CorrectAnswer = $questionData[$i][$arr_keys[6]];
					}else if($questionData[$i][$arr_keys[8]] == 4){
						$CorrectAnswer = $questionData[$i][$arr_keys[7]];
					}

				}
				else if($questionData[$i][$arr_keys[1]] == 2)
				{
					$QuestionsType = "Logical Answer";
					$Answer = array("True","False");
					if($questionData[$i][$arr_keys[8]] == 1){
						$CorrectAnswer = 'true';
					}else if($questionData[$i][$arr_keys[8]] == 2){
						$CorrectAnswer = 'false';
					}
				}

				//echo "<pre>======================="; print_r($questionData[$i][$arr_keys[3]]);
				if($questionData[$i][$arr_keys[3]] != "")
				{
					$qc++;

					$QAData = $this->Questionbank_model->addQA($this->SessionUserID, $this->EntityID, array(
						"SubjectID"	=>	$this->CategoryID,		
						"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
						"QuestionsLevel"	=>	$QuestionsLevel,
						"QuestionsType"	=>	$QuestionsType,
						"QuestionsMarks"	=>	$questionData[$i][$arr_keys[2]],
						"QuestionContent"	=>	$questionData[$i][$arr_keys[3]],
						"AnswerContent"		=>	$Answer,
						"CorrectAnswer"		=>	$CorrectAnswer,
						"Explanation"		=>	$questionData[$i][$arr_keys[9]]
					));
				}	
			}	
		}
		

		$this->Return['Message']      	=	$qc." Questions are added successfully.";
	}


	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionBank/getQuestionBankStatistics
	Input (Sample JSON): 		
	*/
	public function getQuestionBankStatistics_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		if($this->input->post('SubjectGUID') != ""){
			$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}else if($this->input->post('CourseGUID') != ""){
			$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}

		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('TopicID') != "")
		{
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics($this->EntityID,'','',$this->input->post('TopicID'));
		}
		elseif($this->input->post('SubjectGUID') != "")
		{			
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics($this->EntityID,$this->CategoryID,'','');
		}
		elseif($this->input->post('CourseGUID') != "")
		{
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics($this->EntityID,'',$this->CategoryID,'');
		}
		else
		{
			$QuestionBankData = $this->Questionbank_model->getQuestionBankStatistics($this->EntityID,'','','');
		}
		
		if(!empty($QuestionBankData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $QuestionBankData;
		}	
	}


	/*
	Name: 			posts
	Description: 	Use to add new Question
	URL: 			/api/post	
	*/
	public function addQA_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUIDs', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('TopicID', 'Topic', 'trim');

		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('QuestionsLevel', 'Difficulty Level', 'required|in_list[Easy,Moderate,High]');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim|required|in_list[Multiple Choice,Logical Answer]');	
		$this->form_validation->set_rules('QuestionsMarks', 'Mark', 'trim|required');
		$this->form_validation->set_rules('QuestionContent', 'Enter Question', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim');
		
		
		if($this->input->post('QuestionsType') == 'Multiple Choice')
		{
			$this->form_validation->set_rules('AnswerContent[]', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}
		else if($this->input->post('QuestionsType') == 'Logical Answer')
		{
			$this->form_validation->set_rules('CorrectAnswers', 'Correct Answer', 'trim|required');
		}
		
		
		$this->form_validation->validation($this);  

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 


		if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent[]', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = $this->Post['AnswerContent'];
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswers', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswers'];
			$Answer = "";
		}

		
		if(!isset($this->Post['TopicID']) || empty($this->Post['TopicID'])) $this->Post['TopicID'] = 0;


		$QAData = $this->Questionbank_model->addQA($this->SessionUserID, $this->EntityID, array(
			"SubjectID"	=>	$this->CategoryID,
			"TopicID"	=>	$this->Post['TopicID'],		
			"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
			"QuestionsLevel"	=>	$this->Post['QuestionsLevel'],
			"QuestionsType"	=>	$this->Post['QuestionsType'],
			"QuestionsMarks"	=>	$this->Post['QuestionsMarks'],
			"QuestionContent"	=>	$this->Post['QuestionContent'],
			"AnswerContent"		=>	$Answer,
			"CorrectAnswer"		=>	$CorrectAnswer,
			"Explanation"		=>	@$this->input->Post('Explanation')
		));

		if($QAData){

			$InstituteID  =  $this->Common_model->getInstituteByEntity($this->EntityID);

			if($this->Post['MediaGUID']){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$this->Post['MediaGUID']));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $QAData['QtBankID']);
				}
			}
			

			$this->Return['Data']['QtBankGUID'] = $QAData['QtBankGUID'];
			$this->Return['Message'] = "Added successfully.";
		}
	}


	/*
	Name: 			posts
	Description: 	Use to add new post
	URL: 			/api/post	
	*/
	public function editQA_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->set_rules('CategoryGUIDs', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('TopicID', 'Topic', 'trim');

		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('QuestionsLevel', 'Difficulty Level', 'required|in_list[Easy,Moderate,High]');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim|required|in_list[Multiple Choice,Logical Answer,Short Answer]');	
		$this->form_validation->set_rules('QuestionsMarks', 'Mark', 'trim|required');
		$this->form_validation->set_rules('QuestionContent', 'Enter Question', 'trim|required');
		
		
		if($this->input->post('QuestionsType') == 'Multiple Choice')
		{
			$this->form_validation->set_rules('AnswerContent[]', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswers', 'Correct Answer', 'trim|required');
		}
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 


		if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent[]', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = $this->Post['AnswerContent'];
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswers', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswers'];
			$Answer = "";
		}

		if(!isset($this->Post['TopicID']) || empty($this->Post['TopicID'])) $this->Post['TopicID'] = 0;

		$QAData = $this->Questionbank_model->editQA($this->SessionUserID, $this->EntityID, array(
			"QtBankID"	=>	$this->QtBankID,
			"SubjectID"	=>	$this->CategoryID,
			"TopicID"	=>	$this->Post['TopicID'],
			"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
			"QuestionsLevel"	=>	$this->Post['QuestionsLevel'],
			"QuestionsType"	=>	$this->Post['QuestionsType'],
			"QuestionsMarks"	=>	$this->Post['QuestionsMarks'],
			"QuestionContent"	=>	$this->Post['QuestionContent'],
			"AnswerContent"		=>	$Answer,
			"CorrectAnswer"		=>	$CorrectAnswer,
			"Explanation"		=>	@$this->input->Post('Explanation')
		));

		if($QAData){

			if($this->Post['MediaGUID']){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $this->QtBankID);
				}
			}


			$QAData = $this->Questionbank_model->getQuestionAnswer($this->SessionUserID,$this->EntityID,$this->QtBankID,"","");

			$this->Return['Data'] = $QAData['Data'];
			$this->Return['Message'] = "Updated successfully.";

			//$this->Return['Data']['QtBankGUID'] = $this->input->post('QtBankGUID');

		}
	}



	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/getPosts
	*/
	public function getQuestions_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		
		if($this->input->post('TopicGUID') != "")
		{
			$this->form_validation->set_rules('TopicGUID', 'Topic', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}
		elseif($this->input->post('SubjectGUID') != "")
		{
			$this->form_validation->set_rules('SubjectGUID', 'Subject', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}		
		elseif($this->input->post('CourseGUID') != "")
		{
			$this->form_validation->set_rules('CourseGUID', 'Course', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}
		
		

		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'trim');
		$this->form_validation->set_rules('QuestionsLevel', 'Question Level', 'trim');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim');	
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CourseID = $SubjectID = $TopicID = "";
		if($this->input->post('TopicGUID') != "")
		{
			$TopicID = $this->CategoryID;
		}
		elseif($this->input->post('SubjectGUID') != "")
		{
			$SubjectID = $this->CategoryID;		
		}
		elseif($this->input->post('CourseGUID') != "")
		{
			$CourseID = $this->CategoryID;
		}
		
		//echo "SubjectID=$SubjectID===CourseID=$CourseID===TopicID=$TopicID";		


		$fields = array('QtBankID','QtBankGUID','CourseID','SubjectID','TopicID','QuestionContent','QuestionsGroup','QuestionsLevel','QuestionsMarks','QuestionsType');

		$Questions=$this->Questionbank_model->getQuestions($fields,array(
				'SessionUserID'	=>	$this->SessionUserID,
				'EntityID'		=>	(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),
				'CourseID'		=>  $CourseID,
				'SubjectID'		=>	$SubjectID,
				'TopicID'		=>	$TopicID,
				'QuestionsGroup' =>	@$this->Post['QuestionsGroup'],
				'QuestionsLevel' =>	@$this->Post['QuestionsLevel'],
				'QuestionsType' =>	@$this->Post['QuestionsType'],
				'Filter'		=>	@$this->Post['Filter'],
				'PostType'     =>	@$this->Post['PostType'],
				'Keyword'		=>	@$this->Post['Keyword']
			), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}



	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/getPosts
	*/
	public function getQuestionAnswer_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Questions=$this->Questionbank_model->getQuestionAnswer($this->SessionUserID,(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtBankID,$this->input->post('courseID'),$this->input->post('subjectID'),$this->input->post('view'));

		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}


	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/change entity status
	*/
	public function changeStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Questions=$this->Questionbank_model->changeStatus((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtBankID,$this->input->post('StatusID'));

		if($Questions){
			$QAData = $this->Questionbank_model->getQuestionAnswer((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtBankID,"","");

			$this->Return['Data'] = $QAData['Data'];
		}
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Post,QtBankID]');
		$this->form_validation->validation($this);  /* Run validation */	
		echo "fdsfds";	
		/* Validation - ends */
		if(!$this->Questionbank_model->deletePost($this->SessionUserID, $this->QtBankID)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}



	/*
	Name: 			posts
	Description: 	Use to add new post
	URL: 			/api/post	
	*/
	public function generateQuestionPaper_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'required');
		$this->form_validation->set_rules('QuestionsLevel', 'Difficulty Level', 'required|in_list[Easy,Moderate,High]');
		$this->form_validation->set_rules('QuestionsType', 'Question Type', 'trim|required|in_list[Multiple Choice,Logical Answer,Short Answer]');	
		$this->form_validation->set_rules('QuestionsMarks', 'Mark', 'trim|required');
		$this->form_validation->set_rules('QuestionContent', 'Enter Question', 'trim|required');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|callback_validateEntityGUID[Media]');
		
		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
		}
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 


		if($this->input->post('QuestionsType') == 'Short Answer'){
			$this->form_validation->set_rules('ShortAnswer', 'Short Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = 1;
			$Answer = $this->Post['ShortAnswer'];
		}else if($this->input->post('QuestionsType') == 'Multiple Choice'){
			$this->form_validation->set_rules('AnswerContent', 'Multiple Answer', 'trim|required');
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = $this->Post['AnswerContent'];
		}else if($this->input->post('QuestionsType') == 'Logical Answer'){
			$this->form_validation->set_rules('CorrectAnswer', 'Correct Answer', 'trim|required');
			$CorrectAnswer = $this->Post['CorrectAnswer'];
			$Answer = "";
		}



		$QAData = $this->Questionbank_model->addQA($this->SessionUserID, $this->EntityID, array(
			"SubjectID"	=>	$this->CategoryID,		
			"QuestionsGroup"	=>	implode(',',$this->Post['QuestionsGroup']),
			"QuestionsLevel"	=>	$this->Post['QuestionsLevel'],
			"QuestionsType"	=>	$this->Post['QuestionsType'],
			"QuestionsMarks"	=>	$this->Post['QuestionsMarks'],
			"QuestionContent"	=>	$this->Post['QuestionContent'],
			"AnswerContent"		=>	$Answer,
			"CorrectAnswer"		=>	$CorrectAnswer,
			"Explanation"		=>	@$this->input->Post('Explanation')
		));

		if($QAData){

			if($this->Post['MediaGUID']){
				$MediaData=$this->Media_model->getMedia('MediaID',array('MediaGUID'=>$MediaGUID));
				if ($MediaData){
					$this->Media_model->addMediaToEntity($MediaData['MediaID'], $this->SessionUserID, $QAData['QtBankID']);
				}
			}
			

			$this->Return['Data']['QtBankGUID'] = $QAData['QtBankGUID'];

		}
	}

}