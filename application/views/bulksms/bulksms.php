<!-- Head -->
<header class="panel-heading">
	<h1 class="h4">Bulk SMS</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" id="content-body" ng-controller="PageController" ng-init="getFilterData()"><!-- Body -->
	<div class="clearfix mt-2 mb-2">	  
	  <a class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" href="<?php echo base_url(); ?>bulksms/orders">My Orders</a>
	  <button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadBuyCredit()" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Buy Credits</button>
	  <span class="float-right" style="color: blue;margin: 10px;">Available Credits : {{SMSCredits.AvailableSMS}}</span>
	  <span class="float-right" style="color: blue;margin: 10px;">Used Credits : {{SMSCredits.UsedSMS}}</span>
	</div>

	<div class="form-area">

<!-- 		<div class="form-group">
			<div id="picture-box" class="picture-box banner">

				<img id="picture-box-picture" src="./asset/img/broadcast.png">


				<div class="picture-upload">
					<img src="./asset/img/upload.svg" id="picture-uploadBtn">
					<form enctype="multipart/form-data" action="../api/upload/image" method="post" name="picture_upload_form" id="picture_upload_form">
						<input type="hidden" name="Section" value="Broadcast">
						<input type="file" accept="image/*" name="File" id="fileInput" data-target="#picture-box #picture-box-picture" data-targetinput="#MediaGUIDs">
					</form>
				</div>

				<div class="progressBar">
					<div class="bar"></div>
					<div class="percent">0%</div>
				</div>
			</div>
		</div> -->


		<form id="add_form" name="add_form" autocomplete="off" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">
			
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-2">
					<div class="form-group">
						<h6>Send To:</h6>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<input name="SendTo" type="radio"  value="All" maxlength="40"  onchange="showStudents('users')" checked="checked">						
						<label class="control-label">All</label>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<input name="SendTo" type="radio"  value="Staff" maxlength="40" onchange="showStudents('Staff')">						
						<label class="control-label">Staff</label>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<input name="SendTo" type="radio"  value="Students" maxlength="40" onchange="showStudents('Students')">						
						<label class="control-label">Students</label>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6" id="show_staff" style="max-width:50%; display: none; border:1px solid #f7f7f7; padding:10px;">
					<div class="row" style="margin-top: 20px; margin-bottom: 20px;">		 
						<div class="col-md-6">
							<!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
							<select class="form-control" onchange="getStaffList(this.value)" name="Role" id="UserTypeID">
				               <option value="">Select Role</option>
				               <option ng-repeat="row in data.roles" value="{{row.UserTypeID}}">{{row.UserTypeName}}</option>
				            </select>
						</div>				
					</div>
					<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

					<table class="table table-striped table-condensed table-hover table-sortable"  ng-repeat="(key, row) in data.staffList">

						<!-- table heading -->

						<thead ng-if="row.heading == 1">

							<tr>

								<th style="width: 50%;"><input id="role{{row.UserTypeID}}" name="UserType" type="checkbox" ng-click="selectStaff(row.UserTypeID,'role')" value="All" maxlength="40" style="margin-right: 10px;" class="rolecheck">Role</th>

								<th style="width: 50%;"><!-- <input name="staff" id="staff{{row.UserTypeID}}" ng-click="selectStaff(row.UserTypeID,'staff')" type="checkbox"  value="All" maxlength="40" style="margin-right: 10px;"> -->Staff</th>

							</tr>

						</thead>

						<!-- table body -->

						<tbody id="tabledivbody">

							<tr scope="row">
								<!-- <tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.CategoryID}}"> -->

								<td class="listed sm text-left" style="width: 50%;">
									<div ng-if="row.UserTypeName">
										<!-- <input name="CategoryGUID[]" type="checkbox" class="course_details{{row.CategoryID}}"  value="{{row.CategoryGUID}}" maxlength="40" style="margin-right: 8px;margin-top: 5px;" ng-click="selectUser(row.CategoryID,'Batch')">	 -->					
										<label class="control-label">{{row.UserTypeName}}</label>
									</div>
								</td>



								<td class="listed sm text-left">

									<input name="UserGUID[]" type="checkbox" class="staff_details{{row.UserTypeID}} UserGUID"  value="{{row.UserGUID}}" maxlength="40" style="margin-right: 8px;margin-top: 5px;">						
									<label class="control-label">{{row.FullName}}</label>

								</td>

								

							</tr>

						</tbody>

					</table>
					
					<!-- no record -->
					<p class="no-records text-center" ng-if="!data.staffList.length">
						<span>No records found.</span>
					</p>
				</div>
				<div class="col-md-6" id="show_students" style="max-width:50%; display: none; border:1px solid #f7f7f7; padding:10px;">
					<div class="row" style="margin-top: 20px; margin-bottom: 20px;">		 
						<div class="col-md-6">
							<!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
							<select class="form-control" ng-model="CategoryGUID" ng-change="getFilterData(1,CategoryGUID)" name="ParentCat" id="Courses">
				               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
				               <option ng-repeat="row in categoryDataList" value="{{row.CategoryGUID}}" ng-selected="row.CategoryGUID == CourseGUID">{{row.CategoryName}}</option>
				            </select>
						</div>				
					</div>
					<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

					<table class="table table-striped table-condensed table-hover table-sortable"  ng-repeat="(key, row) in filterData">

						<!-- table heading -->

						<thead ng-if="row.heading == 1">

							<tr>

								<th style="width: 50%;"><input id="course{{row.CategoryID}}" name="Course" type="checkbox" ng-click="selectUser(row.CategoryID,'Course')" value="All" maxlength="40" style="margin-right: 10px;" class="coursecheck">Course</th>

								<th style="width: 50%;"><!-- <input name="Batch" id="batch{{row.CategoryID}}" ng-click="selectUser(row.CategoryID,'Batch')" type="checkbox"  value="All" maxlength="40" style="margin-right: 10px;"> -->Batch</th>

							</tr>

						</thead>

						<!-- table body -->

						<tbody id="tabledivbody">

							<tr scope="row">
								<!-- <tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.CategoryID}}"> -->

								<td class="listed sm text-left" style="width: 50%;">
									<div ng-if="row.CategoryName">
										<!-- <input name="CategoryGUID[]" type="checkbox" class="course_details{{row.CategoryID}}"  value="{{row.CategoryGUID}}" maxlength="40" style="margin-right: 8px;margin-top: 5px;" ng-click="selectUser(row.CategoryID,'Batch')">	 -->					
										<label class="control-label">{{row.CategoryName}}</label>
									</div>
								</td>



								<td class="listed sm text-left">

									<input name="BatchGUID[]" type="checkbox" class="batch_details{{row.CategoryID}} batch_details"  value="{{row.BatchGUID}}" maxlength="40" style="margin-right: 8px;margin-top: 5px;">						
									<label class="control-label">{{row.BatchName}}</label>

								</td>

								

							</tr>

						</tbody>

					</table>
					
					<!-- no record -->
					<p class="no-records text-center" ng-if="noRecords">
						<span>No records found.</span>
					</p>
				</div>
				<div class="col-md-6" style="max-width:50%; /*margin: auto;*/ border:1px solid #f7f7f7; padding:10px;">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Message<em style="color: red">*</em> </label>
							<textarea name="Message" class="form-control" style="height:80px" maxlength="153" ng-model="noofchars" ng-trim="false"></textarea>
							<label class="float-left"> Note : Max character count 153</label><span class="float-right">{{153 - noofchars.length}} left</span>  
						</div>
					</div>
					<div class="col-md-12" style="height: 100px;float: left;">
						<img src="asset/img/loader.svg" ng-if="addDataLoading" style="position: absolute;    top: 80%;    left: 50%;    transform: translate(-50%, -50%);">
						<button type="submit" class="btn btn-success btn-sm" ng-disabled="addDataLoading" ng-click="sendMessage()" style="float: right;">Send</button>
					</div>
				</div>
			<!-- <div class="row" style="max-width:500px; margin: auto; border:1px solid #f7f7f7; padding:10px;">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Title</label>
						<input name="Title" type="text" class="form-control" value="" maxlength="40">
					</div>
				</div>
			</div> -->


			</div>
		</form>

		<div class="modal fade" id="add_model">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">SMS Credits</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLAdd"></div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="add_model">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">SMS Credits</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLAdd"></div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="add_payment">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Make Payment</h3> 
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLPayment"></div>
				</div>
			</div>
		</div>
	</div>
</div><!-- Body/ -->