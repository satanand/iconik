<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questionpaper_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}	
 
 
	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getQuestionsPaperList($fields,$Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		$InstituteID = $this->Common_model->getInstituteByEntity($Where['EntityID']);
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select(implode(',', $fields));
		$this->db->from('tbl_question_paper');
		$this->db->join('tbl_entity', 'tbl_entity.EntityID = tbl_question_paper.QtPaperID');
		$this->db->where("tbl_entity.InstituteID",$InstituteID);

		if(!empty($Where['SubjectID'])){
			$this->db->where("SubjectID",$Where['SubjectID']);
		}else if(!empty($Where['CourseID'])){
			$this->db->where("CourseID",$Where['CourseID']);
		}

		
		if(isset($Where['PageName']) && !empty($Where['PageName']) && $Where['PageName'] == "QPList")
		{
			if(!empty($Where['QuestionsGroup']))
			{
				$this->db->where("(QuestionsGroup like '%".$Where['QuestionsGroup']."%')");
			}					
		}
		else
		{
			if(isset($Where['QuestionsGroup']) && !empty($Where['QuestionsGroup']))
			{
				$this->db->where("(QuestionsGroup like '%".$Where['QuestionsGroup']."%')");
			}
			else
			{
				$this->db->where("QuestionsGroup != 2");
			}			
		}
			

		if(!empty($Where['QuestionsLevel'])){
			if($Where['QuestionsLevel'] == 'Easy'){
				$this->db->where("QuestionsLevelEasy IS NOT NULL");
			}else if($Where['QuestionsLevel'] == 'Moderate'){
				$this->db->where("QuestionsLevelModerate IS NOT NULL");
			}else if($Where['QuestionsLevel'] == 'High'){
				$this->db->where("QuestionsLevelHigh IS NOT NULL");
			}			
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->group_start();			
				$this->db->or_like("tbl_question_paper.QtPaperTitle", $Where['Keyword']);
				$this->db->or_like("tbl_question_paper.TotalQuestions", $Where['Keyword']);
				$this->db->or_like("tbl_question_paper.QuestionsLevelEasy",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper.QuestionsLevelModerate",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper.QuestionsLevelHigh",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper.QuestionsType",$Where['Keyword']);
				$this->db->or_like("tbl_question_paper.PassingMarks",$Where['Keyword']);	
				$this->db->or_like("tbl_question_paper.NegativeMarks",$Where['Keyword']);		
			$this->db->group_end();
		}

		if(!empty($Where['QtPaperID']))	{
			$this->db->where("QtPaperID",$Where['QtPaperID']);
		}

		$this->db->order_by('QtPaperID','DESC');
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();			
			if($TempQ != ""){
				$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			}else{
				$Return['Data']['TotalRecords'] = 0;
			}
			
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		
		if($Query->num_rows() > 0){ 
			foreach($Query->result_array() as $Record){

				//echo 'SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"';

				$query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"');

				$result = $query->result_array();

				$Record['QuestionsGroup'] = $result[0]['QuestionGroupName'];

				$query = $this->db->query('SELECT ResultID FROM tbl_test_results where QtPaperID = "'.$Record['QtPaperID'].'"');

				$result = $query->result_array();

				if(!$query->num_rows() > 0){
					$Record['EditStatus'] = "No";
				}else{
					$Record['EditStatus'] = "Yes";
				}
				

				$Records[] = $Record;
			}			
			$Return['Data'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getQuestionsPaperByID($EntityID,$QtPaperID,$fields){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select(implode(',', $fields));
		$this->db->from('tbl_question_paper');
		$this->db->join('tbl_entity', 'tbl_entity.EntityID = tbl_question_paper.QtPaperID');
		$this->db->where('tbl_question_paper.QtPaperID',$QtPaperID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				$course = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['CourseID']."' limit 1");
				$data = $course->result_array()[0]; //print_r($data);
				$Record['CourseName'] = $data['CategoryName'];
				$Record['CourseGUID'] = $data['CategoryGUID'];

				$subject = $this->db->query("select CategoryName,CategoryGUID from set_categories where CategoryID = '".$Record['SubjectID']."' limit 1");
				$data1 = $subject->result_array()[0];
				$Record['SubjectName'] = $data1['CategoryName'];
				$Record['SubjectGUID'] = $data1['CategoryGUID'];

				$Record['QuestionsGroupID'] = $Record['QuestionsGroup'];

				//print_r($Record['QuestionsGroup']);

				$query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"');

				$result = $query->result_array();

				//print_r($result); die;

				$Record['QuestionsGroup'] = $result[0]['QuestionGroupName'];

				$Records[] = $Record;
			}
			$Return['Data'] = $Records;
			return $Return;
		}
		return FALSE;		
	}


	function getQuestionAnswerByID($EntityID,$QtBankID){
		$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  = ".$QtBankID);
		$QuestionsLevelEasy = $course->result_array(); 

		foreach ($QuestionsLevelEasy as $key => $value) {
			$QuestionsLevelEasy[$key]['QuestionContent'] = htmlspecialchars(trim(($value['QuestionContent'])));

			$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
			$QuestionsLevelEasy[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

			$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."' ORDER BY AnswerID");
			$answer = $answer->result_array();

			if($value['QuestionsType'] == 'Short Answer'){ 
				foreach ($answer as $ka => $va) {
					$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(($va['AnswerContent'])));
				}
			}

			$QuestionsLevelEasy[$key]['answer'] = $answer;
		}

		if(!empty($QuestionsLevelEasy)){
			return $QuestionsLevelEasy;
		}else{
			return FALSE;
		}
	}

	function removeCH($str)
	{
		$str = html_entity_decode($str);

		$str = preg_replace('/[\x00-\x1F\x7F-\xFF]/', ' ', $str);		

		$str = str_replace('"', " ", $str);
		$str = str_replace("'", " ", $str);
		
		$str = str_replace('"', " ", $str);
		$str = str_replace("'", " ", $str);

		$str = str_replace('"', ' ', $str);
		$str = str_replace("'", ' ', $str);

		$str = str_replace(":", " ", $str);

		$str = str_replace("NULL", 'NULLs', $str);
		$str = str_replace("Null", 'Nulls', $str);

		
		$str = stripslashes($str);


		return $str;
	}

	function getQuestionAnswerList($EntityID,$QtPaperID,$fields)
	{
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select(implode(',', $fields));
		$this->db->from('tbl_question_paper');
		$this->db->where('QtPaperID',$QtPaperID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$QuestionsLevelEasy = array();

					if($Record['QuestionsLevelEasy'] > 0)
					{
						if(!empty($Record['EasyLevelQuestionsID']))
						{
							//echo "select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['EasyLevelQuestionsID'],',').")";
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['EasyLevelQuestionsID'],',').")");
						}
						else
						{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.SubjectID = '".$Record['SubjectID']."' and QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%' and QB.QuestionsLevel = 'Easy' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2 ORDER BY rand() limit ".$Record['QuestionsLevelEasy']);
						}
						
						$QuestionsLevelEasy = $course->result_array(); 
						foreach ($QuestionsLevelEasy as $key => $value) 
						{
							
							$value['QuestionContent1'] = $value['QuestionContent'] = /*$this->removeCH*/($value['QuestionContent']);

							$QuestionsLevelEasy[$key]['QuestionContent'] = /*htmlspecialchars*/(trim(($value['QuestionContent'])));

							$QuestionsLevelEasy[$key]['QuestionContent1'] = /*htmlspecialchars*/(trim(($value['QuestionContent1'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$QuestionsLevelEasy[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."' ORDER BY AnswerID ");
							$answer = $answer->result_array();

							/*if($value['QuestionsType'] == 'Short Answer'){ 
								foreach ($answer as $ka => $va) {
									
									$va['AnswerContent'] = $this->removeCH($va['AnswerContent']);

									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(($va['AnswerContent'])));
								}
							}*/

							$QuestionsLevelEasy[$key]['answer'] = $answer;
						}

						$ids = array_column($QuestionsLevelEasy, 'QtBankID');

						if(!empty($ids)){
							$this->db->where('QtPaperID', $QtPaperID);
							$this->db->update('tbl_question_paper', array('EasyLevelQuestionsID' => implode(',', $ids)));
						}
					}

					$QuestionsLevelModerate = array();
					if($Record['QuestionsLevelModerate'] > 0){
						if(!empty($Record['ModerateLevelQuestionsID'])){
							//echo "select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['ModerateLevelQuestionsID'],',').")";
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['ModerateLevelQuestionsID'],',').")");
						}else{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.SubjectID = '".$Record['SubjectID']."' and QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%' and QB.QuestionsLevel = 'Moderate' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2  ORDER BY rand() limit ".$Record['QuestionsLevelModerate']);
						}
						$QuestionsLevelModerate = $course->result_array(); 
						foreach ($QuestionsLevelModerate as $key => $value) 
						{
							$value['QuestionContent1'] = $value['QuestionContent'] = /*$this->removeCH*/($value['QuestionContent']);

							$QuestionsLevelModerate[$key]['QuestionContent'] = /*htmlspecialchars*/(trim(($value['QuestionContent'])));

							$QuestionsLevelModerate[$key]['QuestionContent1'] = /*htmlspecialchars*/(trim(($value['QuestionContent1'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$QuestionsLevelModerate[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());


							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."'  ORDER BY AnswerID ");
							$answer = $answer->result_array();						

							/*if($value['QuestionsType'] == 'Short Answer'){ 
								foreach ($answer as $ka => $va) {
									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(($va['AnswerContent'])));
								}
							}*/

							$QuestionsLevelModerate[$key]['answer'] = $answer;
						}

						$ids = array_column($QuestionsLevelModerate, 'QtBankID');

						if(!empty($ids)){
							$this->db->where('QtPaperID', $QtPaperID);
							$this->db->update('tbl_question_paper', array('ModerateLevelQuestionsID' => implode(',', $ids)));
						}
					}

					$QuestionsLevelHigh = array();
					if($Record['QuestionsLevelHigh'] > 0){
						if(!empty($Record['HighLevelQuestionsID'])){
							//echo "select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['HighLevelQuestionsID'],',').")";
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['HighLevelQuestionsID'],',').")");
						}else{
							$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.SubjectID = '".$Record['SubjectID']."' and QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%'  and QB.QuestionsLevel = 'High' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2 ORDER BY rand() limit ".$Record['QuestionsLevelHigh']);
						}
						$QuestionsLevelHigh = $course->result_array(); 
						foreach ($QuestionsLevelHigh as $key => $value) 
						{
							$value['QuestionContent1'] = $value['QuestionContent'] = /*$this->removeCH*/($value['QuestionContent']);

							$QuestionsLevelHigh[$key]['QuestionContent'] = /*htmlspecialchars*/(trim(($value['QuestionContent'])));

							$QuestionsLevelHigh[$key]['QuestionContent1'] = /*htmlspecialchars*/(trim(($value['QuestionContent1'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$QuestionsLevelHigh[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."' ORDER BY AnswerID");
							$answer = $answer->result_array();						

							/*if($value['QuestionsType'] == 'Short Answer'){ 
								foreach ($answer as $ka => $va) 
								{
									$va['AnswerContent'] = $this->removeCH($va['AnswerContent']);
									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(($va['AnswerContent'])));
								}
							}*/

							$QuestionsLevelHigh[$key]['answer'] = $answer;
						}

						$ids = array_column($QuestionsLevelHigh, 'QtBankID');

						if(!empty($ids)){
							$this->db->where('QtPaperID', $QtPaperID);
							$this->db->update('tbl_question_paper', array('HighLevelQuestionsID' => implode(',', $ids)));
						}
					}

					$QA_data = array_merge($QuestionsLevelEasy,$QuestionsLevelModerate,$QuestionsLevelHigh);

					$ids = array_column($QA_data, 'QtBankID');

					if(!empty($ids)){
						$this->db->where('QtPaperID', $QtPaperID);
						$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $ids)));
					}
				//}

				$Record['question_answer'] = $QA_data;
				$Record['TotalQuestions'] = count($Record['question_answer']);
				$Record['Total_marks'] = array_sum(array_column($Record['question_answer'], 'QuestionsMarks'));
			}
			$Return['Data'] = $Record;
			return $Return;
		}
		return FALSE;		
	}



	function getQuestionAnswerList1($EntityID,$QtPaperID,$fields){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select(implode(',', $fields));
		$this->db->from('tbl_question_paper');
		$this->db->where('QtPaperID',$QtPaperID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				$QuestionsLevelEasy = array();

				$EntityID = $this->Common_model->getInstituteByEntity($EntityID);

				if(empty($Record['EasyLevelQuestionsID']) && empty($Record['ModerateLevelQuestionsID'])  && empty($Record['HighLevelQuestionsID'])){
					$Record['Total_marks'] = 0;
					$Return['Data'] = $Record;
					return $Return;
				}else{
					$this->db->select("QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType");
					$this->db->from("tbl_question_bank as QB");
					if(!empty($Record['EasyLevelQuestionsID'])){
						$this->db->where("QB.QtBankID  IN (".ltrim($Record['EasyLevelQuestionsID'],',').")");
					}

					if(!empty($Record['ModerateLevelQuestionsID'])){
						$this->db->where("QB.QtBankID  IN (".ltrim($Record['ModerateLevelQuestionsID'],',').")");
					}

					if(!empty($Record['HighLevelQuestionsID'])){
						$this->db->where("QB.QtBankID  IN (".ltrim($Record['HighLevelQuestionsID'],',').")");
					}

					$Query = $this->db->get();
					//echo $this->db->last_query();
					if($Query->num_rows() > 0){
						$Questions = $Query->result_array();
						foreach ($Questions as $key => $value) {
							$Questions[$key]['QuestionContent'] = htmlspecialchars(trim(strip_tags($value['QuestionContent'])));

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
							$Questions[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

							$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."' ORDER BY AnswerID");
							$answer = $answer->result_array();						

							if($value['QuestionsType'] == 'Short Answer'){ 
								foreach ($answer as $ka => $va) {
									$answer[$ka]['AnswerContent'] = htmlspecialchars(trim(strip_tags($va['AnswerContent'])));
								}
							}

							$Questions[$key]['answer'] = $answer;
						}
					}
					
					if(!empty($Questions)){
						$Record['question_answer'] = $Questions;
						$Record['TotalQuestions'] = count($Record['question_answer']);
						$Record['Total_marks'] = array_sum(array_column($Record['question_answer'], 'QuestionsMarks'));
					}

					$Return['Data'] = $Record;
					return $Return;
				}
			}			
		}
		return FALSE;		
	}

	/*
	Description: 	Use to get question paper stastics
	*/
	function getQuestionPaperStatistics($EntityID,$SubjectID="",$CourseID=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		if(!empty($CourseID)){
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2 and CategoryID='".$CourseID."'");
		}else{
			$query = $this->db->query("select CategoryID,CategoryName,CategoryGUID from set_categories join tbl_entity On tbl_entity.EntityID = set_categories.CategoryID where tbl_entity.InstituteID = ".$InstituteID." and CategoryTypeID = 2");
		}

		$course = $query->result_array();

		$result = array(); 
		$new_result = array(); 
		$check_array = array();
		$check = 1; 
		if(count($course) > 0){
			//print_r($course);
			$p = 0;
			foreach ($course as $key => $value) {

					if(!empty($SubjectID)){
						$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CategoryID']."' and CategoryID = '".$SubjectID."'");
					}else{
						$query = $this->db->query("select ParentCategoryID,CategoryID,CategoryName,CategoryGUID,CategoryTypeID from set_categories where CategoryTypeID = 3 and ParentCategoryID = '".$value['CategoryID']."'");
					}

					$SubCategoryData = $query->result_array();

					//print_r($SubCategoryData);

					$parentCategoryName = $value['CategoryName'];
					$parentCategoryGUID = $value['CategoryGUID'];
					$total_question = 0;

					foreach ($SubCategoryData as $k => $v) {

						if(!in_array($parentCategoryName, $check_array)){
							array_push($check_array, $parentCategoryName);
							$result[$parentCategoryName][$k]['ParentCategoryName'] = $parentCategoryName;
						}else{
							$result[$parentCategoryName][$k]['ParentCategoryName'] = "";	
						}
						$result[$parentCategoryName][$k]['ParentName'] = $parentCategoryName;
						$result[$parentCategoryName][$k]['parentCategoryGUID'] = $parentCategoryGUID;
						$result[$parentCategoryName][$k]['SubCategoryName'] = $v['CategoryName'];
						$result[$parentCategoryName][$k]['CategoryGUID'] = $v['CategoryGUID'];
						$result[$parentCategoryName][$k]['question_count'] = 0;

						$query = $this->db->query("SELECT count(qb.QtPaperID) as count_question FROM tbl_question_paper as qb where qb.SubjectID = ".$v['CategoryID']);

						if($query !== FALSE && $query->num_rows() > 0){
							$data = $query->result_array();

							$result[$parentCategoryName][$k]['question_count'] = $data[0]['count_question'];
							
							$total_question = $total_question+$data[0]['count_question'];

						}else{

							$result[$parentCategoryName][$k]['question_count'] = 0;
							
							//$total_question = $total_question+$data[0]['count_question'];
						}

						

						if(!empty($result[$parentCategoryName][$k])){
							array_push($new_result, $result[$parentCategoryName][$k]);
						}

						$count_sub = $k+1;
						if($count_sub == count($SubCategoryData)){ 	//echo $count_sub; echo "<br>";
							$result[$parentCategoryName][$count_sub]['question_count'] = $total_question;
							$result[$parentCategoryName][$count_sub]['ParentName'] = $parentCategoryName;
							$result[$parentCategoryName][$count_sub]['parentCategoryGUID'] = $parentCategoryGUID;
							$result[$parentCategoryName][$count_sub]['SubCategoryName'] = 'Total';
							$result[$parentCategoryName][$count_sub]['CategoryGUID'] = $v['CategoryGUID'];
							//print_r(expression)
							array_push($new_result, $result[$parentCategoryName][$count_sub]);
						}
					}
			}
			return $new_result;
		}
		return false;
	
	}


	/*
	Description: 	Use to add new post
	*/
	function createQusetionPaper($EntityID, $ToEntityID, $Input=array()){
		$this->db->trans_start();
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		// $EntityID_list = $this->Entity_model->getUserRelatedEntityList($EntityID);

		// if(!empty($EntityID_list)){
		// 	$query = $this->db->query("select QtPaper.QtPaperTitle from tbl_question_paper as QtPaper join tbl_entity ON tbl_entity.EntityID =  QtPaper.QtPaperID where tbl_entity.EntityTypeID = 11 and tbl_entity.CreatedByUserID IN (".implode(',', $EntityID_list).") and LCASE(QtPaper.QtPaperTitle) = '".strtolower($Input["QtPaperTitle"])."'");
		// }else{
		// 	$query = $this->db->query("select QtPaper.QtPaperTitle from tbl_question_paper as QtPaper join tbl_entity ON tbl_entity.EntityID =  QtPaper.QtPaperID where tbl_entity.EntityTypeID = 11 and tbl_entity.CreatedByUserID = ".$EntityID." and LCASE(QtPaper.QtPaperTitle) = '".strtolower($Input["QtPaperTitle"])."'");
		// }

		// echo $QtPaperTitle = $query->result_array();

		// if(!empty($QtPaperTitle) && count($QtPaperTitle) > 0){
		// 	return 'exist';
		// }else{
			$QtPaperGUID = get_guid();
			/* Add post to entity table and get EntityID. */
			$QtPaperID = $this->Entity_model->addEntity($QtPaperGUID, array(
				"EntityTypeID"	=>	11,
				"UserID"		=>	$EntityID,
				"Privacy"		=>	@$Input["Privacy"],
				"StatusID"		=>	2,
				"Rating"		=>	@$Input["Rating"]
			));

			$this->db->where('EntityID',$QtPaperID);
			$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));


			$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

			$this->db->select('ParentCategoryID');
	        $this->db->where('CategoryID',$Input["SubjectID"]);
	        $this->db->from('set_categories');
	        $query = $this->db->get();
	        //echo $this->db->last_query(); die;
	        if ( $query->num_rows() > 0 )
	        {
	            $row = $query->row_array();
	            $Input["CourseID"] = $row['ParentCategoryID'];

	            if(!empty($Input["StartDateTime"]) && !empty($Input["EndDateTime"])){	            	
	            	$Input["StartDateTime"] = date("Y-m-d H:i:s", strtotime($Input['StartDateTime']));
	            	$Input["EndDateTime"] = date("Y-m-d H:i:s", strtotime($Input['EndDateTime']));
	            }

	            $InsertData = array_filter(array(
					"QtPaperID" 		=>	$QtPaperID,
					"QtPaperGUID" 	=>	$QtPaperGUID,			
					"CourseID" =>	$Input["CourseID"],
					"SubjectID" 		=>	$Input["SubjectID"],
					"EntityID" 		=>	$EntityID,
					"QuestionsGroup" =>	$Input["QuestionsGroup"],
					"QtPaperTitle" => 	$Input["QtPaperTitle"],
					"TotalQuestions" =>	$Input["TotalQuestions"],
					"PassingMarks" =>	$Input["PassingMarks"],
					"QuestionsLevelEasy" => $Input["QuestionsLevelEasy"],
					"QuestionsLevelHigh"	=>	$Input["QuestionsLevelHigh"],
					"QuestionsLevelModerate"	=>	$Input["QuestionsLevelModerate"],
					"NegativeMarks"	=>	@$Input["NegativeMarks"],
					"Instruction"	=>	@$Input["Instruction"],
					"StartDateTime"	=>	@$Input["StartDateTime"],
					"EndDateTime"	=>	@$Input["EndDateTime"]
				));
				$this->db->insert('tbl_question_paper', $InsertData);

				//echo $this->db->last_query();
				//echo 
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return FALSE;
				}
				return array('QtPaperID' => $QtPaperID, 'QtPaperGUID' => $QtPaperGUID);
	        }else{
	        	return FALSE;
	        }
		//}
	}


	/*
	Description: 	Use to add new post
	*/
	function editQusetionPaper($EntityID, $ToEntityID, $Input=array()){
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($EntityID_list)){
			$query = $this->db->query("select QtPaper.QtPaperTitle from tbl_question_paper as QtPaper join tbl_entity ON tbl_entity.EntityID =  QtPaper.QtPaperID where tbl_entity.EntityTypeID = 11 and tbl_entity.InstituteID = ".$InstituteID." and LCASE(QtPaper.QtPaperTitle) = '".strtolower($Input["QtPaperTitle"])."' and QtPaper.QtPaperID != ".$Input["QtPaperID"]);
		}else{
			$query = $this->db->query("select QtPaper.QtPaperTitle from tbl_question_paper as QtPaper join tbl_entity ON tbl_entity.EntityID =  QtPaper.QtPaperID where tbl_entity.EntityTypeID = 11 and tbl_entity.InstituteID = ".$InstituteID." and LCASE(QtPaper.QtPaperTitle) = '".strtolower($Input["QtPaperTitle"])."'  and QtPaper.QtPaperID != ".$Input["QtPaperID"]);
		}

		$QtPaperTitle = $query->result_array();

		//print_r($QtPaperTitle); die;

		if(!empty($QtPaperTitle) && count($QtPaperTitle) > 0){
			//echo "sdsadasd"; die;
			return 'exist';
		}else{

			$this->db->select('ParentCategoryID');
	        $this->db->where('CategoryID',$Input["SubjectID"]);
	        $this->db->from('set_categories');
	        $query = $this->db->get();
	        //echo $this->db->last_query(); die;
	        if ( $query->num_rows() > 0 )
	        {
	            $row = $query->row_array();
	            $Input["CourseID"] = $row['ParentCategoryID'];

				/* Define variables - ends */
				$this->db->select('*');
				$this->db->from('tbl_question_paper');
				$this->db->where('QtPaperID',$Input["QtPaperID"]);
				$this->db->limit(1);
				$Query = $this->db->get();	
				//echo $this->db->last_query();
				if($Query->num_rows()>0){
					foreach($Query->result_array() as $Record){
						if($Record['CourseID'] != $Input["CourseID"]){
							$UpdateData = array_filter(array(		
								"CourseID" =>	$Input["CourseID"],
								"SubjectID" 		=>	$Input["SubjectID"],
								"EntityID" 		=>	$EntityID,
								"QuestionsGroup" =>	$Input["QuestionsGroup"],
								"QtPaperTitle" => 	$Input["QtPaperTitle"],
								"TotalQuestions" =>	$Input["TotalQuestions"],
								"PassingMarks" =>	$Input["PassingMarks"],
								"QuestionsLevelEasy" => $Input["QuestionsLevelEasy"],
								"QuestionsLevelHigh"	=>	$Input["QuestionsLevelHigh"],
								"QuestionsLevelModerate"	=>	$Input["QuestionsLevelModerate"],
								"EasyLevelQuestionsID" => NULL,
								"ModerateLevelQuestionsID"	=>	NULL,
								"HighLevelQuestionsID"	=>	NULL,
								"QuestionsID"	=>	NULL,
								"NegativeMarks"	=>	@$Input["NegativeMarks"],
								"Instruction"	=>	@$Input["Instruction"]
							));
						}else if($Record['SubjectID'] != $Input["SubjectID"]){
							$UpdateData = array_filter(array(		
								"CourseID" =>	$Input["CourseID"],
								"SubjectID" 		=>	$Input["SubjectID"],
								"EntityID" 		=>	$EntityID,
								"QuestionsGroup" =>	$Input["QuestionsGroup"],
								"QtPaperTitle" => 	$Input["QtPaperTitle"],
								"TotalQuestions" =>	$Input["TotalQuestions"],
								"PassingMarks" =>	$Input["PassingMarks"],
								"QuestionsLevelEasy" => $Input["QuestionsLevelEasy"],
								"QuestionsLevelHigh"	=>	$Input["QuestionsLevelHigh"],
								"QuestionsLevelModerate"	=>	$Input["QuestionsLevelModerate"],
								"EasyLevelQuestionsID" => NULL,
								"ModerateLevelQuestionsID"	=>	NULL,
								"HighLevelQuestionsID"	=>	NULL,
								"QuestionsID"	=>	NULL,
								"NegativeMarks"	=>	@$Input["NegativeMarks"],
								"Instruction"	=>	@$Input["Instruction"]
							));
						}else {
							$UpdateData = array(		
								"CourseID" =>	$Input["CourseID"],
								"SubjectID" 		=>	$Input["SubjectID"],
								"EntityID" 		=>	$EntityID,
								"QuestionsGroup" =>	$Input["QuestionsGroup"],
								"QtPaperTitle" => 	$Input["QtPaperTitle"],
								"TotalQuestions" =>	$Input["TotalQuestions"],
								"PassingMarks" =>	$Input["PassingMarks"],
								"QuestionsLevelEasy" => $Input["QuestionsLevelEasy"],
								"QuestionsLevelHigh"	=>	$Input["QuestionsLevelHigh"],
								"QuestionsLevelModerate"	=>	$Input["QuestionsLevelModerate"],
								"NegativeMarks"	=>	@$Input["NegativeMarks"],
								"Instruction"	=>	@$Input["Instruction"]
							);
							if($Record['QuestionsLevelEasy'] != $Input["QuestionsLevelEasy"]){
								if($Record['QuestionsLevelEasy'] > $Input["QuestionsLevelEasy"]){
									$remove = $Record['QuestionsLevelEasy'] - $Input["QuestionsLevelEasy"];
									$ids = explode(",", $Record['EasyLevelQuestionsID']);
									$length = count($ids);
									$ids = array_slice($ids, 0, $length-$remove);
									if(!empty($ids)){
										$UpdateData['EasyLevelQuestionsID'] = implode(',', $ids);
									}else{
										$UpdateData['EasyLevelQuestionsID'] = NULL;
									}
									
								}else{
									$limit = $Input['QuestionsLevelEasy'] - $Record["QuestionsLevelEasy"];
									$course = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.SubjectID = '".$Input['SubjectID']."' and QB.QuestionsGroup like '%".$Input['QuestionsGroup']."%' and QB.QuestionsLevel = 'Easy' and QB.QtBankID NOT IN ('".$Record['EasyLevelQuestionsID']."') and QB.StatusID = 2  ORDER BY rand() limit ".$limit);
									$ids = explode(",", $Record['EasyLevelQuestionsID']);
									$QuestionsLevelEasy = $course->result_array(); 
									foreach ($QuestionsLevelEasy as $key => $value) {
										array_push($ids, $value['QtBankID']);
									}
									if(!empty($ids)){
										$UpdateData['EasyLevelQuestionsID'] = implode(',', $ids);
									}else{
										$UpdateData['EasyLevelQuestionsID'] = NULL;
									}
								}
							}

							if($Record['QuestionsLevelHigh'] != $Input["QuestionsLevelHigh"]){
								if($Record['QuestionsLevelHigh'] > $Input["QuestionsLevelHigh"]){
									$remove = $Record['QuestionsLevelHigh'] - $Input["QuestionsLevelHigh"];
									$ids = explode(",", $Record['HighLevelQuestionsID']);
									$length = count($ids);
									$ids = array_slice($ids, 0, $length-$remove);
									if(!empty($ids)){
										$UpdateData['HighLevelQuestionsID'] = implode(',', $ids);
									}else{
										$UpdateData['HighLevelQuestionsID'] = NULL;
									}
								}else{
									$limit = $Input['QuestionsLevelHigh'] - $Record["QuestionsLevelHigh"];
									$course = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.SubjectID = '".$Input['SubjectID']."' and QB.QuestionsGroup like '%".$Input['QuestionsGroup']."%' and QB.QuestionsLevel = 'High' and QB.QtBankID NOT IN ('".$Record['HighLevelQuestionsID']."') and QB.StatusID = 2 ORDER BY rand() limit ".$limit);
									$ids = explode(",", $Record['HighLevelQuestionsID']);
									$QuestionsLevelHigh = $course->result_array(); 
									foreach ($QuestionsLevelHigh as $key => $value) {
										array_push($ids, $value['QtBankID']);
									}
									if(!empty($ids)){
										$UpdateData['HighLevelQuestionsID'] = implode(',', $ids);
									}else{
										$UpdateData['HighLevelQuestionsID'] = NULL;
									}
								}
							}

							if($Record['QuestionsLevelModerate'] != $Input["QuestionsLevelModerate"]){
								if($Record['QuestionsLevelModerate'] > $Input["QuestionsLevelModerate"]){
									$remove = $Record['QuestionsLevelModerate'] - $Input["QuestionsLevelModerate"];
									$ids = explode(",", $Record['ModerateLevelQuestionsID']);
									$length = count($ids);
									$ids = array_slice($ids, 0, $length-$remove);
									if(!empty($ids)){
										$UpdateData['ModerateLevelQuestionsID'] = implode(',', $ids);
									}else{
										$UpdateData['ModerateLevelQuestionsID'] = NULL;
									}
								}else{
									$limit = $Input['QuestionsLevelModerate'] - $Record["QuestionsLevelModerate"];
									$course = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.SubjectID = '".$Input['SubjectID']."' and QB.QuestionsGroup like '%".$Input['QuestionsGroup']."%' and QB.QuestionsLevel = 'Moderate' and QB.QtBankID NOT IN ('".$Record['ModerateLevelQuestionsID']."') and QB.StatusID = 2 ORDER BY rand() limit ".$limit);
									$ids = explode(",", $Record['ModerateLevelQuestionsID']);
									$QuestionsLevelModerate = $course->result_array(); 
									foreach ($QuestionsLevelModerate as $key => $value) {
										array_push($ids, $value['QtBankID']);
									}
									if(!empty($ids)){
										$UpdateData['ModerateLevelQuestionsID'] = implode(',', $ids);
									}else{
										$UpdateData['ModerateLevelQuestionsID'] = NULL;
									}
								}
							}
							
						}
					}
				}

				//print_r($Input); die;

	            

				//print_r($UpdateData); die;

				$this->db->where('QtPaperID', $Input["QtPaperID"]);
				$this->db->update('tbl_question_paper', $UpdateData);

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return FALSE;
				}
				return TRUE;
	        }else{
	        	return FALSE;
	        }
	    }
	}




	/*
	Description: 	Use to get count of question for generate question paper.
	Note:			$EntityID,$CategoryID very important. 
	*/
	function getQuestionsCountByCategory($EntityID,$CategoryID,$QuestionGroup=""){ //echo $CategoryID;
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		/* Define section  */
		$Return = array('Data' => array('Records' => array())); //print_r($QuestionGroup);


		$EntityID_list = $this->Entity_model->getUserRelatedEntityList($EntityID);
		$new_EntityID_list = array();
		foreach ($EntityID_list as $key => $value) {
			array_push($new_EntityID_list, $value['EntityID']);
		}


		//$EntityID_list = implode(',', $new_EntityID_list);

		///print_r($EntityID_list);

		if(!empty($new_EntityID_list)){
			$EntityID_list = implode(',', $new_EntityID_list);
			$query_part = "and tbl_entity.InstituteID = ".$InstituteID;
		}else{
			$query_part = "and tbl_entity.InstituteID = ".$InstituteID;
		}

		/* Define variables - ends */
		if(!empty($QuestionGroup)){
			//$QuestionGroup = json_decode($QuestionGroup);
			$data['Easy'] = 0; $data['Moderate'] = 0; $data['High'] = 0;
			//echo 'SELECT QuestionsGroup,QuestionsLevel FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%'.$QuestionGroup.'%" and tbl_entity.StatusID = 2 '.$query_part;
			$query = $this->db->query('SELECT QuestionsGroup,QuestionsLevel FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%'.$QuestionGroup.'%" and tbl_question_bank.StatusID = 2 '.$query_part);

			$result = $query->result_array();

			if(count($result) > 0){
				$data['Easy'] = 0; $data['Moderate'] = 0; $data['High'] = 0;
				foreach ($result as $k => $val) {
					$QuestionsGroup = explode(',', $val['QuestionsGroup']);
					//print_r($QuestionsGroup); die;
					if($val['QuestionsLevel'] == 'Easy'){
						$data['Easy'] = $data['Easy']+1;
					}

					if($val['QuestionsLevel'] == 'Moderate'){
						$data['Moderate'] = $data['Moderate']+1;
					}

					if($val['QuestionsLevel'] == 'High'){
						$data['High'] = $data['High']+1;
					}
				}
			}
		}else{
			//echo 'SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "Easy" and tbl_entity.StatusID = 2 '.$query_part;

			$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "Easy" and tbl_question_bank.StatusID = 2 '.$query_part);

			$result = $query->result_array();
			//print_r($data);
			$data['Easy'] = $result[0]['QtBankID'];

			$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "Moderate" and tbl_question_bank.StatusID = 2 '.$query_part);

			$result = $query->result_array();
			$data['Moderate'] = $result[0]['QtBankID'];

			$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "High" and tbl_question_bank.StatusID = 2 '.$query_part);

			$result = $query->result_array();
			$data['High'] = $result[0]['QtBankID'];
		}
		
		$data['Total'] = $data['Easy']+$data['Moderate']+$data['High'];

		//echo "SELECT count(QtBankID) as QtBankID FROM tbl_question_bank where SubjectID = '".$CategoryID."' and QuestionsGroup like '%1%'";
		$query = $this->db->query("SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = '".$CategoryID."' and tbl_question_bank.QuestionsGroup like '%1%' and tbl_question_bank.StatusID = 2 ".$query_part);

		$result = $query->result_array(); //print_r($result);
		$data['MockTest'] = $result[0]['QtBankID'];


		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%2%" and tbl_question_bank.StatusID = 2  '.$query_part);

		$result = $query->result_array();
		$data['Quiz'] = $result[0]['QtBankID'];		


		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%3%" and tbl_question_bank.StatusID = 2  '.$query_part);

		$result = $query->result_array();
		$data['Test'] = $result[0]['QtBankID'];

		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%4%" and tbl_question_bank.StatusID = 2  '.$query_part);

		$result = $query->result_array();
		$data['Contest'] = $result[0]['QtBankID'];


		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%5%" and tbl_question_bank.StatusID = 2  '.$query_part);

		$result = $query->result_array();
		$data['Scholarship'] = $result[0]['QtBankID'];
		//print_r($data); die;
		
		//echo $this->db->last_query();
		if($data){
			return $Return['Data'] = $data;
		}else{
			return FALSE;	
		}
			
	}


	/*
	Description: 	Use to get count of question for generate question paper.
	Note:			$EntityID,$CategoryID very important. 
	*/
	function getQuestionsCountOnEdit($EntityID,$CategoryID,$QuestionGroup="",$QtPaperID){ //echo $CategoryID;
		/* Define section  */
		$Return = array('Data' => array('Records' => array())); //print_r($QuestionGroup);


		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(!empty($InstituteID)){
			$query_part = "and tbl_entity.InstituteID = ".$InstituteID;
		}else{
			$query_part = "and tbl_entity.InstituteID = ".$InstituteID;
		}

		/* Define variables - ends */
		if(!empty($QuestionGroup)){
			//$QuestionGroup = json_decode($QuestionGroup);
			$data['Easy'] = 0; $data['Moderate'] = 0; $data['High'] = 0;
			//echo 'SELECT QuestionsGroup,QuestionsLevel FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%'.$QuestionGroup.'%" and tbl_entity.StatusID = 2  and QtBankID NOT IN(select EasyLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.')  and QtBankID NOT IN(select ModerateLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.')  and QtBankID NOT IN(select HighLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.') '.$query_part;
			$query = $this->db->query('SELECT QuestionsGroup,QuestionsLevel FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%'.$QuestionGroup.'%" and tbl_question_bank.StatusID = 2  and QtBankID NOT IN(select EasyLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.')  and QtBankID NOT IN(select ModerateLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.')  and QtBankID NOT IN(select HighLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.') and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') '.$query_part);

			$result = $query->result_array();

			if(count($result) > 0){
				$data['Easy'] = 0; $data['Moderate'] = 0; $data['High'] = 0;
				foreach ($result as $k => $val) {
					$QuestionsGroup = explode(',', $val['QuestionsGroup']);
					//print_r($QuestionsGroup); die;
					if($val['QuestionsLevel'] == 'Easy'){
						$data['Easy'] = $data['Easy']+1;
					}

					if($val['QuestionsLevel'] == 'Moderate'){
						$data['Moderate'] = $data['Moderate']+1;
					}

					if($val['QuestionsLevel'] == 'High'){
						$data['High'] = $data['High']+1;
					}
				}
			}
		}else{
			$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "Easy" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN(select EasyLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.') and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') '.$query_part);

			$result = $query->result_array();
			//print_r($data);
			$data['Easy'] = $result[0]['QtBankID'];

			$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "Moderate" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN(select ModerateLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.') and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.')  '.$query_part);

			$result = $query->result_array();
			$data['Moderate'] = $result[0]['QtBankID'];

			$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsLevel = "High" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN(select HighLevelQuestionsID from tbl_question_paper where QtPaperID = '.$QtPaperID.') and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') '.$query_part);

			$result = $query->result_array();
			$data['High'] = $result[0]['QtBankID'];
		}
		
		$data['Total'] = $data['Easy']+$data['Moderate']+$data['High'];

		//echo "SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = '".$CategoryID."' and tbl_question_bank.QuestionsGroup like '%1%' and tbl_entity.StatusID = 2 and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') ".$query_part;
		$query = $this->db->query("SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = '".$CategoryID."' and tbl_question_bank.QuestionsGroup like '%1%' and tbl_question_bank.StatusID = 2 and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '".$QtPaperID."') ".$query_part);

		$result = $query->result_array(); //print_r($result);
		$data['MockTest'] = $result[0]['QtBankID'];


		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%2%" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.')  '.$query_part);

		$result = $query->result_array();
		$data['Quiz'] = $result[0]['QtBankID'];		


		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%3%" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') '.$query_part);

		$result = $query->result_array();
		$data['Test'] = $result[0]['QtBankID'];

		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%4%" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') '.$query_part);

		$result = $query->result_array();
		$data['Contest'] = $result[0]['QtBankID'];

		$query = $this->db->query('SELECT count(QtBankID) as QtBankID FROM tbl_question_bank join tbl_entity ON tbl_entity.EntityID = tbl_question_bank.QtBankID where tbl_question_bank.SubjectID = "'.$CategoryID.'" and tbl_question_bank.QuestionsGroup like "%5%" and tbl_question_bank.StatusID = 2 and QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = '.$QtPaperID.') '.$query_part);

		$result = $query->result_array();
		$data['Scholarship'] = $result[0]['QtBankID'];
		//print_r($data); die;
		
		//echo $this->db->last_query();
		if($data){
			return $Return['Data'] = $data;
		}else{
			return FALSE;	
		}
			
	}


	function addBlockQuestion($EntityID, $QtPaperID, $QtBankID, $addNext){
		$this->db->trans_start();
		$this->db->select('QuestionsLevel,QuestionsType,QuestionsGroup');
		$this->db->from('tbl_question_bank');
		$this->db->where('QtBankID',$QtBankID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			$question_info = $Query->result_array();
			//print_r($question_info); die;
			$this->db->select('QuestionsID,QuestionsGroup,TotalQuestions,QuestionsLevelEasy,QuestionsLevelModerate, QuestionsLevelHigh, 	EasyLevelQuestionsID, ModerateLevelQuestionsID, HighLevelQuestionsID');
			$this->db->from('tbl_question_paper');
			$this->db->where('QtPaperID',$QtPaperID);
			$this->db->limit(1);
			$Query = $this->db->get();	
				
			if($addNext == 'Yes'){
				if($Query->num_rows()>0){
					$questionPaper = $Query->result_array();
					//print_r($questionPaper);
					//if(!empty($questionPaper[0]['QuestionsID'])){
						$key_LevelQuestionsID = $question_info[0]['QuestionsLevel']."LevelQuestionsID"; 

						if(!empty($questionPaper[0][$key_LevelQuestionsID])){
							$questions = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.QuestionsLevel = '".$question_info[0]['QuestionsLevel']."' and QB.QuestionsGroup like '%".$questionPaper[0]['QuestionsGroup']."%' and  QB.QtBankID NOT IN (".ltrim($questionPaper[0][$key_LevelQuestionsID],',').") limit 1");
						}else{
							$questions = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.QuestionsLevel = '".$question_info[0]['QuestionsLevel']."' and QB.QuestionsGroup like '%".$questionPaper[0]['QuestionsGroup']."%' limit 1");
						}
						

						$QA_data = $questions->result_array(); 

						

						if(count($QA_data) > 0){
							$questionID = explode(',', $questionPaper[0]['QuestionsID']);

							if (($key = array_search($QtBankID, $questionID)) !== false) {
							    unset($questionID[$key]);
							}

							array_push($questionID,$QA_data[0]['QtBankID']);

							$key_LevelQuestionsID = $question_info[0]['QuestionsLevel']."LevelQuestionsID";

							if(!empty($questionPaper[0][$key_LevelQuestionsID])){
								$value_LevelQuestionsID = explode(',', $questionPaper[0][$key_LevelQuestionsID]);

								if (($key = array_search($QtBankID, $value_LevelQuestionsID)) !== false) {
								    unset($value_LevelQuestionsID[$key]);
								}

								array_push($value_LevelQuestionsID,$QA_data[0]['QtBankID']);
							}else{
								$value_LevelQuestionsID = array($QA_data[0]['QtBankID']);
							}

							

							if(!empty($questionID)){
								$this->db->where('QtPaperID', $QtPaperID);
								$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $questionID), $key_LevelQuestionsID => implode(',', $value_LevelQuestionsID)));
							}

							// $InsertData = array_filter(array(		
							// 	"QtPaperID" =>	$QtPaperID,
							// 	"QtBankID" 		=>	$QtBankID
							// ));

							// $this->db->insert('tbl_disable_questions', $InsertData);

							$this->db->trans_complete();
							if ($this->db->trans_status() === FALSE)
							{
								return FALSE;
							}

							return $data = $this->getQuestionAnswerByID($EntityID, $QA_data[0]['QtBankID']);
						}else{
							if($question_info[0]['QuestionsLevel'] == 'Easy'){
								$nextLevel1 = 'Moderate';
								$nextLevel2 = 'High';
							}else if($question_info[0]['QuestionsLevel'] == 'Moderate'){
								$nextLevel1 = 'High';
								$nextLevel2 = 'Easy';
							}else if($question_info[0]['QuestionsLevel'] == 'High'){
								$nextLevel1 = 'Moderate';
								$nextLevel2 = 'Easy';
							}

							$key_LevelQuestionsID = $nextLevel1."LevelQuestionsID"; 
							if(!empty($questionPaper[0][$key_LevelQuestionsID])){
								$questions = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.QuestionsLevel = '".$nextLevel1."' and QB.QuestionsGroup like '%".$questionPaper[0]['QuestionsGroup']."%' and  QB.QtBankID NOT IN (".ltrim($questionPaper[0][$key_LevelQuestionsID],',').") limit 1");
							}else{
								$questions = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.QuestionsLevel = '".$nextLevel1."' and QB.QuestionsGroup like '%".$questionPaper[0]['QuestionsGroup']."%' limit 1");
							}
							
							$QA_data = $questions->result_array(); 

							if(count($QA_data) > 0){
								return array('QtBankID'=>$QA_data[0]['QtBankID'], 'QtBankID'=>$QA_data[0]['QtBankID'], "message"=> $question_info[0]['QuestionsLevel'].' level Question is not available to add with this paper. Do you want to add '.$nextLevel1.' level question?','QuestionsLevel'=>$nextLevel1);
							}else{
								$key_LevelQuestionsID = $nextLevel2."LevelQuestionsID"; 
								if(!empty($questionPaper[0][$key_LevelQuestionsID])){
									$questions = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.QuestionsLevel = '".$nextLevel2."' and QB.QuestionsGroup like '%".$questionPaper[0]['QuestionsGroup']."%' and  QB.QtBankID NOT IN (".ltrim($questionPaper[0][$key_LevelQuestionsID],',').") limit 1");
								}else{
									$questions = $this->db->query("select QB.QtBankID from tbl_question_bank as QB where QB.QuestionsLevel = '".$nextLevel2."' and QB.QuestionsGroup like '%".$questionPaper[0]['QuestionsGroup']."%' limit 1");
								}
								
								$QA_data = $questions->result_array(); 

								if(count($QA_data) > 0){
									return array('QtBankID'=>$QA_data[0]['QtBankID'], "message"=> $question_info[0]['QuestionsLevel'].' level Question is not available to add with this paper. Do you want to add '.$nextLevel2.' level question?','QuestionsLevel'=>$nextLevel2);
								}else{
									return array("message"=> 'Sorry! Question are not available to replace this question.');
								}
							}
							
						}
						
					// }else{
					// 	return FALSE;
					// }
				}
			}else{
				if($Query->num_rows()>0){
					$questionPaper = $Query->result_array();

					$questionID = explode(',', $questionPaper[0]['QuestionsID']);
					if (($key = array_search($QtBankID, $questionID)) !== false) {
					    unset($questionID[$key]);
					}

					$TotalQuestions = $questionPaper[0]['TotalQuestions'] - 1;

					$QuestionsLevel_key = 'QuestionsLevel'.$question_info[0]['QuestionsLevel'];
					$QuestionsLevel_value = $questionPaper[0][$QuestionsLevel_key]-1;
					
					$key_LevelQuestionsID = $question_info[0]['QuestionsLevel']."LevelQuestionsID";
					
					$this->db->where('QtPaperID', $QtPaperID);

					if($QuestionsLevel_value > 0){
						if(!empty($questionPaper[0][$key_LevelQuestionsID])){
						$value_LevelQuestionsID = explode(',', $questionPaper[0][$key_LevelQuestionsID]);

						if (($key = array_search($QtBankID, $value_LevelQuestionsID)) !== false) {
						    unset($value_LevelQuestionsID[$key]);
						}

						}else{
							$value_LevelQuestionsID = NULL;
						}

						$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $questionID), "TotalQuestions" => $TotalQuestions, $QuestionsLevel_key => $QuestionsLevel_value, $key_LevelQuestionsID => implode(',', $value_LevelQuestionsID)));
					}else{
						$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $questionID), "TotalQuestions" => $TotalQuestions, $QuestionsLevel_key => $QuestionsLevel_value, $key_LevelQuestionsID => NULL));
					}
					
					

					$InsertData = array_filter(array(		
						"QtPaperID" =>	$QtPaperID,
						"QtBankID" 	=>	$QtBankID
					));

					$this->db->insert('tbl_question_rejected', $InsertData);

					$this->db->trans_complete();
					if ($this->db->trans_status() === FALSE)
					{
						return FALSE;
					}
					return TRUE;
				}
			}
		}else{
			return FALSE;
		}
	}


	function replaceQuestion($EntityID, $QtPaperID, $RemoveQtBankID, $NewQtBankID, $QuestionsLevel){
		$this->db->trans_start();
		$this->db->select('QuestionsLevel,QuestionsType,QuestionsGroup');
		$this->db->from('tbl_question_bank');
		$this->db->where('QtBankID',$RemoveQtBankID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			$question_info = $Query->result_array();
			//print_r($question_info); die;
			$this->db->select('QuestionsID,QuestionsGroup,TotalQuestions,QuestionsLevelEasy,QuestionsLevelModerate,QuestionsLevelHigh, 	EasyLevelQuestionsID, ModerateLevelQuestionsID, HighLevelQuestionsID');
			$this->db->from('tbl_question_paper');
			$this->db->where('QtPaperID',$QtPaperID);
			$this->db->limit(1);
			$Query = $this->db->get();
			
			if($Query->num_rows()>0){

				$questionPaper = $Query->result_array();

				$questionID = explode(',', $questionPaper[0]['QuestionsID']);

				if (($key = array_search($RemoveQtBankID, $questionID)) !== false) {
				    unset($questionID[$key]);
				}

				array_push($questionID,$NewQtBankID);

				$QuestionsLevel_key = 'QuestionsLevel'.$question_info[0]['QuestionsLevel'];
				$QuestionsLevel_value = $questionPaper[0][$QuestionsLevel_key]-1;

				$QuestionsLevel_key1 = 'QuestionsLevel'.$QuestionsLevel;
				if(!empty($questionPaper[0][$QuestionsLevel_key1])){
					$QuestionsLevel_value1 = $questionPaper[0][$QuestionsLevel_key1]+1;
				}else{
					$QuestionsLevel_value1 = 1;
				}
				

				$key_LevelQuestionsID = $question_info[0]['QuestionsLevel']."LevelQuestionsID";

				//print_r( array('QuestionsID' => implode(',', $questionID), $QuestionsLevel_key => $QuestionsLevel_value, $QuestionsLevel_key1 => $QuestionsLevel_value1));

				if(!empty($questionPaper[0][$key_LevelQuestionsID])){
					$value_LevelQuestionsID = explode(',', $questionPaper[0][$key_LevelQuestionsID]);

					//print_r($value_LevelQuestionsID);

					if (($key = array_search($RemoveQtBankID, $value_LevelQuestionsID)) !== false) {
					    unset($value_LevelQuestionsID[$key]);
					}

					if(empty($value_LevelQuestionsID)){
						$value_LevelQuestionsID = array();
					}
					
				}else{
					$value_LevelQuestionsID = array($NewQtBankID);
				}

				$key_LevelQuestionsID1 = $QuestionsLevel."LevelQuestionsID";

				if(!empty($questionPaper[0][$key_LevelQuestionsID1])){
					$value_LevelQuestionsID1 = explode(',', $questionPaper[0][$key_LevelQuestionsID1]);
					array_push($value_LevelQuestionsID1,$NewQtBankID);
				}else{
					$value_LevelQuestionsID1 = array();
					array_push($value_LevelQuestionsID1,$NewQtBankID);
				}

				

				

				//print_r(array('QuestionsID' => implode(',', $questionID), $QuestionsLevel_key => $QuestionsLevel_value, $QuestionsLevel_key1 => $QuestionsLevel_value1, $key_LevelQuestionsID => implode(',', $value_LevelQuestionsID), $key_LevelQuestionsID1 => implode(',', $value_LevelQuestionsID1))); die;
					
				$this->db->where('QtPaperID', $QtPaperID);
				$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $questionID), $QuestionsLevel_key => $QuestionsLevel_value, $QuestionsLevel_key1 => $QuestionsLevel_value1, $key_LevelQuestionsID => implode(',', $value_LevelQuestionsID), $key_LevelQuestionsID1 => implode(',', $value_LevelQuestionsID1)));
				//echo $this->db->last_query(); die;

				$InsertData = array_filter(array(		
					"QtPaperID" =>	$QtPaperID,
					"QtBankID" 		=>	$RemoveQtBankID
				));

				$this->db->insert('tbl_question_rejected', $InsertData);

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return FALSE;
				}
				return TRUE;
			}
			
		}else{
			return FALSE;
		}
	}


	function addAssignPaper($EntityID, $Input){
		//print_r($Input); die;
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$QtAssignGUID = get_guid();

		$AFT = strtotime($Input['ActivatedFrom']);
		$ATT = strtotime($Input['ActivatedTo']);
		
		$ST = strtotime($Input['StartTime']);
		$ET = strtotime($Input['EndTime']);

		
		if($AFT >= $ATT)
		{
			return array("exist","Activated From time should be less than of Activated To time.");
		}
		elseif($AFT > $ST || $ST > $ATT)
		{
			return array("exist","Start time should be between Activated From and To time.");
		}
		elseif($AFT > $ET || $ET > $ATT)
		{
			return array("exist","End time should be between Activated From and To time.");
		}
		elseif($ST >= $ET)
		{
			return array("exist","Start time should be less than of End time.");
		}
		elseif(strtotime($Input['StartTime']) < strtotime(date("H:i:s")) && strtotime(date("Y-m-d",strtotime($Input['Date']))) == strtotime(date("Y-m-d"))){
			return array("exist","Start time should be ahead of current time.");
		}else if(strtotime($Input['StartTime']) == strtotime($Input['EndTime'])){
			return array("exist","Start time and end time can not be same.");
		}else if(strtotime($Input['StartTime']) > strtotime($Input['EndTime'])){
			return array("exist","End time should be ahead of start time.");
		}else{
			
			

			// $query = $this->db->query("select TotalStudents from tbl_question_paper_assign join tbl_entity ON tbl_entity.EntityID = tbl_question_paper_assign.QtAssignID  where tbl_entity.InstituteID = '".$InstituteID."' and QtAssignBatch = '".$Input['QtAssignBatch']."' and QtPaperID = '".$Input['QtPaperID']."'");

			// echo "select TotalStudents from tbl_question_paper_assign join tbl_entity ON tbl_entity.EntityID = tbl_question_paper_assign.QtAssignID  where tbl_entity.InstituteID = '".$InstituteID."' and QtAssignBatch = '".$Input['QtAssignBatch']."' and QtPaperID = '".$Input['QtPaperID']."'"; //die;

			$this->db->select("TotalStudents");
			$this->db->from("tbl_question_paper_assign");
			$this->db->join("tbl_entity","tbl_entity.EntityID = tbl_question_paper_assign.QtAssignID");
			$this->db->where("tbl_entity.InstituteID",$InstituteID);
			$this->db->where("tbl_question_paper_assign.QtAssignBatch",$Input['QtAssignBatch']);
			$this->db->where("tbl_question_paper_assign.QtPaperID",$Input['QtPaperID']);

			$QA = $this->db->get();

			// echo $this->db->last_query();

			
			// if(count($QA) > 0){
			// 	$QA = $QA->result_array();
			// 	if(empty($QA[0]['TotalStudents'])){
			// 		return array("exist","You have already assigned this paper to the same batch.");
			// 		die;
			// 	}
			// }

			$query = $this->db->query("select QtAssignID from tbl_question_paper_assign join tbl_entity ON tbl_entity.EntityID = tbl_question_paper_assign.QtAssignID  where tbl_entity.InstituteID = '".$InstituteID."' and QtAssignBatch = '".$Input['QtAssignBatch']."' and StartTime  BETWEEN '".date("H:i:s",strtotime($Input['StartTime']))."' AND '".date("H:i:s",strtotime($Input['EndTime']))."' and EndTime  BETWEEN '".date("H:i:s",strtotime($Input['StartTime']))."' AND '".date("H:i:s",strtotime($Input['EndTime']))."' and Date = '".date("Y-m-d",strtotime($Input['Date']))."'");

			$QA = $query->result_array();
			//echo $this->db->last_query();
			if(count($QA) > 0){
				return array("exist","This time duration is not available for this batch.");
			}else{
				$QtAssignID = $this->Entity_model->addEntity($QtAssignGUID, array(
					"EntityTypeID"	=>	12,
					"UserID"		=>	$EntityID,
					"Privacy"		=>	@$Input["Privacy"],
					"StatusID"		=>	2,
					"Rating"		=>	@$Input["Rating"]
				));

				$this->db->where('EntityID',$QtAssignID);
				$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

			    $InsertData = array_filter(array(		
					"QtAssignID" =>	$QtAssignID,
					"QtAssignGUID" 	=>	$QtAssignGUID,
					"QtPaperID" =>	$Input['QtPaperID'],
					"QtAssignBatch" =>	$Input['QtAssignBatch'],
					"Date"  => date("Y-m-d",strtotime($Input['Date'])),
					"StartTime" =>	date("H:i:s",strtotime($Input['StartTime'])),
					"EndTime" 	=>	date("H:i:s",strtotime($Input['EndTime'])),


					"ActivatedFrom" =>	date("H:i:s",strtotime($Input['ActivatedFrom'])),
					"ActivatedTo" 	=>	date("H:i:s",strtotime($Input['ActivatedTo']))
				));

				$this->db->insert('tbl_question_paper_assign', $InsertData);

				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE)
				{
					return array("exist","This time duration is not available for this batch.");
				}
				return array('QtAssignID' => $QtAssignID, 'QtAssignGUID' => $QtAssignGUID);
			}
		}
	}


	function changeStatus($EntityID, $QtPaperID, $StatusID){		
		return $status = $this->Entity_model->updateEntityInfo($QtPaperID, array(
			"StatusID"	=>	$StatusID
		));
	}


	/*
	Description: 	Use to delete post by owner
	*/
	function deletePost($UserID, $QtPaperID){
		$QuestionData=$this->getPosts('P.EntityID',array('QtPaperID'=>$QtPaperID, 'SessionUserID'=>$UserID));
		if(!empty($QuestionData) && $UserID==$QuestionData['EntityID']){
			$this->Entity_model->deleteEntity($QtPaperID);
			return TRUE;
		}
		return FALSE;
	}


	function removeQuestion($QuestionID,$QuestionPaperID="",$check=""){
		$this->db->select('QuestionsLevel');
		$this->db->from('tbl_question_bank');
		$this->db->where("QtBankID",$QuestionID);
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows()>0){
			$QuestionsLevel=$Query->result_array();
			$QuestionsLevel = $QuestionsLevel[0]['QuestionsLevel'];
		}

	
		$this->db->select('QtPaperID');
		$this->db->from('tbl_question_paper');
		$this->db->where('QuestionsID like "%'.$QuestionID.'%"');
		$this->db->where("StatusID",1);
		if($check!="All"){
			$this->db->where("QtPaperID",$QuestionPaperID);
		}
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			$QtPaperID = $Query->result_array(); //print_r($QtPaperID);
			$QtPaperID = array_column($QtPaperID, 'QtPaperID');
			//print_r($QtPaperID);
			$QtPaperID = implode(",", $QtPaperID);
		}

		if($check!="All"){
			//echo "UPDATE tbl_question_paper SET QuestionsLevel".$QuestionsLevel." = QuestionsLevel".$QuestionsLevel." - 1 , TotalQuestions = TotalQuestions - 1 , QuestionsID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', QuestionsID, ','), ',".$QuestionID.",', ',')), ".$QuestionsLevel."LevelQuestionsID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', ".$QuestionsLevel."LevelQuestionsID, ','), ',".$QuestionID.",', ',')) WHERE QtPaperID IN (".$QuestionPaperID.")";
			$this->db->insert('tbl_question_rejected',array('QtPaperID' => $QuestionPaperID,'QtBankID' => $QuestionID));
			$query = $this->db->query("UPDATE tbl_question_paper SET QuestionsLevel".$QuestionsLevel." = QuestionsLevel".$QuestionsLevel." - 1 , TotalQuestions = TotalQuestions - 1 , QuestionsID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', QuestionsID, ','), ',".$QuestionID.",', ',')), ".$QuestionsLevel."LevelQuestionsID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', ".$QuestionsLevel."LevelQuestionsID, ','), ',".$QuestionID.",', ',')) WHERE QtPaperID IN (".$QuestionPaperID.")");
		}else{

			$this->db->where('EntityID',$QuestionID);
			$this->db->update('tbl_entity',array('StatusID' => 1));
			$query = $this->db->query("UPDATE tbl_question_paper SET QuestionsLevel".$QuestionsLevel." = QuestionsLevel".$QuestionsLevel." - 1 , TotalQuestions = TotalQuestions - 1 , QuestionsID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', QuestionsID, ','), ',".$QuestionID.",', ',')), ".$QuestionsLevel."LevelQuestionsID = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', ".$QuestionsLevel."LevelQuestionsID, ','), ',".$QuestionID.",', ',')) WHERE QtPaperID IN (".$QtPaperID.")");
			$query = $this->db->query("UPDATE tbl_question_bank SET StatusID = 1 where QtBankID = ".$QuestionID." limit 1");
		}
		
		$afftectedRows = $this->db->affected_rows();

		if(!$afftectedRows){
			return FALSE;
		}else{
			return TRUE;
		}
		
	}


	function getSubjectByBatch($BatchGUID)
	{
		$this->db->select('sc.*');
		$this->db->from('set_categories sc');
		$this->db->join('tbl_batch B', 'B.CourseID = sc.ParentCategoryID', 'left');
		$this->db->join('tbl_entity E', 'E.EntityID = sc.CategoryID', 'left');
		$this->db->where('B.BatchGUID',$BatchGUID);
		$this->db->where('E.StatusID',2);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;
	}

	function getBatchByPaper($QtPaperGUID){
		$this->db->select('B.*');
		$this->db->from('tbl_batch B');
		$this->db->where('B.CourseID = (select CourseID from  tbl_question_paper where QtPaperGUID = "'.$QtPaperGUID.'")');
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;
	}


	function saveTestResults($EntityID,$QtPaperID,$ResultData,$QtAssignID)
	{
		$this->db->trans_start();
		$ResultGUID = get_guid();
		$insert = array_filter(array(
			"ResultGUID" => $ResultGUID,
			"StudentID" => $EntityID,
			"QuestionGroup" => $ResultData['QuestionGroup'],
			"QtPaperTitle" => $ResultData['QtPaperTitle'],			
			"QtPaperID" => $QtPaperID,
			"QtAssignID" => $QtAssignID,
			"TotalMarks" => $ResultData['TotalMarks'],
			"SkipQuestions" => $ResultData['SkipQuestions'],
			"AttemptQuestions" => $ResultData['AttemptQuestions'],
			"CorrectAnswers" => $ResultData['CorrectAnswers'],
			"WrongAnswers" => $ResultData['WrongAnswers'],
			"GainedMarks" => $ResultData['GainedMarks'],
			"ResultData" => $ResultData['ResultData'],
			"AttemptStartTime" => @$ResultData['AttemptStartTime'],
			"AttemptEndTime" => @$ResultData['AttemptEndTime'],
			"AttemptDate" => date("Y-m-d H:i:s")
		));
		//print_r($insert); die;
		$this->db->insert("tbl_test_results",$insert);
		$insertID = $this->db->insert_id();
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	


	function getTestResults($EntityID,$where="",$multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		
		if(!empty($where['CheckPaperData']) && $where['CheckPaperData'] == "Yes"){
			$this->db->select('tbl_question_paper.QtPaperTitle,tbl_test_results.*,tbl_question_paper.QuestionsGroup,tbl_users.FirstName,tbl_users.LastName,tbl_question_paper.TotalQuestions,tbl_question_paper.PassingMarks');
			$this->db->from('tbl_test_results');
			$this->db->join('tbl_question_paper',"tbl_question_paper.QtPaperID = tbl_test_results.QtPaperID");
			$this->db->join('tbl_users',"tbl_users.UserID = tbl_test_results.StudentID");
		}else{
			$this->db->select('tbl_users.FirstName,tbl_users.LastName,tbl_question_paper.QtPaperTitle,tbl_test_results.*,tbl_question_paper.QuestionsGroup');
			$this->db->from('tbl_test_results');
			$this->db->join('tbl_question_paper',"tbl_question_paper.QtPaperID = tbl_test_results.QtPaperID");
			$this->db->join('tbl_users',"tbl_users.UserID = tbl_test_results.StudentID");
			$this->db->where('StudentID',$EntityID);
		}
		
		
		if(!empty($where['QtPaperID'])){
			$this->db->where('tbl_test_results.QtPaperID',$where['QtPaperID']);
		}

		if(!empty($where['QtAssignID'])){
			$this->db->where('tbl_test_results.QtAssignID',$where['QtAssignID']);
		}

		if(!empty($where['QuestionsGroup'])){
			$this->db->where('tbl_question_paper.QuestionsGroup',$where['QuestionsGroup']);
		}


		if(!empty($where['ResultGUID'])){
			$this->db->where('tbl_test_results.ResultID',$where['ResultGUID']);
		}

		/*if(!empty($where['QtAssignID'])){
			$this->db->where('tbl_test_results.QtAssignID',$where['QtAssignID']);
		}*/

		$ReturnTemp = array();
		if(isset($where['QtPaperID']) && !empty($where['QtPaperID']))
		{			
			$QtPaperID = $where['QtPaperID'];
			$sql = "SELECT c.CategoryName as SubjectName, p.QtPaperTitle 
			FROM tbl_question_paper p 
			INNER JOIN set_categories c ON p.SubjectID = c.CategoryID AND c.CategoryTypeID = 3
			WHERE p.QtPaperID = '".$QtPaperID."' 
			LIMIT 1";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$ReturnTemp["SubjectName"] = $result[0]['SubjectName'];
			$ReturnTemp["QtPaperTitle"] = $result[0]['QtPaperTitle'];



			$sql = "SELECT a.StartTime, a.EndTime, b.BatchName
			FROM tbl_question_paper p 
			INNER JOIN tbl_question_paper_assign a ON a.QtPaperID = p.QtPaperID AND a.QtPaperID = '".$QtPaperID."'
			INNER JOIN tbl_batch b ON b.BatchID = a.QtAssignBatch
			WHERE p.QtPaperID = '".$QtPaperID."' 
			LIMIT 1";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			$ReturnTemp["StartTime"] = $result[0]['StartTime'];
			$ReturnTemp["EndTime"] = $result[0]['EndTime'];
			$ReturnTemp["BatchName"] = $result[0]['BatchName'];

			$TodayDate = date("Y-m-d");
			$etime = $TodayDate." ".$ReturnTemp["EndTime"];
			$stime = $TodayDate." ".$ReturnTemp["StartTime"];
			$diff = (strtotime($etime) - strtotime($stime));
			$ReturnTemp["TestDuration"] = ($diff / 60);			
		}	

		
		if(!empty($where['Keyword'])){
			$this->db->group_start();
			$this->db->like("tbl_users.FirstName", trim($where['Keyword']));
			$this->db->or_like("tbl_users.LastName", trim($where['Keyword']));
			$this->db->or_like("CONCAT_WS(' ',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($where['Keyword'])), FALSE);
			$this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.Middlename,tbl_users.LastName)", preg_replace('/\s+/', ' ', trim($where['Keyword'])), FALSE);
			if(is_numeric($where['Keyword'])){
				$this->db->or_like("tbl_test_results.AttemptDate",$where['Keyword']);
				$this->db->or_like("tbl_question_paper.TotalQuestions",$where['Keyword']);
				$this->db->or_like("tbl_test_results.AttemptQuestions",$where['Keyword']);
				$this->db->or_like("tbl_test_results.SkipQuestions",$where['Keyword']);	
				$this->db->or_like("tbl_test_results.WrongAnswers",$where['Keyword']);	
				$this->db->or_like("tbl_test_results.TotalMarks",$where['Keyword']);	
				$this->db->or_like("tbl_question_paper.PassingMarks",$where['Keyword']);	
				$this->db->or_like("tbl_test_results.GainedMarks",$where['Keyword']);
			}			
			$this->db->group_end();
		}

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();			
			if($TempQ != ""){
				$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			}else{
				$Return['Data']['TotalRecords'] = 0;
			}
			
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		$Records = array();
		if($Query->num_rows() > 0){ 
			foreach($Query->result_array() as $Record){

				$QtBankIDs = $Record['EasyLevelQuestionsID'].','.$Record['HighLevelQuestionsID'].','.$Record['ModerateLevelQuestionsID'].','.$Record['QuestionsID'];

				$QtBankIDs =  array_unique(explode(',', $QtBankIDs));

				//print_r($QtBankIDs); die;

				$QtBankIDs = implode(",", $QtBankIDs);

				$QtBankIDs = str_replace(',,', ',', $QtBankIDs);

				if(!empty($QtBankIDs)){
					$QuestionsMarks = $this->db->query("SELECT sum(qtb.QuestionsMarks) as count_marks FROM tbl_question_bank as qtb where qtb.QtBankID IN (".$QtBankIDs.")");

					//echo $QuestionsMarks->num_rows(); die;

					if($QuestionsMarks !== FALSE && $QuestionsMarks->num_rows()){
						$QuestionsMarks = $QuestionsMarks->result_array();

						//$Record['TotalMarks'] = $QuestionsMarks[0]['count_marks'];
					}else{
						//$Record['TotalMarks'] = 0;
					}
				}

				

				$Record['AttemptDate'] = date('d-m-Y H:i:s', strtotime($Record['AttemptDate']));


				$Record['ResultData'] = json_decode($Record['ResultData'], TRUE);
				//$Record['ResultData'] = array();
				foreach ($Record['ResultData'] as $k=>$v) 
				{
					$data = json_decode($v['is_answer']);
					$Record['ResultData'][$k]['GivenAnswerID'] = $data->AnswerID;

					if(isset($v['QuestionContent']))
					{
						$temp = $v['QuestionContent'];
						$v['QuestionContent'] = htmlspecialchars(trim(strip_tags($temp)));
						$Record['ResultData'][$k]['QuestionContent'] = $v['QuestionContent'];


						//$v['QuestionContent'] = (trim(($temp)));
						//$v['QuestionContent1'] = htmlspecialchars(trim(($temp)));
						//$v['QuestionContent2'] = htmlspecialchars(trim(strip_tags($temp)));					


						//$Record['ResultData'][$k]['QuestionContent'] = $v['QuestionContent'];
						//$Record['ResultData'][$k]['QuestionContent1'] = $v['QuestionContent1'];	
						//$Record['ResultData'][$k]['QuestionContent2'] = $v['QuestionContent2'];	
					}
				}

				// die;

				// if(!empty($Record['ResultData'][0]['is_answer'])){
				// 	$newString = $Record['ResultData'][0]['is_answer']; 
				// 	$data = json_decode($newString);
				// 	$Record['GivenAnswerID'] = $data->AnswerID;	
				// }else{
				// 	$Record['GivenAnswerID'] = $data->AnswerID;	
				// }
					

				// //echo 'SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"';

				// $query = $this->db->query('SELECT QuestionGroupName FROM tbl_question_group where QuestionGroupID = "'.$Record['QuestionsGroup'].'"');

				// $result = $query->result_array();

				// //print_r($result); die;

				// $Record['QuestionsGroup'] = $result[0]['QuestionGroupName'];
				//print_r($Record); die;
				
				$Record['CommonDetails'] = $ReturnTemp;

				$Records[] = $Record;


			}			
			
			
			$Return['Data'] = $Records;
			
			return $Return;
		}
		return FALSE;
	}



	function processPracticeTestForm_backup($EntityID, $Inputs)
	{
		$response_data = $result_data = $api_format = array();

		$response_data['QtAssignGUID'] = $Inputs['QtAssignGUID'];
		$response_data['QtPaperGUID'] = $TotalQuestions = $Inputs['QtPaperGUID'];
		$response_data['CourseID'] = $PassingMarks = $Inputs['CourseID'];
		$response_data['SubjectID'] = $NegativeMarks = $Inputs['SubjectID'];
		$response_data['QuestionsGroup'] = $TotalQuestions = $Inputs['QuestionsGroup'];
		$response_data['QuestionsID'] = $PassingMarks = $Inputs['QuestionsID'];
		$response_data['TotalQuestions'] = $TotalQuestions = $Inputs['TotalQuestions'];
		$response_data['PassingMarks'] = $PassingMarks = $Inputs['PassingMarks'];
		$response_data['NegativeMarks'] = $NegativeMarks = $Inputs['NegativeMarks'];

		$Questions = $Inputs['questions'];
		
		if(isset($Inputs['qpfq']) && !empty($Inputs['qpfq']))
		{
			foreach($Inputs['qpfq'] as $fk => $fq)
			{
				$result_data[$fk]['q'] = $fq;

				if(isset($Inputs["questions_marks_$fk"]))
				{
					$result_data[$fk]['m'] = $Inputs["questions_marks_$fk"];
				}	

				if(isset($Inputs["correct_answer_$fk"]))
				{
					$result_data[$fk]['c'] = $Inputs["correct_answer_$fk"];
				}
 
				if(isset($Inputs['qpfa'][$fk]) && !empty($Inputs['qpfa'][$fk]))
				{					
					$kk = 0; $your_ans = 0; $ans_arr = array();
					foreach($Inputs['qpfa'][$fk] as $fak => $fa)
					{						
						$is_correct = 0;
						if(isset($Inputs["correct_answer_$fk"]) && $Inputs["correct_answer_$fk"] == $fak)	
						{
							$is_correct = 1;	
						}
						
						/*if(isset($Inputs["answers_$fk"]) && !empty($Inputs["answers_$fk"]) && $Inputs["answers_$fk"] == $fak && (!isset($your_ans)) || $your_ans == 0)	
						{
							$ans_arr[$fk] = $your_ans = $kk;	
						}*/

						$result_data[$fk]['ans'][$kk] = array("id"=> $fak, "val" => $fa, "is_correct" => $is_correct);
						
						$kk++;	
					}

					//$result_data[$fk]['ans']['your_ans'] = $your_ans;
				}				
			}
		}


		//echo "<pre>"; print_r($result_data); die;



		$skipped = $attempt = $correct = $incorrect = $marks_gained = $total_marks = 0;
		if(isset($Questions) && !empty($Questions))
		{			
			$arr = array();
			foreach($Questions as $qid)
			{
				$marks = 0;
				if(isset($Inputs["questions_marks_$qid"]) && !empty($Inputs["questions_marks_$qid"]))
					$marks = $Inputs["questions_marks_$qid"];

				if(isset($Inputs["answers_$qid"]) && !empty($Inputs["answers_$qid"]))
				{
					$attempt++;

					if(isset($Inputs["correct_answer_$qid"]) && !empty($Inputs["correct_answer_$qid"]))
					{
						if($Inputs["correct_answer_$qid"] == $Inputs["answers_$qid"])
						{
							$correct++;

							$marks_gained = $marks_gained + $marks;

							$arr[$qid]['is_correct'] = 1;
						}
						else
						{
							$incorrect++;

							$marks_gained = $marks_gained + ($NegativeMarks);

							$arr[$qid]['is_correct'] = 0;
						}

						$result_data[$qid]['user_ans'] = $Inputs["answers_$qid"];
						$result_data[$qid]['is_correct'] = $arr[$qid]['is_correct'];
					}

					//$arr[$qid]['your_answer'] = $Inputs["answers_$qid"];
					//$arr[$qid]['correct_answer'] = $Inputs["correct_answer_$qid"];
					//$arr[$qid]['is_attempt'] = 1;

					$result_data[$qid]['is_attempt'] = 1;
				}
				else
				{
					$skipped++;
					//$arr[$qid]['is_attempt'] = 0;

					$result_data[$qid]['is_attempt'] = 0;
				}

				//$arr[$qid]['marks'] = $marks;
				$total_marks = $total_marks + $marks;
			}

			$response_data['Report'] = $result_data;
			$response_data['TotalMarks'] = $total_marks;
			$response_data['TotalQuestions'] = $TotalQuestions;
			$response_data['MarksGained'] = $marks_gained;
			$response_data['TotalAttempt'] = $attempt;
			$response_data['TotalSkipped'] = $skipped;
			$response_data['TotalCorrect'] = $correct;
			$response_data['TotalInCorrect'] = $incorrect;
		}

		

		$this->db->trans_start();
		$ResultGUID = get_guid();
		$insert = array(
			"ResultGUID" => $ResultGUID,
			"StudentID" => $EntityID,
			"QtPaperID" => $this->QtPaperID,
			"QtAssignID" => $this->QtAssignID,
			"TotalQuestions" => $TotalQuestions,
			"TotalMarks" => $response_data['TotalMarks'],
			"SkipQuestions" => $response_data['TotalSkipped'],
			"AttemptQuestions" => $response_data['TotalAttempt'],
			"CorrectAnswers" => $response_data['TotalCorrect'],
			"WrongAnswers" => $response_data['TotalInCorrect'],
			"GainedMarks" => $response_data['MarksGained'],
			"ResultData" => json_encode($response_data),
			"AttemptDate" => date("Y-m-d H:i:s")
		);
		
		
		$this->db->insert("tbl_test_results",$insert);
		
		$insertID = $this->db->insert_id();
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return 0;
		}

		return $ResultGUID;
		
	}

	function processPracticeTestForm($EntityID, $Inputs)
	{		
		$allQuestions = json_decode($Inputs['AllQuestion']);		
		//echo "<pre>"; print_r($allQuestions); die;
		$response_data = $result_data = $api_format = array();
		
		$TotalQuestions = count($allQuestions);
		$QtPaperTitle = $Inputs['QtPaperTitle'];
		$CourseID = $Inputs['CourseID'];
		$SubjectID = $Inputs['SubjectID'];
		$QuestionsGroup = $Inputs['QuestionsGroup'];		
		$PassingMarks = $Inputs['PassingMarks'];
		$NegativeMarks = $Inputs['NegativeMarks'];
				
		$skipped = $attempt = $correct = $incorrect = $marks_gained = $total_marks = 0;
		
		if(isset($allQuestions) && !empty($allQuestions))
		{			
			$arr = array();			
			foreach($allQuestions as $key => $obj)
			{
				$paperid = $obj-> QtBankID;
				$api_format[$key] = $allQuestions[$key];

				$QuestionsMarks = $obj-> QuestionsMarks;
				$total_marks = $total_marks + $QuestionsMarks;

				if(isset($Inputs["answers_".$paperid]) && $Inputs["answers_".$paperid] >= 0)
				{
					$attempt++;

					$is_found = 0;
					
					foreach($obj-> answer as $akey => $aobj)
					{
						if($akey == $Inputs["answers_".$paperid] && $aobj-> CorrectAnswer == 1)
						{
							$is_found = 1;
						}
					}

					if($is_found == 1)
					{
						$correct++;	
						$marks_gained = $marks_gained + $QuestionsMarks;
					}
					else
					{
						$incorrect++;
						$marks_gained = $marks_gained - $NegativeMarks;
					}

					$api_format[$key]-> is_answer = $obj-> answer[$Inputs["answers_$paperid"]];
					$api_format[$key]-> attempt = $Inputs["answers_$paperid"] + 1;
				}
				else
				{
					$skipped++;
					$api_format[$key]-> attempt = 0;
				}				
			}			
		}
		
		$this->db->trans_start();
		$ResultGUID = get_guid();
		$insert = array(
			"ResultGUID" => $ResultGUID,
			"StudentID" => $EntityID,
			"QtPaperID" => $this->QtPaperID,
			"QtAssignID" => $this->QtAssignID,

			"QuestionGroup" => $QuestionsGroup,
			"QtPaperTitle" => $QtPaperTitle,

			"TotalQuestions" => $TotalQuestions,
			"TotalMarks" => $total_marks,
			"SkipQuestions" => $skipped,
			"AttemptQuestions" => $attempt,
			"CorrectAnswers" => $correct,
			"WrongAnswers" => $incorrect,
			"GainedMarks" => $marks_gained,
			"ResultData" => json_encode($api_format),
			"AttemptDate" => date("Y-m-d H:i:s")
		);
		//echo "<pre>"; print_r($insert); die;
		
		$this->db->insert("tbl_test_results",$insert);
		
		$insertID = $this->db->insert_id();
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return false;
		}

		return $ResultGUID;
		
	}

	function getTestsResult($EntityID, $Inputs)
	{
		$data = array();

		$ResultID = $Inputs['ResultID'];

		$sql = "SELECT t.*  
		FROM tbl_test_results t
		INNER JOIN tbl_students s ON s.StudentID = t.StudentID AND s.StudentID = $EntityID		
		WHERE t.StudentID = $EntityID AND t.ResultGUID = '".$ResultID."'
		ORDER BY ResultID DESC
		LIMIT 1
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$data = $Where;				
			}

			if(isset($data['ResultData']))
			{
				$data['ResultData'] = json_decode($data['ResultData']);
			}

			return $data;
		}

		return FALSE;	
	}


	function getTestsHistoryLog($EntityID, $Inputs = array())
	{  
		$data = array();

		$QuestionGroup = 1;
		if(isset($Inputs['QuestionGroup']) && !empty($Inputs['QuestionGroup']))
		{
			$QuestionGroup = $Inputs['QuestionGroup'];
		}

		$QtPaperID = $this->QtPaperID;

		$sql = "SELECT t.ResultID, t.ResultGUID, t.QtPaperID, t.TotalMarks, t.SkipQuestions, t.AttemptQuestions,	t.CorrectAnswers, t.WrongAnswers, t.GainedMarks, DATE_FORMAT(t.AttemptDate, '%d-%M-%Y') as AttemptDate, t.QuestionGroup, t.QtPaperTitle
		FROM tbl_test_results t
		INNER JOIN tbl_students s ON s.StudentID = t.StudentID AND s.StudentID = $EntityID	
		INNER JOIN tbl_question_paper qp ON qp.QtPaperID = t.QtPaperID AND qp.QuestionsGroup = '$QuestionGroup'	AND qp.QtPaperID = '".$QtPaperID."'  
		WHERE t.StudentID = $EntityID AND t.QtPaperID = '".$QtPaperID."' 
		ORDER BY ResultID DESC		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$data[] = $Where;				
			}

			return $data;
		}

		return FALSE;	
	}


	function getScholarshipsApplicants($EntityID, $Inputs = array())
	{  
		$data = array();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$QuestionGroup = 5;
		
		$search = "";
		if(isset($Inputs['filterKeyword']) && !empty($Inputs['filterKeyword']))
		{
			$key = $Inputs['filterKeyword'];
			$search = " AND ( u.FirstName LIKE '$key%' OR u.LastName LIKE '$key%' OR u.Email LIKE '$key%' OR u.PhoneNumberForChange LIKE '$key%' OR qp.QtPaperTitle LIKE '$key%' OR a.PaidAmount LIKE '$key%') ";
		}

		if(isset($Inputs['filterFromDate']) && !empty($Inputs['filterFromDate']))
		{
			$fd = $Inputs['filterFromDate'];
			$fd = date("Y-m-d", strtotime($fd));
			$search .= " AND DATE(a.ApplyDate) >= '$fd' ";
		}

		if(isset($Inputs['filterToDate']) && !empty($Inputs['filterToDate']))
		{
			$td = $Inputs['filterToDate'];
			$td = date("Y-m-d", strtotime($td));
			$search .= " AND DATE(a.ApplyDate) <= '$td' ";
		}	


		$sql = "SELECT a.*, qp.QtPaperTitle, qp.StartDateTime, qp.EndDateTime, u.FirstName, u.LastName, u.Email, u.PhoneNumberForChange
		FROM tbl_scholarship_applicants a		
		INNER JOIN tbl_question_paper qp ON qp.QtPaperID = a.QtPaperID AND qp.QuestionsGroup = '$QuestionGroup'
		INNER JOIN tbl_users u ON a.StudentID = u.UserID
		INNER JOIN tbl_entity e ON e.EntityID = u.UserID AND e.InstituteID = $InstituteID	
		WHERE e.InstituteID = $InstituteID $search
		ORDER BY a.ApplyID ASC		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$data[] = $Where;				
			}

			return $data;
		}

		return FALSE;	
	}


	/*-----------------------------------------------------------------------
    Scholarship Module List
    -----------------------------------------------------------------------*/
	function getQuestionsCount($EntityID, $Inputs = array())
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$QuestionsGroup = $Inputs['QuestionsGroup'];
		$QtPaperID = $Inputs['QtPaperID'];

		$data = $tnm = array(); 
		
		$TNames = $this->getScholarshipList($EntityID, array(), $PageNo = 1, $PageSize = 1000);
		if(isset($TNames) && !empty($TNames))
		{
			foreach($TNames['Data'] as $arr)
			{
				$sel = "";
				if($QtPaperID == $arr['QtPaperID']) $sel = "selected";

				$tnm[] = array("QtPaperID" => $arr['QtPaperID'], 
					"QtPaperTitle" => ucfirst($arr['QtPaperTitle']),
					"Selected" => $sel 
				); 
			}
		}
		$data['TestNameList'] = $tnm;

		//Get Question Bank Counts----------------------------------------
		$sql = "SELECT COUNT(QtBankID) as Total, 'Easy' as LevelType
		FROM tbl_question_bank qp		
		INNER JOIN tbl_entity e ON e.EntityID = qp.EntityID AND e.InstituteID = $InstituteID	
		WHERE e.EntityID = $InstituteID AND qp.QuestionsLevel = 'Easy' AND FIND_IN_SET($QuestionsGroup,qp.QuestionsGroup) 
		GROUP BY QuestionsLevel

		UNION ALL

		SELECT COUNT(QtBankID) as Total, 'Moderate' as LevelType
		FROM tbl_question_bank qp		
		INNER JOIN tbl_entity e ON e.EntityID = qp.EntityID AND e.InstituteID = $InstituteID	
		WHERE e.EntityID = $InstituteID AND qp.QuestionsLevel = 'Moderate' AND FIND_IN_SET($QuestionsGroup,qp.QuestionsGroup) 
		GROUP BY QuestionsLevel

		UNION ALL

		SELECT COUNT(QtBankID) as Total, 'High' as LevelType
		FROM tbl_question_bank qp		
		INNER JOIN tbl_entity e ON e.EntityID = qp.EntityID AND e.InstituteID = $InstituteID	
		WHERE e.EntityID = $InstituteID AND qp.QuestionsLevel = 'High' AND FIND_IN_SET($QuestionsGroup,qp.QuestionsGroup) 
		GROUP BY QuestionsLevel
		";

		$temp = array('Easy' => 0, 'Moderate' => 0, 'High' => 0, 'TotalQuestions' => 0);
		
		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{
				if($Where['LevelType'] == 'Easy')
				{
					$temp['Easy'] = $Where['Total'];
				}
				elseif($Where['LevelType'] == 'Moderate')
				{
					$temp['Moderate'] = $Where['Total'];
				}
				elseif($Where['LevelType'] == 'High')
				{
					$temp['High'] = $Where['Total'];
				}
			}

			$temp['TotalQuestions'] = $temp['Easy'] + $temp['Moderate'] + $temp['High'];	
		}

		$data['Counts'] = $temp;	

		return $data;
	}

	function getScholarshipList($EntityID, $Inputs = array(), $PageNo = 1, $PageSize = 50)
	{  
		$data = array();

		$PageNo = ($PageNo - 1) * $PageSize;

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$QuestionGroup = 5;
		
		$search = "";
		if(isset($Inputs['filterKeyword']) && !empty($Inputs['filterKeyword']))
		{
			$key = $Inputs['filterKeyword'];
			$search = " AND ( qp.QtPaperTitle LIKE '%$key%'	) ";
		}

		if(isset($Inputs['filterFromDate']) && !empty($Inputs['filterFromDate']))
		{
			$fd = $Inputs['filterFromDate'];
			$fd = date("Y-m-d", strtotime($fd));
			$search .= " AND DATE(qp.ScheduleDate) >= '$fd' ";
		}

		if(isset($Inputs['filterToDate']) && !empty($Inputs['filterToDate']))
		{
			$td = $Inputs['filterToDate'];
			$td = date("Y-m-d", strtotime($td));
			$search .= " AND DATE(qp.ScheduleDate) <= '$td' ";
		}	


		$sql = "SELECT qp.QtPaperID, qp.QtPaperGUID, qp.QtPaperTitle, DATE_FORMAT(qp.ScheduleDate,'%d-%m-%Y') as ScheduleDate, qp.Duration, qp.TestActivatedFrom, qp.TestActivatedTo, qp.TestStartTime, qp.ScheduleType, qp.QuestionsLevelEasy, qp.QuestionsLevelModerate, qp.QuestionsLevelHigh, qp.PassingMarks, qp.NegativeMarks, qp.TotalQuestions, qp.QuestionsGroup, qp.Instruction 
		FROM tbl_question_paper qp		
		INNER JOIN tbl_entity e ON e.EntityID = qp.EntityID AND e.InstituteID = $InstituteID	
		WHERE e.InstituteID = $InstituteID AND qp.QuestionsGroup = '$QuestionGroup' AND DATE(qp.ScheduleDate) != '' AND qp.StatusID != 3 $search
		ORDER BY qp.QtPaperID DESC
		LIMIT $PageNo, $PageSize		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data['TotalRecords'] = $Query->num_rows();

			$temp = array();

			foreach($Query->result_array() as $Where)
			{				
				$Where['ApplicantsCount'] = $this->getScholarshipsApplicantsCount($EntityID, $Where['QtPaperID']);

				if($Where['ScheduleType'] == 2)
				{
					$Where['TestActivatedFrom'] = $Where['TestActivatedTo'] = "";
				}
				else
				{
					$Where['TestStartTime'] = "";
				}

				$Where['NegativeMarks'] = abs($Where['NegativeMarks']);

				$temp[] = $Where;				
			}

			$data['Data'] = $temp; 			

			return $data;
		}

		return FALSE;	
	}

	function getScholarshipsApplicantsCount($EntityID, $PaperID)
	{
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$sql = "SELECT count(app.ApplyID) as total
		FROM tbl_scholarship_applicants app	
		INNER JOIN tbl_question_paper qp ON app.QtPaperID = qp.QtPaperID AND qp.EntityID = $InstituteID	AND qp.StatusID != 3
		INNER JOIN tbl_users u ON u.UserID = app.StudentID
		WHERE qp.EntityID = $InstituteID AND app.Status IN ('Success', 'success') AND qp.QuestionsGroup = '5'
		AND app.QtPaperID = '$PaperID'
		GROUP BY app.QtPaperID		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data = $Query->result_array();

			return $data[0]['total'];
		}

		return 0;	
	}


	function ScholarshipTestSave($EntityID, $Input=array())
	{
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$QtPaperGUID = get_guid();
		
		$QtPaperID = $this->Entity_model->addEntity($QtPaperGUID, array(
			"EntityTypeID"	=>	11,
			"UserID"		=>	$EntityID,
			"Privacy"		=>	@$Input["Privacy"],
			"StatusID"		=>	2,
			"Rating"		=>	@$Input["Rating"]
		));

		$this->db->where('EntityID',$QtPaperID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		$Input["ScheduleDate"] = date("Y-m-d", strtotime($Input['ScheduleDate']));
		$Input["Duration"] = date("H:i", strtotime($Input['ScheduleDuration']));

		if($Input["ScheduleType"] == 1)
		{
			$Input["ActivatedFromTime"] = date("H:i:s", strtotime($Input['ActivatedFromTime']));
			$Input["ActivatedToTime"] = date("H:i:s", strtotime($Input['ActivatedToTime']));
			$Input["StartTime"] = "";
		}
		elseif($Input["ScheduleType"] == 2)
		{
			$Input["ActivatedFromTime"] = "";
			$Input["ActivatedToTime"] = "";
			$Input["StartTime"] = date("H:i:s", strtotime($Input['StartTime']));
		}	
		
	    
        $InsertData = (array(
			"QtPaperID" 		=>	$QtPaperID,
			"QtPaperGUID" 	=>	$QtPaperGUID,					
			"EntityID" 		=>	$InstituteID,
			"QuestionsGroup" =>	5,
			"QtPaperTitle" => 	$Input["SPaperTitle"],					
			"ScheduleDate"	=>	@$Input["ScheduleDate"],
			"TestActivatedFrom"	=>	@$Input["ActivatedFromTime"],
			"TestActivatedTo"	=>	@$Input["ActivatedToTime"],
			"TestStartTime"	=>	@$Input["StartTime"],			
			"Duration"	=>	@$Input["Duration"],
			"ScheduleType"	=>	@$Input["ScheduleType"]
		));
		$this->db->insert('tbl_question_paper', $InsertData);

				
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		
		return array('QtPaperID' => $QtPaperID, 'QtPaperGUID' => $QtPaperGUID);		
	}


	function ScholarshipTestSaveEdit($EntityID, $Input=array())
	{
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('QtPaperID');
		$this->db->from('tbl_question_paper');
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows()<=0)
		{
			return 'NotFound';
			die;
		}

		$this->db->select('ResultID');
		$this->db->from('tbl_test_results');		
		$this->db->where('QuestionGroup', 5);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows() > 0)
		{
			return 'TestGiven';
			die;
		}


		$Input["ScheduleDate"] = date("Y-m-d", strtotime($Input['ScheduleDate']));
		$Input["Duration"] = date("H:i", strtotime($Input['ScheduleDuration']));

		if($Input["ScheduleType"] == 1)
		{
			$ActivatedFromTime = strtotime($Input['ActivatedFromTime']);
			$ActivatedToTime = strtotime($Input['ActivatedToTime']);
			$StartTime = "";

			$Input["ActivatedFromTime"] = date("H:i:s", strtotime($Input['ActivatedFromTime']));
			$Input["ActivatedToTime"] = date("H:i:s", strtotime($Input['ActivatedToTime']));
			$Input["StartTime"] = "";
		}
		elseif($Input["ScheduleType"] == 2)
		{
			$ActivatedFromTime = "";
			$ActivatedToTime = "";
			$StartTime = strtotime($Input['StartTime']);			

			$Input["ActivatedFromTime"] = "";
			$Input["ActivatedToTime"] = "";
			$Input["StartTime"] = date("H:i:s", strtotime($Input['StartTime']));			
		}

        $UpdateData = (array(			
			"QtPaperTitle" => 	$Input["SPaperTitle"],			
			"ScheduleDate"	=>	@$Input["ScheduleDate"],
			"TestActivatedFrom"	=>	@$Input["ActivatedFromTime"],
			"TestActivatedTo"	=>	@$Input["ActivatedToTime"],
			"TestStartTime"	=>	@$Input["StartTime"],
			"TestEndTime"	=>	"",			
			"Duration"	=>	@$Input["Duration"],
			"ScheduleType"	=>	@$Input["ScheduleType"]
		));		
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->update('tbl_question_paper', $UpdateData);
		$this->db->limit(1);

		$SPaperTitle = $Input['SPaperTitle'];
		$ScheduleDate = strtotime($Input['ScheduleDate']);		
		$Duration = strtotime($Input['Duration']);	


		$HdnQtPaperTitle = $Input['HdnQtPaperTitle'];
		$HdnScheduleDate = strtotime($Input['HdnScheduleDate']);		
		$HdnScheduleDuration = strtotime($Input['HdnScheduleDuration']);
		$HdnActivatedFromTime = strtotime($Input['HdnActivatedFromTime']);
		$HdnActivatedToTime = strtotime($Input['HdnActivatedToTime']);
		$HdnStartTime = strtotime($Input['HdnStartTime']);

		$isUpdated = 0;
		if($SPaperTitle != $HdnQtPaperTitle || $ScheduleDate != $HdnScheduleDate || 
			$ActivatedFromTime != $HdnActivatedFromTime || 
			$ActivatedToTime != $HdnActivatedToTime || 
			$StartTime != $HdnStartTime || 
			$Duration != $HdnScheduleDuration)
		{
			$isUpdated = 1;
		}

		if($isUpdated == 1)
		{
			$InsertData = array(
			"CronFor" 		=>	'scholarship_test',
			"TargetIds" 	=>	$Input["QtPaperID"],					
			"SentType" 		=>	'sms',
			"InsertDate" 	=>	date("Y-m-d H:i:s"),
			"UpdateDate" 	=>	date("Y-m-d H:i:s")		
			);
			$this->db->insert('cronjobs', $InsertData);
		}


				
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		
		return array('QtPaperID' => $Input["QtPaperID"]);		
	}


	function getScholarshipApplicants($EntityID, $Inputs = array(), $PageNo = 1, $PageSize = 50)
	{  
		$data = array();

		$PageNo = ($PageNo - 1) * $PageSize;


		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$QuestionGroup = 5;
		
		$search = "";
		if(isset($Inputs['filterKeyword']) && !empty($Inputs['filterKeyword']))
		{
			$key = $Inputs['filterKeyword'];
			$search = " AND ( qp.QtPaperTitle LIKE '%$key%' OR
				u.FirstName LIKE '%$key%' OR
				u.LastName LIKE '%$key%' OR
				u.Email LIKE '%$key%' OR
				u.PhoneNumber LIKE '%$key%'
			) ";
		}

		if(isset($Inputs['filterFromDate']) && !empty($Inputs['filterFromDate']))
		{
			$fd = $Inputs['filterFromDate'];
			$fd = date("Y-m-d", strtotime($fd));
			$search .= " AND DATE(app.PaymentDate) >= '$fd' ";
		}

		if(isset($Inputs['filterToDate']) && !empty($Inputs['filterToDate']))
		{
			$td = $Inputs['filterToDate'];
			$td = date("Y-m-d", strtotime($td));
			$search .= " AND DATE(app.PaymentDate) <= '$td' ";
		}	


		$sql = "SELECT qp.QtPaperID, qp.QtPaperTitle, DATE_FORMAT(qp.ScheduleDate,'%d-%M-%Y') as ScheduleDate, qp.Duration, qp.TestStartTime, DATE_FORMAT(app.PaymentDate,'%d-%M-%Y %H:%i') as PaymentDate, u.FirstName, u.LastName, u.Email, u.PhoneNumber
		FROM tbl_scholarship_applicants app	
		INNER JOIN tbl_question_paper qp ON app.QtPaperID = qp.QtPaperID AND qp.EntityID = $InstituteID AND qp.StatusID != 3	
		INNER JOIN tbl_users u ON u.UserID = app.StudentID
		WHERE qp.EntityID = $InstituteID AND app.Status IN ('Success', 'success') AND qp.QuestionsGroup = '$QuestionGroup' $search
		ORDER BY qp.QtPaperTitle, qp.ScheduleDate, app.ApplyID DESC
		LIMIT $PageNo, $PageSize		
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data['TotalRecords'] = $Query->num_rows();

			$temp = array();

			foreach($Query->result_array() as $Where)
			{				
				$temp[] = $Where;				
			}

			$data['Data'] = $temp; 

			return $data;
		}

		return FALSE;	
	}


	function GeneratePaperSave($EntityID, $Input=array())
	{
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('QtPaperID');
		$this->db->from('tbl_question_paper');
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows()<=0)
		{
			return 'NotFound';
			die;
		}

		$this->db->select('ResultID');
		$this->db->from('tbl_test_results');		
		$this->db->where('QuestionGroup', $Input["QuestionsGroup"]);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows() > 0)
		{
			return 'TestGiven';
			die;
		}	

			

        $UpdateData = array(			
			"TotalQuestions" => 	@$Input["TotalQuestions"],
			"QuestionsLevelEasy" => 	@$Input["QuestionsLevelEasy"],
			"QuestionsLevelModerate" => 	@$Input["QuestionsLevelModerate"],
			"QuestionsLevelHigh" => 	@$Input["QuestionsLevelHigh"],
			"PassingMarks" => 	@$Input["PassingMarks"],
			"NegativeMarks" => 	@$Input["NegativeMarks"],
			"Instruction" => 	@$Input["Instruction"],

			"QuestionsID" => 	"",
			"EasyLevelQuestionsID" => 	"",
			"ModerateLevelQuestionsID" => 	"",
			"HighLevelQuestionsID" => 	""			
		);		

		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->update('tbl_question_paper', $UpdateData);
		$this->db->limit(1);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		$this->db->select('QtPaperGUID');
		$this->db->from('tbl_question_paper');
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $Input["QtPaperID"]);
		$this->db->limit(1);
		$Query = $this->db->get();

		$QtPaperGUID = "";
		if($Query->num_rows() > 0)
		{
			$result = $Query->result_array();
			
			$QtPaperGUID = $result[0]['QtPaperGUID'];
		}	
		
		return array('QtPaperID' => $Input["QtPaperID"], 'QtPaperGUID' => $QtPaperGUID);		
	}


	function GeneratePaperDelete($EntityID, $QtPaperID, $Input = array())
	{
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('QtPaperID');
		$this->db->from('tbl_question_paper');
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $QtPaperID);
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows()<=0)
		{
			return 'NotFound';
			die;
		}

		$this->db->select('ResultID');
		$this->db->from('tbl_test_results');		
		$this->db->where('QuestionGroup', $Input['QuestionsGroup']);
		$this->db->where('QtPaperID', $QtPaperID);
		$this->db->limit(1);
		$Query = $this->db->get();
		if($Query->num_rows() > 0)
		{
			return 'TestGiven';
			die;
		}


		$this->db->select('ApplyID');
		$this->db->from('tbl_scholarship_applicants');		
		$this->db->where('QtPaperID', $QtPaperID);
		$this->db->where('Status = "Success" OR Status = "success" ');
		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows() > 0)
		{
			return 'Applicants';
			die;
		}

        $UpdateData = array(			
			"StatusID" => 	3
		);		
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $QtPaperID);
		$this->db->where('QuestionsGroup', $Input['QuestionsGroup']);
		$this->db->update('tbl_question_paper', $UpdateData);
		$this->db->limit(1);

		
				
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		
		return array('QtPaperID' => $QtPaperID);		
	}


	function viewPaper($EntityID, $QtPaperID)
	{
		$Return = array('Data' => array('Records' => array()));
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('QtPaperID, QtPaperTitle, QuestionsGroup, TotalQuestions, QuestionsLevelEasy, QuestionsLevelModerate, QuestionsLevelHigh, PassingMarks, NegativeMarks, QuestionsID, EasyLevelQuestionsID, ModerateLevelQuestionsID, HighLevelQuestionsID, Instruction');
		$this->db->from('tbl_question_paper');
		$this->db->where('EntityID', $InstituteID);
		$this->db->where('QtPaperID', $QtPaperID);
		$this->db->limit(1);
		
		$Query = $this->db->get();			
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$QuestionsLevelEasy = array();

				if($Record['QuestionsLevelEasy'] > 0)
				{
					if(!empty($Record['EasyLevelQuestionsID']))
					{
						$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID IN (".ltrim($Record['EasyLevelQuestionsID'],',').")");
					}
					else
					{
						$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%' and QB.QuestionsLevel = 'Easy' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2 ORDER BY rand() limit ".$Record['QuestionsLevelEasy']);
					}
					
					$QuestionsLevelEasy = $course->result_array(); 
					foreach ($QuestionsLevelEasy as $key => $value) 
					{
						$value['QuestionContent1'] = $value['QuestionContent'] = $this->removeCH($value['QuestionContent']);

						$QuestionsLevelEasy[$key]['QuestionContent'] = htmlspecialchars(trim(($value['QuestionContent'])));

						$QuestionsLevelEasy[$key]['QuestionContent1'] = htmlspecialchars(trim(($value['QuestionContent1'])));

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
						$QuestionsLevelEasy[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

						$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."' ORDER BY AnswerID ");
						$answer = $answer->result_array();						

						$QuestionsLevelEasy[$key]['answer'] = $answer;
					}

					$ids = array_column($QuestionsLevelEasy, 'QtBankID');

					if(!empty($ids))
					{
						$this->db->where('QtPaperID', $QtPaperID);
						$this->db->update('tbl_question_paper', array('EasyLevelQuestionsID' => implode(',', $ids)));
					}
				}

				$QuestionsLevelModerate = array();
				if($Record['QuestionsLevelModerate'] > 0)
				{
					if(!empty($Record['ModerateLevelQuestionsID']))
					{
						$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['ModerateLevelQuestionsID'],',').")");
					}
					else
					{
						$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%' and QB.QuestionsLevel = 'Moderate' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2  ORDER BY rand() limit ".$Record['QuestionsLevelModerate']);
					}

					$QuestionsLevelModerate = $course->result_array(); 

					foreach ($QuestionsLevelModerate as $key => $value) 
					{
						$value['QuestionContent1'] = $value['QuestionContent'] = $this->removeCH($value['QuestionContent']);

						$QuestionsLevelModerate[$key]['QuestionContent'] = htmlspecialchars(trim(($value['QuestionContent'])));

						$QuestionsLevelModerate[$key]['QuestionContent1'] = htmlspecialchars(trim(($value['QuestionContent1'])));

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
						$QuestionsLevelModerate[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());


						$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."'  ORDER BY AnswerID ");
						$answer = $answer->result_array();

						$QuestionsLevelModerate[$key]['answer'] = $answer;
					}

					$ids = array_column($QuestionsLevelModerate, 'QtBankID');

					if(!empty($ids))
					{
						$this->db->where('QtPaperID', $QtPaperID);
						$this->db->update('tbl_question_paper', array('ModerateLevelQuestionsID' => implode(',', $ids)));
					}
				}

				$QuestionsLevelHigh = array();
				if($Record['QuestionsLevelHigh'] > 0)
				{
					if(!empty($Record['HighLevelQuestionsID']))
					{
						$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where QB.QtBankID  IN (".ltrim($Record['HighLevelQuestionsID'],',').")");
					}
					else
					{
						$course = $this->db->query("select QB.QtBankID,QB.QtBankGUID,QB.QuestionContent,QB.QuestionsMarks,QB.Explanation,QB.QuestionsType from tbl_question_bank as QB where  QB.QuestionsGroup like '%".$Record['QuestionsGroup']."%'  and QB.QuestionsLevel = 'High' and QB.QtBankID NOT IN (select QtBankID from tbl_question_rejected where QtPaperID = ".$QtPaperID.") and QB.StatusID = 2 ORDER BY rand() limit ".$Record['QuestionsLevelHigh']);
					}
					$QuestionsLevelHigh = $course->result_array(); 
					foreach ($QuestionsLevelHigh as $key => $value) 
					{
						$value['QuestionContent1'] = $value['QuestionContent'] = $this->removeCH($value['QuestionContent']);

						$QuestionsLevelHigh[$key]['QuestionContent'] = htmlspecialchars(trim(($value['QuestionContent'])));

						$QuestionsLevelHigh[$key]['QuestionContent1'] = htmlspecialchars(trim(($value['QuestionContent1'])));

						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'QuestionBank',"EntityID" => $value['QtBankID']),TRUE);
						$QuestionsLevelHigh[$key]['Media'] = ($MediaData ? $MediaData['Data'] : new stdClass());

						$answer = $this->db->query("select AnswerID, AnswerContent, CorrectAnswer from tbl_questions_answer where QtBankID = '".$value['QtBankID']."' ORDER BY AnswerID");
						
						$answer = $answer->result_array();

						$QuestionsLevelHigh[$key]['answer'] = $answer;
					}

					$ids = array_column($QuestionsLevelHigh, 'QtBankID');

					if(!empty($ids))
					{
						$this->db->where('QtPaperID', $QtPaperID);
						$this->db->update('tbl_question_paper', array('HighLevelQuestionsID' => implode(',', $ids)));
					}
				}

				$QA_data = array_merge($QuestionsLevelEasy,$QuestionsLevelModerate,$QuestionsLevelHigh);

				$ids = array_column($QA_data, 'QtBankID');

				if(!empty($ids))
				{
					$this->db->where('QtPaperID', $QtPaperID);
					$this->db->update('tbl_question_paper', array('QuestionsID' => implode(',', $ids)));
				}
				

				$Record['question_answer'] = $QA_data;
				$Record['TotalQuestions'] = count($Record['question_answer']);
				$Record['Total_marks'] = array_sum(array_column($Record['question_answer'], 'QuestionsMarks'));
			}

			$Return['Data'] = $Record;

			return $Return;
		}

		return FALSE;		
	}


}
