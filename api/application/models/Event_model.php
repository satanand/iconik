<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	} 


	/*
	Description: 	Use to add new category
	*/
	function addEvent($Input=array()){

        $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('Ev.EventID');
		$this->db->from('tbl_events Ev');
	    $this->db->join('tbl_entity E', 'E.EntityGUID = Ev.EventGUID', 'left');
	    $this->db->where('LOWER(`EventName`)="'.strtolower($Input['EventName']).'"');
	    $this->db->where('E.InstituteID',$InstituteID);

		$query = $this->db->get();
		if( $query->num_rows() > 0 )
	    {
	    	
	        return 'EXIST';
	        
	    }else{

		//$this->db->trans_start();
		$EventGUID = get_guid();


		$EventID=$this->Entity_model->addEntitys($EventGUID,array('UserID'=>$this->UserID,"EntityTypeID"=>22,'InstituteID'=>$InstituteID,"StatusID"=>2));
		$MediaID='';

		if(!empty($Input['MediaGUID'])){

		$this->db->select('MediaID');
		$this->db->from('tbl_media');
		$this->db->where('MediaGUID',$Input['MediaGUID']);
         $query = $this->db->get();
		 $data = $query->result_array();

		$MediaID=$data[0]['MediaID'];

		  $UpdateInstituted = array_filter(array(

				"InstituteID"=>$InstituteID
			));

			$this->db->where('MediaID', $MediaID);
			$data=$this->db->update('tbl_media', $UpdateInstituted);

		}

			/* Add Event */
	
			$InsertData = array_filter(array(
				"EventID"=>$EventID,
				"EventGUID"=>$EventGUID,
				"MediaID"=>$MediaID,
				"EventName"=>$Input['EventName'],
				"CategoryName"=>implode(',',$Input['Category']),
				"Address"=>$Input['Address'],
				"EventStartDateTime"=>date("Y-m-d H:i:s",strtotime($Input['EventStartDateTime'])),
				"EventEndDateTime"=>date("Y-m-d H:i:s",strtotime($Input['EventEndDateTime'])),
				"EventDescription"=>@$Input['EventDescription']
			));
			
			$EventID=$this->db->insert('tbl_events', $InsertData);
			//echo $this->db->last_query(); die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return $EventID;
		}
	}



	/*
	Description: 	Use to update Event.
	*/
	function editEvent($Input=array()){

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		if(!empty($Input['MediaGUID'])){

			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
	        $query = $this->db->get();

		   
		    $data = $query->result_array();

			$MediaID=$data[0]['MediaID'];

			$UpdateArray = array_filter(array(
	
					"EventName"=>@$Input['EventName'],
					"MediaID"=>$MediaID,
					"CategoryName"=>implode(',',$Input['Category']),
					"Address"=>@$Input['Address'],
					"EventStartDateTime"=>date("Y-m-d H:i:s",strtotime($Input['EventStartDateTime'])),
					"EventEndDateTime"=>date("Y-m-d H:i:s",strtotime($Input['EventEndDateTime'])),
					"EventDescription"=>@$Input['EventDescription'],
			));
			
			$UpdateInstituted = array_filter(array(

				"InstituteID"=>$InstituteID
			));

			$this->db->where('MediaID', $MediaID);
			$data=$this->db->update('tbl_media', $UpdateInstituted);
		}else{
			$UpdateArray = array_filter(array(
	
					"EventName"=>@$Input['EventName'],
					"CategoryName"=>implode(',',$Input['Category']),
					"Address"=>@$Input['Address'],
					"EventStartDateTime"=>date("Y-m-d H:i:s",strtotime($Input['EventStartDateTime'])),
					"EventEndDateTime"=>date("Y-m-d H:i:s",strtotime($Input['EventEndDateTime'])),
					"EventDescription"=>@$Input['EventDescription'],
			));
		}


		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('EventGUID', $Input['EventGUID']);
			//$this->db->limit(1);
			$data=$this->db->update('tbl_events', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		
		return TRUE;
	}



/*add category */
	function addcategory($data=array()){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->db->select('CategoryID');

	    $this->db->from('tbl_event_category');
	    
	    $this->db->where('LOWER(`Category`)="'.strtolower($data['Category']).'"');

         $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

			$InsertData = array_filter(array(

				"Category"  =>	$data['Category'],
				"InstituteID"  => $InstituteID,	
			));

			$this->db->insert('tbl_event_category', $InsertData);

		}
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}

	
		return TRUE;
	}	

   /*
	Description: 	Use to  DeteleMedia.
	*/
	function DeteleMedia($MediaGUID){

		if(!empty($MediaGUID)){

			$this->db->where('MediaGUID', $MediaGUID);
			$this->db->delete('tbl_media');
			
		}
		return TRUE;
	} 

	/*
	Description: 	Use to  DeteleMedia.
	*/
	function delete($EventGUID){
		if(!empty($EventGUID)){

			$UpdateData = array_filter(array(

				"StatusID"  =>1
				
			));

			$this->db->where('EventGUID', $EventGUID);

			$this->db->update('tbl_events',$UpdateData);

			// $this->db->where("EventID",$Input['EventID']);
			// $this->db->where("EventActivityID",$Input['EventActivityID']);
			// $this->db->from("tbl_event_activity");

			// $this->db->where("EventActivityID",$Input['EventActivityID']);
			// $this->db->from("tbl_event_activity_task");
			
		}
		return TRUE;
	}
	




	/*
	Description: 	Use to  get EventBy
	*/
	function getEventByID($EventGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->db->select("*,DATE_FORMAT(EventEndDateTime,'%d-%m-%Y %H:%s') EventEndDateTime,DATE_FORMAT(EventStartDateTime,'%d-%m-%Y %H:%s') EventStartDateTime",false);
		$this->db->from('tbl_events b');
		//$this->db->join('tbl_event_category c','b.Category=c.ID','left');
		//$this->db->join('tbl_media m','m.MediaID=b.MediaID');
		if(!empty($Where['EventGUID'])){
			$this->db->where('EventGUID',$Where['EventGUID']);
		}
		if(!empty($Where['EventID'])){
			$this->db->where('EventID',$Where['EventID']);
		}

		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Event',"MediaID" => $Record['MediaID']),TRUE);
				$Record['MediaID'] = ($MediaData ? $MediaData['Data'] : new stdClass());
				$Record['MediaThumbURL'] = $Record['MediaID']['Records'][0]['MediaThumbURL'];
				$Record['MediaURL'] = $Record['MediaID']['Records'][0]['MediaURL'];


				if(!$multiRecords){

					return $Record;
				}
		
				$Records[] = $Record;
				}
			 
			
			return $Records;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to get Event
	*/
	function getEvent($EventGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		//print_r($Where); die();

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select("b.*,DATE_FORMAT(b.EventEndDateTime,'%d-%m-%Y %H:%i') EventEndDateTime,DATE_FORMAT(b.EventStartDateTime,'%d-%m-%Y %H:%s') EventStartDateTime",TRUE);

		$this->db->from('tbl_events b');
		$this->db->join('tbl_entity e','e.EntityID=b.EventID');
		//$this->db->join('tbl_event_category c','b.CategoryID=c.CategoryID','left');
		if(!empty($InstituteID)){
			$this->db->where('e.InstituteID',$InstituteID);
		}else if(!empty($Where['UserID'])){
			$this->db->where('e.InstituteID',$Where['UserID']);
		}
		
		//$this->db->where('c.InstituteID',$InstituteID);
		$this->db->where('b.StatusID',2);

		if(!empty($Where['Keyword'])){

			$Where['Keyword'] = trim($Where['Keyword']);

			$this->db->group_start();
			
			$this->db->like("b.EventName", $Where['Keyword']);
			$this->db->or_like("b.Address", $Where['Keyword']);
			$this->db->or_like("b.CategoryName", $Where['Keyword']);
			$this->db->group_end();
				
		}


		if(!empty($Where['Category'])){
			$this->db->group_start();
			$this->db->like('b.CategoryName',$Where['Category']);
			$this->db->group_end();
		}
		

		if(!empty($Where['EventGUID'])){

			$this->db->where('EventGUID',$Where['EventGUID']);
		}

		$this->db->order_by('EventStartDateTime','DESC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			//$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die;
		//print_r($Query);
		
		
		if($Query->num_rows()>0){
			
			foreach($Query->result_array() as $Record){

				if(!empty($Record['MediaID'])){
	 				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'Event',"MediaID" => $Record['MediaID']),TRUE);
	 				$MediaDatas = ($MediaData ? $MediaData['Data'] : new stdClass());
	 				$Record['MediaThumbURL'] = $MediaDatas['Records'][0]['MediaThumbURL'];
	 				$Record['MediaURL'] = $MediaDatas['Records'][0]['MediaURL'];
	 			}else{
	 				$Record['MediaThumbURL'] = "";
	 				$Record['MediaURL'] = "";
	 			}

	      		if(!empty($Record['CategoryName'])){
					$this->db->select('');
				
					$cat=$Record['CategoryName'];
					 $vegetables = explode(',', $cat);
	
					 $CategoryData=array();
	
					for ($i=0; $i < sizeof($vegetables) ; $i++) { 

						$arr = $this->getCategory('',array("CategoryName" => $vegetables[$i]),TRUE);
						
						$this->db->select("EventActivityID");
						$this->db->from("tbl_events_participants");
						$this->db->where("EventID",$Record['EventID']);
						$this->db->where("EventActivityID",$vegetables[$i]);
						$this->db->where("UserID",$this->EntityID);

						$this->db->limit(1);
						$dataParticipate = $this->db->get();
						//echo $this->db->last_query(); die;
						if($dataParticipate->num_rows() > 0){
							$arr['Data']['Records'][0]['participated'] = "yes";
						}else{
							$arr['Data']['Records'][0]['participated'] = "no";
						}
						
						$CategoryData[] = $arr['Data']['Records'][0];	

					}
			        $Record['CategoryData'] = $CategoryData;
			   	}else{
			   		$Record['CategoryData'] = "";
			   	}

				if(!$multiRecords){

					return $Record;
				}


				$Records[] = $Record;

			}
              
			$Return['Data']['Records'] = $Records;

		
			return $Return;
		}
		return FALSE;		
	}

	/*
	Description: 	Use to get Cetegories
	*/
	function getCategory($EventGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('CategoryID,Category');
		$this->db->from('tbl_event_category');

		if(!empty($Where['CategoryID'])){

				$this->db->where('CategoryID',$Where['CategoryID']);
			}

		if(!empty($Where['CategoryName'])){

				$this->db->where('Category',$Where['CategoryName']);
			}

		$this->db->where('InstituteID',$InstituteID);
		$this->db->order_by('Category','ASC');
	

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}
		
	/*
	Description: 	Use to get Author
	*/
	function getAuthor($EventGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->db->select('b.Author');
		
		$this->db->from('tbl_events b');
		$this->db->join('tbl_entity e','e.EntityID=b.EventID','left');
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->distinct('Author');
		$this->db->order_by('Title','ASC');
		
		
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return = $Records;

			return $Return;
		}
		return FALSE;		
	}	

	/*
	Description: 	Use to get Modules
	*/
	function getUserData(){
	

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID');
		$this->db->from('tbl_users u');
		$this->db->join('tbl_entity e','e.EntityID=u.UserID','left');
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('u.FirstName!=','');
		$this->db->order_by('FirstName','ASC');
		$Query = $this->db->get();	
      	$data = $Query->result_array();
		if(count($data) > 0){
			foreach($data as $Record){

			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	

/*edit get data*/
	function getEDEvent($Field='UserTypeID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select('*');
		$this->db->from('tbl_events');
		
		$this->db->order_by('UserTypeID','ASC');

		if(!empty($Where['UserTypeID'])){
			$this->db->where("UserTypeID",$Where['UserTypeID']);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			
				$Records = $Record;
			}
			//$Return['Records'] = $Records;

			return $Records;
		}
		return FALSE;		
	}


	function addParticipants($EntityID, $EventID, $EventActivityID){
		$this->db->select('*');
		$this->db->from('tbl_events_participants');
		$this->db->where('EventID',$EventID);
		$this->db->where('UserID',$EntityID);
		$this->db->where('EventActivityID',$EventActivityID);
		$this->db->limit(1);
		$Query = $this->db->get();
		if($Query->num_rows() > 0){
			return "Exist";
		}else{
			$data = array("EventID"=>$EventID,"UserID"=>$EntityID,"EventActivityID"=>$EventActivityID);
			$this->db->insert('tbl_events_participants',$data);				
			return "added";
		}
	}


	/*
	Description: 	Use to get Modules
	*/
	function getStaffList($EntityID){

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID');
		$this->db->from('tbl_users u');
		$this->db->join('tbl_entity e','e.EntityID=u.UserID','left');

		// if(!empty($Input['BookGUID'])){
		// 	$this->db->where('u.UserID NOT IN (select UserID from tbl_library_issue_books where BookID = (select BookID from tbl_library where BookGUID = "'.$Input['BookGUID'].'" limit 1) and StatusID = 1)');
		// }
		
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('u.UserTypeID!=',10);
		$this->db->where('u.UserTypeID!=',1);
		$this->db->where('u.UserTypeID!=',7);
		$this->db->where('u.FirstName!=','');

		// if(!empty($Input['keyvalue'])){
		// 	$this->db->like('u.FirstName=',$Input['keyvalue']);
		// }
		
		$this->db->order_by('FirstName','ASC');
		$Query = $this->db->get();	//echo $this->db->last_query();
      	$data = $Query->result_array();
		if(count($data) > 0){
			foreach($data as $Record){
			 $Records[] = $Record;
			}
			//$Return['Data']['Records'] = $Records;

			return $Records;
		}
		return FALSE;		
	}


	function ManageActivity($EventID, $Input)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);



		$InsertData = array_filter(array(
			"EventID" => $EventID,
			"ActivityName" => $Input['ActivityName'],
			"Accountaibility" => $Input['Accountaibility'],
			"Coordinator" => $Input['Coordinator'],
			"ActivityDuration" => $Input['hour'].':'.$Input['minute']
		));
		
		$this->db->insert('tbl_event_activity', $InsertData);
		$insert_id = $this->db->insert_id();

		for ($i=0; $i < count($Input['Task']); $i++) { 

			// if(is_array($Input['ResponsiblePerson'][$i])){
			// 	$ResponsiblePerson = implode(",", $Input['ResponsiblePerson'][$i]);
			// }else{
			// 	$ResponsiblePerson = $Input['ResponsiblePerson'][$i];
			// }

			$InsertData = array_filter(array(
				"EventActivityID" => $insert_id,
				"Task" => $Input['Task'][$i],
				"ResponsiblePerson" => $Input['ResponsiblePerson'][$i],
				"CompletionDate" => date("Y-m-d", strtotime($Input['CompletionDate'][$i]))
			));
			
			$EventID = $this->db->insert('tbl_event_activity_task', $InsertData);
		}

		return TRUE;
		
	}


	function EditActivity($EventID, $Input)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

		$UpdateData = array_filter(array(
			"ActivityName" => $Input['ActivityName'],
			"Accountaibility" => $Input['Accountaibility'],
			"Coordinator" => $Input['Coordinator'],
			"ActivityDuration" => $Input['hour'].':'.$Input['minute']
		));
		
		$this->db->where('EventID', $EventID);
		$this->db->where('EventActivityID', $Input['EventActivityID']);
		$this->db->update('tbl_event_activity', $UpdateData);
		
		$this->db->where('EventActivityID', $Input['EventActivityID']);
		$this->db->delete('tbl_event_activity_task');

		for ($i=0; $i < count($Input['Task']); $i++) { 

			$InsertData = array_filter(array(
				"EventActivityID" => $Input['EventActivityID'],
				"Task" => $Input['Task'][$i],
				"ResponsiblePerson" => $Input['ResponsiblePerson'][$i],
				"CompletionDate" => date("Y-m-d", strtotime($Input['CompletionDate'][$i]))
			));
			
			$this->db->insert('tbl_event_activity_task', $InsertData);
		}

		return TRUE;
		
	}


	function getEventActivity($EntityID,$Input){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('*');
		$this->db->from('tbl_event_activity');
		$this->db->where('EventID',$Input['EventID']);
		$this->db->order_by('EventActivityID','ASC');
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				$this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID');
				$this->db->from('tbl_users u');
				$this->db->where('u.UserID', $Record['Accountaibility']);
				$this->db->limit(1);
				$Query = $this->db->get();	
		      	$data = $Query->result_array();
		      	$Record['FullName'] = $data[0]['FullName'];

		      	if(empty($Input['UserID'])){
				 	$this->db->select('*');
					$this->db->from('tbl_event_activity_task');
					$this->db->where('EventActivityID',$Record['EventActivityID']);
					$this->db->order_by('ActivityTaskID','ASC');
					$Task = $this->db->get();
					if($Task->num_rows() > 0){
						$Record['Task'] = $Task->result_array();
					}
				}

				if(!empty($Input['UserID'])){
					$this->db->select('participants.*,CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName');
					$this->db->from('tbl_events_participants as participants');
					$this->db->join('tbl_users u','u.UserID = participants.UserID');
					$this->db->where('participants.EventActivityID',$Record['EventActivityID']);
					$participants = $this->db->get();
					if($participants->num_rows() > 0){
						$Record['participants'] = $participants->result_array();
					}
				}	

				$Records[] = $Record;

			}
			$Return['Data'] = $Records;

			return $Return;
		}
		return FALSE;	
	}


	function getEventParticipants($EntityID,$Input){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('participant.*,student.BatchID');
		$this->db->from('tbl_events_participants as participant');
		$this->db->join('tbl_students as student',"student.StudentID = participant.UserID");

		if(!empty($Input['EventID'])){
			$this->db->where('participant.EventID',$Input['EventID']);
		}

		if(!empty($Input['UserID'])){
			$this->db->where('participant.UserID',$Input['UserID']);
		}

		if(!empty($Input['EventActivityID'])){
			$this->db->where('participant.EventActivityID',$Input['EventActivityID']);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		//die();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				$this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,u.Email,u.PhoneNumber,IF(u.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",u.ProfilePic)) AS ProfilePic');
				$this->db->from('tbl_users u');
				$this->db->where('u.UserID', $Record['UserID']);
				$this->db->limit(1);
				$Query = $this->db->get();	
		      	$data = $Query->result_array();
		      	$Record['FullName'] = $data[0]['FullName'];
		      	$Record['Email'] = $data[0]['Email'];
		      	$Record['PhoneNumber'] = $data[0]['PhoneNumber'];
		      	$Record['UserGUID'] = $data[0]['UserGUID'];
		      	$Record['ProfilePic'] = $data[0]['ProfilePic'];

			 	$this->db->select('BatchName');
				$this->db->from('tbl_batch batch');
				$this->db->where('batch.BatchID', $Record['BatchID']);
				$this->db->limit(1); 
				$Query = $this->db->get();	 //echo $this->db->last_query(); die();
		      	$data = $Query->result_array();
		      	$Record['BatchName'] = $data[0]['BatchName'];
				//$Records[] = $Record;

				$this->db->select('activity.ActivityName');
				$this->db->from(' tbl_event_activity activity');
				$this->db->where('activity.EventActivityID', $Record['EventActivityID']);
				$this->db->limit(1); 
				$Query = $this->db->get();	 //echo $this->db->last_query(); die();
		      	$data = $Query->result_array();
		      	$Record['ActivityName'] = $data[0]['ActivityName'];


				$Records[] = $Record;

			}
			$Return['Data'] = $Records;

			return $Return;
		}
		return FALSE;	
	}



	function ActivityDelete($EntityID,$Input){
		$this->db->where("EventID",$Input['EventID']);
		$this->db->where("EventActivityID",$Input['EventActivityID']);
		$this->db->delete("tbl_event_activity");

		$this->db->where("EventActivityID",$Input['EventActivityID']);
		$this->db->delete("tbl_event_activity_task");
	}


	function SetParticipantsPermission($EntityID,$Input)
	{
		$this->db->where("UserID",$Input['UserID']);
		$this->db->where("EventActivityID",$Input['EventActivityID']);
		$this->db->update("tbl_events_participants",array("Permission"=>$Input['Permission']));
		return TRUE;
	}
}