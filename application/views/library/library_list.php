<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 1px;
	}
</style>

<header class="panel-heading"> <h1 class="h4">Manage Library</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-bodys"><!-- Body -->

	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<span class="float-left records d-none d-sm-block">
			<span  class="h5">Total Books: {{data.dataList.length}}</span>
		</span>

		<div class="float-right">
			<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAddCategory();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Books Category</button>         
			<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Books</button>
			<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAddD();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Digital </button>
		</div>
	</div>
		<div class="row">
		 <div class="col-md-3">
			  <div class="form-group">
				<!--  <label class="control-label">Category</label> <br> -->
				<select class="form-control" name="Category" id="Category" ng-model="Category">
					<option value="">Select Category</option>
					<option ng-repeat="rowc in data.filterDataC" value="{{rowc.ID}}" ng-selected="Category==rowc.ID">{{rowc.Category}}</option>
				</select>
			 </div>
		 </div>
	       <div class="col-md-3">
			  <div class="form-group">
				<select class="form-control" name="Author" id="Author"  ng-model="Author">
					<option value="">Select Author</option>
					<option ng-repeat="(key, rowa) in data.filterDataA" value="{{rowa.Author}}" ng-selected="Author==rowa.Author">{{rowa.Author}}</option>
				</select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<form class="form-inline" id="filterForm" role="form" autocomplete="off">
				 <div id="universal_search">
                    <input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Type and search" style="" autocomplete="off">                    
                 </div>
				</form>
			</div>
		</div>
		  <div class="col-md-3">
			  <div class="form-group">
				<select class="form-control" name="FormType" id="FormType"  ng-model="FormType">
					<option value="" ng-selected="FormType==''">Select Type</option>
					<option value="Book" ng-selected="FormType=='Book'">Book</option>
					<option value="Digital" ng-selected="FormType=='Digital'">Digital</option>
				</select>
			</div>
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
			<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>
	  </div>


	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>
					<th style="width: 80px;">Image</th>
					<th>Title</th>
					<th style="width: 150px;">Author</th>
					<th style="width: 100px;">Category</th>
					<th style="width: 150px;">Accession Number</th>
					<th style="width: 100px;">Assets</th>
					<th style="width: 80px;">No of copy</th>
					<th style="width: 80px;">Available</th>
					<th style="width: 50px;">Issued</th>
					<th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">
					<td>
						<img src="{{row.BMediaThumbURL}}">
					</td>
					<td>
						<strong>{{row.Title}}</strong>

					</td>
					<td>
						<strong>{{row.Author}}</strong>

					</td>
					<td>
						<strong>{{row.Category}}</strong>

					</td>
					<td>
						<strong>{{row.AccessionNumber}}</strong>

					</td>
					<td>
						<a ng-if="row.MediaID == 0"></a>
						<div ng-if="row.DigitalType =='File' && row.Extension=='pdf'">
					     <p class="form-control-static"> Digital(<a href="javascript:void(0)" ng-if="row.MediaID != 0" ng-click="LibraryFileInModalBox(API_URL,row.MediaURL,row.Title,row.BookGUID)">Document</a>)</p>
					   </div>
					   <div ng-if="row.DigitalType =='File' && row.Extension!='pdf'">
					     <p class="form-control-static"> Digital(<a href="{{row.MediaURL}}" ng-if="row.MediaID != 0" download="">Document</a>)</p>
					   </div>


                     <div ng-if="row.DigitalType =='Audio'">
						Digital(<a style="cursor: pointer; color:blue;"  ng-click="loadContentView('audio', row.Title, row.AudioURL)" >Audio</a>)
						</div>
						<div ng-if="row.DigitalType =='Video'">
						Digital(<a style="cursor: pointer; color:blue;"  ng-click="loadContentView('video', row.Title, row.VedioURL)" >Video</a>)
						</div>
						<span ng-if="row.FormType == 'Book'">Book</span>
					</td>
					<td><strong>{{row.TotalBook}}</strong></td>
					<td><strong>{{row.available_book}}</strong></td>
					<td><strong>{{row.unavailable_book}}</strong></td>

					

					<td class="text-center" style="text-align: center;">

								<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.BookGUID,row.FormType)" data-toggle="tooltip" title="Edit"></a>

								<a class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.BookGUID,row.FormType)" data-toggle="tooltip" title="Edit"  ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0"></a>
						
								<a class="glyphicon glyphicon-arrow-up" href="" ng-click="loadFormIssueBook(key, row.BookGUID,row.FormType)" title="Book Issue" ng-if="row.available_book > 0 && row.FormType == 'Book' && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)"> </a>

								<a class="glyphicon glyphicon-arrow-down" href="" ng-click="loadFormReturnBook(key, row.BookGUID,row.FormType)" title="Book Return" ng-if="row.TotalBook != row.available_book && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)"> </a>
							
								<!-- <a class="glyphicon glyphicon-link" href="" ng-click="loadFormIssueBook(key, row.BookGUID)" title="Book Issue" ng-if="row.StatusID == 2"> </a>

								<a class="glyphicon glyphicon-link" href="" ng-click="loadFormReturnBook(key, row.BookGUID)" title="Book Return" ng-if="row.StatusID == 1"> </a> -->

								<a class="glyphicon glyphicon-trash" href="" ng-click="loadFormBookDelete(key, row.BookGUID)" data-toggle="tooltip" title="Delete"  ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0"></a>

							
					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->


<!-- add Modal -->

	<div class="modal fade" id="view_media">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Asset</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div autocomplete="off" ng-include="templateURLView"></div>
				<!-- /form -->
			</div>
		</div>
	</div>

	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Book Details</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div autocomplete="off" ng-include="templateURLView"></div>
				<!-- /form -->
			</div>
		</div>
	</div>

	<!-- add Book Category -->

	<div class="modal fade" id="addcategory_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Book Category</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="cancelAddActivity()"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="category_form" name="category_form" autocomplete="off" ng-include="templateURLCategory">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

	<!-- add Modal -->

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Book</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

	<!-- add Modal -->

	<div class="modal fade" id="addd_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Digital</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="addd_form" name="addd_form" autocomplete="off" ng-include="templateURLAddd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Book Details</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>

<!-- issunebook_model form -->

	<div class="modal fade" id="issunebook_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Book Issue </h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="issuebook_form" name="issuebook_form" autocomplete="off" ng-include="templateURLIssueBook">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

	<!-- returnbook_model form -->

<div class="modal fade" id="returnbook_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Book Return </h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="returnbook_form" name="returnbook_form" autocomplete="off" ng-include="templateURLReturnBook">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

</div><!-- Body/ -->