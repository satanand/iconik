app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.availableData = [];

    //$scope.data.batch = [];
    /*----------------*/

    $scope.availableKeys = function (validity)

    {

        var data = 'SessionKey='+SessionKey+'&validity='+validity;

        $http.post(API_URL+'keys/availableKeys', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

             $scope.availableData =  response.Data[0]; console.log($scope.availableData);

             $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 300);          

         }

     });

    }



    $scope.getUser = function ()

    {

        var data = 'SessionKey='+SessionKey;

        $scope.Institute = [];

        $http.post(API_URL+'keys/getUser', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

             $scope.Institute.push(response.Data); //console.log($scope.Institute);      

             }

        });

    }


    $scope.getBatchByCourse = function(CourseGUID){
        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;
        $scope.data.batch = [];
        $http.post(API_URL+'keys/getBatchByCourse', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.batch.push(response.Data);

                console.log($scope.data.batch);
            }
        });
    }


    $scope.getCourses = function(){
        var data = 'SessionKey='+SessionKey+'&CategoryTypeName=Course';
        $scope.data.course = [];
        $http.post(API_URL+'students/getCourses', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */
                $scope.data.course = response.Data;
            }
        });
    }

    /*list*/

    $scope.applyFilter = function ()

    {

        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        $scope.getList();

    }



    $scope.getStudentsList = function (check="",CourseGUID="",BatchGUID="")
    { 
         //alert(1);   

        if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.listLoading = true;

        $scope.data.pageSize = 20;

        $scope.data.pageNo = 1;
        $scope.data.dataList = [];
        
        
        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID+'&BatchID='+BatchGUID;
        console.log(data);
        $http.post(API_URL+'keys/getStudents', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    for (var i in response.Data) {
                       $scope.data.dataList.push(response.Data[i]);
                    }
                    $scope.data.pageNo++;
                }else{
                    $scope.data.noRecords = true;
                   // ////console.log(data.dataList.length);
                }
                        
                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);
        });
    }





    /*list append*/

    $scope.getPricingList = function ()

    {
        //$scope.getUser();

        if ($scope.data.listLoading || $scope.data.noRecords) return; 

        $scope.data.listLoading = true;  

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize;

        $http.post(API_URL+'pricing/getPricingList', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                for (var i in response.Data) {
                    $scope.data.dataList.push(response.Data[i]);
                }

                $scope.data.pageNo++;               

            }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }





    /*load add form*/

    $scope.loadFormAdd = function (Position, CategoryGUID)

    {

        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

        $('#add_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

    }



    $scope.loadFormAllot = function ()

    {
        $scope.getUser();

        $scope.availableData.AvailableKeys = 0;

        $scope.templateURLAllot = PATH_TEMPLATE+module+'/allot_keys_form.htm?'+Math.random();

        $('#allot_key_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);
    }







    /*load edit form*/

    $scope.loadFormEdit = function (Position, CategoryGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'category/getCategory', 'SessionKey='+SessionKey+'&CategoryGUID='+CategoryGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#edit_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }

        });

    }



    /*load delete form*/

    $scope.loadFormDelete = function (Position, CategoryGUID)

    {

        $scope.data.Position = Position;

        // $scope.templateURLDelete = PATH_TEMPLATE+module+'/delete_form.htm?'+Math.random();

        // $scope.data.pageLoading = true;

        // $http.post(API_URL+'category/getCategory', 'SessionKey='+SessionKey+'&CategoryGUID='+CategoryGUID, contentType).then(function(response) {

        //     var response = response.data;

        //     if(response.ResponseCode==200){ /* success case */

        //         $scope.data.pageLoading = false;

        //         $scope.formData = response.Data

        //         $timeout(function(){            
        //            $('.modal-header .close').click();
        //            $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

        //        }, 200);

        //     }

        // });

        $scope.deleteData(CategoryGUID);

    }



    /*add data*/

    $scope.addData = function ()

    {

        $scope.addDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'pricing/add', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                //$scope.getKeysStatisctics();

                $scope.data.dataList.push(response.Data);

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }


    $scope.AssignKey = function(){        
        var UserGUID = $("input[name='UserGUID[]']:checkbox:checked").map(function() {
            if($(this).val()){
                return $(this).val();
            }
        }).get();

       // alert(UserGUID.length);

        if(UserGUID.length > 0){
            var Course = $("#Courses").val();
            var Batch = $("#Batch").val();
            var Validity = $("#Validity").val();
            var AvailableKeys = $("#availableKeys").val();
            //alert(Validity.length);
            if(Validity.length == 0){
                ErrorPopup("Please select keys validity to assign keys.");
            }else if(AvailableKeys.length == 0){
                ErrorPopup("Keys are not available to assign keys.");
            }else{


                $scope.addDataLoading = true;

                var data = 'SessionKey='+SessionKey+'&CategoryGUID='+Course+'&BatchGUID='+Batch+'&Validity='+Validity+'&AvailableKeys='+AvailableKeys+'&UserGUID='+JSON.stringify(UserGUID);
                //console.log(data); return false;
                $http.post(API_URL+'keys/assignKeysToStudent', data, contentType).then(function(response) {

                    var response = response.data;

                    if(response.ResponseCode==200){ /* success case */               

                        SuccessPopup(response.Message);

                        $scope.getStudentsList();

                        $timeout(function(){            

                           $('.modal-header .close').click();

                       }, 200);

                    }else{

                        ErrorPopup(response.Message);

                    }

                    $scope.addDataLoading = false;          

                });
            }
        }else{
            ErrorPopup("Please select student to assign keys.")
        }
    }


    $scope.allotKeys = function ()

    {

        $scope.addDataLoading = true;  $scope.AvailableKeys

        var data = 'SessionKey='+SessionKey+'&AvailableKeys='+$scope.availableData.AvailableKeys+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'keys/allotKeys', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.getKeysStatisctics();

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }





    /*edit data*/

    $scope.editData = function ()

    {

        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'admin/category/editCategory', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }







}); 









/* sortable - starts */

function tblsort() {



  var fixHelper = function(e, ui) {

    ui.children().each(function() {

        $(this).width($(this).width());

    });

    return ui;

}



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

        data: order,

        stop: function(response) {

            if (response.status == "success") {

                window.location.href = window.location.href;

            } else {

                alert('Some error occurred');

            }

        }

    });

}

}


function filterStudentsList(CourseGUID="",BatchGUID=""){
    if(CourseGUID.length > 0){
        angular.element(document.getElementById('keys-body')).scope().getBatchByCourse(CourseGUID); 
    }
    angular.element(document.getElementById('keys-body')).scope().getStudentsList(1,CourseGUID,BatchGUID); 
}

function selectAllStudent(){
    if($('#checkAll').prop("checked") == false){
        $('.SelectStudent').prop('checked', false);
    }else{
        $('.SelectStudent').prop('checked', true);
    }
}
/* sortable - ends */