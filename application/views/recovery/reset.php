<div class="container" ng-controller="PageController"> 
<div id="logo" class="text-center"><img src="<?php echo API_URL;?>asset/img/emailer/output-onlinepngtools.png"></div> 
  <!-- Form -->
  <div class="col-12 col-sm-11 col-md-8 col-lg-6 col-xl-5 login-block">
    <h1 class="h3">Reset password</h1>
    <br>
    <form method="post" id="recovery_reset_form" name="recovery_reset_form"  autocomplete='off'>
      <?php if(isset($OTP)) { ?>
      <div class="form-group">
        <input type="hidden" id="OTP" name="OTP" class="form-control form-control-lg" placeholder="OTP"  autofocus="" maxlength="6" value="<?php echo $OTP; ?>">
      </div>
      <?php }else{ ?>
       <div class="form-group">
        <input type="text" id="OTP" name="OTP" class="form-control form-control-lg" placeholder="OTP"  autofocus="" maxlength="6">
      </div>
      <?php } ?>

      <div class="form-group">
        <input type="password" id="Password" name="Password" class="form-control form-control-lg" placeholder="Password">
      </div>

      <div class="form-group">
       <input type="password" id="retype" name="retype" class="form-control form-control-lg" placeholder="Retype New Password">
     </div>

     <div class="form-group">
      <img src="asset/img/loader.svg" ng-if="processing" style="position: absolute;    top: 50%;    left: 50%;    transform: translate(-50%, -50%);">
      <button type="button" class="btn btn-success  btn-sm" ng-disabled="processing" ng-click="reset()">Reset</button>
      <?php if(empty($OTP)) { ?>
        <span class="float-right"><a href="signin" class="a">Sign in?</a></span>
      <?php } ?>
    </div>

  </form>
</div>
</div><!-- / container -->