<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4">Manage Content</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" id="content-body" ng-init="getCategoryList()" ><!-- Body -->
	<div class="clearfix mt-2 mb-2">
	   <div class="row float-left ml-6" style="width:80%;">
	      <div class="col-md-4" style="margin-left: 35px;">
	         <div class="form-group">
	            <select class="form-control" onchange="getCategorySubjects(this.value,'getList')">
	               <option value="" selected="selected">Select Course</option>
	               <option ng-repeat="row in categoryDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>
	            </select>
	         </div>
	      </div>
	      <div class="col-md-4">
	         <div class="form-group">
	            <div id="subcategory_section">
	               <select onchange="filterContentStatistics(this.value)"  class="form-control">
	               	  <option value="" selected="selected">Select Subject</option>
	                  <option ng-repeat="row in subCategoryDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>
	               </select>
	            </div>
	         </div>
	      </div>
	   </div>
	   <div class="float-right mr-2">		
			<button class="btn btn-success btn-sm ml-1 float-right" id="add-content-btn" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Content</button>
		</div>
	</div>

	<!-- <div class="clearfix mt-2 mb-2">
	   <div class="row float-left ml-6" style="width:80%;">
	      <div class="col-md-6">
	         <div class="form-group">
	            <select class="form-control" ng-change="getCategoryFilterData(CategoryGUID)" name="ParentCat" id="Courses" required="">
	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
	               <option ng-repeat="row in categoryDataList" value="{{$index}}">{{row.CategoryName}}</option>
	            </select>
	         </div>
	      </div>
	      <div class="col-md-6">
	         <div class="form-group">
	            <div id="subcategory_section">
	               <select name="CategoryGUIDs" ng-change="getCategoryFilterData(CategoryGUID)"  class="form-control" onchange="formDataCheck()" onchange="check_subject_available()" required="">
	                  <option value="">Select Subject</option>
	               </select>
	            </div>
	            <a  class="right_link Add_Subject" id="link_add_subject" ng-click="loadFormSubjectAdd(ParentCategoryGUID)" style="display: none;cursor: pointer;
	               color: blue;
	               text-decoration: underline;">Add New Subject</a>
	         </div>
	      </div>
	   </div>
	   <div class="float-right mr-2">		
	      <button class="btn btn-success btn-sm ml-1 float-right" id="add-content-btn" ng-click="loadFormAdd();">Add Content</button>
	   </div>
	</div> -->

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">


	<!-- Data table -->
	<div class="table-responsive block_pad_md"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		<table class="table table-striped table-hover" ng-init="getList()">
			<!-- table heading -->
			<thead>
				<tr>
					<!-- <th style="width: 100px; text-align: center;">S.NO</th> -->
					<th style="width: 100px;">COURSE</th>
					<th style="width: 100px;">Subject</th>
					<!-- <th style="width: 100px; text-align: center;">Content</th> -->
					<th style="width: 100px; text-align: center;">Text Files</th>
					<th style="width: 100px; text-align: center;">Video Files</th>
					<th style="width: 100px; text-align: center;">Audio Files</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row" ng-repeat="(key, row) in data.dataList">
					<td>{{row.ParentCategoryName}}</td>
					<td >{{row.SubCategoryName}}</td>
					<td style="text-align: center;">{{row.text_file_count}}</td>
					<td style="text-align: center;">{{row.video_count}}</td>
					<td style="text-align: center;">{{row.audio_count}}</td>


					

					<td class="text-center" style="text-align: center;">
						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'content/details_list/?parentCategoryGUID={{row.parentCategoryGUID}}&CategoryGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Content list"></a>
						<!-- <a class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEdit(key, row.PostGUID, row.PostID)"></a> -->
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-plus" href="" ng-click="LoadSelectedCategoryFormAdd(row.parentCategoryGUID, row.CategoryGUID)" data-toggle="tooltip" title="Add content with {{row.ParentName}} and {{row.SubCategoryName}}"></a>
						<!-- <a class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.PostGUID)"></a> -->
						<!-- <div class="dropdown">
							<button class="btn btn-secondary  btn-sm action" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8230;</button>
							<div class="dropdown-menu dropdown-menu-left">
									<a class="dropdown-item" href="" ng-click="loadFormView(key, row.PostGUID)">View</a>
									<a class="dropdown-item" href="" ng-click="loadFormEdit(key, row.PostGUID, row.PostID)">Edit</a>
									<a class="dropdown-item" href="" ng-click="loadFormDelete(key, row.PostGUID)">Delete</a>
							</div>
						</div> -->
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="data.noRecords">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->




	<!-- add Modal -->
	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="add_subject">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Subject</h3>     	
					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="subjectTemplateURLAdd"></div>
			</div>
		</div>
	</div>





	<!-- edit Modal -->
	<div class="modal fade" id="edit_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLEdit"></div>
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>


	<!-- delete Modal -->
	<div class="modal fade" id="delete_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
</div><!-- Body/ -->