app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.pageSize = 15;
    $scope.UserTypeID = UserTypeID;    
    /*----------------*/



    /*list append*/

    $scope.getList = function ()

    {
        //alert("getList");
        // if ($scope.data.listLoading || $scope.data.noRecords) return;

        // $scope.data.listLoading = true;

        // var data = 'SessionKey=ab877e94-f885-f9d1-c137-338882149006&QueryType=Query&CourseID=161';

        
        // $http.post(API_URL+'askquestion/getQueries', data, contentType).then(function(response) {

        //     var response = response.data;

        //     if(response.ResponseCode==200){ /* success case */

        //         $scope.data.dataList = response.Data;

        //         $scope.data.pageNo++;               

        //     }else{

        //         $scope.data.noRecords = true;

        //     }

        // $scope.data.listLoading = false;

        // setTimeout(function(){ tblsort(); }, 1000);

    //});

    }


    $scope.loadFormAttendance = function()
    { 
        $scope.data.dataLists = [];

        $scope.getBatch();

        $scope.getStudents('','');

        $scope.templateURLAttendance = PATH_TEMPLATE + 'batch/attendance_form.htm?' + Math.random();
        
        $scope.data.pageLoading = true;  

       // $scope.date = new Date();

        var d = new Date($.now());
        var m = (d.getMonth() + 1); if(m <= 9) m = "0" + m;
        $scope.date = d.getDate()+"-"+m+"-"+d.getFullYear();

        $('#attendance_model').modal({
            show: true
        }); 

        $scope.data.pageLoading = false;
    }


    $scope.getStudents = function(BatchGUID="", EntryDate="") 
    {
        $scope.data.Students = [];

        $scope.data.studentAttLoading = true;

        var data = 'SessionKey=' + SessionKey + '&EntryDate='+EntryDate+'&BatchGUID=' + BatchGUID;

        $("#AttendanceAll1, #AttendanceAll2, #AttendanceAll3, #AttendanceAll4").removeAttr("checked");

        $http.post(API_URL + 'batch/getStudentsList', data, contentType).then(function(response) 
        {
            var response = response.data;
            
            if (response.ResponseCode == 200 && response.Data) 
            {
                for (var i in response.Data) 
                {
                    $scope.data.Students.push(response.Data[i]);
                }
            }

            $scope.data.studentAttLoading = false;
        });
    }


    $scope.getBatch = function() 
    {
        $scope.data.pageNo = 1;
        $scope.data.batchDataList = [];        

        $scope.data.pageSize = 1000;
        $scope.data.pageLoading = true;
        $scope.data.listLoading = true; 

        var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + InstituteGUID+ '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize;

        $http.post(API_URL + 'batch/getBatch', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.batchDataList.push(response.Data.Records[i]);

                }

            } else {

                $scope.data.noRecords = true;

            }


            $scope.data.listLoading = false;
            $scope.data.pageLoading = false;
            

        });

    }


    $scope.SetAttendance = function ()
    { 
        $scope.loadeSubmit = true;
        
        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_attendance_form']").serialize();

        $http.post(API_URL+'batch/SetAttendance', data, contentType).then(function(response) 
        {
            var response = response.data;
            $scope.loadeSubmit = false;
            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                /*Not required to refresh----------------------
                $timeout(function()
                {
                   $('#attendance_model .close').click();

                   window.location.href = BASE_URL;

                }, 3000);*/
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.data.pageLoading = false;
        });


    }


    $scope.loadFormFeeCollection = function()
    { 
        $scope.data.dataLists = [];

        $scope.getBatch();

        $scope.getStudents('','');

        $scope.templateURLFeeCollection = PATH_TEMPLATE + 'feecollection/addfee.htm?' + Math.random();
        
        $scope.data.pageLoading = true;  

       // $scope.date = new Date();

        var d = new Date($.now());
        var m = (d.getMonth() + 1); if(m <= 9) m = "0" + m;
        $scope.date = d.getDate()+"-"+m+"-"+d.getFullYear();

        $('#feecollection_model').modal({
            show: true
        }); 

        $scope.data.pageLoading = false;
    }


    $scope.AddFeeCollection = function ()
    { 

        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_attendance_form']").serialize();

        $http.post(API_URL+'batch/SetAttendance', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                $timeout(function()
                {
                   $('#attendance_model .close').click();

                   window.location.href = BASE_URL;

               }, 3000);
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.data.pageLoading = false;
        });
    }



    $scope.GetBirthDaysList = function() 
    {       
        $scope.data.birthDaysList = [];        
        $scope.data.birthDaysLoading = true; 

        var data = 'SessionKey=' + SessionKey;

        $http.post(API_URL + 'common/GetBirthDaysList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data) 
            {
                for (var i in response.Data) 
                {
                    $scope.data.birthDaysList.push(response.Data[i]);
                }

                if($scope.data.birthDaysList.length > 0)
                {
                    $("#GetBirthDaysListCount").show();
                }
            }

            $scope.data.birthDaysLoading = false;         

        });
    }


    $scope.GetPendingProfile = function() 
    {       
        $scope.data.PendingProfileCount = 0;

        var data = 'SessionKey=' + SessionKey;

        $http.post(API_URL + 'common/GetPendingProfile', data, contentType).then(function(response) 
        {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Total > 0) 
            {
                $scope.data.PendingProfileCount = response.Data.Total;

                $("#PendingProfileCount").show();                
            }
        });
    }



    $scope.GetBirthDaysListPopup = function()
    {        
        $('#birthday_model').modal({
            show: true
        });
    }


});



