<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulksms extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Bulksms_model');
	}

	public function encrypt($plainText,$key)
	{
		$key = hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$openMode = openssl_encrypt($plainText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
		$encryptedText = bin2hex($openMode);
		return $encryptedText;

	}

	public function decrypt($encryptedText,$key)
	{
		$key = hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText = hextobin($encryptedText);
		$decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
		return $decryptedText;
		
	}
	//*********** Padding Function *********************

	public function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 4.0 version ********

	public function hextobin($hexString) 
   	{ 
        	$length = strlen($hexString); 
        	$binString="";   
        	$count=0; 
        	while($count<$length) 
        	{       
        	    $subString =substr($hexString,$count,2);           
        	    $packedString = pack("H*",$subString); 
        	    if ($count==0)
		    {
				$binString=$packedString;
		    } 
        	    
		    else 
		    {
				$binString.=$packedString;
		    } 
        	    
		    $count+=2; 
        	} 
  	        return $binString; 
    }


	/*

	Name: 			get course and batch

	Description: 	Use to get course and batch

	URL: 			/api/Bulksms/getCourseAndBatch

	*/
	public function getCourseAndBatch_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID','trim|callback_validateEntityGUID[Batch,BatchID]');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->getCourseAndBatch($this->EntityID,array("CategoryID"=>@$this->CategoryID,"BatchID"=>@$this->BatchID));

		if($Data){

			$this->Return['Data']      	=	$Data; 
		}
	}



	/*

	Name: 			send sms

	Description: 	Use to send sms

	URL: 			/api/Bulksms/sendMessage

	*/
	public function sendMessage_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SendTo', 'Send To', 'trim|required');
		$this->form_validation->set_rules('Message', 'Message', 'trim|required');
		if($this->input->post('SendTo') == "Students"){
			$this->form_validation->set_rules('BatchGUID[]', 'BatchGUID', 'trim|required');
		}else if($this->input->post('SendTo') == "Staff"){
			$this->form_validation->set_rules('UserGUID[]', 'UserGUID', 'trim|required');
		}
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->sendMessage($this->EntityID,$this->input->post());

		if($Data == 'User not available'){
			$this->Return['ResponseCode']      	=	500; 
			$this->Return['Message']      	=	"Users are not found"; 
		}else if($Data == 'SMS not available'){
			$this->Return['ResponseCode']      	=	500; 
			$this->Return['Message']      	=	"You don't have SMS credits"; 
		}else if($Data){
			$this->Return['Message']      	=	"Message has beed sent successfully to ".$Data." members."; 
		}
	}


	/*

	Name: 			send email

	Description: 	Use to send email

	URL: 			/api/Bulksms/sendEmail

	*/
	public function sendEmail_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SendTo', 'Send To', 'trim|required');
		if($this->input->post('SendTo') == "Students"){
			$this->form_validation->set_rules('BatchGUID[]', 'Student Batch', 'trim|required');
		}else if($this->input->post('SendTo') == "Staff"){
			$this->form_validation->set_rules('UserGUID[]', 'UserGUID', 'trim|required');
		}
		$this->form_validation->set_rules('Subject', 'Subject', 'trim|required');
		$this->form_validation->set_rules('Message', 'Message', 'trim|required');
		// $this->form_validation->set_rules('FromEmail', 'FROM EMAIL', 'trim|valid_email');
		// $this->form_validation->set_rules('FromName', 'From Name', 'trim');
		$this->form_validation->set_rules('SignatureGUID', 'SignatureGUID', 'trim');
		$this->form_validation->set_rules('CC_email', 'CC', 'trim|valid_email');
		$this->form_validation->set_rules('BCC_email', 'BCC', 'trim|valid_email');		
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->sendEmail($this->EntityID,$this->input->post());

		if($Data > 0){
			$this->Return['Message']      	=	"Message sent to ".$Data." members successfully"; 
		}
	}



	/*

	Name: 			add Signature

	Description: 	Use to add Signature

	URL: 			/api/bulkemail/addSignature

	*/
	public function addSignature_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FromName', 'From Name', 'trim');
		$this->form_validation->set_rules('FromEmail', 'From Email', 'trim|valid_email|required');
		$this->form_validation->set_rules('Signature', 'Signature', 'trim|required');		
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->addSignature($this->EntityID,$this->input->post());

		if($Data == true){
			$this->Return['Message']      	=	"Signature added successfully"; 
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message']      	=	"Sorry! This email and signautre already exist"; 
		}
	}



	/*

	Name: 			edit Signature

	Description: 	Use to edit Signature

	URL: 			/api/bulkemail/editSignature

	*/
	public function editSignature_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SignatureGUID', 'SignatureGUID', 'trim|required');
		$this->form_validation->set_rules('FromName', 'From Name', 'trim');
		$this->form_validation->set_rules('FromEmail', 'From Email', 'trim|valid_email|required');
		$this->form_validation->set_rules('Signature', 'Signature', 'trim|required');		
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->editSignature($this->EntityID,$this->input->post());

		if($Data == true){
			$this->Return['Message']      	=	"Signature added successfully"; 
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message']      	=	"Sorry! Some error occcured, Please try later"; 
		}
	}



	/*

	Name: 			get Signature

	Description: 	Use to get Signature

	URL: 			/api/bulkemail/getSignature

	*/
	public function getSignature_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SignatureGUID', 'SignatureGUID', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->getSignature($this->EntityID,$this->input->post(),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data){
			$this->Return['Data']      	=	$Data;
		}
	}



	/*

	Name: 			delete Signature

	Description: 	Use to delete Signature

	URL: 			/api/bulkemail/deleteSignature

	*/
	public function deleteSignature_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SignatureGUID', 'SignatureGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->deleteSignature($this->EntityID,$this->input->post('SignatureGUID'));

		if($Data){
			$this->Return['Message']      	=	"Deleted successfully"; 
		}
	}



	


	


	/*

	Name: 			send email

	Description: 	Use to send email

	URL: 			/api/Bulksms/sendEmail

	*/
	public function getAvailableSMSCredits_post(){

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->getAvailableSMSCredits($this->EntityID);

		if($Data){
			$this->Return['Data']      	=	$Data[0]; 
		}
	}


	public function getStaffList_post(){
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Data = $this->Bulksms_model->getStaffList($this->EntityID,$this->input->post("UserTypeID"));

		if($Data){
			$this->Return['Data']      	=	$Data; 
		}
	}

}