<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
	}

	/*
	Description: To get statics
	*/
	public function statics_post()
	{
		$SiteStatics = new stdClass();
		$SiteStatics->TotalUsers    = $this->db->query('SELECT COUNT(*) AS `TotalUsers` FROM `tbl_users`')->row()->TotalUsers;
		$SiteStatics->TotalContest  = $this->db->query('SELECT COUNT(*) AS `TotalContest` FROM `sports_contest`')->row()->TotalContest;
		$SiteStatics->TotalDeposits = $this->db->query('SELECT SUM(`Amount`) AS TotalDeposits FROM `tbl_users_wallet` WHERE `StatusID` = 5')->row()->TotalDeposits;
		$SiteStatics->TotalWithdraw = $this->db->query('SELECT SUM(`Amount`) AS TotalWithdraw FROM `tbl_users_withdrawal` WHERE `StatusID` = 2')->row()->TotalWithdraw;
		$SiteStatics->TotalEarning  = $SiteStatics->TotalDeposits - $SiteStatics->TotalWithdraw;
		$this->Return['Data'] = $SiteStatics;
	}
}
?>