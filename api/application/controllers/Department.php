<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Department_model');
		$this->load->model('Common_model');
	}



	

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getDepartment_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('DepartmentID', 'DepartmentID', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//print_r($this->Post); die();

		$RolesData = $this->Department_model->getDepartment($this->EntityID,$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($RolesData)){
			$this->Return['Data'] = $RolesData['Data'];
		}	
	}

	/*

	Description: 	Use to add new Department

	URL: 			/api_admin/Department/add	

	*/

	public function addDepartment_post()

	{

		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('DepartmentName', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

        //$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$DepartmentStatus = $this->Department_model->addDepartment($this->EntityID,$this->input->Post());

		//if($RolesData){

			if($DepartmentStatus == 1){
				$this->Return['Message']      	=	"New Department Added successfully.";
							
			}else  {
				
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Department name already exist.";	
			}

		//}

	}




	/*

	Name: 			editRoles_post
	

	*/
	public function editDepartment_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('DepartmentName', 'DepartmentName', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->set_rules('DepartmentID', 'DepartmentID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Department_model->editDepartment($this->EntityID, $this->input->post('DepartmentID'), $this->input->Post());
	
		$this->Return['Data']= $this->Department_model->getDepartment($this->EntityID, array("DepartmentID"=>$this->input->post('DepartmentID')));

	    $this->Return['Message']      	=	"Updated successfully."; 

	}


	public function deleteDepartment_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('DepartmentID', 'DepartmentID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
	
		$this->Return['Data']= $this->Department_model->deleteDepartment($this->EntityID, array("DepartmentID"=>$this->input->post('DepartmentID'),"StatusID"=>$this->input->post('StatusID')));

	    $this->Return['Message']      	=	"Deleted successfully."; 

	}
	
}