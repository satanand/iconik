<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Scholarships extends Admin_Controller_Secure 
{
	public function index()
	{		
		$load['css']=array(
		'asset/plugins/chosen/chosen.min.css',
		'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
		'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);

		$load['js']=array(
		'asset/js/'.$this->ModuleData['ModuleName'].'.js',
		'asset/plugins/chosen/chosen.jquery.min.js',
		'asset/plugins/jquery.form.js',	
		'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
		'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('scholarships/scholarships_list');
		$this->load->view('includes/footer');
		
	}


}
