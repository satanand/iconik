<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
	.col-md-2 {
	    -webkit-box-flex: 0;
	    -ms-flex: 0 0 16.666667%;
	    flex: 0 0 16.666667%;
	    max-width: 14.666667%;
	    padding: 0px 0px 0px 15px!important;
	}
	.fa-key{
		position: relative;
	    top: -4px;
	    display: inline-block;
	    /* font-family: 'Glyphicons Halflings'; */
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<header class="panel-heading">
	<h1 class="h4">Manage Customer Order</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="order-body" ng-init="getFilterData()"><!-- Body -->
	<div class="clearfix mt-2 mb-2">
		<form class="form-inline float-left ng-pristine ng-valid ng-submitted" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter(1,'manage_order')"> 
		    <div class="row float-left ml-6 mt-2 mb-2" style="margin-left: 0px;width: 100%;">
		         <div class="form-group ml-6" style="float: left;width: 85%;">
		            <span ng-if="data.dataList.length" class="h5">Total records: {{data.dataList.length}}</span>
		            <span ng-if="!data.dataList.length" class="h5">Total records: 0</span>
		         </div>
		         <div class="form-group ml-6" style="float: right;">
		         	<a class="btn btn-success btn-sm ml-1 float-right" href="<?php echo base_url(); ?>institute" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Manage Customer</a>
		         </div>
		    </div>
		    <div class="row float-left ml-6 mt-2 mb-2" style="width: 100%;">
		      <div class="col-md-2">
		         <div class="form-group">
		            <select class="form-control chosen-select"  name="UserGUID"  ng-model="UserGUIDs" ng-change="applyFilter(1,'manage_order')">
		               <option value="">Select Customer</option>
		               <option ng-repeat="row in Institutes" value="{{row.UserGUID}}">{{row.FullName}}</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-2">
		         <div class="form-group">
		            <select class="form-control" ng-model="OrderFor" ng-change="applyFilter(1,'manage_order')" name="OrderFor">
		               <option value="">Select Product</option>
		               <option  value="Keys">Keys</option>
		               <option  value="SMS Credits">SMS Credits</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-3">
		         <div class="form-group">
		            <select class="form-control" ng-model="PaymentMethod" ng-change="applyFilter(1,'manage_order')" name="PaymentMethod">
		               <option value="">Payment Method</option>
		               <option  value="Online">Online via CCAvenue</option>
		               <option  value="Cheque">By cheque</option>
		               <option  value="Cheque/Online">By cheque/Online via CCAvenue</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-2">
		         <div class="form-group">
		            <select class="form-control" ng-model="PaymentStatus" ng-change="applyFilter(1,'manage_order')" name="PaymentStatus">
		               <option value="">Payment Status</option>
		               <option  value="Paid">Paid</option>
		               <option  value="Unpaid">Unpaid</option>
		               <option  value="Partly Payment">Partly Payment</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-2">
		         <div class="form-group">
		            <select class="form-control" ng-model="OrderStatusID" ng-change="applyFilter(1,'manage_order')" name="OrderStatusID">
		               <option value="">Stock Received</option>
		               <option  value="2">Yes</option>
		               <option  value="1">No</option>
		            </select>
		         </div>
		      </div>		      
		      <div class="col-md-2">
		      	<div class="form-group">				
					<div id="universal_search">
	                    <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
	                    <span class="glyphicon glyphicon-search" ng-click="applyFilter(1,'manage_order')" data-toggle="tooltip" title="Search"></span>
	                    <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
	                </div>
				</div>
			  </div>
		   </div>
		</form>
	   <!-- <div class="float-right mr-2">		
			<button class="btn btn-success btn-sm ml-1 float-right" id="add-content-btn" ng-click="loadFormAdd();" ng-if="UserTypeID!=1">Add Order</button>
		</div> -->
	</div>
	<!-- <div class="clearfix mt-2 mb-2">

		<div class="float-right mr-2">

	   		<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd();" ng-if="UserTypeID!=1">Add Order</button>

		</div>

	</div> -->





	<!-- Top container -->

	<!-- <div class="clearfix mt-2 mb-2">

		<span class="float-left records hidden-sm-down">

			

		</span>

	</div> -->

	<!-- Top container/ -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList(2,'manage_order')" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr><!-- 

					<th style="width: 80px;" class="text-center">Order ID</th> -->

					<th style="width: 100px;">Order ID</th>

					<th style="width: 100px;">Product</th>

					<th style="width: 100px;">Order Date</th>

					<th style="width: 150px;" ng-if="UserTypeID==1">Ordered By</th>

					<!-- <th style="width: 80px;" class="text-center">Amount (<i class="fa fa-rupee"></i>)</th> -->

					

					<th style="width: 150px;" class="text-center">Payment  Method</th>

					<th >Payment Status</th>

					<th style="width: 150px;" class="text-center">Stock Received</th>

					<th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row" ng-repeat="(key, row) in data.dataList" ng-if="row.OrderID.length">

					<td ng-if="row.OrderID.length == 1">{{row.OrderIDPrefix}}00{{row.OrderID}}</td>

					<td  ng-if="row.OrderID.length == 2">{{row.OrderIDPrefix}}0{{row.OrderID}}</td>

					<td ng-if="row.OrderID.length > 2">{{row.OrderIDPrefix}}{{row.OrderID.length}}</td>

					<td>{{row.OrderFor}}</td>

					<td>{{row.EntryDate}}</td>

					<td ng-if="UserTypeID==1">{{row.FirstName}} {{row.LastName}}</td>

					<!-- <td class="text-center">{{row.OrderPrice}}</td> -->
					

					
					<td class="text-center">{{row.PaymentMethod}}</td>

					<td ng-if="row.OrderStatus == 'Offline' && row.PaymentStatus == 'Partly Payment'">Partly Payment ({{row.TotalPaidAmount}}/ <span ng-if="row.PaymentStatusID == 2">Verified</span><span ng-if="row.PaymentStatusID != 2">Unverified</span>)&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a></td>

					<td ng-if="row.OrderStatus == 'Offline' && row.PaymentStatus == 'Paid'">Paid ({{row.TotalPaidAmount}}/ <span ng-if="row.PaymentStatusID == 2">Verified</span><span ng-if="row.PaymentStatusID != 2">Unverified</span>)</td>

					<td ng-if="row.OrderStatus == 'Online' && row.PaymentStatusID == 2 && row.PaymentStatus == 'Paid'">Paid ({{row.TotalPaidAmount}}/ Verified) </td>

					<td ng-if="row.OrderStatus == 'Online' && row.PaymentStatusID == 8">Aborted ({{row.RemainingAmount}})&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a> </td>

					<td ng-if="row.OrderStatus == 'Online' && (row.PaymentStatusID == 3 || row.PaymentStatusID == 4 || row.PaymentStatusID == 1)">Failed ({{row.OrderPrice}})&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a> </td>

					<td ng-if="row.PaymentStatus == 'Unpaid' && row.OrderStatus != 'Online'">Unpaid ({{row.RemainingAmount}})&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a></td>

					<td ng-if="row.OrderStatus == 'Online' && row.RemainingAmount > 0 && row.PaymentStatusID == 2">Unpaid ({{row.RemainingAmount}})&nbsp; <a style="text-decoration: underline; color: blue;" ng-click="payNow(key,row.OrderGUID)">Pay Now</a></td>


					<td class="text-center" ng-if="row.OrderStatusID==2">Yes</td>

					<td class="text-center" ng-if="row.OrderStatusID==1">No</td>
					
<!-- 
					<td class="text-center"><span ng-class="{Pending:'text-danger', Delivered:'text-success',Deleted:'text-danger',Blocked:'text-danger'}[row.Status]">{{row.Status}}</span></td>  -->

					<td class="text-center">

						<a class="glyphicon glyphicon-eye-open" ng-click="loadFormView(key, row.OrderGUID)" data-toggle="tooltip" title="View"></a>

						<div  ng-if="row.OrderFor=='Keys'"><a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-arrow-up" ng-if="row.OrderStatusID==1 && row.PaymentStatusID!=0 && row.OrderFor=='Keys' && row.PaymentStatus == 'Paid'" ng-click="loadFormKeysAllot(key, row.OrderGUID, row.Order_details,'Allot')" style="position: relative;	    top: -4px;	    display: inline-block;	    font-style: normal;	    font-weight: normal;	    line-height: 1;	    -webkit-font-smoothing: antialiased;	    font-size: 20px;	    padding: 4px;" data-toggle="tooltip" title="Allot Keys"></a></div>

						<div  ng-if="row.OrderFor=='Keys'"><a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-arrow-down" ng-if="row.OrderStatusID==2 && row.PaymentStatusID==2 && row.OrderFor=='Keys' && row.PaymentStatus == 'Paid'" ng-click="loadFormKeysAllot(key, row.OrderGUID, row.Order_details,'Unallot')" style="position: relative;	    top: -4px;	    display: inline-block;	    font-style: normal;	    font-weight: normal;	    line-height: 1;	    -webkit-font-smoothing: antialiased;	    font-size: 20px;	    padding: 4px;" data-toggle="tooltip" title="Unallot Keys"></a></div>

						<div  ng-if="row.OrderFor=='SMS Credits'"><a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-envelope" ng-if="row.OrderStatusID==1 && row.PaymentStatusID!=0 && row.OrderFor=='SMS Credits' && row.PaymentStatus == 'Paid'" ng-click="loadFormSMSAllot(key, row.OrderGUID, row.Order_details)" title="Allot SMS Credits"></a></div>

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" id="PaymentDetails{{key}}" class="glyphicon glyphicon-check" ng-if="row.PaymentStatusID!=2 && row.OrderStatus != '' && row.RemainingVerification=='Yes'" ng-click="loadPaymentDetails(key, row.OrderGUID, row.OrderFor, row.Payment_details)" data-toggle="tooltip" title="Verify Order"></a>

						
<!-- 
						<div class="dropdown">

							<button class="btn btn-secondary  btn-sm action" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8230;</button>

							<div class="dropdown-menu dropdown-menu-left">

								<a class="dropdown-item" href="" ng-click="loadFormView(key, row.OrderGUID)">View</a>

							</div>

						</div> -->

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->


	<div class="modal fade" id="allot_key_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">{{keysType}} Keys</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAllot"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="allot_sms_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Allot SMS Credits</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAllot"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="add_model">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Order</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_payment">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Make Payment</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLPayment"></div>

			</div>

		</div>

	</div>



	<div class="modal fade" id="payment_details_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Verify Order</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<div ng-include="templateURLPaymentDetails"></div>

				<!-- /form -->

			</div>

		</div>

	</div>

	<div class="modal fade" id="order_verify_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Verify Order</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<div ng-include="templateURLVerify"></div>

				<!-- /form -->

			</div>

		</div>

	</div>

	<!-- view Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Order</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<div ng-include="templateURLView"></div>

				<!-- /form -->

			</div>

		</div>

	</div>


	<div id="myModal" class="modal fade myModal{{key+1}}">
        <div class="modal-dialog modal-lg">              
          <div class="modal-content" style="top:100px;">

            <div class="modal-header">
             <!--  <button type="button" class="close" ng-click="loadFormView(key,POrderGUID)"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>  -->
             
             <h3 class="modal-title h5">Deposite Slip</h3>     	
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="loadFormView(key,POrderGUID)"><span aria-hidden="true">&times;</span></button>                 
            </div>

            <div class="modal-body">
            	<img  src="{{MediaURL}}" class="img-responsive" style="height: 500px; width:500px;">
            </div>
          </div>
        </div>
    </div>

</div><!-- Body/ -->