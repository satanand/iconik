<?php

if (!defined('BASEPATH')) {

	exit('No direct script access allowed');

}



/*------------------------------*/

/*------------------------------*/	

function ValidateUserAccess($PermittedModules, $Path) {

	if(!empty($PermittedModules)){

	foreach($PermittedModules as $Value){

		if($Value['ModuleName'] == $Path){

			return $Value;

		}

	}}

	$Obj =& get_instance();

	$Obj->session->sess_destroy();

	exit("You do not have permission to access this module.");

	return false;

}



/*------------------------------*/

/*------------------------------*/	

function APICall($URL, $JSON='') {

	$CH = curl_init();

	$Headers = array('Accept: application/json', 'Content-Type: application/json');



	curl_setopt($CH, CURLOPT_URL, $URL);

	if ($JSON != '') {

		//curl_setopt($CH, CURLOPT_POST, count($JSON));

		curl_setopt($CH, CURLOPT_POSTFIELDS, $JSON);

	}



	curl_setopt($CH, CURLOPT_SSL_VERIFYPEER, false);

	curl_setopt($CH, CURLOPT_CONNECTTIMEOUT, 50);

	curl_setopt($CH, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($CH, CURLOPT_HTTPHEADER, $Headers);

	curl_setopt($CH, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);



	$Response = curl_exec($CH);
	$Response = json_decode($Response,true);
 //    echo "<pre>";
	// print_r($Response); die;
	// curl_close($CH);

	return $Response;

}





/*------------------------------*/

/*------------------------------*/	

if (!function_exists('response')) {

	function response($data) {

		header('Content-type: application/json');

		echo json_encode($data/*,JSON_NUMERIC_CHECK*/);

		exit;

	}

}


function prepare_report_filters($Where, $separator = '&nbsp;')
{
	$filter_within = "";

	if($Where['filterReportType'] == "enquiries")
	{	
		if(!empty($Where['innerFilterFullName']))
		{
			$word = $Where['innerFilterFullName'];
			$filter_within .= "Full Name:".$word.",$separator";
		}

		if(!empty($Where['innerFilterSource']))
		{
			$word = $Where['innerFilterSource'];
			$filter_within .= "Source:".$word.",$separator";
		}

		if(!empty($Where['innerFilterAssignedto']))
		{			
			$word = $Where['innerFilterAssignedto'];
			$filter_within .= "Assigned to:".$word.",$separator";
		}

		if(!empty($Where['innerFilterInterestedin']))
		{
			$word = $Where['innerFilterInterestedin'];
			$filter_within .= "Interestedin:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCurrentStatus']))
		{
			$word = $Where['innerFilterCurrentStatus'];
			$filter_within .= "Current Status:".$word.",$separator";
		}

		if(!empty($Where['innerFilterEnquiryDate']))
		{
			$word = $Where['innerFilterEnquiryDate'];					
			$filter_within .= "EnquiryDate:".$word.",$separator";
		}

		if(!empty($Where['innerFilterMobile']))
		{
			$word = $Where['innerFilterMobile'];
			$filter_within .= "Mobile:".$word.",$separator";
		}

		if(!empty($Where['innerFilterLatestRemarks']))
		{
			$word = $Where['innerFilterLatestRemarks'];
			$filter_within .= "Latest Remarks:".$word.",$separator";
		}
	}
	elseif($Where['filterReportType'] == "courses")
	{	
		if(!empty($Where['innerFilterStream']))
		{
			$word = $Where['innerFilterStream'];
			$filter_within .= "Stream:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCourse']))
		{
			$word = $Where['innerFilterCourse'];
			$filter_within .= "Course:".$word.",$separator";
		}		
	}
	elseif($Where['filterReportType'] == "fees")
	{			
		if(!empty($Where['innerFilterStream']))
		{
			$word = $Where['innerFilterStream'];
			$filter_within .= "Stream:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCourse']))
		{
			$word = $Where['innerFilterCourse'];
			$filter_within .= "Course:".$word.",$separator";
		}

		if(!empty($Where['innerFilterStudentName']))
		{
			$word = $Where['innerFilterStudentName'];
			$filter_within .= "Student Name:".$word.",$separator";
		}
	}
	elseif($Where['filterReportType'] == "fee_collections")
	{
		if(!empty($Where['innerFilterDateofPayment']))
		{
			$word = $Where['innerFilterDateofPayment'];					
			$filter_within .= "PaymentDate:".$word.",$separator";
		}

		if(!empty($Where['innerFilterStudentName']))
		{
			$word = $Where['innerFilterStudentName'];
			$filter_within .= "Student Name:".$word.",$separator";
		}

		if(!empty($Where['innerFilterBatch']))
		{
			$word = $Where['innerFilterBatch'];
			$filter_within .= "Batch:".$word.",$separator";
		}

		if(!empty($Where['innerFilterAmountPaid']))
		{
			$word = $Where['innerFilterAmountPaid'];
			$filter_within .= "Amount Paid:".$word.",$separator";
		}

		if(!empty($Where['innerFilterMode']))
		{
			$word = $Where['innerFilterMode'];
			$filter_within .= "Mode:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCollectedBy']))
		{
			$word = $Where['innerFilterCollectedBy'];
			$filter_within .= "Collected By:".$word.",$separator";
		}
	}
	elseif($Where['filterReportType'] == "batches")
	{
		if(!empty($Where['innerFilterBatchCreationDate']))
		{
			$word = $Where['innerFilterBatchCreationDate'];					
			$filter_within .= "Batch Creation Date:".$word.",$separator";
		}

		if(!empty($Where['innerFilterBatchStartDate']))
		{
			$word = $Where['innerFilterBatchStartDate'];					
			$filter_within .= "Batch Start Date:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCourse']))
		{
			$word = $Where['innerFilterCourse'];
			$filter_within .= "Course:".$word.",$separator";
		}

		if(!empty($Where['innerFilterBatchName']))
		{
			$word = $Where['innerFilterBatchName'];
			$filter_within .= "Batch Name:".$word.",$separator";
		}

		if(!empty($Where['innerFilterDuration']))
		{
			$word = $Where['innerFilterDuration'];
			$filter_within .= "Duration:".$word.",$separator";
		}

		if(!empty($Where['innerFilterSubject']))
		{
			$word = $Where['innerFilterSubject'];
			$filter_within .= "Subject:".$word.",$separator";
		}

		if(!empty($Where['innerFilterFaculty']))
		{
			$word = $Where['innerFilterFaculty'];
			$filter_within .= "Faculty:".$word.",$separator";
		}
	}
	elseif($Where['filterReportType'] == "batch_attendance_details")
	{
		if(!empty($Where['innerFilterStream']))
		{
			$word = $Where['innerFilterStream'];				
			$filter_within .= "Stream:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCourse']))
		{
			$word = $Where['innerFilterCourse'];
			$filter_within .= "Course:".$word.",$separator";
		}

		if(!empty($Where['innerFilterBatchDetails']))
		{
			$word = $Where['innerFilterBatchDetails'];
			$filter_within .= "Batch Details:".$word.",$separator";
		}

		if(!empty($Where['innerFilterStudentName']))
		{
			$word = $Where['innerFilterStudentName'];
			$filter_within .= "Student Name:".$word.",$separator";
		}		
	}
	elseif($Where['filterReportType'] == "student_details")
	{			
		if(!empty($Where['innerFilterStream']))
		{
			$word = $Where['innerFilterStream'];				
			$filter_within .= "Stream:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCourse']))
		{
			$word = $Where['innerFilterCourse'];
			$filter_within .= "Course:".$word.",$separator";
		}

		if(!empty($Where['innerFilterStudentName']))
		{
			$word = $Where['innerFilterStudentName'];
			$filter_within .= "Student Name:".$word.",$separator";
		}

		if(!empty($Where['innerFilterBatchDetails']))
		{
			$word = $Where['innerFilterBatchDetails'];
			$filter_within .= "Batch Details:".$word.",$separator";
		}

		if(!empty($Where['innerFilterMobile']))
		{
			$word = $Where['innerFilterMobile'];
			$filter_within .= "Mobile:".$word.",$separator";
		}

		if(!empty($Where['innerFilterEmail']))
		{
			$word = $Where['innerFilterEmail'];
			$filter_within .= "Email:".$word.",$separator";
		}

		if(!empty($Where['innerFilterRegistrationDate']))
		{
			$word = $Where['innerFilterRegistrationDate'];
			$filter_within .= "RegistrationDate:".$word.",$separator";
		}

		if(!empty($Where['innerFilterAddress']))
		{
			$word = $Where['innerFilterAddress'];
			$filter_within .= "Address:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCityName']))
		{
			$word = $Where['innerFilterCityName'];
			$filter_within .= "City Name:".$word.",$separator";
		}

		if(!empty($Where['innerFilterStateName']))
		{
			$word = $Where['innerFilterStateName'];
			$filter_within .= "State Name:".$word.",$separator";
		}	
	}
	elseif($Where['filterReportType'] == "student_key_details")
	{			
		if(!empty($Where['innerFilterKey']))
		{
			$word = $Where['innerFilterKey'];
			$filter_within .= "Key:".$word.",$separator";
		}

		if(!empty($Where['innerFilterStatus']))
		{
			$word = $Where['innerFilterStatus'];
			$filter_within .= "Status:".$word.",$separator";
		}		

		if(!empty($Where['innerFilterAssignedDate']))
		{
			$word = $Where['innerFilterAssignedDate'];
			$filter_within .= "Assigned Date:".$word.",$separator";
		}

		if(!empty($Where['innerFilterActivationDate']))
		{
			$word = $Where['innerFilterActivationDate'];
			$filter_within .= "Activation Date:".$word.",$separator";
		}

		if(!empty($Where['innerFilterExpiryDate']))
		{
			$word = $Where['innerFilterExpiryDate'];					
			$filter_within .= "Expiry Date:".$word.",$separator";
		}

		
		if(!empty($Where['innerFilterAssignedTo']))
		{
			$word = trim($Where['innerFilterAssignedTo']);
			$filter_within .= "Assigned To:".$word.",$separator";
		}

		if(!empty($Where['innerFilterFullName']))
		{
			$word = $Where['innerFilterFullName'];
			$filter_within .= "Full Name:".$word.",$separator";
		}

		/*if(!empty($Where['innerFilterStream']))
		{
			$word = $Where['innerFilterStream'];				
			$filter_within .= "Stream:".$word.",$separator";
		}

		if(!empty($Where['innerFilterCourse']))
		{
			$word = $Where['innerFilterCourse'];
			$filter_within .= "Course:".$word.",$separator";
		}
		
		if(!empty($Where['innerFilterBatchDetails']))
		{
			$word = $Where['innerFilterBatchDetails'];
			$filter_within .= "Batch Details:".$word.",$separator";
		}
		*/		
	}

	return $filter_within;
}
