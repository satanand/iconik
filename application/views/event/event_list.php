<style type="text/css">
   .glyphicon {
   position: relative;
   top: -4px;
   display: inline-block;
   font-family: 'Glyphicons Halflings';
   font-style: normal;
   font-weight: normal;
   line-height: 1;
   -webkit-font-smoothing: antialiased;
   font-size: 20px;
   padding: 4px;
   }
</style>
<header class="panel-heading">
   <h1 class="h4">Manage Events</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body">
   <!-- Body -->
   <!-- Top container -->
   <div class="clearfix mt-2 mb-2">
      <div class="row">
         <div class="col-md-6">
               <div class="form-group">
                  <span class="h5">Total Events: {{data.dataList.length}}</span>
               </div>   
         </div> 

         <div class="col-md-6 float-right">
            <div class="float-right">
               <button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Event</button>
            </div>
         </div>     
      </div>


      <div class="row">
         <!-- <div class="col-md-3">
            <div class="form-group">
            <select class="form-control" name="Category" id="Category" onchange="getCategorybyList(this.value)">
            <option value="">All Activity</option>
            <option ng-repeat="(key, rowc) in data.CategoryData" value="{{rowc.Category}}">{{rowc.Category}}</option>
            </select>
            </div>
            </div> -->
         <div class="col-md-4">
            <div class="form-group">               
               <form class="form-inline" id="filterForm" role="form" autocomplete="off">
                  <div id="universal_search">
                     <input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Type And Search">
                  </div>
               </form>
            </div>
         </div>

         <div class="col-md-3">
            <div class="form-group">
               <button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
               <button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
            </div>
         </div>


       
        
         <!-- <div class="col-md-4">
            <div class="float-right">
            	<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAddCategory();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add New Activity</button>
            
            </div>
            
            </div> -->
        
      </div>
     
   </div>
   <!-- Top container/ -->
   <!-- Data table -->
   <div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0">
      <!-- loading -->
      <p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
      <!-- data table -->
      <table class="table table-striped table-condensed table-hover table-sortable">
         <!-- table heading -->
         <thead>
            <tr>
               <th style="width: 400px;">Event Name</th>
               <!-- <th style="width: 200px;">Multiple Activity</th> -->
               <th style="width: 400px;">Address-Date/Time</th>
               <th style="width: 200px;">Event Image</th>
               <!-- <th style="width: 200px;">Description</th> -->
               <th style="width: 150px;" class="text-center">Action</th>
            </tr>
         </thead>
         <!-- table body -->
         <tbody id="tabledivbody">
            <tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">
               <td>
                  <strong>{{row.EventName}}</strong>
               </td>
               <!-- 	<td>
                  <strong ng-repeat="cate in row.CategoryData">{{cate.Category}} </strong>
                  
                  </td> -->
               <td>
                  Start Date: <strong>{{row.EventStartDateTime}} </strong><br>
                  End Date: <strong>{{row.EventEndDateTime}} </strong><br> 
                  Address: <strong>{{row.Address}}</strong>
               </td>
               <td>
                  <img src="{{row.MediaThumbURL}}">
               </td>
               <!-- <td >
                  <strong ng-bind-html="row.EventDescription">{{row.EventDescription}}</strong>
                  
                  </td> -->
               <td class="text-center" style="text-align: center;">
                  <a style="text-decoration: underline;color: blue;" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-click="loadFormEdit(key, row.EventGUID)">Manage Event</a><br>
                  <a style="text-decoration: underline;color: blue;" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-click="loadFormActivity(key, row.EventGUID)">Manage Activities</a><br>
                  <a style="text-decoration: underline;color: blue;" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-click="loadEventParticipants(key, row.EventGUID)">Manage Participant</a><br>
                   <a style="text-decoration: underline;color: blue;" ng-click="loadFormView(key, row.EventGUID)" data-toggle="tooltip" title="View">View Event</a><br>
                  <a style="text-decoration: underline;color: blue;" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-click="loadFormEventDelete(key, row.EventGUID)">Delete Event</a>
                 
                    <!--  <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.EventGUID)" data-toggle="tooltip" title="Edit"></a> -->
                  <!-- <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormEventDelete(key, row.EventGUID)" data-toggle="tooltip" title="Delete"></a> -->
               </td>
            </tr>
         </tbody>
      </table>
      <!-- no record -->
      <p class="no-records text-center" ng-if="data.noRecords">
         <span ng-if="data.dataList.length">No more records found.</span>
         <span ng-if="!data.dataList.length">No records found.</span>
      </p>
   </div>
   <!-- Data table/ -->
   <!-- add Book Category -->
   <div class="modal fade" id="addcategory_model">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Add New Activity</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="cancelAddActivity()"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="category_form" name="category_form" autocomplete="off" ng-include="templateURLCategory">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- add Modal -->
   <div class="modal fade" id="add_model">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Add Event</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- View Modal -->
   <div class="modal fade" id="View_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">View Event</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- edit Modal -->
   <div class="modal fade" id="edit_model">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Manage Event</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
   <div class="modal fade" id="view_activity_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Activiy Details</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="cancelActivityView()"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <div ng-include="templateURLActivityView">
            </div>
            <!-- /form -->
         </div>
      </div>
   </div>
   <div class="modal fade" id="edit_activity_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Edit Activity Details</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="cancelActivityView()"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <div ng-include="templateURLActivityEdit">
            </div>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- Manage Activity -->
   <div class="modal fade" id="manage_activity_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Manage Activity</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <div ng-include="templateURLActivity"></div>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- Manage Participants -->
   <div class="modal fade" id="manage_participants_model">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Manage Participants</h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <div ng-include="templateURLParticipants"></div>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- issunebook_model form -->
   <div class="modal fade" id="issunebook_model">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Book Issue </h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="issuebook_form" name="issuebook_form" autocomplete="off" ng-include="templateURLIssueBook">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- returnbook_model form -->
   <div class="modal fade" id="returnbook_model">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Book Return </h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="returnbook_form" name="returnbook_form" autocomplete="off" ng-include="templateURLReturnBook">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
   <!-- returnbook_model form -->
   <div class="modal fade" id="delete_model">
      <div class="modal-dialog modal-md" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h3 class="modal-title h5">Delete </h3>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <!-- form -->
            <form id="returnbook_form" name="returnbook_form" autocomplete="off" ng-include="templateURLDelete">
            </form>
            <!-- /form -->
         </div>
      </div>
   </div>
</div>
<!-- Body/ -->