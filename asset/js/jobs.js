/*app.filter('removeHTMLTags', function() 
{
    return function(text) 
    {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});*/


app.controller('PageController', function($scope, $http, $timeout) 
{
    $( "#FilterStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    


    $scope.data.qualification = [];

    $scope.data.states = [];


	$scope.getFilterData = function()
	{
		/*$scope.data.qualification = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'qualification/getQualification', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ 

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.qualification.push(response.Data.Records[i]);
            	}

         	}

        });*/


        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }    
	}


	$scope.applyFilter = function() 
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
        $scope.getFilterData();
    }


	$scope.getList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'jobs/getJobs', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            { 

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                 	$scope.data.dataList.push(response.Data.Records[i]);
            	}

                $scope.data.pageNo++;
         	}
            else
            {
            	$scope.data.noRecords = true;
        	}

	        $scope.data.listLoading = false;
	    });

    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) 
    {
        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_jobs.htm?' + Math.random();

        $('#add_model').modal({
            show: true
        });
        var currentTime = new Date();

        $timeout(function() {

            $( "#StartDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              onClose: function( selectedDate ) {
                $( "#EndDate" ).datepicker( "option", "minDate", selectedDate );
              }
            });
            $( "#EndDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              onClose: function( selectedDate ) {
                $( "#StartDate" ).datepicker( "option", "maxDate", selectedDate );
              }
            });

            tinymce.init({
                selector: '#Description'
            });

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 700);

        $scope.getQualification();
        $scope.getStates();
        
    }


    $scope.addData = function() 
    {
        $scope.addDataLoading = true;
        tinyMCE.triggerSave();
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();

        $http.post(API_URL + 'jobs/add', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                /* success case */

                SuccessPopup(response.Message);
                $scope.applyFilter();
                $timeout(function() {

                    $('.modal-header .close').click();

                    window.location.href = window.location.href;

                }, 200);

            } 
            else 
            {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }


    $scope.editData = function() {
        $scope.addDataLoading = true;
        tinyMCE.triggerSave();
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();

        $http.post(API_URL + 'jobs/edit', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);
               $scope.applyFilter();
                $timeout(function() {

                    $('.modal-header .close').click();

                    window.location.href = window.location.href;

                }, 200);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });


    }



    $scope.loadFormJobsDelete = function(Position, PostJobGUID) {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL + 'jobs/delete', 'SessionKey=' + SessionKey + '&PostJobGUID=' + PostJobGUID, contentType).then(function(response) {
				var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.applyFilter();
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


    $scope.loadFormView = function(Position, PostJobGUID, Data) {
        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLView = PATH_TEMPLATE + module + '/view_jobs.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#View_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }

    $scope.loadFormViewApplicants = function(Position, PostJobGUID, Data) {
        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLView = PATH_TEMPLATE + 'jobs/view_applicants.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#View_model').modal({
            show: true
        });

        $scope.data.pageLoading = false;
    }


    $scope.loadFormEdit = function(Position, PostJobGUID, Data) {

        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_jobs.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#edits_model').modal({
            show: true
        });


        $timeout(function() 
        {           
            $( "#StartDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              onClose: function( selectedDate ) {
                $( "#EndDate" ).datepicker( "option", "minDate", selectedDate );
              }
            });
            $( "#EndDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              onClose: function( selectedDate ) {
                $( "#StartDate" ).datepicker( "option", "maxDate", selectedDate );
              }
            });

            tinymce.init({
                selector: '#Description' 
            });

            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");
        }, 2000);

        $scope.data.pageLoading = false;

        $scope.getQualification();
        $scope.getStates();       

    }


    $scope.getQualification = function()
    {
        var qualificationDropDown = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'qualification/getQualification', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            { 
                $scope.data.qualification = [];

                for (var i in response.Data.Records) 
                {
                    $scope.data.qualification.push(response.Data.Records[i]);

                    qualificationDropDown.push(response.Data.Records[i]['Qualification']);
                }

                $( "#Qualification" ).autocomplete({
                source: qualificationDropDown
                });
            }
        });   
    }

    
    $scope.getStates = function()
    {
        var data = 'SessionKey='+SessionKey;       

        $http.post(API_URL+'students/getStatesList', data, contentType).then(function(response) 
        {
            var response = response.data;
            
            if(response.ResponseCode==200 && response.Data)
            { /* success case */

                $scope.data.states = response.Data;

                /*var option = "";

                for (var i in $scope.data.states) 
                {

                  option += '<option value="'+$scope.data.states[i].State_id+'">'+$scope.data.states[i].StateName+'</option>';
               }
               $scope.StateOptions = option;*/
            }
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

        });

    }





    $scope.getCities = function(State_id)
    {
        var data = 'SessionKey='+SessionKey+'&State_id='+State_id;

        $scope.data.cities = [];

        $scope.data.citylistLoading = true;

        $http.post(API_URL+'students/getCitiesList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data)
            { 
                for (var i in response.Data) 
                {
                     $scope.data.cities.push(response.Data[i]);
                }

                 $timeout(function()
                 {
                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                }, 200);

                $scope.data.citylistLoading = false;
            }
        });
    }


    $scope.openApplicants = function (PostJobID)
    {
        window.location.href = BASE_URL + "jobs/applicants?PostJobID=" + PostJobID;
    }


    $scope.getApplicants = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'jobs/getApplicants', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }

                $scope.data.pageNo++;
            }
            else
            { 
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });
    }


    $scope.FileInModalBoxLocal = function (url, title, modal="") 
    {
        $scope.data.pageLoading = false;
        if(modal.length){
            $('#'+modal+' .close').click();
        }
        $scope.file_title = title;
        $scope.modal = modal;
        $('#iframe-info').html('<iframe style="width: 100%;height: 400px;border:0px none;" src="https://docs.google.com/gview?url='+url+'&amp;embedded=true"></iframe>');
        $('#extension_popup').modal({
            show : true
        });
    }


    $scope.saveFacultyStatus = function() 
    {
        $scope.saveDataLoading = true;
        
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='facultyStatusForm']").serialize();

        $http.post(API_URL + 'jobs/saveFacultyStatus', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
                
                $timeout(function() 
                {                    
                    window.location.href = window.location.href;
                }, 3000);

            } 
            else 
            {
                ErrorPopup(response.Message);
            }

            $scope.saveDataLoading = false;

        });
    }


    $scope.loadFixInterview = function(FacultyData) 
    {
        $scope.templateURLAdd = PATH_TEMPLATE + 'jobs/fix_interview.htm?' + Math.random();

        $scope.data.FacultyData = FacultyData;

        $('#fix_interview_model').modal({
            show: true
        });    
    }


    $scope.addFixInterview = function() 
    {
        $scope.addInterviewLoading = true;
        
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_fix_interview_form']").serialize();

        $http.post(API_URL + 'jobs/addFixInterview', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
                
                $timeout(function() 
                {
                    $('.modal-header .close').click();

                    window.location.href = window.location.href;

                }, 2000);

            } 
            else 
            {
                ErrorPopup(response.Message);
            }

            $scope.addInterviewLoading = false;

        });
    }
    

    $scope.loadJobLog = function (ApplyID)
    {
        $scope.data.JobLogList = [];
       
        $scope.data.JobLogLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&ApplyID='+ApplyID;

        $http.post(API_URL+'jobs/getJobLog', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                for (var i in response.Data.Records) 
                {
                    $scope.data.JobLogList.push(response.Data.Records[i]);
                }

                $scope.templateURLJobLog = PATH_TEMPLATE + 'jobs/job_log.htm?' + Math.random();

                $('#job_log_model').modal({
                    show: true
                });
            }

            $scope.data.JobLogLoading = false;
        });
    }

});


function getCities(state_id)
{
  angular.element(document.getElementById('content-body')).scope().getCities(state_id);  
}

/*function filterJob()
{
    if($("#FilterStartDate").val() !="" && $("#FilterEndDate").val() !="")
    {
        angular.element(document.getElementById('content-body')).scope().getList(1);
    }
}*/


function RemoveJobFilters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword").val('');

    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function RemoveApplicantsFilters()
{
    $("#FilterStartDate, #FilterEndDate, #FilterKeyword").val('');

    $("#FilterStatus").val(10);

    angular.element(document.getElementById('content-body')).scope().getApplicants(1);
}