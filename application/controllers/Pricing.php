<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"Pricing" => ""
		);



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('pricing/iconik_price');

		$this->load->view('includes/footer');

	}


	public function rsp()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

		);	



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('pricing/retailer_price');

		$this->load->view('includes/footer');

	}

}

