<?php
/*-----------------------------------------------------
Enquiry Filters
-----------------------------------------------------*/
?>
<div class="hide_class" id="filters_enquiries">	
<div class="row">
<div class="col-md-4">
<div class="form-group">
	<input name="filterEnquiryKeyword" id="filterEnquiryKeyword" type="text" class="form-control" value="" placeholder="Search by Name, Mobile, Status, Remarks etc.">
</div>
</div>


<div class="col-md-2">
<div class="form-group">
	<select name="filterEnquirySource" id="filterEnquirySource" class="form-control">
		<option value="">Select Source</option>

		<option value="Reference">Reference</option>

		<option value="Website">Website</option>

		<option value="App">App</option>

		<option value="Walkin">Walk-in</option>

		<option value="Advertisement">Advertisement</option>

	</select>
</div>
</div>


<div class="col-md-2">
<div class="form-group">
	<select name="filterEnquiryInterestedIn" id="filterEnquiryInterestedIn" class="form-control">

		<option value="">Select Interested In</option>

		<option ng-repeat="xx in data.InterestedIn" value="{{xx.EnquiryInterestedIn}}">{{xx.EnquiryInterestedIn}}</option>

	</select>
</div>
</div>


<div class="col-md-2">
	<div class="form-group">
		<select name="filterEnquiryAssignedToUser" id="filterEnquiryAssignedToUser" class="form-control">

			<option value="">Select Assigned To</option>

			<option ng-repeat="xx in data.Users" value="{{xx.UserID}}">{{xx.FirstName}}&nbsp;{{xx.LastName}}&nbsp;({{xx.UserTypeName}})</option>

		</select>
	</div>
</div>


<div class="col-md-2">
<div class="form-group">
	<select name="filterEnquiryStatus" id="filterEnquiryStatus" class="form-control">
		<option value="">Select Status</option>

		<option value="1">Hot</option>

		<option value="2">Mild</option>

		<option value="3">Cold</option>

		<option value="4">Closed</option>

		<option value="5">Dropped</option>
	</select>
</div>
</div>
</div>
</div>


<?php
/*-----------------------------------------------------
Courses Filters
-----------------------------------------------------*/
?>