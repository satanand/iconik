<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Batch_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    function getAttendanceData($EntityID,$Where=array()){
        $this->db->select("EntryDate,AttendanceStatus");
        $this->db->from("tbl_attendance");
        $this->db->where("StudentIDs",$EntityID);
        
        if(!empty($Where['StartDate']) && !empty($Where['EndDate'])){
            $this->db->where('EntryDate between "'.date("Y-m-d", strtotime($Where['StartDate'])).'" and "'.date("Y-m-d", strtotime($Where['EndDate'])).'"');
        }        
        $this->db->order_by("EntryDate","DESC");
        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if($query->num_rows() > 0){
            $data = $query->result_array();
            return $data;
        }
    }


    
    function getAttendanceCountByBatch($EntityID,$Input=array())
    {
        $newArr = array();
        
        $StartDate = date("Y-m-d", strtotime($Input['StartDate']));
        $EndDate = date("Y-m-d", strtotime($Input['EndDate']));

        $sql = 'SELECT DATE(a.EntryDate) as EntryDate, BatchID
        FROM tbl_attendance a
        WHERE a.BatchID = "'.$Input['BatchID'].'" AND DATE(a.EntryDate) >= "'.$StartDate.'" AND DATE(a.EntryDate) <= "'.$EndDate.'" 
        GROUP BY DATE(a.EntryDate)';

        $Query = $this->db->query($sql);    
                    
        if($Query->num_rows() > 0)
        {
            foreach ($Query->result_array() as $record) 
            {
                $AttendanceStatus = array("Present"=>0,"Absent"=>0,"OnLeave"=>0);

                foreach ($AttendanceStatus as $key => $value) 
                {
                    $EntryDate = $record['EntryDate'];
                    $BatchID = $record['BatchID'];

                    $sql = "SELECT count(AttendanceID) as total
                            FROM tbl_attendance a
                            WHERE DATE(a.EntryDate) = '".$EntryDate."' AND a.BatchID = '".$BatchID."' AND AttendanceStatus = '".$key."'";

                    
                    $que = $this->db->query($sql);    
                                        
                    if($que->num_rows() > 0)
                    {
                        $ID = $que->result();

                        $record[$key] = $ID[0]-> total;
                    }
                    else
                    {
                        $record[$key] = 0;
                    }
                }

                $newArr[] = $record;
                
            }
            return $newArr; 
        } 
        else
        {
            return false; 
        }
    }


    function getAttendanceDetailByBatch($EntityID,$Input=array())
    {
        $newArr = array();
       $sql = 'SELECT a.BatchID, a.EntryDate,a.AttendanceStatus,u.FirstName,u.LastName,u.Email,u.PhoneNumber
        FROM tbl_attendance a
        Inner join tbl_users u ON u.UserID = a.StudentIDs
        WHERE a.BatchID = "'.$Input['BatchID'].'" AND a.EntryDate = "'.$Input['EntryDate'].'" and a.AttendanceStatus = "'.$Input['Status'].'" order by a.AttendanceID ASC';

        $Query = $this->db->query($sql);    
                    
        if($Query->num_rows() > 0)
        {
            foreach ($Query->result_array() as $record) {
                $newArr[] = $record;
            }
            return $newArr; 
        } else{
            return false; 
        }
    }
    
    
    /*
    Description: 	Use to add new Batch
    */
    function addBatch($data)
    {
        $this->UserID   = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('B.BatchID');
        $this->db->from('tbl_batch B');
        $this->db->join('tbl_entity E', 'E.EntityGUID = B.BatchGUID');
        $this->db->where('LOWER(`BatchName`)="' . strtolower($data['BatchName']) . '"');
        $this->db->where('E.InstituteID', $InstituteID);
        
        $query = $this->db->get();
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            
            return 'EXIST';
            
        } else {
            
            $EntityGUID = get_guid();
            
            
            $EntityID = $this->Entity_model->addEntitys($EntityGUID, $Input = array(
                'UserID' => $this->UserID,
                "EntityTypeID" => 16,
                'InstituteID' => $InstituteID,
                "StatusID" => 1
            ));
            
            $originalDate = $data['StartDate'];
            $newDate      = date("Y-m-d", strtotime($originalDate));
            
            /* Add Batch */
            $InsertData = array_filter(array(
                "BatchID" => $EntityID,
                "BatchGUID" => $EntityGUID,
                "BatchName" => $data['BatchName'],
                "Duration" => $data['Duration'], 
                "StartDate" => $newDate,
                "CourseID" => $data['CourseID']
                
            ));
            
            $this->db->insert('tbl_batch', $InsertData);            
            
            //sendPushMessage($this->UserID, "New Batch Created: ".$data['BatchName'], $Data = array(), $package_name = "com.iconik.qikllp.CourseModule"); 

            return TRUE;
        }
        
    }
    /*
    Description: 	Use to add Batch Asaining
    */
    function addBatchAsaining($data = array())
    {

        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('BatchID');
        $this->db->from('tbl_batchbyfaculty');
        $this->db->where('BatchID',$data['BatchID']);
        $this->db->where('FacultyID',$data['FacultyID']);
        $this->db->where('InstituteID', $InstituteID);
        
        $query = $this->db->get();
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            
            return 'EXIST';
            
        } else {
        
        
        $InsertData = array_filter(array(
            
            'BatchID' => $data['BatchID'],
            'FacultyID' => $data['FacultyID'],
            'InstituteID' => $InstituteID
            
        ));
        
        //print_r($InsertData); die();
        
        $this->db->insert('tbl_batchbyfaculty', $InsertData);
        }
        
        return TRUE;
        
    }
    
    
    
    /*
    Description: 	Use to update Batch.
    */
    function editBatch($data)
    {
        
        $this->UserID   = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('B.BatchID');
        $this->db->from('tbl_batch B');
        $this->db->join('tbl_entity E', 'E.EntityGUID = B.BatchGUID');
        $this->db->where('LOWER(`BatchName`)="' . strtolower($data['BatchName']) . '"');
        $this->db->where('E.InstituteID', $InstituteID);
        $this->db->where('B.BatchGUID !=', $data['BatchGUID']);
        
        $query = $this->db->get();
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            
            return 'EXIST';
            
        } else {
            
            $originalDate = $data['StartDate'];
            $newDate      = date("Y-m-d", strtotime($originalDate));

            $Durationdata = $this->Common_model->getDuration("",$data['CourseID']);

            //print_r($Durationdata[0]['Duration']);
            $Durationdata = $Durationdata[0]['Duration'];

            if(intval($Durationdata) == intval($data['Duration'])){
                 $DuratioExtendMonth = 0;
            }else if(intval($Durationdata) > intval($data['Duration'])){
                $DuratioExtendMonth = 0; 
            }else if(intval($Durationdata) < intval($data['Duration'])){
                $DuratioExtendMonth = intval($data['Duration']) - intval($data['LastDuration']);
            }

            // if($DuratioExtendMonth == 0 && $data['BatchName']){
                
            // }
            
            
            $UpdateArray = array(
                "BatchName" => $data['BatchName'],
                "Duration" => $data['Duration'],
                "StartDate" => $newDate,
                "CourseID" => $data['CourseID'],
                "CourseDuration" =>  $Durationdata,
                "DuratioExtendMonth" => $DuratioExtendMonth
            );
            
            if (!empty($UpdateArray)) {
                $this->db->where('BatchGUID', $data['BatchGUID']);
                $this->db->limit(1);
                $data = $this->db->update('tbl_batch', $UpdateArray);
               // echo $this->db->last_query(); die;
            }
        }
        
        return TRUE;
    }



    /*
    Description: 	Use to update editAssignData.
    */
    function editAssignData($data = array())
    {
        
        $UpdateArray = array_filter(array(
            
            'BatchID' => $data['BatchID'],
            'FacultyID' => $data['FacultyID']
            
        ));
        
        if (!empty($UpdateArray)) {
            $this->db->where('AsID', $data['AsID']);
            $this->db->limit(1);
            $data = $this->db->update('tbl_batchbyfaculty', $UpdateArray);
        }
        
        
        return TRUE;
    }
     
    
    /* get batch data*/
    function getBatch($BatchID, $Where = array(), $multiRecords = FALSE, $PageNo = 1, $PageSize = 10)
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        if (!empty($BatchID) && $BatchID != 'undefined') {
            
            $this->db->select('bf.AsID,B.BatchName,B.BatchID,B.BatchGUID,CONCAT_WS(" ",u.FirstName,u.LastName) FullName,u.UserID');
            $this->db->from('tbl_batch B');
            $this->db->join('tbl_batchbyfaculty bf', 'bf.BatchID = B.BatchID', 'left');
            $this->db->join('tbl_users u', 'u.UserID = bf.FacultyID', 'left');
            $this->db->where("bf.InstituteID", $InstituteID);
            $this->db->where("bf.BatchID", $BatchID);
            $this->db->where("B.StatusID != 3");
            $this->db->where("bf.AssignStatus != 3");
            
            if (!empty($Where['CourseID'])) {
                $this->db->where("B.CourseID", $Where['CourseID']);
            }

            if(isset($Where['CategoryID']) && !empty($Where['CategoryID']))
            {
                $this->db->where("B.CourseID", $Where['CategoryID']);
            }

            if (!empty($Where['Keyword'])) {
                $this->db->group_start();

                $this->db->like("u.FirstName", $Where['Keyword']);
                $this->db->or_like("u.LastName", $Where['Keyword']);
                $this->db->or_like("CONCAT_WS('',u.FirstName,u.Middlename,u.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
                $this->db->or_like("B.BatchName", $Where['Keyword']);
               $this->db->group_end();
            }
           
            $this->db->order_by('bf.AsID', 'DESC');
            
        } else {
            
            $this->db->select('B.BatchName,B.BatchID,B.BatchGUID,B.StartDate,B.Duration,C.CategoryName');
            $this->db->from('tbl_batch B');
            $this->db->join('tbl_entity E', 'E.EntityGUID = B.BatchGUID', 'left');
            $this->db->join('set_categories C', 'C.CategoryID = B.CourseID', 'left');
            $this->db->where("B.StatusID != 3");

            if (!empty($Where['CourseID'])) {
                $this->db->where("B.CourseID", $Where['CourseID']);
            }

            if(isset($Where['CategoryID']) && !empty($Where['CategoryID']))
            {
                $this->db->where("B.CourseID", $Where['CategoryID']);
            }
            
             if (!empty($Where['Keyword'])) {
                $this->db->group_start();

                $this->db->like("C.CategoryName", $Where['Keyword']);
                $this->db->or_like("B.BatchName", $Where['Keyword']);
                $this->db->or_like("B.Duration", $Where['Keyword']);
                $this->db->or_like("B.StartDate", $Where['Keyword']);
               $this->db->group_end();
            }
            $this->db->where("E.InstituteID", $InstituteID);
            $this->db->order_by('B.BatchID', 'DESC');
            
        }
        
        if ($multiRecords) {
            $TempOBJ                        = clone $this->db;
            $TempQ                          = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize));
            /*for pagination*/
        } else {
            $this->db->limit(1);
        }
        $Query = $this->db->get();
        // echo $this->db->last_query(); die();
        if ($Query->num_rows() > 0) {
            foreach ($Query->result_array() as $Record) {
                $Records[] = $Record;
            }
            $Return['Data']['Records'] = $Records;
            
            return $Return;
        }
        return FALSE;
    }
    
    /* get batch getBatchcreate*/
    
    function getBatchcreate($FacultyID)
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        
        $this->db->select('B.BatchName,B.BatchID,B.BatchGUID');
        $this->db->from('tbl_batch B');
        $this->db->join('tbl_batchbyfaculty bf', 'bf.BatchID = B.BatchID');
        $this->db->where("bf.FacultyID", $FacultyID);
        $this->db->order_by('BatchName', 'ASC');
        
        if ($multiRecords) {
            $TempOBJ                        = clone $this->db;
            $TempQ                          = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize));
            /*for pagination*/
        } else {
            $this->db->limit(1);
        }
        $Query = $this->db->get();
        echo $this->db->last_query();
        die();
        if ($Query->num_rows() > 0) {
            foreach ($Query->result_array() as $Record) {
                $Records[] = $Record;
            }
            $Return['Data']['Records'] = $Records;
            
            return $Return;
        }
        return FALSE;
    }
    
    /* get get Batchs Assign data*/
    function getBatchsAssign($AsID)
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('bf.AsID,B.BatchName,B.BatchID,B.BatchGUID,u.UserGUID,u.UserID,CONCAT_WS(" ",u.FirstName,u.LastName) FullName,st.CategoryName,st.CategoryID');
        $this->db->from('tbl_batch B');
        $this->db->join('tbl_batchbyfaculty bf', 'bf.BatchID = B.BatchID', 'left');
        $this->db->join('tbl_users u', 'u.UserID = bf.FacultyID', 'left');
        $this->db->join('set_categories st', 'st.CategoryID = B.CourseID', 'left');
        $this->db->where("bf.AsID", $AsID);
        $this->db->limit(1);
        
        $Query = $this->db->get();
        //echo $this->db->last_query();die();
        if ($Query->num_rows() > 0) {
            
            foreach ($Query->result_array() as $Record) {
                
                
                $Records = $Record;
            }
            //$Return['Records'] = $Records;
            return $Records;
        }
        return FALSE;
    }
    
    
    /*edit get Batch data*/
    function getBatchByID($BatchGUID)
    {
        
        $this->db->select("b.*,DATE_FORMAT(b.StartDate,'%d-%m-%Y') StartDate,c.CategoryName", false);
        $this->db->from('tbl_batch b');
        $this->db->join('set_categories c', 'b.CourseID=c.CategoryID');
        $this->db->limit(1);
        if (!empty($BatchGUID)) {
            $this->db->where("BatchGUID", $BatchGUID);
        }
        $Query = $this->db->get();
        //echo $this->db->last_query(); die()	
        if ($Query->num_rows() > 0) {
            foreach ($Query->result_array() as $Record) {
                
                
                $Records = $Record;
            }
            //$Return['Records'] = $Records;
            return $Records;
        }
        return FALSE;
    }
    
    /*
    Description: 	Use to get Batch
    */
    function getBatchd($BatchGUID, $Where = array(), $multiRecords = FALSE, $PageNo = 1, $PageSize = 10)
    {
        $this->db->select("b.*,DATE_FORMAT(b.StartDate,'%d-%m-%Y') StartDate,c.CategoryName", false);
        
        //$this->db->where('Entity',$EntityGUID);
        
        $this->db->from('tbl_batch b');
        $this->db->join('set_categories c', 'c.CategoryID = b.CourseID', 'left');
        $this->db->where("BatchGUID", $BatchGUID);
        $this->db->order_by('CategoryName', 'ASC');
        
        if ($multiRecords) {
            $TempOBJ                        = clone $this->db;
            $TempQ                          = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize));
            /*for pagination*/
        } else {
            $this->db->limit(1);
        }
        
        
        $Query = $this->db->get();
        if ($Query->num_rows() > 0) {
            foreach ($Query->result_array() as $Record) {
                if (!$multiRecords) {
                    return $Record;
                }
                $Records[] = $Record;
            }
            $Return['Data']['Records'] = $Records;
            
            return $Return;
        }
        return FALSE;
    }
    
    /* get Categories */
    function geCategories($CategoryGUID = '')
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select("CategoryName,CategoryGUID,CategoryID");
        $this->db->from("set_categories");
        $this->db->join("tbl_entity as E", "E.EntityID = set_categories.CategoryID");
        $this->db->join("tbl_entity as EP", "EP.EntityID = set_categories.ParentCategoryID");
        $this->db->where("E.StatusID", 2);
        $this->db->where("EP.StatusID", 2);
        $this->db->where("EP.InstituteID", $InstituteID);
        $this->db->where("CategoryTypeID", 2);

        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if($query->num_rows() > 0){
            $course = $query->result_array();
            return  $course;
        }
        return FALSE;
        
    }
    
    /* get Categories */
    function getFaculty($CourseID)
    {
        
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('U.UserID,U.UserGUID,CONCAT_WS(" ",U.FirstName,U.LastName) FullName');
        $this->db->from('tbl_users U');
        $this->db->join('tbl_entity E', 'E.EntityGUID = U.UserGUID', 'left');
        $this->db->join('tbl_user_jobs UJ', 'UJ.UserID = U.UserID');
        $this->db->where("U.FirstName!=", '');
        $this->db->where("U.UserGUID!=", '');
        $this->db->where("U.UserTypeID", 11);
        $this->db->like("UJ.CourseID", $CourseID);
        $this->db->where("E.InstituteID", $InstituteID);
        $this->db->where("E.StatusID", 2);
        $this->db->GROUP_BY('UserID');
        $this->db->order_by('FullName', 'ASC');
        $Query = $this->db->get();
        //echo $this->db->last_query(); die();
        if ($Query->num_rows() > 0) {
            
            return $Query->result_array();
        }
        return FALSE;
        
    }
    
    function deleteBatch($EntityID, $BatchID)
    {
        $this->db->where("BatchID", $BatchID);
        $this->db->update("tbl_batch", array(
            "StatusID" => 3
        ));
        return TRUE;
    }

    function deleteAssingBatch($AsID)
    {
        $this->db->where("AsID", $AsID);
        $this->db->update("tbl_batchbyfaculty", array(
            "AssignStatus" => 3
        ));
        return TRUE;
    }


    /*
    Description:    Use to get staff  
    */  
    function getStudentsList($EntityID,$BatchID="",$EntryDate = "")
    {

        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

        $this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,s.BatchID');
        $this->db->from('tbl_users u');
        $this->db->join('tbl_entity e','e.EntityID=u.UserID','left'); 
        $this->db->join('tbl_students s','s.StudentID=u.UserID','left');          
        $this->db->where('e.InstituteID',$InstituteID);
        $this->db->where('u.UserTypeID',7);

        if(!empty($UserTypeID)) {
            $this->db->where('u.UserTypeID',$UserTypeID);
        }

        if(!empty($BatchID)) {
            $this->db->where('s.BatchID',$BatchID);
        }

        $this->db->order_by('e.EntityID','DESC');     

        $Query = $this->db->get();  //echo $this->db->last_query();
        $data = $Query->result_array();

        $usertype = array();

        if(count($data) > 0)
        {
            if($EntryDate == "")    $EntryDate = date("Y-m-d");
            else                    $EntryDate = date("Y-m-d", strtotime($EntryDate));
            foreach($data as $Record)
            {  
                $this->db->select("AttendanceStatus");
                $this->db->from("tbl_attendance"); 
                $this->db->where("StudentIDs",$Record['UserID']);
                $this->db->where("DATE(EntryDate) = '".$EntryDate."'");  
                $this->db->limit(1); 
                $AttendanceQuery = $this->db->get(); 
                if($AttendanceQuery->num_rows() > 0)
                {
                    $AttendanceQuery = $AttendanceQuery->result_array();

                    $Record['AttendanceStatus'] = $AttendanceQuery[0]['AttendanceStatus'];
                } 
                             
                $Records[] = $Record;
            }
            return $Records;
        }
        return FALSE;       
    }


    function SetAttendance($EntityID, $Input)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"FirstName,PhoneNumber,CityName");

        $this->db->select("AttendanceID");
        $this->db->from("tbl_attendance");
        $this->db->where("StudentIDs",$Input['UserID']);
        $this->db->where("BatchID",$Input['BatchID']);
        $this->db->like("EntryDate",date("Y-m-d", strtotime($Input['EntryDate'])));
        $this->db->limit(1); 
        $AttendanceQuery = $this->db->get(); 
        if($AttendanceQuery->num_rows() > 0){
            $AttendanceQuery = $AttendanceQuery->result_array();
            $AttendanceID = $AttendanceQuery[0]['AttendanceID']; 

            $Update = array_filter(array(
                "StudentIDs" => $Input['UserID'],
                "EntityID" => $EntityID,
                "BatchID" => $Input['BatchID'],
                "AttendanceStatus" => $Input['AttendanceStatus'],
                "EntryDate" => date("Y-m-d", strtotime($Input['EntryDate']))
            ));             
            
            $this->db->where("AttendanceID",$AttendanceID);          
            $this->db->update('tbl_attendance', $Update);

            $this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,s.GuardianPhone,s.FathersPhone');
            $this->db->from("tbl_students s");
            $this->db->join('tbl_users u', 'u.UserID = s.StudentID');
            $this->db->where("u.UserID",$Input['UserID']);
            $this->db->limit(1); 
            $StudnetQuery = $this->db->get(); 

            if($StudnetQuery->num_rows() > 0){
                $StudnetQuery = $StudnetQuery->result_array();
                $GuardianPhone = $StudnetQuery[0]['GuardianPhone']; 
                $FathersPhone = $StudnetQuery[0]['FathersPhone']; 
                $FullName = $StudnetQuery[0]['FullName']; 
                $message = "your ward ".$FullName." is ".$Input['AttendanceStatus']." for class at ".$InstituteData['FirstName']." on ".date("d-m-Y H:i:s", strtotime($Input['EntryDate'])) ."<br>". "Feel free to talk us on ".$InstituteData['PhoneNumber']."<br>". "Team ".$InstituteData['FirstName'].",".$InstituteData['CityName'].".";
                $GuardianPhone = "7000688248";
                if(!empty($GuardianPhone)){                    
                    sendSMS(array("PhoneNumber"=>$GuardianPhone,"Message"=>$message));
                }else if(!empty($FathersPhone)){
                    sendSMS(array("PhoneNumber"=>$FathersPhone,"Message"=>$message));
                }
            }
        }else{
            $InsertData = array_filter(array(
                "StudentIDs" => $Input['UserID'],
                "EntityID" => $EntityID,
                "BatchID" => $Input['BatchID'],
                "AttendanceStatus" => $Input['AttendanceStatus'],
                "EntryDate" => date("Y-m-d", strtotime($Input['EntryDate']))
            ));            
            $this->db->insert('tbl_attendance', $InsertData);            
            
            $this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,s.GuardianPhone,s.FathersPhone');
            $this->db->from("tbl_students s");
            $this->db->join('tbl_users u', 'u.UserID = s.StudentID');
            $this->db->where("u.UserID",$Input['UserID']);
            $this->db->limit(1); 
            $StudnetQuery = $this->db->get(); 

            if($StudnetQuery->num_rows() > 0){
                $StudnetQuery = $StudnetQuery->result_array();
                $GuardianPhone = $StudnetQuery[0]['GuardianPhone']; 
                $FathersPhone = $StudnetQuery[0]['FathersPhone']; 
                $FullName = $StudnetQuery[0]['FullName']; 
                $message = "your ward ".$FullName." is ".$Input['AttendanceStatus']." for class at ".$InstituteData['FirstName']." on ".date("d-m-Y H:i:s", strtotime($Input['EntryDate'])) ."<br>". "Feel free to talk us on ".$InstituteData['PhoneNumber']."<br>". "Team ".$InstituteData['FirstName'].",".$InstituteData['CityName'].".";
                $GuardianPhone = "7000688248";
                if(!empty($GuardianPhone)){                    
                    sendSMS(array("PhoneNumber"=>$GuardianPhone,"Message"=>$message));
                }else if(!empty($FathersPhone)){
                    sendSMS(array("PhoneNumber"=>$FathersPhone,"Message"=>$message));
                }
            }
        }
        return TRUE;
    }


    function SetAttendanceBulk($EntityID, $BatchID, $Inputs)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"FirstName,PhoneNumber,CityName");

        
       
        $EntryDate = date("Y-m-d", strtotime($Inputs['EntryDate']));
        $EntryDateDis = date("d-m-Y", strtotime($Inputs['EntryDate']));

        $sid_arr = $Inputs['uid'];

        if(isset($sid_arr) && !empty($sid_arr))
        {            
            $sids_str = implode(",", array_keys($sid_arr));

            $sql = "DELETE FROM tbl_attendance WHERE EntityID = '$InstituteID' AND BatchID = '$BatchID' AND EntryDate = '$EntryDate' AND StudentIDs IN ($sids_str) ";
            
            $this->db->query($sql);

            foreach($sid_arr as $id => $att)
            {
                $InsertData = array_filter(array(
                "StudentIDs" => $id,
                "EntityID" => $EntityID,
                "BatchID" => $BatchID,
                "AttendanceStatus" => $att,
                "EntryDate" => $EntryDate
                ));   

                $this->db->insert('tbl_attendance', $InsertData);
            }

            foreach($sid_arr as $id => $att)
            {            
                $this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,s.GuardianPhone,s.FathersPhone,');
                $this->db->from("tbl_students s");
                $this->db->join('tbl_users u', 'u.UserID = s.StudentID');
                $this->db->where("u.UserID", $id);
                $this->db->limit(1); 
                $StudnetQuery = $this->db->get(); 

                if($StudnetQuery->num_rows() > 0)
                {
                    $StudnetQuery = $StudnetQuery->result_array();
                    $GuardianPhone = $StudnetQuery[0]['GuardianPhone']; 
                    $FathersPhone = $StudnetQuery[0]['FathersPhone']; 
                    $FullName = $StudnetQuery[0]['FullName']; 
                    

                    $message = "Your ward ".$FullName." is ".$att." for class at ".$InstituteData['FirstName']." on ". $EntryDateDis .". ". "Feel free to talk us on ".$InstituteData['PhoneNumber'].".". "Team ".$InstituteData['FirstName'].",".$InstituteData['CityName'].".";
                    
                    if(!empty($GuardianPhone))
                    {                    
                        sendSMS(array("PhoneNumber"=>$GuardianPhone,"Message"=>$message));
                    }
                    else if(!empty($FathersPhone))
                    {
                        sendSMS(array("PhoneNumber"=>$FathersPhone,"Message"=>$message));
                    }
                }
            } 
        }
        else
        {
            return 1;
        }        
        
        return 2;
    }



    function SetAttendanceByFaculty($EntityID, $BatchID, $Inputs)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"FirstName,PhoneNumber,CityName");
        
       
        $EntryDate = date("Y-m-d", strtotime($Inputs['EntryDate']));
        $EntryDateDis = date("d-m-Y", strtotime($Inputs['EntryDate']));

        $sid_arr = $Inputs['uid'];

        if(isset($sid_arr) && !empty($sid_arr))
        {            
            $sid_arr_remind = array();

            $sid_arr1 = explode(",", $sid_arr);

            foreach($sid_arr1 as $obj)
            {
                $sid_arr2 = array();
                $sid_arr2 = explode("#", $obj);
                if(isset($sid_arr2) && !empty($sid_arr2) && count($sid_arr2) == 2)
                {
                    $id     = $sid_arr2[0];
                    $att    = $sid_arr2[1];

                    $sid_arr_remind[$id] = $att;


                    $sql = "DELETE FROM tbl_attendance WHERE EntityID = '$InstituteID' AND BatchID = '$BatchID' AND EntryDate = '$EntryDate' AND StudentIDs = $id ";

                    $this->db->query($sql);

                    $InsertData = array_filter(array(
                    "StudentIDs" => $id,
                    "EntityID" => $InstituteID,
                    "BatchID" => $BatchID,
                    "AttendanceStatus" => $att,
                    "EntryDate" => $EntryDate
                    ));   

                    $this->db->insert('tbl_attendance', $InsertData);
                }
            }    

            if(isset($sid_arr_remind) && !empty($sid_arr_remind))
            {
                foreach($sid_arr_remind as $id => $att)
                {            
                    $this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,s.GuardianPhone,s.FathersPhone,');
                    $this->db->from("tbl_students s");
                    $this->db->join('tbl_users u', 'u.UserID = s.StudentID');
                    $this->db->where("u.UserID", $id);
                    $this->db->limit(1); 
                    $StudnetQuery = $this->db->get(); 

                    if($StudnetQuery->num_rows() > 0)
                    {
                        $StudnetQuery = $StudnetQuery->result_array();
                        $GuardianPhone = $StudnetQuery[0]['GuardianPhone']; 
                        $FathersPhone = $StudnetQuery[0]['FathersPhone']; 
                        $FullName = $StudnetQuery[0]['FullName']; 
                        

                        $message = "Your ward ".$FullName." is ".$att." for class at ".$InstituteData['FirstName']." on ". $EntryDateDis .". ". "Feel free to talk us on ".$InstituteData['PhoneNumber'].".". "Team ".$InstituteData['FirstName'].",".$InstituteData['CityName'].".";
                        
                        if(!empty($GuardianPhone))
                        {                    
                            sendSMS(array("PhoneNumber"=>$GuardianPhone,"Message"=>$message));
                        }
                        elseif(!empty($FathersPhone))
                        {
                            sendSMS(array("PhoneNumber"=>$FathersPhone,"Message"=>$message));
                        }
                    }
                }
            }     
        }
        else
        {
            return 1;
        }        
        
        return 2;
    }
    
}