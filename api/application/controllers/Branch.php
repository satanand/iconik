<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('Branch_model');
		$this->load->model('Entity_model');
		$this->load->model('Common_model');
		$this->load->model('Users_model');
	}

	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function add_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('BranchName', 'Branch Name', 'trim|required');
		$this->form_validation->set_rules('Email', 'Email', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail',array("validateEmail"=>'Email addres already in used'));
		$this->form_validation->set_rules('Address', 'Address','trim|required');
		$this->form_validation->set_rules('StateName', 'State Name','trim|required');
		$this->form_validation->set_rules('CityName', 'City Name','trim|required');
		$this->form_validation->set_rules('CityCode', 'CityCode','trim');
		
		$this->form_validation->set_rules('CountryCode', 'CountryCode','trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		/*$BranchData = $this->Branch_model->addBranch(array('BranchName'=>$this->Post['BranchName'],'FirstName'=>$this->Post['FirstName'],'LastName'=>$this->Post['LastName'],'Email'=>$this->Post['Email'],'Address'=>$this->Post['Address'],'CityName'=>$this->Post['CityName'],'StateName'=>$this->Post['StateName'],'CountryCode'=>$this->Post['CountryCode'],'CityCode'=>$this->Post['CityCode']));*/

		$BranchData = $this->Branch_model->addBranch(array('FirstName'=>$this->Post['BranchName'],'Email'=>$this->Post['Email'],'Address'=>$this->Post['Address'],'CityName'=>$this->Post['CityName'],'StateName'=>$this->Post['StateName'],'CountryCode'=>$this->Post['CountryCode'],'CityCode'=>$this->Post['CityCode']));

			//echo $BranchData; die();

			if($BranchData==FALSE){

				$this->Return['Message'] ="Incorrect email id. Invite not Send"; 
				$this->Return['Data'] = $BranchData;

			}elseif($BranchData > 0){

				$this->Return['Message'] =	"New Branch added successfully."; 
				$this->Return['Data'] = $BranchData;
				

			}else{
				
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Sorry! some error occured, please try later.";
		    }

	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getBranch_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		 
	  $BranchData = $this->Branch_model->getBranch('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		//print_r($BranchData); die();
		if(!empty($BranchData)){

			$this->Return['Data'] = $BranchData['Data'];
		}	
	}
	
	/*
	Description: 	Use to get Get single EditBranch.
	URL: 			/api/EditBranch/getEditBranch
	Input (Sample JSON): 		
	*/
	public function getEditBranch_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$EditBranchData = $this->Branch_model->getBranchByID($this->input->post('EntityGUID'));
		//print_r($EditBranchData); die();
		if(!empty($EditBranchData)){
			$this->Return['Data'] = $EditBranchData;
		}	
	}

	public function getProfile_post()

	{

	
		$formData=$this->Users_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array('UserID'=>$this->input->Post('UserID')));

		$UserTypeData = $this->Users_model->getusertype($this->input->Post('UserID'));

		    $this->Return['Data'] = $UserTypeData['Data'];
			$this->Return['Data'] = $formData;


	}

	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function editBranch_post()
	{

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('BranchName', 'Branch Name', 'trim|required');
		
		$this->form_validation->set_rules('Email', 'Email', 'trim|required');
		$this->form_validation->set_rules('Address', 'Address','trim|required');
		$this->form_validation->set_rules('StateName', 'State Name','trim|required');
		$this->form_validation->set_rules('CityName', 'City Name','trim|required');		
		$this->form_validation->set_rules('CountryCode', 'CountryCode','trim|required');
		$this->form_validation->set_rules('UserGUID', 'UserGUID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		$UserData = $this->Branch_model->editBranch(array('FirstName'=>$this->Post['BranchName'],'Email'=>$this->Post['Email'],'Address'=>$this->Post['Address'],'CityName'=>$this->Post['CityName'],'StateName'=>$this->Post['StateName'],'CountryCode'=>$this->Post['CountryCode'],'CityCode'=>$this->Post['CityCode'],'UserGUID'=>$this->Post['UserGUID']));

	    $this->Return['Data'] = $this->Branch_model->getBranchByID($this->input->Post('UserGUID'),array());

	    

	    $this->Return['Message'] =	"Branch updated successfully."; 

		}

		public function editEmailBranch_post()
	    {

			$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
			$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|required');
			$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');
			$this->form_validation->set_rules('MediaURL', 'MediaURL', 'trim');
			$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|required');
			$this->form_validation->validation($this);  /* Run validation */		

			/* Validation - ends */

			$UserData = $this->Branch_model->editEmailBranch(array('Email'=>$this->Post['Email'],'UserGUID'=>$this->Post['UserGUID'],'UserID'=>$this->Post['UserID'],'MediaURL'=>$this->Post['MediaURL'],'FirstName'=>$this->Post['FirstName']));

			//die;
				if($UserData == 1){
					$this->Return['Data'] = $this->Branch_model->getBranchByID($this->input->Post('UserGUID'),array());
					$this->Return['Message'] =	"Invitation sent successfully."; 
				}elseif($UserData == 'exist'){
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message'] ="Email address is already in used";
				}else{
					$this->Return['ResponseCode'] 	=	500;
					$this->Return['Message'] ="Incorrect email id. Invite not Send"; 
				}
			
		}

	public function Inactive_post()

	{

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StatusID', 'Please Checked', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		$UserData = $this->Branch_model->Inactive($this->input->Post('UserGUID'));

	    $this->Return['Data'] = $this->Branch_model->getBranchd($this->input->Post('UserGUID'),array());

	    $this->Return['Message'] =	"Inactive Successfully."; 

		}

public function Active_post()

	{

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('StatusID', 'Please Checked', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		$UserData = $this->Branch_model->Active($this->input->Post('UserGUID'));

	    $this->Return['Data'] = $this->Branch_model->getBranchd($this->input->Post('UserGUID'),array());

	    

	    $this->Return['Message'] =	"Active Successfully."; 

		}

  public function delete_post()
    {

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
	     $this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		$UserData = $this->Branch_model->delete($this->input->Post('UserID'));

	    $this->Return['Data'] = $this->Branch_model->getBranchd($this->input->Post('UserID'),array());
        $this->Return['Message'] =	"Delete Successfully."; 

	}


}