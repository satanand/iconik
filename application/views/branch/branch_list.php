<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<header class="panel-heading"> <h1 class="h4">Manage Branch</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="branches-body" ng-init="getFilterData()"><!-- Body -->
  <!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<span class="float-left records d-none d-sm-block">

			<span class="h5">Total Branch: {{data.totalRecords}}</span>

		</span>
		  <form class="form-inline float-left ml-4" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()">
		  <div id="universal_search">
                <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
                <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
                <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
            </div>
		  </form>
		<div class="float-right">

		<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Send Invitation</button>

	</div>
  </div>

	<!-- Top container/ -->
	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable">

			<!-- table heading -->

			<thead>

				<tr>

                    <th  >Institute Name</th>
                    <th style="width: 300px;">Email</th>
                     <th style="width: 300px;">Address</th>
                    <th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">

				
					<td>
						<strong>{{row.FirstName}}</strong>

					</td>
				
					<td>
						<strong>{{row.Email}} <a href="" ng-if="row.StatusID==1" ng-click="loadFormEmail(key, row.EntityGUID)">Invite Again</a></strong>

					</td>
					<td>
						<strong>{{row.Address}}<span ng-if="row.CityCode!=''">, {{row.CityName}}</span> <span ng-if="row.CityCode!=''">- {{row.CityCode}}</span> <span ng-if="row.StateName!=''">, {{row.StateName}}</span></strong>

					</td>
				
					<td class="text-center" style="text-align: center;">

                   <!--  <a class="glyphicon glyphicon-thumbs-up" href="" ng-click="loadFormInactive(key, row.EntityGUID)" data-toggle="tooltip" title="Active" ng-if="row.StatusID==1"></a> -->

                    <!-- <a class="glyphicon glyphicon-thumbs-down" href="" ng-click="loadFormActive(key, row.EntityGUID)" data-toggle="tooltip" title="Inactive" ng-if="row.StatusID==3"></a>
 -->
                    <a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.UserID)" data-toggle="tooltip" title="View"></a>



                    <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.EntityGUID)" data-toggle="tooltip" title="Edit"></a>

                     <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.UserID)" data-toggle="tooltip" title="Delete"></a>

					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->


	<!-- add Modal -->

	<div class="modal fade" id="email_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Invite Again</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="email_form" name="email_form" autocomplete="off" ng-include="templateURLEmail">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="active_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Inactive</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="active_form" name="active_form" autocomplete="off" ng-include="templateURLActive">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>	
		<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Branch</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="inactive_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Active</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="inactive_form" name="inactive_form" autocomplete="off" ng-include="templateURLInactive">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>	

	<!-- add Modal -->

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Invite Branch</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

<!-- permission form -->

<div class="modal fade" id="permission_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Permission <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="permission_form" name="permission_form" autocomplete="off" ng-include="templateURLPermission" method="POST">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

</div><!-- Body/ -->







