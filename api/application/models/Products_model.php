<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*------------------------------------------------------------------------
	Manage Category
	------------------------------------------------------------------------*/
	function getCategoryList($EntityID, $Where=array())
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = $arr1 = $arr2 = $arr3 = $arr4 = $arr5 = array(); 
		
		//Get Category Only-----------------------
		$sql1 = "SELECT CategoryID, CategoryName
		FROM store_products_category
		ORDER BY CategoryName";


		//Get Sub Category-----------------------
		$sql2 = "SELECT * FROM
		(
			SELECT SubCategoryID, CategoryID, SubCategoryName
			FROM store_products_subcategory
			WHERE InstituteID = '$InstituteID' AND CategoryID > 0

			UNION 

			SELECT SubCategoryID, CategoryID, SubCategoryName
			FROM store_products_subcategory
			WHERE CategoryID > 0

		) as temp
		GROUP BY SubCategoryName
		ORDER BY SubCategoryName		
		";


		//Get Category with Sub Category-----------------------
		$sql3 = "SELECT c.CategoryName, sc.SubCategoryID, sc.SubCategoryName
		FROM store_products_category c
		LEFT JOIN store_products_subcategory sc ON c.CategoryID = sc.CategoryID
		WHERE sc.SubCategoryName IS NOT NULL
		ORDER BY c.CategoryName, sc.SubCategoryName";


		//Get Available Products with Category and Sub Category-----------------------
		$sql4 = "SELECT c.CategoryName, sc.SubCategoryID, sc.SubCategoryName
		FROM store_products_category c
		INNER JOIN store_products_subcategory sc ON c.CategoryID = sc.CategoryID
		INNER JOIN store_products p ON p.CategoryID = c.CategoryID AND p.SubCategoryID = sc.SubCategoryID	
		GROUP BY p.CategoryID, p.SubCategoryID
		ORDER BY c.CategoryName, sc.SubCategoryName";


		//Get Institute having Products-----------------------
		$sql5 = "SELECT p.InstituteID, u.FirstName, u.LastName
		FROM store_products p			
		INNER JOIN tbl_entity e ON e.EntityID = p.InstituteID AND e.StatusID = 2
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		GROUP BY p.InstituteID
		ORDER BY u.FirstName, u.LastName";

		$Query1 = $this->db->query($sql1);
		$Query2 = $this->db->query($sql2);
		$Query3 = $this->db->query($sql3);
		$Query4 = $this->db->query($sql4);
		$Query5 = $this->db->query($sql5);

		if($Query1->num_rows()>0)
		{
			foreach($Query1->result_array() as $Where)
			{
				$arr1[] = $Where;				
			}

			$arr['Category'] = $arr1;			
		}

		
		if($Query2->num_rows()>0)
		{
			foreach($Query2->result_array() as $Where)
			{
				$arr2[] = $Where;				
			}

			$arr['SubCategory'] = $arr2;			
		}


		if($Query3->num_rows()>0)
		{
			foreach($Query3->result_array() as $Where)
			{
				$Where['CategoryName'] = ucfirst($Where['CategoryName']);
				$Where['SubCategoryName'] = ucfirst($Where['SubCategoryName']);

				$arr3[$Where['CategoryName']][] = array("SubCategoryID" => $Where['SubCategoryID'], "SubCategoryName" => $Where['SubCategoryName']);				
			}

			$arr['CategorySubCategory'] = $arr3;			
		}


		if($Query4->num_rows()>0)
		{
			foreach($Query4->result_array() as $Where)
			{
				$Where['CategoryName'] = ucfirst($Where['CategoryName']);
				$Where['SubCategoryName'] = ucfirst($Where['SubCategoryName']);

				$arr4[$Where['CategoryName']][] = array("SubCategoryID" => $Where['SubCategoryID'], "SubCategoryName" => $Where['SubCategoryName']);				
			}

			$arr['ProductWithCategorySubCategory'] = $arr4;			
		}


		if($Query5->num_rows()>0)
		{
			foreach($Query5->result_array() as $Where)
			{
				$arr5[$Where['InstituteID']] = ucwords($Where['FirstName']." ".$Where['LastName']);		
			}

			$arr5 = array_unique($arr5);
			$arr['InstituteNames'] = $arr5;			
		}

		$Return['Data']['Records'] = $arr;	

		return $Return;	
	}


	/*------------------------------------------------------------------------
	Manage Product
	------------------------------------------------------------------------*/
	function getProducts($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(p.CreatedDate) >= '$FilterStartDate' AND DATE(p.CreatedDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (p.ProductName LIKE '%$Keyword%' OR 
			p.ProductPrice LIKE '$Keyword%' OR 
			p.ProductQty LIKE '$Keyword%' OR 
			p.IsDownlodable LIKE '$Keyword%' OR 
			p.MaxQtyCanPurchase LIKE '$Keyword%'
			) ";
		}
		

		$sql = "SELECT p.*, c.CategoryName
		FROM store_products p				
		INNER JOIN store_products_category c ON c.CategoryID = p.CategoryID
		WHERE p.StatusID = 2 AND p.InstituteID = '$InstituteID' $append		
		ORDER BY p.ProductID 
		LIMIT $PageNo, $PageSize ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{				
				$Where['ProductImageName'] = $Where['ProductImageURL'] = "";
				$Where['ProductContentName'] = $Where['ProductContentURL'] = "";

				if($Where['ProductImage'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductImage']));		
					if($MediaData) 
					{ 
						$Where['ProductImageName'] = $MediaData['MediaName'];
						$Where['ProductImageURL'] = $MediaData['MediaURL'];
					}
				}

				if($Where['ProductContent'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductContent']));		
					if($MediaData) 
					{ 
						$Where['ProductContentName'] = $MediaData['MediaName'];
						$Where['ProductContentURL'] = $MediaData['MediaURL'];
					}
				}

				$Where['ProductQtyAvail'] = $Where['ProductQty'];
				$Where['SoldQuantity'] = $this->getSoldQuantityOfProduct($Where['ProductID']);
				if(isset($Where['SoldQuantity']) && !empty($Where['SoldQuantity']) && $Where['IsDownlodable'] == 0)
				{
					$Where['ProductQtyAvail'] = $Where['ProductQty'] - $Where['SoldQuantity'];	
				}


				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getProductsBySubCategory($EntityID, $Where=array())
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = array();		

		$cat = $Where['ProductsSubCategory'];

		$sql = "SELECT p.ProductID, p.ProductName
		FROM store_products p				
		WHERE p.StatusID = 2 AND p.InstituteID = '$InstituteID' AND p.SubCategoryID IN($cat)
		ORDER BY p.ProductName";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{
				$chk = array();
				$chk = $this->getDiscountOfProduct($Where['ProductID']);

				if(!isset($chk) || empty($chk))
				{
					$arr[] = $Where;				
				}	
			}
		
			return $arr;
		}

		return FALSE;	
	}


	function add($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);		
		
		$this->db->select('SubCategoryID');
		$this->db->from('store_products_subcategory');
		$this->db->where('SubCategoryName = "'.strtolower($Input['ProductSubCategory']).'"');
		$this->db->where('CategoryID = "'.$Input['ProductCategory'].'"');
		$this->db->limit(1);

	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {	    
			$arr = $query->result_array();
			$SubCategoryID = $arr[0]['SubCategoryID'];			
		}
		else
		{
			$InsertData = (array(
			"InstituteID"=>$InstituteID,
			"CategoryID"=>$Input['ProductCategory'],
			"SubCategoryName"=>$Input['ProductSubCategory'],
			"CreatedDate"=>date("Y-m-d H:i:s")
			));
		
			$this->db->insert('store_products_subcategory', $InsertData);

			$SubCategoryID = $this->db->insert_id();
		}		


		$this->db->select('ProductID');
		$this->db->from('store_products');
		$this->db->where('ProductName = "'.strtolower($Input['ProductName']).'"');
		$this->db->where('CategoryID = "'.$Input['ProductCategory'].'"');
		$this->db->where('SubCategoryID = "'.$SubCategoryID.'"');        
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {			
			return "Exist"; die;
		}
		

		$InsertData = (array(
			"InstituteID"=>$InstituteID,			
			"ProductName"=>$Input['ProductName'],			
			"ProductDesc"=>$Input['ProductDesc'],
			"ProductPrice"=>$Input['ProductPrice'],
			"ProductQty"=>$Input['ProductQty'],
			"IsDownlodable"=>$Input['IsDownlodable'],
			"MaxQtyCanPurchase"=>$Input['MaxQtyCanPurchase'],
			"ShippingCost"=>$Input['ShippingCost'],
			"ProductImage"=>$Input['MediaGUIDs'],
			"ProductContent"=>$Input['MediaGUIDss'],
			"Tax"=>$Input['Tax'],
			"CategoryID"=>$Input['ProductCategory'],
			"SubCategoryID"=>$SubCategoryID,
			"SubCategoryName"=>$Input['ProductSubCategory'],
			"ProductFeature"=>$Input['ProductFeatures'],
			"NoOfPages"=>$Input['NoOfPages'],
			"PublisherName"=>$Input['PublisherName'],
			"AuthorName"=>$Input['AuthorName'],
			"Language"=>$Input['Language'],			
			"ProductPriceMRP"=>$Input['ProductPriceMRP'],
			"CreatedDate"=>date("Y-m-d H:i:s"),
			"UpdatedDate"=>date("Y-m-d H:i:s")
		));
				
		
		$this->db->insert('store_products', $InsertData);		

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return TRUE;
	}



	function edit($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('ProductID');
		$this->db->from('store_products');
		$this->db->where('ProductID = '.$Input['ProductID']);     
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			return "NotExist"; die;
		}	


		$this->db->select('ProductID');
		$this->db->from('store_products');
		$this->db->where('ProductName = "'.strtolower($Input['ProductName']).'"');        
        $this->db->where('CategoryID = "'.$Input['ProductCategory'].'"');
		$this->db->where('SubCategoryID = "'.$Input['ProductSubCategory'].'"');   
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('ProductID != '.$Input['ProductID']); 
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {	    
			return "Exist"; die;
		}


		$this->db->select('SubCategoryID');
		$this->db->from('store_products_subcategory');
		$this->db->where('SubCategoryName = "'.strtolower($Input['ProductSubCategory']).'"');
		$this->db->where('CategoryID = "'.$Input['ProductCategory'].'"');
		$this->db->limit(1);

	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {	    
			$arr = $query->result_array();
			$SubCategoryID = $arr[0]['SubCategoryID'];			
		}
		else
		{
			$InsertData = (array(
			"InstituteID"=>$InstituteID,
			"CategoryID"=>$Input['ProductCategory'],
			"SubCategoryName"=>$Input['ProductSubCategory'],
			"CreatedDate"=>date("Y-m-d H:i:s")
			));
		
			$this->db->insert('store_products_subcategory', $InsertData);

			$SubCategoryID = $this->db->insert_id();
		}	
		
		
		$UpdateData = (array(						
			"ProductName"=>$Input['ProductName'],			
			"ProductDesc"=>$Input['ProductDesc'],
			"ProductPrice"=>$Input['ProductPrice'],			
			"IsDownlodable"=>$Input['IsDownlodable'],
			"MaxQtyCanPurchase"=>$Input['MaxQtyCanPurchase'],
			"ShippingCost"=>$Input['ShippingCost'],
			"ProductImage"=>$Input['MediaGUIDs'],
			"ProductContent"=>$Input['MediaGUIDss'],			
			"ProductFeature"=>$Input['ProductFeatures'],
			"NoOfPages"=>$Input['NoOfPages'],
			"PublisherName"=>$Input['PublisherName'],
			"AuthorName"=>$Input['AuthorName'],
			"Language"=>$Input['Language'],
			"Tax"=>$Input['Tax'],
			"ProductPriceMRP"=>$Input['ProductPriceMRP'],
			"CategoryID"=>$Input['ProductCategory'],
			"SubCategoryID"=>$SubCategoryID,
			"SubCategoryName"=>$Input['ProductSubCategory'],
			"UpdatedDate"=>date("Y-m-d H:i:s")
		));	

		if($Input['ProductCategory'] != 2 && $Input['ProductCategory'] != 10 && $Input['ProductCategory'] != 12)
		{
			if(isset($Input['IncreaseProductQty']) && !empty($Input['IncreaseProductQty']) && $Input['IncreaseProductQty'] > 0)	
			{
				$UpdateData["ProductQty"] = $Input['ProductQty1'] + $Input['IncreaseProductQty'];
			}	
		}
			
		$this->db->where('ProductID', $Input['ProductID']);
		$this->db->where('InstituteID', $InstituteID);
		$this->db->update('store_products', $UpdateData);		

		$this->db->trans_complete();
 
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function delete($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('ProductID');
		$this->db->from('store_products');
		$this->db->where('ProductID = '.$Input['ProductID']);     
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			return "NotExist"; die;
		}


		$this->db->where('ProductID', $Input['ProductID']);
		$this->db->where('InstituteID', $InstituteID);
		$this->db->update('store_products', array("StatusID" => 3));
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		
		return TRUE;
	}


	/*------------------------------------------------------------------------
	Manage Coupons
	------------------------------------------------------------------------*/
	function getCoupons($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(p.ExpiryDate) >= '$FilterStartDate' AND DATE(p.ExpiryDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (p.CouponsCode LIKE '$Keyword%' OR			
			p.Discount LIKE '$Keyword%'			
			) ";
		}
		

		$sql = "SELECT p.*
		FROM store_coupons p				
		WHERE p.StatusID = 2 AND p.InstituteID = '$InstituteID' $append		
		ORDER BY p.CouponsID 
		LIMIT $PageNo, $PageSize ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{				
				$Where['CouponOnProductsStr'] = "";
				$Where['CouponOnProducts'] = $this->getApplicableProductOnCoupon($Where['ProductsInclude']);
				if(isset($Where['CouponOnProducts']) && !empty($Where['CouponOnProducts']))
				{
					$Where['CouponOnProductsStr'] = implode(",", $Where['CouponOnProducts']);
				}

				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function coupons_add($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('CouponsID');
		$this->db->from('store_coupons');
		$this->db->where('CouponsCode = "'.strtolower($Input['CouponsCode']).'"');        
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {	    
			return "Exist"; die;
		}

		foreach($Input['ProductsInclude'] as $p)
		{
			$sql = "SELECT CouponsID
			FROM store_coupons				
			WHERE date(ExpiryDate) >= CURDATE() AND FIND_IN_SET($p,ProductsInclude)		
			LIMIT 1 ";
			$Query = $this->db->query($sql);
			if ( $Query->num_rows() > 0 )
		    {	    
				return "CouponOnProductExist"; die;
			}
		}

		if(isset($Input['IsPublished']) && !empty($Input['IsPublished']) && $Input['IsPublished'] == 1)
			$Input['IsPublished'] = 1;
		else
			$Input['IsPublished'] = 0;

		$InsertData = (array(
			"InstituteID"=>$InstituteID,
			"CouponsCode"=>$Input['CouponsCode'],					
			"ExpiryDate"=>date("Y-m-d", strtotime($Input['ExpiryDate'])),			
			"Discount"=>$Input['Discount'],
			"ProductsCategory"=>implode(",", $Input['ProductsCategory']),			
			"ProductsInclude"=>implode(",", $Input['ProductsInclude']),
			"IsPublished"=>$Input['IsPublished'],
			"CreatedDate"=>date("Y-m-d H:i:s"),
			"UpdatedDate"=>date("Y-m-d H:i:s")
		));
		
		$ID = $this->db->insert('store_coupons', $InsertData);
		

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}



	function coupons_edit($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('CouponsID');
		$this->db->from('store_coupons');
		$this->db->where('CouponsID = '.$Input['CouponsID']);     
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			return "NotExist"; die;
		}	

		$CouponsID = $Input['CouponsID'];
		foreach($Input['ProductsInclude'] as $p)
		{
			$sql = "SELECT CouponsID
			FROM store_coupons				
			WHERE date(ExpiryDate) >= CURDATE() AND CouponsID != '$CouponsID' AND FIND_IN_SET('$p',ProductsInclude)		
			LIMIT 1 ";
			$Query = $this->db->query($sql);
			if ( $Query->num_rows() > 0 )
		    {	    
				return "CouponOnProductExist"; die;
			}
		}	
		
		if(isset($Input['IsPublished']) && !empty($Input['IsPublished']) && $Input['IsPublished'] == 1)
			$Input['IsPublished'] = 1;
		else
			$Input['IsPublished'] = 0;

		$UpdateData = (array(					
			"CouponsCode"=>$Input['CouponsCode'],
			"ExpiryDate"=>date("Y-m-d", strtotime($Input['ExpiryDate'])),			
			"Discount"=>$Input['Discount'],
			"ProductsCategory"=>implode(",", $Input['ProductsCategory']),			
			"ProductsInclude"=>implode(",", $Input['ProductsInclude']),
			"IsPublished"=>$Input['IsPublished'],			
			"UpdatedDate"=>date("Y-m-d H:i:s")
		));

		$this->db->where('CouponsID', $Input['CouponsID']);
		$this->db->where('InstituteID', $InstituteID);
		$this->db->update('store_coupons', $UpdateData);		

		$this->db->trans_complete();
 
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}


	function coupons_delete($EntityID, $Input)
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($EntityID);
		
		$this->db->select('CouponsID');
		$this->db->from('store_coupons');
		$this->db->where('CouponsID = '.$Input['CouponsID']);     
        $this->db->where('InstituteID = '.$InstituteID);
        $this->db->where('StatusID = 2');
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			return "NotExist"; die;
		}


		$this->db->where('CouponsID', $Input['CouponsID']);
		$this->db->where('InstituteID', $InstituteID);
		$this->db->update('store_coupons', array("StatusID" => 3));
		
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		
		return TRUE;
	}


	/*------------------------------------------------------------------------
	Manage Products On MP
	------------------------------------------------------------------------*/
	function getProductsMP($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;
	

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (p.ProductName LIKE '%$Keyword%' OR 
			p.ProductDesc LIKE '%$Keyword%' OR 
			p.ProductPrice LIKE '$Keyword%' OR 
			p.ProductQty LIKE '$Keyword%' OR 
			p.IsDownlodable LIKE '$Keyword%' OR
			u.FirstName LIKE '$Keyword%' OR
			u.LastName LIKE '$Keyword%' OR 
			p.MaxQtyCanPurchase LIKE '$Keyword%'
			) ";
		}

		if(isset($Where['Sid']) && !empty($Where['Sid']) && $Where['Sid'] > 0)
		{
			$Sid = $Where['Sid'];
			$append .= " AND p.InstituteID = '".$Sid."' ";
		}


		if(isset($Where['ProductCategoryID']) && !empty($Where['ProductCategoryID']) && $Where['ProductCategoryID'] != "")
		{
			$CategoryID = $Where['ProductCategoryID'];
			$append .= " AND p.CategoryID = '".$CategoryID."' ";
		}


		if(isset($Where['SubCategoryName']) && !empty($Where['SubCategoryName']) && count($Where['SubCategoryName']) > 0)
		{
			$Sid = implode(",", $Where['SubCategoryName']);
			$append .= " AND p.SubCategoryID IN (".$Sid.") ";
		}


		if(isset($Where['InstituteName']) && !empty($Where['InstituteName']) && count($Where['InstituteName']) > 0)
		{
			$Sid = implode(",", $Where['InstituteName']);
			$append .= " AND p.InstituteID IN (".$Sid.") ";
		}	
		

		$sql = "SELECT p.ProductID, p.ProductImage, p.ProductName, p.ProductDesc, p.ProductPrice, p.ProductPriceMRP, p.ProductQty, p.IsDownlodable, p.MaxQtyCanPurchase, u.FirstName, u.LastName, c.CategoryName, p.SubCategoryID, p.SubCategoryName, p.InstituteID
		FROM store_products p
		INNER JOIN store_products_category c ON c.CategoryID = p.CategoryID
		INNER JOIN tbl_entity e ON e.EntityID = p.InstituteID AND e.StatusID = 2
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		WHERE p.StatusID = 2 $append		
		ORDER BY p.ProductName 
		LIMIT $PageNo, $PageSize ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();
			
			foreach($Query->result_array() as $Where)
			{				
				$Where['ProductImageName'] = $Where['ProductImageURL'] = "";				

				if($Where['ProductImage'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductImage']));		
					if($MediaData) 
					{ 
						$Where['ProductImageName'] = $MediaData['MediaName'];
						$Where['ProductImageURL'] = $MediaData['MediaURL'];
					}
				}

				$Where['Discount'] = $Where['OfferPrice'] = 0;
				$Where['CouponsDetails'] = $this->getDiscountOfProduct($Where['ProductID']);
				if(isset($Where['CouponsDetails']) && !empty($Where['CouponsDetails']))
				{					
					if(isset($Where['CouponsDetails']['Discount']) && !empty($Where['CouponsDetails']['Discount']) && $Where['CouponsDetails']['Discount'] > 0)
					{
						$Where['Discount'] = $Where['CouponsDetails']['Discount'];
						$Where['OfferPrice'] = $Where['ProductPriceMRP'] - round(($Where['ProductPriceMRP'] * $Where['Discount']) / 100);

					}
				}
				unset($Where['CouponsDetails']);
				
				$arr[] = $Where;				
			}			
			
			$Return['Data']['Records'] = $arr;
			
		
			return $Return;
		}	

		return FALSE;	
	}


	function getProductsDetails($EntityID, $Inputs = array())
	{
		$pid = $Inputs['pid'];

		$arr = array();
	
		$sql = "SELECT p.*, u.FirstName, u.LastName
		FROM store_products p
		INNER JOIN tbl_entity e ON e.EntityID = p.InstituteID AND e.StatusID = 2
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		WHERE p.StatusID = 2 AND p.ProductID IN($pid)		
		ORDER BY p.ProductName
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$total_shipping = $total_discount = 0;

			foreach($Query->result_array() as $Where)
			{				
				//$Where['CouponsCode'] = $this->getDiscountCouponOfProduct($Where['ProductID']);
				$Where['SoldQuantity'] = $this->getSoldQuantityOfProduct($Where['ProductID']);
				if(isset($Where['SoldQuantity']) && !empty($Where['SoldQuantity']))
				{
					$Where['ProductQty'] = $Where['ProductQty'] - $Where['SoldQuantity'];
					if($Where['ProductQty'] < 0) $Where['ProductQty'] = 0;
				}

				$Where['ProductImageName'] = $Where['ProductImageURL'] = "";
				if($Where['ProductImage'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductImage']));		
					if($MediaData) 
					{ 
						$Where['ProductImageName'] = $MediaData['MediaName'];
						$Where['ProductImageURL'] = $MediaData['MediaURL'];
					}
				}


				if(!isset($Where['Tax']) || empty($Where['Tax'])) $Where['Tax'] = 0;


				$Where['DiscountAmount'] = $Where['Discount'] = 0;
				$Where['OfferPrice'] = $Where['ProductPriceMRP'];
				$Where['CouponsDetails'] = $this->getDiscountOfProduct($Where['ProductID']);
				if(isset($Where['CouponsDetails']) && !empty($Where['CouponsDetails']))
				{					
					if(isset($Where['CouponsDetails']['Discount']) && !empty($Where['CouponsDetails']['Discount']) && $Where['CouponsDetails']['Discount'] > 0)
					{
						$Where['Discount'] = $Where['CouponsDetails']['Discount'];
						$Where['DiscountAmount'] = (($Where['ProductPrice'] * $Where['Discount']) / 100);
						$Where['OfferPrice'] = $Where['ProductPrice'] - $Where['DiscountAmount'];

						if(isset($Where['Tax']) && !empty($Where['Tax']))
						{
							$Where['OfferPrice'] = ($Where['OfferPrice']) + (($Where['OfferPrice'] * $Where['Tax']) / 100);
						}	
					}
				}

				if(isset($Where['ShippingCost']) && !empty($Where['ShippingCost']))
				{
					$Where['NetAmount'] = $Where['OfferPrice'] + $Where['ShippingCost'];
				}
				else
				{
					$Where['NetAmount'] = $Where['OfferPrice'];
				}	

				if(isset($Where['DiscountAmount']) && !empty($Where['DiscountAmount']))
				$total_discount = $total_discount + $Where['DiscountAmount'];

				if(isset($Where['ShippingCost']) && !empty($Where['ShippingCost']))
				$total_shipping = $total_shipping + $Where['ShippingCost'];

				$arr[] = $Where;				
			}

			
			$DeliveryAddress = array();
			if(isset($EntityID) && !empty($EntityID) && $EntityID > 0)
			{
				$DeliveryAddress = $this->getDeliveryAddress($EntityID);
			}

			$VisitorProducts = array();
			if(isset($Inputs['vid']) && !empty($Inputs['vid']))
			{
				$VisitorProducts = $this->getVisitorProducts($Inputs['vid']);
			}	

			$RelatedProducts = $this->getRelatedProducts($pid);

			$Return['Records'] = $arr;
			$Return['RelatedProducts'] = $RelatedProducts;
			$Return['DeliveryAddress'] = $DeliveryAddress;
			$Return['VisitorProducts'] = $VisitorProducts;

			$Return['TotalShipping'] = $total_shipping;
			$Return['TotalDiscount'] = $total_discount;

			return $Return;
		}	

		return FALSE;	
	}


	function getVisitorProducts($VisitorID)
	{
		$arr = array();
	
		$sql = "SELECT ProductID, ProductQty
		FROM store_order_temp		
		WHERE VisitorID = $VisitorID
		ORDER BY TempID";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;
			}
		}	

		return $arr;	
	}


	function getDeliveryAddress($EntityID)
	{
		$arr = array();
		
		$sql = "SELECT OrderID, ShippingAddress, ShippingCity, ShippingState, ShippingZipCode
		FROM store_orders		
		WHERE UserID = $EntityID AND ShippingAddress != '' AND ShippingCity != '' AND ShippingState != '' AND ShippingZipCode != ''
		GROUP BY ShippingAddress, ShippingCity, ShippingState, ShippingZipCode 
		ORDER BY OrderID, ShippingCity, ShippingState, ShippingZipCode, ShippingAddress";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}
		}	

		return $arr;	
	}

	function getRelatedProducts($ProductIDs)
	{		
		$append = "";

		$catarr = $this->getCategoryOfProduct($ProductIDs);		
		if(isset($catarr) && !empty($catarr))
		{
			$t = array();
			foreach($catarr as $obj) 
			{
				$t[] = $obj['SubCategoryID'];
			}

			$append = implode(",", $t);

			$append = " AND p.SubCategoryID IN ($append) ";
		}


		$arr = array();

		$sql = "SELECT p.ProductID, p.ProductImage, p.ProductName, p.ProductDesc, p.ProductPrice, p.ProductPriceMRP, p.ProductQty, p.IsDownlodable, p.MaxQtyCanPurchase, u.FirstName, u.LastName, c.CategoryName, p.SubCategoryName
		FROM store_products p
		INNER JOIN store_products_category c ON c.CategoryID = p.CategoryID
		INNER JOIN tbl_entity e ON e.EntityID = p.InstituteID AND e.StatusID = 2
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		WHERE p.StatusID = 2 AND p.ProductID NOT IN ($ProductIDs) $append		
		ORDER BY p.ProductName 
		LIMIT 100";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$Where['ProductImageName'] = $Where['ProductImageURL'] = "";				

				if($Where['ProductImage'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductImage']));		
					if($MediaData) 
					{ 
						$Where['ProductImageName'] = $MediaData['MediaName'];
						$Where['ProductImageURL'] = $MediaData['MediaURL'];
					}
				}

				$Where['Discount'] = $Where['OfferPrice'] = 0;
				$Where['CouponsDetails'] = $this->getDiscountOfProduct($Where['ProductID']);
				if(isset($Where['CouponsDetails']) && !empty($Where['CouponsDetails']))
				{					
					if(isset($Where['CouponsDetails']['Discount']) && !empty($Where['CouponsDetails']['Discount']) && $Where['CouponsDetails']['Discount'] > 0)
					{
						$Where['Discount'] = $Where['CouponsDetails']['Discount'];
						$Where['OfferPrice'] = $Where['ProductPriceMRP'] - round(($Where['ProductPriceMRP'] * $Where['Discount']) / 100);

					}
				}
				unset($Where['CouponsDetails']);

				$arr[] = $Where;				
			}

			$Return = $arr;
		
			return $Return;
		}	

		return FALSE;	
		
	}


	function getCategoryOfProduct($ProductIDs)
	{
		$arr = array();
	
		$sql = "SELECT CategoryID, SubCategoryID
		FROM store_products		
		WHERE StatusID = 2 AND ProductID IN ($ProductIDs) ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}
		}	

		return $arr;	
	}

	function getDiscountOfProduct($ProductID)
	{
		$arr = array();
	
		$sql = "SELECT CouponsCode, Discount
		FROM store_coupons				
		WHERE StatusID = 2 AND IsPublished = 1 AND date(ExpiryDate) >= CURDATE() AND FIND_IN_SET('$ProductID',ProductsInclude)	
		LIMIT 1 ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$arr = $Query->result_array();

			$arr = $arr[0];
		}	

		return $arr;	
	}

	function getDiscountCouponOfProduct($ProductID)
	{
		$CouponsCode = "";
	
		$sql = "SELECT CouponsCode
		FROM store_coupons				
		WHERE StatusID = 2 AND IsPublished = 1 AND date(ExpiryDate) >= CURDATE() AND FIND_IN_SET('$ProductID',ProductsInclude)	
		LIMIT 1 ";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$arr = $Query->result_array();

			$CouponsCode = $arr[0]['CouponsCode'];
		}	

		return $CouponsCode;	
	}

	function getSoldQuantityOfProduct($ProductID)
	{
		$TotalSold = 0;
	
		$sql = "SELECT SUM(op.OrderedQty) as TotalSold
		FROM store_orders o	
		INNER JOIN store_ordered_products op ON o.OrderID = op.OrderID AND op.ProductID = '$ProductID'	
		WHERE o.OrderStatus = 'success' AND op.ProductID = '$ProductID'		
		GROUP BY op.ProductID
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$arr = $Query->result_array();

			$TotalSold = $arr[0]['TotalSold'];
		}	

		return $TotalSold;	
	}


	function checkDiscountCoupon($EntityID, $Where = array())
	{
		$Amount = $Where['Amount'];
		$ProductID = $Where['ProductID'];
		$CouponsCode = $Where['CouponsCode'];

		$arr = array();
	
		$sql = "SELECT Discount, ExpiryDate
		FROM store_coupons		
		WHERE StatusID = 2 AND IsPublished = 1 AND CouponsCode = '$CouponsCode' AND FIND_IN_SET('$ProductID',ProductsInclude)
		LIMIT 1
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr = $Where;				
			}
		
			return $arr;
		}	

		return FALSE;	
	}



	function confirmOrder($EntityID, $Input)
	{		
		$ProductIDArr = $Input['ProductID'];
		$ProductQtyArr = $Input['ProductQty'];	

		foreach($ProductIDArr as $p)
		{
			if(is_numeric($p) && is_numeric($ProductQtyArr[$p]))
			{
				$data = $this->getProductsDetailsById($p);
				
				$SoldQuantity = 0;
				$SoldQuantity = $this->getSoldQuantityOfProduct($p);

				if(isset($data) && !empty($data))
				{						
					$ProductName = $MaxQtyCanPurchase = "";

					$OrderedQty = $ProductQtyArr[$p];					
					$MaxQtyCanPurchase = $data['MaxQtyCanPurchase'];
					$ProductName = $data['ProductName'];
					$ProductQtyDB = $data['ProductQty'];

					$ProductQtyDB = $ProductQtyDB - $SoldQuantity;
					if($ProductQtyDB < $OrderedQty && $data['IsDownlodable'] == 0)
					{
						return array("msg"=> "OrderExceed", "data" => "Order quantity of product is more than available quantity. Please reduce order quantity.<br/>Product Name = $ProductName<br/>Order Quantity = $OrderedQty<br/>Available Quantity = $ProductQtyDB"); 
						die;
					}
										

					$chkdata = $this->getUserProductQuantityPurchased($p);
					
					if(isset($chkdata) && !empty($chkdata))
					{
						$TotalPur = $chkdata['TotalPur'];

						if(($TotalPur + $OrderedQty) > $MaxQtyCanPurchase)
						{
							return array("msg"=> "Exceed", "data" => "Maximum ".$MaxQtyCanPurchase." quantity are allowed to purchase for each user for ".$ProductName." product. Please reduce order quantity or remove it."); 
							die;
						}
					}					
				}
			}		
		}
		
		$InstituteNameArr = $Input['InstituteNameHdn'];

		$HTML = "";

		$CustomerName = $Input['CustomerName'];
		$CustomerMobile = $Input['CustomerMobile'];
		$CustomerEmail = $Input['CustomerEmail'];
		
		$BillingAddress = $Input['BillingAddress'];
		$BillingCity = $Input['BillingCity'];
		$BillingState = $Input['BillingState'];
		$BillingZipCode = $Input['BillingZipCode'];
		$BillingAddress = $BillingAddress.", ".$BillingCity.", ".$BillingState.", ".$BillingZipCode;

		$ShippingAddress = $Input['ShippingAddress'];
		$ShippingCity = $Input['ShippingCity'];
		$ShippingState = $Input['ShippingState'];
		$ShippingZipCode = $Input['ShippingZipCode'];
		$ShippingAddress = $ShippingAddress."<br/>".$ShippingCity.", ".$ShippingState.", ".$ShippingZipCode;
				

		$trRows = "";
		if(true)
		{
			$subTotal = $netTotal = $counter = $ShippingCostTotal = 0;			

			foreach($ProductIDArr as $p)
			{				
				if(is_numeric($p) && is_numeric($ProductQtyArr[$p]))
				{
					$data = $this->getProductsDetailsById($p);
					
					if(isset($data) && !empty($data))
					{
						$counter++;
						$ProductPrice = $Amount = $DiscountAmount = $ShippingCost = $NetAmount = $ProductTax = $OrderedQty = "";
						
						$OrderedQty = $ProductQtyArr[$p];
						$InstituteName = $InstituteNameArr[$p];						

						$ProductUnitPrice = $data['ProductPrice'];
						$ProductPrice = $data['ProductPriceMRP'];					
						$ShippingCost = $data['ShippingCost'];
						$ProductTax = $data['Tax'];	
						$ProductName = $data['ProductName'];

						$CouponsDetails = $this->getDiscountOfProduct($p);
						if(isset($CouponsDetails) && !empty($CouponsDetails))
						{					
							if(isset($CouponsDetails['Discount']) && !empty($CouponsDetails['Discount']) && $CouponsDetails['Discount'] > 0)
							{
								$DiscountAmount = $CouponsDetails['Discount'];
							}
						}

						if(!isset($ShippingCost) || empty($ShippingCost)) $ShippingCost = 0;
						if(!isset($DiscountAmount) || empty($DiscountAmount)) $DiscountAmount = 0;
						if(!isset($ProductTax) || empty($ProductTax)) $ProductTax = 0;

						
						$Amount = $OrderedQty * $ProductUnitPrice;
						$DiscountAmount = (($Amount * $DiscountAmount) / 100);
						
						$AmountAfterDiscount = $Amount - $DiscountAmount;

						$TaxAmount = (($AmountAfterDiscount * $ProductTax) / 100);

						$TaxableAmount = $AmountAfterDiscount + $TaxAmount;

						$ShippingCostTotal = $ShippingCostTotal + $ShippingCost;
						
						$TaxableAmount = round($TaxableAmount);
						
						$trRows .= '<tr>
							<td>'.$ProductName.'</td>
							<td>'.$InstituteName.'</td>
							<td>'.$OrderedQty.'</td>
							<td>'.$ProductUnitPrice.'</td>
							<td>'.$DiscountAmount.'</td>
							<td>'.$ProductTax.'</td>
							<td>'.$TaxableAmount.'</td>							
						</tr>
						';


						$subTotal = $subTotal + $TaxableAmount;
					}
				}		
					
			}
			
			$Total = $subTotal + $ShippingCostTotal;

			$subTotal = round($subTotal);
			$Total = round($Total);

			$trRows .= '<tr>				
				<td colspan="6" align="right">SubTotal</td>
				<td>'.$subTotal.'</td>							
			</tr>';


			$trRows .= '<tr>				
				<td colspan="6" align="right">Total Shipping</td>
				<td>'.$ShippingCostTotal.'</td>							
			</tr>';


			$trRows .= '<tr>				
				<td colspan="6" align="right">Total</td>
				<td>'.$Total.'</td>							
			</tr>';
		}
	
		$HTML = '<table width="100%" border="0" style="border-collapse: collapse; border:0px none !important;">

		<td align="left"><br/><b>BILLING DETAILS</b><br/>
		'.$CustomerName.'<br/>
		'.$CustomerMobile.'<br/>
		'.$CustomerEmail.'<br/>
		'.$BillingAddress.'		
		</td>

		<td align="right"><b>DELIVERY ADDRESS</b><br/>
		'.$ShippingAddress.'		
		</td>		
		</table>
		<hr/>

		<table class="table">
		<thead class="thead-dark">
		<tr>		       
		    <th class="align-top">Product</th>
		    <th class="align-top">Institute Name</th>
		    <th class="align-top">Quantity</th>
		    <th class="align-top">Unit Price</th>
		    <th class="align-top">Discount Amount</th>
		    <th class="align-top">Tax %</th>
		    <th class="align-top">Amount</th>		    
		</tr>    
		</thead>
		<tbody>

		'.$trRows.'

		</tbody>
		</table>

		<div class="row">
		<div class="col-md-12">
		<div class="float-right">
			<img src="./asset/img/ajax-loader-small.gif" id="proceedCheckoutLoading" style="display:none;">
			
			<button type="button" class="btn btn-secondary btn-sm" id="proceedCheckoutLoadingCancelBtn" data-dismiss="modal">Cancel</button>

			<button type="button" name="proceedCheckout" id="proceedCheckoutLoadingBtn" class="btn btn-primary" onclick="proceed_checkout();">Confirm & Place Order</button>
		</div></div></div>			
		';

		$HTML = '<div class="modal-body"><div class="form-area"><div class="col-md-12"><div class="row"><div class="col-md-12">'.$HTML.'</div></div></div></div></div>';

		return array("msg"=> "", "data" => $HTML);		

		$this->db->trans_complete();
	}


	function proceedCheckout($EntityID, $Input)
	{		
		$ProductIDArr = $Input['ProductID'];
		$ProductQtyArr = $Input['ProductQty'];			
		

		foreach($ProductIDArr as $p)
		{
			if(is_numeric($p) && is_numeric($ProductQtyArr[$p]))
			{
				$data = $this->getProductsDetailsById($p);
				
				$SoldQuantity = 0;
				$SoldQuantity = $this->getSoldQuantityOfProduct($p);

				if(isset($data) && !empty($data))
				{						
					$ProductName = $MaxQtyCanPurchase = "";

					$OrderedQty = $ProductQtyArr[$p];					
					$MaxQtyCanPurchase = $data['MaxQtyCanPurchase'];
					$ProductName = $data['ProductName'];
					$ProductQtyDB = $data['ProductQty'];

					$ProductQtyDB = $ProductQtyDB - $SoldQuantity;
					if($ProductQtyDB < $OrderedQty && $data['IsDownlodable'] == 0)
					{
						return array("msg"=> "OrderExceed", "data" => "Order quantity of product is more than available quantity. Please reduce order quantity.<br/>Product Name = $ProductName<br/>Order Quantity = $OrderedQty<br/>Available Quantity = $ProductQtyDB"); 
						die;
					}
										

					$chkdata = $this->getUserProductQuantityPurchased($p);
					
					if(isset($chkdata) && !empty($chkdata))
					{
						$TotalPur = $chkdata['TotalPur'];

						if(($TotalPur + $OrderedQty) > $MaxQtyCanPurchase)
						{
							return array("msg"=> "Exceed", "data" => "Maximum ".$MaxQtyCanPurchase." quantity are allowed to purchase for each user for ".$ProductName." product. Please reduce order quantity or remove it."); 
							die;
						}
					}					
				}
			}		
		}
		

		$DeliveryAddress = array();
		$OrderDate = date("Y-m-d");


		$InsertData = (array(
		"UserID"=>$EntityID,
		"OrderDate"=>$OrderDate,
		"CustomerName"=>$Input['CustomerName'],
		"CustomerMobile"=>$Input['CustomerMobile'],
		"CustomerEmail"=>$Input['CustomerEmail'],
		
		"BillingAddress"=>$Input['BillingAddress'],
		"BillingCity"=>$Input['BillingCity'],
		"BillingState"=>$Input['BillingState'],
		"BillingZipCode"=>$Input['BillingZipCode'],			

		"ShippingAddress"=>$Input['ShippingAddress'],
		"ShippingCity"=>$Input['ShippingCity'],
		"ShippingState"=>$Input['ShippingState'],
		"ShippingZipCode"=>$Input['ShippingZipCode'],

		"CreatedDate"=>date("Y-m-d H:i:s"),
		"UpdatedDate"=>date("Y-m-d H:i:s")));
		

		$dd = date("d", strtotime($OrderDate));
		if($dd>=1 && $dd<=15)
		{
			$InsertData['PaymentDate'] = date("Y", strtotime($OrderDate))."-".(date("m", strtotime($OrderDate." +1 MONTH")))."-07";
		}
		else
		{
			$InsertData['PaymentDate'] = date("Y", strtotime($OrderDate))."-".(date("m", strtotime($OrderDate." +1 MONTH")))."-21";
		}


		$this->db->insert('store_orders', $InsertData);
		$OID = $this->db->insert_id();

		if($OID > 0)
		{
			$subTotal = $netTotal = $counter = 0;			

			foreach($ProductIDArr as $p)
			{
				if(is_numeric($p) && is_numeric($ProductQtyArr[$p]))
				{
					$data = $this->getProductsDetailsById($p);
					
					if(isset($data) && !empty($data))
					{
						$counter++;
						$ProductPrice = $Amount = $DiscountAmount = $ShippingCost = $NetAmount = $ProductTax = $OrderedQty = "";
						
						$OrderedQty = $ProductQtyArr[$p];						

						$ProductUnitPrice = $data['ProductPrice'];
						$ProductPrice = $data['ProductPriceMRP'];					
						$ShippingCost = $data['ShippingCost'];
						$ProductTax = $data['Tax'];	


						$CouponsDetails = $this->getDiscountOfProduct($p);
						if(isset($CouponsDetails) && !empty($CouponsDetails))
						{					
							if(isset($CouponsDetails['Discount']) && !empty($CouponsDetails['Discount']) && $CouponsDetails['Discount'] > 0)
							{
								$DiscountAmount = $CouponsDetails['Discount'];
							}
						}

						if(!isset($ShippingCost) || empty($ShippingCost)) $ShippingCost = 0;
						if(!isset($DiscountAmount) || empty($DiscountAmount)) $DiscountAmount = 0;
						if(!isset($ProductTax) || empty($ProductTax)) $ProductTax = 0;

						
						$Amount = $OrderedQty * $ProductUnitPrice;
						$DiscountAmount = (($Amount * $DiscountAmount) / 100);
						
						$AmountAfterDiscount = $Amount - $DiscountAmount;

						$TaxAmount = (($AmountAfterDiscount * $ProductTax) / 100);

						$TaxableAmount = $AmountAfterDiscount + $TaxAmount;

						$NetAmount = $TaxableAmount + $ShippingCost;

						$NetAmount = round($NetAmount);
						
						$InsertData = (array(
						"OrderID"=>$OID,						
						"ProductPrice"=>$ProductPrice,			
						"OrderedQty"=>$OrderedQty,
						"Amount"=>$Amount,
						"DiscountAmount"=>$DiscountAmount,

						"AmountAfterDiscount"=>$AmountAfterDiscount,
						"TaxAmount"=>$TaxAmount,
						"TaxableAmount"=>$TaxableAmount,


						"NetAmount"=>$NetAmount,
						"TaxPercent"=>$ProductTax,						

						"ProductUnitPrice"=>$ProductUnitPrice,
						"ProductID"=>$data['ProductID'],
						"InstituteID"=>$data['InstituteID'],			
						"ProductName"=>$data['ProductName'],			
						"ProductDesc"=>$data['ProductDesc'],						
						"IsDownlodable"=>$data['IsDownlodable'],						
						"ShippingCost"=>$data['ShippingCost'],
						"ProductImage"=>$data['ProductImage'],
						"ProductContent"=>$data['ProductContent'],						
						"CategoryID"=>$data['CategoryID'],
						"SubCategoryID"=>$data['SubCategoryID'],
						"SubCategoryName"=>$data['SubCategoryName'],
						"ProductFeature"=>$data['ProductFeature'],
						"NoOfPages"=>$data['NoOfPages'],
						"PublisherName"=>$data['PublisherName'],
						"AuthorName"=>$data['AuthorName'],
						"Language"=>$data['Language']
						));

						$this->db->insert('store_ordered_products', $InsertData);

						$subTotal = $subTotal + $NetAmount;
					}
				}		
			}
			
			$subTotal = round($subTotal);
			$netTotal = $subTotal;

			$this->db->where('OrderID', $OID);
			$this->db->where('UserID', $EntityID);
			$this->db->update('store_orders', array("SubTotal" => $subTotal, "NetTotal" => $netTotal));

			if($counter <= 0)
			{
				$this->db->where('OrderID', $OID);
				$this->db->where('UserID', $EntityID);
				$this->db->delete('store_orders');

				return array();
			}			

			return array("msg"=> "", "data" => $OID);
		}		

		$this->db->trans_complete();

		return array();
	}


	function getProductsDetailsById($ProductID)
	{
		$arr = array();
	
		$sql = "SELECT p.*
		FROM store_products p		
		WHERE p.StatusID = 2 AND p.ProductID = '$ProductID'		
		LIMIT 1
		";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			$arr = $Query->result_array();	
			
			$arr = $arr[0];		
		}	

		return $arr;	
	}

	function getUserProductQuantityPurchased($ProductID)
	{
		$userID = $this->EntityID;

		$arr = array();
	
		$sql = "SELECT SUM(op.OrderedQty) as TotalPur
		FROM store_orders o	
		INNER JOIN store_ordered_products op ON o.OrderID = op.OrderID AND op.ProductID = '$ProductID'	
		WHERE o.OrderStatus = 'success' AND o.UserID = '$userID' AND op.ProductID = '$ProductID'		
		GROUP BY o.UserID
		";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			$arr = $Query->result_array();	
			
			$arr = $arr[0];		
		}	

		return $arr;	
	}

	function getInstituteByProductId($ProductID)
	{
		$arr = array();
	
		$sql = "SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as FullName, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email
		FROM store_products p	
		INNER JOIN tbl_users u ON u.UserID = p.InstituteID	
		WHERE p.ProductID = '$ProductID'		
		LIMIT 1
		";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			$arr = $Query->result_array();	
			
			$arr = $arr[0];		
		}	

		return $arr;	
	}


	function getInstituteByOrderId($OrderID)
	{
		$arr = array();
	
		/*$sql = "SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as FullName, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, op.OrderedQty, op.ProductName, o.CustomerName
		FROM store_orders o	
		INNER JOIN store_ordered_products op ON o.OrderID = op.OrderID			
		INNER JOIN tbl_users u ON u.UserID = p.InstituteID
		WHERE o.OrderID = '$OrderID'
		ORDER BY u.FirstName
		";*/

		$sql = "SELECT * FROM 
		(SELECT CONCAT(u.FirstName, ' ', IF(u.LastName IS NOT NULL, u.LastName,'')) as FullName, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, GROUP_CONCAT(concat('Product Name: ',op.ProductName,'\n','Ordered Quantity: ',op.OrderedQty,'\n') SEPARATOR '\n') as ProductName, o.CustomerName
		FROM store_orders o	
		INNER JOIN store_ordered_products op ON o.OrderID = op.OrderID		
		INNER JOIN tbl_users u ON u.UserID = op.InstituteID
		WHERE o.OrderID = '$OrderID'
        GROUP BY u.FirstName
		) as temp         
        ORDER BY FullName";

		$Query = $this->db->query($sql);
		
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}	
		}	

		return $arr;	
	}

	function validateUserOrder($EntityID, $Where = array())
	{
		$OrderID = $Where['OrderID'];

		$arr = array();
	
		$sql = "SELECT OrderID
		FROM store_orders		
		WHERE OrderID = '$OrderID' AND UserID = '$EntityID' AND OrderStatus = 'pending'
		LIMIT 1
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows() > 0)
		{			
			return TRUE;
		}	

		return FALSE;	
	}


	function savePaymentSlip($EntityID, $Where = array())
	{
		$OrderID = $Where['OrderID'];
		$PaidAmount = $Where['PaidAmount'];
		$PaymentDate = $Where['PaymentDate'];
		$OrderStatus = $Where['OrderStatus'];
		$TransactionID = $Where['TrackingID'];	


		$UpdateData = (array(
		"OrderStatus"=>$OrderStatus,			
		"PaidAmount"=>$PaidAmount,		
		"TransactionID"=>$TransactionID,					
		"UpdatedDate"=>date("Y-m-d H:i:s")
		));

		$this->db->where('OrderID', $OrderID);
		$this->db->where('UserID', $EntityID);
		$this->db->update('store_orders', $UpdateData);


		$id = $this->db->affected_rows();

		if($id > 0)
		{
			//Send mail to institutes for product sold--------------------------			
			$data = $this->getInstituteByOrderId($OrderID);					
			if(isset($data) && !empty($data))
			{					
				foreach($data as $arr)
				{
					$ProductName = ucfirst($arr['ProductName']);
					$OrderedQty = $arr['OrderedQty'];
					$CustomerName = ucfirst($arr['CustomerName']);

					//Send Mail to Institutes--------------------------
					//$content = "New Order Of Your Product.<br/><br/>Product Name: $ProductName<br/>Ordered Quantity: $OrderedQty<br/><br/>Customer Name: ".$CustomerName.".<br/><br/>Please login to view order details.";

					$content = "New Order Of Your Product.<br/><br/>$ProductName<br/><br/>Customer Name: ".$CustomerName.".<br/><br/>Please login to view order details.";
					
					sendMail(array(
						'emailTo' => $arr['Email'],								
						'emailSubject' => "New Order Of Your Product - Iconik",						
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
					));					
				}				

				//Send Mail to Customer--------------------------
				$OrderArr['Data'] = $this->getCustomerInvoiceDetails($EntityID, array("OrderID" => $OrderID));
				$content = $this->load->view('emailer/cust_invoice',array("content" =>  $OrderArr),TRUE);
				sendMail(array(
					'emailTo' => $OrderArr['Data']['Order']['CustomerEmail'],								
					'emailSubject' => "New Order Of Products - Iconik",						
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));

			}						
			
		}	

		return $id;	
	}


	/*------------------------------------------------------------------------
	Manage Orders
	------------------------------------------------------------------------*/
	function getOrders($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(o.OrderDate) >= '$FilterStartDate' AND DATE(o.OrderDate) <= '$FilterEndDate') ";
		}


		if(isset($Where['FilterPayoutStartDate']) && !empty($Where['FilterPayoutStartDate']) && isset($Where['FilterPayoutEndDate']) && !empty($Where['FilterPayoutEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterPayoutStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterPayoutEndDate']));

			$append .= " AND (DATE(opay.PayoutDate) >= '$FilterStartDate' AND DATE(opay.PayoutDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (o.CustomerName LIKE '%$Keyword%' OR 
			o.CustomerMobile LIKE '%$Keyword%' OR 
			o.OrderStatus LIKE '$Keyword%' OR 
			op.ProductPrice LIKE '$Keyword%' OR 
			op.OrderedQty LIKE '$Keyword%' OR 
			op.Amount LIKE '$Keyword%' OR
			op.DiscountAmount LIKE '$Keyword%' OR
			op.ShippingCost LIKE '$Keyword%' OR
			op.NetAmount LIKE '$Keyword%'
			) ";
		}
		

		$sql = "SELECT op.ProductImage, o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.OrderStatus, o.OrderDate, o.BillingAddress, o.BillingCity, o.BillingState, o.BillingZipCode, o.OrderID, op.ProductID, op.OrderedProductID, o.NetTotal as NetAmount, op.AmountAfterDiscount, op.TaxAmount, op.ProductUnitPrice, DATE_FORMAT(o.PaymentDate, '%d-%m-%Y') as PaymentDate, DATE_FORMAT(opay.PayoutDate, '%d-%m-%Y') as PayoutDate, opay.PayoutDesc
		FROM store_ordered_products op		
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID AND e.EntityID = '$InstituteID'		
		INNER JOIN store_orders o ON o.OrderID = op.OrderID		
		LEFT JOIN store_orders_payouts opay ON opay.OrderID = o.OrderID AND opay.InstituteID = op.InstituteID
		WHERE op.InstituteID = '$InstituteID' $append
		GROUP BY op.OrderID		
		ORDER BY op.OrderID DESC, op.ProductName
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$temp = $this->getCommissionAmount($Where['OrderID']);
				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;															

					/*$dd = date("d", strtotime($Where['OrderDate']));
					if($dd>=1 && $dd<=15)
					{
						$Where['PaymentDate'] = "07-".(date("M", strtotime($Where['OrderDate']." +1 MONTH")))."-".date("Y", strtotime($Where['OrderDate']));
					}
					else
					{
						$Where['PaymentDate'] = "21-".(date("M", strtotime($Where['OrderDate']." +1 MONTH")))."-".date("Y", strtotime($Where['OrderDate']));
					}*/

					$arr[] = $Where;
				}					
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getCommissionAmount($OrderID, $InstituteParamID = "")
	{
		if(!isset($InstituteParamID) || empty($InstituteParamID) || $InstituteParamID == "")
			$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
		else
			$InstituteID = $InstituteParamID;

		$Return = array();		

		$sql = "SELECT SUM(op.AmountAfterDiscount) as AmountAfterDiscountSum, SUM(op.NetAmount) as NetAmountSum
		FROM store_ordered_products op				
		WHERE op.OrderID = '$OrderID' AND op.InstituteID = '$InstituteID'
		GROUP BY op.OrderID, op.InstituteID		
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{			
			$result = $Query->result_array();		

			$d = $result[0]['AmountAfterDiscountSum'];
			$n = $result[0]['NetAmountSum'];
			
			$com = ($d * 30) / 100;

			$net = round($n - $com);

			$Return['AmountAfterDiscountSum'] = $d;
			$Return['NetAmountSum'] = $n;
			$Return['Commission'] = $com;
			$Return['PayableAmount'] = $net;
		}	

		return $Return;	
	}

	function getOrdersDetailsInstitute($EntityID, $Where=array())
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$OrderID = $Where['OrderID'];

		$arr = array();		

		$sql = "SELECT op.ProductImage, op.ProductName, o.OrderID, o.CustomerName, o.OrderStatus, o.OrderDate, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, op.NetAmount, op.ProductUnitPrice, op.TaxAmount, op.AmountAfterDiscount
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID		
		WHERE op.InstituteID = '$InstituteID' AND op.OrderID = '$OrderID'			
		ORDER BY op.ProductName		
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getMyOrders($EntityID, $Where = array())
	{
		$arr = array();
	
		$sql = "SELECT *
		FROM store_orders		
		WHERE UserID = '$EntityID'
		ORDER BY OrderID DESC
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows() > 0)
		{			
			$Return['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}

			$Return['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getOrdersDetails($EntityID, $Where = array())
	{
		$OrderID = $Where['OrderID'];

		$arr = array();
	
		$sql = "SELECT op.ProductImage, op.ProductName, u.FirstName, u.LastName, o.OrderStatus, op.OrderID, op.ProductID, op.OrderedProductID, op.IsDownlodable, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, op.NetAmount, op.ProductUnitPrice, op.TaxAmount
		FROM store_ordered_products op		
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		INNER JOIN store_orders o ON o.OrderID = op.OrderID AND o.UserID = '$EntityID'		
		WHERE op.OrderID = '$OrderID' AND o.UserID = '$EntityID'		
		ORDER BY op.ProductName
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$Where['ProductImageName'] = $Where['ProductImageURL'] = "";
				if($Where['ProductImage'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductImage']));		
					if($MediaData) 
					{ 
						$Where['ProductImageName'] = $MediaData['MediaName'];
						$Where['ProductImageURL'] = $MediaData['MediaURL'];
					}
				}
				
				$arr[] = $Where;				
			}

			$Return['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function checkForDownloadProduct($EntityID, $Where = array())
	{
		$OrderedProductID = $Where['OrderedProductID'];
		$OrderID = $Where['OrderID'];
		$ProductID = $Where['ProductID'];

		$arr = array();
	
		$sql = "SELECT op.ProductContent, op.IsDownlodable 
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID AND o.UserID = '$EntityID' AND o.OrderID = '$OrderID' AND o.OrderStatus = 'success'		
		WHERE op.OrderedProductID = '$OrderedProductID' AND op.OrderID = '$OrderID' AND op.ProductID = '$ProductID' AND o.UserID = '$EntityID' AND o.OrderStatus = 'success'		
		LIMIT 1
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{
				if($Where['ProductContent'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductContent']));		
					if($MediaData) 
					{
						$Where['ProductContent'] = $MediaData['MediaURL'];
					}
				}
				
				$arr = $Where;				
			}

			$Return['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}



	function getApplicableProductOnCoupon($ProductsInclude)
	{
		$arr = array();
	
		$sql = "SELECT p.ProductName
		FROM store_products p			
		WHERE p.ProductID IN ($ProductsInclude)	
		ORDER BY p.ProductName
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where['ProductName'];				
			}
		}	

		return $arr;	
	}


	function getCartItems($EntityID, $Input)
	{		
		if(!isset($EntityID) || empty($EntityID)) $EntityID = 0;

		$VisitorID = $Input['VisitorID'];
		$pid = $Input['pid'];
		$typ = $Input['typ'];							

		if($typ == 'b' && $pid > 0)
		{
			$this->saveProductInfo($EntityID, array("VisitorID" => $VisitorID, "ProductID" => $pid, 'Qty' => 1));
		}


		$sql = "SELECT TempID, ProductID, ProductQty
		FROM store_order_temp			
		WHERE VisitorID = '$VisitorID'
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			return $Query->result_array();
		}			

		return FALSE;
	}

	function saveProductInfo($EntityID, $Input)
	{		
		if(!isset($EntityID) || empty($EntityID)) $EntityID = 0;

		$this->db->where('VisitorID', $Input['VisitorID']);
		$this->db->where('ProductID', $Input['ProductID']);
		$this->db->delete('store_order_temp');

		if($Input['Qty'] > 0)
		{
			$InsertData = (array(
			"SessionID"=>$EntityID,
			"VisitorID"=>$Input['VisitorID'],
			"ProductID"=>$Input['ProductID'],
			"ProductQty"=>$Input['Qty'],		
			"AddDate"=>date("Y-m-d H:i:s")
			));
		
			$this->db->insert('store_order_temp', $InsertData);			
			$ID = $this->db->insert_id();
			$this->db->trans_complete();

			if($ID > 0)
			{
				return TRUE;
			}

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	

		return TRUE;
	}


	function removeFromCartProduct($EntityID, $Input)
	{		
		if(!isset($EntityID) || empty($EntityID)) $EntityID = 0;

		$this->db->where('VisitorID', $Input['VisitorID']);
		$this->db->where('ProductID', $Input['ProductID']);
		$this->db->delete('store_order_temp');			

		return TRUE;
	}


	function getSoldProductDetails($EntityID, $Where=array())
	{     
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$ProductID = $Where['ProductID'];

		$arr = array();		

		$sql = "SELECT op.ProductImage, op.ProductName, o.OrderID, o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.OrderStatus, o.OrderDate, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, o.NetTotal as NetAmount, op.ProductUnitPrice
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID		
		WHERE op.InstituteID = '$InstituteID' AND op.ProductID = '$ProductID' AND o.OrderStatus = 'success'	
		ORDER BY o.CustomerName, op.ProductName		
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$temp = $this->getCommissionAmount($Where['OrderID']);
				
				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;

					$arr[] = $Where;
				}								
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getInvoiceDetails($EntityID, $Where=array())
	{
		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$OrderID = $Where['OrderID'];

		$arr = array();		

		$sql1 = "SELECT o.OrderID, DATE_FORMAT(o.OrderDate, '%d-%M-%Y') as OrderDate, o.OrderStatus, o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.BillingAddress, o.BillingCity, o.BillingState, o.BillingZipCode, o.NetTotal, o.PaidAmount, o.PaymentDate, o.TransactionID, DATE_FORMAT(opay.PayoutDate, '%d-%M-%Y') as PayoutDate, opay.PayoutDesc
		FROM store_orders o				
		INNER JOIN store_ordered_products op ON o.OrderID = op.OrderID
		LEFT JOIN store_orders_payouts opay ON opay.OrderID = o.OrderID AND opay.InstituteID = op.InstituteID
		WHERE op.InstituteID = '$InstituteID' AND o.OrderID = '$OrderID'			
		LIMIT 1	";

		$sql2 = "SELECT op.ProductImage, op.ProductName, op.ProductDesc, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, op.NetAmount, op.IsDownlodable, op.ProductUnitPrice, op.TaxAmount, op.AmountAfterDiscount, op.TaxPercent
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID		
		WHERE op.InstituteID = '$InstituteID' AND op.OrderID = '$OrderID'			
		ORDER BY op.ProductName	";

		$Query1 = $this->db->query($sql1);
		$Query2 = $this->db->query($sql2);

		if($Query1->num_rows()>0)
		{
			foreach($Query1->result_array() as $Where)
			{				
				$temp = $this->getCommissionAmount($Where['OrderID']);

				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;				

					$arr = $Where;
				}				
			}
			
			$Return['Order'] = $arr;


			$arr = array();
			foreach($Query2->result_array() as $Where)
			{
				/*$Where['ProductImageURL'] = "";
				if($Where['ProductImage'])
				{
					$MediaData = $this->Media_model->getMedia('MediaName',array('MediaGUID'=>$Where['ProductImage']));		
					if($MediaData) 
					{
						$Where['ProductImageURL'] = $MediaData['MediaURL'];
					}
				}*/

				$arr[] = $Where;				
			}			
			$Return['OrderDetails'] = $arr;
		

			return $Return;
		}	

		return FALSE;	
	}


	function getCustomerInvoiceDetails($EntityID, $Where=array())
	{		
		$OrderID = $Where['OrderID'];

		$arr = array();		

		$sql1 = "SELECT o.OrderID, DATE_FORMAT(o.OrderDate, '%d-%M-%Y') as OrderDate, o.OrderStatus, o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.BillingAddress, o.BillingCity, o.BillingState, o.BillingZipCode, o.NetTotal, o.PaidAmount, o.PaymentDate, o.TransactionID
		FROM store_orders o			
		WHERE o.UserID = '$EntityID' AND o.OrderID = '$OrderID'	AND o.OrderStatus = 'success'		
		LIMIT 1	";


		$sql2 = "SELECT op.ProductImage, op.ProductName, op.ProductDesc, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, op.NetAmount, op.IsDownlodable, u.FirstName, u.LastName
		FROM store_ordered_products op			
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID
		INNER JOIN tbl_users u ON u.UserID = e.EntityID
		WHERE op.OrderID = '$OrderID'			
		ORDER BY op.ProductName	";

		$Query1 = $this->db->query($sql1);
		$Query2 = $this->db->query($sql2);

		if($Query1->num_rows()>0)
		{
			foreach($Query1->result_array() as $Where)
			{
				$arr = $Where;				
			}
			$Return['Order'] = $arr;


			$arr = array();
			foreach($Query2->result_array() as $Where)
			{
				$arr[] = $Where;				
			}			
			$Return['OrderDetails'] = $arr;
		

			return $Return;
		}	

		return FALSE;	
	}



	/*------------------------------------------------------------------------
	Admin Manage Orders 
	------------------------------------------------------------------------*/
	function getOrdersAdmin($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(o.OrderDate) >= '$FilterStartDate' AND DATE(o.OrderDate) <= '$FilterEndDate') ";
		}


		if(isset($Where['FilterPaymentStartDate']) && !empty($Where['FilterPaymentStartDate']) && isset($Where['FilterPaymentEndDate']) && !empty($Where['FilterPaymentEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterPaymentStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterPaymentEndDate']));

			$append .= " AND (DATE(o.PaymentDate) >= '$FilterStartDate' AND DATE(o.PaymentDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (o.CustomerName LIKE '%$Keyword%' OR 
			o.CustomerMobile LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			u.LastName LIKE '%$Keyword%' OR 
			o.OrderStatus LIKE '$Keyword%' OR 
			op.ProductPrice LIKE '$Keyword%' OR 
			op.OrderedQty LIKE '$Keyword%' OR 
			op.Amount LIKE '$Keyword%' OR
			op.DiscountAmount LIKE '$Keyword%' OR
			op.ShippingCost LIKE '$Keyword%' OR
			op.NetAmount LIKE '$Keyword%'
			) ";
		}
		

		$sql = "SELECT o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.OrderStatus, o.OrderDate, o.BillingAddress, o.BillingCity, o.BillingState, o.BillingZipCode, o.OrderID, o.NetTotal, u.FirstName, u.LastName, op.InstituteID, DATE_FORMAT(o.PaymentDate, '%d-%M-%Y') as PaymentDate
		FROM store_ordered_products op			
		INNER JOIN store_orders o ON o.OrderID = op.OrderID
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID
		INNER JOIN tbl_users u ON u.UserID = e.EntityID		
		WHERE o.OrderStatus = 'success' $append	
		GROUP BY op.OrderID, op.InstituteID			
		ORDER BY op.OrderID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$temp = $this->getCommissionAmount($Where['OrderID'], $Where['InstituteID']);
				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;					

					$arr[] = $Where;
				}					
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getOrdersDetailsAdmin($EntityID, $Where=array())
	{		
		$OrderID = $Where['OrderID'];
		$InstituteID = $Where['InstituteID'];

		$arr = array();		

		$sql = "SELECT op.ProductImage, op.ProductName, o.OrderID, o.CustomerName, o.OrderStatus, o.OrderDate, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, op.NetAmount, op.ProductUnitPrice, op.TaxAmount, op.AmountAfterDiscount 
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID		
		WHERE op.OrderID = '$OrderID' AND op.InstituteID = '$InstituteID'			
		ORDER BY op.ProductName		
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$arr[] = $Where;				
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function getInvoiceDetailsAdmin($EntityID, $Where=array())
	{		
		$OrderID = $Where['OrderID'];
		$InstituteID = $Where['InstituteID'];

		$arr = array();		

		$sql1 = "SELECT o.OrderID, DATE_FORMAT(o.OrderDate, '%d-%M-%Y') as OrderDate, o.OrderStatus, o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.BillingAddress, o.BillingCity, o.BillingState, o.BillingZipCode, o.NetTotal, o.PaidAmount, o.PaymentDate, o.TransactionID, op.InstituteID, u.FirstName, u.LastName, u.Email, u.EmailForChange, u.PhoneNumber, u.PhoneNumberForChange, u.Address, u.CityName, u.StateName, u.Postal
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID
		INNER JOIN tbl_users u ON u.UserID = e.EntityID		
		WHERE op.OrderID = '$OrderID' AND op.InstituteID = '$InstituteID'
		GROUP BY op.OrderID, op.InstituteID			
		LIMIT 1	";

		$sql2 = "SELECT op.ProductImage, op.ProductName, op.ProductDesc, op.ProductPrice, op.OrderedQty, op.Amount, op.DiscountAmount, op.ShippingCost, op.NetAmount, op.IsDownlodable, op.ProductUnitPrice, op.TaxAmount, op.TaxPercent
		FROM store_ordered_products op				
		INNER JOIN store_orders o ON o.OrderID = op.OrderID		
		WHERE op.OrderID = '$OrderID' AND op.InstituteID = '$InstituteID'			
		ORDER BY op.ProductName	";

		$Query1 = $this->db->query($sql1);
		$Query2 = $this->db->query($sql2);

		if($Query1->num_rows()>0)
		{
			foreach($Query1->result_array() as $Where)
			{				
				$temp = $this->getCommissionAmount($Where['OrderID'], $Where['InstituteID']);

				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;					

					$arr = $Where;
				}				
			}
			
			$Return['Order'] = $arr;


			$arr = array();
			foreach($Query2->result_array() as $Where)
			{
				$arr[] = $Where;				
			}			
			$Return['OrderDetails'] = $arr;
		

			return $Return;
		}	

		return FALSE;	
	}



	function getPayoutsAdmin($EntityID, $Where=array(), $PageNo=1, $PageSize=10)
	{
		$arr = array(); $append = "";

		$PageNo = ($PageNo - 1) * $PageSize;

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(opay.PayoutDate) >= '$FilterStartDate' AND DATE(opay.PayoutDate) <= '$FilterEndDate') ";
		}


		if(isset($Where['FilterPaymentStartDate']) && !empty($Where['FilterPaymentStartDate']) && isset($Where['FilterPaymentEndDate']) && !empty($Where['FilterPaymentEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterPaymentStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterPaymentEndDate']));

			$append .= " AND (DATE(o.PaymentDate) >= '$FilterStartDate' AND DATE(o.PaymentDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (o.CustomerName LIKE '%$Keyword%' OR 
			o.CustomerMobile LIKE '%$Keyword%' OR
			u.FirstName LIKE '%$Keyword%' OR
			u.LastName LIKE '%$Keyword%' OR 
			o.OrderStatus LIKE '$Keyword%' OR 
			op.ProductPrice LIKE '$Keyword%' OR 
			op.OrderedQty LIKE '$Keyword%' OR 
			op.Amount LIKE '$Keyword%' OR
			op.DiscountAmount LIKE '$Keyword%' OR
			op.ShippingCost LIKE '$Keyword%' OR
			op.NetAmount LIKE '$Keyword%'
			) ";
		}
		

		$sql = "SELECT o.CustomerName, o.CustomerEmail, o.CustomerMobile, o.OrderStatus, o.OrderDate, o.BillingAddress, o.BillingCity, o.BillingState, o.BillingZipCode, o.OrderID, o.NetTotal, u.FirstName, u.LastName, op.InstituteID, DATE_FORMAT(o.PaymentDate, '%d-%M-%Y') as PaymentDate, DATE_FORMAT(opay.PayoutDate, '%d-%M-%Y') as PayoutDate, opay.PayoutDesc, opay.OrderedPayoutID, DATE_FORMAT(o.PaymentDate, '%d-%m-%Y') as PaymentDateChk
		FROM store_ordered_products op			
		INNER JOIN store_orders o ON o.OrderID = op.OrderID
		LEFT JOIN store_orders_payouts opay ON opay.OrderID = o.OrderID AND opay.InstituteID = op.InstituteID
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID
		INNER JOIN tbl_users u ON u.UserID = e.EntityID		
		WHERE o.OrderStatus = 'success' $append	
		GROUP BY op.OrderID, op.InstituteID			
		ORDER BY op.OrderID DESC
		LIMIT $PageNo, $PageSize
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$temp = $this->getCommissionAmount($Where['OrderID'], $Where['InstituteID']);
				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;					

					$arr[] = $Where;
				}					
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}


	function addPayout($EntityID, $Input)
	{				
		$this->db->select('OrderID');
		$this->db->from('store_orders');
		$this->db->where('OrderID = '.$Input['OrderID']);        
        $this->db->where('OrderStatus = "success"');
	    $query = $this->db->get();
	    if ( $query->num_rows() <= 0 )
	    {	    
			return "NotExist"; die;
		}	

		$this->db->select('OrderedPayoutID');
		$this->db->from('store_orders_payouts');
		$this->db->where('OrderID = '.$Input['OrderID']); 
		$this->db->where('InstituteID = '.$Input['InstituteID']);        
	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {	    
			return "AlreadyPaid"; die;
		}	


		$InsertData = (array(
		"OrderID"=>$Input['OrderID'],
		"InstituteID"=>$Input['InstituteID'],
		"PayoutDate"=>date("Y-m-d", strtotime($Input['PayoutDate'])),
		"PayoutDesc"=>$Input['Remarks'],
		"AmountTotal"=>$Input['AmountTotal'],
		"PayableAmount"=>$Input['PayableAmount'],
		"Commission"=>$Input['Commission'],		
		"CreatedDate"=>date("Y-m-d H:i:s")
		));
	
		$this->db->insert('store_orders_payouts', $InsertData);			
		$ID = $this->db->insert_id();
		$this->db->trans_complete();

		if($ID > 0)
		{
			return TRUE;
		}
 
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}



	function getPayoutInvoiceDetailsAdmin($EntityID, $Where=array())
	{
		$arr = array(); $append = "";		

		if(isset($Where['FilterStartDate']) && !empty($Where['FilterStartDate']) && isset($Where['FilterEndDate']) && !empty($Where['FilterEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterEndDate']));

			$append .= " AND (DATE(opay.PayoutDate) >= '$FilterStartDate' AND DATE(opay.PayoutDate) <= '$FilterEndDate') ";
		}


		if(isset($Where['FilterPaymentStartDate']) && !empty($Where['FilterPaymentStartDate']) && isset($Where['FilterPaymentEndDate']) && !empty($Where['FilterPaymentEndDate']))
		{
			$FilterStartDate = date("Y-m-d", strtotime($Where['FilterPaymentStartDate']));
			$FilterEndDate = date("Y-m-d", strtotime($Where['FilterPaymentEndDate']));

			$append .= " AND (DATE(o.PaymentDate) >= '$FilterStartDate' AND DATE(o.PaymentDate) <= '$FilterEndDate') ";
		}

		if(isset($Where['Keyword']) && !empty($Where['Keyword']))
		{
			$Keyword = trim($Where['Keyword']);

			$append .= " AND (
			u.FirstName LIKE '%$Keyword%' OR
			u.LastName LIKE '%$Keyword%' OR 
			o.OrderStatus LIKE '$Keyword%' OR						
			opay.PayoutDesc LIKE '%$Keyword%'			
			) ";
		}
		

		$sql = "SELECT o.OrderStatus, o.OrderDate, o.OrderID, o.NetTotal, u.FirstName, u.LastName, op.InstituteID, DATE_FORMAT(o.PaymentDate, '%d-%M-%Y') as PaymentDate, DATE_FORMAT(opay.PayoutDate, '%d-%M-%Y') as PayoutDate, opay.PayoutDesc, opay.OrderedPayoutID
		FROM store_ordered_products op			
		INNER JOIN store_orders o ON o.OrderID = op.OrderID
		LEFT JOIN store_orders_payouts opay ON opay.OrderID = o.OrderID AND opay.InstituteID = op.InstituteID
		INNER JOIN tbl_entity e ON e.EntityID = op.InstituteID
		INNER JOIN tbl_users u ON u.UserID = e.EntityID		
		WHERE o.OrderStatus = 'success' $append	
		GROUP BY op.OrderID, op.InstituteID			
		ORDER BY opay.OrderedPayoutID, op.OrderID		
		";

		$Query = $this->db->query($sql);
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Where)
			{
				$temp = $this->getCommissionAmount($Where['OrderID'], $Where['InstituteID']);
				if(isset($temp) && !empty($temp))
				{
					$Where['Payouts'] = $temp;					

					$arr[] = $Where;
				}					
			}

			$Return['Data']['Records'] = $arr;
		
			return $Return;
		}	

		return FALSE;	
	}
}