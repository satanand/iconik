<?php

defined('BASEPATH') OR exit('No direct script access allowed');  

class Signup extends API_Controller

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('Recovery_model');

		$this->load->model('Common_model');

	}



	/*

	Name: 			Signup

	Description: 	Use to register user to system.

	URL: 			/api/signup/

	*/

	public function index_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Email', 'Email', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : '') . '|valid_email|callback_validateEmail');

		$this->form_validation->set_rules('Username', 'Username', 'trim|alpha_dash|callback_validateUsername');

		$this->form_validation->set_rules('Password', 'Password', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] == 'Direct' ? '|required' : ''));

		$this->form_validation->set_rules('FirstName', 'FirstName', 'trim|required');

		$this->form_validation->set_rules('MiddleName', 'MiddleName', 'trim');

		$this->form_validation->set_rules('LastName', 'LastName', 'trim');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required|in_list[2,3]');

		$this->form_validation->set_rules('Gender', 'Gender', 'trim|in_list[Male,Female,Other]');

		$this->form_validation->set_rules('BirthDate', 'BirthDate', 'trim|callback_validateDate');

		$this->form_validation->set_rules('Age', 'Age', 'trim|integer');

		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|callback_validatePhoneNumber|min_length[10]|max_length[10]');

		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');



		$this->form_validation->set_rules('SourceGUID', 'SourceGUID', 'trim' . (empty($this->Post['Source']) || $this->Post['Source'] != 'Direct' ? '|required' : '') . '|callback_validateSourceGUID[' . @$this->Post['Source'] . ']');



		$this->form_validation->set_rules('DeviceType', 'Device type', 'trim|required|callback_validateDeviceType');

		$this->form_validation->set_rules('IPAddress', 'IPAddress', 'trim|callback_validateIP');

		$this->form_validation->set_rules('ReferralCode', 'ReferralCode', 'trim|callback_validateReferralCode');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserID = $this->Users_model->addUser(array_merge($this->Post, array(

			"Referral" => @$this->Referral

		)) , $this->Post['UserTypeID'], $this->SourceID, ($this->Post['Source'] != 'Direct' ? '2' : '1') /*if source is not Direct Account treated as Verified*/);

		if (!$UserID) {

			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "An error occurred, please try again later.";

		}

		else {

			if (!empty($this->Post['Email'])) {

				/* Send welcome Email to User with Token. (only if source is Direct) */

				$Token = ($this->Post['Source'] == 'Direct' ? $this->Recovery_model->generateToken($UserID, 2) : '');

				$content = $this->load->view('emailer/signup', array(

						"Name" => $this->Post['FirstName'],

						'Token' => $Token,

						'DeviceTypeID' => $this->DeviceTypeID

					) , TRUE);

				sendMail(array(

					'emailTo' => $this->Post['Email'],

					'emailSubject' => $this->Post['FirstName']. " Verify Your Email",

					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

				));

			}



			/*for update phone number*/

			if (!empty($this->Post['PhoneNumber']) && PHONE_NO_VERIFICATION) {

				/* Genrate a Token for PhoneNumber verification and save to tokens table. */

				$this->load->model('Recovery_model');

				$Token = $this->Recovery_model->generateToken($UserID, 3);

				/* Send change phonenumber SMS to User with Token. */

				sendSMS(array(

					'PhoneNumber' => $this->Post['PhoneNumber'],

					'Text' => SITE_NAME . ", OTP for Verify Mobile No. is: $Token"

				));

			}



			/*referal code generate*/

			$this->load->model('Utility_model');

			$this->Utility_model->generateReferralCode($UserID);

			/* Send welcome notification */

			$this->Notification_model->addNotification('welcome', 'Hi and welcome to ' . SITE_NAME . '!', $UserID, $UserID);

			/* get user data */

			$UserData = $this->Users_model->getUsers('FirstName,MiddleName,LastName,Email,ProfilePic,UserTypeID,UserTypeName', array(

				'UserID' => $UserID

			));

			/* create session only if source is not Direct and account treated as Verified. */

			$UserData['SessionKey'] = '';



			$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(

				"IPAddress" => @$this->Post['IPAddress'],

				"SourceID" => $this->SourceID,

				"DeviceTypeID" => $this->DeviceTypeID,

				"DeviceGUID" => @$this->Post['DeviceGUID'],

				"DeviceToken" => @$this->Post['DeviceToken']

			));



			$this->Return['Data'] = $UserData;

		}

	}



	/*

	Name: 			resendverify

	Description: 	Use to resend OTP for Email address verification.

	URL: 			/api/signup/resendverify

	*/

	public function resendverify_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim|required|valid_email');

		$this->form_validation->set_rules('DeviceType', 'Device type', 'trim|required|callback_validateDeviceType');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserData = $this->Users_model->getUsers('UserID, FirstName, StatusID', array(

			'Email' => $this->Post['Keyword']

		));



		if(empty($UserData)){

			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"If your account is registered here you will receive an email from us.";

		}

		elseif($UserData && $UserData['StatusID']==2){

			$this->Return['Message']      	=	"Your account is already verified.";

		}

		elseif($UserData && $UserData['StatusID']==3){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been deleted. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==4){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been blocked. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==6){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"You have deactivated your account, please contact the Admin to reactivate.";

		}

		else{

			/* Re-Send welcome Email to User with Token. */

			$content = $this->load->view('emailer/signup', array(

					"Name" 			=> $UserData['FirstName'],

					'Token' 		=> $this->Recovery_model->generateToken($UserData['UserID'], 2),

					'DeviceTypeID' 	=> $this->DeviceTypeID

				) , TRUE);

			sendMail(array(

				'emailTo' 		=> $this->Post['Keyword'],

				'emailSubject' 	=> "Verify your account " . SITE_NAME,

				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

			));

			$this->Return['Message'] = "Please check your email for instructions.";

		}

	}



	/*

	Name: 			resendverify

	Description: 	Use to resend OTP for mobile number verification.

	URL: 			/api/signup/resendPhoneOTP

	*/

	public function resendPhoneOTP_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('UserID', 'UserID', 'trim|required');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserData = $this->Users_model->getUsers('UserID, FirstName, StatusID, Email, PhoneNumberForChange', array(

			'UserID' => $this->Post['UserID']

		));



		if(empty($UserData)){

			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Sorry! Some error occured, Please try Later.";

		}

		elseif($UserData && $UserData['StatusID']==3){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been deleted. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==4){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been blocked. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==6){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"You have deactivated your account, please contact the Admin to reactivate.";

		}
		elseif(empty($UserData['PhoneNumberForChange'])){
			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"You have already verified your mobile number.";
		}
		else{

			/* Re-Send OTP to user to verify mobile number. */
			$Token = $this->Recovery_model->generateToken($UserData['UserID'], 3);
			$Input_arr['PhoneNumber'] =  $UserData['PhoneNumberForChange'];
			$Input_arr['Message'] =  "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes.";
			sendSMS($Input_arr);

			$this->Return['Message'] = "Please enter the OTP, just sent on your registered mobile number.";

		}

	}



	/*

	Name: 			verifyEmail

	Description: 	Use to verify Email address and activate account by OTP.

	URL: 			/api/signup/verifyEmail

	*/

	public function verifyEmail_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('OTP', 'OTP', 'trim|required|callback_validateToken[2]');

		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');

		$this->form_validation->set_rules('DeviceType', 'Device type', 'trim|required|callback_validateDeviceType');

		$this->form_validation->set_rules('DeviceGUID', 'DeviceGUID', 'trim');

		$this->form_validation->set_rules('DeviceToken', 'DeviceToken', 'trim');

		$this->form_validation->set_rules('IPAddress', 'IPAddress', 'trim|callback_validateIP');

		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');

		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserID = $this->Recovery_model->verifyToken($this->Post['OTP'], 2);

		/* check for email update */

		$UserData = $this->Users_model->getUsers('UserTypeID,FirstName,LastName,Email,EmailForChange,StatusID,ProfilePic', array(

				'UserID' => $UserID

			));

		if (!empty($UserData['EmailForChange'])) {

			if($this->Users_model->updateEmail($UserID, $UserData['EmailForChange'])){

				$content = $this->load->view('emailer/change_email_confirmed', array(

						"Name" => $UserData['FirstName']

					) , TRUE);

				sendMail(array(

					'emailTo' => $UserData['Email'],

					'emailSubject' => "Your ".SITE_NAME." email has been updated!",

					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

				));

			}

		}

		else {

			/* change entity status to activate */

			$this->Entity_model->updateEntityInfo($UserID, array(

				"StatusID" => 2

			));

			/*Create Session*/

			$UserData['SessionKey'] = $this->Users_model->createSession($UserID, array(

				"IPAddress" => @$this->Post['IPAddress'],

				"SourceID" => $this->SourceID,

				"DeviceTypeID" => $this->DeviceTypeID,

				"DeviceGUID" => @$this->Post['DeviceGUID'],

				"DeviceToken" => @$this->Post['DeviceToken'],

				"Latitude" => @$this->Post['Latitude'],

				"Longitude" => @$this->Post['Longitude']

			));

			$this->Return['Data'] = $UserData;

			$this->Return['Message'] = "Your account has been successfully verified, please login to get access your account.";

		}



		$this->Recovery_model->deleteToken($this->Post['OTP'], 2); /*delete token in any case*/

	}



	/*

	Name: 			verifyEmail

	Description: 	Use to verify Email address and activate account by OTP.

	URL: 			/api/signup/verifyEmail

	*/

	public function verifyPhoneNumber_post()
	{
		/* Validation section */
 
		$this->form_validation->set_rules('OTP', 'OTP', 'trim|required|callback_validateToken[3]');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserIDObj = $this->Recovery_model->verifyTokenPhone($this->Post['OTP'], 3);
		
		if(isset($UserIDObj) && !empty($UserIDObj))
		{
			$UserID = $UserIDObj-> UserID;

			$datetime1 = new DateTime();
			$datetime2 = new DateTime($UserIDObj-> EntryDate);
			$interval = $datetime1->diff($datetime2);
			$elapsed = $interval->format('%i');
			
			$this->Return['Data'] = $UserID;

			if($elapsed > 30)
			{
				$this->Recovery_model->expiredToken($this->Post['OTP'], 3);
				
				$this->Return['ResponseCode'] = 700;

				$this->Return['Message'] = "OTP expired.";
			}
			else
			{
				/* check for PhoneNo. update */

				$UserData = $this->Users_model->getUsers('PhoneNumberForChange', array(

					'UserID' => $UserID 

				));

				$this->Recovery_model->deleteToken($this->Post['OTP'], 3); /*delete token in any case*/

				if (!empty($UserData['PhoneNumberForChange'])) 
				{
					if (!$this->Users_model->updatePhoneNumber($UserID, $UserData['PhoneNumberForChange'])) 
					{

						$this->Return['ResponseCode'] = 500;

						$this->Return['Message'] = "An error occurred. Please contact the Admin for more info.";

					}
				}
				else 
				{
					$this->Return['Message'] = "Mobile number has been verified successfully.";
					die;
				}

				//echo "<pre>Before = "; print_r($this->Return);
				//$this->Recovery_model->deleteToken($this->Post['OTP'], 3); /*delete token in any case*/
				//echo "<pre>After = "; print_r($this->Return);
			}	
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Invalid OTP.";
		}	

	}



	public function verifyPhoneStudent_post()
	{
		/* Validation section */
 
		$this->form_validation->set_rules('OTP', 'OTP', 'trim|required|callback_validateToken[3]');

		$this->form_validation->validation($this); /* Run validation */

		/* Validation - ends */

		$UserIDObj = $this->Recovery_model->verifyTokenPhone($this->Post['OTP'], 3);
		
		if(isset($UserIDObj) && !empty($UserIDObj))
		{
			$UserID = $UserIDObj-> UserID;

			$datetime1 = new DateTime();
			$datetime2 = new DateTime($UserIDObj-> EntryDate);
			$interval = $datetime1->diff($datetime2);
			$elapsed = $interval->format('%i');
			
			$this->Return['Data'] = $UserID;

			if($elapsed > 30)
			{
				$this->Recovery_model->expiredToken($this->Post['OTP'], 3);
				
				$this->Return['ResponseCode'] = 700;

				$this->Return['Message'] = "OTP is expired, Please click on send OTP button to get new OTP.";
			}
			else
			{
				/* check for PhoneNo. update */

				$UserData = $this->Users_model->getUsers('PhoneNumber,PhoneNumberForChange,Email', array(

					'UserID' => $UserID 
				));				
				

				$this->Recovery_model->deleteToken($this->Post['OTP'], 3); /*delete token in any case*/

				if (!empty($UserData['PhoneNumberForChange'])) 
				{
					if (!$this->Users_model->updatePhoneNumber($UserID, $UserData['PhoneNumberForChange'])) 
					{
						$this->Return['ResponseCode'] = 500;

						$this->Return['Message'] = "Error occur while updating info. Please contact to Institute/Admin.";

						die;
					}
				}

				
				$sql = "UPDATE tbl_tokens SET StatusID = '3' WHERE UserID = '$UserID' AND Type IN(2,3) LIMIT 2";
				$this->Users_model->updateBySql($sql);

				
				if(isset($UserData['PhoneNumber']) && !empty($UserData['PhoneNumber']))
				{
					$mobile = trim($UserData['PhoneNumber']);
				}
				else
				{
					$mobile = trim($UserData['PhoneNumberForChange']);
				}

				$sql = "UPDATE tbl_users SET PhoneNumber = '$mobile', PhoneNumberForChange = '' WHERE UserID = '$UserID' LIMIT 1";
				$this->Users_model->updateBySql($sql);			
				
				
				$email = trim($UserData['Email']);
				$password = $this->Users_model->random_strings(6);
				$passwordMD5 = md5($password);
				
				$date = date("Y-m-d H:i:s");

				
				$sql = "UPDATE tbl_users_login SET Password = '$passwordMD5' WHERE UserID = '$UserID' AND SourceID = 1 LIMIT 1";
				
				$c = $this->Users_model->updateBySql($sql);
				
				if($c <= 0)
				{
					$sql = "DELETE FROM tbl_users_login WHERE UserID = '$UserID'";						
					$this->Users_model->updateBySql($sql);


					$sql = "INSERT INTO tbl_users_login(UserID,Password,SourceID,EntryDate,ModifiedDate) VALUES('$UserID', '$passwordMD5', '1','$date','$date')";
					
					$c = $this->Users_model->updateBySql($sql);
				}


				sendSMS(array(
					'PhoneNumber' 	=> $mobile,			
				 	'Message'			=> "Your login details are: URL=https://iconik.in, Email=".$email." and Password=".$password." Regards Iconik Team."
				));

				$this->Return['Message'] = "Mobile number has been verified successfully. Login details has been sent on your registered mobile number.";

				die;								
			}	
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Invalid OTP.";
		}	

	}







	public function verify_get()
	{
		$OTP = @$this->input->get('otp');

		if(!empty($this->input->get('type'))){
			$TYPE = $this->input->get('type');
		}

		$UserData = array();

		$Msg = '';

		$this->load->model('Recovery_model');

		$UserID = $this->Recovery_model->verifyToken($OTP,2);

		// if (!$UserID){

		// 	$MediaURL = ASSET_BASE_URL."img/emailer/output-onlinepngtools.png"; 

		// 	$Msg = "Sorry! You have a wrong entry or you have already verified your email address.";

		// }else{

			$UserData = $this->Users_model->getUsers('UserTypeID,FirstName,LastName,Email,EmailForChange,PhoneNumberForChange,StatusID,ProfilePic,ParentUserID', array('UserID'=>$UserID));

			//echo "<pre>"; print_r($UserData);

						/* change entity status to activate */

			$this->Entity_model->updateEntityInfo($UserID, array("StatusID"=>2));



			$this->Recovery_model->deleteToken($OTP,2); /*delete token in any case*/	
		

			if(!empty($UserData['ParentUserID'])){
				$InstituteID = $this->Common_model->getInstituteByEntity($UserData['ParentUserID']);
				$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName,ProfilePic");

				$instProfilePic = $InstituteData['ProfilePic'];

				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $UserData['ParentUserID']),TRUE);
			    $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

			    if(!empty($instProfilePic) && isset($instProfilePic))
		        {				    	
			    	$MediaURL= BASE_URL."uploads/profile/picture/".$instProfilePic;
			    }
			    /*elseif(!empty($Record['InstituteProfilePic']))
			    {
			    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
			    }*/
			    else
			    {
			    	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
			    }

			    $Signature =  "Thanks, <br>".$InstituteData['FirstName'];
			    $InstituteName = $InstituteData['FirstName'];
			}else{
				if(!empty($UserData['UserTypeID']) && $UserData['UserTypeID'] == 7){

					$InstituteID = $this->Common_model->getInstituteByEntity($UserID);
					$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName,PhoneNumber,ProfilePic");

					$instProfilePic = $InstituteData['ProfilePic'];

					if(!empty($InstituteID))
					{
						$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
					    $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

					    if(!empty($instProfilePic) && isset($instProfilePic))
				        {				    	
					    	$MediaURL= BASE_URL."uploads/profile/picture/".$instProfilePic;
					    }
					    /*elseif(!empty($Record['InstituteProfilePic']))
					    {
					    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
					    }*/
					    else
					    {
					    	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
					    }
					}

					$Signature =  "Thanks, <br>".$InstituteData['FirstName'];
					$InstituteName = $InstituteData['FirstName'];					
				}else{
					$MediaURL= BASE_URL."uploads/profile/picture/default.jpg"; 
					$Signature = "";
					$InstituteName = "";
				}
			}
		//}
			//echo "type=".$TYPE;
			//echo "<pre>"; print_r($UserData);
			//echo $MediaURL;die;

		$Input_arr = array();
		if(!empty($TYPE) && $TYPE == 'MarketPlace'){
			$Token = $this->Recovery_model->generateToken($UserID, 3);
			$Input_arr['PhoneNumber'] =  $UserData['PhoneNumberForChange'];
			$Input_arr['Message'] =  "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes.";
			sendSMS($Input_arr);
		}else{
			$Token = $this->Recovery_model->generateToken($UserID, 3);
			$Input_arr['PhoneNumber'] =  $UserData['PhoneNumberForChange'];
			$Input_arr['Message'] = "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes.";
			sendSMS($Input_arr);
		}
		
 		
		$content = $this->load->view('emailer/email_verify',array('Error'=>$Msg, 'UserData'=>$UserData,"InstituteProfilePic"=>$MediaURL,'Signature' => " ", "InstituteName" => $InstituteName),true);
		//echo $content;die;
		echo emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE));
		//echo emailTemplate($this->load->view('emailer/email_verify',array('Error'=>$Msg, 'UserData'=>$UserData,"InstituteProfilePic"=>$MediaURL,'Signature' => $Signature, "InstituteName" => $InstituteName),true));

	}



	public function mpverify_post()
	{
		$OTP = @$this->input->get('otp');

		$PASSOTP = @$this->input->get('passotp');

		$UserData = array();

		$Msg = '';

		$this->load->model('Recovery_model');

		$UserID = $this->Recovery_model->verifyToken($OTP,2);

		$this->Entity_model->updateEntityInfo($UserID, array("StatusID"=>2));

		$this->Recovery_model->deleteToken($OTP,2); /*delete token in any case*/	
			
		redirect(ADMIN_BASE_URL.'recovery/reset?OTP='.$PASSOTP, 'refresh');				
	}

	public function verifyEmail1_post()
	{
		$OTP = @$this->input->post('otp');

		if(!empty($this->input->post('type'))){
			$TYPE = $this->input->post('type');
		}

		$UserData = array();

		$Msg = '';

		$this->load->model('Recovery_model');

		$UserIDObj = $this->Recovery_model->verifyTokenAndGetData($OTP,2,3);
		
		
		if($UserIDObj->UserID)
		{
			$UserID = $UserIDObj->UserID;

			$phoneData = $this->Recovery_model->getTokenInfoByID($UserIDObj->UserID,3);
			//print_r($phoneData); //die;
			$datetime1 = new DateTime();
			//print_r($datetime1);
			$datetime2 = new DateTime($phoneData->EntryDate);
			//print_r($datetime2);
			$interval = $datetime1->diff($datetime2);
			//print_r($interval);	
			$elapsed = ($interval->format('%d') * (24*60)) + ($interval->format('%h') * 60) + $interval->format('%i');

			if($elapsed > 30)
			{				
				$this->Return['ResponseCode'] = 700;
				$this->Return['Message'] = "OTP expired.";
				$this->Return['Data'] = $phoneData;
			}
			else if($phoneData->StatusID != 1)
			{
				$this->Return['ResponseCode'] = 300;
				$this->Return['Message'] = "Already verified.";
			}

			//echo "111"; die;
		}
		else
		{
			
			$UserIDObj = $this->Recovery_model->verifyTokenAndGetData($OTP,2,1);

			$UserID = $UserIDObj->UserID;
			
				$UserData = $this->Users_model->getUsers('UserTypeID,FirstName,LastName,Email,EmailForChange,PhoneNumberForChange,StatusID,ProfilePic,ParentUserID',

				array('UserID'=>$UserID));

if($UserData['UserTypeID']!=10){
				$this->Entity_model->updateEntityInfo($UserID, array("StatusID"=>2));
}


				$this->Recovery_model->deleteToken($OTP,2); /*delete token in any case*/	
			

				if(!empty($UserData['ParentUserID'])){
					$InstituteID = $this->Common_model->getInstituteByEntity($UserData['ParentUserID']);
					$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $UserData['ParentUserID']),TRUE);
				    $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

				    if(!empty($Record['InstituteProfilePic'])){
				    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
				    }else{
				    	$MediaURL = ASSET_BASE_URL."img/emailer/output-onlinepngtools.png"; 
				    }

				    $Signature =  "Thanks, <br>".$InstituteData['FirstName'];
				    $InstituteName = $InstituteData['FirstName'];
				}else{
					if(!empty($UserData['UserTypeID']) && $UserData['UserTypeID'] == 7){

						$InstituteID = $this->Common_model->getInstituteByEntity($UserID);
						$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName,PhoneNumber");
						if(!empty($InstituteID)){

							$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
						    $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

						    if(!empty($Record['InstituteProfilePic'])){
						    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
						    }else{
						    	$MediaURL = ASSET_BASE_URL."img/emailer/output-onlinepngtools.png"; 
						    }
						}

						$Signature =  "Thanks, <br>".$InstituteData['FirstName'];
						$InstituteName = $InstituteData['FirstName'];					
					}else{
						$MediaURL = ASSET_BASE_URL."img/emailer/output-onlinepngtools.png"; 
						$Signature = "";
						$InstituteName = "";
					}
				}

				$Input_arr = array();
				if(!empty($TYPE) && $TYPE == 'MarketPlace'){
					$Token = $this->Recovery_model->generateToken($UserID, 3);
					$Input_arr['PhoneNumber'] =  $UserData['PhoneNumberForChange'];
					$Input_arr['Message'] =  "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes.";
					
					sendSMS($Input_arr);
				}else{
					$Token = $this->Recovery_model->generateToken($UserID, 3);
					$Input_arr['PhoneNumber'] =  $UserData['PhoneNumberForChange'];
					$Input_arr['Message'] = "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes.";
					sendSMS($Input_arr);
				}
				
		 
				echo emailTemplate($this->load->view('emailer/email_verify',array('Error'=>$Msg, 'UserData'=>$UserData,"InstituteProfilePic"=>$MediaURL,'Signature' => $Signature, "InstituteName" => $InstituteName),true));
			
		}
	}

	//Apply for loan---------------------------------------------
	public function applyForLoan_post()
	{ 
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('LoanPurpose', 'Purpose of loan', 'trim|required');
		$this->form_validation->set_rules('LoanAmount', 'Loan amount required', 'trim|required');
		$this->form_validation->set_rules('LoanFamilyIncome', 'Family annual income', 'trim|required');

		$this->form_validation->validation($this); /* Run validation */
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$LoanID = $this->Users_model->applyForLoan($this->EntityID, $this->Post);
		
		if($LoanID == "Already Applied")
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "You have already applied for the loan.";
		}
		elseif($LoanID > 0)
		{
			$this->Return['ResponseCode'] = 200;

			$this->Return['Message'] = "Successfully applied for loan.";
		}
		else
		{
			$this->Return['ResponseCode'] = 500;

			$this->Return['Message'] = "Can not saved record. Please check entry and try again.";
		}
	}

}