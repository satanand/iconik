app.controller('PageController', function($scope, $http, $timeout) {



    $scope.data.dataList = [];
    $scope.APIURL = API_URL + "upload/image";

    console.log($scope.APIURL);
    $scope.getFilterData = function() {
        $scope.getSecondLevelPermission();
    }

    $('.Year').datetimepicker({
        format: "yyyy",
        startView: 'decade',
        minView: 'decade',
        viewSelect: 'decade',
        autoclose: true,
    });

    $scope.applyFilter = function() {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }


    $scope.applytinymce = function() {
        tinymce.init({
            selector: '#MediaContent',
            setup: function(editor) {
                editor.on('keyup', function(e) {
                    console.log('edited. Contents: ' + editor.getContent());
                    formDataCheck(editor.getContent());
                });
            }

        });
    }

    /*get user NAme*/
    $scope.getUserData = function()

    {

        var data = 'SessionKey=' + SessionKey;
        $scope.data.UserData = [];
        $http.post(API_URL + 'Library/getUserData', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data) {
                /* success case */


                for (var i in response.Data.Records) {

                    $scope.data.UserData.push(response.Data.Records[i]);
                }

                //console.log($scope.data.UserData);

                $timeout(function() {

                    $("select.chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8
                    }).trigger("chosen:updated");

                }, 200);
            }

        });


    }
    /*list append*/



    $scope.getList = function(FromPG = 0, Year = "", Keyword = "") {
        //if ($scope.data.listLoading || $scope.data.noRecords) return;



        $scope.data.pageSize = 20;

        if (FromPG == 1) 
        {

            $scope.data.pageNo = 1;

            $scope.data.dataList = [];
        }

        $scope.data.listLoading = true;


        var data = 'SessionKey=' + SessionKey + '&Year=' + Year + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize() + '&Keyword=' + Keyword;

        $http.post(API_URL + 'walloffame/getwalloffame', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }
                // console.log($scope.data.dataList);
                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;

            // $timeout(function(){  }, 800);

        });

    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) {
        $scope.getUserData();
        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add_form.htm?' + Math.random();
        $('#add_model').modal({
            show: true
        });
        var currentTime = new Date();
        $timeout(function() {


            $('#NewsDate').datepicker();

            $('.Year').datetimepicker({
                format: "yyyy",
                startView: 'decade',
                minView: 'decade',
                viewSelect: 'decade',
                autoclose: true,
            });


            tinymce.init({
                selector: '#MediaContent'
            });


            $(".chosen-select").chosen({
                width: '100%',
                "disable_search_threshold": 8,
                "placeholder_text_multiple": "Please Select",
            }).trigger("chosen:updated");

        }, 700);
    }



    /*load edit form*/



    $scope.loadFormEdit = function(Position, MediaGUID) {

        $scope.data.Position = Position;
        $scope.getUserData();
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'walloffame/getwalloffameBy', 'SessionKey=' + SessionKey + '&MediaGUID=' + MediaGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#edits_model').modal({
                    show: true
                });

                //var currentTime = new Date();

                $timeout(function() {
                    $('#NewsDate').datepicker();

                    // $('#NewsDate').datetimepicker({
                    //     format: 'dd-mm-yyyy',
                    //     minView: 2
                    // });
                    $('.Year').datetimepicker({
                        format: "yyyy",
                        startView: 'decade',
                        minView: 'decade',
                        viewSelect: 'decade',
                        autoclose: true,
                    });
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                    tinymce.init({
                        selector: '#MediaContent'
                    });
                }, 100);

            }

        });

    }


    $scope.loadFormView = function(Position, MediaGUID) {

        $scope.data.Position = Position;
        $scope.getUserData();
        $scope.templateURLView = PATH_TEMPLATE + module + '/view_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'walloffame/getwalloffameBy', 'SessionKey=' + SessionKey + '&MediaGUID=' + MediaGUID, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200) {
                /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#view_model').modal({
                    show: true
                });
            }

        });
    }


    $scope.addData = function() {
        $scope.editDataLoading = true;

        $scope.loadeSubmit = true;

        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();
        $http.post(API_URL + 'walloffame/add', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */

                SuccessPopup(response.Message);
                $scope.applyFilter();
                $timeout(function() {

                    $('#add_model .close').click();
                    window.location.href = window.location.href;

                }, 1000);

            } else {

                ErrorPopup(response.Message);
            }

            $scope.editDataLoading = false;

            $scope.loadeSubmit = false;

        });

    }
    /*edit data*/


    $scope.editData = function() {

        $scope.editDataLoading = true;

        $scope.loadeSubmit = true;


        tinyMCE.triggerSave();

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();

        $http.post(API_URL + 'walloffame/editWalloffame', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) {
                /* success case */
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position] = response.Data;
                $scope.editDataLoading = false;

                $scope.loadeSubmit = false;
                
                $timeout(function() {

                    $('#edits_model .close').click();
                    window.location.href = window.location.href;

                }, 1000);

            } else {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;

            $scope.loadeSubmit = false;

        });

    } /*PermissionData data*/



    $scope.loadFormWallDelete = function(Position, MediaGUID) {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL + 'walloffame/delete', 'SessionKey=' + SessionKey + '&MediaGUID=' + MediaGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) {
                    /* success case */

                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;

                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }




});



$("#add_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});

$("#edits_model").on("hidden.bs.modal", function() {
    tinymce.remove();
});



function search_records()
{    
    var Year = $("#Year").val();
    var Keyword = $("#Keyword").val();

    angular.element(document.getElementById('content-body')).scope().getList(1, Year, Keyword);
}


function clear_search_records()
{    
    $("#Year, #Keyword").val("");    

    search_records();
}

/* sortable - ends */