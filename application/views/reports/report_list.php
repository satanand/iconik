<?php
$qs_sid = $qs_rt = "";
if(isset($_REQUEST['sid']) && !empty($_REQUEST['sid']))
{
	$qs_sid = $_REQUEST['sid'];
}

if(isset($_REQUEST['rt']) && !empty($_REQUEST['rt']))
{
	$qs_rt = $_REQUEST['rt'];
}

$qs_fd = date("01-m-Y");
if(isset($_REQUEST['fd']) && !empty($_REQUEST['fd']))
{
	$qs_fd = $_REQUEST['fd'];
}

$qs_td = date("d-m-Y");
if(isset($_REQUEST['td']) && !empty($_REQUEST['td']))
{
	$qs_td = $_REQUEST['td'];
}
?>

<style type="text/css">
.glyphicon {
position: relative;
top: -4px;
display: inline-block;
font-family: 'Glyphicons Halflings';
font-style: normal;
font-weight: normal;
line-height: 1;
-webkit-font-smoothing: antialiased;
font-size: 20px;
padding: 4px;
}

.hide_class
{
	display: none;
}

fieldset
{
	padding: 5px !important;
	border: 1px solid #ccc !important;
	margin-bottom: 3% !important; 
}

fieldset legend
{
	font-size: 12px !important;
}
</style>

<header class="panel-heading">
<h1 class="h4" id="top-heading">Manage Reports</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()" id="content-body">
<form id="filterForm" role="form" autocomplete="off">
<div class="clearfix mt-2 mb-2">
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-3">
<div class="form-group">
	<label><span style="color:red;">*</span>Report Type</label>	
	<select name="filterReportType" id="filterReportType" class="form-control" onchange="report_type(this.value);">
		<option value="">Select Report Type</option>
		<option value="enquiries">Enquiries</option>
		<option value="courses">Courses</option>
		<option value="fees">Fees</option>
		<option value="fee_collections">Fee Collections</option>
		<option value="batches">Batches</option>
		<option value="batch_attendance_details">Batch Attendance Details</option>
		<option value="student_details">Student Details</option>
		<option value="student_key_details">Student Key Details</option>
	</select>
</div>
</div>	

<div class="col-md-3">
<div class="form-group">
	<label><span style="color:red;">*</span><span class="ReportDateLabel"></span> From Date</label>
	<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="<?php echo date('01-m-Y');?>" placeholder="From Date" readonly>
</div>
</div>

<div class="col-md-3">
<div class="form-group">
	<label><span style="color:red;">*</span><span class="ReportDateLabel"></span> To Date</label>
	<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="<?php echo date('d-m-Y');?>" placeholder="To Date" readonly>
</div>
</div>	

<div class="col-md-2"></div>	
</div>



<!--Columns Of Reports=================================================================================-->
<fieldset id="columns_fieldset" style="display: none;">
<legend>Select columns to include in Report</legend>
<?php
	$this->load->view('reports/report_columns');
?>	
</fieldset>
<!--End Of Columns Fieldset==============================================================================-->



<!--Filters Of Reports=================================================================================-->
<!-- <fieldset id="filters_fieldset" style="display: none;">
<legend>Select filters to sort the Search Result</legend>
<?php
	//$this->load->view('reports/report_filters');
?>
</fieldset> -->
<!--End Of Filters Fieldset==============================================================================-->



<div class="row">
<div class="col-md-6"></div>
<div class="col-md-6">
<div class="form-group" style="float: right;">
	<button class="btn btn-primary ml-1 disable_btn" name="search_btn" onclick="search_records();" disabled><i class="fa fa-search"></i> Generate Report</button>&nbsp;
	
	<button class="btn btn-danger ml-1 disable_btn" name="clear_search_btn" ng-click="search_history();" disabled><i class="fa fa-save"></i> Search History</button>

	<!-- <button class="btn btn-danger ml-1 disable_btn" name="clear_search_btn" onclick="clear_search_records();" disabled><i class="fa fa-refresh"></i> Reset Filters</button> -->
	<input name="PageNo" type="hidden" value="1">
	<input name="PageSize" type="hidden" value="100">
	<input name="SearchType" id="SearchType" type="hidden" value="s">
	<input name="SearchID" id="SearchID" type="hidden" value="<?php echo $qs_sid;?>">

</div>
</div>
</div>
			
</div>

<div class="row">
<div class="col-md-12"><hr/></div>
</div>


<div class="table-responsive" infinite-scroll="getList(0);" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

<h5 id="totalRecords" class="hide_class">Total Records: <b></b></h5>

<table id="reports_content" class="table table-striped table-condensed table-hover table-sortable"></table>

<hr/>

<div class="row hide_class" id="action_btn">
<div class="col-md-12">
<div class="form-group" style="float: right;">
<button class="btn btn-warning ml-1 btn-sm" name="search_btn" onclick="export_pdf();"><i class="fa fa-file"></i> Export in PDF</button>&nbsp;

<button class="btn btn-warning ml-1 btn-sm" name="search_btn" onclick="export_excel();"><i class="fa fa-file"></i> Export in Excel</button>&nbsp;

<button class="btn btn-warning ml-1 btn-sm" name="search_btn" ng-click="loadFormAdd();"><i class="fa fa-save"></i> Save Search</button>&nbsp;
</div>
</div>		
</div>
</div>
</form>


<div class="modal fade" id="add_model">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
	<div class="modal-header">
		<h3 class="modal-title h5">Save Search</h3>					    	
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</div>

	<div ng-include="templateURLAdd"></div>
</div>
</div>
</div>


<div class="modal fade" id="list_model">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
	<div class="modal-header">
		<h3 class="modal-title h5">Search History</h3>					    	
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</div>

	<div ng-include="templateURLList"></div>
</div>
</div>
</div>


</div><!-- Body/ -->

<script type="text/javascript">
var qs_rt = "<?php echo $qs_rt;?>";	
var qs_fd = "<?php echo $qs_fd;?>";	
var qs_td = "<?php echo $qs_td;?>";	

$(function()
{
	if(qs_rt != '' && qs_fd != '' && qs_td != '')
	{
		$("#filterReportType").val(qs_rt);
		$("#filterFromDate").val(qs_fd);
		$("#filterToDate").val(qs_td);		

		report_type(qs_rt);

		search_records();
	}
});
</script>

<div style="display: none;">
<form name="newregform" method="post" action="reports/daily_registrations">
	<button class="btn btn-warning" name="newregbtn" ng-click="export_new_reg();">.</button>
</form>
</div>