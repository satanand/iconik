<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Students</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="student-body" ng-init="getFilterData(0)"><!-- Body -->

	<div class="clearfix mt-2 mb-2">

		<div class="row float-right ml-6">   
	   <div class="float-right mr-2">

	   		<!-- <a style="text-decoration: underline; padding: 12px;" href="/asset/student/tbl_users.xlsx" download>Download excel to get format to add students</a> -->


	   		<button ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd('FromStatistics');">Add Student</button>

	   		<button class="btn btn-success btn-sm ml-1 float-right" ng-click="loadImportStudents();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Import Students</button>

	    	<button title="Assign batch to students who purchase the APP." class="btn btn-success btn-sm ml-1 float-right" ng-click="showAssignBatchStudentModal();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Assign Batch to APP Students</button>

		</div>

	</div>


	   <div class="row">

	      <div class="col-md-4">

	         <div class="form-group">

	            <select class="form-control" onchange="getCategoryBatch(this.value,'getStudentStatistics')" name="ParentCat" id="Courses">

	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>

	               <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

	            </select>

	         </div>

	      </div>

	      <div class="col-md-4">

	         <div class="form-group">

	            <div id="subcategory_section">

	               <select id="subject" name="CategoryGUIDs" onchange="filterStudentStatistics(this.value)"  class="form-control">

	                  <option value="">Select Batch</option>

	                  <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}">{{row.BatchName}}</option>

	               </select>

	            </div>

	         </div>

	      </div>


	   

	   </div>
	</div>   
	   
	



	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">

	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">





	<!-- Data table -->

<div class="table-responsive block_pad_md"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

	

		<!-- data table -->

		<table class="table table-striped table-hover"  id="tableToExport">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 100px; text-align: center;">Course</th>

					<th style="width: 100px; text-align: center;">Batch</th>

					<th style="width: 100px; text-align: center;">Students</th>

					<th style="width: 100px; text-align: center;">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row" ng-repeat="(key, row) in data.dataList">

					<td style="text-align: center;">{{row.ParentCategoryName}}</td>

					<td style="text-align: center;" ng-if="row.BatchName == 'Total'"><b>{{row.BatchName}}</b></td>

					<td style="text-align: center;" ng-if="row.BatchName != 'Total'">{{row.BatchName}}</td>

					<td style="text-align: center;" ng-if="row.BatchName == 'Total'"><b>{{row.student_count}}</b></td>

					<td style="text-align: center;" ng-if="row.BatchName != 'Total'">{{row.count_student}}</td>

					<td class="text-center" style="text-align: center;">

						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'students/students_list/?parentCategoryGUID={{row.parentCategoryGUID}}&BatchGUID={{row.BatchGUID}}'; ?>" data-toggle="tooltip" title="Students list"></a>

					</td>

				</tr>

			</tbody>

		</table>

		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span>No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	

	<div class="modal fade" id="import_excel_model">

		<div class="modal-dialog" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Import Students</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLImport"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Student</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_subject">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Subjcet</h3>     	

					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="subjectTemplateURLAdd"></div>

			</div>

		</div>

	</div>











	<!-- edit Modal -->

	<div class="modal fade" id="edit_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>





	<!-- edit Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>



<div class="modal fade" id="bulk_assign_student">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h3 class="modal-title h5">Assign Batch</h3>     	
<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div>
<form id="bulk_assign_batch_student" name="bulk_assign_batch_student" autocomplete="off" >
<div class="modal-body">
<div class="form-area">

<div class="row">
<div class="col-md-6">
 <div class="form-group">
 	<label>Select Course <em style="color:red;">*</em></label>
    <select class="form-control" onchange="getCategoryBatch(this.value,'getStudentStatistics')" name="BASCourseID">
       <option value="">Select Course</option>
       <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>
    </select>
 </div>
</div>


<div class="col-md-6">
 <div class="form-group">
    <div id="subcategory_section">
    	<label>Select Batch <em style="color:red;">*</em></label>
       	<select name="BASBatchID" onchange="filterStudentStatistics(this.value)"  class="form-control">
          <option value="">Select Batch</option>
          <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}">{{row.BatchName}}</option>
       	</select>
    </div>
 </div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="form-group">

<table class="table table-striped table-hover">
<thead>
<tr>
	
	<th style="width: 10px;"><input type="checkbox" id="BASChkBoxAll" onchange="chkUnChkAllBoxes();" value="1"></th>
	
	<th style="width: 100px;">Student Name</th>
	
	<th style="width: 100px;">Email</th>
	
	<th style="width: 100px;">Mobile</th>

</tr>
</thead>

<tbody>

<tr scope="row" ng-repeat="(key, row) in data.StudentDataList">
	
	<td><input type="checkbox" name="BASChkBox[]" class="BASChkBox" value="{{row.StudentID}}"></td>
	
	<td>{{row.FirstName}}&nbsp;{{row.LastName}}</td>
	
	<td>{{row.Email}}</td>
	
	<td>{{row.Mobile}}</td>

</tr>

<tr ng-if="data.StudentDataList.length <= 0">
	
	<td colspan="4">No record found.</td>

</tr>

</tbody>
</table>

<p ng-if="data.StudentDataList.length > 0"><br/>
	<img src="./asset/img/ajax-loader.gif" ng-if="loadeSubmitStudent" class="processingLoaderSetDown">

	<button type="button" name="BulkAssign" class="btn btn-primary btn-sm" ng-disabled="loadeSubmitStudent" ng-click="BulkAssignBatchSave()">Submit</button>&nbsp;&nbsp;&nbsp;

	<button type="button" class="btn btn-danger btn-sm" ng-disabled="loadeSubmitStudent" data-dismiss="modal">Cancel</button>
</p>



<p ng-if="data.UnAssignListLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

</div>
</div>
</div>


</div>
</div>
</form>
</div>
</div>
</div>
</div>

</div><!-- Body/ -->

<script type="text/javascript">
function chkUnChkAllBoxes()
{
	var chk = $("#BASChkBoxAll").is(":checked");

	if(chk)
	{
		$(".BASChkBox").prop("checked", "checked");
		$(".BASChkBox").attr("checked", "checked");
	}
	else
	{
		$(".BASChkBox").removeAttr("checked");
	}
}
</script>