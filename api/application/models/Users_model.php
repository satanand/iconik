<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}

	function updateBySql($sql) 
	{   
	    $this->db->query($sql);

	    return $this->db->affected_rows();
	} 


	function random_strings($length_of_string) { 
	  
	    // String of all alphanumeric character 
	    $str_result = '23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz'; 
	  
	    // Shufle the $str_result and returns substring 
	    // of specified length 
	    return substr(str_shuffle($str_result), 0, $length_of_string); 
	} 



	/*
	Description: 	Use to update user profile info.
	*/
	function updateUserInfoOnSignup($UserEmail, $Input=array()){
		// if(!empty($Input['MasterFranchisee']) && $Input['MasterFranchisee'] == 'yes'){
		// 	$Input['InstitutrID'] = NULL;
		// }


			$UpdateArray = array_filter(array(	
				"FirstName" 			=>	@$Input['FirstName'],
				"Email" 				=>	@strtolower($Input['Email']),
				"PhoneNumber" 			=>	@$Input['PhoneNumber']
			));



		
		
			if(isset($Input['PhoneNumber']) && $Input['PhoneNumber']==''){	$UpdateArray['PhoneNumber'] = null;	}
		

			/*for change email address*/
			if(!empty($UpdateArray['Email']) || !empty($UpdateArray['PhoneNumber'])){
				$UserData = $this->Users_model->getUsers('UserID,Email,FirstName,PhoneNumber,ParentUserID',array('Email'=>$UpdateArray['Email']));	
			}

			if(empty($UserData)){
				return false;
			}


			//print_r($UserData); die;

			/*for update email address*/
			if(!empty($UpdateArray['Email'])){   
				//if($UserData['Email']==$UpdateArray['Email']){  print_r($UpdateArray); die;
					$UpdateArray['EmailForChange'] = $UpdateArray['Email'];			
					/* Genrate a Token for Email verification and save to tokens table. */
					$this->load->model('Recovery_model');
					$Token = $this->Recovery_model->generateToken($UserData['UserID'], 2);

					$InstituteData = $this->Common_model->getInstituteDetailByID($UserData['ParentUserID'],"Email,UserID,FirstName");

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteData['UserID']),TRUE);
				    $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

				   

				    if(!empty($Record['InstituteProfilePic'])){
				    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
				    }else{
				    	$MediaURL= "";
				    }


				     //print_r($Record['InstituteProfilePic']); die;
				
				    //echo  $MediaURL; die;
					$content = $this->load->view('emailer/signup', 
							array("Name" => $Input['FirstName'],'Token' => $Token,"InstituteProfilePic"=>@$MediaURL) , TRUE);
					sendMail(array(

						'emailTo' => $UpdateArray['EmailForChange'],
						'From_name' => $InstituteData['FirstName'],		
						'emailSubject' => $Input['FirstName']. " Verify Your Email",						
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

					));
					
					
					$content = $this->load->view('emailer/adduser',array("Name" =>  $Input['FirstName'], 'Password' => $Input['Password'],"Email"=>$Input['Email'],"InstituteProfilePic"=>@$MediaURL),TRUE);
					sendMail(array(
						'emailTo' 		=> $UpdateArray['Email'],
						// 'From_email' => $InstituteData['Email'],
						'From_name' => $InstituteData['FirstName'],			
						'emailSubject'	=> $UpdateArray['FirstName']." Welcome to ICONIK.",					
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
					));

					unset($UpdateArray['Email']);
				//}
			}


			/*for update phone number*/
			if(!empty($UpdateArray['PhoneNumber']) && PHONE_NO_VERIFICATION && !isset($Input['SkipPhoneNoVerification'])){
				if($UserData['PhoneNumber']!=$UpdateArray['PhoneNumber']){
					$UpdateArray['PhoneNumberForChange'] = $UpdateArray['PhoneNumber'];			
					/* Genrate a Token for PhoneNumber verification and save to tokens table. */
					$this->load->model('Recovery_model');
					$Token = $this->Recovery_model->generateToken($UserData['UserID'], 3);

					/* Send change phonenumber SMS to User with Token. */
					sendSMS(array(
						'PhoneNumber' 	=> $UpdateArray['PhoneNumberForChange'],			
						'Text'			=> SITE_NAME.", OTP to verify Mobile no. is: $Token",
					));
					unset($UpdateArray['PhoneNumber']);
				}
			}
		
			/* Update User details to users table. */
			if(!empty($UpdateArray)){
				$this->db->where('Email', $UserEmail);
				$this->db->limit(1);
				$this->db->update('tbl_users', $UpdateArray);
			}
	        
	        if(!empty($Input['Password'])){
	        	$UpdateArray = array_filter(array(
					"Password" 			=>	(!empty($Input['Password']) ? md5($Input['Password']) : '') ,
					"EntryDate" 	=>	date("Y-m-d H:i:s"),
					"UserID"  => $UserData['UserID'],
					"SourceID" => 1
				));

				// $this->db->where('Email', $UserEmail);
				// $this->db->limit(1);
				// $this->db->where('UserID', $UserData['UserID']);
				// $this->db->limit(1);
				$this->db->insert('tbl_users_login', $UpdateArray);
	        }

		return $UserData['UserID'];
	}

	/*
	Description: 	Use to update user profile info.
	*/
	function updateUserInfo($UserID, $Input=array()){
		// if(!empty($Input['MasterFranchisee']) && $Input['MasterFranchisee'] == 'yes'){
		// 	$Input['InstitutrID'] = NULL;
		// }
		if(!empty($Input['OwnerName']) && is_array($Input['OwnerName'])){
			$Input['OwnerName'] = implode(",", array_filter($Input['OwnerName']));
		}

		//print_r($Input['OwnerName']); die;

		if(!empty($Input['TeachingAids']) && is_array($Input['TeachingAids'])){
			$Input['TeachingAids'] = implode(",", $Input['TeachingAids']);
		}

		$UpdateArray = array_filter(array(
			"UserTypeID" 			=>	@$Input['UserTypeID'],			
			"FirstName" 			=>	@$Input['FirstName'],
			"MiddleName" 			=>	@$Input['MiddleName'],
			"LastName" 				=>	@$Input['LastName'],
			"About" 				=>	@$Input['About'],
			"About1" 				=>	@$Input['About1'],
			"About2" 				=>	@$Input['About2'],
			"ProfilePic" 			=>	@$Input['ProfilePic'],
			"ProfileCoverPic" 		=>	@$Input['ProfileCoverPic'],
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],			
			"BirthDate" 			=>	@$Input['BirthDate'],
			"Age" 					=>	@$Input['Age'],			
			"Height" 				=>	@$Input['Height'],		
			"Weight" 				=>	@$Input['Weight'],	
			"MaritalStatus"			=>	@$Input['MaritalStatus'],		


			"PhoneNumberForChange"	=>	@$Input['PhoneNumberForChange'],	
			"TelePhoneNumber"	=>	@$Input['TelePhoneNumber'],	
			"RegistrationNumber"	=>	@$Input['RegistrationNumber'],
			"CityCode"	=>	@$Input['CityCode'],
			"UrlType"	=>	@$Input['UrlType'],	

			"Address" 				=>	@$Input['Address'],	
			"Address1" 				=>	@$Input['Address1'],
			"Postal" 				=>	@$Input['Postal'],
			"CountryCode" 			=>	@$Input['CountryCode'],

			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"CountryCode" 			=>	@$Input['CountryCode'],
			"CityName" 				=>	@$Input['CityName'],
			"StateName" 			=>	@$Input['StateName'],
			"StateID" 			=>	@$Input['StateID'],
			"Latitude" 				=>	@$Input['Latitude'],	
			"Longitude" 			=>	@$Input['Longitude'],


			"LanguageKnown" 		=>	@$Input['LanguageKnown'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"Website" 				=>	@strtolower($Input['Website']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),	
			"TwitterURL" 			=>	@strtolower($Input['TwitterURL']),
			"GoogleURL" 			=>	@strtolower($Input['GoogleURL']),
			"InstagramURL" 			=>	@strtolower($Input['InstagramURL']),
			"LinkedInURL" 			=>	@strtolower($Input['LinkedInURL']),
			"WhatsApp" 				=>	@strtolower($Input['WhatsApp']),
			"MasterFranchisee"		=>	@strtolower($Input['MasterFranchisee']),
			"ParentUserID"		=>	@strtolower($Input['ParentUserID']),

			"OwnerName" 	=>	@$Input['OwnerName'],
			"Achivements" 	=>	@$Input['Achivements'],
			"ScholarshipTest" 	=>	@$Input['ScholarshipTest'],	
			"Classrooms" 	=>	@$Input['Classrooms'],
			"Library" 	=>	@$Input['Library'],
			"TeachingAids" 	=>	@$Input['TeachingAids'],
			"HostelFacility" =>	@$Input['HostelFacility'],
			"NumberOfRooms" =>	@$Input['NumberOfRooms'],
			"StudentPerRooms" =>	@$Input['StudentPerRooms'],
			"MessFacility" =>	@$Input['MessFacility']
		));

		if(!empty($Input['MasterFranchisee']) && $Input['MasterFranchisee'] == 'yes'){
			$UpdateArray['FranchiseeAssignCount'] = FranchiseeAssignCount + 1;
		}

		if(isset($Input['LastName']) && $Input['LastName']==''){		$UpdateArray['LastName'] = null;	}
		if(isset($Input['Username']) && $Input['Username']==''){		$UpdateArray['Username'] = null;	}
		if(isset($Input['Gender']) && $Input['Gender']==''){			$UpdateArray['Gender'] = null;		}
		if(isset($Input['BirthDate']) && $Input['BirthDate']==''){		$UpdateArray['BirthDate'] = null;	}
		if(isset($Input['Address']) && $Input['Address']==''){			$UpdateArray['Address'] = null;		}	
		if(isset($Input['PhoneNumber']) && $Input['PhoneNumber']==''){	$UpdateArray['PhoneNumber'] = null;	}
		if(isset($Input['Website']) && $Input['Website']==''){			$UpdateArray['Website'] = null;		}
		if(isset($Input['FacebookURL']) && $Input['FacebookURL']==''){	$UpdateArray['FacebookURL'] = null;	}
		if(isset($Input['TwitterURL']) && $Input['TwitterURL']==''){	$UpdateArray['TwitterURL'] = null;	}
		if(isset($Input['LinkedInURL']) && $Input['LinkedInURL']==''){	$UpdateArray['LinkedInURL'] = null;	}
		if(isset($Input['GoogleURL']) && $Input['GoogleURL']==''){	$UpdateArray['GoogleURL'] = null;	}
		if(isset($Input['InstagramURL']) && $Input['InstagramURL']==''){	$UpdateArray['InstagramURL'] = null;	}
		if(isset($Input['PhoneNumber']) && $Input['PhoneNumber']==''){	$UpdateArray['PhoneNumber'] = null;	}
		

		/*for change email address*/
		if(!empty($UpdateArray['Email']) || !empty($UpdateArray['PhoneNumber'])){
			$UserData = $this->Users_model->getUsers('Email,FirstName,PhoneNumber',array('UserID'=>$UserID));	
		}

		/*for update email address*/
		if(!empty($UpdateArray['Email'])){
			if($UserData['Email']!=$UpdateArray['Email']){
				$UpdateArray['EmailForChange'] = $UpdateArray['Email'];			
				/* Genrate a Token for Email verification and save to tokens table. */
				$this->load->model('Recovery_model');
				$Token = $this->Recovery_model->generateToken($UserID, 2);
				/* Send welcome Email to User with Token. */
				/*sendMail(array(
					'emailTo' 		=> $UpdateArray['EmailForChange'],			
					'emailSubject'	=> SITE_NAME.", OTP for change of email address.",
					'emailMessage'	=> emailTemplate($this->load->view('emailer/change_email',array("Name" =>  $UserData['Username'], 'Token' => $Token),TRUE)) 
				));*/

				$content = $this->load->view('emailer/signup', 
						array("Name" => $Input['FirstName'],'Token' => $Token) , TRUE);
				sendMail(array(
					'emailTo' => $UpdateArray['EmailForChange'],
					'emailSubject' => $Input['FirstName']. " Verify Your Email",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

				));
				unset($UpdateArray['Email']);
			}
		}


		/*for update phone number*/
		if(!empty($UpdateArray['PhoneNumber']) && PHONE_NO_VERIFICATION && !isset($Input['SkipPhoneNoVerification'])){
			if($UserData['PhoneNumber']!=$UpdateArray['PhoneNumber']){
				$UpdateArray['PhoneNumberForChange'] = $UpdateArray['PhoneNumber'];			
				/* Genrate a Token for PhoneNumber verification and save to tokens table. */
				$this->load->model('Recovery_model');
				$Token = $this->Recovery_model->generateToken($UserID, 3);

				/* Send change phonenumber SMS to User with Token. */
				sendSMS(array(
					'PhoneNumber' 	=> $UpdateArray['PhoneNumberForChange'],			
					'Text'			=> SITE_NAME.", OTP to verify Mobile no. is: $Token",
				));
				unset($UpdateArray['PhoneNumber']);
			}
		}

		if(!empty($Input['PanStatus'])){
			$UpdateArray['PanStatus'] = $Input['PanStatus'];
		}
		if(!empty($Input['BankStatus'])){
			$UpdateArray['BankStatus'] = $Input['BankStatus'];
		}

		//print_r($UpdateArray); die;
		
		/* Update User details to users table. */
		if(!empty($UpdateArray)){
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', $UpdateArray);
		}
        
        if(!empty($Input['Password'])){

        	$UpdateArray = array_filter(array(
			"Password" 			=>	(!empty($Input['Password']) ? md5($Input['Password']) : '') ,
			"ModifiedDate" 	=>	(!empty($Input['Password']) ? date("Y-m-d H:i:s") : '') ,
			"LastLoginDate" 	=>	@$Input['LastLoginDate']
		));

		$this->db->where('UserID', $UserID);
		$this->db->limit(1);
		$this->db->update('tbl_users_login', $UpdateArray);

        }
		


		if(!empty($Input['InterestGUIDs'])){
			/*Revoke categories - starts*/
			$this->db->where(array("EntityID"=>$UserID));
			$this->db->delete('tbl_entity_categories');
			/*Revoke categories - ends*/

			/*Assign categories - starts*/
			$this->load->model('Category_model');
			foreach($Input['InterestGUIDs'] as $CategoryGUID){
				$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
				if($CategoryData){
					$InsertCategory[] = array('EntityID'=>$UserID, 'CategoryID'=>$CategoryData['CategoryID']);
				}
			}
			if(!empty($InsertCategory)){
				$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
			}
			/*Assign categories - ends*/
		}


		if(!empty($Input['SpecialtyGUIDs'])){
			/*Revoke categories - starts*/
			$this->db->where(array("EntityID"=>$UserID));
			$this->db->delete('tbl_entity_categories');
			/*Revoke categories - ends*/

			/*Assign categories - starts*/
			$this->load->model('Category_model');
			foreach($Input['SpecialtyGUIDs'] as $CategoryGUID){
				$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
				if($CategoryData){
					$InsertCategory[] = array('EntityID'=>$UserID, 'CategoryID'=>$CategoryData['CategoryID']);
				}
			}
			if(!empty($InsertCategory)){
				$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
			}
			/*Assign categories - ends*/
		}


		
		$this->Entity_model->updateEntityInfo($UserID,array('StatusID'=>@$Input['StatusID']));
		return TRUE;
	}


	/*
	Description: 	Use to add student type 2 : updated profile info for approval by institute.
	*/
	function addStudentInfo($UserID, $Input=array()){

		$UserData = $this->Users_model->getUsers('Email,FirstName,PhoneNumber',array('UserID'=>$UserID));

		$InsertArray = array();

		// if(!empty($Input['MediaGUID'])){
		// 	$this->db->select("UserID");
		// 	$this->db->from("tbl_users");
		// 	$this->db->join("tbl_students","tbl_users.UserID = tbl_students.StudentID");
		// 	$this->db->where("tbl_users.FirstName", trim($Input['FirstName']));
		// 	$this->db->where("tbl_users.LastName", trim($Input['LastName']));
		// 	$this->db->where("tbl_users.Email",$Input['Email']);
		// 	$this->db->where("tbl_users.PhoneNumber",$Input['PhoneNumber']);
		// 	$this->db->where("tbl_users.Address",$Input['Address']);
		// 	$this->db->where("tbl_users.CountryCode",$Input['CountryCode']);
		// 	$this->db->where("tbl_users.StateName",$Input['StateName']);
		// 	$this->db->where("tbl_users.CityName",$Input['CityName']);
		// 	$this->db->where("tbl_users.Postal",$Input['Postal']);
		// 	$this->db->where("tbl_users.ProfilePic",$Input['MediaName']);
		// 	$this->db->where("tbl_students.FathersName",$Input['FathersName']);
		// 	$this->db->where("tbl_students.MothersName",$Input['MothersName']);
		// 	$this->db->where("tbl_students.PermanentAddress",$Input['PermanentAddress']);
		// 	$this->db->where("tbl_students.FathersEmail",$Input['FathersEmail']);
		// 	$this->db->where("tbl_students.FathersPhone",$Input['FathersPhone']);

		// 	if(!empty($Input['GuardianName'])){
		// 		$this->db->where("tbl_students.GuardianName",$Input['GuardianName']);
		// 	}else{
		// 		$this->db->where("tbl_students.GuardianName IS NULL");
		// 	}

		// 	if(!empty($Input['GuardianPhone'])){
		// 		$this->db->where("tbl_students.GuardianPhone",$Input['GuardianPhone']);
		// 	}else{
		// 		$this->db->where("tbl_students.GuardianPhone IS NULL");
		// 	}

		// 	if(!empty($Input['GuardianEmail'])){
		// 		$this->db->where("tbl_students.GuardianEmail",$Input['GuardianEmail']);
		// 	}else{
		// 		$this->db->where("tbl_students.GuardianEmail IS NULL");
		// 	}

		// 	if(!empty($Input['GuardianAddress'])){
		// 		$this->db->where("tbl_students.GuardianAddress",$Input['GuardianAddress']);
		// 	}else{
		// 		$this->db->where("tbl_students.GuardianAddress IS NULL");
		// 	}

		// 	$this->db->where("tbl_users.UserID",$UserID);
		// 	$data = $this->db->get(); //echo $this->db->last_query(); die;
		// 	if($data->num_rows() == 0){
		// 		$InsertArray = array_filter(array(
		// 				"StudentID" 			=>	$UserID,			
		// 				"FirstName" 			=>	@$Input['FirstName'],
		// 				"LastName" 			=>	@$Input['LastName'],
		// 				"ProfilePic" 			=>	@$Input['MediaName'],
		// 				"Email" 				=>	@strtolower($Input['Email']),
		// 				"PhoneNumber"	=>	@$Input['PhoneNumber'],
		// 				"Address" 				=>	@$Input['Address'],
		// 				"CountryCode" 			=>	@$Input['CountryCode'],
		// 				"CityName" 				=>	@$Input['CityName'],
		// 				"StateName" 			=>	@$Input['StateName'],
		// 				"Postal" 			=>	@$Input['Postal'],		
		// 				"FathersName" 			=>	@$Input['FathersName'],
		// 				"MothersName" 			=>	@$Input['MothersName'],
		// 				"PermanentAddress" 				=>	@strtolower($Input['PermanentAddress']),
		// 				"FathersEmail"	=>	@$Input['FathersEmail'],
		// 				"FathersPhone" 				=>	@$Input['FathersPhone'],
		// 				"GuardianName" 			=>	@$Input['GuardianName'],
		// 				"GuardianAddress" 				=>	@$Input['GuardianAddress'],
		// 				"GuardianEmail" 			=>	@$Input['GuardianEmail'],
		// 				"GuardianPhone" =>	@$Input['GuardianPhone'],
		// 				"EntryDate" 			=>	date("Y-m-d h:i:s")
		// 			));
		// 	}
		// }else{
			$this->db->select("UserID");
			$this->db->from("tbl_users");
			$this->db->join("tbl_students","tbl_users.UserID = tbl_students.StudentID");
			$this->db->where("tbl_users.FirstName", trim($Input['FirstName']));
			$this->db->where("tbl_users.LastName", trim($Input['LastName']));
			$this->db->where("tbl_users.Email",trim($Input['Email']));
			$this->db->where("tbl_users.PhoneNumber",trim($Input['PhoneNumber']));
			$this->db->where("tbl_users.Address",trim($Input['Address']));
			$this->db->where("tbl_users.CountryCode",trim($Input['CountryCode']));
			$this->db->where("tbl_users.StateName",trim($Input['StateName']));
			$this->db->where("tbl_users.CityName",trim($Input['CityName']));
			$this->db->where("tbl_users.Postal",trim($Input['Postal']));
			$this->db->where("tbl_students.FathersName",trim($Input['FathersName']));
			$this->db->where("tbl_students.MothersName",trim($Input['MothersName']));
			$this->db->where("tbl_students.PermanentAddress",trim($Input['PermanentAddress']));
			$this->db->where("tbl_students.FathersEmail",trim($Input['FathersEmail']));
			$this->db->where("tbl_students.FathersPhone",trim($Input['FathersPhone']));
			if(!empty($Input['GuardianName'])){
				$this->db->where("tbl_students.GuardianName",trim($Input['GuardianName']));
			}else{
				$this->db->where("tbl_students.GuardianName IS NULL");
			}

			if(!empty($Input['GuardianPhone'])){
				$this->db->where("tbl_students.GuardianPhone",trim($Input['GuardianPhone']));
			}else{
				$this->db->where("tbl_students.GuardianPhone IS NULL");
			}

			if(!empty($Input['GuardianEmail'])){
				$this->db->where("tbl_students.GuardianEmail",trim($Input['GuardianEmail']));
			}else{
				$this->db->where("tbl_students.GuardianEmail IS NULL");
			}

			if(!empty($Input['GuardianAddress'])){
				$this->db->where("tbl_students.GuardianAddress",trim($Input['GuardianAddress']));
			}else{
				$this->db->where("tbl_students.GuardianAddress IS NULL");
			}
			$this->db->where("tbl_users.UserID",$UserID);
			$data = $this->db->get(); //echo $this->db->last_query(); die;
			if($data->num_rows() == 0){
				$InsertArray = array_filter(array(
					"StudentID" 			=>	$UserID,			
					"FirstName" 			=>	@trim($Input['FirstName']),
					"LastName" 			=>	@trim($Input['LastName']),
					"ProfilePic" 			=>	@trim($Input['MediaName']),
					"Email" 				=>	@strtolower(trim($Input['Email'])),
					"PhoneNumber"	=>	@trim($Input['PhoneNumber']),
					"Address" 				=>	@trim($Input['Address']),
					"CountryCode" 			=>	@trim($Input['CountryCode']),
					"CityName" 				=>	@trim($Input['CityName']),
					"StateName" 			=>	@trim($Input['StateName']),
					"Postal" 			=>	@trim($Input['Postal']),		
					"FathersName" 			=>	@trim($Input['FathersName']),
					"MothersName" 			=>	@trim($Input['MothersName']),
					"PermanentAddress" 				=>	@strtolower(trim($Input['PermanentAddress'])),
					"FathersEmail"	=>	@trim($Input['FathersEmail']),
					"FathersPhone" 				=>	@trim($Input['FathersPhone']),
					"GuardianName" 			=>	@trim($Input['GuardianName']),
					"GuardianAddress" 				=>	@trim($Input['GuardianAddress']),
					"GuardianEmail" 			=>	@trim($Input['GuardianEmail']),
					"GuardianPhone" =>	@trim($Input['GuardianPhone']),
					"EntryDate" 			=>	date("Y-m-d h:i:s")
				));
			}
		//}

		/* Insert student type 2 details to users table. */
		if(!empty($InsertArray)){
			$this->db->insert('tbl_student_pending_profile', $InsertArray);
		

			$StudentData = $this->Common_model->getInstituteDetailByID($UserID,"Email,UserID,FirstName,LastName,PhoneNumber");

			$InstituteID=$this->Common_model->getInstituteByEntity($UserID);
			$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

			/* Send Email to institute for update profile approval. */
			$EmailText = $StudentData['FirstName']." ".$StudentData['LastName']." has updated his profile info, Please check and approve request to update profile.";
			
			$content = $this->load->view('emailer/common', 
					array("Name" => $InstituteData['FirstName'],'EmailText' => $EmailText) , TRUE);
			sendMail(array(
				'emailTo' => $InstituteData['Email'],
				'emailSubject' => "Student Profile Update Approval Request",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			/* Send Email to show student update profile status. */
			$EmailText = "Your profile update request successfully sent to institute for approval";
			$content = $this->load->view('emailer/common', 
					array("Name" => $StudentData['FirstName']." ".$StudentData['LastName'],'EmailText' => $EmailText) , TRUE);
			sendMail(array(
				'emailTo' => $StudentData['Email'],
				'emailSubject' => "Profile Update Status",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			/* Send SMS to show student update profile status. */
			if(!empty($StudentData['PhoneNumber'])){
				sendSMS(array(
				 	'PhoneNumber' 	=> $StudentData['PhoneNumber'],//			
				 	'Message'		=> "Thank you to update your profile, Your profile update request successfully sent to institute for approval."
				));
			}
			return 1;
		}else{
			return 2;
		}
		
	}




	/*
	Description: 	Use to set user new password.
	*/
	function updateUserLoginInfo($UserID, $Input=array(), $SourceID, $FROM=""){
		$UpdateArray = array_filter(array(
			"Password" 			=>	(!empty($Input['Password']) ? md5($Input['Password']) : '') ,
			"ModifiedDate" 	=>	(!empty($Input['Password']) ? date("Y-m-d H:i:s") : '') ,
			"LastLoginDate" 	=>	@$Input['LastLoginDate'],
			"SourceID" =>	$SourceID
		));

		/* Update User Login details */
		$this->db->where('UserID', $UserID);
		if(empty($FROM)){
			$this->db->where('SourceID', $SourceID);
		}else{
			$this->db->where('SourceID != 6');
		}		
		$this->db->limit(1);
		$this->db->update('tbl_users_login', $UpdateArray);
		//echo $this->db->last_query();die;

		if(!empty($Input['Password'])){
			/* Send Password Assistance Email to User with Token (If user is not Pending or Email-Confirmed then email send without Token). */
			$UserData = $this->Users_model->getUsers('FirstName,Username,Email',array('UserID'=>$UserID));
			
			$content = $this->load->view('emailer/change_password',array("Name" => $UserData['FirstName']),TRUE);
			$SendMail = sendMail(array(
				'emailTo' 		=> $UserData['Email'],			
				'emailSubject'	=> SITE_NAME . " Password Assistance",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));
		}

		if(!empty($FROM)){
			return $UserData;
		}else{
			return TRUE; 
		}
		
	}


	function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    return $ipaddress;
	}


	/*
	Description: 	ADD user to system.
	Procedures:
	1. Add user to user table and get UserID.
	2. Save login info to users_login table.
	3. Save User details to users_profile table.
	4. Genrate a Token for Email verification and save to tokens table.
	5. Send welcome Email to User with Token.
	*/
	function addUser($Input=array(), $UserTypeID, $SourceID, $StatusID=1)
	{
		//if(!empty($_SERVER['HTTP_CLIENT_IP']))
		//{
		//           $IPAddress = $_SERVER['HTTP_CLIENT_IP'];
		//    }
		//    elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		//    {
		//        $IPAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		//    }
		//    else
		//    {
		//        $IPAddress = $_SERVER['REMOTE_ADDR'];
		//    }


	 	$IPAddress = $this->get_client_ip();	

	 	$IPAddressArr = explode(",", $IPAddress);
	 	
        if(isset($IPAddressArr) && count($IPAddressArr) == 2)
        {
            $IPAddress = $IPAddressArr[0];
        }

		$this->db->trans_start();
		$EntityGUID = get_guid();

	 	$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		/* Add user to entity table and get EntityID. */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		if(!empty($InstituteID)){

				if($UserTypeID == "88")
				{
					$StatusIDVerified = 2;
				}
				else
				{
					$StatusIDVerified = 1;
				}


				$InsertData = array_filter(array(
					"EntityGUID" 		=>	$EntityGUID,
					"EntityTypeID"		=>	1,
					"CreatedByUserID"  => $this->UserID,
					"EntryDate" 		=>	date("Y-m-d H:i:s"),
					"StatusID"			=>	$StatusIDVerified
				));

				$this->db->insert('tbl_entity', $InsertData);
				$EntityID = $this->db->insert_id();
				//echo $this->db->last_query();

				$this->db->where("EntityID",$EntityID);
				$this->db->update('tbl_entity',array("InstituteID"=>$EntityID));
				//echo $this->db->last_query(); die;
		}else{

			/* Add user to entity table and get EntityID. */
				//$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>1, "StatusID"=>$StatusID));

				if($UserTypeID == "88")
				{
					$StatusIDVerified = 2;
				}
				else
				{
					$StatusIDVerified = $StatusID;
				}

				$InsertData = array_filter(array(
					"EntityGUID" 		=>	$EntityGUID,
					"EntityTypeID"		=>	1,
					"EntryDate" 		=>	date("Y-m-d H:i:s"),
					"StatusID"			=>	$StatusIDVerified
				));
				$this->db->insert('tbl_entity', $InsertData);
				$EntityID = $this->db->insert_id();

				$this->db->where("EntityID",$EntityID);
				$this->db->update('tbl_entity',array("InstituteID"=>$EntityID,"CreatedByUserID"=>$CreatedByUserID));


		}
		/* Add user to user table . */
		// if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
		// 	$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
		// 	unset($Input['PhoneNumber']);
		// }
		if(!empty($Input['BirthDate'])){
			$originalDate = $Input['BirthDate'];
			$newDate = date("Y-m-d", strtotime($originalDate));
		}else{
			$newDate = NULL;
		}

		if(!empty($Input['AnniversaryDate'])){
			$AnniversaryDate = date("Y-m-d", strtotime($Input['AnniversaryDate']));
		}else{
			$AnniversaryDate = NULL;
		}
		
		$FirstNameForSMS = ucfirst(@$Input['FirstName']);

		$InsertData = array_filter(array(
			"UserID" 				=> 	$EntityID,
			"UserGUID" 				=> 	$EntityGUID,			
			"UserTypeID" 			=>	$UserTypeID,
			"StoreID" 				=>	@$Input['StoreID'],
			"FirstName" 			=> 	@$Input['FirstName'],
			"MiddleName" 			=> 	@$Input['MiddleName'],			
			"LastName" 				=> 	@$Input['LastName'],
			"About" 				=>	@$Input['About'],
			"ProfilePic" 			=>	@$Input['ProfilePic'],
			"ProfileCoverPic" 		=>	@$Input['ProfileCoverPic'],			
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],
			"BirthDate" 			=>	@$newDate,

			"Address" 				=>	@$Input['Address'],
			"Address1" 				=>	@$Input['Address1'],
			"Postal" 				=>	@$Input['Postal'],	
			"CountryCode" 			=>	@$Input['CountryCode'],

			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"Latitude" 				=>	@$Input['Latitude'],
			"Longitude"				=>	@$Input['Longitude'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"PhoneNumberForChange" 	=>	@$Input['PhoneNumberForChange'],
			"Website" 				=>	@strtolower($Input['Website']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),
			"TwitterURL" 			=>	@strtolower($Input['TwitterURL']),
			"ReferredByUserID" 		=>	@$Input['Referral']->UserID,
			"LivingStatus" 			=>	@$Input['LivingStatus'],
			"AnniversaryDate" 		=>	@$AnniversaryDate, 
			"AdminAccess" 		=>	@$Input['AdminAccess'],
			"IPAddress" 		=>	$IPAddress,
			"Resource"   =>	@$Input['Resource']
			 
		));

		
		$this->db->insert('tbl_users', $InsertData);
		//echo $this->db->last_query(); die;

		if($Input['UserTypeName'] == 'Staff'){

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$EntityID,			
				"Nominee" 			=>	@$Input['Nominee'],
				"RelationWithNominee" 			=>	@$Input['RelationWithNominee'],
				"NomineePhoneNumber" 			=>	@$Input['NomineePhoneNumber'],
				"NomineeGender" 		    =>	@$Input['NomineeGender'],
				"NomineeAddress" 			    =>	@$Input['NomineeAddress'],
				"NomineeState" 		=>	@$Input['NomineeState'],
				"NomineeCityName" 			=>	@$Input['NomineeCityName'],
				"NomineeCityCode" 			=>	@$Input['NomineeCityCode']
			));
			$this->db->insert('tbl_user_nominee', $InsertDatad);

			$count = count($Input['Qualification']);

			for($key=0; $key<$count; $key++) {
			 $InsertDatas[$key] = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"Qualification" 		    =>	@$Input['Qualification'][$key],
				"University" 			=>	@$Input['University'][$key],
				"Specialization" 		=>	@$Input['Specialization'][$key],
				"PassingYear"  =>	@$Input['PassingYear'][$key],
				"Percentage" 			=>	@$Input['Percentage'][$key],
			 ));
			 $this->db->insert('tbl_user_eduction', $InsertDatas[$key]);
			//echo $this->db->last_query();
		    }


		    if(!empty($Input['JoiningDate'])){
				$JoiningDate = date("Y-m-d", strtotime($Input['JoiningDate']));
			}else{
				$JoiningDate = NULL;
			}

			if(!empty($Input['AppraisalDate'])){
				$AppraisalDate = date("Y-m-d", strtotime($Input['AppraisalDate']));
			}else{
				$AppraisalDate = NULL;
			}

			if(!empty($Input['AppraisalDate'])){
				$AppraisalDate = date("Y-m-d", strtotime($Input['AppraisalDate']));
			}else{
				$AppraisalDate = NULL;
			}

			if(!empty($Input['CourseID'])){
				$CourseID = implode(",",$Input['CourseID']);
			}else{
				$CourseID = NULL;
			}

			if(!empty($Input['SubjectID'])){
				$SubjectID = implode(",",$Input['SubjectID']);
			}else{
				$SubjectID = NULL;
			}

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"Designation" 			=>	@$Input['JobDesignation'],
				"ReportingManager" 		    =>	@$Input['ReportingManager'],
				"Department" 			    =>	@$Input['Department'],
				"ReportingManager" 		=>	@$Input['ReportingManager'],
				"Job" 		=>	@$Input['Job'],
				"JoiningDate" 			=>	$JoiningDate,
				"AppraisalDate" 		=>	$AppraisalDate,
				"CourseID" 		        =>	$CourseID,
				"SubjectID" 		    =>	$SubjectID
			));

			$this->db->insert('tbl_user_jobs', $InsertDatad);

			$counts = count($Input['Designation']);

			


			for($keys=0; $keys<$counts; $keys++) {
				if(!empty($Input['From'])){
					$From = date("Y-m-d", strtotime($Input['From'][$keys]));
				}else{
					$From = NULL;
				}

				if(!empty($Input['To'])){
					$To = date("Y-m-d", strtotime($Input['To'][$keys]));
				}else{
					$To = NULL;
				}
			$InsertDatae = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"Employer" 			    =>	@$Input['Employer'][$keys],
				"ExpDesignation" 	=>	@$Input['ExpDesignation'][$keys],
				"JobProfile" 	 =>	@$Input['JobProfile'][$keys],
				"From" 	=>	$From,
				"To" =>	$To));
			$this->db->insert('tbl_user_experience', $InsertDatae);
		   }
		}
		//echo $this->db->last_query(); die();
		
		if(empty($Input['Password'])){
			$Input['Password'] = $this->random_strings(6);
		}

		/* Save login info to users_login table. */
		$InsertDataa = array_filter(array(
			"UserID" 		=> $EntityID,
			"Password"		=> md5(($SourceID=='1' ? $Input['Password'] : $Input['SourceGUID'])),
			"SourceID"		=> $SourceID,
			"EntryDate"		=> date("Y-m-d H:i:s")));

		$this->db->insert('tbl_users_login', $InsertDataa);

		/*save user settings*/
		$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));

		

		$smsurl = $linkForSMS = "";
		/*for update email address*/
		if(!empty($Input['Email']))
		{	
			$this->load->model('Recovery_model');
			
			$Tokens = $this->Recovery_model->generateToken($EntityID, 1);
			
			$Token = $this->Recovery_model->generateToken($EntityID, 2);
			
			$linkForSMS = MP_BASE_URL."verify_mobile.php?otp=".$Token."&passotp=".$Tokens;	


			$content = $this->load->view('emailer/adduser',array("Name" =>  $Input['FirstName'], 'Password' => $Input['Password'], "Email"=>$Input['Email'], 'Token' => @$Token, "Tokens"=>@$Tokens),TRUE);

			if($UserTypeID != "88" /*&& $UserTypeID!=10*/ && $UserTypeID!=87)
			{
				sendMail(array(
					'emailTo' 		=> $Input['Email'],	
					'emailSubject'	=> "Thank you for registering at Iconik.",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));
			}	


			if(!empty($UserTypeID) && $UserTypeID == "88")	
			{
				$CC_email = "piyushmedia22@gmail.com";
			}else{
				$CC_email = "";
			}

			$EmailText = "New Registration has happened, details are as follows :";

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Name : ".$Input['FirstName'];

			$EmailText .= "<br>";

			$EmailText .= "Email : ".$Input['Email'];

			$EmailText .= "<br>";

			$EmailText .= "Contact Number : ".$Input['PhoneNumberForChange'];

			$EmailText .= "<br>";


			if(!empty($Input['UserTypeID']) && $Input['UserTypeID'] == '88'){
				$EmailText .= "User Type : Student";
			}else if(!empty($Input['UserTypeID'])  && ($Input['UserTypeID'] == '10' ||  $Input['UserTypeID'] == '87')){
				$EmailText .= "User Type : Institute";
			}else if(!empty($Input['UserTypeID']) && $Input['UserTypeID'] == '90'){
				$EmailText .= "User Type : Faculty";
			}else if(!empty($Input['UserTypeID']) && $Input['UserTypeID'] == '82'){
				$EmailText .= "User Type : Job Provider";
			}else if(!empty($Input['UserTypeID']) && $Input['UserTypeID'] == '83'){
				$EmailText .= "User Type : Advertiser";
			}

			$EmailText .= "<br>";

			$EmailText .= "IP Address : $IPAddress";

			$EmailText .= "<br>";

			if(!empty($Input['Resource']) && $Input['Resource'] == 'xbmNFB'){
				$EmailText .= "Resource : Facebook";
			}else if(!empty($Input['Resource'])  && $Input['Resource'] == '2QBoTW'){
				$EmailText .= "Resource : Twitter";
			}else if(!empty($Input['Resource']) && $Input['Resource'] == 'N1pfIN'){
				$EmailText .= "Resource : Instagram";
			}else if(!empty($Input['Resource']) && $Input['Resource'] == 'lpmcLN'){
				$EmailText .= "Resource : LinkedIn";
			}else{
				$EmailText .= "Resource : Direct";
			}
			
			$EmailText .= "<br>";

			$content = $this->load->view('emailer/common', array("EmailText" =>  $EmailText),TRUE);
			sendMail(array(
				'emailTo' 		=> "contact@iconik.in, cse@iconik.in",	
				'CC_email' 		=> $CC_email,			
				'emailSubject'	=> "New Registration",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));
		}


		//Send SMS to MP students with Token.
		/*if(!empty($Input['PhoneNumberForChange']))
		{
			$this->load->model('Recovery_model');
			$Token = $this->Recovery_model->generateToken($EntityID, 3);

			
			sendSMS(array(
			'PhoneNumber' 	=> $Input['PhoneNumberForChange'],			
			'Message'		=> "Thank you registering with ".ucfirst(SITE_NAME).". Use OTP ".$Token." to verify your mobile number. Valid 30 minutes."
			));			
		}*/




		/*echo "PhoneNumber=".$UserData['PhoneNumber'];
		echo "..............PhoneNumber=".$UpdateArray['PhoneNumber'];
		die;*/

		/*for update phone number*/
		//if(!empty($Input['PhoneNumberForChange']) && PHONE_NO_VERIFICATION && !isset($Input['SkipPhoneNoVerification']))
		if(!empty($Input['PhoneNumberForChange']) && PHONE_NO_VERIFICATION)
		{
			//if($Input['PhoneNumberForChange']!=$UpdateArray['PhoneNumber'])
			//{
				//echo "Innn.";
				//$UpdateArray['PhoneNumberForChange'] = $UpdateArray['PhoneNumber'];			
				/* Genrate a Token for PhoneNumber verification and save to tokens table. */
				$this->load->model('Recovery_model');
				$Token = $this->Recovery_model->generateToken($EntityID, 3);

				/* Send change phonenumber SMS to User with Token. */
				if($UserTypeID == "88")
				{					
					sendSMS(array(
						'PhoneNumber' 	=> $Input['PhoneNumberForChange'],			
					 	'Message'			=> $Token." is the OTP for your mobile verification. You can also click this link to complete the verification ".$linkForSMS." OTP valid 30 min."

					));
				}	


				//unset($UpdateArray['PhoneNumber']);
			//}
		}

		//echo  $EntityID; die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return $EntityID;
	}




	/*
	Description: 	Use to get single user info or list of users.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getUsers($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
			
		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'AdminAccess'           =>  'U.AdminAccess',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'MasterFranchisee'				=>	'U.MasterFranchisee',
				'ParentUserID'	=>	'U.ParentUserID',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'InstituteProfilePic'		=>	'IF(INST.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",INST.ProfilePic)) AS InstituteProfilePic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'ls.StateName',
				'State_id'				=>	'ls.State_id',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	=>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	=>	'U.CityCode',
				'UrlType'	=>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
				'CreatedByUserID' =>'E.CreatedByUserID',
				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			=>	'E.StatusID',
				'PanStatusID'				=>	'U.PanStatus',
				'BankStatusID'				=>	'U.BankStatus',

				'OwnerName'				=>	'U.OwnerName',
				'Achivements'				=>	'U.Achivements',
				'ScholarshipTest'				=>	'U.ScholarshipTest',
				'Classrooms'				=>	'U.Classrooms',
				'Library'				=>	'U.Library',
				'TeachingAids'				=>	'U.TeachingAids',
				'HostelFacility'				=>	'U.HostelFacility',
				'NumberOfRooms'				=>	'U.NumberOfRooms',
				'StudentPerRooms'				=>	'U.StudentPerRooms',
				'MessFacility'				=>	'U.MessFacility',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.FirstName,U.LastName,U.Email,U.EmailForChange,U.ProfilePic,U.PhoneNumber,U.PhoneNumberForChange,U.CityName,U.CountryCode,U.StateName as State,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.BirthDate,U.TelePhoneNumber,U.UrlType,U.MasterFranchisee,U.AdminAccess,E.InstituteID,ls.StateName,ls.State_id,CO.CountryName, DATE_FORMAT(E.EntryDate, "%d-%m-%Y %H:%i:%s") as EntryDate');
		

		$this->db->select($Field,false);

		if(!empty($Where['SourceID']) && $Where['SourceID'] == 6)
		{
			$words3 = substr($Where['Password'], 0, 3);
			if($words3 == "ST#")
			{				
				$this->db->select('0 as CourseID,App.KeyName as Key,"As per expiry date" as Validity,DATE(App.PaymentDate) as ActivatedOn,DATE(App.ValidUpto) as ExpiredOn,2 as KeyStatusID');
			}
			else
			{
				$this->db->select('S.CourseID,S.Key,S.Validity,S.ActivatedOn,S.ExpiredOn,S.KeyStatusID');
			}	
		}
		
		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}	
			
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		if(!empty($Where['StudentID'])){
			$this->db->from('tbl_students S');
		}
		
		
		$this->db->where("U.UserID","E.EntityID", FALSE);	

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
		}
		
		$this->db->join('tbl_users INST', 'E.InstituteID = INST.UserID', 'left');
		$this->db->join('set_location_states ls', 'ls.State_id = U.StateID', 'left');

		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		
		if(!empty($Where['SourceID']) && $Where['SourceID'] == 6)
		{
			$words3 = substr($Where['Password'], 0, 3);
			if($words3 == "ST#")
			{
				$this->db->join('tbl_scholarship_applicants App', 'U.UserID = App.StudentID');
			}
			else
			{
				$this->db->join('tbl_students S', 'U.UserID = S.StudentID','left');
			}	
		}
		

		//if(array_keys_exist($Params, array('CountryName'))) {
			$this->db->join('set_location_country CO', 'U.CountryCode = CO.CountryCode', 'left');
		//}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			if(validateEmail($Where['Keyword'])){
				$Where['Email'] = $Where['Keyword'];
			}
			elseif(is_numeric($Where['Keyword'])){
				$Where['PhoneNumber'] = $Where['Keyword'];
			}else{
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.LastName", $Where['Keyword']);
				$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
				$this->db->group_end();				
			}

		}

		if(!empty($Where['SourceID'])){
			$this->db->where("UL.SourceID",$Where['SourceID']);			
		}
		

		if(!empty($Where['UserTypeID'])){
			$this->db->where_in("U.UserTypeID",$Where['UserTypeID']);
		}
		
		if(!empty($Where['UserTypeIDNot']) && $Where['UserTypeIDNot']=='Yes'){
			$this->db->where("U.UserTypeID!=",$Where['UserTypeIDNot']);
		}

		if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}
		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}
		// error_reporting(1);
		// echo $this->db->last_query();die;
		ini_set('display_errors', 1);

		
		if(!empty($Where['LoginKeyword'])){
			$this->db->group_start();
			$this->db->where("U.Email",$Where['LoginKeyword']);
			$this->db->or_where("U.Username",$Where['LoginKeyword']);
			$this->db->or_where("U.PhoneNumber",$Where['LoginKeyword']);
			$this->db->group_end();
		}
		
		if(!empty($Where['Password'])){
			$this->db->where("UL.Password",md5($Where['Password']));
		}
		

		if(!empty($Where['IsAdmin'])){
			$this->db->where("UT.IsAdmin",$Where['IsAdmin']);
		}
		if(!empty($Where['StatusID'])){
			$this->db->where("E.StatusID",$Where['StatusID']);
		}
		if(!empty($Where['PanStatus'])){
			$this->db->where("U.PanStatus",$Where['PanStatus']);
		}
		if(!empty($Where['BankStatus'])){
			$this->db->where("U.BankStatus",$Where['BankStatus']);
		}
		

		if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
			$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		}else{
			$this->db->order_by('U.FirstName','ASC');			
		}


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		// echo $this->db->last_query(); die();

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				if(!empty($Record['UserTypeID']) && $Record['UserTypeID'] != 7){
					$this->db->select("*");
					$this->db->from("tbl_staff_assign_keys");
					$this->db->where("StaffGUID",$Record['UserGUID']);
					$this->db->limit(1);
					$que = $this->db->get();
					if($que->num_rows() > 0){
						$staffdata = $que->result_array();
						$Record['ActivatedOn'] = $staffdata[0]['ActivatedOn'];
						$Record['Key'] = $staffdata[0]['Key'];
						$Record['Validity'] = $staffdata[0]['Validity'];
						$Record['ExpiredOn'] = $staffdata[0]['ExpiredOn'];
						$Record['KeyStatusID'] = $staffdata[0]['KeyStatusID'];
					}
				}else if(empty($Record['CourseID'])){
					$this->db->select("*");
					$this->db->from("tbl_students");
					$this->db->where("StudentGUID",$Record['UserGUID']);
					$this->db->limit(1);
					$que = $this->db->get();
					if($que->num_rows() > 0){
						$staffdata = $que->result_array();
						$Record['ActivatedOn'] = $staffdata[0]['ActivatedOn'];
						$Record['Key'] = $staffdata[0]['Key'];
						$Record['Validity'] = $staffdata[0]['Validity'];
						$Record['ExpiredOn'] = $staffdata[0]['ExpiredOn'];
						$Record['KeyStatusID'] = $staffdata[0]['KeyStatusID'];
						$Record['CourseID'] = $staffdata[0]['CourseID'];
					}
				}

				if(!empty($Record['CourseID'])){
					$this->load->model('Category_model');
					$CategoryName = $this->getCourseName($Record['CourseID']);
					$Record['CourseName'] = $CategoryName;
					$CategoryGUID = $this->getCourseGUID($Record['CourseID']);
					$Record['CategoryGUID'] = $CategoryGUID;
				}else{
					$Record['CourseName'] = "";
					$Record['CategoryGUID'] = "";
				}

				if(!empty($Record['InstituteID'])){
					$Record['InstituteName'] = $this->getUserName($Record['InstituteID']);
				}

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}
	
	/*
	Description: 	Use to create session.
	*/
	function createSession($UserID, $Input=array())
	{
		/* Multisession handling */
		if (!MULTISESSION)
		{ 
			$this->db->delete('tbl_users_session', array('UserID' => $UserID));
		}else{
/*			if(empty(@$Input['DeviceGUID'])){
				$this->db->delete('tbl_users_session', array('DeviceGUID' => $Input['DeviceGUID']));
			}*/
		}

		/* Multisession handling - ends */
		$InsertData = array_filter(array(
			'UserID' 		=> $UserID,
			'SessionKey' 	=> get_guid(),
			'IPAddress' 	=> @$Input['IPAddress'],
			'SourceID'		=> (!empty($Input['SourceID']) ? $Input['SourceID'] : DEFAULT_SOURCE_ID),
			'DeviceTypeID' 	=> (!empty($Input['DeviceTypeID']) ? $Input['DeviceTypeID'] : DEFAULT_DEVICE_TYPE_ID),
			'DeviceGUID' 	=> @$Input['DeviceGUID'],
			'DeviceToken' 	=> @$Input['DeviceToken'],
			'EntryDate' 	=> date("Y-m-d H:i:s"),
		));

		$this->db->insert('tbl_users_session', $InsertData);

		$this->db->select('tbl_users_login.LastLoginDate');
		$this->db->from('tbl_users');
		$this->db->join('tbl_users_login','tbl_users.UserID = tbl_users_login.UserID');
		$this->db->where('tbl_users.UserTypeID',10);
		$this->db->where('tbl_users_login.LastLoginDate IS NULL');
		$this->db->limit(1);
		$lastLoginQuery = $this->db->get();

		if($lastLoginQuery->num_rows() > 0){
			$arr = array("UserID"=>$UserID,"TotalSMS"=>200,"AvailableSMS"=>200,"UsedSMS"=>0);
			$this->db->insert("tbl_bulk_sms_credits",$arr);
		}



		/*update current date of login*/
		$this->updateUserLoginInfo($UserID, array("LastLoginDate"=>date("Y-m-d H:i:s")), $InsertData['SourceID']);
		/*Update Latitude, Longitude */
		if(!empty($Input['Latitude']) && !empty($Input['Longitude'])){
			$this->updateUserInfo($UserID, array("Latitude"=>$Input['Latitude'], "Longitude"=>$Input['Longitude']));	
		}
		return $InsertData['SessionKey'];
	}



	/*
	Description: 	Use to get UserID by SessionKey and validate SessionKey.
	*/
	function checkSession($SessionKey,$UserID=""){
		$this->db->select('UserID');
		$this->db->from('tbl_users_session');
		if(!empty($SessionKey)){
			$this->db->where("SessionKey",$SessionKey);
		}
		if(!empty($UserID)){
			$this->db->where("UserID",$UserID);
		}
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			return $Query->row()->UserID;
		}
		return FALSE;
	}

	/*
	Description: 	Use to delete Session.
	*/
	function deleteSession($SessionKey){
		$this->db->limit(1);
		$this->db->delete('tbl_users_session', array('SessionKey' => $SessionKey));
		return TRUE;
	}
	
	/*
	Description: 	Use to set new email address of user.
	*/
	function updateEmail($UserID, $Email){
		/*check new email address is not in use*/
		$UserData=$this->Users_model->getUsers('', array('Email'=>$Email,));
		if(!$UserData){
			$this->db->trans_start();
			/*update profile table*/
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', array("Email" => $Email, "EmailForChange" => null));

			/* Delete session */
			$this->db->limit(1);
			$this->db->delete('tbl_users_session', array('UserID' => $UserID));
			/* Delete session - ends */
			$this->db->trans_complete();	        
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	
		return TRUE;
	}



	/*
	Description: 	Use to set new email address of user.
	*/
	function updatePhoneNumber($UserID, $PhoneNumber){
		/*check new PhoneNumber is not in use*/
		$UserData=$this->Users_model->getUsers('StatusID, UserTypeID', array('PhoneNumber'=>$PhoneNumber));
		if(!$UserData){
			$this->db->trans_start();
			/*update profile table*/
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', array("PhoneNumber" => $PhoneNumber, "PhoneNumberForChange" => null));

			/* change entity status to activate */
			/*edited by Gautam: not allow institute user to verify account*/
			if($UserData['UserTypeID']!=10){
				$this->Entity_model->updateEntityInfo($UserID, array("StatusID"=>2));					
			}

			$this->db->trans_complete();	        
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	
		return TRUE;
	}

	/*user type get*/
	function getusertype($EntityID)
	{
	    $this->db->select('*');
		$this->db->from('tbl_users_type');
		$this->db->where("EntityID",$EntityID);
		$Query = $this->db->get();	
		
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;
	}

	/*user type get*/
	function getusertypes($EntityID)
	{
	    $this->db->select('UserTypeID');
		$this->db->from('tbl_users');
		$this->db->where("UserID",$EntityID);
		$Query = $this->db->get();	
		
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;
	}


	/*user type name*/
	function getUserName($UserID)
	{
	    $this->db->select('FirstName,MiddleName,LastName');
		$this->db->from('tbl_users');
		$this->db->where("UserID",$UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$UserData=$Query->result_array();

		if(!empty($UserData))
		{
			$fullname = $UserData[0]['FirstName'];
			if(isset($UserData[0]['MiddleName']) && !empty($UserData[0]['MiddleName']))
			{
				$fullname = $fullname." ".$UserData[0]['MiddleName']; 
			}

			if(isset($UserData[0]['LastName']) && !empty($UserData[0]['LastName']))
			{
				$fullname = $fullname." ".$UserData[0]['LastName']; 
			}

			return $fullname;			

		}else{

			return FALSE;
		}
	}
	


	/*get user course*/
	function getCourseName($CourseID)
	{
	    $this->db->select('CategoryName');
		$this->db->from('set_categories');
		$this->db->where("CategoryID",$CourseID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		$UserData = $Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['CategoryName'];

		}else{

			return FALSE;
		}
	}


	/*get user course*/
	function getCourseGUID($CourseID)
	{
	    $this->db->select('CategoryGUID');
		$this->db->from('set_categories');
		$this->db->where("CategoryID",$CourseID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		$UserData = $Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['CategoryGUID'];

		}else{

			return FALSE;
		}
	}


	/*create by institutrID*/
	function createbyinstitutrID($UserID)
	{
	    $this->db->select('EntityGUID');
		$this->db->from('tbl_entity');
		$this->db->where("CreatedByUserID = (select CreatedByUserID from tbl_entity where EntityID = ".$UserID.")");
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$EntityGUID=$Query->result_array();

		if(!empty($EntityGUID)){

			return $EntityGUID[0]['EntityGUID'];

		}else{

			$this->db->select('EntityGUID');
			$this->db->from('tbl_entity');
			$this->db->where("EntityID", $UserID);
			$this->db->limit(1);
			$Query = $this->db->get();	
			//echo $this->db->last_query();
			$EntityGUID=$Query->result_array();

			return $EntityGUID[0]['EntityGUID'];

		}
	
	}


	/**/
	function getMasterFranchisee($UserID)
	{
		$this->db->select('MasterFranchisee');
		$this->db->from('tbl_users');
		$this->db->where("UserID", $UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$UserData=$Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['MasterFranchisee'];

		}else{

			return FALSE;
		}
	}


	function getFranchiseeAssignCount($UserID)
	{
		$this->db->select('FranchiseeAssignCount');
		$this->db->from('tbl_users');
		$this->db->where("UserID", $UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$UserData=$Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['FranchiseeAssignCount'];

		}else{

			return FALSE;
		}
	}
 


	function applyForLoan($EntityID = 0, $Input=array())
	{		
		if(!empty($EntityID) && $EntityID > 0)
		{
			$this->db->select('LoanID');
			$this->db->from('tbl_apply_for_loan');
			$this->db->where('StudentID', $EntityID);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'Already Applied';
				die;
			}
		}

		$this->db->trans_start();

		$InsertData = array_filter(array(
		"StudentID" => $EntityID,			
		"LoanPurpose" => @$Input['LoanPurpose'],
		"LoanAmount" => @$Input['LoanAmount'],
		"LoanFamilyIncome" =>	@$Input['LoanFamilyIncome'],
		"LoanRequestDate" =>	date('Y-m-d H:i:s')
		));

		$this->db->insert('tbl_apply_for_loan', $InsertData);
		
		$LoanID = $this->db->insert_id();		
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}else{

			$IPAddress = $this->get_client_ip();	

		 	$IPAddressArr = explode(",", $IPAddress);
		 	
	        if(isset($IPAddressArr) && count($IPAddressArr) == 2)
	        {
	            $IPAddress = $IPAddressArr[0];
	        }

			$StudentData = $this->Common_model->getInstituteDetailByID($EntityID,"Email,UserID,FirstName,LastName, PhoneNumber, PhoneNumberForChange");

			$EmailText = "Your loan application has been sent successfully, details are as follows :";
			

			$EmailText .= "Loan Purpose : ".$Input['LoanPurpose'];

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Loan Amount : ".$Input['LoanAmount'];

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Family Income : ".$Input['LoanFamilyIncome'];

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Applied Date : ".date('d-m-Y H:i:s');

			$EmailText .= "<br>";

            $SendMail = sendMail(array(
                'emailTo'         => $StudentData['Email'],
                'emailSubject'    => "New loan application",
                'emailMessage'    =>  $this->load->view('emailer/layout',array("HTML" =>  $EmailText),TRUE)
            ));

			$EmailText = "New loan application has generated, details are as follows :";

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Name : ".$StudentData['FirstName']." ".$StudentData['LastName'];

			$EmailText .= "<br>";

			$EmailText .= "Email : ".$StudentData['Email'];

			$EmailText .= "<br>";

			if(!empty($StudentData['PhoneNumber'])){
				$EmailText .= "Contact Number : ".$StudentData['PhoneNumber'];
			}else{
				$EmailText .= "Contact Number : ".$StudentData['PhoneNumberForChange'];
			}	

			$EmailText .= "<br>";

			$EmailText .= "IP Address : $IPAddress";

			$EmailText .= "<br>";

			$EmailText .= "Loan Purpose : ".$Input['LoanPurpose'];

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Loan Amount : ".$Input['LoanAmount'];

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Family Income : ".$Input['LoanFamilyIncome'];

			$EmailText .= "<br>";

			$EmailText .= "<br>";

			$EmailText .= "Applied Date : ".date('d-m-Y H:i:s');

			$EmailText .= "<br>";

            $SendMail = sendMail(array(
                'emailTo'         => "contact@iconik.in",
                'emailSubject'    => "New loan application",
                'emailMessage'    =>  $this->load->view('emailer/layout',array("HTML" =>  $EmailText),TRUE)
            ));
		}

		return $LoanID;
		
	}


	function checkLoanStatus($Email = 0)
	{
		//print_r($Email); die;		
		if(!empty($Email))
		{
			$this->db->select('tbl_apply_for_loan.LoanID');
			$this->db->from('tbl_apply_for_loan');
			$this->db->join('tbl_users','tbl_users.UserID = tbl_apply_for_loan.StudentID');
			$this->db->where('tbl_users.Email', $Email);
			$this->db->limit(1);
			$query = $this->db->get(); 
			if($query->num_rows() > 0)
			{
				return 'Already Applied';
				die;
			}else{
				$this->db->select('UserID');
				$this->db->from('tbl_users');
				$this->db->where('Email', $Email);
				$this->db->limit(1);
				$query = $this->db->get(); //echo $this->db->last_query(); die;
				if($query->num_rows() > 0)
				{
					return 'Already Registered';
					die;
				}else{
					return 'Not available';
					die;
				}
			}
		}else{
			return false;
		}
	}


	function checkUserExist($Input)
	{
		$Username = $Input['Username'];
		$Password = md5($Input['Password']);
		$Source = 1;

		$sql = "SELECT u.UserID, u.Email, u.FirstName 
		FROM tbl_users u 
		INNER JOIN tbl_users_login ul ON u.UserID = ul.UserID AND ul.Password = '$Password' AND ul.SourceID = '$Source'
		WHERE u.Email = '$Username' AND ul.Password = '$Password' AND ul.SourceID = '$Source'
		LIMIT 1	";

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows() > 0)
		{
			$result = $Query->result_array();
			$UserID = $result[0]['UserID'];

			$Records["Basic"]['FirstName'] = ucfirst($result[0]['FirstName']);
			$Records["Basic"]['Email'] = $result[0]['Email'];

			$sql = "SELECT Type, Token
			FROM tbl_tokens			
			WHERE UserID = '$UserID' AND StatusID = 1 AND Type IN (1,2)
			LIMIT 2	";
			$Query = $this->db->query($sql);			
			if($Query->num_rows() > 0)
			{
				foreach($Query->result_array() as $arr)
				{
					$Records["Token"][$arr['Type']] = $arr['Token'];
				}
			}
		}

		return $Records;

	}



	function addBAUser($Input=array(), $UserTypeID, $SourceID, $StatusID=1)
	{		

	 	$IPAddress = $this->get_client_ip();	

	 	$IPAddressArr = explode(",", $IPAddress);
	 	
        if(isset($IPAddressArr) && count($IPAddressArr) == 2)
        {
            $IPAddress = $IPAddressArr[0];
        }

		$this->db->trans_start();
		$EntityGUID = get_guid();

	 	$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		/* Add user to entity table and get EntityID. */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=0;//$this->Common_model->getInstituteByEntity($this->EntityID);

				

		$InsertData = array_filter(array(
			"EntityGUID" 		=>	$EntityGUID,
			"EntityTypeID"		=>	1,
			"EntryDate" 		=>	date("Y-m-d H:i:s"),
			"StatusID"			=>	$StatusID
		));
		$this->db->insert('tbl_entity', $InsertData);
		$EntityID = $this->db->insert_id();

		$this->db->where("EntityID",$EntityID);
		$this->db->update('tbl_entity',array("InstituteID"=>$EntityID,"CreatedByUserID"=>$CreatedByUserID));

		
		$InsertData = array_filter(array(
			"UserID" 				=> 	$EntityID,
			"UserGUID" 				=> 	$EntityGUID,			
			"UserTypeID" 			=>	$UserTypeID,
			"StoreID" 				=>	@$Input['StoreID'],
			"FirstName" 			=> 	@$Input['FirstName'],				
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],			
			

			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"Latitude" 				=>	@$Input['Latitude'],
			"Longitude"				=>	@$Input['Longitude'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"PhoneNumberForChange" 	=>	@$Input['PhoneNumberForChange'],

			"BusinessName" 	=>	@$Input['BusinessName'],
			"BankName" 	=>	@$Input['BankName'],
			"AccountNumber" 	=>	@$Input['AccountNumber'],
			"AccountName" 	=>	@$Input['AccountName'],
			"IFSCCode" 	=>	@$Input['AccountIFSCCode'],

			"FacebookURL" 			=>	@$Input['FacebookURL'],	
			"TwitterURL" 			=>	@$Input['TwitterURL'],
			"GoogleURL" 			=>	@$Input['GoogleURL'],			
			"LinkedInURL" 			=>	@$Input['LinkedInURL'],
			"TelePhoneNumber"	=>	@$Input['TelePhoneNumber'],
			"Address" 				=>	@$Input['Address'],	
			"CountryCode" 			=>	@$Input['CountryCode'],
			"StateID" 			=>	@$Input['StateCode'],
			"CityCode"			=>	@$Input['CityName'],
			"Postal" 			=>	@$Input['ZipCode'],
			"UrlType" 			=>	@$Input['UrlType'],
			"Website" 			=>	@$Input['Website'],
			"GSTNumber" 			=>	@$Input['GSTNumber'],
			
			"AdminAccess" 		=>	@$Input['AdminAccess'],
			"IPAddress" 		=>	$IPAddress,
			"Resource"   =>	@$Input['Resource']
			 
		));		
		$this->db->insert('tbl_users', $InsertData);	

		
		$Input['Password'] = $this->random_strings(6);		

		/* Save login info to users_login table. */
		$InsertDataa = array_filter(array(
			"UserID" 		=> $EntityID,
			"Password"		=> md5($Input['Password']),
			"SourceID"		=> $SourceID,
			"EntryDate"		=> date("Y-m-d H:i:s")));
		$this->db->insert('tbl_users_login', $InsertDataa);


		/*save user settings*/
		$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));

		

			/*for update email address*/
		if(!empty($Input['Email']))
		{	
			$this->load->model('Recovery_model');
			
			$Tokens = $this->Recovery_model->generateToken($EntityID, 1);
			
			$Token = $this->Recovery_model->generateToken($EntityID, 2);
			
			$content = $this->load->view('emailer/addbauser',array("Name" =>  $Input['FirstName'], 'Password' => $Input['Password'], "Email"=>$Input['Email'], 'Token' => @$Token, "Tokens"=>@$Tokens),TRUE);

			sendMail(array(
				'emailTo' 		=> $Input['Email'],	
				'emailSubject'	=> "Thank you for registering at Iconik.",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));


			$CC_email = "";			

			$EmailText = "New Business Associates Registration has happened, details are as follows :";
			
			
			$EmailText .= "<br><br>Business Name : ".$Input['BusinessName'];
			$EmailText .= "<br>GST Number : ".$Input['GSTNumber'];			
			$EmailText .= "<br>Name : ".$Input['FirstName'];
			$EmailText .= "<br>Email : ".$Input['Email'];
			$EmailText .= "<br>Mobile Number : ".$Input['PhoneNumberForChange'];
			$EmailText .= "<br>Address : ".$Input['Address'];


			$EmailText .= "<br><br>Bank Details : ";
			$EmailText .= "<br>Bank Name : ".$Input['BankName'];
			$EmailText .= "<br>Account Number : ".$Input['AccountNumber'];
			$EmailText .= "<br>Account Name : ".$Input['AccountName'];
			$EmailText .= "<br>IFSC Code : ".$Input['AccountIFSCCode'];
			$EmailText .= "<br>Bank Address : ".$Input['BankAddress'];

			$EmailText .= "<br>";		

			$EmailText .= "IP Address : $IPAddress";

			$EmailText .= "<br>";

			$content = $this->load->view('emailer/common', array("EmailText" =>  $EmailText),TRUE);
			sendMail(array(
				'emailTo' 		=> "developer@iconik.in, ceo@iconik.in, operations@iconik.in",
				'CC_email' 		=> $CC_email,			
				'emailSubject'	=> "New Business Associates Registration",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));
			//"chairman@iconik.in, ceo@iconik.in, operations@iconik.in",
		}

		
		if(!empty($Input['PhoneNumberForChange']) && PHONE_NO_VERIFICATION)
		{			
			$this->load->model('Recovery_model');
		
			$Token = $this->Recovery_model->generateToken($EntityID, 3);				
		}

		//echo  $EntityID; die;

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return $EntityID;
	}


	function addStudentDirect($Input = array())
	{	
		$UserTypeID = 7;
		$StatusID = 1;
		$CategoryID = $Input['Course'];		
		$BatchID = $Input['Batch'];

		$InstituteID = $EntityID = $Input['instID'];

		$this->db->trans_start();

		$UPhoneNumber = trim($Input['PhoneNumber']);
		if(!empty($UPhoneNumber))
		{
			$this->db->select('PhoneNumber');
			$this->db->from('tbl_users');
			$this->db->where('PhoneNumber = "'.$UPhoneNumber.'" OR PhoneNumberForChange = "'.$UPhoneNumber.'"');
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'PhoneNumber Exist';
				die;
			}
		}

		$UEmail = trim($Input['Email']);
		if(!empty($UEmail))
		{
			$this->db->select('Email');
			$this->db->from('tbl_users');
			$this->db->where('Email = "'.$UEmail.'" OR EmailForChange = "'.$UEmail.'"');
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'Email Exist';
				die;
			}
		}

			
		$EntityGUID = get_guid();	
		
		
		$InsertData = array_filter(array(
		"EntityGUID" 		=>	$EntityGUID,
		"EntityTypeID"		=>	15,
		"CreatedByUserID" 	=>	$EntityID,		
		"EntryDate" 		=>	date("Y-m-d H:i:s"),
		"StatusID"			=>	1
		));
		
		$this->db->insert('tbl_entity', $InsertData);
		$EntityID = $this->db->insert_id();

		$this->db->where('EntityID',$EntityID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		
		if(!empty($Input['StateID']))
		{
			$this->load->model('Common_model');
			$StateName = $this->Common_model->getStatesNameById($Input['StateID']);
		}
		else
		{
			$StateName = "";
		}	

		$FirstName = ucfirst(strtolower($Input['FirstName']));
		$LastName = ucfirst(strtolower($Input['LastName']));
		
		$Address = (($Input['Address']));
		$Postal = (($Input['Postal']));
		$CityName = (($Input['CityName']));
		$PhoneNumber = (($Input['PhoneNumber']));		
		
		$sql = "INSERT INTO tbl_users(UserID, UserGUID, UserTypeID, FirstName, LastName, Email, Address, Postal, StateName, CityName, PhoneNumber) VALUES('$EntityID', '$EntityGUID', 7, '$FirstName', '$LastName', '$UEmail', '$Address', '$Postal', '$StateName', '$CityName', '$PhoneNumber') ";		
		$this->db->query($sql);			
				
		
		$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));				

		$InsertDatas = array_filter(array(
		"StudentID" => 	$EntityID,
		"StudentGUID" => $EntityGUID,			
		"CourseID" =>	$CategoryID,
		"BatchID" =>	$BatchID,						
		"Email" =>	@strtolower($Input['Email']),
		"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),		
		"FathersName"  =>	@$Input['FathersName'],
		"MothersName"  =>	@$Input['MothersName'],
		"PermanentAddress"  =>	@$Input['Address'],
		"FathersPhone"  =>	@$Input['FathersPhone'],
		"FathersEmail"  =>	@$Input['FathersEmail'],
		"Remarks" =>	@$Input['Remarks']));
		
		$this->db->insert('tbl_students', $InsertDatas);
		$StudentsID = $this->db->insert_id();		
				
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}		

		if (!empty($Input['Email'])) 
		{
			if($Input['instID'] == 252)
			{
				$InstName = "Sarvodaya Civil Services";
				$InstEmail = "msarvodayacs@gmail.com";				
			}
			else
			{
				$InstName = "Aim Civil Services";
				$InstEmail = "aimcivilservices.munna.ji@gmail.com";				
			}


			$InstUserID = $InstituteID;			
			
	       	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
		   	
		   	$this->load->model('Recovery_model');
			$Token = $this->Recovery_model->generateToken($EntityID, 2);

			$content = $this->load->view('emailer/signup', array(
			"Name" => $Input['FirstName'].' '.$Input['LastName'],
			'Token' => $Token,
			'DeviceTypeID' => 1,
			"InstituteProfilePic"=>@$MediaURL,
			'Signature' => "Thanks, <br>".$InstName
			) , TRUE);

			
			sendMail(array(
				"From_name"  => $InstName,
				'emailTo' => $Input['Email'],
				'emailSubject' => "Welcome at " . $InstName,
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			return $EntityID;
		}	
		

		return $EntityID;		
	}



	function addStudentByCodeDirect($Input = array())
	{	
		$UserTypeID = 7;
		$StatusID = 1;
		$CategoryID = $Input['Course'];		
		$BatchID = $Input['Batch'];

		$InstituteID = $EntityID = 224;	


		$this->db->trans_start();

		$UPhoneNumber = trim($Input['PhoneNumber']);
		if(!empty($UPhoneNumber))
		{
			$this->db->select('PhoneNumber');
			$this->db->from('tbl_users');
			$this->db->where('PhoneNumber = "'.$UPhoneNumber.'" OR PhoneNumberForChange = "'.$UPhoneNumber.'"');
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'PhoneNumber Exist';
				die;
			}
		}

		$UEmail = trim($Input['Email']);
		if(!empty($UEmail))
		{
			$this->db->select('Email');
			$this->db->from('tbl_users');
			$this->db->where('Email = "'.$UEmail.'" OR EmailForChange = "'.$UEmail.'"');
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'Email Exist';
				die;
			}
		}

			
		$EntityGUID = get_guid();	
		
		
		$InsertData = array_filter(array(
		"EntityGUID" 		=>	$EntityGUID,
		"EntityTypeID"		=>	15,
		"CreatedByUserID" 	=>	$EntityID,		
		"EntryDate" 		=>	date("Y-m-d H:i:s"),
		"StatusID"			=>	1
		));
		
		$this->db->insert('tbl_entity', $InsertData);
		$EntityID = $this->db->insert_id();

		$this->db->where('EntityID',$EntityID);
		$this->db->update('tbl_entity',array('InstituteID' => $InstituteID));

		
		if(!empty($Input['StateID']))
		{
			$this->load->model('Common_model');
			$StateName = $this->Common_model->getStatesNameById($Input['StateID']);
		}
		else
		{
			$StateName = "";
		}	

		$FirstName = ucfirst(strtolower($Input['FirstName']));
		$LastName = ucfirst(strtolower($Input['LastName']));
		
		$Address = (($Input['Address']));
		$Postal = (($Input['Postal']));
		$CityName = (($Input['CityName']));
		$PhoneNumber = (($Input['PhoneNumber']));		
		
		$sql = "INSERT INTO tbl_users(UserID, UserGUID, UserTypeID, FirstName, LastName, Email, Address, Postal, StateName, CityName, PhoneNumber) VALUES('$EntityID', '$EntityGUID', 7, '$FirstName', '$LastName', '$UEmail', '$Address', '$Postal', '$StateName', '$CityName', '$PhoneNumber') ";		
		$this->db->query($sql);			
				
		
		$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));				

		$InsertDatas = array_filter(array(
		"StudentID" => 	$EntityID,
		"StudentGUID" => $EntityGUID,			
		"CourseID" =>	$CategoryID,
		"BatchID" =>	$BatchID,						
		"Email" =>	@strtolower($Input['Email']),
		"PhoneNumber" =>	@strtolower($Input['PhoneNumber']),		
		"FathersName"  =>	@$Input['FathersName'],
		"MothersName"  =>	@$Input['MothersName'],
		"PermanentAddress"  =>	@$Input['Address'],
		"FathersPhone"  =>	@$Input['FathersPhone'],
		"FathersEmail"  =>	@$Input['FathersEmail'],
		"Remarks" =>	@$Input['Remarks']));
		
		$this->db->insert('tbl_students', $InsertDatas);
		$StudentsID = $this->db->insert_id();		
				
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}		

		if (!empty($Input['Email'])) 
		{
			$InstName = "Aim Civil Services";
			$InstEmail = "aimcivilservices.munna.ji@gmail.com";
			$InstUserID = $InstituteID;			
			
	       	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
		   	
		   	$this->load->model('Recovery_model');
			$Token = $this->Recovery_model->generateToken($EntityID, 2);

			$content = $this->load->view('emailer/signup', array(
			"Name" => $Input['FirstName'].' '.$Input['LastName'],
			'Token' => $Token,
			'DeviceTypeID' => 1,
			"InstituteProfilePic"=>@$MediaURL,
			'Signature' => "Thanks, <br>".$InstName
			) , TRUE);

			
			sendMail(array(
				"From_name"  => $InstName,
				'emailTo' => $Input['Email'],
				'emailSubject' => "Welcome at " . $InstName,
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			return $EntityID;
		}	
		

		return $EntityID;		
	}


	


	function registrationByCodeDirectValidate($Input = array())
	{
		$UPhoneNumber = trim($Input['PhoneNumberForChange']);
		if(!empty($UPhoneNumber))
		{
			$this->db->select('PhoneNumber');
			$this->db->from('tbl_users');
			$this->db->where('PhoneNumber = "'.$UPhoneNumber.'" OR PhoneNumberForChange = "'.$UPhoneNumber.'"');
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'PhoneNumber Exist';
				die;
			}
		}

		$UEmail = trim($Input['Email']);
		if(!empty($UEmail))
		{
			$this->db->select('Email');
			$this->db->from('tbl_users');
			$this->db->where('Email = "'.$UEmail.'" OR EmailForChange = "'.$UEmail.'"');
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'Email Exist';
				die;
			}
		}
		

		return 1;		
	}


	function addStudentToInstituteForBuyApp($Where = array())
	{
		$InstituteID = $Where['InstCode'];
		$FirstName = $Where['FirstName'];
		$Email = $Where['Email'];
		$Mobile = $Where['Mobile'];
		
		$PaidAmount = $Where['PaidAmount'];
		$PaymentDate = $Where['PaymentDate'];
		$OrderStatus = $Where['OrderStatus'];
		$TransactionID = $Where['TrackingID'];	

		$this->db->trans_start();

		$EntityGUID = get_guid();

		$UserTypeID = 7;

		$InsertData = array(
			"EntityGUID" 		=>	$EntityGUID,
			"EntityTypeID"		=>	15,
			"CreatedByUserID"  => $InstituteID,
			"InstituteID"		=>	$InstituteID,
			"EntryDate" 		=>	date("Y-m-d H:i:s"),
			"StatusID"			=>	1
		);

		$this->db->insert('tbl_entity', $InsertData);
		$EntityID = $this->db->insert_id();

		
		$InsertData = array_filter(array(
			"UserID" 				=> 	$EntityID,
			"UserGUID" 				=> 	$EntityGUID,			
			"UserTypeID" 			=>	$UserTypeID,			
			"FirstName" 			=> 	@$FirstName,					
			"Email" 				=>	@strtolower($Email),
			"Username" 				=>	@strtolower($Email),
			"PhoneNumber" 			=>	@$Mobile,
			"PhoneNumberForChange" 	=>	@$Mobile,			
			"AdminAccess" 		=>	"Yes",			
			"Resource"   =>	"Direct"		 
		));		
		$this->db->insert('tbl_users', $InsertData);



		$InsertData = array(
		"StudentID"=>$EntityID,			
		"StudentGUID"=>$EntityGUID,
		"Email"	=>	@strtolower($Email),
		"PhoneNumber"	=>	@$Mobile,
		"ConvertPaidAmount"=>$PaidAmount,
		"ConvertOrderStatus"=>$OrderStatus,
		"ConvertTrackingID"=>$TransactionID,					
		"ConvertDate"=>date("Y-m-d H:i:s")
		);
		$this->db->insert('tbl_students', $InsertData);



		/*$Password = $this->random_strings(6);		
		$InsertDataa = array_filter(array(
			"UserID" 		=> $EntityID,
			"Password"		=> md5($Password),
			"SourceID"		=> 1,
			"EntryDate"		=> date("Y-m-d H:i:s")));
		$this->db->insert('tbl_users_login', $InsertDataa);*/

		
		//$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));


		$this->db->trans_complete();

		if($EntityID > 0)
		{
			$this->load->model('Recovery_model');
			
			
			$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName,ProfilePic");
			$instProfilePic = $InstituteData['ProfilePic'];			
	        if(!empty($instProfilePic) && isset($instProfilePic))
	        {				    	
		    	$MediaURL= BASE_URL."uploads/profile/picture/".$instProfilePic;
		    }
		    else
		    {
		    	$MediaURL= BASE_URL."uploads/profile/picture/default.jpg";
		    }

			$Token = $this->Recovery_model->generateToken($EntityID, 2);

			$content = $this->load->view('emailer/signup', array(
				"Name" => $FirstName,
				'Token' => $Token,
				'DeviceTypeID' => 1,
				"InstituteProfilePic"=>@$MediaURL,
				'Signature' => "Thanks, <br>".$InstituteData['FirstName']
			) , TRUE);

			sendMail(array(
				"From_name"  => $InstituteData['FirstName'],
				'emailTo' => $Email,
				'emailSubject' => "Welcome at " . $InstituteData['FirstName'],
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			$EmailText = "New Registration For Buy App Of Institute Code ".$InstituteID.":";
			$EmailText .= "<br>";
			$EmailText .= "<br>";
			$EmailText .= "Name : ".$FirstName;
			$EmailText .= "<br>";
			$EmailText .= "Email : ".$Email;
			$EmailText .= "<br>";
			$EmailText .= "Mobile Number : ".$Mobile;
			$EmailText .= "<br>";
			$content = $this->load->view('emailer/common', array("EmailText" =>  $EmailText),TRUE);
			sendMail(array(
				'emailTo' 		=> "contact@iconik.in, cse@iconik.in",							
				'emailSubject'	=> "New Registration For Buy App Of Institute Code ".$InstituteID,
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));					
			
		}	

		return $EntityID;	
	}

}

