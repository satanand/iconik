<table border="0" width="100%">
<tr>
	<td width="10%">
		<?php
		
		if(isset($session_data['InstituteProfilePic']) && !empty($session_data['InstituteProfilePic']))
		{
			$ProfilePic = $session_data['InstituteProfilePic'];
		}
		elseif(isset($session_data['ProfilePic']) && !empty($session_data['ProfilePic']))
		{
			$ProfilePic = $session_data['ProfilePic'];
		}
		else
		{
			$ProfilePic = "default.jpg";
		}	
			
		?>

		<img src="<?php echo $ProfilePic;?>" width="100px" height="70px">
		
	</td>  

	<td width="90%" align="center">
		<h1><?php echo ucwords($session_data['FullName']);?></h1>
		<?php echo ucwords($session_data['Address'].", ".$session_data['CityName'].', '.$session_data['State']);?>
		<br/><?php echo $session_data['EmailForChange'];?>
	</td>
</tr>		
</table>  

<hr class="light-grey-hr" />



<table border="0" width="100%">
<tr>
<td colspan="2" align="center"><b>Paper Title:</b>&nbsp;<?php echo ucfirst($header_title);?></td>
</tr>

<tr>
	<td>
		<b>Batch Name:</b>&nbsp;<?php echo ucfirst($listing_data['Data'][0]['CommonDetails']['BatchName']);?><br/>
		<b>Subject Name:</b>&nbsp;<?php echo ucfirst($listing_data['Data'][0]['CommonDetails']['SubjectName']);?>
	</td>

	<td align="right">
		<b>Test Duration:</b>&nbsp;<?php echo $listing_data['Data'][0]['CommonDetails']['TestDuration'];?> Min.<br/>
		
	</td>
</tr>
</table>

<hr class="light-grey-hr" />


<table class="table table-striped table-hover" width="100%" border="0">
<thead>
<tr>
	<th>Student</th>

	<th>Attempt Date</th>		

	<th style="text-align: center;">Total Questions</th>								

	<th style="text-align: center;">Attempted</th>

	<th style="text-align: center;">Skip Questions</th>

	<th style="text-align: center;">Correct Answers</th>

	<th style="text-align: center;">Wrong Answers</th>

	<th style="text-align: center;">Total Marks</th>

	<th style="text-align: center;">Passing %</th>

	<th style="text-align: center;">Obtained Marks</th>
</tr>
</thead>

<tbody>
<?php
if(isset($listing_data['Data']) && !empty($listing_data['Data']))
{
	foreach($listing_data['Data'] as $key => $arr)
	{
?>
		<tr>

			<td><?php echo ucwords($arr['FirstName'] ." ".$arr['LastName']);?></td>

			<td><?php echo $arr['AttemptDate'];?></td>

			<td style="text-align: center;"><?php echo $arr['TotalQuestions'];?></td>

			<td style="text-align: center;"><?php echo $arr['AttemptQuestions'];?></td>

			<td style="text-align: center;"><?php echo $arr['SkipQuestions'];?></td>

			<td style="text-align: center;"><?php echo $arr['CorrectAnswers'];?></td>

			<td style="text-align: center;"><?php echo $arr['WrongAnswers'];?></td>

			<th style="text-align: center;"><?php echo $arr['TotalMarks'];?></th>  

			<td style="text-align: center;"><?php echo $arr['PassingMarks'];?></td>  

			<th style="text-align: center;"><?php echo $arr['GainedMarks'];?></th>

		</tr>

<?php
	}
}
?>
</tbody>

</table>