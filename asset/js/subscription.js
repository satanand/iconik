app.controller('PageController', function($scope, $http, $timeout) {

    $scope.data.pageSize = 20;
    $scope.data.dataList = [];

    $( "#FilterStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });



    $( "#FilterPaymentStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterPaymentEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterPaymentEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterPaymentStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    $scope.applyFilter = function()

    {

        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getCategoryList();
        $scope.getList();
    }




    /*list append*/

    $scope.getList = function(check=0)

    {
        if (check != 1) {

            if ($scope.data.listLoading || $scope.data.noRecords) return;

        } else {

            $scope.data.pageNo = 1;
            $scope.data.dataList = [];

        }


        $scope.data.pageSize = 20;


        $scope.data.listLoading = true;

        //alert(type);

        var data = 'SessionKey=' + SessionKey + '&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&' + $('#filterForm').serialize();


        $http.post(API_URL + 'subscription/getSubscribedUsers', data, contentType).then(function(response) {

            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) {
                /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    $scope.data.dataList.push(response.Data.Records[i]);

                }

                $scope.data.pageNo++;

            } else {

                $scope.data.noRecords = true;

            }


            $scope.data.listLoading = false;

            //setTimeout(function(){ tblsort(); }, 1000);

        });

    }

    
    $scope.getBrochuresList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'subscription/getBrochures', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                
                $scope.data.pageNo++;
            }
            else
            {
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });

    }



    $scope.getOrdersList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'subscription/getOrders', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                
                $scope.data.pageNo++;
            }
            else
            {
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });

    }


    $scope.loadViewOrder = function(OrderID, InstituteID)
    {        
        $scope.data.OrderDetails = [];

        $('#view_order_model').modal({
            show: true
        });

        $scope.OrderDetailsLoading = true;
        
        var APIURL = API_URL+'subscription/getOrdersDetails';

        $http.post(APIURL, 'SessionKey=' + SessionKey + '&OrderID=' + OrderID + '&InstituteID=' + InstituteID, contentType).then(function(response) 
        {
            $scope.OrderDetailsLoading = false;

            var response = response.data;

            if (response.ResponseCode == 200) 
            {
                $scope.data.OrderDetails = response.Data.Records;                    
            }
        });
    }


    $scope.generateInvoice = function(OrderID, InstituteID)
    {        
        if(OrderID <= 0)
        {
            alert("Invalid order"); return false;    
        }        

        window.location.href = BASE_URL + "subscription/generateInvoice?oid=" + OrderID + '&iid=' + InstituteID;
    }

    $scope.generatePayoutPDF = function()
    {
        var FilterPaymentStartDate = $("#FilterPaymentStartDate").val();
        var FilterPaymentEndDate = $("#FilterPaymentEndDate").val();
        var FilterStartDate = $("#FilterStartDate").val();
        var FilterEndDate = $("#FilterEndDate").val();
        var Keyword = $("#Keyword").val();

        window.location.href = BASE_URL + "subscription/payouts_pdf?FPS=" + FilterPaymentStartDate + '&FPE=' + FilterPaymentEndDate + '&FSD=' + FilterStartDate + '&FED=' + FilterEndDate + '&KEY=' + Keyword;
    }   



    $scope.getPayoutsList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'subscription/getPayouts', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                
                $scope.data.pageNo++;
            }
            else
            {
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });
    }


    $scope.loadFormMakePayment = function (OrderID, InstituteID, NetAmountSum, PayableAmount, Commission, PayDate)
    {
        $scope.templateURLAdd = PATH_TEMPLATE+'subscription/add_payment_form.htm?'+Math.random();

        $scope.OrderID = OrderID;
        $scope.InstituteID = InstituteID;

        $scope.AmountTotal = NetAmountSum;
        $scope.PayableAmount = PayableAmount;
        $scope.Commission = Commission;
        $scope.PayDate = PayDate;

        $('#add_model').modal({show:true});

        $timeout(function(){            

           $( "#PayoutDate" ).datepicker({
              dateFormat: "dd-mm-yy",
              changeMonth: true,
              minDate: $scope.PayDate
            });

           tinymce.init({
              selector: '#Remarks'
            });

       }, 1000);

    }


    $scope.addDataPayout = function ()
    {
        $scope.addDataLoading = true;

        var v1 = tinymce.get('Remarks').getContent();        
        
        $("#Remarks").val(v1);

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_make_payment_form']").serialize();

        $http.post(API_URL+'subscription/addPayout', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);                

                $timeout(function()
                {
                   $('#add_model .close').click();
                   window.location.href = BASE_URL + "subscription/payouts";

               }, 1000);

            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;          

        });
    }

});

function clear_filters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword, #DType").val('');

    angular.element(document.getElementById('content-body')).scope().getBrochuresList(1);
}


function clear_order_filters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword, #FilterPaymentStartDate, #FilterPaymentEndDate").val('');

    angular.element(document.getElementById('content-body')).scope().getOrdersList(1);
}


function clear_payout_filters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword, #FilterPaymentStartDate, #FilterPaymentEndDate").val('');

    angular.element(document.getElementById('content-body')).scope().getPayoutsList(1);
}

/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



    $(".table-sortable tbody").sortable({

        placeholder: 'tr_placeholder',

        helper: fixHelper,

        cursor: "move",

        tolerance: 'pointer',

        axis: 'y',

        dropOnEmpty: false,

        update: function(event, ui) {

            sendOrderToServer();

        }

    }).disableSelection();

    $(".table-sortable thead").disableSelection();




    function sendOrderToServer() {

        var order = 'SessionKey=' + SessionKey + '&' + $("#tabledivbody").sortable("serialize");

        $.ajax({

            type: "POST",
            dataType: "json",
            url: API_URL + 'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }


}
/* sortable - ends */