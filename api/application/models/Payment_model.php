<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	function random_strings($length_of_string) { 
	  
	    // String of all alphanumeric character 
	    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
	  
	    // Shufle the $str_result and returns substring 
	    // of specified length 
	    return substr(str_shuffle($str_result), 0, $length_of_string); 
	} 


	function addPricing($EntityID,$Input){
		$this->db->trans_start();	

		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);

		if($UserTypeID!=1){
			$EntityID = $this->Common_model->getInstituteByEntity($EntityID);
		}

		$InsertData = array_filter(array(
			"Validity" 	=>	$Input['Validity'],
			"Price" 	=>	$Input['Price'],		
			"EntryDate" =>	date("Y-m-d H:i:s"),
			"InstituteID" => $EntityID
		));

		$this->db->insert('set_keys_price', $InsertData);
		$PriceID = $this->db->insert_id();
			
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return $data['PriceID'] = $PriceID;
	}

	

	
	/*
	Description: 	Use to get pricing list
	*/
	function getPricing($EntityID,$PriceID="",$multiRecords=FALSE,  $PageNo=1, $PageSize=15){

		$UserTypeID = $this->Common_model->getUserTypeByEntityID($EntityID);

		if($UserTypeID!=1){
			$EntityID = $this->Common_model->getInstituteByEntity($EntityID);
		}

		$this->db->select('*');
		$this->db->from('set_keys_price');

		if(!empty($PriceID)){
			$this->db->where('PriceID',$PriceID);
		}

		$this->db->where('InstituteID',$EntityID);

		$this->db->order_by("PriceID","DESC");

		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}

		$Query = $this->db->get();
		//echo $this->db->last_query(); die;

		if($Query->num_rows()>0){
			$data = $Query->result_array();
		}else{
			$data = array();
		}

		return $data;
	}

}