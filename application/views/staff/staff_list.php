<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}

.content
{
	position: relative !important;
}	
</style>
<header class="panel-heading"> <h1 class="h4">Manage Staff</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController"  ng-init="getFilterData()"><!-- Body -->

	<!-- Top container -->
	<div class="clearfix mt-2 mb-2">
		<div class="row float-left" style="margin-left: 0px;width:100%;">
			<span class="float-left records hidden-sm-down" style="width:50%;">
				<span class="h5">Total Staff: {{data.dataList.length}}</span>
			</span>
			<div class="float-right" style="width:50%;">		
				<button class="btn btn-success btn-sm ml-1 float-right" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Staff</button>
				<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadImportStaff();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Import Staff</button>
			</div>
		</div>
	</div>
	<div class="clearfix mt-2 mb-2">
		<form class="form-inline" id="filterForm" role="form" autocomplete="off">
			<div class="row">					
					<div class="col-md-2">
			         <div class="form-group">
						<select class="form-control" id="UserTypeID"  name="UserTypeID" ng-model="UserTypeIDs">
							<option value="">Select Role</option>
							<option ng-repeat="rows in data.Roles" value="{{rows.UserTypeID}}"
							ng-selected="UserTypeID==rows.UserTypeID">{{rows.UserTypeName}}</option>
						</select>
					  </div>
					</div>
					<div class="col-md-2">
			         <div class="form-group">
						<select class="form-control" id="AppAccess" name="AppAccess" ng-model="AppAccess" id="AppAccess">
							<option value="">App Access</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
					  </div>
					</div>
					<div class="col-md-3">
			         <div class="form-group">
						<select class="form-control" id="AdminAccess" name="AdminAccess" ng-model="AdminAccess" id="AdminAccess">
							<option value="">Admin Panel Access</option>
							<option value="Yes">Yes</option>
							<option value="No">No</option>
						</select>
					  </div>
					</div>
					<!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
					<div class="col-md-2">
					 	<div id="universal_search">
					 		<input type="hidden" name="PageType" value="StaffList">
			                <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Type and search" style="" autocomplete="off">
			            </div>    
			        </div>
			        
			        <div class="col-md-3">    
			            <div class="form-group">
			      			<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
							<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
			      		</div>
			            
			        </div>
		    </div>
		</form>
	</div>




	<!-- Top container/ -->



	<!-- Data table -->
	<div class="table-responsive block_pad_md"  infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0" id="staff-body"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>


		<form name="records_form" id="records_form">
			<!-- data table -->
			<table class="table table-striped table-hover">
				<!-- table heading -->
				<thead>
					<tr>
						<!-- <th style="width: 50px;" class="text-center" ng-if="data.dataList.length>1"><input type="checkbox" name="select-all" id="select-all" class="mt-1"></th>	 -->

						<th style="width: 300px;">Staff</th>
						<th>Contact No.</th>
						<!-- <th style="width: 70px;">Gender</th> -->
						<th style="width: 200px;">Role</th>
						<th style="width: 160px;">Admin Panel Access</th>
						<th style="width: 160px;">App Access</th>
						<!-- <th style="width: 100px;" class="text-center">Email Status</th> -->
						<th style="width: 150px;" class="text-center">Action</th>

					</tr>
				</thead>
				<!-- table body -->
				<tbody>
					<tr scope="row" ng-repeat="(key, row) in data.dataList">
						<!-- <td class="text-center"  ng-if="data.dataList.length>1">
							<input type="checkbox" name="select-all-checkbox[]" id="select-all-checkbox-{{key}}" class="mt-2 select-all-checkbox" value="{{row.UserGUID}}" ng-if="data.UserGUID!=row.UserGUID">
						</td> -->

						<td class="listed sm clearfix">
							<img class="rounded-circle float-left" ng-src="{{row.ProfilePic}}">
							<div class="content"><strong>{{row.FullName | capitalizeFirstLetter}}</strong>
								<div ng-if="row.Email"><a href="mailto:{{row.Email}}" target="_top">{{row.Email}}</a></div><div ng-if="!row.Email">-</div>
							</div>

						</td> 
						
						<td><span ng-if="row.PhoneNumber">{{row.PhoneNumber}}</span><span ng-if="!row.PhoneNumber">-</span></td> 
						<!-- <td><span ng-if="row.Gender">{{row.Gender}}</span><span ng-if="!row.Gender">-</span></td> --> 
						<!-- <td><span ng-if="row.BirthDate">{{row.BirthDate}}</span><span ng-if="!row.BirthDate">-</span></td>  -->
						<td ng-bind="row.UserTypeName | capitalizeFirstLetter"></td>
						<td>{{row.AdminAccess}}</td> 
						<td>{{row.AppAccess}}</td>  
						<!-- <td ng-bind="row.RegisteredOn"></td>  
						<td><span ng-if="row.LastLoginDate">{{row.LastLoginDate}}</span><span ng-if="!row.LastLoginDate">-</span></td>  -->
						<!-- <td class="text-center"><span ng-class="{Pending:'text-danger', Verified:'text-success',Deleted:'text-danger',Blocked:'text-danger'}[row.Status]">{{row.Status}}</span></td>  -->
						<td class="text-center">
									<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.UserID)" title="View"></a>

									<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.UserID)" title="Edit"></a>

									<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.UserID)" title="Delete"></a>

									<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0"  href="" ng-click="loadFormWalloffame(key,row.UserID,row.FullName,row.ProfilePic)" title="Add Wall Of Fame"> 
									<img src="./asset/img/wall.png" style="width: 35px;"></a>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
			<!-- no record -->
			<p class="no-records text-center" ng-if="!data.dataList.length">
				<span ng-if="data.dataList.length">No more records found.</span>
				<span ng-if="!data.dataList.length">No records found.</span>
			</p>
		</div>
		<!-- Data table/ -->



		<!-- add Modal -->
		<div class="modal fade" id="add_model">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 875px; height: 850px;">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Add <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLAdd"></div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="import_model">

			<div class="modal-dialog modal-md" role="document">

				<div class="modal-content">

					<div class="modal-header">

						<h3 class="modal-title h5">Import Data</h3>     	

						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					</div>

					<div ng-include="templateURLImport"></div>

				</div>

			</div>

		</div>


		<!-- add Modal -->
		<div class="modal fade" id="view_model">
			<div class="modal-dialog modal-lg" role="document" style="height: 850px; max-width: 875px;">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?> Details</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLView"></div>
				</div>
			</div>
		</div>

		<!-- add Wall Modal -->
		<div class="modal fade" id="addwall_model">
			<div class="modal-dialog modal-md" role="document" style="max-width: 650px;">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Add Wall Of Fame</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLAddWall"></div>
				</div>
			</div>
		</div>



		<!-- edit Modal -->
		<div class="modal fade" id="edits_model">
			<div class="modal-dialog modal-lg" role="document" style="height: 850px;max-width: 875px;">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?> Details</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="edit_form" name="edit_form" ng-include="templateURLEdit">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>



		<!-- add Modal Category-->
		<div class="modal fade" id="add_category_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Add Role</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div ng-include="templateURLCategory"></div>
				</div>
			</div>
		</div>

	</div><!-- Body/ -->