<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Recovery_model extends CI_Model

{

	public function __construct()

	{

		parent::__construct();	

	}



	/*

	Description: 	Use to set OTP and send to user for password recovery.

	Procedures:

	1. Select User data by provided username, and continew if user exist.

	2. Delete old unused verification token.

	3. Genrate a Token for set new password and save to tokens table, if user status is Pending or Email-Confirmed.

	4. Send Password Assistance Email to User with Token (If user is not Pending or Email-Confirmed then email send without Token).

	*/

	function addTest($EntityID, $ToEntityID, $Input=array()){
		$QtBankGUID = get_guid();
		/* Add post to entity table and get EntityID. */
		$QtBankID = $this->Entity_model->addEntity($QtBankGUID, array(
			"EntityTypeID"	=>	10,
			"UserID"		=>	$EntityID,
			"Privacy"		=>	@$Input["Privacy"],
			"StatusID"		=>	2,
			"Rating"		=>	@$Input["Rating"]
		));

		$this->db->select('ParentCategoryID');
        $this->db->where('CategoryID',$Input["SubjectID"]);
        $this->db->from('set_categories');
        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ( $query->num_rows() > 0 )
        {
            $row = $query->row_array();
            $Input["CourseID"] = $row['ParentCategoryID'];

            $InsertData = array_filter(array(
				"QtBankID" 		=>	$QtBankID,
				"QtBankGUID" 	=>	$QtBankGUID,			
				"ParentQtBankID" =>	$QtBankID,
				"CourseID" 		=>	$Input["CourseID"],	
				"SubjectID" 	=>	$Input["SubjectID"],	
				"EntityID" 		=>	$EntityID,
				"QuestionsGroup" =>	$Input["QuestionsGroup"],
				"QuestionsLevel" => 	$Input["QuestionsLevel"],
				"QuestionsType" =>	$Input["QuestionsType"],
				"QuestionsMarks" =>	$Input["QuestionsMarks"],
				"QuestionContent" => $Input["QuestionContent"],
				"Explanation"	=>	@$Input["Explanation"]
			));
			$this->db->insert('tbl_question_bank', $InsertData);

			if($Input["QuestionsType"] == 'Multiple Choice'){
				$AnswerContent = json_decode($Input["AnswerContent"]);
				foreach ($AnswerContent as $key => $value) {
					if($Input["CorrectAnswer"] == $value){
						$CorrectAnswer = 1;
					}else{
						$CorrectAnswer = 0;
					}
					
					$AnswerData[] = array(
						"QtBankID" 		=>	$QtBankID,
						"AnswerContent" => $value,
						"CorrectAnswer"	=> $CorrectAnswer
					);
				}
				if($AnswerData){
					$this->db->insert_batch('tbl_questions_answer', $AnswerData);
				}
			}else if($Input["QuestionsType"] == 'Logical Answer'){
					if($Input["CorrectAnswer"] == 'true'){
						$CorrectAnswerTrue = 1;
						$CorrectAnswerFalse = 0;
					}else{
						$CorrectAnswerTrue = 0;
						$CorrectAnswerFalse = 1;
					}
					$AnswerData[] = array(
						"QtBankID" 		=>	$QtBankID,
						"AnswerContent" => 'True',
						"CorrectAnswer"	=> $CorrectAnswerTrue
					);
					$AnswerData[] = array(
						"QtBankID" 		=>	$QtBankID,
						"AnswerContent" => 'False',
						"CorrectAnswer"	=> $CorrectAnswerFalse
					);
				if($AnswerData){
					$this->db->insert_batch('tbl_questions_answer', $AnswerData);
				}
			}else if($Input["QuestionsType"] == 'Short Answer'){
				$AnswerContent = json_decode($Input["AnswerContent"]);
				$AnswerData[] = array(
					"QtBankID" 		=>	$QtBankID,
					"AnswerContent" => $Input["AnswerContent"],
					"CorrectAnswer"	=> $Input["AnswerContent"]
				);
				if($AnswerData){
					$this->db->insert_batch('tbl_questions_answer', $AnswerData);
				}
			}
			return array('QtBankID' => $QtBankID, 'QtBankGUID' => $QtBankGUID);
        }else{
        	return FALSE;
        }
	}



	function recovery($Username){

		

		$UserData=$this->Users_model->getUsers('UserID,Email,StatusID',array('LoginKeyword'=>$Username));
		
		


		if (!empty($UserData)){



			if($UserData['StatusID']==1 || $UserData['StatusID']==2){

				/* Genrate a Token for set new password and save to tokens table, if user status is Pending or Email-Confirmed. */

				$Token = $this->generateToken($UserData['UserID'], 1);

              $EmailText = "One time password reset code is given below:";

			}elseif($UserData['StatusID']==3){

				$EmailText = "Your account has been deleted. Please contact the Admin for more info.";

			}elseif($UserData['StatusID']==4){

				$EmailText = "Your account has been blocked. Please contact the Admin for more info.";

			}elseif($UserData['StatusID']==5){

				$EmailText = "You have deactivated your account, please contact the Admin to reactivate.";

			}



			/* Send Password Assistance Email to User with Token (If user is not Pending or Email-Confirmed then email send without Token). */
			$content = $this->load->view('emailer/recovery',array("Name" => $UserData['FirstName'], 'Token' => @$Token, 'EmailText' => $EmailText),TRUE);
			$SendMail = sendMail(array(

				'emailTo' 		=> $UserData['Email'],			

				'emailSubject'	=> $UserData['FirstName'] . " Password Reset Request",

				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

			));

			

			

			if($SendMail){

				return TRUE;

			}

		}

		return FALSE;

	}


	function recoverys($Username){



		$UserData=$this->Users_model->getUsers('UserID,Email,StatusID',array('LoginKeyword'=>$Username));



		if (!empty($UserData)){



				$Token = $this->generateToken($UserData['UserID'], 1);



				$EmailText = "One time password reset code is given below:";



			/* Send Password Assistance Email to User with Token (If user is not Pending or Email-Confirmed then email send without Token). */
			$content = $this->load->view('emailer/recoverys',array("Name" => $UserData['Username'], 'Token' => @$Token, 'EmailText' => $EmailText),TRUE);

			$SendMail = sendMail(array(

				'emailTo' 		=> $UserData['Email'],			

				'emailSubject'	=> SITE_NAME . " Password Reset Request",

				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

			));



			if($SendMail){

				return TRUE;

			}

		}

		return FALSE;

	}


	
	function contactUs($user_details){


		if (!empty($user_details)){


			/* Send Password Assistance Email to User with Token (If user is not Pending or Email-Confirmed then email send without Token). */

			$content = $this->load->view('emailer/recovery',array("Name" => $UserData['Username'], 'Token' => @$Token, 'EmailText' => $EmailText),TRUE);

			$SendMail = sendMail(array(

				'emailTo' 		=> 'contact@iconik.in',			

				'emailSubject'	=> "Iconik : Contact Us",

				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))

			));



			if($SendMail){

				return TRUE;

			}

		}

		return FALSE;

	}
	/*

	Description: 	Use to Verify Token

	*/

	function verifyToken($Token,$Type)
	{
		if(empty($Token)){return FALSE;}

		$this->db->select('UserID');

		$this->db->from('tbl_tokens');

		$this->db->where('Token',$Token);

		$this->db->where('Type',$Type);

		$this->db->where('StatusID',1); /*check for pending status*/

		$this->db->limit(1);

		$Query = $this->db->get();	

		if($Query->num_rows()>0)
		{
			return $Query->row()->UserID;
		}
		else
		{
			return FALSE;
		}

	}

	function verifyTokenPhone($Token,$Type)
	{
		if(empty($Token)){return FALSE;}

		$this->db->select('UserID, EntryDate');

		$this->db->from('tbl_tokens');

		$this->db->where('Token',$Token);

		$this->db->where('Type',$Type);

		$this->db->where('StatusID',1); /*check for pending status*/

		$this->db->limit(1);

		$Query = $this->db->get();	

		if($Query->num_rows()>0)
		{
			return $Query->row();
		}
		else
		{
			return FALSE;
		}

	}

/*

	Description: 	Use to Delete Token

	*/

	function deleteToken($Token,$Type=1){

		$this->db->where(array("Token" => $Token, "Type" => $Type));

		$this->db->limit(1);

		$this->db->update('tbl_tokens', array("StatusID" => 3));

		return TRUE;

	}
	/*

	Description: 	Use to add Token

	*/

	function generateToken($UserID, $Type=1){

		/* delete old unused token */	

		$this->db->where(array("UserID"=>$UserID, "Type"=>$Type, "StatusID"=>1));

		$this->db->delete('tbl_tokens');

		$this->db->limit(1);
	 	$Token =  random_string('numeric', 6);

	 	if(isset($Token) && !empty($Token))
	 	{
	 		$this->db->select('TokenID');
			$this->db->from('tbl_tokens');
			$this->db->where('Token',$Token);
			$this->db->limit(1);
			$Query = $this->db->get();	
			if($Query->num_rows()>0)
			{
				$Token =  time();
			}
	 	}

		$this->db->insert('tbl_tokens', array('UserID'=>$UserID,'Type'=>$Type,'Token'=>$Token,'EntryDate'=>date("Y-m-d H:i:s")));

		return $Token;

	}





	/*

	Description: 	Use to Expired Token

	*/

	function expiredToken($Token,$Type=3){

		$this->db->where(array("Token" => $Token, "Type" => $Type));

		$this->db->limit(1);

		$this->db->update('tbl_tokens', array("StatusID" => 9));

		return TRUE;

	}
	

	function verifyTokenAndGetData($Token,$Type, $StatusID=1)
	{
		if(empty($Token)){return FALSE;}

		//echo "$Token===$Type===$StatusID";

		$this->db->select('UserID, EntryDate');

		$this->db->from('tbl_tokens');

		$this->db->where('Token', $Token);

		$this->db->where('Type', $Type);

		$this->db->where('StatusID', $StatusID); /*check for pending status*/

		$this->db->limit(1);

		$Query = $this->db->get();	

		

		if($Query->num_rows()>0)
		{
			return $Query->row();
		}
		else
		{
			return FALSE;
		}

	}

	function getTokenInfoByID($UserID,$Type)
	{

		$this->db->select('*');

		$this->db->from('tbl_tokens');

		$this->db->where('UserID',$UserID);

		$this->db->where('Type',$Type);

		$this->db->limit(1);

		$Query = $this->db->get();

		//echo $this->db->last_query(); die;	

		if($Query->num_rows()>0)
		{
			return $Query->row();
		}
		else
		{
			return FALSE;
		}

	}

}



