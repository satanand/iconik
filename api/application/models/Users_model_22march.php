<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}



	/*
	Description: 	Use to update user profile info.
	*/
	function updateUserInfo($UserID, $Input=array()){
		// if(!empty($Input['MasterFranchisee']) && $Input['MasterFranchisee'] == 'yes'){
		// 	$Input['InstitutrID'] = NULL;
		// }


		$UpdateArray = array_filter(array(
			"UserTypeID" 			=>	@$Input['UserTypeID'],			
			"FirstName" 			=>	@$Input['FirstName'],
			"MiddleName" 			=>	@$Input['MiddleName'],
			"LastName" 				=>	@$Input['LastName'],
			"About" 				=>	@$Input['About'],
			"About1" 				=>	@$Input['About1'],
			"About2" 				=>	@$Input['About2'],
			"ProfilePic" 			=>	@$Input['ProfilePic'],
			"ProfileCoverPic" 		=>	@$Input['ProfileCoverPic'],
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],			
			"BirthDate" 			=>	@$Input['BirthDate'],
			"Age" 					=>	@$Input['Age'],			
			"Height" 				=>	@$Input['Height'],		
			"Weight" 				=>	@$Input['Weight'],	
			"MaritalStatus"			=>	@$Input['MaritalStatus'],		


			"PhoneNumberForChange"	=>	@$Input['PhoneNumberForChange'],	
			"TelePhoneNumber"	=>	@$Input['TelePhoneNumber'],	
			"RegistrationNumber"	=>	@$Input['RegistrationNumber'],
			"CityCode"	=>	@$Input['CityCode'],
			"UrlType"	=>	@$Input['UrlType'],	

			"Address" 				=>	@$Input['Address'],	
			"Address1" 				=>	@$Input['Address1'],
			"Postal" 				=>	@$Input['Postal'],
			"CountryCode" 			=>	@$Input['CountryCode'],

			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"CityName" 				=>	@$Input['CityName'],
			"StateName" 			=>	@$Input['StateName'],
			"StateID" 			=>	@$Input['StateID'],
			"Latitude" 				=>	@$Input['Latitude'],	
			"Longitude" 			=>	@$Input['Longitude'],


			"LanguageKnown" 		=>	@$Input['LanguageKnown'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"Website" 				=>	@strtolower($Input['Website']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),	
			"TwitterURL" 			=>	@strtolower($Input['TwitterURL']),
			"GoogleURL" 			=>	@strtolower($Input['GoogleURL']),
			"InstagramURL" 			=>	@strtolower($Input['InstagramURL']),
			"LinkedInURL" 			=>	@strtolower($Input['LinkedInURL']),
			"WhatsApp" 				=>	@strtolower($Input['WhatsApp']),
			"MasterFranchisee"		=>	@strtolower($Input['MasterFranchisee']),
			"ParentUserID"		=>	@strtolower($Input['ParentUserID'])
		));

		if(!empty($Input['MasterFranchisee']) && $Input['MasterFranchisee'] == 'yes'){
			$UpdateArray['FranchiseeAssignCount'] = FranchiseeAssignCount + 1;
		}

		if(isset($Input['LastName']) && $Input['LastName']==''){		$UpdateArray['LastName'] = null;	}
		if(isset($Input['Username']) && $Input['Username']==''){		$UpdateArray['Username'] = null;	}
		if(isset($Input['Gender']) && $Input['Gender']==''){			$UpdateArray['Gender'] = null;		}
		if(isset($Input['BirthDate']) && $Input['BirthDate']==''){		$UpdateArray['BirthDate'] = null;	}
		if(isset($Input['Address']) && $Input['Address']==''){			$UpdateArray['Address'] = null;		}	
		if(isset($Input['PhoneNumber']) && $Input['PhoneNumber']==''){	$UpdateArray['PhoneNumber'] = null;	}
		if(isset($Input['Website']) && $Input['Website']==''){			$UpdateArray['Website'] = null;		}
		if(isset($Input['FacebookURL']) && $Input['FacebookURL']==''){	$UpdateArray['FacebookURL'] = null;	}
		if(isset($Input['TwitterURL']) && $Input['TwitterURL']==''){	$UpdateArray['TwitterURL'] = null;	}
		if(isset($Input['PhoneNumber']) && $Input['PhoneNumber']==''){	$UpdateArray['PhoneNumber'] = null;	}
		

		/*for change email address*/
		if(!empty($UpdateArray['Email']) || !empty($UpdateArray['PhoneNumber'])){
			$UserData = $this->Users_model->getUsers('Email,FirstName,PhoneNumber',array('UserID'=>$UserID));	
		}

		/*for update email address*/
		if(!empty($UpdateArray['Email'])){
			if($UserData['Email']!=$UpdateArray['Email']){
				$UpdateArray['EmailForChange'] = $UpdateArray['Email'];			
				/* Genrate a Token for Email verification and save to tokens table. */
				$this->load->model('Recovery_model');
				$Token = $this->Recovery_model->generateToken($UserID, 2);
				/* Send welcome Email to User with Token. */
				/*sendMail(array(
					'emailTo' 		=> $UpdateArray['EmailForChange'],			
					'emailSubject'	=> SITE_NAME.", OTP for change of email address.",
					'emailMessage'	=> emailTemplate($this->load->view('emailer/change_email',array("Name" =>  $UserData['Username'], 'Token' => $Token),TRUE)) 
				));*/
				sendMail(array(

					'emailTo' => $UpdateArray['EmailForChange'],
					'emailSubject' => $Input['FirstName']. "Verify Your Email",
					'emailMessage' => emailTemplate($this->load->view('emailer/signup', 
						array("Name" => $Input['FirstName'],'Token' => $Token) , TRUE))

				));
				unset($UpdateArray['Email']);
			}
		}


		/*for update phone number*/
		if(!empty($UpdateArray['PhoneNumber']) && PHONE_NO_VERIFICATION && !isset($Input['SkipPhoneNoVerification'])){
			if($UserData['PhoneNumber']!=$UpdateArray['PhoneNumber']){
				$UpdateArray['PhoneNumberForChange'] = $UpdateArray['PhoneNumber'];			
				/* Genrate a Token for PhoneNumber verification and save to tokens table. */
				$this->load->model('Recovery_model');
				$Token = $this->Recovery_model->generateToken($UserID, 3);

				/* Send change phonenumber SMS to User with Token. */
				sendSMS(array(
					'PhoneNumber' 	=> $UpdateArray['PhoneNumberForChange'],			
					'Text'			=> SITE_NAME.", OTP to verify Mobile no. is: $Token",
				));
				unset($UpdateArray['PhoneNumber']);
			}
		}

		if(!empty($Input['PanStatus'])){
			$UpdateArray['PanStatus'] = $Input['PanStatus'];
		}
		if(!empty($Input['BankStatus'])){
			$UpdateArray['BankStatus'] = $Input['BankStatus'];
		}

		//print_r($UpdateArray); die;
		
		/* Update User details to users table. */
		if(!empty($UpdateArray)){
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', $UpdateArray);
		}
        
		if(!empty($Input['InterestGUIDs'])){
			/*Revoke categories - starts*/
			$this->db->where(array("EntityID"=>$UserID));
			$this->db->delete('tbl_entity_categories');
			/*Revoke categories - ends*/

			/*Assign categories - starts*/
			$this->load->model('Category_model');
			foreach($Input['InterestGUIDs'] as $CategoryGUID){
				$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
				if($CategoryData){
					$InsertCategory[] = array('EntityID'=>$UserID, 'CategoryID'=>$CategoryData['CategoryID']);
				}
			}
			if(!empty($InsertCategory)){
				$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
			}
			/*Assign categories - ends*/
		}


		if(!empty($Input['SpecialtyGUIDs'])){
			/*Revoke categories - starts*/
			$this->db->where(array("EntityID"=>$UserID));
			$this->db->delete('tbl_entity_categories');
			/*Revoke categories - ends*/

			/*Assign categories - starts*/
			$this->load->model('Category_model');
			foreach($Input['SpecialtyGUIDs'] as $CategoryGUID){
				$CategoryData = $this->Category_model->getCategories('CategoryID', array('CategoryGUID'=>$CategoryGUID));
				if($CategoryData){
					$InsertCategory[] = array('EntityID'=>$UserID, 'CategoryID'=>$CategoryData['CategoryID']);
				}
			}
			if(!empty($InsertCategory)){
				$this->db->insert_batch('tbl_entity_categories', $InsertCategory); 		
			}
			/*Assign categories - ends*/
		}


		
		$this->Entity_model->updateEntityInfo($UserID,array('StatusID'=>@$Input['StatusID']));
		return TRUE;
	}




	/*
	Description: 	Use to set user new password.
	*/
	function updateUserLoginInfo($UserID, $Input=array(), $SourceID){
		$UpdateArray = array_filter(array(
			"Password" 			=>	(!empty($Input['Password']) ? md5($Input['Password']) : '') ,
			"ModifiedDate" 	=>	(!empty($Input['Password']) ? date("Y-m-d H:i:s") : '') ,
			"LastLoginDate" 	=>	@$Input['LastLoginDate']
		));

		/* Update User Login details */
		$this->db->where('UserID', $UserID);
		$this->db->where('SourceID', $SourceID);
		$this->db->limit(1);
		$this->db->update('tbl_users_login', $UpdateArray);
		//echo $this->db->last_query();die;

		if(!empty($Input['Password'])){
			/* Send Password Assistance Email to User with Token (If user is not Pending or Email-Confirmed then email send without Token). */
			$UserData = $this->Users_model->getUsers('FirstName,Username,Email',array('UserID'=>$UserID));
			$SendMail = sendMail(array(
				'emailTo' 		=> $UserData['Email'],			
				'emailSubject'	=> SITE_NAME . " Password Assistance",
				'emailMessage'	=> emailTemplate($this->load->view('emailer/change_password',array("Name" => $UserData['Username']),TRUE))
			));
		}
		return TRUE; 
	}




	/*
	Description: 	ADD user to system.
	Procedures:
	1. Add user to user table and get UserID.
	2. Save login info to users_login table.
	3. Save User details to users_profile table.
	4. Genrate a Token for Email verification and save to tokens table.
	5. Send welcome Email to User with Token.
	*/
	function addUser($Input=array(), $UserTypeID, $SourceID, $StatusID=1){
		$this->db->trans_start();
		$EntityGUID = get_guid();

	 	$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		/* Add user to entity table and get EntityID. */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		if(!empty($InstituteID)){

				$EntityTypeID=14;
				$EntityID=$this->Entity_model->addEntitys($EntityGUID, array('UserID'=>$this->UserID,"EntityTypeID"=>$EntityTypeID,'InstituteID'=>$InstituteID,"StatusID"=>1));

		}else{

			/* Add user to entity table and get EntityID. */
				//$EntityID = $this->Entity_model->addEntity($EntityGUID, array("EntityTypeID"=>1, "StatusID"=>$StatusID));

				$InsertData = array_filter(array(
					"EntityGUID" 		=>	$EntityGUID,
					"EntityTypeID"		=>	1,
					"EntryDate" 		=>	date("Y-m-d H:i:s"),
					"StatusID"			=>	$StatusID
				));
				$this->db->insert('tbl_entity', $InsertData);
				$EntityID = $this->db->insert_id();

				$this->db->where("EntityID",$EntityID);
				$this->db->update('tbl_entity',array("InstituteID"=>$EntityID));


		}
		/* Add user to user table . */
		if(!empty($Input['PhoneNumber']) && PHONE_NO_VERIFICATION){
			$Input['PhoneNumberForChange'] = $Input['PhoneNumber'];
			unset($Input['PhoneNumber']);
		}
		$originalDate = $Input['BirthDate'];
		$newDate = date("Y-m-d", strtotime($originalDate));
		$InsertData = array_filter(array(
			"UserID" 				=> 	$EntityID,
			"UserGUID" 				=> 	$EntityGUID,			
			"UserTypeID" 			=>	$UserTypeID,
			"StoreID" 				=>	@$Input['StoreID'],
			"FirstName" 			=> 	@$Input['FirstName'],
			"MiddleName" 			=> 	@$Input['MiddleName'],			
			"LastName" 				=> 	@$Input['LastName'],
			"About" 				=>	@$Input['About'],
			"ProfilePic" 			=>	@$Input['ProfilePic'],
			"ProfileCoverPic" 		=>	@$Input['ProfileCoverPic'],			
			"Email" 				=>	@strtolower($Input['Email']),
			"Username" 				=>	@strtolower($Input['Username']),
			"Gender" 				=>	@$Input['Gender'],
			"BirthDate" 			=>	@$newDate,

			"Address" 				=>	@$Input['Address'],
			"Address1" 				=>	@$Input['Address1'],
			"Postal" 				=>	@$Input['Postal'],	
			"CountryCode" 			=>	@$Input['CountryCode'],

			"TimeZoneID" 			=>	@$Input['TimeZoneID'],
			"Latitude" 				=>	@$Input['Latitude'],
			"Longitude"				=>	@$Input['Longitude'],

			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"PhoneNumberForChange" 	=>	@$Input['PhoneNumberForChange'],
			"Website" 				=>	@strtolower($Input['Website']),
			"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),
			"TwitterURL" 			=>	@strtolower($Input['TwitterURL']),
			"ReferredByUserID" 		=>	@$Input['Referral']->UserID,
		));

		
		$this->db->insert('tbl_users', $InsertData);

		if($EntityTypeID==14){

			$count = count($Input['Education']);

			for($key=0; $key<$count; $key++) {
			 $InsertDatas[$key] = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"UserGUID" 				=> 	$EntityGUID,
				"Education" 		    =>	@$Input['Education'][$key],
				"Course" 			    =>	@$Input['Course'][$key],
				"Institute" 			=>	@$Input['Institute'][$key],
				"Specialization" 		=>	@$Input['Specialization'][$key],
				"Year" 			        =>	@$Input['Year'][$key],
				"Percentage" 			=>	@$Input['Percentage'][$key],

				));
			 	$this->db->insert('tbl_user_eduction', $InsertDatas[$key]);
			//echo $this->db->last_query();
		     }
		     $CourseID = ""; $SubjectID = "";
		     if(!empty($Input['CourseID'])){
		     	$CourseID = implode(",",$Input['CourseID']);
		     }

		     if(!empty($Input['SubjectID'])){
		     	$SubjectID = implode(",",$Input['SubjectID']);
		     }

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"UserGUID" 				=> 	$EntityGUID,			
				"UserTypeID" 			=>	$UserTypeID,
				"Grade" 			    =>	@$Input['Grade'],
				"Department" 			=>	@$Input['Department'],
				"Applicable" 		    =>	@$Input['Applicable'],
				"Reportto" 			    =>	@$Input['Reportto'],
				"ReportingManager" 		=>	@$Input['ReportingManager'],
				"JoiningDate" 			=>	date("Y-m-d", strtotime($Input['JoiningDate'])),
				"AppraisalDate" 		=>	date("Y-m-d", strtotime($Input['AppraisalDate'])),
				"CourseID" 		        =>	@$CourseID,
				"SubjectID" 		    =>	@$SubjectID

				));
			$this->db->insert('tbl_user_jobs', $InsertDatad);

			$counts = count($Input['Designation']);

			for($keys=0; $keys<$counts; $keys++) {
			$InsertDatae = array_filter(array(
				"UserID" 				=> 	$EntityID,
				"UserGUID" 				=> 	$EntityGUID,			
				"UserTypeID" 			=>	$UserTypeID,
				"Employer" 			    =>	@$Input['Employer'][$keys],
				"Designation" 			=>	@$Input['Designation'][$keys],
				"Industry" 		        =>	@$Input['Industry'][$keys],
				"From" 		            =>	date("Y-m-d", strtotime($Input['From'][$keys])),
				"To" 		            =>	date("Y-m-d", strtotime($Input['To'][$keys])),
			

				));
			$this->db->insert('tbl_user_experience', $InsertDatae);
		   }

		 }
		//echo $this->db->last_query(); die();
		/* Save login info to users_login table. */
		$InsertDataa = array_filter(array(
			"UserID" 		=> $EntityID,
			"Password"		=> md5(($SourceID=='1' ? $Input['Password'] : $Input['SourceGUID'])),
			"SourceID"		=> $SourceID,
			"EntryDate"		=> date("Y-m-d H:i:s")));

		$this->db->insert('tbl_users_login', $InsertDataa);

		/*save user settings*/
		$this->db->insert('tbl_users_settings', array("UserID"=>$EntityID));

			/*for update email address*/
		if(!empty($Input['Email'])){	
				$this->load->model('Recovery_model');
				sendMail(array(
					'emailTo' 		=> $Input['Email'],			
					'emailSubject'	=> $Input['FirstName']."  Welcome to Iconik.",
					'emailMessage'	=> emailTemplate($this->load->view('emailer/adduser',array("Name" =>  $Input['FirstName'], 'Password' => $Input['Password'],"Email"=>$Input['Email']),TRUE)) 
				));
				
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return $EntityID;
	}




	/*
	Description: 	Use to get single user info or list of users.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getUsers($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'MasterFranchisee'				=>	'U.MasterFranchisee',
				'ParentUserID'	=>	'U.ParentUserID',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'InstituteProfilePic'		=>	'IF(INST.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",INST.ProfilePic)) AS InstituteProfilePic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'ls.StateName',
				'State_id'				=>	'ls.State_id',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	=>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	=>	'U.CityCode',
				'UrlType'	=>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
				'CreatedByUserID' =>'E.CreatedByUserID',
				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			=>	'E.StatusID',
				'PanStatusID'				=>	'U.PanStatus',
				'BankStatusID'				=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.FirstName,U.LastName,U.Email,U.EmailForChange,U.ProfilePic,U.PhoneNumber,U.PhoneNumberForChange,U.CityName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.BirthDate,U.TelePhoneNumber,U.UrlType,U.MasterFranchisee,E.InstituteID,ls.StateName,ls.State_id,');

		$this->db->select($Field,false);

		if(!empty($Where['SourceID']) && $Where['SourceID'] == 6){
			$this->db->select('S.CourseID,S.Key,S.Validity,S.ActivatedOn,S.ExpiredOn,S.KeyStatusID');
		}
		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}		
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		if(!empty($Where['StudentID'])){
			$this->db->from('tbl_students S');
		}
		
		$this->db->where("U.UserID","E.EntityID", FALSE);	

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
		}

		$this->db->join('tbl_users INST', 'E.InstituteID = INST.UserID', 'left');
		$this->db->join('set_location_states ls', 'ls.State_id = U.StateID', 'left');

		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		
		if(!empty($Where['SourceID']) && $Where['SourceID'] == 6){
			$this->db->join('tbl_students S', 'U.UserID = S.StudentID','left');
		}

		if(array_keys_exist($Params, array('CountryName'))) {
			$this->db->join('set_location_country CO', 'U.CountryCode = CO.CountryCode', 'left');
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			if(validateEmail($Where['Keyword'])){
				$Where['Email'] = $Where['Keyword'];
			}
			elseif(is_numeric($Where['Keyword'])){
				$Where['PhoneNumber'] = $Where['Keyword'];
			}else{
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.LastName", $Where['Keyword']);
				$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
				$this->db->group_end();				
			}

		}

		if(!empty($Where['SourceID'])){
			$this->db->where("UL.SourceID",$Where['SourceID']);			
		}

		if(!empty($Where['UserTypeID'])){
			$this->db->where_in("U.UserTypeID",$Where['UserTypeID']);
		}
		
		if(!empty($Where['UserTypeIDNot']) && $Where['UserTypeIDNot']=='Yes'){
			$this->db->where("U.UserTypeID!=",$Where['UserTypeIDNot']);
		}

		if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}
		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}

		if(!empty($Where['LoginKeyword'])){
			$this->db->group_start();
			$this->db->where("U.Email",$Where['LoginKeyword']);
			$this->db->or_where("U.Username",$Where['LoginKeyword']);
			$this->db->or_where("U.PhoneNumber",$Where['LoginKeyword']);
			$this->db->group_end();
		}
		if(!empty($Where['Password'])){
			$this->db->where("UL.Password",md5($Where['Password']));
		}

		if(!empty($Where['IsAdmin'])){
			$this->db->where("UT.IsAdmin",$Where['IsAdmin']);
		}
		if(!empty($Where['StatusID'])){
			$this->db->where("E.StatusID",$Where['StatusID']);
		}
		if(!empty($Where['PanStatus'])){
			$this->db->where("U.PanStatus",$Where['PanStatus']);
		}
		if(!empty($Where['BankStatus'])){
			$this->db->where("U.BankStatus",$Where['BankStatus']);
		}

		if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
			$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		}else{
			$this->db->order_by('U.FirstName','ASC');			
		}


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	

		//echo $this->db->last_query(); die();

		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				if(!empty($Record['CourseID'])){
					$this->load->model('Category_model');
					$CategoryName = $this->getCourseName($Record['CourseID']);
					$Record['CourseName'] = $CategoryName;
				}

				if(!empty($Record['InstituteID'])){
					$Record['InstituteName'] = $this->getUserName($Record['InstituteID']);
				}

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}
	
	/*
	Description: 	Use to create session.
	*/
	function createSession($UserID, $Input=array())
	{
		/* Multisession handling */
		if (!MULTISESSION)
		{ 
			$this->db->delete('tbl_users_session', array('UserID' => $UserID));
		}else{
/*			if(empty(@$Input['DeviceGUID'])){
				$this->db->delete('tbl_users_session', array('DeviceGUID' => $Input['DeviceGUID']));
			}*/
		}

		/* Multisession handling - ends */
		$InsertData = array_filter(array(
			'UserID' 		=> $UserID,
			'SessionKey' 	=> get_guid(),
			'IPAddress' 	=> @$Input['IPAddress'],
			'SourceID'		=> (!empty($Input['SourceID']) ? $Input['SourceID'] : DEFAULT_SOURCE_ID),
			'DeviceTypeID' 	=> (!empty($Input['DeviceTypeID']) ? $Input['DeviceTypeID'] : DEFAULT_DEVICE_TYPE_ID),
			'DeviceGUID' 	=> @$Input['DeviceGUID'],
			'DeviceToken' 	=> @$Input['DeviceToken'],
			'EntryDate' 	=> date("Y-m-d H:i:s"),
		));

		$this->db->insert('tbl_users_session', $InsertData);

		$this->db->select('tbl_users_login.LastLoginDate');
		$this->db->from('tbl_users');
		$this->db->join('tbl_users_login','tbl_users.UserID = tbl_users_login.UserID');
		$this->db->where('tbl_users.UserTypeID',10);
		$this->db->where('tbl_users_login.LastLoginDate IS NULL');
		$this->db->limit(1);
		$lastLoginQuery = $this->db->get();

		if($lastLoginQuery->num_rows() > 0){
			$arr = array("UserID"=>$UserID,"TotalSMS"=>200,"AvailableSMS"=>200,"UsedSMS"=>0);
			$this->db->insert("tbl_bulk_sms_credits",$arr);
		}



		/*update current date of login*/
		$this->updateUserLoginInfo($UserID, array("LastLoginDate"=>date("Y-m-d H:i:s")), $InsertData['SourceID']);
		/*Update Latitude, Longitude */
		if(!empty($Input['Latitude']) && !empty($Input['Longitude'])){
			$this->updateUserInfo($UserID, array("Latitude"=>$Input['Latitude'], "Longitude"=>$Input['Longitude']));	
		}
		return $InsertData['SessionKey'];
	}



	/*
	Description: 	Use to get UserID by SessionKey and validate SessionKey.
	*/
	function checkSession($SessionKey,$UserID=""){
		$this->db->select('UserID');
		$this->db->from('tbl_users_session');
		if(!empty($SessionKey)){
			$this->db->where("SessionKey",$SessionKey);
		}
		if(!empty($UserID)){
			$this->db->where("UserID",$UserID);
		}
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			return $Query->row()->UserID;
		}
		return FALSE;
	}

	/*
	Description: 	Use to delete Session.
	*/
	function deleteSession($SessionKey){
		$this->db->limit(1);
		$this->db->delete('tbl_users_session', array('SessionKey' => $SessionKey));
		return TRUE;
	}
	
	/*
	Description: 	Use to set new email address of user.
	*/
	function updateEmail($UserID, $Email){
		/*check new email address is not in use*/
		$UserData=$this->Users_model->getUsers('', array('Email'=>$Email,));
		if(!$UserData){
			$this->db->trans_start();
			/*update profile table*/
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', array("Email" => $Email, "EmailForChange" => null));

			/* Delete session */
			$this->db->limit(1);
			$this->db->delete('tbl_users_session', array('UserID' => $UserID));
			/* Delete session - ends */
			$this->db->trans_complete();	        
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	
		return TRUE;
	}


	/*
	Description: 	Use to set new email address of user.
	*/
	function updatePhoneNumber($UserID, $PhoneNumber){
		/*check new PhoneNumber is not in use*/
		$UserData=$this->Users_model->getUsers('StatusID', array('PhoneNumber'=>$PhoneNumber));
		if(!$UserData){
			$this->db->trans_start();
			/*update profile table*/
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', array("PhoneNumber" => $PhoneNumber, "PhoneNumberForChange" => null));

			/* change entity status to activate */
			if($UserData['StatusID']==1){
				$this->Entity_model->updateEntityInfo($UserID, array("StatusID"=>2));					
			}

			$this->db->trans_complete();	        
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	
		return TRUE;
	}

	/*user type get*/
	function getusertype($EntityID)
	{
	    $this->db->select('*');
		$this->db->from('tbl_users_type');
		$this->db->where("EntityID",$EntityID);
		$Query = $this->db->get();	
		
		if($Query->result_array()>0){
			foreach($Query->result_array() as $Record){
				
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;
	}


	/*user type name*/
	function getUserName($UserID)
	{
	    $this->db->select('FirstName,MiddleName,LastName');
		$this->db->from('tbl_users');
		$this->db->where("UserID",$UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$UserData=$Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['FirstName'].' '.$UserData[0]['MiddleName'].' '.$UserData[0]['LastName'];

		}else{

			return FALSE;
		}
	}
	


	/*get user course*/
	function getCourseName($CourseID)
	{
	    $this->db->select('CategoryName');
		$this->db->from('set_categories');
		$this->db->where("CategoryID",$CourseID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		$UserData = $Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['CategoryName'];

		}else{

			return FALSE;
		}
	}


	/*create by institutrID*/
	function createbyinstitutrID($UserID)
	{
	    $this->db->select('EntityGUID');
		$this->db->from('tbl_entity');
		$this->db->where("CreatedByUserID = (select CreatedByUserID from tbl_entity where EntityID = ".$UserID.")");
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$EntityGUID=$Query->result_array();

		if(!empty($EntityGUID)){

			return $EntityGUID[0]['EntityGUID'];

		}else{

			$this->db->select('EntityGUID');
			$this->db->from('tbl_entity');
			$this->db->where("EntityID", $UserID);
			$this->db->limit(1);
			$Query = $this->db->get();	
			//echo $this->db->last_query();
			$EntityGUID=$Query->result_array();

			return $EntityGUID[0]['EntityGUID'];

		}
	
	}


	/**/
	function getMasterFranchisee($UserID)
	{
		$this->db->select('MasterFranchisee');
		$this->db->from('tbl_users');
		$this->db->where("UserID", $UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$UserData=$Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['MasterFranchisee'];

		}else{

			return FALSE;
		}
	}


	function getFranchiseeAssignCount($UserID)
	{
		$this->db->select('FranchiseeAssignCount');
		$this->db->from('tbl_users');
		$this->db->where("UserID", $UserID);
		$this->db->limit(1);
		$Query = $this->db->get();	
		//echo $this->db->last_query();
		$UserData=$Query->result_array();

		if(!empty($UserData)){

			return $UserData[0]['FranchiseeAssignCount'];

		}else{

			return FALSE;
		}
	}
}

