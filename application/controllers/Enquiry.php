<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Enquiry extends Admin_Controller_Secure 
{
	
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		//echo "<pre>"; print_r($_SESSION); die;	
		

		if(isset($_SESSION['UserData']['UserTypeID']) && ($_SESSION['UserData']['UserTypeID'] == 1 || $_SESSION['UserData']['UserTypeID'] == 10))
		{
			$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
			);

			$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
			);


			$this->load->view('includes/header',$load);
			$this->load->view('includes/menu');
			$this->load->view('enquiry/enquiry_list');
			$this->load->view('includes/footer');
		}
		else
		{
			$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'			
			);


			$load['js']=array(
			'asset/js/enquiry_assigned_to.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
			);

			$this->load->view('includes/header',$load);
			$this->load->view('includes/menu');
			$this->load->view('enquiry/enquiry_assigned_to_list');
			$this->load->view('includes/footer');
		}
	}


	public function assigned()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/enquiry_assigned.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',	
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'

		);

		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('enquiry/enquiry_assigned_list');
		$this->load->view('includes/footer');
	}



	public function churned()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/enquiry_churned.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',	
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'

		);

		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('enquiry/enquiry_churn_list');
		$this->load->view('includes/footer');
	}

}
