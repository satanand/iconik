app.controller('PageController', function ($scope, $http,$timeout){

    /*----------------*/
    $scope.getFilterData = function ()
    {
        var data = 'SessionKey='+SessionKey+'&'+$('#filterPanel form').serialize();
        $http.post(API_URL+'admin/store/getProductFilterData', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */
               $scope.filterData =  response.Data;
               $timeout(function(){
                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");
            }, 300);          
           }
       });
    }


    /*list*/
    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }


    /*list append*/
    $scope.getList = function ()
    {
        if ($scope.data.listLoading || $scope.data.noRecords) return;
        $scope.data.listLoading = true;
        var data = 'SessionKey='+SessionKey+'&StoreGUID='+StoreGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        $http.post(API_URL+'admin/store/getProducts', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) {
                   $scope.data.dataList.push(response.Data.Records[i]);
               }
               $scope.data.pageNo++;               
           }else{
            $scope.data.noRecords = true;
        }
        $scope.data.listLoading = false;
        setTimeout(function(){ tblsort(); }, 1000);
    });
    }



    /*load add form*/
    $scope.loadFormAdd = function (Position, ProductGUID)
    {
        $scope.data.Position = Position;
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL+'admin/store/getProduct', 'SessionKey='+SessionKey+'&StoreGUID='+StoreGUID, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data

                $('#add_model').modal({show:true});
                $timeout(function(){            
                 $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
  tinymce.init({
    selector: '#ProductDesc'
  });
             }, 200);
            }
        });
    }



    /*delete selected */
    $scope.deleteSelectedRecords = function ()
    {
        alertify.confirm('Are you sure you want to delete?', function(){  
            var data = 'SessionKey='+SessionKey+'&'+$('#records_form').serialize();
            $http.post(API_URL+'admin/entity/deleteSelected', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */               
                    SuccessPopup(response.Message);
                    $scope.applyFilter();
                }else{
                    ErrorPopup(response.Message);
                }
                if($scope.data.totalRecords==0){
                   $scope.data.noRecords = true;
               }
           });
        }).set('labels', {ok:'Yes', cancel:'No'});
    }






    /*load edit form*/
    $scope.loadFormEdit = function (Position, ProductGUID)
    {
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL+'admin/store/getProduct', 'SessionKey='+SessionKey+'&StoreGUID='+StoreGUID+'&ProductGUID='+ProductGUID, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                /* hack to remove space - start */
                response.Data.Product.CategoryGUIDs = response.Data.Product.CategoryGUIDs.split(/\s*,\s*/);
                response.Data.Product.NavigationCategoryGUIDs = response.Data.Product.CategoryGUIDs;
                response.Data.Product.CategoriesCategoryGUIDs = response.Data.Product.CategoryGUIDs;
                response.Data.Product.CuisineCategoryGUIDs = response.Data.Product.CategoryGUIDs;
                response.Data.Product.AddOnProductsGUIDs = response.Data.Product.AddOnProductsGUIDs.split(/\s*,\s*/); 
                /* hack to remove space - ends */
                $scope.formData = response.Data

                $('#edit_model').modal({show:true});
                $timeout(function(){            
                 $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
             }, 200);
            }
        });
    }






    /*load delete form*/
    $scope.loadFormDelete = function (Position, ProductGUID)
    {
        $scope.data.Position = Position;
        $scope.templateURLDelete = PATH_TEMPLATE+module+'/delete_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL+'/store/getProduct', 'SessionKey='+SessionKey+'&ProductGUID='+ProductGUID, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#delete_model').modal({show:true});
                $timeout(function(){            
                 $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
             }, 200);
            }
        });
    }

    /*add data*/
    $scope.addData = function ()
    {
        $scope.addDataLoading = true;
tinyMCE.triggerSave();
        var data = 'SessionKey='+SessionKey+'&StoreGUID='+StoreGUID+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'admin/store/addProduct', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                $scope.applyFilter();
                $('.modal-header .close').click();
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });
    }


    /*edit data*/
    $scope.editData = function ()
    {
        $scope.editDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();
        $http.post(API_URL+'admin/store/editProduct', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position] = response.Data;
                $('.modal-header .close').click();
            }else{
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;          
        });
    }



}); 




/* sortable - starts */
function tblsort() {

  var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}

$(".table-sortable tbody").sortable({
    placeholder: 'tr_placeholder',
    helper: fixHelper,
    cursor: "move",
    tolerance: 'pointer',
    axis: 'y',
    dropOnEmpty: false,
    update: function (event, ui) {
      sendOrderToServer();
  }      
}).disableSelection();
$(".table-sortable thead").disableSelection();


function sendOrderToServer() {
    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");
    $.ajax({
        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',
        data: order,
        stop: function(response) {
            if (response.status == "success") {
                window.location.href = window.location.href;
            } else {
                alert('Some error occurred');
            }
        }
    });
}

$("#add_model").on("hidden.bs.modal", function () {
    tinymce.remove();
});

}


/* sortable - ends */
