<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walloffame extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Walloffame_model');
		$this->load->model('Utility_model');
	}



	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
/*	public function getFilterData_post()
	{	
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
		$WallOfFameDataC = $this->Walloffame_model->getCategory('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		$WallOfFameDataA = $this->Walloffame_model->getAuthor('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

	
			$this->Return['Data']['Category'] = $WallOfFameDataC;
			$this->Return['Data']['Author'] = $WallOfFameDataA;
		
	}*/

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getwalloffame_post()
	{
		$this->form_validation->set_rules('Keyword', 'Keyword','trim');
		$this->form_validation->set_rules('Year', 'Year','trim|required');
		$this->form_validation->set_rules('Month', 'Month','trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
    
		$WallOfFameData = $this->Walloffame_model->getwalloffame('',$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);
		if(!empty($WallOfFameData)){
			$this->Return['Data'] = $WallOfFameData['Data'];
		}	
	}




	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function add_post()

	{

		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('MediaCaption', 'Title', 'trim|required');
		$this->form_validation->set_rules('Name', 'Name', 'trim|required');
		$this->form_validation->set_rules('NewsDate', 'Date', 'trim|required|callback_validateDate');
		$this->form_validation->set_rules('MediaContent', 'Content', 'trim|required');
		$this->form_validation->set_rules('MediaGUIDs', 'Upload Image', 'trim');
		//$this->form_validation->set_rules('Year', 'Year', 'trim|required');
		

		$this->form_validation->validation($this);  /* Run validation */		

		//echo $this->Post['MediaGUIDs'];
		
		$WallOfFameData = $this->Walloffame_model->addWallOfFame($this->Post);

		//echo $WallOfFameData;

			if($WallOfFameData == 1){

				$this->Return['Message']      	=	"Wall of Fame Added successfully."; 

			} else {

                $this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Media Title Name Already Exist.";
				
			}


	}


	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategory
	Input (Sample JSON): 		
	*/
	public function getwalloffameBy_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID','trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$WallOfFameData = $this->Walloffame_model->getwalloffameBy('',array("MediaGUID"=>$this->input->post('MediaGUID')));
		if(!empty($WallOfFameData)){
			$this->Return['Data'] = $WallOfFameData;
		}	
	}

	
	/*

	Name: 			editWallOfFame_post
	

	*/
	public function editWalloffame_post()
	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('MediaCaption', 'Title', 'trim|required');
		$this->form_validation->set_rules('Name', 'Name', 'trim|required');
		$this->form_validation->set_rules('NewsDate', 'Date', 'trim|required|callback_validateDate');
		$this->form_validation->set_rules('MediaContent', 'Content', 'trim|required');
		$this->form_validation->set_rules('MediaGUIDe', 'MediaGUIDe', 'trim');
		$this->form_validation->set_rules('MediaGUID', 'MediaGUID', 'trim|required');
		// $this->form_validation->set_rules('Year', 'Year', 'trim|required');
		 

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		

		if(!empty($this->Post['MediaGUIDe'])){

			$this->Walloffame_model->delete($this->input->Post('MediaGUID'));

			$this->Walloffame_model->addWallOfFame($this->Post);
			
			$this->Return['Data']= $this->Walloffame_model->getwalloffameBy('',array("MediaGUID"=>$this->input->Post('MediaGUIDe')));
			
		}else{

			$this->Walloffame_model->editWalloffame($this->Post);
			$this->Return['Data']= $this->Walloffame_model->getwalloffameBy('',array("MediaGUID"=>$this->input->Post('MediaGUID')));
		}
	
		

	    $this->Return['Message']      	=	"Wall Of Fame Updated successfully."; 

	}



	public function getYears_post(){
		$this->form_validation->set_rules('InstituteID', 'Institute ID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */

		$data = $this->Walloffame_model->getYears($this->input->post('InstituteID'));	

		if(!$data){
			$this->Return['Message']      	=	"Sorry! Some error occured."; 
		}else{
			$this->Return['Data']      	=	$data; 
		}
	}



	public function delete_post()
	{
		
		$data=$this->Walloffame_model->delete($this->input->Post('MediaGUID'));

		if(empty($data)){

			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 

		}else{

			$this->Return['Message']      	=	"Successfully Delete"; 
		}
	}

}