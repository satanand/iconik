<html>
<head>
<title>Iconik Technologies LLP</title> 
</head>


<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; font-family: 'Noto Sans', sans-serif;  font-weight: 400;
font-size: 15px;  line-height: 1.8;  color: #6a6a6a; margin: 0 auto !important; padding: 0 !important; height: 100% !important; width: 100% !important; background: #f1f1f1;">

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;   font-family: 'Noto Sans', sans-serif;  font-weight: 400;
font-size: 15px;  line-height: 1.8;  color: rgba(0,0,0,.4); margin: 0 auto !important; padding: 0 !important; height: 100% !important; width: 100% !important; background: #f1f1f1; font-family: 'Noto Sans', sans-serif;">

<center style="width: 100%; background-color: #fff;">
<div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
</div>
<div style="max-width: 600px; margin: 0 auto;" class="email-container">
<!-- BEGIN BODY -->
<table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto; border: 1px solid rgba(144,144,144,0.6); background: #f7f7f7; border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important;">
<tr>
<td valign="top" style="padding: 0.5em 2.5em 0 2.5em; border-bottom: 1px solid rgba(0,0,0,.10); color: rgba(0,0,0,.5);mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;">


<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<tr>
<td class="logo" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
  
<?php
if(isset($InstituteProfilePic) && !empty($InstituteProfilePic))
{ 
?>

  <a href="https://iconik.in/" style="text-decoration: none;">

  <a href="<?php echo site_url()?>" style="text-decoration: none;">

    <img src="<?php echo $InstituteProfilePic;?>" width="100px" alt="<?php echo strtoupper(SITE_NAME) ?>">
  </a>
<?php 
}
else
{
?>

    <a href="https://iconik.in/" style="text-decoration: none;">

    <a href="<?php echo site_url()?>" style="text-decoration: none;">

      <img src="https://mp.iconik.in/marketplace/asset/img/email_template_icons/logo.png" width="100px" alt="<?php echo strtoupper(SITE_NAME) ?>">
    </a>
<?php  
}
?>
</td>
</tr>
</table>

</td>
</tr><!-- header end tr -->




<td class="email-section" style="padding: 0; width: 100%; background: #fff; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<tr>
<td valign="middle" width="100%" style="mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style=" border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important;">
<tr>
<td class="text-services" style="text-align: left; padding: 20px 30px; color:#6a6a6a; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">

<?php
if (!empty($HTML))
{
  echo $HTML; /*main containt*/
}
?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr><!-- middle end: tr -->



<tr>
<td valign="middle" width="100%" align="center" style="border-top: 1px solid rgba(0,0,0,.10);  color: rgba(0,0,0,.5); background: #f7f7f7; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<table width="100%" align="center" style=" border-spacing: 0 !important; border-collapse: collapse !important; table-layout: fixed !important; margin: 0 auto !important; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">

<?php
if(isset($Signature) && !empty($Signature))
{
  echo $Signature;
}
else
{
?>
<tr style="text-align: center;">
<td valign="middle" width="100%" style="padding-top: 20px; text-align: center; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">

<ul class="social" style=" margin: 0;  padding: 0;">
<li style=" list-style: none;  display: inline-block; margin-right: 10px;"><a href="https://www.facebook.com/Iconik-Technologies-LLP-484685462300270/" style="color: rgba(0,0,0,1); text-decoration: none;"><img src="https://mp.iconik.in/marketplace/asset/img/email_template_icons/fb.png" alt="" style="width: 24px; max-width: 600px; height: auto; display: block;"></a></li>
<li style=" list-style: none;  display: inline-block; margin-right: 10px;"><a href="https://twitter.com/Iconiktech" style="color: rgba(0,0,0,1); text-decoration: none;"><img src="https://mp.iconik.in/marketplace/asset/img/email_template_icons/twitter.png" alt="" style="width: 24px; max-width: 600px; height: auto; display: block;"></a></li>
<li style=" list-style: none;  display: inline-block; margin-right: 10px;"><a href="https://www.linkedin.com/in/iconik-technologies-llp-we-help-you-73418018a/"><img src="https://mp.iconik.in/marketplace/asset/img/email_template_icons/linkdin.png" alt="" style="width: 24px; max-width: 600px; height: auto; display: block; text-decoration: none; color: rgba(0,0,0,1);">
</a></li>
<li style=" list-style: none; display: inline-block;margin-right: 10px;"><a href="https://www.instagram.com/iconiktechnology/"><img src="https://mp.iconik.in/marketplace/asset/img/email_template_icons/instagram.png" alt="" style="width: 24px; max-width: 600px; height: auto; display: block; color: rgba(0,0,0,1); text-decoration: none;" ></a></li>
</ul>
</td>
</tr>

<tr>
<td valign="middle" width="60%" style="text-align: center; mso-table-lspace: 0pt !important; mso-table-rspace: 0pt !important;">
<h3 class="heading" style="font-weight: 700; color: #1A4475; line-height: 1.3; font-size: 20px; margin-top: 0;">Iconik Technologies LLP</h3>
<div style="margin-top:-3%; color: rgba(0,0,0,.8); line-height: 1.3;">#615, Apollo Premier, Vijay Nagar <br>Indore (MP), 452010, India</div>
<div style="color: rgba(0,0,0,.8); line-height: 1.3;">Tel: +91.0731-4989666   <span style="font-size: 20px;">|</span>   eMail: contact@iconik.in</div>
</td>
</tr>
<?php
}
?>
</table>
</td>
</tr><!-- footer end: tr -->
</table>

</div>
</center>
</body>
</html>