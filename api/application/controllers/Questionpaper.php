<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionpaper extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Questionpaper_model');
	}

 
	/*
	Description: 	Use to get Get Questions count with subject and course.
	URL: 			/api/QuestionPaper/getQuestionPaperStatistics
	Input (Sample JSON): 		
	*/
	public function getQuestionPaperStatistics_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		if($this->input->post('SubjectGUID') != ""){
			$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}else if($this->input->post('CourseGUID') != ""){
			$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}

		$this->form_validation->validation($this);
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		if($this->input->post('SubjectGUID') != ""){			
			$QuestionPaperData = $this->Questionpaper_model->getQuestionPaperStatistics($this->EntityID,$this->CategoryID);
		}else if($this->input->post('CourseGUID') != ""){
			$QuestionPaperData = $this->Questionpaper_model->getQuestionPaperStatistics($this->EntityID,'',$this->CategoryID);
		}else{
			$QuestionPaperData = $this->Questionpaper_model->getQuestionPaperStatistics($this->EntityID);
		}
		
		if(!empty($QuestionPaperData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $QuestionPaperData;
		}	
	}


	/*
	Name: 			getPosts
	Description: 	Use to get list of question paper.
	URL: 			/api/questionPaper/getQuestionsPaperList
	*/
	public function getQuestionsPaperList_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		if($this->input->post('SubjectGUID') != ""){
			$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}else if($this->input->post('CourseGUID') != ""){
			$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		}	
		$this->form_validation->set_rules('QuestionsGroup[]', 'Question Group', 'trim');
		$this->form_validation->set_rules('QuestionsLevel', 'Question Level', 'trim');
		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Popular,Saved,MyPosts]');
		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		if($this->input->post('SubjectGUID') != ""){
			$SubjectID = $this->CategoryID;
			$CourseID = "";
		}else if($this->input->post('CourseGUID') != ""){
			$CourseID = $this->CategoryID;
			$SubjectID = "";
		}else{
			$CourseID = "";
			$SubjectID = "";
		}	


		$fields = array('QtPaperID','QtPaperGUID','CourseID','SubjectID','QtPaperTitle','TotalQuestions','QuestionsGroup','QuestionsLevelEasy','QuestionsLevelModerate','QuestionsLevelHigh','PassingMarks','NegativeMarks','tbl_entity.StatusID');

		$Questions=$this->Questionpaper_model->getQuestionsPaperList($fields,array(
				'SessionUserID'	=>	$this->SessionUserID,
				'EntityID'		=>	(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),
				'CourseID'		=>  $CourseID,
				'SubjectID'		=>	$SubjectID,
				'QuestionsGroup' =>	@$this->Post['QuestionsGroup'],
				'QuestionsLevel' =>	@$this->Post['QuestionsLevel'],
				'Keyword' =>	@$this->Post['Keyword'],
				'PageName' =>	@$this->Post['PageName']
			), TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);
		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}


	/*
	Name: 			getPosts
	Description: 	Use to get single question paper by paper ID
	URL: 			/api/questionPaper/getQuestionsPaperByID
	*/
	public function getQuestionsPaperByID_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');	
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$fields = array('QtPaperGUID','CourseID','SubjectID','QtPaperTitle','TotalQuestions','QuestionsGroup','QuestionsLevelEasy','QuestionsLevelModerate','QuestionsLevelHigh','PassingMarks','NegativeMarks');

		$Questions=$this->Questionpaper_model->getQuestionsPaperByID((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtPaperID,$fields);
		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}



	/*
	Name: 			getPosts
	Description: 	Use to get questionAnswer
	URL: 			/api/questionAnswer/getQuestionAnswerList
	*/
	public function getQuestionAnswerList_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');	
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$fields = array('QtPaperGUID','CourseID','SubjectID','QtPaperTitle','TotalQuestions','QuestionsGroup','QuestionsLevelEasy','QuestionsLevelModerate','QuestionsLevelHigh','PassingMarks','NegativeMarks','QuestionsID','EasyLevelQuestionsID','ModerateLevelQuestionsID','HighLevelQuestionsID','Instruction');

		$Questions=$this->Questionpaper_model->getQuestionAnswerList((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtPaperID,$fields);
		if($Questions){
			$this->Return['Data'] = $Questions['Data'];
		}
	}

	/*
	Name: 			getPosts
	Description: 	Use to get count of question by category and question group
	URL: 			/api/post/getQuestionsCountByCategory
	*/
	public function getQuestionsCountByCategory_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		//if($this->input->post('SubjectGUID') != ""){
		$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		// }else if($this->input->post('CourseGUID') != ""){
		// 	$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		// }	
		
		$this->form_validation->validation($this);  /* Run validation */	

		//echo $this->CategoryID;	
		/* Validation - ends */
		$this->Return['Data'] = array();
		if(!empty($this->input->post('SubjectGUID'))){
			if(!empty($this->input->post('QuestionGroup'))){
				$Questions=$this->Questionpaper_model->getQuestionsCountByCategory((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID,$this->input->post('QuestionGroup'));
				if($Questions){
					$this->Return['Data'] = $Questions;
				}
			}else{
				$Questions=$this->Questionpaper_model->getQuestionsCountByCategory((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID);
				if($Questions){
					$this->Return['Data'] = $Questions;
				}
			}		
		}else{
			$this->Return['Data'] = array('Easy'=>0,'Moderate'=>0,'High'=>0,'MockTest'=>0,'Quiz'=>0,'Test'=>0,'Contest'=>0,'Total'=>0);
		}
		
	}


	/*
	Name: 			getPosts
	Description: 	Use to get count of question by category and question group
	URL: 			/api/post/getQuestionsCountOnEdit
	*/
	public function getQuestionsCountOnEdit_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		//if($this->input->post('SubjectGUID') != ""){
		$this->form_validation->set_rules('SubjectGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		// }else if($this->input->post('CourseGUID') != ""){
		// 	$this->form_validation->set_rules('CourseGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');
		// }	

		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		
		$this->form_validation->validation($this);  /* Run validation */	

		//echo $this->CategoryID;	
		/* Validation - ends */
		$this->Return['Data'] = array();
		if(!empty($this->input->post('SubjectGUID'))){
			if(!empty($this->input->post('QuestionGroup'))){
				$Questions=$this->Questionpaper_model->getQuestionsCountOnEdit((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID,$this->input->post('QuestionGroup'),$this->QtPaperID);
				if($Questions){
					$this->Return['Data'] = $Questions;
				}
			}else{
				$Questions=$this->Questionpaper_model->getQuestionsCountOnEdit((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->CategoryID,"",$this->QtPaperID);
				if($Questions){
					$this->Return['Data'] = $Questions;
				}
			}		
		}else{
			$this->Return['Data'] = array('Easy'=>0,'Moderate'=>0,'High'=>0,'MockTest'=>0,'Quiz'=>0,'Test'=>0,'Contest'=>0,'Total'=>0);
		}
		
	}


	/*
	Name: 			posts
	Description: 	Use to add new Question paper
	URL: 			/api/post	
	*/
	public function createQusetionPaper_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		
		$this->form_validation->set_rules('QtPaperTitle', 'Title', 'trim|required'); 

		$this->form_validation->set_rules('QuestionsGroup', 'Question Group', 'required');

		$this->form_validation->set_rules('TotalQuestions', 'Total Question', 'trim|required');

		$this->form_validation->set_rules('PassingMarks', 'Passing Percentage (%)', 'trim|required');

		$this->form_validation->set_rules('QuestionsLevelEasy', 'Easy', 'trim|required');

		$this->form_validation->set_rules('QuestionsLevelModerate', 'Moderate', 'trim|required');

		$this->form_validation->set_rules('QuestionsLevelHigh', 'High', 'trim|required');

		if(!empty($this->input->post('QuestionsGroup')) && $this->input->post('QuestionsGroup') == 5){
			$this->form_validation->set_rules('StartDateTime', 'StartDateTime', 'trim|required');

			$this->form_validation->set_rules('EndDateTime', 'End Date Time', 'trim|required');
		}else{
			$this->form_validation->set_rules('StartDateTime', 'Start Date Time', 'trim');

			$this->form_validation->set_rules('EndDateTime', 'EndDateTime', 'trim');
		}

		$this->form_validation->set_rules('NegativeMarks1', 'Negative Mark', 'trim');

		$this->form_validation->set_rules('NegativeMarks2', 'Negative Mark', 'trim');

		$this->form_validation->set_rules('Instruction', 'Instruction', 'trim');		
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 

		$NegativeMarks1 = $this->Post['NegativeMarks1'];
		$NegativeMarks2 = $this->Post['NegativeMarks2'];

		$NegativeMarks = $NegativeMarks1+$NegativeMarks2; //echo $NegativeMarks; die;
		if($NegativeMarks != 0){
			$NegativeMarks = '-'.$NegativeMarks;
		}

		$QPaperData = $this->Questionpaper_model->createQusetionPaper($this->SessionUserID, $this->EntityID, array(
			"SubjectID"	=>	$this->CategoryID,		
			"QuestionsGroup"	=>	$this->Post['QuestionsGroup'],
			"QtPaperTitle"	=>	$this->Post['QtPaperTitle'],
			"TotalQuestions"	=>	$this->Post['TotalQuestions'],
			"PassingMarks"	=>	$this->Post['PassingMarks'],
			"QuestionsLevelEasy"	=>	$this->Post['QuestionsLevelEasy'],
			"QuestionsLevelModerate"	=>	$this->Post['QuestionsLevelModerate'],
			"QuestionsLevelHigh"	=>	$this->Post['QuestionsLevelHigh'],
			"NegativeMarks"	=>	$NegativeMarks,
			"Instruction"	=>	@$this->Post['Instruction'],
			"StartDateTime"	=>	@$this->Post['StartDateTime'],
			"EndDateTime"	=>	@$this->Post['EndDateTime']
		));

		//print_r($QPaperData); die;
		if($QPaperData){			

			$this->Return['Data']['QtPaperGUID'] = $QPaperData['QtPaperGUID'];

		}
	}


	/*
	Name: 			posts
	Description: 	Use to add new Question paper
	URL: 			/api/post	
	*/
	public function editQusetionPaper_post()
	{
		/* Validation section */
		//print_r($this->Post); die;
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('CategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');

		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper ID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		
		$this->form_validation->set_rules('QtPaperTitle', 'Title', 'trim|required'); 

		$this->form_validation->set_rules('QuestionsGroup', 'Question Group', 'required');

		$this->form_validation->set_rules('TotalQuestions', 'Total Question', 'trim|required');

		$this->form_validation->set_rules('PassingMarks', 'Passing Percentage (%)', 'trim|required');

		$this->form_validation->set_rules('QuestionsLevelEasy', 'Easy', 'trim|required');

		$this->form_validation->set_rules('QuestionsLevelModerate', 'Moderate', 'trim|required');

		$this->form_validation->set_rules('QuestionsLevelHigh', 'High', 'trim|required');

		$this->form_validation->set_rules('NegativeMarks1', 'Negative Mark', 'trim');

		$this->form_validation->set_rules('NegativeMarks2', 'Negative Mark', 'trim');

		$this->form_validation->set_rules('Instruction', 'Instruction', 'trim');
		
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		/* Define section */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		/* Define section - ends */ 

		$NegativeMarks1 = $this->Post['NegativeMarks1'];
		$NegativeMarks2 = $this->Post['NegativeMarks2'];

		$NegativeMarks = $NegativeMarks1+$NegativeMarks2; //echo $NegativeMarks; die;
		if($NegativeMarks != 0){
			$NegativeMarks = '-'.$NegativeMarks;
		}

		$QPaperData = $this->Questionpaper_model->editQusetionPaper($this->SessionUserID, $this->EntityID, array(
			"QtPaperID"	=>	$this->QtPaperID,
			"SubjectID"	=>	$this->CategoryID,		
			"QuestionsGroup"	=>	$this->Post['QuestionsGroup'],
			"QtPaperTitle"	=>	$this->Post['QtPaperTitle'],
			"TotalQuestions"	=>	$this->Post['TotalQuestions'],
			"PassingMarks"	=>	$this->Post['PassingMarks'],
			"QuestionsLevelEasy"	=>	$this->Post['QuestionsLevelEasy'],
			"QuestionsLevelModerate"	=>	$this->Post['QuestionsLevelModerate'],
			"QuestionsLevelHigh"	=>	$this->Post['QuestionsLevelHigh'],
			"NegativeMarks"	=>	$NegativeMarks,
			"Instruction"	=>	@$this->Post['Instruction']
		));

		if($QPaperData){	

			$fields = array('QtPaperGUID','CourseID','SubjectID','QtPaperTitle','TotalQuestions','QuestionsGroup','QuestionsLevelEasy','QuestionsLevelModerate','QuestionsLevelHigh','PassingMarks','NegativeMarks','tbl_entity.StatusID');

			$Questions=$this->Questionpaper_model->getQuestionsPaperByID($this->EntityID,$this->QtPaperID,$fields);		

			$this->Return['Data'] = $Questions['Data'];

		}
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function addBlockQuestion_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->set_rules('addNext', 'addNext', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */	
		/* Validation - ends */
		$data = $this->Questionpaper_model->addBlockQuestion($this->SessionUserID, $this->QtPaperID, $this->QtBankID, $this->input->post('addNext'));
		if(is_array($data) && array_key_exists('message', $data)){
			$this->Return['ResponseCode'] 	=	200;
			if(!empty($data['QtBankID'])){
				$this->Return['Data'] 	=	array('QtBankID' => $data['QtBankID'], 'QuestionsLevel' => $data['QuestionsLevel']);
			}		
			$this->Return['Message']      	=	$data['message']; 
		}else if(!$data){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to remove it."; 
		}else{
			$this->Return['Data'] = $data;
		}
	}


	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function replaceQuestion_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->validation($this);  /* Run validation */	
		/* Validation - ends */
		$data = $this->Questionpaper_model->replaceQuestion($this->SessionUserID, $this->QtPaperID, $this->QtBankID, $this->input->post('addQtBankID'),$this->input->post('QuestionsLevel'));
		if(!$data){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Sorry! Some error occured."; 
		}else{
			$data = $this->Questionpaper_model->getQuestionAnswerByID($this->SessionUserID, $this->input->post('addQtBankID'));
			$this->Return['Data'] = $data;
		}
	}



	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function addAssignPaper_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtPaperGUID', 'Question Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('BatchGUID', 'Batch','trim|required|callback_validateEntityGUID[Batch,BatchID]');
		//$this->form_validation->set_rules('CategoryGUID', 'Subject', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');	
		$this->form_validation->set_rules('Date', 'Date', 'trim|required');	

		$this->form_validation->set_rules('ActivatedFromTime', 'Activated From Time', 'trim|required');
		$this->form_validation->set_rules('ActivatedToTime', 'Activated To Time', 'trim|required');

		$this->form_validation->set_rules('StartTime', 'START DATE TIME', 'trim|required');
		$this->form_validation->set_rules('EndTime', 'END DATE TIME', 'trim|required');


		$this->form_validation->validation($this);  /* Run validation */	
		/* Validation - ends */
		//print_r($_POST); die;
		$assigned = $this->Questionpaper_model->addAssignPaper($this->SessionUserID, array(
			"QtPaperID" =>	$this->QtPaperID,
			"SubjectID" 		=>	$this->CategoryID,
			"QtAssignBatch" =>	@$this->BatchID,
			"Date" 		=>	$this->input->post('Date'),
			"StartTime" 		=>	$this->input->post('StartTime'),
			"EndTime" 		=>	$this->input->post('EndTime'),
			"ActivatedFrom" 		=>	$this->input->post('ActivatedFromTime'),
			"ActivatedTo" 		=>	$this->input->post('ActivatedToTime')
			));

		if($assigned){
			if(in_array('exist', $assigned)){
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	$assigned[1]; 
			}else{
				$this->Return['Data']['QtAssignGUID'] = $assigned['QtAssignGUID'];
			}
		}
	}


	/*
	Name: 			getPosts
	Description: 	Use to get list of post.
	URL: 			/api/post/change entity status
	*/
	public function changeStatus_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$Questions=$this->Questionpaper_model->changeStatus((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtPaperID,$this->input->post('StatusID'));

		if($Questions){
			$fields = array('QtPaperID','QtPaperGUID','CourseID','SubjectID','QtPaperTitle','TotalQuestions','QuestionsGroup','QuestionsLevelEasy','QuestionsLevelModerate','QuestionsLevelHigh','PassingMarks','NegativeMarks','tbl_entity.StatusID');

			$Questions=$this->Questionpaper_model->getQuestionsPaperList($fields,array(
					'EntityID'	   =>	(empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),
					'QtPaperID'    =>  $this->QtPaperID
				));

			if($Questions){
				$this->Return['Data'] = $Questions['Data'];
			}
		}
	}


	public function removeQuestion_post(){
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('QtBankGUID', 'QtBankGUID', 'trim|required|callback_validateEntityGUID[Question Bank,QtBankID]');
		$this->form_validation->set_rules('check', 'check', 'trim|required');
		$this->form_validation->validation($this);

		$Questions=$this->Questionpaper_model->removeQuestion($this->QtBankID,$this->QtPaperID,$this->input->post('check'));
	}

	/*
	Name: 			delete post
	Description: 	Use to delete post by owner.
	URL: 			/api/post/save
	*/
	public function delete_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->validation($this);  /* Run validation */	
		//echo "fdsfds";	
		/* Validation - ends */
		if(!$this->Questionpaper_model->deletePost($this->SessionUserID, $this->QtPaperID)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You do not have permission to delete it."; 
		}
	}


	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function getSubjectByBatch_post()
	{
		$this->form_validation->set_rules('BatchGUID', 'BatchGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$SubjectData = $this->Questionpaper_model->getSubjectByBatch($this->Post['BatchGUID']);

		$this->Return['Data'] = $SubjectData['Data'];

	}


	public function getBatchByPaper_post()
	{
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		$BatchData = $this->Questionpaper_model->getBatchByPaper($this->Post['QtPaperGUID']);
		$this->Return['Data'] = $BatchData['Data'];
	}


	/*

	Name: 			saveTestResults

	Description: 	Use to save test results

	URL: 			/questionpaper/saveTestResults/	

	*/

	public function saveTestResults_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');

		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|callback_validateEntityGUID[Question Assign,QtAssignID]');	

		$this->form_validation->set_rules('QuestionGroup', 'QuestionGroup', 'trim');
		$this->form_validation->set_rules('QtPaperTitle', 'QtPaperTitle', 'trim|required');

		$this->form_validation->set_rules('TotalMarks', 'TotalMarks', 'trim|required');
		$this->form_validation->set_rules('SkipQuestions', 'SkipQuestions', 'trim|required');	
		$this->form_validation->set_rules('AttemptQuestions', 'AttemptQuestions', 'trim|required');	
		$this->form_validation->set_rules('CorrectAnswers', 'CorrectAnswers', 'trim|required');			
		$this->form_validation->set_rules('WrongAnswers', 'WrongAnswers', 'trim|required');	
		$this->form_validation->set_rules('GainedMarks', 'GainedMarks', 'trim|required');
		$this->form_validation->set_rules('ResultData', 'ResultData', 'trim|required');	

		$this->form_validation->set_rules('AttemptStartTime', 'AttemptStartTime', 'trim');	
		$this->form_validation->set_rules('AttemptEndTime', 'ResultData', 'trim');				
		$this->form_validation->validation($this);  /* Run validation */		
		
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		//print_r($this->input->Post()); die;
		$SubjectData = $this->Questionpaper_model->saveTestResults($this->EntityID,$this->QtPaperID,$this->input->post(),$this->QtAssignID);

		//$this->Return['Data'] = $SubjectData['Data'];

	}



	/*

	Name: 			getTestResults

	Description: 	Use to get test results

	URL: 			/questionpaper/getTestResults/	

	*/

	public function getTestResults_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|callback_validateEntityGUID[Question Assign,QtAssignID]');

		$this->form_validation->set_rules('CheckPaperData', 'CheckPaperData', 'trim');		 
		$this->form_validation->set_rules('QuestionsGroup', 'QuestionsGroup', 'trim');	
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		//print_r($this->input->post()); die;
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		//print_r($this->input->Post()); die;
		//$SubjectData = $this->Questionpaper_model->getTestResults($this->EntityID,array("QtPaperID"=>@$this->QtPaperID,"CheckPaperData"=>@$this->input->post('CheckPaperData'),"QuestionsGroup"=>@$this->input->post('QuestionsGroup'),"Keyword"=>@$this->input->post('Keyword'), "QtAssignID"=>@$this->QtAssignID),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		$SubjectData = $this->Questionpaper_model->getTestResults($this->EntityID,array("QtPaperID"=>@$this->QtPaperID,"QtAssignID"=>@$this->QtAssignID,"CheckPaperData"=>@$this->input->post('CheckPaperData'),"QuestionsGroup"=>@$this->input->post('QuestionsGroup'),"Keyword"=>@$this->input->post('Keyword')),TRUE,  @$this->Post['PageNo'], @$this->Post['PageSize']);

		$this->Return['Data'] = $SubjectData['Data'];
		if(!empty($SubjectData['Data'])){
		}else{
			$this->Return['Message'] = "Data not found";
		}
	}


	/*

	Name: 			getResultPaper

	Description: 	Use to get Result Paper

	URL: 			/questionpaper/getResultPaper/	

	*/

	public function getResultPaper_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('ResultGUID', 'ResultGUID', 'trim|required');	
		$this->form_validation->set_rules('CheckPaperData', 'CheckPaperData', 'trim');	
		$this->form_validation->validation($this);  /* Run validation */		
		//print_r($this->input->post()); die;
		/* Validation - ends */
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		//print_r($this->input->Post()); die;
		$SubjectData = $this->Questionpaper_model->getTestResults($this->EntityID,array('ResultGUID'=>$this->input->post('ResultGUID'),'CheckPaperData'=>@$this->input->post('CheckPaperData')));

		$this->Return['Data'] = $SubjectData['Data'];

	}

 

	public function processPracticeTestForm_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|callback_validateEntityGUID[Question Paper,QtPaperID]');	
		$this->form_validation->set_rules('QtAssignGUID', 'QtAssignGUID', 'trim|callback_validateEntityGUID[Question Assign,QtAssignID]');		
		$this->form_validation->validation($this);  /* Run validation */		
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$id = $this->Questionpaper_model->processPracticeTestForm($this->EntityID, $this->Post);

		//echo $id; die;

		if($id)
		{
			$this->Return['Data'] = $id;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = $this->Post['QtPaperTitle'];
		}
		else
		{
			$this->Return['Data'] = 0;
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Please check test and try again.";
		}	
	}



	public function getTestsResult_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('ResultID', 'ResultID', 'trim');		
		$this->form_validation->validation($this);  /* Run validation */		
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Questionpaper_model->getTestsResult($this->EntityID, $this->Post);

		if($data)
		{	
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['Data'] = 0;
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Invalid request.";
		}	
	}


	public function getTestsHistoryLog_post()
	{  
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		
		$this->form_validation->validation($this);  /* Run validation */		
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Questionpaper_model->getTestsHistoryLog($this->EntityID, $this->Post);

		if($data) 
		{	
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['Data'] = 0;
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found.";
		}	
	}


	public function getScholarshipsApplicants_post()
	{  
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
				
		$this->form_validation->validation($this);  /* Run validation */		
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$data = $this->Questionpaper_model->getScholarshipsApplicants($this->EntityID, $this->Post);

		if($data) 
		{	
			$this->Return['Data'] = $data;
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
		}
		else
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found.";
		}	
	}


	/*-----------------------------------------------------------------------
    Scholarship Module List
    -----------------------------------------------------------------------*/
	public function getQuestionsCount_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('QuestionsGroup', 'Questions Group', 'trim|required');
		
		$this->form_validation->validation($this);	
		
		$EntityID = empty($this->EntityID) ? $this->SessionUserID : $this->EntityID;

		$Data = $this->Questionpaper_model->getQuestionsCount($EntityID, $this->Post);
		
		if($Data)
		{
			$this->Return['Data'] = $Data;
		}
	}

	public function getScholarshipList_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('filterFromDate', 'Apply From Date', 'trim');
		$this->form_validation->set_rules('filterToDate', 'Apply To Date', 'trim');		
		$this->form_validation->set_rules('filterKeyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);	
		
		$EntityID = empty($this->EntityID) ? $this->SessionUserID : $this->EntityID;

		$Data = $this->Questionpaper_model->getScholarshipList($EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);
		
		if($Data)
		{
			$this->Return['Data'] = $Data;
		}
	}


	public function ScholarshipTestSave_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		
		$this->form_validation->set_rules('SPaperTitle', 'Title', 'trim|required');				
		$this->form_validation->set_rules('ScheduleDate', 'Schedule Date', 'trim|required');
		$this->form_validation->set_rules('ScheduleDuration', 'Test Duration', 'trim|required');
		$this->form_validation->set_rules('ScheduleType', 'Schedule Time', 'trim|required');	
						
		$this->form_validation->validation($this);	


		if($this->Post['ScheduleType'] == 1)
		{
			$this->form_validation->set_rules('ActivatedFromTime', 'Active From Time', 'trim|required');
			$this->form_validation->set_rules('ActivatedToTime', 'Active Upto Time', 'trim|required');
			$this->form_validation->validation($this);
		}
		elseif($this->Post['ScheduleType'] == 2)
		{
			$this->form_validation->set_rules('StartTime', 'Start Time', 'trim|required');
			$this->form_validation->validation($this);
		}

		
		$date = date("Y-m-d");
		$time = time();		
		$ScheduleDate = date("Y-m-d", strtotime($this->Post['ScheduleDate']));
		
		if($ScheduleDate < $date)
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Schedule Date should be greater than Current Date.";
			die;
		}

		if($this->Post['ScheduleType'] == 1)
		{
			$this->Post['StartTime'] = "";

			$AFT = strtotime($this->Post['ActivatedFromTime']);
			$ATT = strtotime($this->Post['ActivatedToTime']);

			if($AFT >= $ATT)
			{
				$this->Return['Data'] = array();
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Active From Time should be less than Active Upto Time.";
				die;
			}
			elseif($AFT < $time && $ScheduleDate == $date)
			{
				$this->Return['Data'] = array();
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Active From Time should be ahead of Current Time.";
				die;			
			}
		}
		elseif($this->Post['ScheduleType'] == 2)
		{
			$this->Post['ActivatedFromTime'] = $this->Post['ActivatedToTime'] = "";

			$ST = strtotime($this->Post['StartTime']);

			if($ST < $time && $ScheduleDate == $date)
			{
				$this->Return['Data'] = array();
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Start Time should be ahead of Current Time.";
				die;			
			}
		}		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);		

		$QPaperData = $this->Questionpaper_model->ScholarshipTestSave($this->EntityID, $this->Post);
		
		if($QPaperData)
		{
			$this->Return['Data']['QtPaperGUID'] = $QPaperData['QtPaperGUID'];
			$this->Return['Message'] = "Saved successfully.";
		}
	}

	public function ScholarshipTestSaveEdit_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('QtPaperID', 'Paper ID', 'trim|required');

		$this->form_validation->set_rules('SPaperTitle', 'Title', 'trim|required');				
		$this->form_validation->set_rules('ScheduleDate', 'Schedule Date', 'trim|required');
		$this->form_validation->set_rules('ScheduleDuration', 'Test Duration', 'trim|required');
		$this->form_validation->set_rules('ScheduleType', 'Schedule Time', 'trim|required');	
				
		$this->form_validation->validation($this);	
		
		if($this->Post['ScheduleType'] == 1)
		{
			$this->form_validation->set_rules('ActivatedFromTime', 'Active From Time', 'trim|required');
			$this->form_validation->set_rules('ActivatedToTime', 'Active Upto Time', 'trim|required');
			$this->form_validation->validation($this);
		}
		elseif($this->Post['ScheduleType'] == 2)
		{
			$this->form_validation->set_rules('StartTime', 'Start Time', 'trim|required');
			$this->form_validation->validation($this);
		}

		
		$date = date("Y-m-d");
		$time = time();		
		$ScheduleDate = date("Y-m-d", strtotime($this->Post['ScheduleDate']));
		
		if($ScheduleDate < $date)
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Schedule Date should be greater than Current Date.";
			die;
		}

		if($this->Post['ScheduleType'] == 1)
		{
			$this->Post['StartTime'] = "";
			
			$AFT = strtotime($this->Post['ActivatedFromTime']);
			$ATT = strtotime($this->Post['ActivatedToTime']);

			if($AFT >= $ATT)
			{
				$this->Return['Data'] = array();
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Active From Time should be less than Active Upto Time.";
				die;
			}
			elseif($AFT < $time && $ScheduleDate == $date)
			{
				$this->Return['Data'] = array();
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Active From Time should be ahead of Current Time.";
				die;			
			}
		}
		elseif($this->Post['ScheduleType'] == 2)
		{
			$this->Post['ActivatedFromTime'] = $this->Post['ActivatedToTime'] = "";

			$ST = strtotime($this->Post['StartTime']);

			if($ST < $time && $ScheduleDate == $date)
			{
				$this->Return['Data'] = array();
				$this->Return['ResponseCode'] = 500;
				$this->Return['Message'] = "Start Time should be ahead of Current Time.";
				die;			
			}
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);		

		$QPaperData = $this->Questionpaper_model->ScholarshipTestSaveEdit($this->EntityID, $this->Post);
		
		if($QPaperData === "NotFound")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Paper details not found in the system.";
			die;
		}
		elseif($QPaperData === "TestGiven")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Test attempt by the user, So you can not edit test.";
			die;
		}
		else
		{	
			$this->Return['Data'] = "";
			$this->Return['Message'] = "Saved successfully.";
		}
	}


	public function getScholarshipApplicants_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('filterFromDate', 'Apply From Date', 'trim');
		$this->form_validation->set_rules('filterToDate', 'Apply To Date', 'trim');		
		$this->form_validation->set_rules('filterKeyword', 'Search Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);	
		
		$EntityID = empty($this->EntityID) ? $this->SessionUserID : $this->EntityID;

		$Data = $this->Questionpaper_model->getScholarshipApplicants($EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);
		
		if($Data)
		{
			$this->Return['Data'] = $Data;
		}
	}


	public function GeneratePaperSave_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		
		$this->form_validation->set_rules('QtPaperID', 'Test Name', 'trim|required');			
		$this->form_validation->set_rules('TotalQuestions', 'Total Questions', 'trim|required');
		$this->form_validation->set_rules('PassingMarks', 'Passing Percentage (%)', 'trim|required');
		$this->form_validation->set_rules('QuestionsLevelEasy', 'Easy', 'trim');
		$this->form_validation->set_rules('QuestionsLevelModerate', 'Moderate', 'trim');
		$this->form_validation->set_rules('QuestionsLevelHigh', 'High', 'trim');		
		$this->form_validation->set_rules('NegativeMarks1', 'Negative Mark', 'trim');
		$this->form_validation->set_rules('NegativeMarks2', 'Negative Mark', 'trim');
		$this->form_validation->set_rules('Instruction', 'Instruction', 'trim');		
		
		$this->form_validation->validation($this);

		
		if($this-> Post['QuestionsLevelEasy'] == 0 && $this-> Post['QuestionsLevelModerate'] == 0 && $this-> Post['QuestionsLevelHigh'] == 0)
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Please select atleast one Question Level.";
			die;
		}		
		elseif($this-> Post['HdnTotalQuestions'] < $this-> Post['TotalQuestions'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Total questions is greater than available. Please decrease Total questions count.";
			die;
		}
		elseif($this-> Post['HdnQuestionsLevelEasy'] < $this-> Post['QuestionsLevelEasy'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Easy questions is greater than available. Please decrease Easy questions count.";
			die;
		}
		elseif($this-> Post['HdnQuestionsLevelModerate'] < $this-> Post['QuestionsLevelModerate'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Moderate questions is greater than available. Please decrease Moderate questions count.";
			die;
		}
		elseif($this-> Post['HdnQuestionsLevelHigh'] < $this-> Post['QuestionsLevelHigh'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "High questions is greater than available. Please decrease High questions count.";
			die;
		}
		elseif($this-> Post['TotalQuestions'] < $this-> Post['QuestionsLevelEasy'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Easy questions is greater than Total questions.";
			die;
		}
		elseif($this-> Post['TotalQuestions'] < $this-> Post['QuestionsLevelModerate'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Moderate questions is greater than Total questions available.";
			die;
		}
		elseif($this-> Post['TotalQuestions'] < $this-> Post['QuestionsLevelHigh'])
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "High questions is greater than Total questions available.";
			die;
		}
		elseif($this-> Post['TotalQuestions'] < ($this-> Post['QuestionsLevelEasy'] + $this-> Post['QuestionsLevelModerate'] + $this-> Post['QuestionsLevelHigh']))
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Count of total question level is greater than Total questions.";
			die;
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		

		$NegativeMarks1 = $this->Post['NegativeMarks1'];
		$NegativeMarks2 = $this->Post['NegativeMarks2'];

		$NegativeMarks = $NegativeMarks1+$NegativeMarks2;
		if($NegativeMarks != 0)
		{
			$NegativeMarks = '-'.$NegativeMarks;
		}
		$this->Post['NegativeMarks'] = $NegativeMarks;

		$this-> Post['TotalQuestions'] = ($this-> Post['QuestionsLevelEasy'] + $this-> Post['QuestionsLevelModerate'] + $this-> Post['QuestionsLevelHigh']);

		$QPaperData = $this->Questionpaper_model->GeneratePaperSave($this->EntityID, $this-> Post);
		
		if($QPaperData === "NotFound")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Paper details not found in the system.";
			die;
		}
		elseif($QPaperData === "TestGiven")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Test attempt by the user, So you can not edit test.";
			die;
		}
		elseif($QPaperData)
		{
			$this->Return['Data']['QtPaperGUID'] = $QPaperData['QtPaperGUID'];
			$this->Return['Message'] = "Saved successfully.";
		}
	}


	public function GeneratePaperDelete_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('QtPaperGUID', 'Paper', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');
		$this->form_validation->validation($this);	
		
		$EntityID = empty($this->EntityID) ? $this->SessionUserID : $this->EntityID;

		$QPaperData = $this->Questionpaper_model->GeneratePaperDelete($EntityID, $this->QtPaperID, $this->Post);
		
		if($QPaperData === "NotFound")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Paper details not found in the system.";
			die;
		}
		elseif($QPaperData === "TestGiven")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Test attempt by the user, So you can not delete test.";
			die;
		}
		elseif($QPaperData === "Applicants")
		{
			$this->Return['Data'] = array();
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "User applied for scholarshipt test, So you can not delete test.";
			die;
		}
		elseif($QPaperData)
		{
			$this->Return['Data']['QtPaperGUID'] = $QPaperData['QtPaperGUID'];
			$this->Return['Message'] = "Deleted successfully.";
		}
	}

	public function viewPaper_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('QtPaperGUID', 'QtPaperGUID', 'trim|required|callback_validateEntityGUID[Question Paper,QtPaperID]');	
		$this->form_validation->validation($this);		

		$Questions=$this->Questionpaper_model->viewPaper((empty($this->EntityID) ? $this->SessionUserID : $this->EntityID),$this->QtPaperID);
		
		if($Questions)
		{
			$this->Return['Data'] = $Questions['Data'];
		}
	}


}
