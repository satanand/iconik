<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}



	function Subscribe($Input=array()){
		if(!empty($Input['Email'])){
			$this->db->select("Email");
			$this->db->from('tbl_subscribe_users');
			$this->db->Where("Email", $Input['Email']);
			$this->db->limit(1);
			$Query = $this->db->get();	
			//echo $this->db->last_query();
			if($Query->num_rows()>0){
				return -1;
			}else{
				$arr = array("Email"=>$Input['Email'],"StatusID"=>1);
				$this->db->insert("tbl_subscribe_users",$arr);
				return $insert_id = $this->db->insert_id();
			}
		}else{
			return FALSE;
		}
	}



	/*
	Description: 	Use to update page data.
	*/
	function editPage($PageGUID, $Input=array()){


		$Input['Content'] = htmlspecialchars($Input['Content'], ENT_QUOTES, 'UTF-8');


		$UpdateArray = array_filter(array(
			"Title" 			=>	@$Input['Title'],
			"Content" 			=>	@$Input['Content']
		));

		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('PageGUID', $PageGUID);
			$this->db->limit(1);
			$this->db->update('set_pages', $UpdateArray);
		}
		return TRUE;
	}










	/*
	Description: 	Use to get Page
	*/
	function getPage($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$this->db->select($Field);

		$this->db->from('set_pages P');

		if(!empty($Where['PageGUID'])){
			$this->db->where("P.PageGUID",$Where['PageGUID']);
		}           

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				$Record['Content']= ($Record['Content']);	

				if(!$multiRecords){
					return $Record;
				}

				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}




	function BAEnquiry($Input)
	{		
		$SType = $Input['SType']; 

		$this->db->select('EnquiryID');
		$this->db->from('ba_enquiry');
		$this->db->where('DType = "'.$SType.'"'); 
		$this->db->where('Email = "'.$Input['Email'].'"');        
	    $query = $this->db->get();
	    if ( $query->num_rows() > 0 )
	    {	    
			return "Exist"; die;
		}
		

		$InsertData = array(					
		"FullName"=>$Input['FullName'],			
		"Mobile"=>$Input['MobileNumber'],
		"Email"=>$Input['Email'],
		"DType"=>$SType,			
		"CreatedDate"=>date("Y-m-d H:i:s")			
		);
		
		$this->db->insert('ba_enquiry', $InsertData);
		$ID = $this->db->insert_id();

		if($ID > 0)
		{
			if($SType == "B2C")
			{
				$url = "http://bit.ly/33b2c";
				$SType = $SType." Brochure";
			}
			else
			{
				$url = "http://bit.ly/33b2b";
				$SType = $SType." Brochure";
			}	

			$content = "Hello ".$Input['FullName'].",<br/><br/>Thanks for showing interest.<br/><br/>Please click on the link below to download the brochure.<br/><a href='".$url."'>Download</a><br/><br/>";
					
			sendMail(array(
			'emailTo' => $Input['Email'],								
			'emailSubject' => "Download Brochure - Iconik",						
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));


			$content = "Hello Team,<br/><br/>Brochure has been downloaded. Details are as follows:<br/><br/>
			Type Of Brochure: ".$SType."<br/><br/>
			Full Name: ".$Input['FullName']."<br/><br/>
			Email: ".$Input['Email']."<br/><br/>
			Mobile Number: ".$Input['MobileNumber']."<br/><br/>";
					
			sendMail(array(
			'emailTo' => "contact@iconik.in",								
			'emailSubject' => "New Download of ".$SType." Brochure - Iconik",						
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));				
		}	

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}

}