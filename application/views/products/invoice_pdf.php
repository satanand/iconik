<style type="text/css">
.table td, .table tr, .table th
{
	border: 1px solid #ccc !important;
}
body
{
	color: #111 !important;
	font-size: 11px !important;
}
</style>
<table border="0" width="100%">
<tr>
	<td width="10%">
		<img src="<?php echo $session_data['InstituteProfilePic'];?>" width="100px" height="70px">
	</td>  

	<td width="90%" align="center">
		<h1><?php echo ucwords($session_data['FullName']);?></h1>
		<?php echo ucwords($session_data['Address'].", ".$session_data['CityName'].', '.$session_data['State']);?>
		<br/><?php echo $session_data['EmailForChange'];?>
	</td>
</tr>		
</table>  

<hr class="light-grey-hr" style='color:#ccc !important;' />

<table border="0" width="100%">
<tr>
<td colspan="2" align="center"><b><?php echo ucfirst($report_title);?></b></td>
</tr>
</table>

<br/>


<table width="100%" border="0" style="border-collapse: collapse; border:0px none !important;">
<tr>
	<td align="left"><b>ORDER ID:</b> <?php echo str_pad($content['Data']['Order']['OrderID'], 7, "0", STR_PAD_LEFT);?></th>
	<td align="right">
		<b>ORDER DATE:</b> <?php echo $content['Data']['Order']['OrderDate'];?>
		<?php
		if(isset($content['Data']['Order']['PayoutDate']) && !empty($content['Data']['Order']['PayoutDate']))
		{
		?>
			<br/><b>PAYOUT RECEIVED DATE:</b> <?php echo $content['Data']['Order']['PayoutDate'];?>		
		<?php
		}
		?>	
	</td>	
</tr>	

<tr>
	<td align="left"><br/><b>BILLING DETAILS</b><br/>
	<?php echo $content['Data']['Order']['CustomerName'];?><br/>
	<?php echo $content['Data']['Order']['CustomerEmail'];?><br/>
	<?php echo $content['Data']['Order']['CustomerMobile'];?>		
	</td>
	

	<td align="right"><b>DELIVERY ADDRESS</b><br/>
	<?php echo $content['Data']['Order']['BillingAddress'];?><br/>
	<?php echo $content['Data']['Order']['BillingCity'];?>, 
	<?php echo $content['Data']['Order']['BillingState'];?>, 
	<?php echo $content['Data']['Order']['BillingZipCode'];?>		
	</td>	
</tr>
</table>


<hr/>

<table class="table" width="100%" border="1" style="border-collapse: collapse;" cellpadding="5" cellspacing="5">
<thead>
<tr>	   
    <th>Product Name</th>
    <th>Description (first 100 words)</th>    
    <th>Unit Price</th>
    <th>Ordered Qty</th>
    <th>Amount</th>
    <th>Discount Amount</th>
    <th>Tax Amount</th>    
    <th>Amount</th>
</tr>
</thead>

<tbody id="tabledivbody">
<?php
if(isset($content['Data']['OrderDetails']) && !empty($content['Data']['OrderDetails']))
{
	$subtotal = $distotal = $shptotal = $gndtotal = $taxtotal = 0;
	foreach($content['Data']['OrderDetails'] as $arr)
	{
		if(isset($arr['Amount']) && !empty($arr['Amount']))
		$subtotal = $subtotal + $arr['Amount'];

		if(isset($arr['DiscountAmount']) && !empty($arr['DiscountAmount']))
		$distotal = $distotal + $arr['DiscountAmount'];

		if(isset($arr['ShippingCost']) && !empty($arr['ShippingCost']))
		$shptotal = $shptotal + $arr['ShippingCost'];

		if(isset($arr['TaxAmount']) && !empty($arr['TaxAmount']))
		$taxtotal = $taxtotal + $arr['TaxAmount'];

	?>	
		<tr>	
			<td><?php echo $arr['ProductName'];?></td>	

			<td><?php echo substr($arr['ProductDesc'], 0, 100);?></td> 

			<td align="center"><?php echo $arr['ProductUnitPrice'];?></td>

			<td align="center"><?php echo $arr['OrderedQty'];?></td>

			<td align="center"><?php echo $arr['Amount'];?></td>	

			<td align="center"><?php echo $arr['DiscountAmount'];?></td>

			<td align="center"><?php echo $arr['TaxAmount']." (".$arr['TaxPercent']."%)";?></td>			

			<td align="center"><?php echo $arr['Amount'];?></td>
		</tr>
<?php
	}


	$amtdis = ($subtotal - $distotal);
	$comm = ($amtdis * 30) / 100;
	$order_amount = ( $amtdis + $taxtotal + $shptotal);					
?>
		
		<tr>
			<td colspan="7" align="right"><b>TOTAL</b></td>	
			<td align="center"><b><?php echo $subtotal;?></b></td>			
		</tr>		

		<tr>	
			<td colspan="7" align="right"><b>TOTAL DISCOUNT</b></td>

			<td align="center"><b><?php echo $distotal;?></b></td>
		</tr>


		<tr>	
			<td colspan="7" align="right"><b>TOTAL TAX</b></td>

			<td align="center"><b><?php echo $taxtotal;?></b></td>
		</tr>
		

		<tr>	
			<td colspan="7" align="right"><b>TOTAL SHIPPING</b></td>

			<td align="center"><b><?php echo $shptotal;?></b></td>
		</tr>

		<tr>	
			<td colspan="7" align="right"><b>ORDER AMOUNT</b></td>

			<td align="center"><b><?php echo $order_amount;?></b></td>
		</tr>		
<?php
}
?>			
</tbody>
</table>

<?php
if(isset($content['Data']['Order']['PayoutDesc']) && !empty($content['Data']['Order']['PayoutDesc']))
{
?>
	<p><b>PAYOUT REMARKS:</b><br/><?php echo $content['Data']['Order']['PayoutDesc'];?></p>
<?php
}
?>