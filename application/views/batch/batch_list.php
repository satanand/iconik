<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>

 
<!-- <header class="panel-heading"  ng-if="type != 'facultys'"> <h1 class="h4">Manage Assign Batches </h1> </header>
<?php//this->load->view('includes/breadcrumb'); ?> -->
<div ng-controller="PageController" ng-init="getFilterData()" id="content-body">
<header class="panel-heading" ng-if="type == 'faculty' && type != 'facultys'"> <h1 class="h4">Manage Batches</h1> </header>
<header class="panel-heading"  ng-if="type != 'faculty' && type == 'facultys'"> <h1 class="h4">Manage Assigned Batches </h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController"><!-- Body -->
<!-- Top container -->


<div class="row clearfix mt-2 mb-2">
<div class="col-md-9">
	<span class="float-left records d-none d-sm-block">
		<span class="h5">Total Batches: {{data.totalRecords}}</span>
	</span>
</div>
<div class="col-md-3"  style="float: right;">
	<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAddAs();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Assign Batch</button>

	<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Create Batch</button>
</div>		
</div>



<form class="" id="filterForm" role="form" autocomplete="off" > <!--ng-submit="applyFilter()"-->
<div class="row clearfix mt-2 mb-2">		
<div class="col-md-3" ng-if="type == 'faculty' && type != 'facultys'">
	<div class="form-group">
		<select  name="CategoryID" id="CategoryID" class="form-control" >
			<option value="">Search Course</option>
			<option ng-repeat="rowco in data.categoryList" value="{{rowco.CategoryID}}">{{rowco.CategoryName}}</option>
		</select>
	</div>
</div>		

<div class="col-md-3">
	<div class="form-group">
		<input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Enter search word" style="" autocomplete="off">
	</div>
</div>

<div class="col-md-3">
	<div class="form-group">
		<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
	<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
	</div>
</div>
</div>
</form>	





	<!-- Top container/ -->
	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0" > 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->

		<table class="table table-striped table-condensed table-hover table-sortable" >

			<!-- table heading -->

			<thead>

				<tr>
                    

					
					<th  ng-if="type == 'faculty' && type != 'facultys'">Batch Name</th>
					 <th style="width: 200px;"  ng-if="type == 'faculty' && type != 'facultys'">Course</th>
					 <th style="width: 100px;" ng-if="type == 'faculty' && type != 'facultys'">Start Date</th>
					<th style="width: 100px;" ng-if="type == 'faculty' && type != 'facultys'">Duration<br/>(In Month/s)</th>

					
					<th  ng-if="type != 'faculty' && type == 'facultys'">Staff Name</th>
					<th style="width: 500px;" ng-if="type != 'faculty' && type == 'facultys'">Batch Name</th>

					
                    <th style="width: 150px;" class="text-center">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">

					
					<td ng-if="type == 'faculty'">
						<strong>{{row.BatchName}}</strong>
						<a href="./batch/?BatchID={{row.BatchID}}" style="font-size: 89%;">[View Faculties]</a>

					</td>
					<td ng-if="type == 'faculty'">
						{{row.CategoryName}}

					</td>
					<td ng-if="type == 'faculty'">
						{{row.StartDate | date: 'dd-MM-yyyy'}}

					</td>
					<td ng-if="type == 'faculty'">
						{{row.Duration}}

					</td>
					<td class="text-center" ng-if="type == 'faculty'">
                        <a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.BatchGUID)" data-toggle="tooltip" title="View"></a>
                        <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.BatchGUID)" data-toggle="tooltip" title="Edit"></a>
                        <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.BatchGUID)" data-toggle="tooltip" title="Delete"></a>
					</td>
					<!-- faculty -->
					
					<td ng-if="type == 'facultys'">
						<strong>{{row.FullName}}</strong>
					</td>

					<td ng-if="type == 'facultys'">
						{{row.BatchName}}
					</td>
					
					<td class="text-center" ng-if="type == 'facultys'">
                        <a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormAssingView(key, row.AsID)" data-toggle="tooltip" title="View"></a>
                        <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormAssingEdit(key, row.AsID)" data-toggle="tooltip" title="Edit"></a>
                         <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-ban-circle" href="" ng-click="loadFormAssingDelete(key, row.AsID)" data-toggle="tooltip" title="Unassign"></a>                        
					</td>

				</tr>
			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	 </div>

	 
	   <!-- add Modal -->

		<div class="modal fade" id="add_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Create <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>
	   <!-- add Asaining Modal -->

		<div class="modal fade" id="addasaining_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5"> Assign <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="addasaining_form" name="addasaining_form" autocomplete="off" ng-include="templateURLAddAs">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>

	   <!-- edit Modal -->

		<div class="modal fade" id="edits_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div> 
		<!-- view Modal -->

		<div class="modal fade" id="view_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="view_form" name="view_form" autocomplete="off" ng-include="templateURLView">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>
      <!-- assing -->
		<div class="modal fade" id="editAssign_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Edit Assign <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="editAssign_form" name="editAssign_form" autocomplete="off" ng-include="templateURLAssignEdit">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>

		<!-- assing -->
		<div class="modal fade" id="viewAssign_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">View Assign <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="viewAssign_form" name="viewAssign_form" autocomplete="off" ng-include="templateURLAssignView">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>

         <!-- permission form -->

		<div class="modal fade" id="permission_model">
			<div class="modal-dialog modal-md" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Permission <?php echo $this->ModuleData['ModuleName'];?></h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<!-- form -->
					<form id="permission_form" name="permission_form" autocomplete="off" ng-include="templateURLPermission" method="POST">
					</form>
					<!-- /form -->
				</div>
			</div>
		</div>


		<!-- <div class="modal fade" id="attendance_model">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="modal-title h5">Mark Attendance</h3>     	
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					
					<div ng-include="templateURLAttendance">
					</div>
					
				</div>
			</div>
		</div> -->

</div><!-- Body/ -->
</div>