
app.controller('PageController', function($scope, $http, $timeout) 
{
    $( "#FilterStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    $( "#FilterPayoutStartDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterPayoutEndDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#FilterPayoutEndDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#FilterPayoutStartDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    $scope.APIURL = API_URL + "upload/image";
    $scope.APIURLFILE = API_URL + "upload/file";

    $scope.data.qualification = [];

    $scope.data.states = [];

    

	$scope.getFilterData = function()
	{
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }    
	}


	$scope.applyFilter = function() 
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList(0);
        $scope.getFilterData();
    }


    $scope.getCategoryList = function ()
    {
        $scope.data.CategoryList = [];
        $scope.data.SubCategoryList = [];
        
        var data = 'SessionKey=' + SessionKey;

        $http.post(API_URL+'products/getCategoryList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                for (var i in response.Data.Records.Category) 
                {
                    $scope.data.CategoryList.push(response.Data.Records.Category[i]);
                } 

                for (var i in response.Data.Records.SubCategory) 
                {
                    $scope.data.SubCategoryList.push(response.Data.Records.SubCategory[i]['SubCategoryName']);
                }
                
                $( "#ProductSubCategory" ).autocomplete({
                  source: $scope.data.SubCategoryList
                });               
            }
        });
    }

	$scope.getList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'products/getProducts', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                 	$scope.data.dataList.push(response.Data.Records[i]);
            	}
                
                $scope.data.pageNo++;
         	}
            else
            {
            	$scope.data.noRecords = true;
        	}

	        $scope.data.listLoading = false;
	    });

    }

    /*load add form*/
    $scope.loadFormAdd = function(Position) 
    {
        $scope.templateURLAdd = PATH_TEMPLATE + module + '/add.htm?' + Math.random();

        $('#add_model').modal({
            show: true
        });

        $scope.getCategoryList();

        $timeout(function() {

        /*tinymce.init({
            selector: '#ProductDesc, #ProductFeatures'
        });*/ 

            tinymce.init({
              selector: '#ProductDesc',
              init_instance_callback: function (editor) 
              {
                editor.on('keyup blur', function (e) 
                {
                    $scope.wordLength('ProductDesc', 3000);
                });
              }
            });


            tinymce.init({
              selector: '#ProductFeatures',
              init_instance_callback: function (editor) 
              {
                editor.on('keyup blur', function (e) 
                {
                    $scope.wordLength('ProductFeatures', 3000);
                });
              }
            });
    

        }, 1000);
        
    }


    $scope.addData = function() 
    {
        $scope.addDataLoading = true;

        var v1 = tinymce.get('ProductDesc').getContent();
        var v2 = tinymce.get('ProductFeatures').getContent();
        
        $("#ProductDesc").val(v1);
        $("#ProductFeatures").val(v2);
       
        tinymce.get('ProductDesc').setContent(v1);
        tinymce.get('ProductFeatures').setContent(v2);
        
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='add_form']").serialize();
        
        $http.post(API_URL + 'products/add', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
                
                $timeout(function() 
                {
                    window.location.href = window.location.href;

                }, 1000);
            } 
            else 
            {
                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;

        });

    }


    $scope.editData = function() 
    {
        $scope.addDataLoading = true;
        
        var v1 = tinymce.get('ProductDesc').getContent();
        var v2 = tinymce.get('ProductFeatures').getContent();
        
        $("#ProductDesc").val(v1);
        $("#ProductFeatures").val(v2);
       
        tinymce.get('ProductDesc').setContent(v1);
        tinymce.get('ProductFeatures').setContent(v2);

        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='edit_form']").serialize();

        $http.post(API_URL + 'products/edit', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
               
                $timeout(function() 
                {
                    window.location.href = window.location.href;
                }, 1000);
            } 
            else 
            {

                ErrorPopup(response.Message);
            }

            $scope.addDataLoading = false;
        });
    }



    $scope.loadFormDelete = function(Position, ProductID) 
    {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL + 'products/delete', 'SessionKey=' + SessionKey + '&ProductID=' + ProductID, contentType).then(function(response) 
            {
				var response = response.data;
                if (response.ResponseCode == 200) 
                {
                    /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.applyFilter();
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


    $scope.loadFormView = function(Position, ProductID, Data) 
    {
        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLView = PATH_TEMPLATE + module + '/view.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#View_model').modal({
            show: true
        });        

        $scope.data.pageLoading = false;
    }

    


    $scope.loadFormEdit = function(Position, ProductID, Data) 
    {
        $scope.data.Position = Position;
        $scope.formData = Data;

        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit.htm?' + Math.random();
        $scope.data.pageLoading = true;

        $('#edits_model').modal({
            show: true
        });

        $scope.getCategoryList();

        $timeout(function() {

            tinymce.init({
              selector: '#ProductDesc',
              init_instance_callback: function (editor) 
              {
                editor.on('keyup blur', function (e) 
                {
                    $scope.wordLength('ProductDesc', 3000);
                });
              }
            });


            tinymce.init({
              selector: '#ProductFeatures',
              init_instance_callback: function (editor) 
              {
                editor.on('keyup blur', function (e) 
                {
                    $scope.wordLength('ProductFeatures', 3000);
                });
              }
            }); 

        }, 2000); 

              
        
        $scope.data.pageLoading = false;
    }



    $scope.FileInModalBoxLocal = function (url, title, modal="") 
    {
        $scope.data.pageLoading = false;
        if(modal.length){
            $('#'+modal+' .close').click();
        }
        $scope.file_title = title;
        $scope.modal = modal;
        $('#iframe-info').html('<iframe style="width: 100%;height: 400px;border:0px none;" src="https://docs.google.com/gview?url='+url+'&amp;embedded=true"></iframe>');
        $('#extension_popup').modal({
            show : true
        });
    }



    $scope.getOrdersList = function (check=0)
    {
        if(check == 0)
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }
        else
        {
            $scope.data.dataList = [];
            $scope.data.noRecords = false;
            $scope.data.pageNo = 1;
        }        

        $scope.data.pageSize = 20;
       
        $scope.data.listLoading = true;
        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'products/getOrders', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                
                $scope.data.pageNo++;
            }
            else
            {
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });

    }


    $scope.loadViewOrder = function(OrderID)
    {        
        $scope.data.OrderDetails = [];

        $('#view_order_model').modal({
            show: true
        });

        $scope.OrderDetailsLoading = true;
        
        var APIURL = API_URL+'products/getOrdersDetails';

        $http.post(APIURL, 'SessionKey=' + SessionKey + '&OrderID=' + OrderID, contentType).then(function(response) 
        {
            $scope.OrderDetailsLoading = false;

            var response = response.data;

            if (response.ResponseCode == 200) 
            {
                $scope.data.OrderDetails = response.Data.Records;                    
            }
        });
    } 


    $scope.generateInvoice = function(OrderID)
    {        
        if(OrderID <= 0)
        {
            alert("Invalid order"); return false;    
        }        

        window.location.href = BASE_URL + "products/generateInvoice?oid=" + OrderID;
    } 


    $scope.loadSoldProductDetails = function(ProductID)
    {        
        $scope.data.SoldProductDetails = [];

        $('#view_order_model').modal({
            show: true
        });

        $scope.SoldProductDetailsLoading = true;
        
        var APIURL = API_URL+'products/getSoldProductDetails';

        $http.post(APIURL, 'SessionKey=' + SessionKey + '&ProductID=' + ProductID, contentType).then(function(response) 
        {
            $scope.SoldProductDetailsLoading = false;

            var response = response.data;

            if (response.ResponseCode == 200) 
            {
                $scope.data.SoldProductDetails = response.Data.Records;                    
            }
        });
    }


    $scope.wordLength = function(objID, len)
    {
        var l1 = 0;
        var l2 = 0;

        var txt1 = "";
        var txt2 = "";    
            
        txt1 = tinymce.get(objID).getContent();
        l1 = txt1.length;
        
        if(l1 > len)
        {
            txt1 = txt1.substring(0, len);
            $(this).val(txt1);
        }
        else
        {
            l1 = len - l1;
            
            $("#"+objID+"Len").html(l1);
        }
        
    }


    $scope.loadFormImport = function(Position) 
    {
        $scope.templateURLImport = PATH_TEMPLATE + module + '/import.htm?' + Math.random();

        $('#import_model').modal({
            show: true
        });        
    }

    $scope.addDataImport = function() 
    {
        $scope.addDataImportLoading = true;        
        
        var data = 'SessionKey=' + SessionKey + '&' + $("form[name='import_form']").serialize();
        
        $http.post(API_URL + 'products/importProduct', data, contentType).then(function(response) 
        {
            var response = response.data;
            if (response.ResponseCode == 200) 
            {
                SuccessPopup(response.Message);
                
                $timeout(function() 
                {
                    window.location.href = window.location.href;

                }, 3000);
            } 
            else 
            {
                ErrorPopup(response.Message);
            }

            $scope.addDataImportLoading = false;

        });

    }

});

function clear_filters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword").val('');

    angular.element(document.getElementById('content-body')).scope().getList(1);
}

function clear_order_filters()
{
    $("#FilterStartDate, #FilterEndDate, #Keyword, #FilterPayoutStartDate, #FilterPayoutEndDate").val('');

    angular.element(document.getElementById('content-body')).scope().getOrdersList(1);
}

$(document).on('click', "#picture-uploadBtn1", function() {
        $(this).parent().find('#fileInputProduct').focus().val("").trigger('click');
    });

$(document).on('change', '#fileInputProduct', function() 
{

    var target = $(this).data('target');

    var mediaGUID = $(this).data('targetinput');

    var progressBar = $('.progressBar1'),

    bar = $('.progressBar1 .bar1'),

    percent = $('.progressBar1 .percent1');

    $(this).parent().ajaxForm({

        data: {

            SessionKey: SessionKey

        },

        dataType: 'json',

        beforeSend: function() {

            progressBar.fadeIn();

            var percentVal = '0%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        uploadProgress: function(event, position, total, percentComplete) {

            var percentVal = percentComplete + '%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        success: function(obj, statusText, xhr, $form) {

            console.log(obj);

            if (obj.ResponseCode == 200) {

                var percentVal = '100%';

                bar.width(percentVal)

                percent.html(percentVal);

                $(target).prop("src", obj.Data.MediaURL);

                $("input[name='MediaGUIDss']").val(obj.Data.MediaGUID);

                $(mediaGUID).val(obj.Data.MediaGUID);

                $("#MediaURL").val(obj.Data.MediaURL);

            } else {

                ErrorPopup(obj.Message);

            }

        },

        complete: function(xhr) {

            progressBar.fadeOut();

            //$('#studentFileInput').val("");

        }

    }).submit();

});


function showShipping()
{
    var d = $("#IsDownlodable").val();

    if(d == 0)
    {
        $("#ProductQty").val("");
        $("#ShippingCostDiv").show();
        $("#DownloadableDiv").hide();
        
    }
    else
    {
        $("#ShippingCostDiv").hide();

        $("#ProductQty").val("unlimited");        
        $("#DownloadableDiv").show();
        
    }
}

function SetProductCategory()
{
    $("#BooksDiv").hide();
    //$("#BooksDiv").find("input").val("");

    var pcatid = $("#ProductCategory").val();

    if(pcatid == 13)
    {
        $("#BooksDiv").show();
    }
    
    if(pcatid == 2 || pcatid == 10 || pcatid == 12)
    {
        $("#IsDownlodable").val(1);
        $("#IncreaseProductQtyDiv").hide();        
    }
    else
    {
        $("#IsDownlodable").val(0);
        $("#IncreaseProductQtyDiv").show();
    }

    showShipping();
}


function CalculateMRP()
{
    var Tax = $("#Tax").val();
    var Price = $("#ProductPrice").val();

    var MRP = Price;

    if(Tax != "" && Tax != undefined && Tax >= 0 && Price != "" && Price != undefined && Price >= 0)
    {
        MRP = parseFloat(Price) + ((parseFloat(Price) * parseFloat(Tax)) / 100);
    }

    $("#ProductPriceMRP").val(MRP);
}



