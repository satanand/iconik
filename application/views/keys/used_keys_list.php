<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
</style>
<header class="panel-heading">
	<h1 class="h4">Details of used keys</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="keys-body" ng-init="getCourses()"><!-- Body -->

	<form class="form-inline float-left ml-4 ng-pristine ng-valid ng-submitted filterForm" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()"> 
	<div class="clearfix mt-2 mb-2">
			<input type="hidden" name="Type" id="list_type">
			<div class="row" style="margin-top: 20px;">
				<div class="col-md-12">	
					<div class="form-group">
				      <span class="float-left records hidden-sm-down">
						<span class="h5">Total records: {{data.dataList.length}}</span>
					  </span>
					</div>
				</div>	
			</div>
			
			<div class="row" style="margin-top: 20px;">
			  <div class="col-md-2">
		         <div class="form-group">
		         	<!-- <label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label> -->
		            <select class="form-control" name="Type" onchange="applyFilter(this.value)">
		              <option value="Student" selected="selected">Student</option>
		              <option value="Staff">Staff</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-3">
		         <div class="form-group">
		         	<!-- <label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label> -->
		            <select class="form-control" name="CategoryGUID" onchange="filterUsedKeysList(this.value)" name="ParentCat" id="Courses">
		               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
		               <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-2">
		         <div class="form-group">
		            <div id="subcategory_section">
		            	<!-- <label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label> -->
		               <select id="Batch" name="BatchGUID" ng-model="BatchGUID" ng-change="applyFilter()"  class="form-control">
		                  <option value="">Select Batch</option>
		                  <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}">{{row.BatchName}}</option>
		               </select>
		            </div>
		         </div>
		      </div>
		      <div class="col-md-2" style="margin-left: 25px;">
		         <div class="form-group">
		               <select  id="Validity"  name="Validity" class="form-control" ng-model="Validity" ng-change="UsedKeysDetail(1,Validity)">

								<option value="3" ng-selected="Validity==3">3 Month</option>

								<option value="6" ng-selected="Validity==6">6 Month</option>

								<option value="12" ng-selected="Validity==12">12 Month</option>

								<option value="24" ng-selected="Validity==24">24 Month</option>

						</select> 
		         </div>
		      </div>
		      <div class="col-md-2" style="margin-left: 25px;">
		      	<div class="form-group">
					
						<div id="universal_search">
	                        <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
	                        <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
	                        <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
	                    </div>
					
				</div>
			  </div>
			  
		   </div>
		   
		
	</div>
	</form>
	<form class="form-inline float-left ml-4 ng-pristine ng-valid ng-submitted filterForm" id="filterForm2" role="form" autocomplete="off" ng-submit="applyFilter()" style="display: none;"> 
	<div class="clearfix mt-2 mb-2">
		
			<div class="row" style="margin-top: 20px;">
				<div class="col-md-12">	
					<div class="form-group">
				      <span class="float-left records hidden-sm-down">
						<span class="h5">Total records: {{data.dataList.length}}</span>
					  </span>
					</div>
				</div>	
			</div>
			
			<div class="row" style="margin-top: 20px;">
			  <div class="col-md-2">
		         <div class="form-group">
		         	<!-- <label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label> -->
		            <select class="form-control" name="Type2" onchange="applyFilter(this.value)">
		              <option value="Staff" selected="selected">Staff</option>
		              <option value="Student">Student</option>		              
		            </select>
		         </div>
		      </div>		     
		      <div class="col-md-2">
		         <div class="form-group">
		            <div id="subcategory_section">
		            	<!-- <label class="filter-col" for="ParentCategory" id="select-label-parent-category"></label> -->
		               <select id="Batch" name="BatchGUID" ng-model="BatchGUID" ng-change="applyFilter()"  class="form-control">
		                  <option value="">Select Batch</option>
		                  <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}">{{row.BatchName}}</option>
		               </select>
		            </div>
		         </div>
		      </div>
		      <div class="col-md-2" style="margin-left: 25px;">
		         <div class="form-group">
		               <select  id="Validity"  name="Validity" class="form-control" ng-model="Validity" ng-change="UsedKeysDetail(1,Validity)">

								<option value="3" ng-selected="Validity==3">3 Month</option>

								<option value="6" ng-selected="Validity==6">6 Month</option>

								<option value="12" ng-selected="Validity==12">12 Month</option>

								<option value="24" ng-selected="Validity==24">24 Month</option>

						</select> 
		         </div>
		      </div>
		      <div class="col-md-2" style="margin-left: 25px;">
		      	<div class="form-group">
					
						<div id="universal_search">
	                        <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
	                        <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
	                        <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
	                    </div>
					
				</div>
			  </div>
			  
		   </div>
		   
		
	</div>
	</form>
	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">


	<!-- Data table -->
	<div class="table-responsive block_pad_md" infinite-scroll="UsedKeysDetail()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

	
		<!-- data table -->
		<table class="table table-striped table-hover"  id="tableToExport">
			<!-- table heading -->
			<thead>
				<tr>
					<th>Student/Staff</th>	
					<th style="width: 120px; ">Batch</th>
					<th style="width: 120px;" class="course-th">Course</th>					
					<th style="width: 100px; text-align: center;">Key</th>
					<th style="width: 100px; text-align: center;">Validity</th>
					<th style="width: 100px; text-align: center;">Key Status</th>
					<th style="width: 100px; ">Assigned Date</th> 
					<th style="width: 100px; ">Activation Date</th>
					<th style="width: 100px; ">Expiry Date</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody ng-repeat="(key, row) in data.dataList">
				<tr scope="row">
					<td>{{row.FirstName}} {{row.LastName}}</td>
					<td>{{row.BatchName}}</td>
					<td class="course-th" ng-if="row.Type == 'Student'">{{row.CategoryName}}</td>
					<td style="text-align: center;">{{row.Key}}</td>
					<td style="text-align: center;">{{row.Validity}}</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==2" class="text-success">Active</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==1" class="text-danger">Inactive</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==6" class="text-danger">Expired</td>
					<td style="text-align: center;" ng-if="row.KeyStatusID==0" class="text-danger">Not Assigned</td>
					<td >{{row.KeyAssignedOn}}</td>
					<td >{{row.ActivatedOn}}</td>
					<td >{{row.ExpiredOn}}</td>  
					<td style="text-align: center;" ng-if="row.KeyStatusID==1 && ((UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)"><a href=""  ng-click="UnassignKey(key,row.UserGUID,row.Type)" data-toggle="tooltip" title="Unassigned key">Unassign Key</a></td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="data.noRecords">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->
	<!-- add Modal -->
</div><!-- Body/ -->

<script type="text/javascript">
	
</script>