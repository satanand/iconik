<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Category_model');
		$this->load->model('Roles_model');
	}



	/*

	Description: 	Use to add new category

	URL: 			/api_admin/category/add	

	*/

	public function add_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('UserTypeName', 'User Role', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

        //$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$RolesData = $this->Roles_model->addRoles($this->Post);
		
		if($RolesData){

			if($RolesData == 'EXIST'){
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This staff role name already exist.";
			}else{

				$this->Return['Message']      	=	"New staff role added successfully.";
				$this->Return['UserTypeID'] 	= $RolesData['UserTypeID'];
			}

		}

	}



	/*
	Description: 	Use to get Get Attributes.
	URL: 			/api/category/getAttributes
	Input (Sample JSON): 		
	*/
	public function getAttributes_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|required|callback_validateEntityGUID[Category,CategoryID]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */
		
		$AttributesData = $this->Category_model->getAttributes('
			E.EntityGUID AttributeGUID,
			A.AttributeName,
			A.AttributeValues
			',
			array("EntityID"=>$this->CategoryID),	TRUE, 1, 25);
		if(!empty($AttributesData)){
			$this->Return['Data'] = $AttributesData['Data'];
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getRoles_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//print_r($this->Post); die();

		$RolesData = $this->Roles_model->getRoles($this->EntityID,$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($RolesData)){
			$this->Return['Data'] = $RolesData['Data'];
		}	
	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/rolse/module
	Input (Sample JSON): 		
	*/
	public function getmodules_post()
	{
		$RolesDataModule = $this->Roles_model->getmodules($this->input->post('UserTypeID'));

		if(!empty($RolesDataModule)){
			$this->Return['Data'] = $RolesDataModule['Data'];
		}	
	}



	


	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategory
	Input (Sample JSON): 		
	*/
	public function getEDRoles_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$CategoryData = $this->Roles_model->getEDRoles('',array("UserTypeID"=>$this->input->post('UserTypeID')));
		if(!empty($CategoryData)){
			$this->Return['Data'] = $CategoryData;
		}	
	}

	/*

	Name: 			editRoles_post
	

	*/
	public function editRoles_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');

		$this->form_validation->set_rules('UserTypeName', 'UserTypeName', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Roles_model->editRoles($this->input->Post('UserTypeID'),$this->input->Post('UserTypeName'));
	
		$this->Return['Data']= $this->Roles_model->getRolesd($this->input->Post('UserTypeID'),array());

	    $this->Return['Message']      	=	"User Roles updated successfully."; 

		}

		
	/* permission */
	public function PermissionRoles_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|required');

		$this->form_validation->set_rules('PermissionType', 'PermissionType', 'trim|required');

		//$this->form_validation->set_rules('ModuleID[]', 'ModuleID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
         
        $this->Roles_model->permissiondel($this->input->Post('UserTypeID'));

        $this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),1);

        //die;
		
		//$modules = $this->input->Post('ModuleID');
		//print_r($checkbox1);

		// $edit = $this->input->Post('edit');

		// $add = $this->input->Post('add');

		// $delete = $this->input->Post('delete');

		$view = $this->input->Post('view');

		

		$all = $this->input->Post('all');

		//print_r($all);

		$modules = array();

		if(!empty($view)){
			foreach($view as $k => $val)  {
				if(!in_array($val, $modules)){
					array_push($modules, $val);	
					$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),$val); 
				}

				
				if($val == 23){
					array_push($modules, 46);	
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),46);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",46); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",23); 

	       	 	}else if($val == 25){
					array_push($modules, 47);	
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),47);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",47); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",25); 

	       	 	}else if($val == 14){
					array_push($modules, 57);	
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),57);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",57); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",14); 

	       	 	}else if($val == 17){
					array_push($modules, 48);	
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),48);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",48); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",17); 

	       	 	}else if($val == 18){
					array_push($modules, 49);	
					array_push($modules, 50);	
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),49);
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),50);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",49);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",50); 

	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",18);
	       	 	}else if($val == 38){
					array_push($modules, 56);	
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),56);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",56);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",38);
	       	 	}else if($val == 22){
					array_push($modules, 51);	
					array_push($modules, 52);
					array_push($modules, 53);	
					array_push($modules, 54);
					array_push($modules, 55);

	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),51);
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),52);
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),53);
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),54);
	       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),55);
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",51);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",52); 
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",53);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",54); 
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",55);

	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",22);
	       	 	}else{
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",$val); 

	       	 	}			
			}
		}
		

		
		if(!empty($all)){ //print_r($view);
			foreach($all as $k => $val)  {
				if(!in_array($val, $modules)){
					//print_r($modules);
					array_push($modules, $val);	
					$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),$val); 
				}
				
				if($val == 23){
					if(!in_array(46, $modules)){
						array_push($modules, 46);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),46);
					}	       	 		
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",46); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",23); 

	       	 	}else if($val == 25){
	       	 		if(!in_array(47, $modules)){
	       	 			array_push($modules, 47);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),47);
					}
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",47); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",25); 

	       	 	}else if($val == 14){
	       	 		if(!in_array(57, $modules)){
	       	 			array_push($modules, 57);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),57);
					}
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",57); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",14); 

	       	 	}else if($val == 17){
					if(!in_array(48, $modules)){
	       	 			array_push($modules, 48);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),48);
					}
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",48); 
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",17); 

	       	 	}else if($val == 18){
	       	 		if(!in_array(49, $modules)){
	       	 			array_push($modules, 49);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),49);
					}

					if(!in_array(50, $modules)){
	       	 			array_push($modules, 50);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),50);
					}
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",49);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",50); 

	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",18);
	       	 	}else if($val == 38){
	       	 		if(!in_array(56, $modules)){
	       	 			array_push($modules, 56);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),56);
					}
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",56);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",38);
	       	 	}else if($val == 22){
	       	 		if(!in_array(51, $modules)){
	       	 			array_push($modules, 51);	
						array_push($modules, 52);
						array_push($modules, 53);	
						array_push($modules, 54);
						array_push($modules, 55);
						$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),51);
		       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),52);
		       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),53);
		       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),54);
		       	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),55);
					}
	       	 		
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",51);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",52); 
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",53);
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",54); 
	   	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",55);

	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",22);
	       	 	}else{
	       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",$val); 

	       	 	}			
			}
		}

		//print_r($modules);

		// foreach($modules as $k=>$chk1)  
  //       {  

  //      	 	$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),$chk1); 

  //      	 	if($chk1 == 23){
  //      	 		$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),46); 
  //      	 		//$this->Roles_model->AddPrmissionRoles($this->input->Post('UserTypeID'),23); 

  //      	 		if(in_array(23, $view)){
  //      	 			$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",46); 
	 //       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",23); 
	 //       	 	}

	 //       	 	if(in_array(23, $all)){
	 //       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",46); 
	 //       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",23); 
	 //       	 	}

  //      	 	}else{
  //      	 		if(in_array($chk1, $view)){
	 //       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"view",$chk1); 
	 //       	 	}

	 //       	 	if(in_array($chk1, $all)){
	 //       	 		$this->Roles_model->AddSecondLevelPermission($this->input->Post('UserTypeID'),"all",$chk1); 
	 //       	 	}

  //      	 	}

		// }
	
		//$this->Return['Data']= $this->Roles_model->getRolesd($this->EntityID,array());
		$this->Return['Data']= $this->Roles_model->getRolesd($this->input->Post('UserTypeID'),array());

	    $this->Return['Message']      	=	"Permission Access successfully."; 

	}

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getContentCategories_post()
	{
		$ContentCategoryData = $this->Category_model->getContentCategories();
		if(!empty($ContentCategoryData)){
			//$this->Return['Data'] = $this->Category_model->test($CategoryData['Data']['Records']);
			$this->Return['Data'] = $ContentCategoryData;
		}	
	}


	public function deleteRole_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
	
		$this->Return['Data']= $this->Roles_model->deleteRole($this->EntityID, array("UserTypeID"=>$this->input->post('UserTypeID')));

	    $this->Return['Message']      	=	"Deleted successfully."; 

	}
}