<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	function triggerSMS()
	{
		$sql = "SELECT QueueID, Content, SendTo
		FROM cronjobs_queue	
		WHERE IsSent = 0 AND SentType = 'sms'				
		ORDER BY QueueID 
		LIMIT 3
		";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{						
			$QueueIDArr = array();

			foreach($Query->result_array() as $counter => $Record)
			{
				$QueueID = trim($Record['QueueID']);
				$Content = trim($Record['Content']);
				$SendTo = trim($Record['SendTo']);

				$Content = substr($Content, 0, 160);					

				++$counter;
				$SendTo = "9993593886";
				$Content = "Job Post Message No = ".$counter;

				sendSMS(array("PhoneNumber" => $SendTo, "Message" => $Content));

				$data = array("IsSent" =>	1);
				$this->db->where("QueueID", $QueueID);
				$this->db->update('cronjobs_queue', $data);

				$chk = "";
				$chk = $this->db->affected_rows();				
				if(isset($chk) && !empty($chk))
				{
					$QueueIDArr[] = $QueueID;
				}
			}


			if(isset($QueueIDArr) && !empty($QueueIDArr))
			{
				$ids = implode(",", $QueueIDArr);

				$sql = "DELETE 
				FROM cronjobs_queue	
				WHERE QueueID IN ($ids) AND IsSent = 1 AND SentType = 'sms'	";			

				$this->db->query($sql);
			}				
		}		

		return true;			
	}


	function triggerEmail()
	{
		$sql = "SELECT QueueID, Subject, Content, SendTo
		FROM cronjobs_queue	
		WHERE IsSent = 0 AND SentType = 'email'				
		ORDER BY QueueID ";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{						
			$QueueIDArr = array();

			foreach($Query->result_array() as $Record)
			{
				$QueueID = trim($Record['QueueID']);
				$Subject = trim($Record['Subject']);
				$Content = trim($Record['Content']);
				$SendTo = trim($Record['SendTo']);					

				$SendMail = sendMail(array(
				'emailTo' 		=> $SendTo,			
				'emailSubject'	=> $Subject,
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $Content),TRUE))
				));

				$data = array("IsSent" =>	1);
				$this->db->where("QueueID", $QueueID);
				$this->db->update('cronjobs_queue', $data);

				$chk = "";
				$chk = $this->db->affected_rows();				
				if(isset($chk) && !empty($chk))
				{
					$QueueIDArr[] = $QueueID;
				}
			}


			if(isset($QueueIDArr) && !empty($QueueIDArr))
			{
				$ids = implode(",", $QueueIDArr);

				$sql = "DELETE 
				FROM cronjobs_queue	
				WHERE QueueID IN ($ids) AND IsSent = 1 AND SentType = 'email'	";			

				$this->db->query($sql);
			}				
		}		

		return true;			
	}


	function getUserDetailsById($UserID)
	{
		$sql = "SELECT UserID, FirstName, LastName, IF(PhoneNumber IS NULL, PhoneNumberForChange, PhoneNumber) as Mobile, IF(Email IS NULL, EmailForChange, Email) as Email  
		FROM tbl_users		
		WHERE UserID = '$UserID'
		LIMIT 1	";			

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$data = $Query->result_array();

			return $data[0];
		}

		return array();
	}			


	function partTimeJobs()
	{				
		$SMS_template = "Dear STUDENT_NAME, A new job available on Iconik! Login to your account & apply now!. https://iconik.in/signin.php";


		$sql = "SELECT cj.CronID 
		FROM cronjobs cj
		INNER JOIN tbl_part_time_job pj ON pj.PartTimeJobID = cj.TargetIds		
		WHERE cj.CronFor = 'part_time_job' AND cj.IsSent = 0				
		ORDER BY cj.CronID
		LIMIT 1	";			

		$MainQuery = $Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$sql = "SELECT * FROM
			(
				SELECT u.FirstName, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile
				FROM tbl_entity e			
				INNER JOIN tbl_users u ON u.UserID = e.EntityID			
				WHERE e.InstituteID != 10 AND u.UserTypeID IN (7, 88)
			) as temp
			WHERE Mobile != ''
			ORDER BY FirstName ";

			$Query = $this->db->query($sql);

			if($Query->num_rows()>0)
			{			
				foreach($Query->result_array() as $Record)
				{
					$Content = $SMS_template;
					$FirstName = ucfirst(trim($Record['FirstName']));				

					$Content = str_replace("STUDENT_NAME", $FirstName, $Content);					

					$data = array(
					"Subject" =>	"New Jobs Posted - Iconik",
					"Content" =>	$Content,
					"SendTo" => 	trim($Record['Mobile']));

					$this->db->insert('cronjobs_queue', $data);
				}
			}


			$date = date("Y-m-d H:i:s");

			$sql = "UPDATE cronjobs 
			SET IsSent = 1, UpdateDate = '$date'
			WHERE CronFor = 'part_time_job' AND IsSent = 0 ";			

			$this->db->query($sql);
			
				
		}		

		return true;			
	}



	function beforeDueDate()
	{				
		$SMS_template = "";
		$EML_template = "";		

		$sql = "SELECT s.StudentID, f.RemindFor, r.EmailContent, r.SmsContent, u.FirstName, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email 
		FROM tbl_fee_reminders_days f
		INNER JOIN tbl_students_feeduedate d ON f.InstituteID = d.InstituteID AND d.IsPaid = 0 AND d.ReceivedDate IS NULL
		LEFT JOIN tbl_fee_reminders r ON r.RemindFeeSettingID = f.RemindFeeSettingID AND r.RemindWhen = 'before' AND r.RemindPhaseType = 'nophase'		
		INNER JOIN tbl_users u ON u.UserID = d.StudentID
		INNER JOIN tbl_students s ON s.StudentID = d.StudentID AND s.FeeStatusID != 5
		WHERE f.RemindWhen = 'before' AND f.RemindPhaseType = 'nophase' AND d.IsPaid = 0 AND d.ReceivedDate IS NULL AND DATE_SUB(d.DueDate, INTERVAL f.OnDays DAY) = CURDATE()		
		GROUP BY d.StudentID, f.RemindFor
		ORDER BY d.StudentID ";

		$MainQuery = $this->db->query($sql);

		if($MainQuery->num_rows()>0)
		{
			foreach($MainQuery->result_array() as $MainRecord)
			{
				$EntityID = $StudentID = $MainRecord['StudentID'];

				$FirstName = $MainRecord['FirstName'];
				$Mobile = $MainRecord['Mobile'];
				$Email = $MainRecord['Email'];

				$RemindFor = $MainRecord['RemindFor'];
				$EmailContent = $MainRecord['EmailContent'];
				$SmsContent = $MainRecord['SmsContent'];

				$DueData = $this->getStudentFeeDetail($EntityID,$StudentID);
				
				if(isset($DueData['DueDates']) && !empty($DueData['DueDates']))
				{
					$DueDate = $DueData['DueDates'];
					$DueAmount = $DueData['InstallmentAmount'];
					$InstituteName = $DueData['InstituteName'];

					$EmailContent = str_replace("{STD_NAME}", $FirstName, $EmailContent);
					$EmailContent = str_replace("{DUE_AMT}", $DueAmount, $EmailContent);
					$EmailContent = str_replace("{DUEDATE}", $DueDate, $EmailContent);
					$EmailContent = str_replace("{INST_NAME}", $InstituteName, $EmailContent);

					$SmsContent = str_replace("{STD_NAME}", $FirstName, $SmsContent);
					$SmsContent = str_replace("{DUE_AMT}", $DueAmount, $SmsContent);
					$SmsContent = str_replace("{DUEDATE}", $DueDate, $SmsContent);
					$SmsContent = str_replace("{INST_NAME}", $InstituteName, $SmsContent);

					$Content = "";						
					if($RemindFor == "email")
					{
						$Content = $EmailContent;
						$SendTo = $Email;
					}
					else
					{
						$Content = $SmsContent;
						$SendTo = $Mobile;							
					}	

					$data = array(
					"Subject" =>	"Fee Installment Reminder",				
					"Content" =>	$Content,
					"SendTo" => 	trim($SendTo),
					"SentType" => 	$RemindFor
					);

					$this->db->insert('cronjobs_queue', $data);
				}	
			}					
		}		

		return true;			
	}


	function afterDueDate()
	{				
		$SMS_template = "";
		$EML_template = "";
		
		$sql = "SELECT s.StudentID, f.InstituteID, d.BatchID, d.CourseID, f.RemindFor, r.RemindPhaseType, r.EmailContent, r.SmsContent, r.RemindToParent, r.RemindToFaculty, r.RemindToManagement, u.FirstName, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, s.FathersPhone, s.FathersEmail, s.FathersName, bb.BatchName 
		FROM tbl_fee_reminders_days f
		INNER JOIN tbl_students_feeduedate d ON f.InstituteID = d.InstituteID AND d.IsPaid = 0 AND d.ReceivedDate IS NULL
		LEFT JOIN tbl_fee_reminders r ON r.RemindFeeSettingID = f.RemindFeeSettingID AND r.RemindWhen = 'after' AND r.RemindPhaseType != 'nophase'		
		INNER JOIN tbl_users u ON u.UserID = d.StudentID
		INNER JOIN tbl_students s ON s.StudentID = d.StudentID AND s.FeeStatusID != 5
		INNER JOIN tbl_batch bb ON bb.BatchID = s.BatchID AND bb.CourseID = s.CourseID
		WHERE f.RemindWhen = 'after' AND f.RemindPhaseType != 'nophase' AND d.IsPaid = 0 AND d.ReceivedDate IS NULL AND DATE_ADD(d.DueDate, INTERVAL f.OnDays DAY) = CURDATE() AND DATE(d.DueDate) < CURDATE()	
		GROUP BY d.StudentID, f.RemindFor, f.RemindPhaseType
		ORDER BY d.StudentID ";		


		$MainQuery = $this->db->query($sql);

		if($MainQuery->num_rows()>0)
		{			
			$ParentSMS = "Dear {PARENT}, your wards fee installment of amount Rs.{DUE_AMT} is due on {DUEDATE}. Kindly pay at the earliest, please ignore if already paid. Regards {INST_NAME}";

			$FacultySMS = "Dear {FACULTY}, the student {STD_NAME} from your batch has fee due of Rs.{DUE_AMT} since {DUEDATE}. Ensure the student is{WORD} reminded to pay immediately.";

			$ManagementSMS = "Dear Business Owner, the student {STD_NAME} from batch {BATCH_NAME} has fee due of Rs.{DUE_AMT} since {DUEDATE}. Reminder to take necessary course of action.";
			

			foreach($MainQuery->result_array() as $MainRecord)
			{
				$EntityID = $StudentID = $MainRecord['StudentID'];

				$StdName = $FirstName = $MainRecord['FirstName'];
				$Mobile = $MainRecord['Mobile'];
				$Email = $MainRecord['Email'];
				$BatchName = $MainRecord['BatchName'];

				$RemindFor = $MainRecord['RemindFor'];
				$RemindPhaseType = $MainRecord['RemindPhaseType'];
				$EmailContent = $MainRecord['EmailContent'];
				$SmsContent = $MainRecord['SmsContent'];

				$RemindToParent = $MainRecord['RemindToParent'];
				$RemindToFaculty = $MainRecord['RemindToFaculty'];
				$RemindToManagement = $MainRecord['RemindToManagement'];

				$BatchID = $MainRecord['BatchID'];
				$CourseID = $MainRecord['CourseID'];
				$InstituteID = $MainRecord['InstituteID'];				


				$DueData = $this->getStudentFeeDetail($EntityID,$StudentID);
				
				if(isset($DueData['DueDates']) && !empty($DueData['DueDates']))
				{
					$DueDate = $DueData['DueDates'];
					$DueAmount = $DueData['InstallmentAmount'];
					$InstituteName = $DueData['InstituteName'];

					$EmailContent = str_replace("{STD_NAME}", $FirstName, $EmailContent);
					$EmailContent = str_replace("{DUE_AMT}", $DueAmount, $EmailContent);
					$EmailContent = str_replace("{DUEDATE}", $DueDate, $EmailContent);
					$EmailContent = str_replace("{INST_NAME}", $InstituteName, $EmailContent);

					$SmsContent = str_replace("{STD_NAME}", $FirstName, $SmsContent);
					$SmsContent = str_replace("{DUE_AMT}", $DueAmount, $SmsContent);
					$SmsContent = str_replace("{DUEDATE}", $DueDate, $SmsContent);
					$SmsContent = str_replace("{INST_NAME}", $InstituteName, $SmsContent);

					$Content = "";						
					if($RemindFor == "email")
					{
						$Content = $EmailContent;
						$SendTo = $Email;

						$ParentSendTo = trim($MainRecord['FathersEmail']);
					}
					else
					{
						$Content = $SmsContent;
						$SendTo = $Mobile;	

						$ParentSendTo = trim($MainRecord['FathersPhone']);						
					}	
				
					$data = array(
					"Subject" =>	"Fee Installment Due",				
					"Content" =>	$Content,
					"SendTo" => 	trim($SendTo),
					"SentType" => 	$RemindFor );
					$this->db->insert('cronjobs_queue', $data);
				
				
					$ParentSendTo = $MainRecord['FathersPhone'];
					//Send Only SMS Reminder to Parent-------------------------------
					if($RemindToParent == 1 && $ParentSendTo != "" && strlen($ParentSendTo) == 10)
					{
						$nmstr = $MainRecord['FathersName'];
						$nmarr = explode(" ", $nmstr);
						$FathersName = ucfirst(trim($nmarr[0]));

						$ParentContent = str_replace("{PARENT}", $FathersName, $ParentSMS);
						$ParentContent = str_replace("{DUE_AMT}", $DueAmount, $ParentContent);
						$ParentContent = str_replace("{DUEDATE}", $DueDate, $ParentContent);
						$ParentContent = str_replace("{INST_NAME}", $InstituteName, $ParentContent);

						$data = array(
						"Subject" =>	"Fee Installment Due - Parent",					
						"Content" =>	$ParentContent,
						"SendTo" => 	$ParentSendTo,
						"SentType" => 	'sms' );
						$this->db->insert('cronjobs_queue', $data);
					}


				

					//Send Reminder to Faculty-------------------------------
					if($RemindToFaculty == 1)
					{
						$sql = "SELECT IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, u.FirstName  
						FROM tbl_batchbyfaculty bf					
						INNER JOIN tbl_batch b ON b.BatchID = bf.BatchID
						INNER JOIN tbl_users u ON u.UserID = bf.FacultyID AND u.UserTypeID = 11					
						WHERE u.UserTypeID = 11 AND b.BatchID = '$BatchID' AND b.CourseID = '$CourseID' AND bf.AssignStatus = 2	";

						$SubQuery = $this->db->query($sql);
						if($SubQuery->num_rows()>0)
						{
							foreach($SubQuery->result_array() as $SubRecord)
							{								
								$FacultyMobile = trim($SubRecord['Mobile']);									
								
								if(isset($FacultyMobile) && !empty($FacultyMobile) && strlen($FacultyMobile) == 10)
								{
									if($RemindPhaseType == "phase3")
										$FacultySMSTxt = str_replace("{WORD}", " strictly", $FacultySMS);
									else
										$FacultySMSTxt = str_replace("{WORD}", "", $FacultySMS);

									$FacultyName = ucfirst(trim($SubRecord['FirstName']));

									$FacultyContent = str_replace("{FACULTY}", $FacultyName, $FacultySMSTxt);
									$FacultyContent = str_replace("{DUE_AMT}", $DueAmount, $FacultyContent);
									$FacultyContent = str_replace("{DUEDATE}", $DueDate, $FacultyContent);
									$FacultyContent = str_replace("{STD_NAME}", $StdName, $FacultyContent);

									$data = array(
									"Subject" =>	"Fee Installment Due - Faculty",					
									"Content" =>	$FacultyContent,
									"SendTo" => 	$FacultyMobile,
									"SentType" => 	'sms');
									$this->db->insert('cronjobs_queue', $data);
								}	
							}
						}		
					}	




					//Send Reminder to Management-------------------------------
					if($RemindToManagement == 1)
					{
						$sql = "SELECT IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile  
						FROM tbl_entity e					
						INNER JOIN tbl_users u ON u.UserID = e.EntityID AND u.UserTypeID = 10					
						WHERE e.EntityID = '$InstituteID'
						LIMIT 1	";

						$SubQuery = $this->db->query($sql);
						if($SubQuery->num_rows()>0)
						{
							foreach($SubQuery->result_array() as $SubRecord)
							{								
								$ManagementMobile = trim($SubRecord['Mobile']);
								
								if(isset($ManagementMobile) && !empty($ManagementMobile) && strlen($ManagementMobile) == 10)
								{			
									$ManagementContent = str_replace("{STD_NAME}", $StdName, $ManagementSMS);
									$ManagementContent = str_replace("{BATCH_NAME}", $BatchName, $ManagementContent);
									$ManagementContent = str_replace("{DUE_AMT}", $DueAmount, $ManagementContent);
									$ManagementContent = str_replace("{DUEDATE}", $DueDate, $ManagementContent);
									

									$data = array(
									"Subject" =>	"Fee Installment Due - Management",					
									"Content" =>	$ManagementContent,
									"SendTo" => 	$ManagementMobile,
									"SentType" => 	'sms' );
									$this->db->insert('cronjobs_queue', $data);
								}	
							}
						}		
					}
				}		



			}					
		}		

		return true;			
	}



	function getStudentFeeDetail($EntityID,$UserID)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);       

        $sql = "SELECT s.CourseID,s.BatchID,s.FeeID,s.InstallmentID,s.FeeStatusID,s.TotalFeeAmount,s.TotalFeeInstallments,s.InstallmentAmount
        FROM tbl_students s         			
        WHERE s.StudentID = '$UserID'
        LIMIT 1";

        $Query = $this->db->query($sql);
       
        $Records = array();
        $appdd = "";
        if($Query->num_rows() > 0)
        {
            foreach ($Query->result_array() as $record) 
            {   
                $appdd = "";

                if(!empty($record['CourseID']))
                {
                    $word = $record['CourseID'];
                    $appdd .= " AND CourseID = '$word'";
                }

                if(!empty($record['BatchID']))
                {
                    $word = $record['BatchID'];
                    $appdd .= " AND BatchID = '$word'";
                }
                    
                             
                                                  
                if(!empty($record['FeeID']))
                {
                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();                    
                    $record['InstallmentAmount'] = $record['TotalFee'];                     
                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {                        
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {                    
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];                    
                }
                else
                {
                	$record['InstallmentAmount'] = 0; 
                }


                //Due Dates-------------------------------------------
                $sql = "SELECT date_format(DueDate, '%d-%m-%Y') as DueDate 
                    FROM tbl_students_feeduedate 
                    WHERE StudentID = ".$UserID." AND IsPaid = 0 AND InstituteID = '".$InstituteID."' $appdd 
                    ORDER BY DueDate ASC 
                    LIMIT 1";
                $query = $this->db->query($sql);
                $DueDates = $query->result_array();
                $record['DueDates'] = array();
                if(isset($DueDates) && !empty($DueDates))
                {
                    $record['DueDates'] = $DueDates[0]['DueDate'];
                }


                //Institute Name-------------------------------------------
                $sql = "SELECT u.FirstName as Name
		        FROM tbl_users u 		        
		        INNER JOIN tbl_entity e ON u.UserID = e.InstituteID	AND e.InstituteID = '$InstituteID'		
		        WHERE u.UserID = '$InstituteID'
		        LIMIT 1";

		        $Query = $this->db->query($sql);
		        $Name = $Query->result_array();
		        $record['InstituteName'] = "";
		        if(isset($Name) && !empty($Name))
                {
                    $record['InstituteName'] = $Name[0]['Name'];
                }

                $Records = $record;
            }
            
            return $Records;
        }
        else
        {
            return $Records;
        }
    }



    function scholarshipTestEdited()
	{				
		$EMAIL_template = "Dear USER_NAME, <br/><br/>The scholarshipt test <b>TEST_NAME</b> of <b>INSTITUTE_NAME</b> institute, on which you applied is modified.<br/><br/>Updated details are as follows:<br/>Scholarshipt Test Name: TEST_NAME<br/>Schedule Date: TEST_DATE<br/>Activation Time: ACTIVATION_TIME<br/>Test Time: TEST_TIME<br/>Duration: DURATION<br/><br/>Please login on Iconik website to view the details.<br/><br/>Regards<br/>Iconik Team";

		$SMS_template = "The scholarshipt test TEST_NAME of INSTITUTE_NAME institute, on which you applied is modified. Please login to view the details.";


		$sql = "SELECT cj.CronID 
		FROM cronjobs cj
		INNER JOIN tbl_question_paper tp ON tp.QtPaperID = cj.TargetIds		
		WHERE cj.CronFor = 'scholarship_test' AND cj.IsSent = 0				
		ORDER BY cj.CronID
		LIMIT 1	";			

		$MainQuery = $Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			$sql = "SELECT * FROM
			(
				SELECT u.FirstName, IF(u.PhoneNumber IS NULL, u.PhoneNumberForChange, u.PhoneNumber) as Mobile, IF(u.Email IS NULL, u.EmailForChange, u.Email) as Email, tp.QtPaperTitle, tp.ScheduleDate, tp.TestActivatedFrom, tp.TestActivatedTo, tp.TestStartTime, tp.TestEndTime, tp.Duration, tp.EntityID 
				FROM tbl_scholarship_applicants app
				INNER JOIN cronjobs cj ON app.QtPaperID = cj.TargetIds AND cj.CronFor = 'scholarship_test' AND cj.IsSent = 0
				INNER JOIN tbl_entity e ON app.StudentID = e.EntityID
				INNER JOIN tbl_question_paper tp ON app.QtPaperID = tp.QtPaperID			
				INNER JOIN tbl_users u ON u.UserID = app.StudentID				
				WHERE app.Status IN('Success', 'success') AND DATE(app.ValidUpto) >= CURDATE() AND cj.CronFor = 'scholarship_test' AND cj.IsSent = 0
			) as temp
			WHERE Mobile != ''
			ORDER BY FirstName ";

			$Query = $this->db->query($sql);

			if($Query->num_rows()>0)
			{			
				foreach($Query->result_array() as $Record)
				{
					$EMAILContent = $EMAIL_template;
					$SMSContent = $SMS_template;

					$InstituteName = "Iconik"; $InstituteNameSuffix = "";
					$UserInfo = $this->getUserDetailsById($Record['EntityID']);
					if(isset($UserInfo) && !empty($UserInfo))
					{
						$InstituteName = ucfirst(trim($UserInfo['FirstName']));
						$InstituteNameSuffix = ucfirst(trim($UserInfo['LastName']));
					}

					$FirstName = ucfirst(trim($Record['FirstName']));
					$Mobile = trim($Record['Mobile']);
					$Email = trim($Record['Email']);

					
					$QtPaperTitle = trim($Record['QtPaperTitle']);
					
					$ScheduleDate = date("d-m-Y", strtotime(trim($Record['ScheduleDate'])));
					
					$TestActivatedFrom = date("H:i",strtotime(trim($Record['TestActivatedFrom'])))."-".date("H:i",strtotime(trim($Record['TestActivatedTo'])));					
					
					$TestStartTime = date("H:i",strtotime(trim($Record['TestStartTime'])))."-".date("H:i",strtotime(trim($Record['TestEndTime'])));			
					
					$Duration = trim($Record['Duration'])." min.";


					$EMAILContent = str_replace("USER_NAME", $FirstName, $EMAILContent);
					$EMAILContent = str_replace("TEST_NAME", $QtPaperTitle, $EMAILContent);
					$EMAILContent = str_replace("TEST_DATE", $ScheduleDate, $EMAILContent);
					$EMAILContent = str_replace("ACTIVATION_TIME", $TestActivatedFrom, $EMAILContent);
					$EMAILContent = str_replace("TEST_TIME", $TestStartTime, $EMAILContent);
					$EMAILContent = str_replace("DURATION", $Duration, $EMAILContent);
					$EMAILContent = str_replace("INSTITUTE_NAME", $InstituteName." ".$InstituteNameSuffix, $EMAILContent);


					$SMSContent = str_replace("USER_NAME", $FirstName, $SMSContent);
					$SMSContent = str_replace("TEST_NAME", $QtPaperTitle, $SMSContent);
					$SMSContent = str_replace("INSTITUTE_NAME", $InstituteName, $SMSContent);
					/*$EMAILContent = str_replace("TEST_DATE", $ScheduleDate, $SMSContent);
					$SMSContent = str_replace("ACTIVATION_TIME", $TestActivatedFrom, $SMSContent);
					$SMSContent = str_replace("TEST_TIME", $TestStartTime, $SMSContent);
					$SMSContent = str_replace("DURATION", $Duration, $SMSContent);*/


					$data = array(
					"Subject" =>	"Scholarship Test Details Updated - Iconik",
					"Content" =>	$EMAILContent,					
					"SendTo" => 	$Email,
					"SentType" => 	'email'
					);
					$this->db->insert('cronjobs_queue', $data);


					$data = array(
					"Subject" =>	"Scholarship Test Details Updated - Iconik",
					"Content" =>	$SMSContent,
					"SendTo" => 	$Mobile,
					"SentType" => 	'sms'
					);
					$this->db->insert('cronjobs_queue', $data);
				}
			}


			$date = date("Y-m-d H:i:s");

			$sql = "UPDATE cronjobs 
			SET IsSent = 1, UpdateDate = '$date'
			WHERE CronFor = 'scholarship_test' AND IsSent = 0 ";			

			$this->db->query($sql);
			
				
		}		

		return true;			
	}

    
}