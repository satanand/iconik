<style type="text/css">
	.content {
	 /* width: 240px;*/
	  overflow: hidden;
	  word-wrap: break-word;
	  text-overflow: ellipsis;
	  line-height: 18px;
	  text-align: center;
	}
	.less {
	  max-height: 54px;
	}
	.menu-text {
	    /*display: block !important;*/
	}
	.modal { overflow: auto !important; }
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}
	.tox-statusbar__branding{
		display: none;
	}
</style>
<!-- Head -->
<header class="panel-heading">
	<h1 class="h4">Manage Signature</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<!-- Head/ -->
<div class="panel-body" ng-controller="PageController" id="content-body" ng-init="getSignature()"><!-- Body -->

	<div class="clearfix mt-2 mb-2">
	 	<div class="row float-left ml-6">
		  <div class="col-md-6">
	      	<div class="form-group">
		      <span class="float-left records hidden-sm-down">
				<span class="h5" ng-if="signature.length">Total records: {{signature.length}}</span>
				<span class="h5" ng-if="!signature.length">Total records: 0</span>
			  </span>
			 </div>
			</div>
	      <div class="col-md-6">
	      	<div class="form-group">
			 	<form class="form-inline" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()"> 
			 		<div id="universal_search">
		                <input type="text" class="form-control" name="Keyword" placeholder="Type and search"  autocomplete="off" onkeyup="SearchTextClear(this.value)">
		                <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
		                <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
		            </div>
		         </form>
			 </div>
		  </div>     

	   </div>
	   <div class="float-right mr-2">		
			<button class="btn btn-success btn-sm ml-1 float-right" id="add-content-btn" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Signature</button>
		</div>
	</div>

	<!-- <div class="clearfix mt-2 mb-2">
	   <div class="row float-left ml-6" style="width:80%;">
	      <div class="col-md-6">
	         <div class="form-group">
	            <select class="form-control" ng-change="getCategoryFilterData(CategoryGUID)" name="ParentCat" id="Courses" required="">
	               <option value="{{data.ParentCategoryGUID}}">Select Course</option>
	               <option ng-repeat="row in categoryDataList" value="{{$index}}">{{row.CategoryName}}</option>
	            </select>
	         </div>
	      </div>
	      <div class="col-md-6">
	         <div class="form-group">
	            <div id="subcategory_section">
	               <select name="CategoryGUIDs" ng-change="getCategoryFilterData(CategoryGUID)"  class="form-control" onchange="formDataCheck()" onchange="check_subject_available()" required="">
	                  <option value="">Select Subject</option>
	               </select>
	            </div>
	            <a  class="right_link Add_Subject" id="link_add_subject" ng-click="loadFormSubjectAdd(ParentCategoryGUID)" style="display: none;cursor: pointer;
	               color: blue;
	               text-decoration: underline;">Add New Subject</a>
	         </div>
	      </div>
	   </div>
	   <div class="float-right mr-2">		
	      <button class="btn btn-success btn-sm ml-1 float-right" id="add-content-btn" ng-click="loadFormAdd();">Add Content</button>
	   </div>
	</div> -->

	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">
	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">


	<!-- Data table -->
	<div class="table-responsive block_pad_md"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		<table class="table table-striped table-hover">
			<!-- table heading -->
			<thead>
				<tr>
					<!-- <th style="width: 100px; text-align: center;">S.NO</th> -->
					<th style="width: 100px; text-align: center;">From Name</th>
					<th style="width: 100px; text-align: center;">From Email</th>
					<th style="text-align: center;">Signature</th>
					<th style="width: 100px; text-align: center;">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row" ng-repeat="(key, row) in signature">
					<td style="text-align: center;">{{row.FromName}}</td>
					<td style="text-align: center;">{{row.FromEmail}}</td>
					<td style="text-align: center;"><p ng-bind-html="row.Signature"></p></td>


					

					<td class="text-center" style="text-align: center;">
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.SignatureGUID)" data-toggle="tooltip" title="edit"></a>
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.SignatureGUID)" data-toggle="tooltip" title="Delete"></a>

					
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="!signature.length">
			<span ng-if="signature.length">No more records found.</span>
			<span ng-if="!signature.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->




	<!-- add Modal -->
	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Signature</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="add_subject">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Subjcet</h3>     	
					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="subjectTemplateURLAdd"></div>
			</div>
		</div>
	</div>

	<!-- edit Modal -->
	<div class="modal fade" id="edits_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLEdit"></div>
			</div>
		</div>
	</div>


	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>


	<!-- delete Modal -->
	<div class="modal fade" id="delete_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
</div><!-- Body/ -->