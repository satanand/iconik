<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Industry extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Industry_model');
		$this->load->model('Common_model');
	}



	

	/*
	Description: 	Use to get Get single category.
	URL: 			/api/category/getCategories
	Input (Sample JSON): 		
	*/
	public function getIndustry_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('IndustryID', 'IndustryID', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this); 
		
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		//print_r($this->Post); die();

		$RolesData = $this->Industry_model->getIndustry($this->EntityID,$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if(!empty($RolesData)){
			$this->Return['Data'] = $RolesData['Data'];
		}	
	}

	/*

	Description: 	Use to add new Industry

	URL: 			/api_admin/Industry/add	

	*/

	public function addIndustry_post()

	{

		/* Validation section */
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('IndustryName', 'Industry Name', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

        //$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$IndustryStatus = $this->Industry_model->addIndustry($this->EntityID,$this->input->Post());

		//if($RolesData){

			if($IndustryStatus == 1){
				$this->Return['Message']      	=	"New Industry Added successfully.";				
			}else  {
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"This Industry name already exist.";
			}

		//}

	}




	/*

	Name: 			editRoles_post
	

	*/
	public function editIndustry_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('IndustryName', 'IndustryName', 'trim|required');
		$this->form_validation->set_rules('Description', 'Description', 'trim');
		$this->form_validation->set_rules('IndustryID', 'IndustryID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->Industry_model->editIndustry($this->EntityID, $this->input->post('IndustryID'), $this->input->Post());
	
		$this->Return['Data']= $this->Industry_model->getIndustry($this->EntityID, array("IndustryID"=>$this->input->post('IndustryID')));

	    $this->Return['Message']      	=	"Updated successfully."; 

	}


	public function deleteIndustry_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|required|callback_validateSession');
		$this->form_validation->set_rules('IndustryID', 'IndustryID', 'trim');
		$this->form_validation->set_rules('StatusID', 'StatusID', 'trim');
		$this->form_validation->validation($this);  /* Run validation */		  /* Run validation */		

		/* Validation - ends */

		//print_r($this->input->Post()); die;
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
	
		$this->Return['Data']= $this->Industry_model->deleteIndustry($this->EntityID, array("IndustryID"=>$this->input->post('IndustryID'),"StatusID"=>$this->input->post('StatusID')));

	    $this->Return['Message']      	=	"Deleted successfully."; 

	}
	
}