<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recovery extends API_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Recovery_model');
	}

	/*
	Name: 			Recovery Password
	Description: 	Use to set OTP and send to user for password recovery.
	URL: 			/api/recovery
	*/
	public function index_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$this->Return['Message']   ="Please check your email for instructions."; 
        $recovery=$this->Recovery_model->recovery($this->Post['Keyword']);
       // echo  $recovery; die();
	    if(!empty($recovery)){
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"If your account is registered here you will receive an email from us.";
		}
	}

	/*
	Name: 			Set Password From OTP
	Description: 	Use to set user password from OTP.
	URL: 			/api/recovery/setPassword
	*/
	public function setPassword_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('OTP', 'OTP', 'trim|required|callback_validateToken[1]');
		//$this->form_validation->set_rules('Password', 'Password', 'trim|required');
		$this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('retype', 'Confirm Password', 'trim|required|matches[Password]');
		//$this->form_validation->set_rules('Retype', 'Confirm Password', 'trim|matches[Password]');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$UserID = $this->Recovery_model->verifyToken($this->Post['OTP'],1);

		if($this->Users_model->updateUserLoginInfo($UserID, array("Password"=>$this->Post['Password']), DEFAULT_SOURCE_ID)){
			$this->Recovery_model->deleteToken($this->Post['OTP'],1); /*delete token*/
			$this->Return['Message']      	=	"New password has been set, please login to get access your account."; 	
		}
	}


	public function contactUs_post(){
		/* Validation section */
		$this->form_validation->set_rules('inputName', 'Name', 'trim|required');
		$this->form_validation->set_message('inputName', 'Name is required');
		$this->form_validation->set_rules('inputEmail', 'Email', 'trim|valid_email|required');
		$this->form_validation->set_message('inputEmail', 'Email is required');
		$this->form_validation->set_rules('inputMessage', 'Message', 'trim|required');
		$this->form_validation->set_message('inputMessage', 'Message is required');
		$this->form_validation->validation($this);  /* Run validation */		
		//print_r($this->Post); die;

		$content = $this->load->view('emailer/iconik-contact-us', array(
				"Name" 			=> 	$this->Post['inputName'],
				'Email' 		=> 	$this->Post['inputEmail'],
				'Message' 		=> 	$this->Post['inputMessage'],
				) , TRUE);

		sendMail(array(
			'emailTo' => 'contact@iconik.in',
			'emailSubject' => "Iconik : Contact Us",
			'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));
	}
}
