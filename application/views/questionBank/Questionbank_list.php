<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Question Repository</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getList()" ><!-- Body -->

	

	<div class="clearfix mt-2 mb-2">


		<div class="row float-left ml-6" style="width:100%;">  	

	      <div class="col-md-12">

	         <div class="form-group">

	           <button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Question</button>

				<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadImportQuestion();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Import Questions</button>


	         </div>

	      </div>

	   </div>


	   <div class="row float-left ml-6" style="width:100%;">

	     
	   		<div class="col-md-3">
			<div class="form-group">
			<label for="inputName" class="control-label mb-10">Select Course</label>
			<select class="form-control" onchange="getSubCategoryList(this.value, 's');" name="ParentCat" id="Courses">

			   <option value="">Select Course</option>

			   <option ng-repeat="row in categoryDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

			</select>
			</div>
			</div>


			<div class="col-md-3">
			<div class="form-group">
			<label for="inputName" class="control-label mb-10">Select Subject</label>
			<div>	
				<select name="CategoryGUIDs" id="Subjects" class="form-control" onchange="getSubCategoryList(this.value, 't')">

			      <option value="">Select Subject</option>

			      <option ng-repeat="row in subCategoryDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

			   </select>
			</div>
			</div>
			</div>


			<div class="col-md-3">
			<div class="form-group">
			<label for="TopicID" class="control-label mb-10">Select Topic</label>

			<div>
				<select id="Topics" name="Topics"  class="form-control">

			      <option value="">Select Topic</option>

			      <option ng-repeat="row in subTopicDataList" value="{{row.CategoryID}}">{{row.CategoryName}}</option>

			   </select>
			</div>
			</div>
			</div>


			<div class="col-md-3">
				<div class="form-group"><br/>
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records('sta');"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records('sta');"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>

	    </div> 


	    

	   <div class="float-right mr-2">		   		

			
		</div>

	</div>



	<input type="hidden" name="ParentCategoryGUIDIndex" id="ParentCategoryGUIDIndex">

	<input type="hidden" name="ParentCategoryGUID" id="ParentCategoryGUID">





	<!-- Data table -->

	<div class="table-responsive block_pad_md"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		

		<!-- data table -->

		<table class="table table-striped table-hover" id="tableToExport">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 150px;">Course</th>

					<th style="width: 150px;">Subject</th>

					<th style="width: 150px;">Topic</th>

					<th style="width: 70px;">Questions</th>

					<th style="width: 70px; text-align: center;">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody ng-repeat="(key, row) in data.dataList">

				<tr scope="row"  ng-if="row.TopicName == 'Total'" style="background: #EAECEE;">

					<td >{{row.CourseName}}</td>

					<td >{{row.SubjectName}}</td>

					<td ><b>{{row.TopicName}}</b></td>

					<td ><b>{{row.question_count}}</td>					

					<td class="text-center" style="text-align: center;">						

					</td>

				</tr>



				<tr scope="row" ng-if="row.TopicName != 'Total'">					
					<td >{{row.CourseName}}</td>

					<td >{{row.SubjectName}}</td>

					<td >{{row.TopicName}}</td>

					<td >{{row.question_count}}</td>
					
					<td class="text-center" style="text-align: center;">

						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'questionbank/questions_list/?CourseGUID={{row.CourseGUID}}&SubjectGUID={{row.SubjectGUID}}&TopicGUID={{row.TopicGUID}}'; ?>" data-toggle="tooltip" title="Questions list"></a>

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Question</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_subject">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Subjcet</h3>     	

					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="subjectTemplateURLAdd"></div>

			</div>

		</div>

	</div>




	<!-- edit Modal -->

	<div class="modal fade" id="import_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Import Data</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLImport"></div>

			</div>

		</div>

	</div>







	<!-- edit Modal -->

	<div class="modal fade" id="edit_model">

		<div class="modal-dialog modal-lg" role="document" style="max-width: 85%;">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>





	<!-- edit Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>

</div><!-- Body/ -->