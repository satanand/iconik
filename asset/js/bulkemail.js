app.controller('PageController', function ($scope, $http,$timeout){
  $scope.show_students = false;
  $scope.data.SecondLevelPermission = [];

  $scope.applyFilter = function() {
      $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
      $scope.getSignature();
  }

  $scope.getFilterData = function (check="",CourseGUID="")
  {
      if(!$scope.data.SecondLevelPermission.length){
          $scope.getSecondLevelPermission();
      }

       $scope.noRecords = false;

      var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

      $scope.data.listLoading = true;

      $http.post(API_URL+'bulksms/getCourseAndBatch', data, contentType).then(function(response) {

          var response = response.data;

          if(response.ResponseCode==200 && response.Data){ /* success case */

           $scope.filterData =  response.Data;

           $scope.data.listLoading = false;

          if($scope.filterData.length > 0){
            $scope.noRecords = false;
          }else{
            $scope.noRecords = true;
          }

          }else{
            $scope.noRecords = true;
          }
       });

       if(check!=1){

          $scope.getSignature(1);
          $scope.getRoles();
          $scope.getStaffList();
          $scope.categoryDataList = [];
          $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
              var response = response.data;
              if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                  $scope.data.totalRecords = response.Data.TotalRecords;
                  for (var i in response.Data.Records) {
                      for(var j in response.Data.Records[i].SubCategories.Records){                           
                          $scope.categoryDataList.push(response.Data.Records[i].SubCategories.Records[j]);
                      } 
                  }          
              } ////console.log($scope.categoryDataList);
          });


           $timeout(function(){            

               tinymce.init({selector: '#Signature, #Message' });

           }, 600);


          
      }



  }


   $scope.getRoles = function ()
  {
      $scope.data.listLoading = true;
      $scope.data.roles = [];
      var data = 'SessionKey='+SessionKey;

      $http.post(API_URL+'roles/getRoles', data, contentType).then(function(response) {

             var response = response.data;

             if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                 // $scope.data.totalRecords = response.Data.TotalRecords;

                  for (var i in response.Data.Records) {

                   $scope.data.roles.push(response.Data.Records[i]);
                  }
          

             }

            $scope.data.listLoading = false;
      });
  }


  $scope.getStaffList = function($UserTypeID=""){
      var data = 'SessionKey='+SessionKey+'&IsAdmin=Yes&UserTypeID='+$UserTypeID;
       $scope.data.listLoading = true; 
     $scope.data.staffList = [];
     $scope.UserTypeID = UserTypeID;
      $http.post(API_URL+'bulksms/getStaffList', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) {

                   $scope.data.staffList.push(response.Data[i]);

               }

               $scope.data.listLoading = false; 

               $scope.data.pageNo++;               
            }else{

                $scope.data.noRecords = true;
            }

             $scope.data.listLoading = false; 

            $scope.data.listLoading = false;

      });
  }



  $scope.selectUser = function(data,check="")
  {
    console.log('.chkSelect'+data);

    if($('.course_details' + data).is(':checked')){
      $('.batch_details'+data).prop("checked", false);
    }

    if (($('#course' + data).is(':checked') || $('#batch' + data).is(':checked')) || ($('#course' + data).is(':checked') && $('#batch' + data).is(':checked'))) {
     // $('#batch' + data).prop("checked", true);
     // $('#course' + data).prop("checked", true);
      $('.course_details'+data).prop("checked", true);
      $('.batch_details'+data).prop("checked", true);
    }else {
      //$('#batch' + data).prop("checked", false);
      //$('#course' + data).prop("checked", false);
      $('.course_details'+data).prop("checked", false);
      $('.batch_details'+data).prop("checked", false);
    }
  } 


  $scope.selectStaff = function(data,check="")
  {
    console.log('.chkSelect'+data);

    if($('.course_details' + data).is(':checked')){
      $('.staff_details'+data).prop("checked", false);
    }

    if (($('#role' + data).is(':checked') || $('#staff' + data).is(':checked')) || ($('#role' + data).is(':checked') && $('#staff' + data).is(':checked'))) {
     // $('#batch' + data).prop("checked", true);
     // $('#course' + data).prop("checked", true);
      // $('#role' + data).prop("checked", true);
      // $('#staff' + data).prop("checked", true);
      $('.course_details'+data).prop("checked", true);
      $('.staff_details'+data).prop("checked", true);
    }else {
      //$('#batch' + data).prop("checked", false);
      //$('#course' + data).prop("checked", false);
      $('#role' + data).prop("checked", false);
      $('#staff' + data).prop("checked", false);
      $('.course_details'+data).prop("checked", false);
      $('.staff_details'+data).prop("checked", false);
    }
  } 

  /*add data*/
  $scope.addData = function ()
  {
      $scope.addDataLoading = true;
      tinyMCE.triggerSave();
      var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
      $http.post(API_URL+'bulksms/addSignature', data, contentType).then(function(response) {
          var response = response.data;
          if(response.ResponseCode==200){ /* success case */               
              SuccessPopup(response.Message);
              $('#add_model .close').click();
              $scope.getSignature(1);
          }else{
              ErrorPopup(response.Message);
          }
          $scope.addDataLoading = false;          
      });
  }


  /*add data*/
  $scope.editData = function ()
  {
      $scope.addDataLoading = true;
      tinyMCE.triggerSave();
      console.log($scope.formData);
      // $scope.formData
      var data = 'SessionKey='+SessionKey+'&SignatureGUID='+$scope.formData.SignatureGUID+'&'+$("form[name='edit_form']").serialize();
      $http.post(API_URL+'bulksms/editSignature', data, contentType).then(function(response) {
          var response = response.data;
          if(response.ResponseCode==200){ /* success case */               
              SuccessPopup(response.Message);
              $('#edits_model .close').click();
              $scope.getSignature(1);
          }else{
              ErrorPopup(response.Message);
          }
          $scope.addDataLoading = false;          
      });
  }


  $scope.showTextBox = function (type){
    if(type == 'CC'){
      $("#CC_text_box").toggle();
    }else if(type == 'BCC'){
      $("#BCC_text_box").toggle();
    }
  }



  $scope.getSignature = function (check="",SignatureGUID="")
  {
      if(!$scope.data.SecondLevelPermission.length){
          $scope.getSecondLevelPermission();
      }

      if(check!=1){
        if ($scope.data.listLoading || $scope.data.noRecords) return;
      }else{
        $scope.data.pageNo = 1;
      }
      
      $scope.listLoading = true;
      tinyMCE.triggerSave();
      var data = 'SessionKey='+SessionKey+'&SignatureGUID='+SignatureGUID+'&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize+'&'+$('#filterForm').serialize();
      $http.post(API_URL+'bulksms/getSignature', data, contentType).then(function(response) {
          var response = response.data;
          if(response.ResponseCode==200 && response.Data.length){ /* success case */ 
            if(SignatureGUID.length && check == 1) {
              $scope.SignatureData = response.Data[0].Signature;
              //alert($scope.SignatureData);
              tinymce.get("Signature").setContent($scope.SignatureData);
              $("#FromName").val(response.Data[0].FromName);
              $("#FromEmail").val(response.Data[0].FromEmail);
              //$("#Signature").val($scope.SignatureData);
            }else{
              $scope.signature = response.Data;
            }             
             
             $scope.data.pageNo++;
          }else {
            $scope.FromName = FromName;
            $scope.FromEmail = FromEmail;
            $scope.signature = [];
            $scope.data.noRecords = true;
          }
          $scope.listLoading = false;          
      });
  }


  $scope.loadFormEdit = function(Position,SignatureGUID)
  {

  		$scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE+'bulkemail/edit_signature.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'bulksms/getSignature', 'SessionKey='+SessionKey+'&SignatureGUID='+SignatureGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data[0];

                $('#edits_model').modal({show:true});

                $timeout(function(){            

                   tinymce.init({selector: '#Signature' });

               }, 600);

            }

        });
  }


  $scope.loadFormDelete = function(key,SignatureGUID)
    {
        $scope.deleteDataLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'bulksms/deleteSignature', 'SessionKey='+SessionKey+'&SignatureGUID='+SignatureGUID, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.getSignature(1);
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


  $scope.showStudents = function(data)
  {
     if(data=="Students"){
      $scope.show_students = true;
     }else{
      $scope.show_students = false;
     }
  }




  $scope.sendEmail = function(){
    tinyMCE.triggerSave();
    $scope.addDataLoading = true;
    tinyMCE.triggerSave();
      var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
      //console.log(data);
      $http.post(API_URL+'bulksms/sendEmail', data, contentType).then(function(response) {
          var response = response.data;
          if(response.ResponseCode==200){ /* success case */               
              SuccessPopup(response.Message);
              //$scope.getFilterData();
              $timeout(function(){
                  location.reload();
              }, 2000);
          }else{
              ErrorPopup(response.Message);
          }
          $scope.addDataLoading = false;          
      });
  }


   /*load add form*/
    $scope.loadBuyCredit = function (Position)
    {
     

      $scope.data.pageLoading = true;

      $scope.data.Position = Position;

      $scope.templateURLAdd = PATH_TEMPLATE+'bulkemail/sms_credits.htm?'+Math.random();

        $scope.data.pageLoading = false;

        $('#add_model').modal({show:true});

       
    }


    $scope.loadFormAdd = function ()



    {

        $scope.templateURLAdd = PATH_TEMPLATE+'bulkemail/add_signature.htm?'+Math.random();



        $('#add_model').modal({show:true});



        $timeout(function(){            



           tinymce.init({selector: '#Signature' });



       }, 700);



    }


});

function showStudents(data)
{
   if(data=="Students"){
    $("#show_staff").hide();
    $("#show_students").show();
    $('.UserGUID').prop("checked", false);
    $(".rolecheck").prop("checked", false);
   }else if(data=="Staff"){
    $("#show_students").hide();
    $("#show_staff").show();
    $(".coursecheck").prop("checked", false);
    $('.batch_details').prop("checked", false);
   }else{
    $("#show_staff").hide();
    $("#show_students").hide();
    $(".coursecheck").prop("checked", false);
    $(".rolecheck").prop("checked", false);
    $('.UserGUID').prop("checked", false);
    $('.batch_details').prop("checked", false);
   }
}

function getStaffList(UserTypeID){
  angular.element(document.getElementById('content-body')).scope().getStaffList(UserTypeID);  
}