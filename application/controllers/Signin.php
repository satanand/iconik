<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Signin extends Main_Controller {

	
	public function index()



	{
		
		

		if(!empty($this->session->userdata('UserData'))){
			redirect(base_url().'dashboard');
			exit;
		}

		// $data = parse_str($_SERVER['QUERY_STRING'], $_GET); 

		// print_r($_GET);

		// die;

		if(!empty($_GET['Username']) && !empty($_GET['Password'])){



			$JSON = json_encode(array(



				"Username" 		=> @$_GET['Username'],



				"Password" 		=> @$_GET['Password'],


				"g-recaptcha-response" => @$_GET['g-recaptcha-response'],



				"Source" 		=> 'Direct',



				"DeviceType" 	=> 'Native'



			));



			$Response = APICall(API_URL.'admin/signin', $JSON); /* call API and get response */

			print_r($Response); die;

			if($Response['ResponseCode'] == 200){ /*check for admin type user*/

				//print_r($Response['Data']); die;

				$this->session->set_userdata('UserData',$Response['Data']); /* Set data in PHP session */

				redirect('/dashboard', 'refresh');

			}


			//redirect(base_url().'dashboard');
			response($Response);



			exit;



		}

		else if(!empty($this->Post)){



			$JSON = json_encode(array(



				"Username" 		=> @$this->Post['Username'],



				"Password" 		=> @$this->Post['Password'],


				"g-recaptcha-response" 		=> @$this->Post['g-recaptcha-response'],



				"Source" 		=> 'Direct',



				"DeviceType" 	=> 'Native'



			));



			$Response = APICall(API_URL.'admin/signin', $JSON); /* call API and get response */

			//print_r($Response); die;

			if($Response['ResponseCode'] == 200){ /*check for admin type user*/

				//print_r($Response['Data']); die;

				$this->session->set_userdata('UserData',$Response['Data']); /* Set data in PHP session */

				//print_r($this->session->userdata('UserData')); die;

			}


			//redirect(base_url().'dashboard');
			
			response($Response);



			exit;



		}







		/* load view */



		$load['js']=array(



			'asset/js/signin.js'



		);	



		$this->load->view('includes/header',$load);



		$this->load->view('signin/signin');



		$this->load->view('includes/footer');



	}





	public function signout($SessionKey='')  

	{  

		$JSON = json_encode(array(

			"SessionKey" 	=> $SessionKey

		));

		APICall(API_URL.'signin/signout/', $JSON); /* call API and get response */

		$this->session->sess_destroy();

		redirect(base_url(),'refresh');

		exit();  		

	}
}