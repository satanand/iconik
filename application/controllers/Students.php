<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends Admin_Controller_Secure {

	

	/*------------------------------*/

	/*------------------------------*/	

	public function index()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'

		);

		$load['js']=array(

			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',			

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'asset/js/excelexportjs.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
			
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Students" => "",
			"Students" => ""
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('students/Students_statistics');

		$this->load->view('includes/footer');

	}



	public function students_list()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'

		);

		$load['js']=array(

			'asset/js/students.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js',

			'asset/js/excelexportjs.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
			
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Students" => base_url()."students",
			"Students" => "",
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('students/Students_list');

		$this->load->view('includes/footer');

	}


	public function pending_profile()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/students.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js',

			'asset/js/excelexportjs.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Students" => base_url()."students",
			"Pending Profiles" => "",
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('students/Pending_profile');

		$this->load->view('includes/footer');

	}


	public function mp_students(){
		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/students.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',

			'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js',

			'asset/js/excelexportjs.js'

		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Students" => base_url()."students",
			"Students" => "",
		);

		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('students/MP_students_list');

		$this->load->view('includes/footer');
	}

}