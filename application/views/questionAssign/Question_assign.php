<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Assign Paper</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getQuestionAssignStatistics()"><!-- Body -->

	<div class="clearfix mt-4 mb-2">

	   <div class="row float-left ml-6" style="width:80%;">

	      <div class="col-md-4" style="margin-left: 35px;">

	         <div class="form-group">

	            <select class="form-control" onchange="getSubCategoryList(this.value,'getQuestionAssignStatistics')" name="ParentCat" id="Batch">

	               <option value="">Select Batch</option>

	               <option ng-repeat="row in batch" value="{{row.BatchGUID}}" ng-selected="row.BatchGUID == BatchGUID">{{row.BatchName}}</option>

	            </select>

	         </div>

	      </div>

	      <div class="col-md-4">

	         <div class="form-group">

	            <div id="subcategory_section">

	               <select id="subject" name="CategoryGUIDs" onchange="filterAssignPaperStatistics(this.value)"  class="form-control">

	                  <option value="">Select Subject</option>

	                  <option ng-repeat="row in subject" value="{{row.CategoryGUID}}" ng-selected="row.CategoryGUID == SubjectGUID">{{row.CategoryName}}</option>

	               </select>

	            </div>

	         </div>

	      </div>

	   </div>

	   <div class="float-right mr-2">		
			<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadAssignPaper1();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Assign Test</button>
		</div>

	</div>



	<!-- Data table -->

	<div class="table-responsive block_pad_md"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr>

					<th style="width: 250px;">Batch</th>

					<th >Subject</th>

					<th style="width: 10px;">Test</th>

					<th style="width: 100px; text-align: center;">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody ng-repeat="(key, row) in data.dataList">

				<tr scope="row"  ng-if="row.SubCategoryName == 'Total'" style="background: #EAECEE;">

					<td >{{row.ParentCategoryName}}</td>

					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.SubCategoryName}}</b></td>

					<td  ng-if="row.SubCategoryName != 'Total'">{{row.SubCategoryName}}</td>

					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.total_assign_count}}</b></td>

					<td  ng-if="row.SubCategoryName != 'Total'">{{row.assign_paper_count}}</td>

					<td class="text-center" style="text-align: center;">

						<!-- <a class="glyphicon glyphicon-edit" href="<?php //echo base_url().'questionbank/questions_list/?parentCategoryGUID={{row.parentCategoryGUID}}&CategoryGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Questions list"></a> -->

					</td>

				</tr>

				<tr scope="row" ng-if="row.SubCategoryName != 'Total'">

					<td>{{row.ParentCategoryName}}</td>

					<td  ng-if="row.SubCategoryName == 'Total'"><b>{{row.SubCategoryName}}</b></td>

					<td  ng-if="row.SubCategoryName != 'Total'">{{row.SubCategoryName}}</td>

					<td ng-if="row.SubCategoryName == 'Total'"><b>{{row.total_assign_count}}</b></td>

					<td ng-if="row.SubCategoryName != 'Total'">{{row.assign_paper_count}}</td>

					<td class="text-center" style="text-align: center;">

						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'questionassign/list/?BatchGUID={{row.parentCategoryGUID}}&CategoryGUID={{row.CategoryGUID}}'; ?>" data-toggle="tooltip" title="Questions Paper"></a>

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span>No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	<div class="modal fade" id="assign_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Assign Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAssign"></div>

			</div>

		</div>

	</div>

</div><!-- Body/ -->