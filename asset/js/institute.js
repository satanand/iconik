app.controller('PageController', function($scope, $http, $timeout) {

    /*list*/
    $scope.applyFilter = function() {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }


     $scope.getFilterData = function (){
        $scope.getStates();
        $scope.getSecondLevelPermission();
       //$scope.getList();
     }


    $scope.loadImportInstitute  = function ()
    {
        //alert("loadImportInstitute");
        $scope.templateURLImport = PATH_TEMPLATE+'institute/import_inst.htm?'+Math.random();
        $('#import_excel_model').modal({show:true});
    }


     /*state and city*/
    $scope.getStates = function(){

        var data = 'SessionKey='+SessionKey;

        $scope.data.states = [];

        $http.post(API_URL+'students/getStatesList', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.states = response.Data;

            }
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

        });

    }





    $scope.getCities = function(State_id){
     // alert(State_id);
       $scope.data.cities = [];
       //console.log(State_id);

        var data = 'SessionKey='+SessionKey+'&State_id='+State_id;
       
        $http.post(API_URL+'students/getCitiesList', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.cities.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }





    $scope.getCitiesByStateName = function(StateName){
        $scope.data.cities = [];
        var data = 'SessionKey='+SessionKey+'&StateName='+StateName;
        $http.post(API_URL+'students/getCitiesByStateName', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data){ /* success case */

                //$scope.data.cities = response.Data;

                for (var i in response.Data) {
                    $scope.data.cities.push(response.Data[i]);

                }

               // console.log($scope.data.cities);

            }

        });

    }




    $scope.updateMasterFranchisee = function(MasterFranchisee,UserGUID,Position){
        //alertify.confirm('Are you sure to provide Master Franchisee to selected institute.', function(){ 
            $scope.data.listLoading = true;
            $scope.data.Position = Position;
            var data = 'SessionKey=' + SessionKey + '&MasterFranchisee=' + MasterFranchisee + '&UserGUID=' + UserGUID;
            $http.post(API_URL + 'institute/updateMasterFranchisee', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                   SuccessPopup(response.Message);
                   $scope.data.dataList[$scope.data.Position] = response.Data;
                   if(MasterFranchisee == 'yes'){
                    $("#institute"+Position).hide(); 
                   }else{
                    $("#institute"+Position).show(); 
                   }
                } else {
                   ErrorPopup(response.Message);
                }
                $scope.data.listLoading = false;
            });
        //}).set('oncancel', function(closeEvent){ $scope.getList(); } );
    }


    $scope.setMasterInstitute = function(UserGUID,MasterInstituteGUID,Position){
        //alertify.confirm('Are you sure to set a branch to selected institute.', function(){ 
            $scope.data.listLoading = true;
            $scope.data.Position = Position;
            var data = 'SessionKey=' + SessionKey + '&InstituteGUID=' + MasterInstituteGUID + '&UserGUID=' + UserGUID;
            $http.post(API_URL + 'institute/updateMasterInstitute', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                   SuccessPopup(response.Message);
                   //$scope.data.dataList[$scope.data.Position] = response.Data;
                   console.log($scope.data.dataList[$scope.data.Position]);
                } else {
                   ErrorPopup(response.Message);
                }
                $scope.data.listLoading = false;
            });
       // }).set('oncancel', function(closeEvent){ $scope.getList(); } );
        
    }

    /*list append*/
    $scope.getList = function() {
        if ($scope.data.listLoading || $scope.data.noRecords) return;
        $scope.data.listLoading = true;
        var data = 'SessionKey=' + SessionKey + '&UserTypeID=' + 10 +'&IsAdmin=Yes&PageNo=' + $scope.data.pageNo + '&PageSize=' + $scope.data.pageSize + '&OrderBy=' + $scope.data.OrderBy + '&Sequence=' + $scope.data.Sequence + '&' +'Params=RegisteredOn,LastLoginDate,UserTypeName,FullName,Email,Username,ProfilePic,Gender,BirthDate,PhoneNumber,Status,StateName,CityName,ReferredCount,StatusID&'+$('#filterForm').serialize();

        $http.post(API_URL + 'institute/getInstitutes', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) { /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }
                $scope.data.pageNo++;

                $timeout(function(){            
                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                }, 200);
                
            } else {
                $scope.data.noRecords = true;
            }
            
            $scope.data.listLoading = false;
        });
    }



    $scope.tableDatatoExcel = function(){
        var data = 'SessionKey=' + SessionKey;
        $scope.data.newDataList = [];
        $http.post(API_URL + 'institute/getInstitutes', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200 && response.Data.Records) { /* success case */
                for (var i in response.Data.Records) {
                   delete response.Data.Records[i].UserGUID;
                   delete response.Data.Records[i].UserID;
                   delete response.Data.Records[i].StatusID;
                   delete response.Data.Records[i].MasterFranchisee;
                   delete response.Data.Records[i].InstituteID;
                   $scope.data.newDataList.push(response.Data.Records[i]);
                }
                   $("#dvjson").excelexportjs({
                        containerid: "dvjson"
                           , datatype: 'json'
                           , dataset: $scope.data.newDataList
                           , columns: getColumns(response.Data.Records)     
                   });       
            }
        });
    }




    /*load add form*/
    $scope.loadFormAdd = function (Position, UserGUID)
    {
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
        $('#add_model').modal({show:true});
         $timeout(function(){            
           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
        }, 200);
    }




/*add data*/
    $scope.addData = function ()
    {
        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'institute/add', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                 $scope.applyFilter();
                  $('.modal-header .close').click();
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });
    }



/*load edit form*/
    $scope.loadFormEditprofile = function(Position,UserGUID) {
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form_profile.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'users/getProfile', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&Params=Status, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
                $scope.getCities($scope.formData.State_id);
               // console.log($scope.formData);
                //alert(formData);
                $('#edit_profile').modal({
                    show: true
                });

                $scope.getList();
               // $scope.getFilterStateData();

                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });
    }



    $scope.loadFormView = function(Position,UserGUID) {
        $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE + module + '/view_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'users/getProfile', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&Params=Status, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;

                $scope.formData = response.Data;
                $scope.getCities($scope.formData.State_id);
               // console.log($scope.formData);
                //alert(formData);
                $('#view_profile').modal({
                    show: true
                });

                $scope.getList();
               // $scope.getFilterStateData();

                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });
    }




    /*load edit form*/
    $scope.loadFormEdit = function(Position, UserGUID) {
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'users/getProfile', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&Params=Status,ProfilePic,MediaPAN,MediaBANK', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#edit_model').modal({
                    show: true
                });
                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });

    }

     /*load edit form*/
    $scope.loadFormAddCash = function(Position, UserGUID) {
        
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/addCashBonus_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'users/getProfile', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&Params=FirstName,ProfilePic,Status', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#AddCashBonus_model').modal({
                    show: true
                });
                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });

    }

    /*load edit form*/
    $scope.loadFormReferredUsersList = function(Position, UserGUID) {
        
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/referredUserlist_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'admin/users/getReferredUsers', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&Params=FirstName,ProfilePic,Email,Status, Gender, BirthDate, PhoneNumber', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#referralUserList_model').modal({
                    show: true
                });
                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });

    }

    /*load verification form*/
    $scope.loadFormVerification = function(Position, UserGUID) {
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/verification_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'users/getProfile', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&Params=Status,ProfilePic,MediaPAN,MediaBANK,PanStatus,BankStatus', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data;
                
            
                console.log($scope.formData);
                $('#Verification_model').modal({
                    show: true
                });
                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });

    }

    /*delete selected */
    $scope.deleteSelectedRecords = function() {
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&' + $('#records_form').serialize();
            $http.post(API_URL + 'admin/entity/deleteSelected', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.applyFilter();
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
    }


    /*edit data*/
    $scope.editData = function() {
        $scope.editDataLoading = true;
        var data = 'SessionKey=' + SessionKey + '&' + $('#edit_form').serialize();
        $http.post(API_URL + 'admin/users/changeStatus', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position].Status = response.Data.Status;
                $scope.data.dataList[$scope.data.Position].StatusID = response.Data.StatusID;
             

                   $('.modal-header .close').click();


            } else {
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;
        });
    }


/*edit data*/
    $scope.editDataProfile = function ()
    {
        $scope.editDataLoading = true;
        console.log($scope.formData.UserGUID);
        $scope.editDataLoading = true;

        

        var data = 'SessionKey='+SessionKey+'&UserGUID='+$scope.formData.UserGUID+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'admin/users/updateUserProfile', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                //$scope.applyFilter();

                $scope.data.dataList[$scope.data] = response.Data;

                $timeout(function(){            

                   $('.modal-header .close').click();

               }, 200);

            }else{
                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });
    }








    /*add cash bonus data*/
    $scope.addCashBonus = function() {
        $scope.editDataLoading = true;
        var data = 'SessionKey=' + SessionKey + '&Status=Completed&Narration=Admin Cash Bonus&' + $('#addCash_form').serialize();
        $http.post(API_URL + 'admin/users/addCashBonus', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position].Status = response.Data.Status;
                $('.modal-header .close').click();
            } else {
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;
        });
    }


    /*edit data*/
    $scope.verifyDetails = function(UserGUID,VetificationType,Status) {
        $scope.editDataLoading = true;
        if(VetificationType=='PAN'){
            var Params = '&PanStatus='+Status;
        }else{
            var Params = '&BankStatus='+Status;
        }
        var data = 'SessionKey=' + SessionKey + '&UserGUID=' +UserGUID+'&VetificationType='+VetificationType+Params ;
        $http.post(API_URL + 'admin/users/changeVerificationStatus', data, contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                SuccessPopup(response.Message);
                
            } else {
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;
        });
    }


    /*load delete form*/
    $scope.loadFormDelete = function(Position, UserGUID) {
        $scope.data.Position = Position;
        $scope.templateURLDelete = PATH_TEMPLATE + module + '/delete_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $http.post(API_URL + 'users/getProfile', 'SessionKey='+SessionKey+'&UserGUID='+UserGUID+'&Params=Status,ProfilePic', contentType).then(function(response) {
            var response = response.data;
            if (response.ResponseCode == 200) { /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#delete_model').modal({
                    show: true
                });
                $timeout(function() {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");
                }, 200);
            }
        });

    }


    $scope.importData = function ()



    {



        $scope.loadeSubmit = true;



        var data = 'SessionKey='+SessionKey+'&'+$("form[name='importInstitutes']").serialize();



        console.log(data);



        $http.post(API_URL+'institute/importInstitutes', data, contentType).then(function(response) {



            var response = response.data;



            

            if(response.ResponseCode==200){ /* success case */               



                SuccessPopup(response.Message);



               // $scope.data.dataLists[$scope.data.Position] = response.Data[0];


               $scope.getStudentStatistics();



                $timeout(function(){            



                   $('#import_excel_model .close').click();

                   $scope.loadeSubmit = false;

               }, 200);



            }else{



                ErrorPopup(response.Message);



            }

        });



       $scope.loadeSubmit = false;



        setTimeout(function(){ tblsort(); }, 1000);



    }



});






function updateMasterFranchisee(data){
    var array = data.split('&-&');
    angular.element(document.getElementById('institute-body')).scope().updateMasterFranchisee(array[0],array[1],array[2]);  
}  


function setMasterInstitute(data){
    var array = data.split('&-&');
    angular.element(document.getElementById('institute-body')).scope().setMasterInstitute(array[0],array[1],array[2]);  
}


function tableDatatoExcel(){
    $("#tableData").excelexportjs({
        containerid: "tableData"
        , datatype: 'table'
    });
}

/*audio file upload*/

$(document).on('change', '#instituteFileInput', function() {

    var target = $(this).data('target');

    var mediaGUID = $(this).data('targetinput');

    var progressBar = $('.progressBar'),

        bar = $('.progressBar .bar'),

        percent = $('.progressBar .percent');

    $(this).parent().ajaxForm({

        data: {

            SessionKey: SessionKey

        },

        dataType: 'json',

        beforeSend: function() {

            progressBar.fadeIn();

            var percentVal = '0%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        uploadProgress: function(event, position, total, percentComplete) {

            var percentVal = percentComplete + '%';

            bar.width(percentVal)

            percent.html(percentVal);

        },

        success: function(obj, statusText, xhr, $form) {

            if (obj.ResponseCode == 200) {

                var percentVal = '100%';

                bar.width(percentVal)

                percent.html(percentVal);

                $(target).prop("src", obj.Data.MediaURL);

                //$("input[name='MediaGUIDs']").val(obj.Data.MediaGUID);

                $(mediaGUID).val(obj.Data.MediaGUID);

                $("#MediaURL").val(obj.Data.MediaURL);

            } else {

                ErrorPopup(obj.Message);

            }

        },

        complete: function(xhr) {

            progressBar.fadeOut();

            //$('#studentFileInput').val("");

        }

    }).submit();




});