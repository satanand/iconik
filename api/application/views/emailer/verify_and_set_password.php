<h2 style="margin-bottom:20px; font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; ">Hi <?php echo $Name;?>,</h2>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Welcome to <?php  echo $InstituteName; ?>.</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">To verify your email address and to set your password please use the link given below</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">

<a href="<?php echo ADMIN_BASE_URL.'recovery/set'?>"><?php echo ADMIN_BASE_URL.'recovery/set'?></a>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Thank you for registering with <span><?php echo $InstituteName;?></span>.</p>