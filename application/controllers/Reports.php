<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends Admin_Controller_Secure 
{
	public function index()
	{		
		$load['css']=array(		
		'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
		'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);

		$load['js']=array(
		'asset/js/'.$this->ModuleData['ModuleName'].'.js',
		'asset/plugins/jquery.form.js',	
		'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
		'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
		'asset/js/excelexportjs.js'
		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",			
			"Manage Reports" => ""
		);	

		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('reports/report_list');
		$this->load->view('includes/footer');		
	}


	public function export_pdf()
	{
		$data['content'] = array();
		
		if(!empty($_POST) && isset($_SESSION['UserData']['SessionKey']) && !empty($_SESSION['UserData']['SessionKey']))
		{
			$this->load->helper('custom_helper');

			$_POST['SessionKey'] = $_SESSION['UserData']['SessionKey'];
			$_POST['Res'] = 'p';
			$_POST['SearchType'] = 's';
			$JSON = json_encode($_POST);
			
			$Response = APICall(API_URL.'reports/generateReport', $JSON);
			$data['content'] = $Response;

			$Where = $_POST;
			$filter_within = prepare_report_filters($Where);			

			$report_title = $_POST['filterReportType'];
			$report_title = str_replace("_", "", $report_title);
			$header_title = $data['content']['report_title'] = ucwords($report_title);
			$data['content']['report_from'] = $_POST['filterFromDate'];
			$data['content']['report_to'] = $_POST['filterToDate'];
			$data['content']['report_filter_within'] = trim($filter_within);
			$data['session_data'] = $_SESSION['UserData'];

			if($Response['ResponseCode'] == 200)
			{
				$html = $this->load->view('reports/report_pdf', $data, true);

				$pdfFilePath = $header_title." ".date('d-M-Y').".pdf";
		        
		        $this->load->library('m_pdf');
		        
		 		$this->m_pdf->pdf->autoScriptToLang = true;
				$this->m_pdf->pdf->baseScript = 1;
				$this->m_pdf->pdf->autoVietnamese = true;
				$this->m_pdf->pdf->autoLangToFont = true;

		 		$this->m_pdf->pdf->setFooter($header_title." (".'{PAGENO}'.")");
		       	
		        $this->m_pdf->pdf->WriteHTML($html);		 
		        
		        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 
			}
			else
			{
				$msg = $Response['Message'];
				echo "<script> alert('".$msg."'); window.history.go(-1); </script>";
				die;
			}
		}
	
	}


	public function export_excel()
	{
		$data['content'] = array();
		
		if(!empty($_POST) && isset($_SESSION['UserData']['SessionKey']) && !empty($_SESSION['UserData']['SessionKey']))
		{
			$this->load->helper('custom_helper');

			$_POST['SessionKey'] = $_SESSION['UserData']['SessionKey'];
			$_POST['Res'] = 'e';
			$_POST['SearchType'] = 's';
			$JSON = json_encode($_POST);
			
			$Response = APICall(API_URL.'reports/generateReport', $JSON);
			$data['content'] = $Response;

			$Where = $_POST;
			$filter_within = prepare_report_filters($Where, ' ');

			if($Response['ResponseCode'] == 200)
			{
				$instName = ucwords($_SESSION['UserData']['FullName']);
				$instAddress = ucwords($_SESSION['UserData']['Address'].", ".$_SESSION['UserData']['CityName'].', '.$_SESSION['UserData']['State']);
				$instEmail = $_SESSION['UserData']['EmailForChange'];

				$report_title = $_POST['filterReportType'];
				$report_title = str_replace("_", "", $report_title);
				$report_title = ucwords($report_title);
				$report_from = $_POST['filterFromDate'];
				$report_to = $_POST['filterToDate'];
				$report_filter_within = trim($filter_within);

				$file_name = $report_title." ".date('d-M-Y').".xls";


				$this->load->library("excel");
  				$object = new PHPExcel();
  				$object->setActiveSheetIndex(0);
  				$sheet = $object->getActiveSheet();

  				
  				//Set font to bold of row----------------------------
  				$object->getActiveSheet()->getStyle("A1:P1")->getFont()->setBold(true);
  				$object->getActiveSheet()->getStyle("A2:P2")->getFont()->setBold(true);
  				$object->getActiveSheet()->getStyle("A3:P3")->getFont()->setBold(true);
  				$object->getActiveSheet()->getStyle("A4:P4")->getFont()->setBold(true);
  				$object->getActiveSheet()->getStyle("A5:P5")->getFont()->setBold(true);
  				

  				//Set width of cell----------------------------
  				$cell_width = 25;
  				$cell_diff = $cell_width - 5;
  				$object->getActiveSheet()->getColumnDimension('A')->setWidth($cell_width);
  				$object->getActiveSheet()->getColumnDimension('B')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('C')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('D')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('E')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('F')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('G')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('H')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('I')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('J')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('K')->setWidth($cell_diff);
  				$object->getActiveSheet()->getColumnDimension('L')->setWidth($cell_diff);

  				
  				//Set height of first cell----------------------------
  				$object->getActiveSheet()->getRowDimension('1')->setRowHeight(50);

  				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    );
			    $sheet->getStyle("A1:B1")->applyFromArray($style);

  				  				

  				//Adding content to excel-----------------------------------
  				$object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, $instName."\n".$instAddress."\n".$instEmail);  				

  				$object->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "Report Title: ".$report_title);

  				$object->getActiveSheet()->setCellValueByColumnAndRow(0, 3, "From Date: ".$report_from);
  				$object->getActiveSheet()->setCellValueByColumnAndRow(1, 3, "To Date: ".$report_to);

  				$object->getActiveSheet()->setCellValueByColumnAndRow(0, 4, "Filters: ".$report_filter_within);


  				$column = 0;

				foreach($data['content']['Data']['Records'][0] as $field)
				{
				   	$object->getActiveSheet()->setCellValueByColumnAndRow($column, 5, $field);
				   	$column++;
				}
				
				$column = chr(65 + $column);
				$sheet->mergeCells('A1:'.$column.'1');  

				$excel_row = 6;

				unset($data['content']['Data']['Records'][0]);
				foreach($data['content']['Data']['Records'] as $arr)
				{
					foreach($arr as $key => $val)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow($key, $excel_row, $val);
					}

					$excel_row++;
				}

				$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$file_name.'"');
				$object_writer->save('php://output');				 
			}
			else
			{
				$msg = $Response['Message'];
				echo "<script> alert('".$msg."'); window.history.go(-1); </script>";
				die;
			}
		}
	
	}	


	
	public function daily_registrations()
	{		
		$data['content'] = array();
		
		if(isset($_SESSION['UserData']['SessionKey']) && !empty($_SESSION['UserData']['SessionKey']))
		{			
			$this->load->helper('custom_helper');
			$_POST['SessionKey'] = $_SESSION['UserData']['SessionKey'];

			$JSON = json_encode($_POST);			
			$Response = APICall(API_URL.'reports/dailyRegistrations', $JSON);
			$data = $Response;

			if($Response['ResponseCode'] == 200)
			{				
				$print_arr = $user_types = $reg_dates = $activation_types = array();
				
				foreach($data['Data']['Records'] as $arr)
				{					
					$arr['RegDate'] = date("d-M-Y", strtotime($arr['RegDate']));
					$reg_dates[] = $arr['RegDate'];
					$user_types[] = $arr['UserTypeName'];
					$activation_types[] = $arr['Type'];					

					if(isset($print_arr[$arr['RegDate']][$arr['UserTypeName']][$arr['Type']]) && !empty($print_arr[$arr['RegDate']][$arr['UserTypeName']][$arr['Type']]))
					{
						$print_arr[$arr['RegDate']][$arr['UserTypeName']][$arr['Type']] = $print_arr[$arr['RegDate']][$arr['UserTypeName']][$arr['Type']] + $arr['Total'];
					}
					else
					{
						$print_arr[$arr['RegDate']][$arr['UserTypeName']][$arr['Type']] = $arr['Total'];
					}
				}

				$reg_dates = array_unique($reg_dates);
				$user_types = array_unique($user_types);
				$activation_types = array_unique($activation_types);


				$subject = "New Registration Counts";				
				$file_name = $subject." ".date('d-M-Y').".xls";


				$this->load->library("excel");
  				$object = new PHPExcel();
  				$object->setActiveSheetIndex(0);
  				$sheet = $object->getActiveSheet();

  				
  				//Set font to bold of row----------------------------
  				$object->getActiveSheet()->getStyle("A1:A25")->getFont()->setBold(true);
  				$object->getActiveSheet()->getStyle("B2:B25")->getFont()->setBold(true);
  				

  				//Set width of cell----------------------------
  				$cell_width = 15;
  				$cell_diff = $cell_width - 5;
  				$object->getActiveSheet()->getColumnDimension('A')->setWidth($cell_width);
  				$object->getActiveSheet()->getColumnDimension('B')->setWidth($cell_diff);  				

  				
  				//Set height of first cell----------------------------
  				$object->getActiveSheet()->getRowDimension('1')->setRowHeight(30);

  				$style = array(
			        'alignment' => array(
			            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			        )
			    );
			    $sheet->getStyle("A1:B1")->applyFromArray($style);

  				  				
  				//Adding content to excel-----------------------------------
  				//$sheet->mergeCells('A1:B1');  

				$column = 0;
				$excel_row = 1;					
				if(isset($print_arr) && !empty($print_arr))
				{
					$date_wise_total = $prev_content = array();

					$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, "Date");
					$object->getActiveSheet()->setCellValueByColumnAndRow(++$column, $excel_row, "");
					foreach($reg_dates as $rd)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow(++$column, $excel_row, $rd);
					}					

					
					foreach ($user_types as $uv) 
					{
						$column = 0;
						$excel_row++;

						$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $uv);
						
						foreach($activation_types as $at)
						{
							$column = 1;							
							$idx = 0;

							$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $at);
							$column++;

							//$total_col = 0;
							foreach($reg_dates as $rd) 
							{
								$val = 0;
								if(isset($print_arr[$rd][$uv][$at]) && !empty($print_arr[$rd][$uv][$at]))
								{
									$val = $print_arr[$rd][$uv][$at];									

									//$total_col = $total_col + $val;

									$dis = $val;
								}
								else
								{
									$dis = "";
								}

								if($idx == 0)
								{
									$prev_content[$idx][$uv][$at] = $val; 								
								}
								else
								{
									$val = $val + $prev_content[$idx - 1][$uv][$at];
									$prev_content[$idx][$uv][$at] = $val; 
								}	

								if(isset($date_wise_total[$rd]) && !empty($date_wise_total[$rd]))
								{
									$date_wise_total[$rd] = $date_wise_total[$rd] + $val;
								}
								else
								{
									$date_wise_total[$rd] = $val;
								}

								$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $val);
								$column++;
								$idx++;
							}

							//$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $total_col);

							$excel_row++;
						}


					}

					$column = 1;
					$excel_row++;

					$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, "Total");
					$column++;

					foreach($date_wise_total as $total)
					{
						$object->getActiveSheet()->setCellValueByColumnAndRow($column, $excel_row, $total);
						$column++;
					}					
				}


				$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$file_name.'"');
				$object_writer->save('php://output');				 
			}
			else
			{
				$msg = $Response['Message'];
				echo "<script> alert('".$msg."'); </script>";
				die;
			}
		}
	
	}

}
