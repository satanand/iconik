<style type="text/css">
	.glyphicon {
	    position: relative;
	    top: -4px;
	    display: inline-block;
	    font-family: 'Glyphicons Halflings';
	    font-style: normal;
	    font-weight: normal;
	    line-height: 1;
	    -webkit-font-smoothing: antialiased;
	    font-size: 20px;
	    padding: 4px;
	}

	.capitalize
	{
		text-transform: capitalize;
	}
</style>  
<header class="panel-heading"> <h1 class="h4">Applicants For Jobs</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getApplicants(0);" id="content-body"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">
 		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<span class="h5">Total Jobs: {{data.dataList.length}}</span>
				</div>
			</div>	
 		</div>

		<form id="filterForm" role="form" autocomplete="off">
		<input type="hidden" name="PostJobID" id="PostJobID" value="<?php echo $_GET['PostJobID'];?>">
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<select class="form-control" name="FilterStatus" id="FilterStatus">		
						<option value="">All</option>
						<option value="10">Applied For Job</option>
						<option value="11">Shortlisted</option>
						<option value="12">Archived</option>
						<option value="13">Deleted</option>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
		            <input name="FilterStartDate" type="text" placeholder="Apply From" id="FilterStartDate" class="form-control">
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
		            <input name="FilterEndDate" type="text" placeholder="Apply To" id="FilterEndDate" class="form-control">
				</div>
			</div>				


			<div class="col-md-3">
				<div class="form-group">			 	
		        	<input type="text" class="form-control" name="FilterKeyword" id="FilterKeyword" placeholder="Search by word" >
		    	</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" title="Search" ng-click="getApplicants(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
					<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="RemoveApplicantsFilters();" title="Refresh"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>
			
		</div>
	</form>		

	</div>


	<!-- Data table -->
	<form id="facultyStatusForm" name="facultyStatusForm" role="form" autocomplete="off">
	<input type="hidden" name="PostJobIDForStatus" id="PostJobIDForStatus" value="<?php echo $_GET['PostJobID'];?>">
	<div class="table-responsive block_pad_md" infinite-scroll="getApplicants();" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
		<p align="right">Shortlist the faculty which suits your job. An email/sms will be send to informing them.</p>
		<table class="table table-striped table-condensed table-hover table-sortable">			
			<thead>
				<tr>
					<th style="width: 100px;">Faculty Name</th>					
					<th style="width: 70px;">Age</th>
					<th style="width: 70px;">Qualification</th> 
					<th style="width: 70px;">Experience</th>
					<!-- <th style="width: 50px;">Resume</th> -->
					<th style="width: 50px;">Applied On</th>
					<th style="width: 50px;">View</th>
					<th style="width: 50px;">Current Status</th>					
					<th style="width: 100px;" class="text-center">Action/Status</th>
				</tr>
			</thead>

			<!-- table body -->

			<tbody id="tabledivbody">
				<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.ApplyID}}">

					<td>
						<strong class="capitalize">{{row.FirstName}} {{row.LastName}}</strong><br/>
						<span style="font-size: 10px;" ng-if="row.Email">({{row.Email}})</span>
						<span style="font-size: 10px;" ng-if="row.PhoneNumberForChange"><br/>({{row.PhoneNumberForChange}})</span>
					</td>
					
					<td>{{row.ApplyAge}} years</td>

					<td>{{row.ApplyQualification}}</td>

					<td>{{row.ApplyExperience}} years</td>

					<!-- <td>
						<p class="mb-0" ng-if="row.MediaID != '' && row.MediaID != '0'">
						    <button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="FileInModalBox(row.MediaID,'Resume')"> <i class="fa fa-download" aria-hidden="true"></i> View</button>
						    <u><a href="javascript:void(0);" ng-click="FileInModalBox(row.MediaID,'Resume')">View</a></u>
						</p>
					</td> -->

					<td>{{row.ApplyDate | date}}</td>

					<td>
						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormViewApplicants(key, row.ApplyID, row)" data-toggle="tooltip" title="View More Details"></a>
						<label class="badge badge-danger" ng-if="row.IsResumeUpdated == 1">New Resume Uploaded</label>					
					</td>

					<td align="center">
						<label class="badge badge-primary">{{row.StatusName}}</label><br/> 
						
						<label class="badge badge-success" ng-if="row.ApplyStatus == 10">{{row.ApplyDate}}</label>
						<label class="badge badge-success" ng-if="row.ApplyStatus == 11">{{row.ModifiedDate}}</label>
						<label class="badge badge-success" ng-if="row.ApplyStatus == 14 || row.ApplyStatus == 19">{{row.TelInterviewDate}}</label>
						<label class="badge badge-success" ng-if="row.ApplyStatus == 15 || row.ApplyStatus == 25">{{row.PerInterviewDate}}</label>

					</td>

					<td class="text-center" style="text-align: center;">
 						<div ng-if="row.ApplyStatus != 20 && row.ApplyStatus != 25 && row.ApplyStatus != 12 && row.ApplyStatus != 13 && row.ApplyStatus != 20 && row.ApplyStatus != 21 && row.ApplyStatus != 20 && row.ApplyStatus != 26">
	 						<select class="form-control" name="FacultyStatus[{{row.ApplyID}}]" id="FacultyStatus[{{row.ApplyID}}]">		
								
								<option value="">Select Status</option>
								
								<option value="11" ng-selected="row.ApplyStatus == 11 || row.ApplyStatus == 14 || row.ApplyStatus == 15" ng-if="row.ApplyStatus != 11">Shortlisted</option>
								
								<option value="12" ng-selected="row.ApplyStatus == 12">Archived</option>
								
								<option value="13" ng-selected="row.ApplyStatus == 13">Deleted</option>

								
								<optgroup label="Telephonic Interview">
									<option value="16" ng-selected="row.ApplyStatus == '16'">Candidate Refused</option>

									<option value="17" ng-selected="row.ApplyStatus == '17'">Did Not Appear</option>

									<option value="20" ng-selected="row.ApplyStatus == '20'">Selected</option>

									<option value="21" ng-selected="row.ApplyStatus == '21'">Rejected</option>
								</optgroup>

								
								<optgroup label="Personal Interview">							
									<option value="22" ng-selected="row.ApplyStatus == '22'">Candidate Refused</option>

									<option value="23" ng-selected="row.ApplyStatus == '23'">Did Not Appear</option>

									<option value="25" ng-selected="row.ApplyStatus == '25'">Selected</option>

									<option value="26" ng-selected="row.ApplyStatus == '26'">Rejected</option>
								</optgroup>

							</select>

							
							<input type="hidden" name="FacultyEmail[{{row.ApplyID}}]" 	value="{{row.Email}}" ng-if="row.Email != ''">
							<input type="hidden" name="FacultyMobile[{{row.ApplyID}}]" 	value="{{row.PhoneNumberForChange}}" ng-if="row.PhoneNumberForChange != ''">

							<button class="btn btn-primary btn-sm ml-1" ng-click="loadFixInterview(row);" ng-if="((UserTypeID == 10 || UserTypeID == 87 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0) && (row.ApplyStatus == 11 || row.ApplyStatus == 14 || row.ApplyStatus == 15 || row.ApplyStatus == 19) && (row.TelInterviewStatus == '' || row.TelInterviewStatus == 18 || row.TelInterviewStatus == 19)"><i class="fa fa-plus" aria-hidden="true"></i> 
								<label ng-if="row.ApplyStatus == 14 || row.ApplyStatus == 15">Set Status</label>
								<label ng-if="row.ApplyStatus == 19">Fix Personal Interview</label>
								<label ng-if="row.ApplyStatus == 10 || row.ApplyStatus == 11">Fix Interview</label>
							</button>
						</div>

						<!-- <div ng-if="row.ApplyStatus == 20 || row.ApplyStatus == 25">
							<p ng-if="data.JobLogLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
							<button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="loadJobLog(row.ApplyID);" ng-disabled="data.JobLogLoading"> <i class="fa fa-list" aria-hidden="true"></i> {{row.StatusName}}</button>
						</div>

						<div ng-if="row.ApplyStatus == 12">
							<p ng-if="data.JobLogLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
							<button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="loadJobLog(row.ApplyID);" ng-disabled="data.JobLogLoading"> <i class="fa fa-list" aria-hidden="true"></i> Archived</button>
						</div>

						<div ng-if="row.ApplyStatus == 13">
							<p ng-if="data.JobLogLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
							<button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="loadJobLog(row.ApplyID);" ng-disabled="data.JobLogLoading"> <i class="fa fa-list" aria-hidden="true"></i> Deleted</button>
						</div> -->	

						<div ng-if="row.ApplyStatus == 20 || row.ApplyStatus == 25">
							<p ng-if="data.JobLogLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
							<button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="loadJobLog(row.ApplyID);" ng-disabled="data.JobLogLoading"> <i class="fa fa-list" aria-hidden="true"></i> View Log</button>
						</div>

						<div ng-if="row.ApplyStatus == 12">
							<p ng-if="data.JobLogLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
							<button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="loadJobLog(row.ApplyID);" ng-disabled="data.JobLogLoading"> <i class="fa fa-list" aria-hidden="true"></i> View Log</button>
						</div>

						<div ng-if="row.ApplyStatus == 13">
							<p ng-if="data.JobLogLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
							<button type="button" name="submit_button" class="btn btn-primary radius-full btn-sm" ng-click="loadJobLog(row.ApplyID);" ng-disabled="data.JobLogLoading"> <i class="fa fa-list" aria-hidden="true"></i> View Log</button>
						</div>
					</td>

				</tr>
			</tbody>

		</table>
		
		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>


		<p ng-if="data.dataList.length > 0" style="float: right; margin: 15px;">
			<p ng-if="saveDataLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

			<button class="btn btn-success btn-sm ml-1" style="float: right; margin: 15px;" ng-disabled="saveDataLoading" ng-click="saveFacultyStatus();">Submit</button>
		</p>	

	</div>
	</form>
	<!-- Data table/ -->

		
	<!-- View Modal -->
	<div class="modal fade" id="View_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Job</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="View_form" name="View_form" autocomplete="off" ng-include="templateURLView">
				</form>
				
				<!-- /form -->
			</div>
		</div>
	</div>	


	<!-- View Modal -->
	<div class="modal fade" id="job_log_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">History</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form autocomplete="off" ng-include="templateURLJobLog"></form>
				
				<!-- /form -->
			</div>
		</div>
	</div>


	<div class="modal fade" id="fix_interview_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Fix Interview</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_fix_interview_form" name="add_fix_interview_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>	



</div><!-- Body/ -->