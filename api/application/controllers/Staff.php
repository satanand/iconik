<?php

defined('BASEPATH') OR exit('No direct script access allowed'); 



class Staff extends API_Controller_Secure

{

	function __construct()

	{

		parent::__construct();
        $this->load->model('Staff_model');
	}
	
	/*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/accountDeactivate/	

	*/

	public function accountDeactivate_post()

	{

		$this->Entity_model->updateEntityInfo($this->SessionUserID, array("StatusID"=>6));

		$this->Staff_model->deleteSessions($this->SessionUserID);

		$this->Return['Message'] =	"Your account has been deactivated.";

	}



	/*

	Name: 			toggleAccountDisplay

	Description: 	Use to hide account to others.

	URL: 			/user/toggleAccountDisplay/	

	*/

	public function toggleAccountDisplay_post()

	{

		$UserData=$this->Staff_model->getUsers('StatusID',array('UserID'=>$this->SessionUserID));

		if($UserData['StatusID']==2){

			$this->Entity_model->updateEntityInfo($this->SessionUserID, array("StatusID"=>8));

		}

		elseif($UserData['StatusID']==8){

			$this->Entity_model->updateEntityInfo($this->SessionUserID, array("StatusID"=>2));

		}		

	}

  /*

	Name: 			search

	Description: 	Use to search users

	URL: 			/api/users/search

	*/

	public function search_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Keyword', 'Search Keyword', 'trim');

		$this->form_validation->set_rules('Filter', 'Filter', 'trim|in_list[Friend,Follow,Followers,Blocked]');

		$this->form_validation->set_rules('UserTypeID', 'UserTypeID', 'trim|in_list[2,3]');

		$this->form_validation->set_rules('Latitude', 'Latitude', 'trim');

		$this->form_validation->set_rules('Longitude', 'Longitude', 'trim');

		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');

		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */



		$UserData=$this->Staff_model->getUsers('Rating,ProfilePic',

			array(

				'SessionUserID'		=>	$this->SessionUserID,

				'Keyword'			=>	@$this->Post['Keyword'],

				'Filter'			=>	@$this->Post['Filter'],

				'SpecialtyGUIDs'	=>	@$this->Post['SpecialtyGUIDs'],

				'UserTypeID'		=>	@$this->Post['UserTypeID'],

				'StatusID'			=>	2

				), TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($UserData){

			$this->Return['Data'] = $UserData['Data'];

		}	

	}

   /*

	Name: 			getProfile

	Description: 	Use to get user profile info.

	URL: 			/api/user/getProfile

	*/

	public function getProfile_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');

		$this->form_validation->validation($this);  /* Run validation */

		/* Validation - ends */

		/*check for self profile or other user profile by GUID*/

		 $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

        /*Basic fields to select*/

		$UserData=$this->Staff_model->getUsers((!empty($this->Post['Params']) ? $this->Post['Params']:''),array('UserID'=>$this->UserID));

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$UserTypeData = $this->Staff_model->getusertype($this->EntityID);

		if(!empty($UserTypeData)){

			$this->Return['Datas'] = $UserTypeData['Data'];
		}
		if($UserData){

			$this->Return['Data'] = $UserData;

		}	

	}

  /*

	Name: 			updateUserInfo

	Description: 	Use to update user profile info.

	URL: 			/user/updateProfile/	

	*/

	public function updateUserInfo_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Email', 'Email', 'trim|valid_email|callback_validateEmail['.$this->Post['SessionKey'].']');

		$this->form_validation->set_rules('Username', 'Username', 'trim');



		$this->form_validation->set_rules('Gender', 'Gender', 'trim|in_list[Male,Female,Other]');

		$this->form_validation->set_rules('BirthDate', 'BirthDate', 'trim|callback_validateDate');



		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim|callback_validatePhoneNumber['.$this->Post['SessionKey'].']');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

       $this->Staff_model->updateUserInfo($this->SessionUserID, $this->Post);



		$this->Return['Data']=$this->Staff_model->getUsers('FirstName,LastName,Email,ProfilePic,UserTypeID,UserTypeName,Username',array("UserID" => $this->SessionUserID));

		$this->Return['Message']      	=	"Profile successfully updated."; 	

	}

 /*

	Name: 			changePassword

	Description: 	Use to change account login password by user.

	URL: 			/api/users/changePassword

	*/

	public function changePassword_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('CurrentPassword', 'Current Password', 'trim|callback_validatePassword');

		$this->form_validation->set_rules('Password', 'Password', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

       if($this->Staff_model->updateUserLoginInfo($this->SessionUserID, array("Password"=>$this->Post['Password']), DEFAULT_SOURCE_ID)){

			$this->Return['Message'] =	"New password has been set."; 	

		}

	}
	/*

	Name: 			referEarn

	Description: 	Use to refer & earn user

	URL: 			/api/users/referEarn

	*/

	public function referEarn_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('ReferType', 'Refer Type', 'trim|required|in_list[Phone,Email]');

		$this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'trim'.(!empty($this->Post['ReferType']) && $this->Post['ReferType']=='Phone' ? '|required|callback_validateAlreadyRegistered[Phone]' : ''));

		$this->form_validation->set_rules('Email', 'Email', 'trim'.(!empty($this->Post['ReferType']) && $this->Post['ReferType']=='Email' ? '|required|valid_email|callback_validateAlreadyRegistered[Email]' : ''));

		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */



		$this->Staff_model->referEarn($this->Post,$this->SessionUserID);

		$this->Return['Message'] =	"User successfully invited."; 	

	}



	/*-----Validation Functions-----*/

	/*------------------------------*/	

	function validatePassword($Password)

	{	

		if(empty($Password)){

			$this->form_validation->set_message('validatePassword', '{field} is required.');

			return FALSE;

		}

		$UserData=$this->Staff_model->getUsers('',array('UserID'=>$this->SessionUserID, 'Password'=>$Password));

		if (!$UserData){

			$this->form_validation->set_message('validatePassword', 'Invalid {field}.');

			return FALSE;

		}

		else{

			return TRUE;

		}

	}



	/**

     * Function Name: validateAlreadyRegistered

     * Description:   To validate already registered number or email

     */

	function validateAlreadyRegistered($Value,$FieldValue)

	{	

		$Query = ($FieldValue == 'Email') ? 'SELECT * FROM `tbl_users` WHERE `Email` = "'.$Value.'" OR `EmailForChange` = "'.$Value.'" LIMIT 1' : 'SELECT * FROM `tbl_users` WHERE `PhoneNumber` = "'.$Value.'" OR `PhoneNumberForChange` = "'.$Value.'" LIMIT 1';

		if ($this->db->query($Query)->num_rows() > 0){

			$this->form_validation->set_message('validateAlreadyRegistered', ($FieldValue == 'Email') ? 'Email is already registered' : 'Phone Number is already registered');

			return FALSE;

		}

		else{

			return TRUE;

		}

	}



	
	/*
	Description: 	Use to Get Batch Of Faculty
	URL: 			/api/Staff/getBatchsOfFaculty
	Input (Sample JSON): 		
	*/
	public function getBatchsOfFaculty_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$Data = $this->Staff_model->getBatchsOfFaculty($this->EntityID);		
		
		if(isset($Data) && !empty($Data))
		{		
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $Data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found";
			$this->Return['Data'] = array();
		}	
	}


	/*
	Description: 	Use to Get students of Batch particular Faculty
	URL: 			/api/Staff/getStudentsOfBatchForFaculty
	Input (Sample JSON): 		
	*/
	public function getStudentsOfBatchForFaculty_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		

		$this->form_validation->set_rules('BatchID', 'BatchID', 'trim|required');

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$Data = $this->Staff_model->getStudentsOfBatchForFaculty($this->EntityID, $this->Post);		
		
		if(isset($Data) && !empty($Data))
		{		
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $Data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found";
			$this->Return['Data'] = array();
		}	
	}



	/*
	Description: 	Use to add annoucement
	URL: 			/api/Staff/addAnnoucement
	Input (Sample JSON): 		
	*/
	public function addAnnoucement_post()
	{ 
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		

		$this->form_validation->set_rules('Message', 'Message', 'trim|required');

		//$this->form_validation->set_rules('ForDate', 'ForDate', 'trim|required|callback_validateDate');

		$this->form_validation->set_rules('BatchID', 'BatchID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

       	$status = $this->Staff_model->addAnnoucement($this->EntityID, $this->Post, 0);

       	if($status > 0 || $status == true)
       	{
			$this->Return['Data'] = array();
			$this->Return['Message'] = "Annoucement saved successfully.";
			$this->Return['ResponseCode'] = 200;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Error occur while saving record. Please check input and try again.";
			$this->Return['Data'] = array();
		}	
	}


	/*
	Description: 	Use to edit annoucement
	URL: 			/api/Staff/editAnnoucement
	Input (Sample JSON): 		
	*/
	public function editAnnoucement_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		

		$this->form_validation->set_rules('Message', 'Message', 'trim|required');

		$this->form_validation->set_rules('ForDate', 'ForDate', 'trim|required|callback_validateDate');

		$this->form_validation->set_rules('BatchID', 'BatchID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

       	$status = $this->Staff_model->addAnnoucement($this->EntityID, $this->Post, 1);

       	if($status > 0 || $status == true)
       	{
			$this->Return['Data'] = array();
			$this->Return['Message'] = "Annoucement saved successfully.";
			$this->Return['ResponseCode'] = 200;
		}
		elseif($status <= 0)
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Invalid request for Annoucement Id.";
			$this->Return['Data'] = array();
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Error occur while saving record. Please check input and try again.";
			$this->Return['Data'] = array();
		}	
	}


	/*
	Description: 	Use to delete annoucement
	URL: 			/api/Staff/deleteAnnoucement
	Input (Sample JSON): 		
	*/
	public function deleteAnnoucement_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		

		$this->form_validation->set_rules('AnnoucementID', 'AnnoucementID', 'trim|required');

		$this->form_validation->validation($this);  /* Run validation */		

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

       	$status = $this->Staff_model->deleteAnnoucement($this->EntityID, $this->Post);

       	if($status == 1)
       	{
			$this->Return['Data'] = array();
			$this->Return['Message'] = "Annoucement deleted successfully.";
			$this->Return['ResponseCode'] = 200;
		}
		elseif($status == 2)
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Invalid request for Annoucement Id.";
			$this->Return['Data'] = array();
		}
		elseif($status == 3)
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "Annoucement details not found in the system.";
			$this->Return['Data'] = array();
		}	
	}


	/*
	Description: 	Use to list Annoucement of faculty
	URL: 			/api/Staff/getBatchStudentsOfFaculty
	Input (Sample JSON): 		
	*/
	public function listAnnoucement_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');			

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$Data = $this->Staff_model->listAnnoucement($this->EntityID);		
		
		if(isset($Data) && !empty($Data))
		{		
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $Data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found";
			$this->Return['Data'] = array();
		}	
	}



	/*
	Description: 	Use to list Loan applied by Student
	URL: 			/api/Staff/getLoanList
	Input (Sample JSON): 		
	*/
	public function getLoanList_post()
	{		
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');			

		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$Data = $this->Staff_model->getLoanList($this->EntityID);		
		
		if(isset($Data) && !empty($Data))
		{		
			$this->Return['ResponseCode'] = 200;
			$this->Return['Message'] = "";
			$this->Return['Data'] = $Data;
		}
		else
		{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Message'] = "No record found";
			$this->Return['Data'] = array();
		}	
	}



}

