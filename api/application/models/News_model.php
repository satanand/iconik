<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to add new category
	*/
	function addNews($Input=array()){

		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('n.NewsID');

	    $this->db->from('tbl_news n');
	    $this->db->join('tbl_entity E', 'E.EntityGUID = n.NewsGUID', 'left');
	    $this->db->where('LOWER(`NewsName`)="'.strtolower($Input['NewsName']).'"');
	    $this->db->where('E.InstituteID',$InstituteID);
	    $this->db->where("n.StatusID",2);
        $query = $this->db->get();

	    //echo $this->db->last_query(); 

	    if( $query->num_rows() > 0 )
	    {
	        return 'EXIST';
	        
	    }else{

		//$this->db->trans_start();
		$NewsGUID = get_guid();

        

		$NewsID=$this->Entity_model->addEntitys($NewsGUID,array('UserID'=>$this->UserID,"EntityTypeID"=>23,'InstituteID'=>$InstituteID,"StatusID"=>2));

		if(!empty($Input['MediaGUID'])){

		$this->db->select('MediaID');
		$this->db->from('tbl_media');
		$this->db->where('MediaGUID',$Input['MediaGUID']);
         $query = $this->db->get();

	   
	    $data = $query->result_array();

		$MediaID=$data[0]['MediaID'];
		}

		
			$InsertData = array_filter(array(

				"NewsID"=>$NewsID,
				"NewsGUID"=>$NewsGUID,
				"MediaID"=>$MediaID,
				"NewsName"=>$Input['NewsName'],
				"NewsDate"=>date("Y-m-d",strtotime($Input['NewsDate'])),
				"NewsDescription"=>@$Input['NewsDescription'],
				
				
			));
			
			$NewsID=$this->db->insert('tbl_news', $InsertData);
			//echo $this->db->last_query(); die();
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
			return TRUE;
		}
	}



	/*
	Description: 	Use to update News.
	*/
	function editNews($Input=array()){

		$MediaID=$Input['MediaID'];

		if(!empty($Input['MediaGUID'])){

			$this->db->select('MediaID');
			$this->db->from('tbl_media');
			$this->db->where('MediaGUID',$Input['MediaGUID']);
	         $query = $this->db->get();
		    $data = $query->result_array();
			$MediaID=$data[0]['MediaID'];
		}
		
		$UpdateArray = array_filter(array(
	
				"MediaID"=>$MediaID,
				"NewsName"=>$Input['NewsName'],
				"NewsDate"=>date("Y-m-d",strtotime($Input['NewsDate'])),
				"NewsDescription"=>@$Input['NewsDescription'],
		));

		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('NewsGUID', $Input['NewsGUID']);
			//$this->db->limit(1);
			$data=$this->db->update('tbl_news', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		
		return TRUE;
	}


	/*
	Description: 	Use to  get NewsBy
	*/
	function getNewsByID($NewsGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){


		$this->db->select("*,DATE_FORMAT(NewsDate,'%d-%m-%Y') NewsDate",false);
		$this->db->from('tbl_news');

		if(!empty($NewsGUID)){

			$this->db->where('NewsGUID',$NewsGUID);
		
		}
		$this->db->where('StatusID',2);

		$this->db->limit(1);
		$Query = $this->db->get();

		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

				if(!empty($Record['MediaID'])){
	 				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'News',"MediaID" => $Record['MediaID']),TRUE);
	 				$MediaDatas = ($MediaData ? $MediaData['Data'] : new stdClass());
	 				$Record['MediaThumbURL'] = $MediaDatas['Records'][0]['MediaThumbURL'];
	 				$Record['MediaURL'] = $MediaDatas['Records'][0]['MediaURL'];
	 			}

				if(!$multiRecords){

					return $Record;
				}
		
				//$Records[] = $Record;
			}
			 
			
			return $Record;
		}
		return FALSE;		
	}


	/*
	Description: 	Use to get News
	*/
	function getNews($NewsGUID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->db->select("n.*,DATE_FORMAT(n.NewsDate,'%d-%m-%Y') NewsDate",TRUE);
		$this->db->from('tbl_news n');
		$this->db->join('tbl_entity e','e.EntityID=n.NewsID','left');
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('n.StatusID',2);
		if(!empty($Where['Keyword'])){

			$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->group_start();
			$this->db->like("n.NewsName", $Where['Keyword']);

			if(!empty($Where['type']) && $Where['type'] == "MarketPlace"){
				$this->db->or_like("n.NewsDescription", $Where['Keyword']);
			}
			
			$this->db->or_like("n.NewsDate", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
			$this->db->group_end();
			//$this->db->or_like("n.NewsDate", $Where['Keyword']);
				
		}
		if(!empty($Where['NewsDates'])){

			$this->db->like("n.NewsDate", date("Y-m-d",strtotime($Where['NewsDates'])));
		}
		

		if(!empty($Where['NewsGUID'])){
			$this->db->where('n.NewsGUID',$Where['NewsGUID']);
		}

		$this->db->order_by('NewsID','DESC');
		
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//print_r($Query);
		//echo $this->db->last_query();  die();
		
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record){

					 if(!empty($Record['MediaID'])){
			 				$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'News',"MediaID" => $Record['MediaID']),TRUE);
			 				$MediaDatas = ($MediaData ? $MediaData['Data'] : new stdClass());
			 				$Record['MediaThumbURL'] = $MediaDatas['Records'][0]['MediaThumbURL'];
			 				$Record['MediaURL'] = $MediaDatas['Records'][0]['MediaURL'];
			 			}
				
				if(!$multiRecords){

					return $Record;
				}
				$Records[] = $Record;

			}
              
			$Return['Data']['Records'] = $Records;

			//print_r($Return); die();
			return $Return;
		}
		return FALSE;		
	}
    /*
	Description: 	Use to  DeteleMedia.
	*/
	function delete($NewsGUID){

		if(!empty($NewsGUID)){

			$UpdateData = array_filter(array(

				"StatusID"  =>1
				
			));

			$this->db->where('NewsGUID', $NewsGUID);

			$this->db->update('tbl_news',$UpdateData);
			
			
		}
		return TRUE;
	}
	







}