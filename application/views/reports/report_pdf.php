<style type="text/css">
.table td, .table tr, .table th
{
	border: 1px solid #ccc !important;
}
body
{
	color: #111 !important;
	font-size: 11px !important;
}
</style>
<table border="0" width="100%">
<tr>
	<td width="10%">
		<img src="<?php echo $session_data['InstituteProfilePic'];?>" width="100px" height="70px">
	</td>  

	<td width="90%" align="center">
		<h1><?php echo ucwords($session_data['FullName']);?></h1>
		<?php echo ucwords($session_data['Address'].", ".$session_data['CityName'].', '.$session_data['State']);?>
		<br/><?php echo $session_data['EmailForChange'];?>
	</td>
</tr>		
</table>  

<hr class="light-grey-hr" style='color:#ccc !important;' />



<table border="0" width="100%">
<tr>
<td colspan="2" align="center"><b>Report Title:</b>&nbsp;<?php echo ucfirst($content['report_title']);?></td>
</tr>

<tr>
	<td>
		<b>From Date:</b>&nbsp;<?php echo $content['report_from'];?>	
	</td>

	<td align="right">
		<b>To Date:</b>&nbsp;<?php echo $content['report_to'];?>		
	</td>
</tr>

<?php
if(isset($content['report_filter_within']) && !empty($content['report_filter_within']))
{
?>
<tr>
	<td colspan="2"><?php echo $content['report_filter_within'];?></td>
</tr>
<?php
}
?>
</table>

<br/>

<table class="table" width="100%" border="1" style="border-collapse: collapse;">
<?php echo $content['Data']['Records'];?>
</table>