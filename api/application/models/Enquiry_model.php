<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enquiry_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/*
	Description: 	Use to get Enquiry Details by id
	*/
	function getEnquiryByID($EntityID, $EnquiryGUID)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('e.EnquiryID, e.EnquiryGUID, 	e.EnquiryPersonName, 	e.EnquiryMobile, 	e.EnquiryEmail, 	e.EnquiryInterestedIn, 	e.EnquiryRemarks, 	e.EnquirySource');				
		$this->db->from('tbl_enquiry e');
		$this->db->where("e.EnquiryInstituteID",$InstituteID);
		$this->db->where("e.EnquiryGUID",$EnquiryGUID);		
		$this->db->limit(1);

		$Query =  $this->db->get();

		$Records = array();

		if($Query->num_rows() > 0)
		{
			foreach ($Query->result_array() as $record) 
			{				
				array_push($Records, $record);
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}


	/*
	Description: 	Use to get Enquiry List
	*/
	function getEnquiryList($Where=array(), $PageNo=1, $PageSize=10)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 
		

		$PageNo = ($PageNo -1) * $PageSize;

		//Filter the records based on search filters applied----------------------------
		$filter = "";

		if(!empty($Where['filterStatus']) && $Where['filterStatus'] == 2)
		{			
			$filter .= " AND e.EnquiryAssignToUser > 0 ";
		}
		else
		{
			$filter .= " AND e.EnquiryAssignToUser <= 0 ";
		}

		if(!empty($Where['filterSource']))
		{
			$filterSource = $Where['filterSource'];
			$filter .= " AND e.EnquirySource = '".$filterSource."' ";
		}

		if(!empty($Where['filterAssignedToUser']))
		{			
			$filterAssignedToUser = $Where['filterAssignedToUser'];
			$filter .= " AND e.EnquiryAssignToUser = ".$filterAssignedToUser;
		}

		if(!empty($Where['filterInterestedIn']))
		{
			$filterInterestedIn = $Where['filterInterestedIn'];
			$filter .= " AND e.EnquiryInterestedIn = '".$filterInterestedIn."' ";
		}

		if(isset($Where['filterFromDate']) && !empty($Where['filterFromDate']) && isset($Where['filterToDate']) && !empty($Where['filterToDate']))
		{
			$filterFromDate = date("Y-m-d", strtotime($Where['filterFromDate']));
			$filterToDate = date("Y-m-d", strtotime($Where['filterToDate']));

			$filter .= " AND (DATE(e.EnquiryDate) >= '$filterFromDate' AND DATE(e.EnquiryDate) <= '$filterToDate' ) ";
		}

		
		$sql = "SELECT e.EnquiryGUID, 	e.EnquiryPersonName, 	e.EnquiryMobile, 	e.EnquiryEmail, 	e.EnquiryInterestedIn, 	e.EnquiryRemarks, 	e.EnquirySource, DATE_FORMAT(e.EnquiryDate, '%d-%M-%Y') as EnquiryDate, e.EnquiryAssignToUser,	s.EnquiryStatusName, CONCAT(u.FirstName, ' ', u.LastName) as AssignedToUser
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
		LEFT JOIN tbl_users u ON e.EnquiryAssignToUser = u.UserID
		WHERE e.StatusID = 2 AND e.EnquiryInstituteID = '$InstituteID' $filter				
		ORDER BY e.EnquiryID
		LIMIT $PageNo, $PageSize
		";			

		$Query = $this->db->query($sql);

		$Records = array();
		
		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Record)
			{
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;

			return $Return;
		}

		return FALSE;		
	}



	function addEnquiry($EntityID, $Input=array())
	{
		if(!empty($Input['EnquiryGUID']))
		{
			$this->db->select('EnquiryGUID');
			$this->db->from('tbl_enquiry');
			$this->db->where('EnquiryAssignToUser >', 0);
			$this->db->where('EnquiryGUID',$Input['EnquiryGUID']);
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0)
			{
				return 'Enquiry Assigned';
				die;
			}
		}

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$EntityID = $this->Common_model->getInstituteByEntity($this->EntityID);
		
		$this->db->trans_start();


		if(!empty($Input['EnquiryGUID']))
		{
			$InsertData = array_filter(array(			
			"EnquiryPersonName" => @$Input['FullName'],
			"EnquiryMobile" =>	@strtolower(@$Input['PhoneNumber']),
			"EnquiryEmail" =>	@$Input['Email'],
			"EnquiryInterestedIn" =>	@$Input['InterestedIn'],						
			"EnquiryRemarks" =>	@$Input['Remarks'],
			"EnquirySource" =>	@$Input['Source']));

			$this->db->where('EnquiryInstituteID', $EntityID);
			$this->db->where('EnquiryGUID', $Input['EnquiryGUID']);
			
			$this->db->update('tbl_enquiry', $InsertData);

			$EnquiryID = $this->db->affected_rows();
		}
		else
		{
			$InstituteID = $EntityID;

			$EnquiryGUID = get_guid();

			$InsertData = array_filter(array(
			"EnquiryGUID" => $EnquiryGUID,			
			"EnquiryInstituteID" =>	$InstituteID,
			"EnquiryPersonName" => @$Input['FullName'],
			"EnquiryMobile" =>	@strtolower(@$Input['PhoneNumber']),
			"EnquiryEmail" =>	@$Input['Email'],
			"EnquiryInterestedIn" =>	@$Input['InterestedIn'],						
			"EnquiryRemarks" =>	@$Input['Remarks'],
			"EnquirySource" =>	@$Input['Source'],
			"EnquiryDate" 	=>	date('Y-m-d H:i:s')));

			$this->db->insert('tbl_enquiry', $InsertData);
			
			$EnquiryID = $this->db->insert_id();
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $EnquiryID;
		
	}



	function getEnquiry($EntityID, $EnquiryGUID)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('e.EnquiryGUID, 	e.EnquiryPersonName, 	e.EnquiryMobile, 	e.EnquiryEmail, 	e.EnquiryInterestedIn, 	e.EnquiryRemarks, 	e.EnquirySource, DATE_FORMAT(e.EnquiryDate, "%d-%M-%Y") as EnquiryDate, 	s.EnquiryStatusName');
				
		$this->db->from('tbl_enquiry e');
		$this->db->from('tbl_enquiry_status s');

		$this->db->where('e.EnquiryStatus','s.EnquiryStatusID',FALSE);		
		$this->db->where("e.EnquiryInstituteID",$InstituteID);
		$this->db->where("e.EnquiryGUID",$EnquiryGUID);		
		
		$this->db->limit(1);

		$Query =  $this->db->get();

		$Records = array();

		if($Query->num_rows() > 0)
		{
			foreach ($Query->result_array() as $record) 
			{				
				array_push($Records, $record);
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}


	function assignEnquiry($EntityID, $EnquiryGUID)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('e.EnquiryGUID, 	e.EnquiryPersonName, 	e.EnquiryMobile, 	e.EnquiryEmail, 	e.EnquiryInterestedIn, 	e.EnquiryRemarks, 	e.EnquirySource, DATE_FORMAT(e.EnquiryDate, "%d-%M-%Y") as EnquiryDate, e.EnquiryAssignToUser,	s.EnquiryStatusName');
				
		$this->db->from('tbl_enquiry e');
		$this->db->from('tbl_enquiry_status s');

		$this->db->where('e.EnquiryStatus', 's.EnquiryStatusID',FALSE);		
		$this->db->where("e.EnquiryInstituteID", $InstituteID);
		$this->db->where("e.EnquiryGUID", $EnquiryGUID);

		$this->db->limit(1);

		$Query =  $this->db->get();

		$Records = array();

		if($Query->num_rows() > 0)
		{
			foreach ($Query->result_array() as $record) 
			{				
				array_push($Records, $record);
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}


	function getInstituteUsers($EntityID, $EnquiryGUID = '')
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		/*$this->db->select('u.FirstName, u.LastName, t.UserTypeName, u.UserID');
				
		$this->db->from('tbl_entity e');
		$this->db->from('tbl_users u');
		$this->db->from('tbl_users_type t');
		$this->db->from('admin_second_level_permission p');

		$this->db->where('e.EntityID','u.UserID',FALSE);
		$this->db->where('u.UserTypeID','t.UserTypeID',FALSE);
		$this->db->where('p.UserTypeID','u.UserTypeID',FALSE);
				
		$this->db->where("e.InstituteID",$InstituteID);
		$this->db->where("e.EntityTypeID", 1);		
		$this->db->where("t.UserTypeID !=", 10);
		$this->db->where("t.UserTypeID !=", 7);
		$this->db->where("t.UserTypeID !=", 1);
		$this->db->where("u.AdminAccess =", 'Yes');

		$this->db->where("p.ModuleID", 41);
		$this->db->where("p.Action", 'all');

		$this->db->order_by('u.FirstName','ASC');

		$Query =  $this->db->get();*/

		//Condition for removing previous assigned user-------------------------
		$append = "";
		if(isset($EnquiryGUID) && !empty($EnquiryGUID))
		{
			$append = " AND u.UserID NOT IN(SELECT q.EnquiryAssignToUser FROM tbl_enquiry q WHERE q.EnquiryGUID = '$EnquiryGUID') ";
		}

		$sql = "SELECT u.FirstName, u.LastName, t.UserTypeName, u.UserID
		FROM tbl_entity e
		INNER JOIN tbl_users u ON e.EntityID = u.UserID AND u.AdminAccess = 'Yes'
		INNER JOIN tbl_users_type t ON u.UserTypeID = t.UserTypeID AND t.UserTypeID NOT IN (1,7,10)
		INNER JOIN admin_second_level_permission p ON p.UserTypeID = u.UserTypeID AND p.ModuleID = 41 AND p.Action = 'all'
		WHERE e.InstituteID = '$InstituteID' $append
		GROUP BY u.UserID
		ORDER BY u.FirstName
		";		

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows() > 0)
		{
			foreach ($Query->result_array() as $record) 
			{				
				array_push($Records, $record);
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}


	function addAssignEnquiry($EntityID, $Input=array(), $isReassign = 0)
	{		
		$this->db->trans_start();

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$EnquiryID = 0;

		if(!empty($Input['EnquiryGUID']))
		{
			$InsertData = array_filter(array(			
			"EnquiryAssignByUser" => $EntityID,
			"EnquiryAssignToUser" =>	@$Input['AssignedTo'],
			"EnquiryAssignDate" =>	date("Y-m-d H:i:s")));

			if(isset($isReassign) && $isReassign == 1)
			{
				$InsertData["EnquiryStatus"] = 1;
				$InsertData["EnquiryIsReassigned"] = 1;
				$InsertData["EnquiryLastestRemark"] = "";
				$InsertData["EnquiryLastestFollowDate"] = "";
			}


			$this->db->where('EnquiryInstituteID', $InstituteID);
			$this->db->where('EnquiryGUID', $Input['EnquiryGUID']);

			$this->db->update('tbl_enquiry', $InsertData);

			
			$InsertData = array_filter(array(
			"EnquiryAssignBy" => $EntityID,			
			"EnquiryAssignTo" =>	@$Input['AssignedTo'],
			"EnquiryAssignDate" => date("Y-m-d H:i:s"),
			"EnquiryAssignRemarks" =>	$Input['Remarks']));

			$this->db->insert('tbl_enquiry_assign_log', $InsertData);
			
			$EnquiryID = $this->db->insert_id();
		}	
		
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $EnquiryID;
		
	}



	function deleteEnquiry($EntityID, $EnquiryGUID)
    {
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

        $this->db->where("EnquiryInstituteID", $InstituteID);
        
        $this->db->where("EnquiryGUID", $EnquiryGUID);
        
        $this->db->update("tbl_enquiry", array(
            "StatusID" => 3
        ));
        
        return $this->db->affected_rows();
    }



    /*
	Description: 	Use to get Assigned Enquiry Count
	*/
	function getAssignedEnquiryCount($EntityID, $Filters)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$filters_app = "";
		if(isset($Filters['filterStatus']) && !empty($Filters['filterStatus']))
		{
			$filterval = $Filters['filterStatus'];
			$filters_app .= " AND s.EnquiryStatusName = '".$filterval."' ";
		}

		if(isset($Filters['filterSource']) && !empty($Filters['filterSource']))
		{
			$filterval = $Filters['filterSource'];
			$filters_app .= " AND e.EnquirySource = '".$filterval."' ";
		}

		if(isset($Filters['filterAssignedToUser']) && !empty($Filters['filterAssignedToUser']))
		{
			$filterval = $Filters['filterAssignedToUser'];
			$filters_app .= " AND e.EnquiryAssignToUser = '".$filterval."' ";
		}

		if(isset($Filters['filterFromDate']) && !empty($Filters['filterFromDate']) && isset($Filters['filterToDate']) && !empty($Filters['filterToDate']))
		{
			$filterFromDate = date("Y-m-d", strtotime($Filters['filterFromDate']));
			$filterToDate = date("Y-m-d", strtotime($Filters['filterToDate']));

			$filters_app .= " AND (DATE(e.EnquiryDate) >= '$filterFromDate' AND DATE(e.EnquiryDate) <= '$filterToDate' ) ";
		}


		/*$sql = "SELECT e.EnquirySource, s.EnquiryStatusName, COUNT(s.EnquiryStatusID) as count
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
		WHERE e.EnquiryInstituteID = '$InstituteID' AND e.EnquiryAssignToUser > 0 AND (DATE(e.EnquiryDate) BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()) $filters_app
		GROUP BY e.EnquirySource, s.EnquiryStatusID
		";*/	

		$sql = "SELECT e.EnquirySource, s.EnquiryStatusName, COUNT(s.EnquiryStatusID) as count
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
		WHERE e.EnquiryInstituteID = '$InstituteID' AND e.EnquiryAssignToUser > 0 $filters_app
		GROUP BY e.EnquirySource, s.EnquiryStatusID
		";	
		

		$query = $this->db->query($sql);

		$data = $query->result(); 

		$Records = array();

		$sourceArr = array('Reference', 'Website', 'App', 'Walkin', 'Advertisement');
		$statusArr = array('Hot', 'Mild', 'Cold', 'Closed', 'Dropped');

		if(isset($data) && !empty($data))
		{
			foreach ($data as $obj) 
			{				
				$Records[$obj-> EnquirySource][$obj-> EnquiryStatusName] = $obj-> count;
			}			
		}


		$dataCount = array();
		foreach($sourceArr as $source)
		{
			foreach($statusArr as $status)
			{
				if(isset($Records[$source][$status]) && !empty($Records[$source][$status]))
					$dataCount[$source][$status] = $Records[$source][$status];
				else
					$dataCount[$source][$status] = 0;
			}
		}

		$Records = array(); //Reset and empty the array

		return $dataCount;
	}



	/*
	Description: 	Use to load Enquiry By Status
	*/
	function loadEnquiryByStatus($EntityID, $SourceID, $StatusID)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$sql = "SELECT e.EnquiryGUID, e.EnquiryPersonName, e.EnquiryMobile, e.EnquiryEmail, e.EnquiryDate, e.EnquiryLastestRemark, COUNT(f.EnquiryFollowupID) as totalCalls
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID AND s.EnquiryStatusName = '$StatusID'
		LEFT JOIN tbl_enquiry_follow_up f ON e.EnquiryID = f.EnquiryParentID
		WHERE e.EnquiryInstituteID = '$InstituteID' AND s.EnquiryStatusName = '$StatusID' AND e.EnquirySource = '$SourceID'
		GROUP BY e.EnquiryID
		ORDER BY e.EnquiryDate, e.EnquiryPersonName
		";		

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;

			return $Return;
		}

		return FALSE;
	}


	function getAssignedUsers()
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		/*$this->db->select('CONCAT(UPPER(SUBSTRING(u.FirstName,1,1)),LOWER(SUBSTRING(u.FirstName,2))) AS FirstName, u.LastName, t.UserTypeName, u.UserID');
				
		$this->db->from('tbl_enquiry te');
		$this->db->from('tbl_entity e');
		$this->db->from('tbl_users u');
		$this->db->from('tbl_users_type t');
		$this->db->from('admin_second_level_permission p');

		$this->db->where('e.EntityID','u.UserID',FALSE);
		$this->db->where('u.UserTypeID','t.UserTypeID',FALSE);
		$this->db->where('te.EnquiryAssignToUser','u.UserID',FALSE);
		$this->db->where('te.EnquiryAssignToUser > ', 0);
		$this->db->where('p.UserTypeID','u.UserTypeID',FALSE);
				
		$this->db->where("e.InstituteID",$InstituteID);
		//$this->db->where("e.EntityTypeID", 1);		
		$this->db->where("te.EnquiryAssignToUser >", 0);

		$this->db->where("t.UserTypeID !=", 10);
		$this->db->where("t.UserTypeID !=", 7);
		$this->db->where("t.UserTypeID !=", 1);
		$this->db->where("u.AdminAccess =", 'Yes');
		$this->db->where("p.ModuleID", 41);
		$this->db->where("p.Action", 'all');
		
		
		$this->db->group_by('te.EnquiryAssignToUser');
		$this->db->order_by('u.FirstName','ASC');*/




		$sql = "SELECT u.FirstName, u.LastName, t.UserTypeName, u.UserID
		FROM tbl_entity e
		INNER JOIN tbl_users u ON e.EntityID = u.UserID AND u.AdminAccess = 'Yes'
		INNER JOIN tbl_users_type t ON u.UserTypeID = t.UserTypeID AND t.UserTypeID NOT IN (1,7,10)
		INNER JOIN tbl_enquiry te ON te.EnquiryAssignToUser = u.UserID AND te.EnquiryAssignToUser > 0
		INNER JOIN admin_second_level_permission p ON p.UserTypeID = u.UserTypeID AND p.ModuleID = 41 AND p.Action = 'all'
		WHERE e.InstituteID = '$InstituteID'
		GROUP BY u.UserID
		ORDER BY u.FirstName
		";

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows() > 0) 
		{
			foreach ($Query->result_array() as $record) 
			{				
				$Records[] = $record;
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}



	/*
	Description: 	Use to load Enquiry Assigned To Login Users
	*/
	function getEnquiryAssignedToList($PageNo=1, $PageSize=10, $Inputs = array())
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$PageNo = ($PageNo -1) * $PageSize;


		//Search Filters--------------------
		$filter = "";
		if(isset($Inputs['filterStatus']) && !empty($Inputs['filterStatus']))
		{
			$filter .= " AND e.EnquiryStatus = ".$Inputs['filterStatus'];
		}

		if(isset($Inputs['filterSource']) && !empty($Inputs['filterSource']))
		{
			$filter .= " AND e.EnquirySource = '".$Inputs['filterSource']."' ";
		}

		if(isset($Inputs['Keyword']) && !empty($Inputs['Keyword']))
		{
			$word = trim($Inputs['Keyword']);
			$filter .= " AND ( e.EnquiryPersonName LIKE '%$word%' OR e.EnquiryMobile LIKE '%$word%' OR e.EnquiryEmail LIKE '%$word%' OR e.EnquiryInterestedIn LIKE '%$word%' OR e.EnquirySource LIKE '%$word%' ) ";
		}

		if(isset($Inputs['filterFromDate']) && !empty($Inputs['filterFromDate']) && isset($Inputs['filterToDate']) && !empty($Inputs['filterToDate']))
		{
			$filterFromDate = date("Y-m-d", strtotime($Inputs['filterFromDate']));
			$filterToDate = date("Y-m-d", strtotime($Inputs['filterToDate']));

			$filter .= " AND (DATE(e.EnquiryNextFollowDate) >= '$filterFromDate' AND DATE(e.EnquiryNextFollowDate) <= '$filterToDate' ) ";
		}


		$sql = "SELECT e.EnquiryGUID, e.EnquiryPersonName, e.EnquiryMobile, e.EnquiryEmail, e.EnquiryDate, e.EnquiryLastestRemark, e.EnquiryInterestedIn, e.EnquirySource, DATE_FORMAT(e.EnquiryLastestFollowDate, '%d-%M-%Y') as EnquiryLastestFollowDate, COUNT(f.EnquiryFollowupID) as totalCalls, s.EnquiryStatusName, DATE_FORMAT(e.EnquiryNextFollowDate, '%d-%M-%Y') as NextFollowDate
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
		LEFT JOIN tbl_enquiry_follow_up f ON e.EnquiryID = f.EnquiryParentID
		WHERE e.EnquiryInstituteID = '$InstituteID' AND e.EnquiryAssignToUser = '".$this->EntityID."' $filter
		GROUP BY e.EnquiryID
		ORDER BY e.EnquiryDate, e.EnquiryPersonName
		LIMIT $PageNo, $PageSize
		";		

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Record)
			{
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;

			return $Return;
		}

		return FALSE;
	}


	/*
	Description: 	Use to get Enquiry Of Assigned User
	*/
	function getEnquiryOfAssignedUser($EntityID, $EnquiryGUID)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('e.EnquiryGUID, 	e.EnquiryPersonName, 	e.EnquiryMobile, 	e.EnquiryEmail, 	e.EnquiryInterestedIn, 	e.EnquiryRemarks, 	e.EnquirySource, e.EnquiryLastestRemark, DATE_FORMAT(e.EnquiryLastestFollowDate, "%d-%M-%Y") as EnquiryLastestFollowDate');
				
		$this->db->from('tbl_enquiry e');
		
		$this->db->where("e.EnquiryInstituteID",$InstituteID);
		$this->db->where("e.EnquiryGUID",$EnquiryGUID);
		$this->db->where("e.EnquiryAssignToUser", $this->EntityID);		
		
		$this->db->limit(1);

		$Query =  $this->db->get();

		$Records = array();

		if($Query->num_rows() > 0)
		{
			foreach ($Query->result_array() as $record) 
			{				
				array_push($Records, $record);
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}


	/*
	Description: 	Use to save call data of user
	*/
	function addCallData($EntityID, $Input=array(), $EnquiryParentID)
	{
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$EntityID = $this->Common_model->getInstituteByEntity($this->EntityID);
		$InstituteID = $EntityID;

		$this->db->trans_start();

		$EnquiryID = 0;

		if(!empty($Input['EnquiryGUID']))
		{
			$datetime = date("Y-m-d H:i:s");

			//Update enquiry record for latest status, remarks and date---------------------
			$InsertData = array_filter(array(			
			"EnquiryStatus" => @$Input['Status'],
			"EnquiryLastestRemark" =>	@$Input['Remarks'],
			"EnquiryNextFollowDate" =>	date("Y-m-d", strtotime($Input['FollowUpDate'])),
			"EnquiryLastestFollowDate" =>	$datetime));

			$this->db->where('EnquiryInstituteID', $InstituteID);
			$this->db->where('EnquiryGUID', $Input['EnquiryGUID']);
			$this->db->where('EnquiryAssignToUser', $this->EntityID);
			
			$this->db->update('tbl_enquiry', $InsertData);
		

			//Save follow up data-----------------------------------------------------------
			$InsertData = array_filter(array(
			"EnquiryParentID" => $EnquiryParentID,			
			"EnquiryByUser" =>	$this->EntityID,
			"EnquiryFollowupRemarks" => @$Input['Remarks'],
			"EnquiryFollowupStatus" =>	@$Input['Status'],
			"EnquiryNextFollowDate" =>	date("Y-m-d", strtotime($Input['FollowUpDate'])),
			"EnquiryFollowupDate" =>	$datetime));

			$this->db->insert('tbl_enquiry_follow_up', $InsertData);
			
			$EnquiryID = $this->db->insert_id();
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $EnquiryID;
		
	}



	/*
	Description: 	Use to load Call Log History Of User
	*/
	function getCallLogHistoryOfUser($EntityID, $EnquiryGUID)
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);		

		/*$sql = "SELECT DATE_FORMAT(f.EnquiryFollowupDate, '%d-%M-%Y %H:%i:%s') as CallDate, f.EnquiryFollowupRemarks as Remarks, s.EnquiryStatusName as Status
		FROM tbl_enquiry_follow_up f
		INNER JOIN tbl_enquiry_status s ON f.EnquiryFollowupStatus = s.EnquiryStatusID
		INNER JOIN tbl_enquiry e ON f.EnquiryParentID = e.EnquiryID AND e.EnquiryInstituteID = '$InstituteID' AND e.EnquiryAssignToUser = '".$this->EntityID."' AND e.EnquiryGUID = '".$EnquiryGUID."'
		WHERE f.EnquiryByUser = '".$this->EntityID."' 		
		ORDER BY f.EnquiryFollowupDate
		";*/

		$sql = "SELECT DATE_FORMAT(f.EnquiryFollowupDate, '%d-%M-%Y %H:%i:%s') as CallDate, f.EnquiryFollowupRemarks as Remarks, s.EnquiryStatusName as Status, f.EnquiryByUser, DATE_FORMAT(f.EnquiryNextFollowDate, '%d-%M-%Y') as NextFollowDate
		FROM tbl_enquiry_follow_up f
		INNER JOIN tbl_enquiry_status s ON f.EnquiryFollowupStatus = s.EnquiryStatusID
		INNER JOIN tbl_enquiry e ON f.EnquiryParentID = e.EnquiryID AND e.EnquiryInstituteID = '$InstituteID' AND e.EnquiryAssignToUser = '".$this->EntityID."' AND e.EnquiryGUID = '".$EnquiryGUID."'		 		
		ORDER BY f.EnquiryFollowupDate
		";		

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Record)
			{
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;

			return $Return;
		}

		return FALSE;
	}


	/*
	Description: 	Use to get Enquiry List
	*/
	function getChurnedEnquiryList($PageNo=1, $PageSize=10, $Inputs = array())
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);		

		$PageNo = ($PageNo -1) * $PageSize;


		//Search Filters--------------------
		$filter = "";
		if(isset($Inputs['filterStatus']) && !empty($Inputs['filterStatus']))
		{
			$filter .= " AND e.EnquiryStatus = ".$Inputs['filterStatus'];
		}

		if(isset($Inputs['filterSource']) && !empty($Inputs['filterSource']))
		{
			$filter .= " AND e.EnquirySource = '".$Inputs['filterSource']."' ";
		}

		if(isset($Inputs['filterAssignedToUser']) && !empty($Inputs['filterAssignedToUser']))
		{
			$filter .= " AND e.EnquiryAssignToUser = ".$Inputs['filterAssignedToUser'];
		}

		if(isset($Inputs['Keyword']) && !empty($Inputs['Keyword']))
		{
			$word = trim($Inputs['Keyword']);
			$filter .= " AND ( e.EnquiryPersonName LIKE '%$word%' OR e.EnquiryMobile LIKE '%$word%' OR e.EnquiryEmail LIKE '%$word%' OR e.EnquiryInterestedIn LIKE '%$word%' OR e.EnquiryLastestRemark LIKE '%$word%' OR u.FirstName LIKE '%$word%' OR u.LastName LIKE '%$word%') ";
		}


		$sql = "SELECT e.EnquiryGUID, e.EnquiryPersonName, e.EnquiryMobile, e.EnquiryEmail, e.EnquiryInterestedIn, e.EnquirySource,  DATE_FORMAT(f.EnquiryFollowupDate, '%d-%M-%Y %H:%i:%s') as LastCallDate, e.EnquiryLastestRemark as LastRemarks, s.EnquiryStatusName as CurrentStatus, COUNT(f.EnquiryFollowupID) as totalCalls, CONCAT(u.FirstName,' ', u.LastName) as AssignedTo
		FROM tbl_enquiry e
		INNER JOIN tbl_enquiry_status s ON e.EnquiryStatus = s.EnquiryStatusID
		INNER JOIN tbl_enquiry_follow_up f ON f.EnquiryParentID = e.EnquiryID
		INNER JOIN tbl_users u ON u.UserID = e.EnquiryAssignToUser
		WHERE e.EnquiryStatus IN (4,5) AND e.EnquiryInstituteID = '$InstituteID' $filter
		GROUP BY e.EnquiryID		
		ORDER BY e.EnquiryID
		LIMIT $PageNo, $PageSize
		";		

		$Query = $this->db->query($sql);

		$Records = array();

		if($Query->num_rows()>0)
		{
			$Return['Data']['TotalRecords'] = $Query->num_rows();

			foreach($Query->result_array() as $Record)
			{
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;

			return $Return;
		}

		return FALSE;
	}


	//Process bulk assign request--------------------------------
	function addBulkAssignEnquiry($EntityID, $Input=array())
	{		
		$this->db->trans_start();

		$EnquiryID = array();
		
		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(isset($Input['EnquiryGUID']) && !empty($Input['EnquiryGUID']))
		{
			$EnquiryGUID_Arr = explode(",", $Input['EnquiryGUID']);
			foreach($EnquiryGUID_Arr as $EnquiryGUID)
			{	
				$InsertData = array_filter(array(			
				"EnquiryAssignByUser" => $EntityID,
				"EnquiryAssignToUser" => @$Input['AssignedTo'],
				"EnquiryAssignDate" =>	date("Y-m-d H:i:s")));

				$this->db->where('EnquiryInstituteID', $InstituteID);
				$this->db->where('EnquiryGUID', $EnquiryGUID);
				$this->db->update('tbl_enquiry', $InsertData);

				
				$InsertData = array_filter(array(
				"EnquiryAssignBy" => $EntityID,			
				"EnquiryAssignTo" =>	@$Input['AssignedTo'],
				"EnquiryAssignDate" => date("Y-m-d H:i:s"),
				"EnquiryAssignRemarks" =>	$Input['Remarks']));

				/*if($actionID == 0)
				{
					$InsertData["EnquiryIsUnAssign"] = 1;
					$InsertData["EnquiryUnAssignDate"] = date("Y-m-d H:i:s");					
				}*/

				$this->db->insert('tbl_enquiry_assign_log', $InsertData);
				
				$EnquiryID[] = $this->db->insert_id();
			}	
		}	
		
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $EnquiryID;
		
	}


	/*Get the Interested In data for particular institute--------------------------*/
	function getInterestedIn()
	{	
		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID); 

		$this->db->select('e.EnquiryInterestedIn');
		$this->db->from('tbl_enquiry e');				
		$this->db->where("e.EnquiryInstituteID", $InstituteID);		
		$this->db->group_by('e.EnquiryInterestedIn');
		$this->db->order_by('e.EnquiryInterestedIn','ASC');

		$Query =  $this->db->get();

		$Records = array();

		if($Query->num_rows() > 0)
		{
			foreach ($Query->result_array() as $record) 
			{				
				array_push($Records, $record);
			}
			
			return $Records;
		}
		else
		{
			return $Records;
		}
	}



	//Process bulk unassign request--------------------------------
	function addBulkUnAssignEnquiry($EntityID, $Input=array())
	{		
		$this->db->trans_start();

		$EnquiryID = array();

		$EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		if(isset($Input['EnquiryChk']) && !empty($Input['EnquiryChk']))
		{
			$EnquiryGUID_Arr = $Input['EnquiryChk'];

			foreach($EnquiryGUID_Arr as $EnquiryGUID)
			{	
				$InsertData = array(			
				"EnquiryAssignByUser" => $EntityID,
				"EnquiryAssignToUser" => 0,
				"EnquiryAssignDate" =>	'');

				$this->db->where('EnquiryInstituteID', $InstituteID);
				$this->db->where('EnquiryGUID', $EnquiryGUID);
				$this->db->where('EnquiryAssignToUser', @$Input['AssignedTo']);
				$this->db->update('tbl_enquiry', $InsertData);
				
				
				$InsertData = array(
				"EnquiryAssignBy" => $EntityID,			
				"EnquiryAssignTo" => @$Input['AssignedTo'],
				"EnquiryAssignDate" => '',
				"EnquiryIsUnAssign" => 1,
				"EnquiryUnAssignDate" => date("Y-m-d H:i:s"),
				"EnquiryAssignRemarks" =>	'Unassigned');

				$this->db->insert('tbl_enquiry_assign_log', $InsertData);
				
				$EnquiryID[] = $this->db->insert_id();
			}	
		}	
		
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $EnquiryID;
		
	}
}