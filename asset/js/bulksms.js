app.controller('PageController', function ($scope, $http,$timeout){
  $scope.show_students = false;
  $scope.data.SecondLevelPermission = [];
  $scope.SessionKey = SessionKey;

  $scope.getFilterData = function (check="",CourseGUID="")
  {
      if(!$scope.data.SecondLevelPermission.length){
          $scope.getSecondLevelPermission();
      }
      $scope.noRecords = false;

      var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

      $scope.data.listLoading = true;

      $http.post(API_URL+'bulksms/getCourseAndBatch', data, contentType).then(function(response) {

          var response = response.data;

          if(response.ResponseCode==200 && response.Data){ /* success case */

           $scope.filterData =  response.Data;

           $scope.data.listLoading = false;

          if($scope.filterData.length > 0){
            $scope.noRecords = false;
          }else{
            $scope.noRecords = true;
          }

          }else{
            $scope.noRecords = true;
          }
       });

      if(check!=1){
        $scope.getStaffList();
        $scope.getRoles();
        $http.post(API_URL+'bulksms/getAvailableSMSCredits', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

               $scope.SMSCredits =  response.Data;

               console.log($scope.SMSCredits);

           }
        });

       
          $scope.categoryDataList = [];
          $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
              var response = response.data;
              if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                  $scope.data.totalRecords = response.Data.TotalRecords;
                  for (var i in response.Data.Records) {
                      for(var j in response.Data.Records[i].SubCategories.Records){                           
                          $scope.categoryDataList.push(response.Data.Records[i].SubCategories.Records[j]);
                      } 
                  }          
              } ////console.log($scope.categoryDataList);
          });
      }
  }


  $scope.getRoles = function ()
  {
      $scope.data.listLoading = true;
      $scope.data.roles = [];
      var data = 'SessionKey='+SessionKey;

      $http.post(API_URL+'roles/getRoles', data, contentType).then(function(response) {

             var response = response.data;

             if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                 // $scope.data.totalRecords = response.Data.TotalRecords;

                  for (var i in response.Data.Records) {

                   $scope.data.roles.push(response.Data.Records[i]);
                  }
          

             }

            $scope.data.listLoading = false;
      });
  }


  $scope.getStaffList = function($UserTypeID=""){
      var data = 'SessionKey='+SessionKey+'&IsAdmin=Yes&UserTypeID='+$UserTypeID;
       $scope.data.listLoading = true; 
     $scope.data.staffList = [];
     $scope.UserTypeID = UserTypeID;
      $http.post(API_URL+'bulksms/getStaffList', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) {

                   $scope.data.staffList.push(response.Data[i]);

               }

               $scope.data.listLoading = false; 

               $scope.data.pageNo++;               
            }else{

                $scope.data.noRecords = true;
            }

             $scope.data.listLoading = false; 

            $scope.data.listLoading = false;

      });
  }


  $scope.selectUser = function(data,check="")
  {
    console.log('.chkSelect'+data);

    if($('.course_details' + data).is(':checked')){
      $('.batch_details'+data).prop("checked", false);
    }

    if (($('#course' + data).is(':checked') || $('#batch' + data).is(':checked')) || ($('#course' + data).is(':checked') && $('#batch' + data).is(':checked'))) {
     // $('#batch' + data).prop("checked", true);
     // $('#course' + data).prop("checked", true);
      $('.course_details'+data).prop("checked", true);
      $('.batch_details'+data).prop("checked", true);
    }else {
      //$('#batch' + data).prop("checked", false);
      //$('#course' + data).prop("checked", false);
      $('.course_details'+data).prop("checked", false);
      $('.batch_details'+data).prop("checked", false);
    }
  } 


  $scope.selectStaff = function(data,check="")
  {
    console.log('.chkSelect'+data);

    if($('.course_details' + data).is(':checked')){
      $('.staff_details'+data).prop("checked", false);
    }

    if (($('#role' + data).is(':checked') || $('#staff' + data).is(':checked')) || ($('#role' + data).is(':checked') && $('#staff' + data).is(':checked'))) {
     // $('#batch' + data).prop("checked", true);
     // $('#course' + data).prop("checked", true);
      // $('#role' + data).prop("checked", true);
      // $('#staff' + data).prop("checked", true);
      $('.course_details'+data).prop("checked", true);
      $('.staff_details'+data).prop("checked", true);
    }else {
      //$('#batch' + data).prop("checked", false);
      //$('#course' + data).prop("checked", false);
      $('#role' + data).prop("checked", false);
      $('#staff' + data).prop("checked", false);
      $('.course_details'+data).prop("checked", false);
      $('.staff_details'+data).prop("checked", false);
    }
  } 

  /*add data*/
  $scope.addData = function ()
  {
      $scope.addDataLoading = true;
      var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
      $http.post(API_URL+'admin/users/broadcast', data, contentType).then(function(response) {
          var response = response.data;
          if(response.ResponseCode==200){ /* success case */               
              SuccessPopup(response.Message);
          }else{
              ErrorPopup(response.Message);
          }
          $scope.addDataLoading = false;          
      });
  }


  $scope.showStudents = function(data)
  {
     if(data=="Students"){
      $scope.show_students = true;
     }else{
      $scope.show_students = false;
     }
  }


  $scope.sendMessage = function(){
    $scope.addDataLoading = true;
      var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
      //console.log(data);
      $http.post(API_URL+'bulksms/sendMessage', data, contentType).then(function(response) {
          var response = response.data;
          if(response.ResponseCode==200){ /* success case */               
              SuccessPopup(response.Message);
              $timeout(function(){
                  location.reload();
              }, 2000);
          }else{
              ErrorPopup(response.Message);
          }
          $scope.addDataLoading = false;          
      });
  }


   /*load add form*/
    $scope.loadBuyCredit = function (Position)
    {
     

      $scope.data.pageLoading = true;

      $scope.data.Position = Position;

      $scope.templateURLAdd = PATH_TEMPLATE+'bulksms/sms_credits.htm?'+Math.random();

        $scope.data.pageLoading = false;

        $('#add_model').modal({show:true});

        // $timeout(function(){  
           
        //    tinymce.init({selector: '.MediaContent' });

        //    $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
          

        // }, 200);
    }


    


    /*load add form*/
    $scope.LoadBuyNow = function (OrderAmount)
    {
     
        $scope.OrderAmount = OrderAmount;
        $('#add_model .close').click();

        $scope.data.pageLoading = true;

       // $scope.data.Position = Position;

        $scope.templateURLPayment = PATH_TEMPLATE+'bulksms/payment_form.htm?'+Math.random();

        $scope.data.pageLoading = false;

        $('#add_payment').modal({show:true});

        // $timeout(function(){  
           
        //    tinymce.init({selector: '.MediaContent' });

        //    $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
          

        // }, 200);
    }


     /*list append*/

    $scope.getSMSOrders = function (check,OrderType,UserGUID="")
    {
      if(!$scope.data.SecondLevelPermission.length){
          $scope.getSecondLevelPermission();
      }

       if ($scope.data.listLoading || $scope.data.noRecords) return;


       if(check!=1 && check!=0){
        var UserGUID = $scope.getUrlParameter('UserGUID');
        $scope.UserGUID = UserGUID;
        $scope.data.dataList = [];
       }

       $scope.data.listLoading = true;
       $scope.data.noRecords = true;
        var data = 'SessionKey='+SessionKey+'&menu_type='+OrderType+'&UserGUID='+UserGUID;

        $http.post(API_URL+'order/getSMSOrders', data, contentType).then(function(response) {

            var response = response.data;

            console.log(response.Data);

            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) {

                 $scope.data.dataList.push(response.Data[i]);

                }
                console.log($scope.data.dataList);
                $scope.data.pageNo++;               

            }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

        });

    }


    $scope.makePayment = function()
    {
        $scope.makePaymentLoading = true;

       // var PaymentType = $('input[name=PaymentType]');

        var PaymentType = $("input[name='PaymentType']:checked").val();

        if(PaymentType == 'Offline'){
            var ChequeNumber = $("#ChequeNumber").val();
            var DrawnBank = $("#DrawnBank").val();
            var ChequeDate = $("#ChequeDate").val();
            var ChequeAmount = $("#ChequeAmount").val();
            var DepositedDate = $("#DepositedDate").val();
            var MediaGUIDs = $("#MediaGUIDs").val();
            var OrderGUID = $("#OrderGUID").val();
            var OrderFor = "SMS Credits";
            var data = 'SessionKey='+SessionKey+'&OrderFor='+OrderFor+'&PaymentType='+PaymentType+'&ChequeNumber='+ChequeNumber+'&DrawnBank='+DrawnBank+'&ChequeDate='+ChequeDate+'&ChequeAmount='+ChequeAmount+'&DepositedDate='+DepositedDate+'&OrderGUID='+OrderGUID+'&MediaGUID='+MediaGUIDs;
        }else{

            $scope.makePaymentLoading = false;
           ErrorPopup("Sorry! We can not continue without payment details."); 
           return false;
        }

       // console.log(PaymentType); return false;

        //var data = 'SessionKey='+SessionKey+'&'+$("form[name='payment_form']").serialize();

        $http.post(API_URL+'order/makePayment', data, contentType).then(function(response) {

            var response = response.data;
            $scope.makePaymentLoading = false;
            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $('#add_payment .close').click();

                window.location.reload();

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });
    }


    /*load delete form*/

    $scope.loadFormView = function (Position, OrderGUID)

    {
        //$('#myModal').modal({show:true});
        console.log(OrderGUID);
        // if($('#myModal').is(':visible')){
        //     $('#myModal .close').click();
        // }
        

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+'bulksms/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

       

        $scope.data.pageLoading = false;

        var data = 'SessionKey='+SessionKey+'&OrderGUID='+OrderGUID;

        $http.post(API_URL+'order/getSMSOrders', data, contentType).then(function(response) {

            var response = response.data;

            console.log(response.Data);

            if(response.ResponseCode==200 && response.Data){ /* success case */

                
                $scope.formData = response.Data[0];  

                $('#view_model').modal({show:true});

            }

        });



    }


});

function display_payment_form(payment_type) {
   // alert(payment_type);
    if(payment_type == 'Offline'){
        $("#modal_footer").show();
        $("#online_payment_form").hide();
        $("#direct_payment_form").hide(); 
        $("#offline_payment_form").toggle();
        var currentTime = new Date();  
        $('input[name=ChequeDate]').datetimepicker({format: 'dd-mm-yyyy',minView: 2});
        //$('input[name=DepositedDate]').datetimepicker({format: 'dd-mm-yyyy',minView: 2});
    }else if(payment_type == 'Direct'){
        $("#modal_footer").show();
        $("#online_payment_form").hide();
        $("#offline_payment_form").hide();
        $("#direct_payment_form").toggle(); 
        var currentTime = new Date();  
       // $('input[name=DepositedOn]').datetimepicker({format: 'dd-mm-yyyy',minView: 2});       
    }else{
        $("#modal_footer").hide();
        $("#online_payment_form").show();
       $("#direct_payment_form").hide();
       $("#offline_payment_form").hide(); 
       $("#continue_payment").trigger("click");
    }
}


function setDepositeDate(date){ //alert(date);
  var d = new Date();

    var month = d.getMonth();

    var day = d.getDate();

    var year = d.getFullYear();

    $("#DepositedDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',date);

    //$('input[name=DepositedDate]').datetimepicker({pickDate: false });
  //$('input[name=DepositedDate]').datepicker({maxDate: date});
}


function showStudents(data)
{
   if(data=="Students"){
    $("#show_staff").hide();
    $("#show_students").show();
    $('.UserGUID').prop("checked", false);
    $(".rolecheck").prop("checked", false);
   }else if(data=="Staff"){
    $("#show_students").hide();
    $("#show_staff").show();
    $(".coursecheck").prop("checked", false);
    $('.batch_details').prop("checked", false);
   }else{
    $("#show_staff").hide();
    $("#show_students").hide();
    $(".coursecheck").prop("checked", false);
    $(".rolecheck").prop("checked", false);
    $('.UserGUID').prop("checked", false);
    $('.batch_details').prop("checked", false);
   }
}

function getStaffList(UserTypeID){
  angular.element(document.getElementById('content-body')).scope().getStaffList(UserTypeID);  
}