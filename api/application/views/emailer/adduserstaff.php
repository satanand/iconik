<h2 style="margin-bottom:20px; font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; ">Hi <?php echo $Name;?>,</h2>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Welcome to <?php  echo SITE_NAME; ?>.</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; "><?php echo $EmailText; ?></p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; "><?php echo $Token; ?></p>

<a href="<?php echo base_url(); ?>/recovery/reset">Please Click Set Password</a>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">If you did not request you can safely ignore this email. Rest assured your account is safe.</p>