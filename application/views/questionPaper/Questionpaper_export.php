<table border="0" width="100%">
<tr>
	<td width="10%">
		<?php	
		
		if(isset($session_data['InstituteProfilePic']) && !empty($session_data['InstituteProfilePic']))
		{
			$ProfilePic = $session_data['InstituteProfilePic'];
		}
		elseif(isset($session_data['ProfilePic']) && !empty($session_data['ProfilePic']))
		{
			$ProfilePic = $session_data['ProfilePic'];
		}
		else
		{
			$ProfilePic = "default.jpg";
		}	
			
		?>

		<img src="<?php echo $ProfilePic;?>" width="100px" height="70px">
	</td>

	<td width="90%" align="center">
		<h1><?php echo ucwords($session_data['FullName']);?></h1>
		<?php echo ucwords($session_data['Address'].", ".$session_data['CityName'].', '.$session_data['State']);?>
		<br/><?php echo $session_data['EmailForChange'];?>
	</td>
</tr>		
</table>

<hr class="light-grey-hr" />



<table border="0" width="100%">
<tr>
<td colspan="2" align="center"><b>Title:</b>&nbsp;<?php echo ucfirst($listing_data['Data']['QtPaperTitle']);?></td>
</tr>

<tr>
	<td><b>Total Questions:</b>&nbsp;<?php echo $listing_data['Data']['TotalQuestions'];?></td>
	<td align="right"><b>Total Marks:</b>&nbsp;<?php echo $listing_data['Data']['Total_marks'];?></td>
</tr>

<tr>
<td colspan="2"><hr class="light-grey-hr" /></td>
</tr>

<tr>
<td colspan="2"><b>Instruction:</b>&nbsp;<?php echo $listing_data['Data']['Instruction'];?></td>
</tr>
</table>

<hr class="light-grey-hr" />


<table border="0" width="100%">
<?php
if(isset($listing_data['Data']['question_answer']) && !empty($listing_data['Data']['question_answer']))
{
	foreach($listing_data['Data']['question_answer'] as $key => $arr)
	{
?>
		<tr>
			<td>
				<b>Question <?php echo ($key + 1);?>:</b>&nbsp;<?php echo $arr['QuestionContent'];?> ?
			</td>
			
			<td align="right">	
				<b>Marks:</b>&nbsp;<?php echo $arr['QuestionsMarks'];?>
			</td>
		</tr>	


		<?php
		if(isset($arr['Media']['Records']) && !empty($arr['Media']['Records']))
		{
			$url = $arr['Media']['Records'][0]['MediaURL'];
		?>
			<tr>
			<td colspan="2">
				<img style="height: 100px;width: 150px;    margin: 10px 10px 10px 80px; cursor: pointer;" src="<?php echo $url;?>">
			</td>
			</tr>
		<?php
		}
		?>

				
		<tr>
		<td colspan="2"><b>Answer:</b><br/>
			<?php
			if($arr['QuestionsType'] == "Multiple Choice")
			{
			?>	
				<table border="0" width="100%">				
				<?php
				foreach($arr['answer'] as $anskey => $ans)
				{
				?>	
					<tr>
					<td><?php echo ($anskey + 1).". ".ucfirst($ans['AnswerContent']);?></td>
					</tr>
				<?php
				}
				?>	
				</table>
			<?php
			}
			elseif($arr['QuestionsType'] == "Logical Answer")
			{
			?>
				<table border="0" width="100%">
				<?php
				foreach($arr['answer'] as $anskey => $ans)
				{
				?>
					<tr>
					<td><?php echo ($anskey + 1).". ".ucfirst($ans['AnswerContent']);?></td>
					</tr>
				<?php
				}
				?>	
				</table>
			<?php
			}
			?>
		</td>
		</tr>

		

		<tr>
			<td colspan="2"><br /></td>
		</tr>	
<?php
	}
}
?>
</table>