<style type="text/css">
	.fc-day-number.fc-other-month{
		opacity: 1.3!important; 
	}
	.fc-day-number.fc-past{
	    opacity: 0.3!important; 
	}
</style>
<div class="panel-body" ng-controller="PageController"><!-- Body -->
  <!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<span class="float-left records d-none d-sm-block">

			<span ng-if="data.dataList.length" class="h5">Total records: {{data.totalRecords}}</span>

		</span>

		<div class="float-right">

			<!-- ng-if="filterData.CategoryTypes.length>1" -->

			<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();">Add Time Scheduling</button>

		</div>

	</div>

	<!-- Top container/ -->
	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->

	<!-- add Modal -->

	<div class="modal fade" id="add_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>
	<!-- add Modal -->

	<div class="modal fade" id="edit_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

<!-- permission form -->

	<div class="modal fade" id="permission_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Permission <?php echo $this->ModuleData['ModuleName'];?></h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<form id="permission_form" name="permission_form" autocomplete="off" ng-include="templateURLPermission" method="POST">
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

</div><!-- Body/ -->