<style type="text/css">
.content {
/* width: 240px;*/
overflow: hidden;
word-wrap: break-word;
text-overflow: ellipsis;
line-height: 18px;
text-align: center;
}
.less {
max-height: 54px;
}
.menu-text {
/*display: block !important;*/
}
.modal { overflow: auto !important; }
.glyphicon {
position: relative;
top: -4px;
display: inline-block;
font-family: 'Glyphicons Halflings';
font-style: normal;
font-weight: normal;
line-height: 1;
-webkit-font-smoothing: antialiased;
font-size: 20px;
padding: 4px;
}
</style>
<header class="panel-heading">
<h1 class="h4">Manage Scholarship Test</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getScholarshipApplicants(0);">
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<h5>Total Applicants: {{data.totalRecords}}</h5>
		</div>
	</div>
</div>

<form class="" id="filterForm" role="form" autocomplete="off">
<div class="row">
	<div class="col-md-3">
		<div class="form-group">
			<input name="filterKeyword" id="filterKeyword" type="text" class="form-control" placeholder="Search by word">
		</div>
	</div>		


	<div class="col-md-3">
		<div class="form-group">
			<input name="filterFromDate" id="filterFromDate" type="text" class="form-control" value="" maxlength="50" placeholder="Apply From Date" readonly>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			<input name="filterToDate" id="filterToDate" type="text" class="form-control" value="" maxlength="50" placeholder="Apply To Date" readonly>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group">
			<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records('app');"><?php echo SEARCH_BTN;?></button>&nbsp;
		<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records('app');"><?php echo CLEAR_SEARCH_BTN;?></button>
		</div>
	</div>
</div>
</form>

<br/>

<div class="table-responsive block_pad_md"> 

<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

<table class="table table-striped table-hover">

<thead>
<tr>
	<th style="width: 150px;">Test Name</th>	

	<!-- <th style="width: 100px;">Schedule Date</th> -->	

	<th style="width: 100px;">Duration</th>

	<th style="width: 100px;">Applicant Name</th>

	<th style="width: 120px;">Contact Info</th>					

	<th style="width: 150px;">Apply Date</th>	
</tr>
</thead>

<tbody ng-repeat="(key, row) in data.dataList">				
<tr scope="row">
	<td >{{row.QtPaperTitle}}</td>
	
	<!-- <td >{{row.ScheduleDate}}</td> -->
	
	<td >{{row.Duration}}</td>	
	
	<td >{{row.FirstName}} {{row.LastName}}</td>
	
	<td >{{row.Email}} / {{row.PhoneNumber}}</td>

	<td >{{row.PaymentDate}}</td>	
</tr>
</tbody>
</table>


<p class="no-records text-center" ng-if="!data.dataList.length">
<span ng-if="data.dataList.length">No more records found.</span>
<span ng-if="!data.dataList.length">No records found.</span>
</p>
</div>


</div><!-- Body/ -->