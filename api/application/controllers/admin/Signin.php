<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Signin extends API_Controller

{

	function __construct()

	{

		parent::__construct();

		$this->load->model('Admin_model');

		$this->load->model('Common_model');

		$this->load->model('Notification_model');

	}



	/*

	Description: 	Verify login and activate session

	URL: 			/api_admin/signin/

	*/

	public function index_post()

	{

		/* Validation section */

		$this->form_validation->set_rules('Username', 'Username', 'trim'.(empty($this->Post['Source']) || $this->Post['Source']=='Direct' ? '|required' : ''));

		$this->form_validation->set_rules('Password', 'Password', 'trim|required');

		$this->form_validation->set_rules('Source', 'Source', 'trim|required|callback_validateSource');	

		$this->form_validation->set_rules('DeviceGUID', 'DeviceGUID', 'trim');	

		$this->form_validation->set_rules('DeviceType', 'Device type', 'trim|required|callback_validateDeviceType');

		$this->form_validation->set_rules('IPAddress', 'IPAddress', 'trim|callback_validateIP');

		
		$this->form_validation->set_rules('LoginFrom', 'LoginFrom', 'trim');	

		if(empty($this->input->post('LoginAfter'))){
			// $this->form_validation->set_rules('g-recaptcha-response','Captcha verification','trim|required');
		}

				
		//$this->form_validation->set_message('g-recaptcha-response', "You can't leave Captcha Code empty");
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */
		//print_r($this->input->post('g-recaptcha-response'));

		if(!empty($this->input->post('g-recaptcha-response')) && empty($this->input->post('LoginAfter')))
      	{
      		$secret = '6LcPqPkUAAAAAC4tfmvr0Rk8OWJSZUexetFbpqOL';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$this->input->post('g-recaptcha-response'));
             $responseData = json_decode($verifyResponse);
             if($responseData){

             }else{
             	$this->Return['ResponseCode']   = 500;  

          		 $this->Return['Message']        = 'Robot verification failed, please try again.';

          		 die();
             }
      	}

		$UserData=$this->Users_model->getUsers('UserTypeName,UserTypeID,UserID,IsAdmin,FirstName,LastName,Email,StatusID,ProfilePic,InstituteProfilePic,AdminAccess',

			array('LoginKeyword'=>@$this->Post['Username'], 'Password'=>$this->Post['Password'], 'SourceID'=>$this->SourceID));	

      	/*if(in_array($UserData['UserID'], array(252, 5901, 4783, 2843) ))
      	{
      		$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Your Trial version is expired please contact support.";

			die;
      	}*/

		if(!$UserData){

			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Invalid login credentials.";

		}elseif($UserData && $UserData['StatusID']==1){

			$this->Return['ResponseCode'] 	=	501;	

			$Username = $this->Post['Username'];
			$Password = $this->Post['Password'];			

			$str = "Please <a href='javascript:void(0);' onclick=resentActivationLink('$Username','$Password');> click here</a> to re-sent activation link. <p id='amloader' ng-if='ShowDataLoading' class='text-center data-loader' style='display:none;'><img src='./asset/img/loader.svg'></p>";

			$this->Return['Message']      	=	"You have not activated your account yet, please verify your email address first. ".$str;

			

		}elseif($UserData && $UserData['StatusID']==3){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been deleted. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==4){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Your account has been blocked. Please contact the Admin for more info.";

		}elseif($UserData && $UserData['StatusID']==5){

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"You have deactivated your account, please contact the Admin to reactivate.";

		}elseif($UserData && $UserData['AdminAccess']=='No') {

			$this->Return['ResponseCode'] 	=	500;	

			$this->Return['Message']      	=	"Access restricted.";
		}
		
		// elseif($UserData && $UserData['IsAdmin']=='No'){

		// 	$this->Return['ResponseCode'] 	=	500;	

		// 	$this->Return['Message']      	=	"Access restricted.";

		// }
		else{

			/*check for store owner in case of multiple store app*/

			$UserData['StoreGUID'] = '0';

			if(MULTISTORE){

				$this->load->model('Store_model');

				$StoreData = $this->Store_model->getUserStores($UserData['UserID']);

				if($StoreData){

					$UserData['StoreGUID'] = $StoreData['StoreGUID'];

				}

			}


			if(!empty($this->input->post('LoginFrom')) && ($UserData['UserTypeID'] == 10 || $UserData['UserTypeID'] == 87)){
				$this->Return['ResponseCode'] 	=	700;	

				$this->Return['Message']      	=	"Sorry! You don't have permission to login here.";

				return FALSE;
			}

			//echo $UserData['UserTypeID']

			if($UserData['UserTypeID'] == 14){
				$checkSession = $this->Users_model->checkSession("",$UserData['UserID']);
				if($checkSession){
					$this->Return['ResponseCode'] 	=	500;	

					$this->Return['Message']      	=	"User is already logged in.";

					return FALSE;
				}
			}


			/*Create Session*/

			$UserData['SessionKey']	= $this->Users_model->createSession($UserData['UserID'], array(

				//"IPAddress"	=>	@$this->Post['IPAddress'],

				"SourceID"		=>	$this->SourceID,

				"DeviceTypeID"	=>	$this->DeviceTypeID,

				//"DeviceGUID"	=>	(empty($this->Post['DeviceGUID']) ? '' : $this->Post['DeviceGUID']),

				//"DeviceToken"	=>	@$this->Post['DeviceToken'],

				//"Latitude"	=>	@$this->Post['Latitude'],

				//"Longitude"	=>	@$this->Post['Longitude']

			));

			//$InstituteData = $this->Users_model->getUsers('FirstName,LastName,Email',array("UserID" => $UserData['InstituteID']));

			$UserData['InstituteGUID'] = $this->Users_model->createbyinstitutrID($UserData['UserID']);

			$UserData['MasterFranchisee'] = $this->Users_model->getMasterFranchisee($UserData['UserID']);

			$UserData['FranchiseeAssignCount'] = $this->Users_model->getFranchiseeAssignCount($UserData['UserID']);


			$UserData['Notification'] = $this->Notification_model->getNotifications($UserData['UserID']);

			$UserData['Notification'] = $this->Notification_model->getNotifications($UserData['UserID']);
			
			
			/*Get Permitted Modules*/

			$UserData['PermittedModules']		= 	$this->Admin_model->getPermittedModules($UserData['UserTypeID'], $UserData['UserID']);

			$UserData['Menu']					= 	$this->Admin_model->getMenu($UserData['UserTypeID']);

			$this->Return['Data']      			=	$UserData;

		}

	}



	public function resentactivation_post()
	{
		$this->form_validation->set_rules('Username', 'Username', 'trim|required');

		$this->form_validation->set_rules('Password', 'Password', 'trim|required');	
		
		$this->form_validation->validation($this);  /* Run validation */		

		$UserData = $this->Users_model->checkUserExist($this->Post);	

      	//echo "<pre>"; print_r($UserData); die;

		if(isset($UserData) && !empty($UserData))			
		{	
			if(isset($UserData["Basic"]) && !empty($UserData["Basic"]) && isset($UserData["Token"]) && !empty($UserData["Token"]))
			{
				$FirstName = $UserData["Basic"]['FirstName'];
				$Email = $UserData["Basic"]['Email'];
				$Pass_Token = $UserData["Token"][1];
				$Email_Token = $UserData["Token"][2];				
				
				$content = $this->load->view('emailer/marketplace/registration',array("Name" => $FirstName, 'Token' => @$Email_Token, 'EmailText' => "","Tokens"=>@$Pass_Token),TRUE);
				sendMail(array(
					'emailTo' 		=> $Email,			
					'emailSubject'	=> "Activation link from Iconik.",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));

				$this->Return['ResponseCode'] 	=	200;
				$this->Return['Message']      	=	"Activation link has been sent successfully on email. Please check your inbox.";
			}
			else
			{
				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"Details not found in the system.";
			}	
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Details not found in the system.";
		}
	}



	public function appversion_post()
	{
		$cversion = 7;

		$this->form_validation->set_rules('version', 'Version', 'trim|required');
		
		$this->form_validation->validation($this);

		if($cversion == $this->Post['version'])			
		{	
			$this->Return['ResponseCode'] 	=	200;
			
			$this->Return['Message']      	=	"";				
		}
		else
		{
			$this->Return['ResponseCode'] 	=	500;

			$this->Return['Message']      	=	"Please update you version.";
		}
	}


}