<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MAIN_Controller
{
	public function index()
	{
		echo "This is a sample page.";
	}

	public function logs()
	{
		$this->load->library('logviewer');
		$this->load->view('logs', $this->logviewer->get_logs());
	}

	public function upload()
	{
		$this->load->view('upload');
	}


	
}
