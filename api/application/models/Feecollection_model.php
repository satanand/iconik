<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feecollection_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    function getStudentFeeDetail($EntityID,$UserID)
    {
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $this->db->select('tbl_students.CourseID,tbl_students.BatchID,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.FeeStatusID,tbl_students.TotalFeeAmount,tbl_students.TotalFeeInstallments,tbl_students.InstallmentAmount');
        $this->db->from("tbl_students");
        $this->db->where("StudentID",$UserID);
        $Query =  $this->db->get(); //echo $this->db->last_query(); die;
        //$StudentData = $Query->result_array(); //print_r($StudentData);
        $Records = array();
        $appdd = "";
        if($Query->num_rows() > 0){
            foreach ($Query->result_array() as $record) 
            {   
                $appdd = "";

                if(!empty($record['CourseID']))
                {
                    $query = $this->db->query("select CategoryGUID,CategoryName from set_categories where CategoryID = ".$record['CourseID']);
                    $CategoryData = $query->result_array();
                    $record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
                    $record['CourseName'] = $CategoryData[0]['CategoryName'];

                    $word = $record['CourseID'];
                    $appdd .= " AND CourseID = '$word'";
                }


                if(!empty($record['BatchID']))
                {
                    $query = $this->db->query("select BatchName from tbl_batch where BatchID = ".$record['BatchID']);
                    $BatchName = $query->result_array();
                    $record['BatchName'] = $BatchName[0]['BatchName'];

                    $word = $record['BatchID'];
                    $appdd .= " AND BatchID = '$word'";
                }


                if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
                    $record['Fee Status'] = "Unpaid";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Fee Status'] = "Paid";
                }else{
                    $record['Fee Status'] = "Unpaid";
                }



                    
                $record['TotalFee'] = 0;                
                $record['NoOfInstallment'] = 0;                                   
                if(!empty($record['FeeID']))
                {
                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();
                    $record['TotalFee'] = $FeeData[0]['FullDiscountFee'];
                    $record['NoOfInstallment'] = 1;
                    $record['InstallmentAmount'] = $record['TotalFee'];                     
                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {
                        $record['TotalFee'] = $InstallmentData[0]['TotalFee'];
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                        $record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['InstallmentAmount'] = 0;
                }


                $record['PaidEMICount'] = 0;
                $record['RemainingEMI'] = 0;
                $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$UserID);
                $SumData = $query->result_array();  
                if(!empty($SumData) && $SumData[0]['FeeCollectionID'] != 0)
                {
                    $record['PaidEMICount'] = $SumData[0]['FeeCollectionID'];
                    //$record['RemainingEMI'] = $record['PaidEMICount'] - count($SumData);                    
                }
                $record['RemainingEMI'] = $record['NoOfInstallment'] - $record['PaidEMICount'];
                    

                

                $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$UserID);
                $SumData = $query->result_array(); 
                if(!empty($SumData) && $SumData[0]['PaidAmount'] > 0){
                    $record['PaidAmount'] = $SumData[0]['PaidAmount'];
                }else{
                    $record['PaidAmount'] = 0;
                } 
                $record['RemainingAmount'] = (int)$record['TotalFee'] - (int)$record['PaidAmount'];
                

                //Due Dates-------------------------------------------
                $query = $this->db->query("SELECT DueDateID, date_format(DueDate, '%d-%m-%Y') as DueDate, date_format(ReceivedDate, '%d-%m-%Y') as ReceivedDate, IsPaid 
                    FROM tbl_students_feeduedate 
                    WHERE StudentID = ".$UserID." AND InstituteID = '".$InstituteID."' $appdd " );
                $DueDates = $query->result_array();
                $record['DueDates'] = array();
                if(isset($DueDates) && !empty($DueDates))
                {
                    $record['DueDates'] = $DueDates;
                }



                //echo $record['RemainingAmount']; die;  

                //if(!empty($record['InstallmentID'])){
                    //echo "select TotalFee,InstallmentAmount,NoOfInstallment from tbl_setting_installment where InstallmentID = ".$record['InstallmentID'];
                    // $query = $this->db->query("select Amount,RemainingAmount,TotalFee from tbl_fee_collection where StudentID = '".$UserID."' order by FeeCollectionID DESC limit 1");
                    // $InstallmentData = $query->result_array(); 
                    // //print_r($InstallmentData); die;
                    // if(!empty($InstallmentData)){
                    //     if(!empty($InstallmentData[0]['RemainingAmount'])){
                    //         $record['RemainingAmount'] = $InstallmentData[0]['RemainingAmount'];
                    //     }else{
                    //         $record['RemainingAmount'] = (int)$record['TotalFeeAmount'] - (int)$record['PaidAmount'];
                    //     }                        
                    // }else if(!empty($record['TotalFeeAmount']) && $record['TotalFeeAmount'] != 0){
                    //     $record['RemainingAmount'] = (int)$record['TotalFeeAmount'] - (int)$record['PaidAmount'];
                    // } else if(!empty($record['FullDiscountFee'])){
                    //     $record['RemainingAmount'] = (int)$record['FullDiscountFee'] - (int)$record['PaidAmount'];
                    // }              
                //}

                

                //print_r($record);
                array_push($Records, $record);
            }
            //print_r($Records);
            return $Records;
        }else{
            return $Records;
        }
    }


    
    
    /*
    Description: 	Use to add new collection
    */
    function addCollection($EntityID, $UserID, $Input)
    {
        $Denomination = "";
        if($Input['PaymentMode'] == "Cash")
        {
            $DenominationArr = array();
            $arr = array(2000, 500, 200, 100, 50, 20, 10, 5, 2, 1);
            foreach($arr as $Note)
            {
                if(isset($Input['Notes'][$Note]) && !empty($Input['Notes'][$Note]))
                {
                    $DenominationArr[] = array("NoteType" => $Note, 
                        "Count" => $Input['Notes'][$Note],
                        "Amount" => ($Note * $Input['Notes'][$Note])
                    );
                }
            }            
            $Denomination = json_encode($DenominationArr);
        }       


        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);       
        if(!empty($Input['MediaGUIDs'])){
            $this->db->select('MediaID');
            $this->db->from('tbl_media');
            $this->db->where('MediaGUID',$Input['MediaGUIDs']);
            $this->db->limit(1);
            $que = $this->db->get();
            //echo $this->db->last_query(); die;
            if($que->num_rows() > 0){
                $mediaID = $que->result_array();
                $MediaID = $mediaID[0]['MediaID'];

                $arr = array("InstituteID"=>$InstituteID);

                $this->db->where("MediaID",$MediaID);
                $this->db->update("tbl_media",$arr);
            }else{
                $MediaID = "";
            } 
        }else{
            $MediaID = "";
        } 

        $Input['RemainingAmount'] = (int)$Input['RemainingAmount'] - (int)$Input['Amount'];  


        
        /* Add Batch */
        $InsertData = array_filter(array(
            "UserID" => $EntityID,
            "StudentID" => $UserID,
            "BatchID" => @$Input['BatchID'],
            "CourseID" => @$Input['CourseID'],
            "PaymentMode" => @$Input['PaymentMode'],
            "Amount" => @$Input['Amount'],
            "PaymentDate" => @date("Y-m-d", strtotime($Input['PaymentDate'])),
            "Denomination" => $Denomination,
            "ChequeNumber" => @$Input['ChequeNumber'],
            "ChequeDate" => @date("Y-m-d", strtotime($Input['ChequeDate'])),
            "BankName" => @$Input['BankName'], 
            "MediaID" => @$MediaID, 
            "PaymentType" => @$Input['PaymentType'],
            "TransactionID" => @$Input['TransactionID'],
            "TotalFee" => @$Input['TotalFee'],
            "RemainingAmount" => @$Input['RemainingAmount'],
            "EntryDate" => date("Y-m-d h:i:s"),
            "ModifyDate" => date("Y-m-d h:i:s"),
            "InstituteID" => $InstituteID
        ));

        
        $this->db->insert('tbl_fee_collection', $InsertData);
        
        if($Input['Amount'] == $Input['RemainingAmount'] || $Input['Amount'] > $Input['RemainingAmount']){
            $this->db->where('StudentID', $UserID);
            $this->db->update('tbl_students',array("FeeStatusID"=>2));
        }

        if(isset($Input['duedates']) && !empty($Input['duedates']))
        {                       
            foreach($Input['duedates'] as $did)
            {
                $UpdateData = array(
                "IsPaid"        =>  1,                
                "ReceivedDate"  =>  @date("Y-m-d", strtotime($Input['PaymentDate'])),
                "UpdateDate" => date("Y-m-d H:i:s")                 
                );

                $this->db->where('DueDateID', $did);
                $this->db->where('StudentID', $UserID);
                $this->db->where('InstituteID', $InstituteID);
                $this->db->update('tbl_students_feeduedate', $UpdateData);
            }   
        }

        return TRUE;         
    }



    /*
    Description:    Use to edit new collection
    */
    function editCollection($EntityID, $UserID, $Input)
    {
        $Denomination = "";
        if($Input['PaymentMode'] == "Cash")
        {
            $DenominationArr = array();
            $arr = array(2000, 500, 200, 100, 50, 20, 10, 5, 2, 1);
            foreach($arr as $Note)
            {
                if(isset($Input['Notes'][$Note]) && !empty($Input['Notes'][$Note]))
                {
                    $DenominationArr[] = array("NoteType" => $Note, 
                        "Count" => $Input['Notes'][$Note],
                        "Amount" => ($Note * $Input['Notes'][$Note])
                    );
                }
            }            
            $Denomination = json_encode($DenominationArr);
        }


        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);       
        if(!empty($Input['MediaGUIDs'])){
            $this->db->select('MediaID');
            $this->db->from('tbl_media');
            $this->db->where('MediaGUID',$Input['MediaGUIDs']);
            $this->db->limit(1);
            $que = $this->db->get();
            //echo $this->db->last_query(); die;
            if($que->num_rows() > 0){
                $mediaID = $que->result_array();
                $MediaID = $mediaID[0]['MediaID'];

                $arr = array("InstituteID"=>$InstituteID);

                $this->db->where("MediaID",$MediaID);
                $this->db->update("tbl_media",$arr);
            }else{
                $MediaID = "";
            } 
        }else{
            $MediaID = "";
        }

        if($Input['PaymentMode'] == "Cash"){
            $Input['ChequeNumber'] = "";
            $Input['ChequeDate'] = "";
            $Input['BankName'] = "";
            $Input['TransactionID'] = "";
            $Input['MediaID'] = "";
            $Input['PaymentType'] = "";
        }else if($Input['PaymentMode'] == "Online"){
            $Input['ChequeNumber'] = "";
            $Input['ChequeDate'] = "";
            $Input['BankName'] = "";
            $Input['Denomination'] = "";
            $Input['MediaID'] = "";
        }else if($Input['PaymentMode'] == "Cheque"){
            $Input['PaymentType'] = "";
            $Input['TransactionID'] = "";
            $Input['Denomination'] = "";           
        }  

        //$Input['RemainingAmount'] = (int)$Input['RemainingAmount'] - (int)$Input['Amount'];

        /* Add Batch */
        $InsertData = array_filter(array(
            "UserID" => $EntityID,            
            "PaymentMode" => @$Input['PaymentMode'],
            "Amount" => @$Input['Amount'],
            "PaymentDate" => @date("Y-m-d", strtotime($Input['PaymentDate'])),
            "Denomination" => $Denomination,
            "ChequeNumber" => @$Input['ChequeNumber'],
            "ChequeDate" => @date("Y-m-d", strtotime($Input['ChequeDate'])),
            "BankName" => @$Input['BankName'], 
            "MediaID" => @$MediaID, 
            "PaymentType" => @$Input['PaymentType'],
            "TransactionID" => @$Input['TransactionID'],
            "TotalFee" => @$Input['TotalFee'],
            "RemainingAmount" => @$Input['RemainingAmount'],            
            "ModifyDate" => date("Y-m-d h:i:s"),
            "InstituteID" => $InstituteID
        ));
        $this->db->where("FeeCollectionID",$Input['FeeCollectionID']);
        $this->db->update('tbl_fee_collection', $InsertData); 

        
        if(isset($Input['duedates']) && !empty($Input['duedates']))
        {
            $UpdateData = array(
            "IsPaid"        =>  0,                
            "ReceivedDate"  =>  NULL,
            "UpdateDate" => date("Y-m-d H:i:s")                 
            );            
            $this->db->where('StudentID', $UserID);
            $this->db->where('InstituteID', $InstituteID);
            $this->db->update('tbl_students_feeduedate', $UpdateData);

            foreach($Input['duedates'] as $did)
            {
                $UpdateData = array(
                "IsPaid"        =>  1,                
                "ReceivedDate"  =>  @date("Y-m-d", strtotime($Input['PaymentDate'])),
                "UpdateDate" => date("Y-m-d H:i:s")                 
                );

                $this->db->where('DueDateID', $did);
                $this->db->where('StudentID', $UserID);
                $this->db->where('InstituteID', $InstituteID);
                $this->db->update('tbl_students_feeduedate', $UpdateData);
            }   
        }

        return TRUE;         
    }

   
    
    
    
    /*
    Description: 	Use to update Batch.
    */
    function editBatch($data)
    {
        
        $this->UserID   = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
        
        $InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
        
        $this->db->select('B.BatchID');
        $this->db->from('tbl_batch B');
        $this->db->join('tbl_entity E', 'E.EntityGUID = B.BatchGUID');
        $this->db->where('LOWER(`BatchName`)="' . strtolower($data['BatchName']) . '"');
        $this->db->where('E.InstituteID', $InstituteID);
        $this->db->where('B.BatchGUID !=', $data['BatchGUID']);
        
        $query = $this->db->get();
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            
            return 'EXIST';
            
        } else {
            
            $originalDate = $data['StartDate'];
            $newDate      = date("Y-m-d", strtotime($originalDate));

            $Durationdata = $this->Common_model->getDuration("",$data['CourseID']);

            //print_r($Durationdata[0]['Duration']);
            $Durationdata = $Durationdata[0]['Duration'];

            if(intval($Durationdata) == intval($data['Duration'])){
                 $DuratioExtendMonth = 0;
            }else if(intval($Durationdata) > intval($data['Duration'])){
                $DuratioExtendMonth = 0; 
            }else if(intval($Durationdata) < intval($data['Duration'])){
                $DuratioExtendMonth = intval($data['Duration']) - intval($data['LastDuration']);
            }

            // if($DuratioExtendMonth == 0 && $data['BatchName']){
                
            // }
            
            
            $UpdateArray = array(
                "BatchName" => $data['BatchName'],
                "Duration" => $data['Duration'],
                "StartDate" => $newDate,
                "CourseID" => $data['CourseID'],
                "CourseDuration" =>  $Durationdata,
                "DuratioExtendMonth" => $DuratioExtendMonth
            );
            
            if (!empty($UpdateArray)) {
                $this->db->where('BatchGUID', $data['BatchGUID']);
                $this->db->limit(1);
                $data = $this->db->update('tbl_batch', $UpdateArray);
               // echo $this->db->last_query(); die;
            }
        }
        
        return TRUE;
    }



    /*
    Description: 	Use to update editAssignData.
    */
    function editAssignData($data = array())
    {
        
        $UpdateArray = array_filter(array(
            
            'BatchID' => $data['BatchID'],
            'FacultyID' => $data['FacultyID']
            
        ));
        
        if (!empty($UpdateArray)) {
            $this->db->where('AsID', $data['AsID']);
            $this->db->limit(1);
            $data = $this->db->update('tbl_batchbyfaculty', $UpdateArray);
        }
        
        
        return TRUE;
    }
     
    
    /* get fee collection*/
    function getFeeCollections($EntityID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15)
    {    
       // print_r($Where); 
       //date_column BETWEEN >= '2014-01-10' AND '2015-01-01';   
        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
        $this->db->select('tbl_students.CourseID,tbl_students.BatchID,tbl_students.FeeID,tbl_students.InstallmentID,tbl_students.FeeStatusID,tbl_students.TotalFeeAmount,tbl_students.TotalFeeInstallments,tbl_students.InstallmentAmount,tbl_fee_collection.*,tbl_users.UserGUID,tbl_users.FirstName,tbl_users.LastName,CONCAT(U.FirstName, " ", IF(U.LastName IS NOT NULL, U.LastName,"")) as fullname');
        $this->db->from("tbl_fee_collection");
        $this->db->join("tbl_students","tbl_fee_collection.StudentID = tbl_students.StudentID",'left');
        $this->db->join("tbl_users","tbl_users.UserID = tbl_students.StudentID",'left');
        $this->db->join("tbl_entity","tbl_students.StudentID = tbl_entity.EntityID");
        $this->db->join("tbl_users as U","U.UserID = tbl_fee_collection.UserID",'left');
        if(!empty($Where['Keyword'])){ /*search in post content*/
            $this->db->group_start();
            // $this->db->where('MATCH (P.QueryContent) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
            // $this->db->or_where('MATCH (P.QueryCaption) AGAINST ("'.$Where['Keyword'].'")', NULL, FALSE);
            $this->db->like("tbl_fee_collection.Amount", $Where['Keyword']);
            $this->db->or_like("tbl_fee_collection.PaymentDate", $Where['Keyword']);
            $this->db->or_like("tbl_fee_collection.PaymentMode", $Where['Keyword']);
            $this->db->or_like("tbl_fee_collection.TransactionID", $Where['Keyword']);
            $this->db->or_like("tbl_users.FirstName", $Where['Keyword']);
            $this->db->or_like("tbl_users.LastName", $Where['Keyword']);
            $this->db->or_like("U.FirstName", $Where['Keyword']);
            $this->db->or_like("U.LastName", $Where['Keyword']);
            $this->db->or_like("EntryDate", $Where['Keyword']);
            $this->db->or_like("CONCAT_WS('',tbl_users.FirstName,tbl_users.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("CONCAT_WS(' ',tbl_users.FirstName,tbl_users.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("CONCAT_WS('',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->or_like("CONCAT_WS(' ',U.FirstName,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
            $this->db->group_end();
        }

        $this->db->where("tbl_entity.InstituteID",$InstituteID);

        if(!empty($Where['CategoryID'])){
            $this->db->where("tbl_students.CourseID",$Where['CategoryID']);
        }
        if(!empty($Where['BatchID'])){
            $this->db->where("tbl_students.BatchID",$Where['BatchID']);
        }
        if(!empty($Where['FeeCollectionID'])){
            $this->db->where("FeeCollectionID",$Where['FeeCollectionID']);
        }
        if(!empty($Where['startDate']) && !empty($Where['endDate'])){
            $Where['startDate'] = date("Y-m-d",strtotime($Where['startDate']));
            $Where['endDate'] = date("Y-m-d",strtotime($Where['endDate']));
            $this->db->where("PaymentDate BETWEEN '".$Where['startDate']."' AND '".$Where['endDate']."'");
        }else if(!empty($Where['startDate'])){
            $Where['startDate'] = date("Y-m-d",strtotime($Where['startDate']));
            $this->db->where("PaymentDate",$Where['startDate']);
        }else if(!empty($Where['endDate'])){
            $Where['endDate'] = date("Y-m-d",strtotime($Where['endDate']));
            $this->db->where("PaymentDate",$Where['endDate']);
        }
        $this->db->order_by("FeeCollectionID","DESC");
        if($multiRecords){ 
            $TempOBJ = clone $this->db;
            $TempQ = $TempOBJ->get();
            $Return['Data']['TotalRecords'] = $TempQ->num_rows();
            $this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
        }else{
            $this->db->limit(1);
        }
        $Query =  $this->db->get(); //echo $this->db->last_query(); die;
        //$StudentData = $Query->result_array(); //print_r($StudentData);
        $Records = array(); $balance = 0; $appdd = "";
        if($Query->num_rows() > 0){
            foreach ($Query->result_array() as $record) 
            {
                $appdd = "";

                $record['PaymentDate'] = date("d-m-Y",strtotime($record['PaymentDate']));
                $record['ChequeDate'] = date("d-m-Y",strtotime($record['ChequeDate']));

                if(!empty($record['CourseID'])){
                    $query = $this->db->query("select CategoryGUID,CategoryName from set_categories where CategoryID = ".$record['CourseID']);
                    $CategoryData = $query->result_array();
                    $record['CategoryGUID'] = $CategoryData[0]['CategoryGUID'];
                    $record['CourseName'] = $CategoryData[0]['CategoryName'];

                    $word = $record['CourseID'];
                    $appdd .= " AND CourseID = '$word' ";
                }


                if(!empty($record['BatchID'])){
                    $query = $this->db->query("select BatchName from tbl_batch where BatchID = ".$record['BatchID']);
                    $BatchName = $query->result_array();
                    $record['BatchName'] = $BatchName[0]['BatchName'];

                    $word = $record['BatchID'];
                    $appdd .= " AND BatchID = '$word' ";
                }

                if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 1){
                    $record['Fee Status'] = "Unpaid";
                }else if(!empty($record['FeeStatusID']) && $record['FeeStatusID'] == 2){
                    $record['Fee Status'] = "Paid";
                }else{
                    $record['Fee Status'] = "Unpaid";
                }    


                $record['TotalFee'] = 0;                
                $record['NoOfInstallment'] = 0;                                   
                if(!empty($record['FeeID']))
                {
                    $query = $this->db->query("select FullDiscountFee,  FullAmount from tbl_setting_fee where FeeID = ".$record['FeeID']);
                    $FeeData = $query->result_array();
                    $record['TotalFee'] = $FeeData[0]['FullDiscountFee'];
                    $record['NoOfInstallment'] = 1;
                    $record['InstallmentAmount'] = $record['TotalFee'];                     
                }
                elseif(!empty($record['InstallmentID']))
                {
                    $sql = "select TotalFee, InstallmentAmount, NoOfInstallment
                    from tbl_setting_installment
                    where InstallmentID = ".$record['InstallmentID'];
                   
                    $query = $this->db->query($sql);
                    $InstallmentData = $query->result_array();  
                    if(!empty($InstallmentData))
                    {
                        $record['TotalFee'] = $InstallmentData[0]['TotalFee'];
                        $record['InstallmentAmount'] = $InstallmentData[0]['InstallmentAmount'];
                        $record['NoOfInstallment'] = $InstallmentData[0]['NoOfInstallment'];
                    }               
                }
                elseif(isset($record['TotalFeeAmount']))
                {
                    $record['TotalFee'] = $record['TotalFeeAmount'];
                    $record['InstallmentAmount'] = $record['InstallmentAmount'];
                    $record['NoOfInstallment'] = $record['TotalFeeInstallments'];
                }
                else
                {
                    $record['InstallmentAmount'] = 0;
                }
                


                $record['PaidEMICount'] = 0;
                $record['RemainingEMI'] = 0;
                if(!empty($Where['FeeCollectionID'])){
                     $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$record['StudentID']." and FeeCollectionID <= ".$Where['FeeCollectionID']);
                }else{
                     $query = $this->db->query("select count(FeeCollectionID) as FeeCollectionID from tbl_fee_collection where StudentID = ".$record['StudentID']);
                }
                $SumData = $query->result_array();  
                if(!empty($SumData))
                {
                    $record['PaidEMICount'] = $SumData[0]['FeeCollectionID'];                    
                }
                $record['RemainingEMI'] = $record['NoOfInstallment'] - $record['PaidEMICount'];  



                if(!empty($Where['FeeCollectionID'])){
                     $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$record['StudentID']." and FeeCollectionID <= ".$Where['FeeCollectionID']);
                }else{
                     $query = $this->db->query("select Sum(Amount) as PaidAmount from tbl_fee_collection where StudentID = ".$record['StudentID']);
                }                
                $SumData = $query->result_array();  
                if(!empty($SumData)){
                    $record['PaidAmount'] = $SumData[0]['PaidAmount'];
                }else{
                    $record['PaidAmount'] = 0;
                } 

                $record['RemainingAmount'] = (int)$record['TotalFee'] - (int)$record['PaidAmount'];                  
                $record['Denomination'] = json_decode($record['Denomination']);

                

                if(!empty($record['MediaID']) && $record['MediaID'] != NULL && $record['PaymentMode'] == "Cheque")
                {
                    $this->load->model('Media_model');
                    $MediaData = $this->Media_model->getMedia('',array("SectionID" => 'OfflinePayment',"MediaID" => $record['MediaID']),TRUE);
                    $MediaID = ($MediaData ? $MediaData['Data'] : new stdClass());
                    $record['MediaThumbURL'] = $MediaID['Records'][0]['MediaThumbURL'];
                    $record['MediaURL'] = $MediaID['Records'][0]['MediaURL'];                    
                }else{
                    $record['MediaThumbURL'] = "";
                    $record['MediaURL'] = "";
                }



                //Due Dates-------------------------------------------
                $query = $this->db->query("SELECT DueDateID, date_format(DueDate, '%d-%m-%Y') as DueDate, date_format(ReceivedDate, '%d-%m-%Y') as ReceivedDate, IsPaid 
                    FROM tbl_students_feeduedate 
                    WHERE StudentID = ".$record['StudentID']." AND InstituteID = '".$InstituteID."' $appdd " );
                $DueDates = $query->result_array();
                $record['DueDates'] = array();
                if(isset($DueDates) && !empty($DueDates))
                {
                    $record['DueDates'] = $DueDates;
                }


                // if(!empty($Where['FeeCollectionID'])){
                //     $query = $this->db->query("select Amount,RemainingAmount,TotalFee from tbl_fee_collection where StudentID = '".$record['StudentID']."' and FeeCollectionID <= ".$Where['FeeCollectionID']." order by FeeCollectionID DESC limit 1");
                // }else{
                //     $query = $this->db->query("select Amount,RemainingAmount,TotalFee from tbl_fee_collection where StudentID = '".$record['StudentID']."' order by FeeCollectionID DESC limit 1");
                // }
                    
                //     $InstallmentData = $query->result_array();  
                //     if(!empty($InstallmentData)){
                //         if(!empty($InstallmentData[0]['RemainingAmount'])){
                //             $record['RemainingAmount'] = $InstallmentData[0]['RemainingAmount'];
                //         }else{
                //             $record['RemainingAmount'] = (int)$record['TotalFeeAmount'] - (int)$record['PaidAmount'];
                //         }                        
                //     }else{
                //         $record['RemainingAmount'] = $record['TotalFee'] - $record['PaidAmount'];
                //     }               
                //}

                $balance = $record['Amount'] + $balance;
                $record['balance'] = $balance;
                //print_r($record);
                array_push($Records, $record);
            }
            //print_r($Records);
            return $Records;
        }else{
            return $Records;
        }
    }
    
    
    
    function delete($FeecollectionID)
    {
        $this->db->where("FeecollectionID", $FeecollectionID);
        $this->db->delete("tbl_fee_collection");

        //echo $this->db->last_query(); die;
        return TRUE;
    }


    /*
    Description:    Use to get staff  
    */  
    function getStudents($EntityID,$Where=array())
    {

        $InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

        $this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,s.BatchID');
        $this->db->from('tbl_users u');
        $this->db->join('tbl_entity e','e.EntityID=u.UserID','left'); 
        $this->db->join('tbl_students s','s.StudentID=u.UserID','left');          
        $this->db->where('e.InstituteID',$InstituteID);
        $this->db->where('u.UserTypeID',7);
        //$this->db->where('s.FeeStatusID',1);
        // if(!empty($UserTypeID)) {
        //     $this->db->where('u.UserTypeID',$UserTypeID);
        // }

        if(!empty($Where['BatchID'])) {
            $this->db->where('s.BatchID',$BatchID);
        }

        $this->db->order_by('e.EntityID','DESC');     

        $Query = $this->db->get();  //echo $this->db->last_query();
        $data = $Query->result_array();

        $usertype = array();

        if(count($data) > 0)
        {
            if($EntryDate == "")    $EntryDate = date("Y-m-d");
            else                    $EntryDate = date("Y-m-d", strtotime($EntryDate));
            foreach($data as $Record)
            {  
                $this->db->select("AttendanceStatus");
                $this->db->from("tbl_attendance"); 
                $this->db->where("StudentIDs",$Record['UserID']);
                $this->db->like("EntryDate", $EntryDate);  
                $this->db->limit(1); 
                $AttendanceQuery = $this->db->get(); 
                if($AttendanceQuery->num_rows() > 0){
                    $AttendanceQuery = $AttendanceQuery->result_array();
                    $Record['AttendanceStatus'] = $AttendanceQuery[0]['AttendanceStatus'];
                } 
                             
                $Records[] = $Record;
            }
            return $Records;
        }
        return FALSE;       
    }
}