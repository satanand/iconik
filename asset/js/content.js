app.controller('PageController', function ($scope, $http,$timeout){
    $scope.data.pageSize = 30;
    $scope.categoryDataList = [];
    $scope.filterData = [];
    $scope.CourseData = [];
    $scope.data.dataList = [];
    $scope.formDataCheck = false;
    $scope.hidden = true;
    $scope.PostGUID;
    $scope.selectedOption;
    $scope.API_URL = API_URL;
    $scope.ParentCategoryGUID;
    $scope.ParentCategoryGUIDIndex;
    $scope.subjectTemplateURLAdd;
    $scope.CategoryGUID;
    $scope.SubCategoryGUID = "";
    $scope.data.SecondLevelPermission = [];
    //console.log( $scope.API_URL);
    $scope.Media = [];
    //$scope.data.ParentCategoryGUID = ParentCategoryGUID;
    $scope.APIURL = API_URL + "upload/file";

    $scope.getIframe = function (url) 
    {       
        var a = $scope.createVideo(url,300,300); $(".iframe:last").html(a);
    }

    /*get category*/
    $scope.getCategoryFilterData = function (ParentCategoryGUIDIndex,check="")
    {
       // alert(ParentCategoryGUIDIndex);    
       // $scope.filterData = [];
        var options = "";        
        $scope.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;
        $("#ParentCategoryGUIDIndex").val(ParentCategoryGUIDIndex);
        if(ParentCategoryGUIDIndex){ console.log($scope.categoryDataList);
            if($scope.categoryDataList){
                $scope.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;
                $scope.CourseData.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;
                $scope.CourseData.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;
                $scope.CourseData.ParentCategoryName = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryName;
                console.log($scope.CourseData);
                if(check=='subCategory'){
                    $scope.getSubCategoryList($scope.ParentCategoryGUID);
                    $("#link_add_subject").show();
                }else{
                    //alert($scope.ParentCategoryGUIDIndex);
                    //console.log($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories);
                    if($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories){
                        var SubCategories = $scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records;
                        //console.log(SubCategories);
                        var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" onchange="" onchange="check_subject_available()" required=""><option value="">Select Subject</option>';
                        for (var i in SubCategories) {
                            $scope.filterData.push($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records);
                            options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               
                        } 
                        options += '</select>';
                        //console.log(options);
                        if(options){
                            //alert("ladjls");
                            $("#subcategory_section").html(options);
                            //$("#CategoryGUID").trigger("chosen:updated");
                            $("#link_add_subject").show();
                            if($scope.SubCategoryGUID.length){
                            $timeout(function(){            
                                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                               }, 200);
                            } 
                        }else{
                           // $scope.ParentCategoryGUID = $('#Courses').find(":selected").val();
                            $("#link_add_subject").show();
                            //$("#link_add_subject").trigger( "click" );
                        }
                    }
                }
            }else{
                //$scope.getCategoryFilterData(ParentCategoryGUID);
            }
        }else{
            var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" onchange="" onchange="check_subject_available()" required=""><option value="">Select Subject</option></select>';
            $("#subcategory_section").html(options);
            $("#link_add_subject").show();
            //$("#link_add_subject").trigger( "click" );
        }
    }


    /*get category*/
    $scope.getCategorySubjects = function (ParentCategoryGUIDIndex,apiCall="")
    {
        //alert(ParentCategoryGUIDIndex);    
       // $scope.filterData = [];
        var options = "";        
        $scope.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;
        $("#ParentCategoryGUIDIndex").val(ParentCategoryGUIDIndex);
        if(ParentCategoryGUIDIndex){ //console.log(ParentCategoryGUIDIndex);
            if($scope.categoryDataList){
                $scope.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;                
                $scope.CourseData.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;
                $scope.CourseData.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;
                $scope.CourseData.ParentCategoryName = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryName;
                console.log($scope.CourseData);
                if(apiCall == "getList"){
                    $scope.getList($scope.ParentCategoryGUID);
                    $("#ParentCategoryGUID").val($scope.ParentCategoryGUID);
                    $scope.getSubCategoryList($scope.ParentCategoryGUID);
                }else{
                    if($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories){
                        var SubCategories = $scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records;
                        
                        var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control"  onchange="getDetailsListByCategory(this.value)"><option value="">Select Subject</option>';
                        for (var i in SubCategories) {
                            $scope.filterData.push($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records);
                            options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               
                        } 
                        options += '</select>';
                        //console.log(options);
                        if(options){
                            //alert("ladjls");
                            $("#subcategory_section").html(options);
                            //$("#CategoryGUID").trigger("chosen:updated");
                            $("#link_add_subject").show();
                            if($scope.SubCategoryGUID.length){
                            $timeout(function(){            
                                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                               }, 200);
                            } 
                        }else{
                           // $scope.ParentCategoryGUID = $('#Courses').find(":selected").val();
                            $("#link_add_subject").show();
                            //$("#link_add_subject").trigger( "click" );
                        }
                    }
                }
            }else{
                //$scope.getCategoryFilterData(ParentCategoryGUID);
            }
        }else{
            var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" onchange="" onchange="check_subject_available()" required=""><option value="">Select Subject</option></select>';
            $("#subcategory_section").html(options);
            $("#link_add_subject").show();
            //$("#link_add_subject").trigger( "click" );
        }
    }


    /*get Stream List*/
    $scope.getCategoryList = function (categoryGUID="")
    {
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+categoryGUID;
        $scope.categoryDataList = [];
        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) {
                    for(var j in response.Data.Records[i].SubCategories.Records){
                        if(categoryGUID.length > 0){
                            if(response.Data.Records[i].SubCategories.Records[j].CategoryGUID == categoryGUID){
                                $scope.ParentCategoryGUIDIndex = j;
                            }
                        }
                        //console.log(response.Data.Records[i].SubCategories.Records[j].CategoryGUID);
                        $scope.categoryDataList.push(response.Data.Records[i].SubCategories.Records[j]);
                    } 
                }          
            } ////console.log($scope.categoryDataList);
        });
    }


    $scope.getSubCategoryList = function (ParentCategoryGUID){
        console.log(ParentCategoryGUID);
        $scope.subCategoryDataList = [];
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID;
        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                $scope.subCategoryDataList = response.Data.Records; 
            } //////console.log($scope.categoryDataList);
        });
        console.log($scope.subCategoryDataList);
    }

    /*list append*/
    $scope.getList = function (courseID="",subjectID="", check=0)
    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        if(check != 1){
          if ($scope.data.listLoading || $scope.data.noRecords) return;   
        }
                 
        $scope.data.listLoading = true;  
        //$scope.SubjectGUID = subjectID;
        //$scope.CourseGUID = courseID;       
        var data = 'SessionKey='+SessionKey+'&SubjectGUID='+subjectID+'&CourseGUID='+courseID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'
        +'&PostType=Content&'+$('#filterForm').serialize();
        $scope.data.dataList = [];
        $http.post(API_URL+'content/getContentCount', data, contentType).then(function(response) {
                var response = response.data;
                ////console.log(response.Data.length);
                if(response.ResponseCode==200 && response.Data){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    //console.log(response.Data.length);
                    if(response.Data.length > 0){
                        for (var i in response.Data) {
                            //console.log(response.Data[i]);
                            //for (var i in response.Data) {
                                $scope.data.dataList.push(response.Data[i]);
                                ////console.log(response.Data[i]);
                            ///}
                        }
                    }else{
                        $scope.data.noRecords = true;
                    }
                    ////console.log($scope.data.dataList);
                 $scope.data.pageNo++;               
             }else{
                $scope.data.noRecords = true;
            }

            $scope.data.listLoading = false;
        });
    }


    /*list append*/
    $scope.getDetailsList = function (check="")
    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }
        $scope.getCategoryList(); 
        if(check == 1){
            $scope.CategoryGUID = $("#subject").val(); 
            $scope.ParentCategoryGUID =   $("#Courses").val();
            //var parentIndex = $("#Courses").val(); 
            // if(parentIndex){
            //      $scope.ParentCategoryGUIDIndex = parentIndex;
            // }
        }else{
            if ($scope.data.listLoading || $scope.data.noRecords) return;       
            $scope.CategoryGUID = $scope.getUrlParameter('CategoryGUID');   
            var parentIndex = $scope.getUrlParameter('index'); 
            if(parentIndex){
                 $scope.ParentCategoryGUIDIndex = parentIndex;
            }
            var parentCategoryGUID = $scope.getUrlParameter('parentCategoryGUID');
            $scope.ParentCategoryGUID =   parentCategoryGUID;
            $scope.getSubCategoryList(parentCategoryGUID);
        }
           
        $scope.data.listLoading = true;  

       
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'
        +'&PostType=Content&CategoryGUID='+$scope.CategoryGUID+'&'+$('#filterForm').serialize();

        
        
        $scope.data.dataList = [];
        $http.post(API_URL+'content/getContentByCategory', data, contentType).then(function(response) {
                var response = response.data;
               // $scope.data.totalRecords = response.Data.TotalCount;
                if(response.ResponseCode==200){ /* success case */
                    
                    //alert($scope.data.totalRecords);
                    var length = 0;
                    for (var i in response.Data) {
                        $scope.data.dataList.push(response.Data[i]); 
                    }

                    $scope.data.totalRecords = $('#content-details-table tr').length;
                 $scope.data.pageNo++;               
            }else{
                $scope.data.noRecords = true;
            }
            if(check != 1){
                $timeout(function(){
                    //$scope.ParentCategoryGUIDIndex = parentIndex;  
                    if($scope.ParentCategoryGUIDIndex){
                        $("#Courses").val($scope.ParentCategoryGUIDIndex);
                        $scope.getCategorySubjects($scope.ParentCategoryGUIDIndex);
                    }
                    $timeout(function(){ 
                        if($scope.ParentCategoryGUIDIndex){
                           $("#subject").val($scope.CategoryGUID); 
                        }                       
                    }, 500);
                }, 500);
            }
            $scope.data.listLoading = false;
        });

    }


    $scope.againLoadFormAdd = function (){
        $scope.data.pageLoading = true;
        $('.modal-header .close-subject').click();
        $('#add_model').modal({show:true}); 
        $timeout(function(){  
            tinymce.init({
                selector: '#PostContent'
            });  
        }, 200);      
        $scope.data.pageLoading = false;
    }


     /*add data*/
    $scope.addSubject = function ()
    {
        $scope.addDataLoading = true;        
        var PCategoryGUIDIndex = $("#ParentCategoryGUIDIndex").val();
        var ParentCategoryName = $("#ParentCategoryName").val();
        var SubCategoryName = $("#SubCategoryName").val();
        var ParentCategoryGUID = $("#ParentCategoryGUID").val();
        $scope.getSubCategoryList(ParentCategoryGUID);
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_subject_form']").serialize();
        $http.post(API_URL+'admin/category/add', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup("New subject added successfully"); 
                $scope.applyFilter();
                $scope.data.pageLoading = false; 
                $scope.SubCategoryGUID = response.Data.CategoryGUID;                   
                $('.modal-header .close-subject').click();
                    $scope.data.pageLoading = true;
                    $scope.categoryDataList = [];
                    $timeout(function(){                            
                        $('#add_model').modal({show:true}); 
                        $scope.data.pageLoading = false;                           
                        $timeout(function(){ 
                           //alert(PCategoryGUIDIndex);
                            $(".Courses option").filter(function() {
                                return this.text == ParentCategoryName; 
                            }).attr('selected', true);
                            $scope.ParentCategoryGUIDIndex = $(".Courses").val(); 
                            $('#subjectID').append(
                                $('<option></option>').val(response.Data.CategoryGUID).html(SubCategoryName)
                            );  
                            $("#subjectID").val(response.Data.CategoryGUID);                          
                        }, 300);                  
                    }, 600);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });
    }


     /*load add form*/
    $scope.loadFormAdd = function ()
    {
        $scope.AddBySelectedCategory = 0;
        $scope.templateURLAdd = PATH_TEMPLATE+'content/add_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $('#add_model').modal({show:true}); 
        $scope.data.pageLoading = false; 
    }


    $scope.LoadSelectedCategoryFormAdd = function (parentCategoryGUID,SubCategoryGUID)
    {
        $scope.AddBySelectedCategory = 1;
        $scope.getSubCategoryList(parentCategoryGUID);
        var a = parentCategoryGUID;
        $scope.templateURLAdd = PATH_TEMPLATE+'content/add_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $scope.SubjectGUID = SubCategoryGUID;
        $scope.CourseGUID = parentCategoryGUID;
        $('#add_model').modal({show:true}); 
        $scope.data.pageLoading = false;
    }


    /*load add form*/
    $scope.loadFormSubjectAdd = function (Position, CategoryGUID)
    {
        console.log($scope.CourseData);  
        $('#add_model .close').click();
        $scope.subjectTemplateURLAdd = PATH_TEMPLATE+'content/add_subject.htm?'+Math.random();        
        $('#add_subject').modal({show:true});
       //  $timeout(function(){            
       //     $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
       // }, 200);
    }



    /*load edit form*/
    $scope.loadFormEdit = function (Position, PostGUID, PostID)
    {
        // //console.log(PostGUID);
        $scope.data.Position = Position;
        $scope.templateURLEdit = PATH_TEMPLATE+'content/edit_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        // $('#edit_model').modal({show:true});
        
        // $scope.data.pageLoading = false;
        ////console.log(PostGUID);
        $http.post(API_URL+'content/getContent', 'SessionKey='+SessionKey+'&PostGUID='+PostGUID+'&PostID='+PostID, contentType).then(function(response) {
            //var response = response.data;
            if(response.data.ResponseCode==200){ /* success case */
                var cat_data = '';
                $scope.formData = response.data.Data;
                $http.post(API_URL+'category/getCategories', cat_data, contentType).then(function(response) {
                    var response = response.data;
                    if(response.ResponseCode==200 && response.Data.Records){
                    var options = '<select  id="subject"  name="CategoryGUIDs" class="form-control" onchange="" onclick="check_subject_available()" required="">'; /* success case */
                            $scope.data.totalRecords = response.Data.TotalRecords;
                            $scope.categoryDataList = [];
                            for (var i in response.Data.Records) {
                                for(var j in response.Data.Records[i].SubCategories.Records){
                                    $scope.categoryDataList.push(response.Data.Records[i].SubCategories.Records[j]);
                                }                                  
                            }
                            for (var k in $scope.categoryDataList) {
                                ////console.log($scope.categoryDataList[i]);
                                  if($scope.categoryDataList[k].CategoryGUID == $scope.formData.ParentCategory.CategoryGUID){
                                    var isn = k;
                                    ////console.log($scope.categoryDataList[i].CategoryGUID);
                                        var SubCategories = $scope.categoryDataList[k].SubCategories.Records;
                                        ////console.log(SubCategories);
                                        options += '<option value="">Select Subject</option>';
                                        for (var i in SubCategories) {
                                            var selected = "";
                                            if(SubCategories[i].CategoryName == $scope.formData.CategoryNames){
                                                var selected = "selected='selected'";
                                            }
                                            options += '<option value="'+SubCategories[i].CategoryGUID+'" '+selected+'>'+SubCategories[i].CategoryName+'</option>';               
                                        } 
                                  }
                            }

                            options += '</select>';
                                $scope.data.pageLoading = false;
                                
                                $('#edit_model').modal({show:true});
                                
                                $timeout(function(){ 
                                    //console.log(isn);
                                    if(isn){
                                        $("#Courses").val(isn);
                                    }                
                                    if(options){
                                     //console.log(options);
                                        $("#subcategory_section").html(options);
                                    }   

                                    tinymce.init({
                                        selector: '#PostContent'
                                      });           
                                    // $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                                }, 250);
                        ////console.log($scope.data.categoryDataList);
                        // $scope.data.pageNo++;               
                     }else{
                        //$scope.data.noRecords = true;
                    }
                });
               
                ////console.log(formData.Media_file);
                
            }
        });
    }



    /*load edit View*/
    $scope.loadFormView = function (Position, PostGUID)
    {
        // //console.log(PostGUID);
        $scope.data.Position = Position;
        $scope.templateURLView = PATH_TEMPLATE+'content/view_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        // $('#edit_model').modal({show:true});
        $scope.categoryDataList = [];
        if($scope.categoryDataList.length < 1){
            $scope.getCategoryList();
        }  
        // $scope.data.pageLoading = false;
        ////console.log(PostGUID);
        $http.post(API_URL+'content/getContent', 'SessionKey='+SessionKey+'&PostGUID='+PostGUID, contentType).then(function(response) {
            //var response = response.data;
            if(response.data.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.data.Data;
                $('#view_model').modal({show:true});
                for (var i in $scope.categoryDataList) {
                    //console.log(1);
                  if($scope.categoryDataList[i].CategoryGUID == $scope.formData.ParentCategory.CategoryGUID){
                        var SubCategories = $scope.categoryDataList[i].SubCategories.Records;
                        //console.log(SubCategories);
                        var options = '<option value="">Select Subject</option>';
                        for (var i in SubCategories) {
                            var selected = "";
                            if(SubCategories[i].CategoryName == $scope.formData.CategoryNames){
                                var selected = "selected";
                            }
                            options += '<option '+selected+' value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               
                        } 
                        if(options){
                            $("#CategoryGUID").html(options);
                        }
                  }
                }
                $scope.selectedOption = $scope.formData.ParentCategory.CategoryGUID;
                ////console.log(formData.Media_file);
                // $timeout(function(){            
                //      $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                // }, 200);
            }
        });
    }

    $scope.applytinymce = function(){
         tinymce.init({
            selector: '#PostContent',
            setup: function(editor) {
                editor.on('keyup', function(e) {
                console.log('edited. Contents: ' + editor.getContent());
                formDataCheck(editor.getContent());
                });
            }
        });  
    }

    $scope.loadContentViewEdit = function (Position, loadType, ContentType, title, content, id, PostGUID)
    {        
        console.log(title); console.log(content);
        $scope.data.Position = Position;
        $scope.data.pageLoading = true;
        $scope.type = ContentType;
        $scope.title = title;
        $scope.content = content;
        $scope.id = id;
        $scope.PostGUID = PostGUID;
        if(ContentType=='file'){
            $scope.fileExtension = content.replace(/^.*\./, '');
        }
        if(loadType == 'view'){
        	$scope.templateURLView = PATH_TEMPLATE+'content/view_content.htm?'+Math.random();
        	$('#view_model').modal({show:true});
        }else{
        	$scope.templateURLEdit = PATH_TEMPLATE+'content/edit_content.htm?'+Math.random();
        	$('#edit_model').modal({show:true});
            $timeout(function(){  console.log('editPostContent');
               $scope.applytinymce();              
            }, 1000);
        }
        $scope.data.pageLoading = false;
    }


    $scope.extension_popup_close = function(){
        alert("fjdskjf");
        $scope.data.pageLoading = true;
        $('#view_model').modal({show:true});
        $scope.data.pageLoading = false;
    }



    /*load delete form*/
    $scope.loadFormDelete = function (Position, PostGUID)
    {
        $scope.data.Position = Position;
        // $scope.templateURLDelete = PATH_TEMPLATE+'content/delete_form.htm?'+Math.random();
        // $scope.data.pageLoading = true;
        // $scope.MediaGUID = "";
        // $scope.VideoEntityGUID = "";
        // $scope.PostGUID = PostGUID;
        // $('#delete_model').modal({show:true});
        // $scope.data.pageLoading = false;
        $scope.deleteTextContent(PostGUID);
    }


     /*load delete form*/
    $scope.loadFormMediaDelete = function (Position, MediaGUID)
    {
        $scope.data.Position = Position;
        //alert(PATH_TEMPLATE+'content/delete_form.htm?');
        // $scope.templateURLDelete = PATH_TEMPLATE+'content/delete_form.htm?'+Math.random();
        // $scope.data.pageLoading = true;
        // $scope.PostGUID = "";
        // $scope.VideoEntityGUID = "";
        // $scope.MediaGUID = MediaGUID;
        // $('#delete_model').modal({show:true});
        // $scope.data.pageLoading = false;
        $scope.deleteMedia(MediaGUID);
    }


    $scope.loadFormVideoDelete = function (Position, VideoEntityGUID)
    {
        $scope.data.Position = Position;
        //alert(PATH_TEMPLATE+'content/delete_form.htm?');
        // $scope.templateURLDelete = PATH_TEMPLATE+'content/delete_form.htm?'+Math.random();
        // $scope.data.pageLoading = true;
        // $scope.PostGUID = "";
        // $scope.MediaGUID = "";
        // $scope.VideoEntityGUID = VideoEntityGUID;
        // $('#delete_model').modal({show:true});
        // $scope.data.pageLoading = false;
        $scope.deleteVideo(VideoEntityGUID);
    }


    $scope.deleteVideo = function (VideoEntityGUID)
    {
        $scope.deleteDataLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'content/deleteVideo', 'SessionKey='+SessionKey+'&VideoEntityGUID='+VideoEntityGUID, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.getDetailsList();
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }



    $scope.deleteMedia = function (MediaGUID)
    {
        $scope.deleteDataLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'content/deleteMedia', 'SessionKey='+SessionKey+'&MediaGUID='+MediaGUID, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.getDetailsList();
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


    $scope.deleteTextContent = function(PostGUID)
    {
        $scope.deleteDataLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'content/deleteTextContent', 'SessionKey='+SessionKey+'&PostGUID='+PostGUID, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.getDetailsList();
                    $('.modal-header .close').click();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }


  

    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }


    $scope.check_url = function(url) {
        var regYoutube = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var regVimeo = new RegExp(/(vimeo\.com)/);
        if(url.match(regYoutube)) {
            return 'youtube';
        }else if(regVimeo.test(url)) {
            return 'vimeo';
        }else{
            return false;
        }
    }


    /*add data*/
    $scope.addData = function ()
    {
        $scope.editDataLoading = true;
        $scope.loadeSubmit = true;
        var check = true;
        tinyMCE.triggerSave();
        var PostVideoLink = $("input[name='video_file[]']")
        .map(function() {
            if($(this).val()){
                return $(this).val();
            }
        }).get();

        if(PostVideoLink.length > 0){
            $.each(PostVideoLink, function( key, value ) {
                if(value){
                   var a = $scope.check_url(value);
                  if(a == false){
                    check = false;
                    ErrorPopup("Wrong entry exist for Video Link.");
                    return false;
                  } 
              }
            });
        }

        if(check){

            var data = 'SessionKey='+SessionKey;

            //$scope.Media = [];

            var audio_MediaGUIDs = $("#audio_MediaGUIDs").val();
            var j = 0;
            if(audio_MediaGUIDs){
                data += '&audio_MediaGUIDs='+audio_MediaGUIDs;
                var audio_MediaGUIDs = audio_MediaGUIDs.split(",");
                for (i = 0; i < audio_MediaGUIDs.length; i++) { 
                  var j = i;
                    $scope.Media.push({
                        MediaGUID: audio_MediaGUIDs[i], 
                        MediaCaption:  $("."+audio_MediaGUIDs[i]).val()
                    });
                }
            }

            var file_MediaGUIDs = $("#file_MediaGUIDs").val();
            if(file_MediaGUIDs){
                data += '&file_MediaGUIDs='+file_MediaGUIDs;
                var file_MediaGUIDs = file_MediaGUIDs.split(",");
                for (i = 0; i < file_MediaGUIDs.length; i++) {
                  $scope.Media.push({
                        MediaGUID: file_MediaGUIDs[i], 
                        MediaCaption:  $("."+file_MediaGUIDs[i]).val()
                    });
                }
            }

            //console.log($scope.Media); //return false;

            if($scope.Media.length > 0){
                data += '&Media='+JSON.stringify($scope.Media);
            }else{
                data += '&Media='+$scope.Media;
            }
            
            var PostType = $("#PostType").val();
            if(PostType){
                data += '&PostType='+PostType;
            }

            var CategoryGUIDs = $("#subjectID").val();
            if(CategoryGUIDs){
                data += '&CategoryGUID='+CategoryGUIDs;
            }
            //console.log(CategoryGUIDs); return false;
            var PostVideoTitle = $("input[name='video_MediaCaption[]']")
                .map(function() {
                    if($(this).val()){
                        return $(this).val();
                    }
                }).get();
            if(PostVideoTitle.length > 0){
                //PostVideoTitle = $.makeArray(PostVideoTitle);
                data += '&PostVideoTitle='+JSON.stringify(PostVideoTitle);
            }


            if(PostVideoLink.length > 0){
                data += '&PostVideoLink='+JSON.stringify(PostVideoLink);
            }



            var PostContent = $("#PostContent").val();
            if(PostContent){
                data += '&PostContent='+PostContent;
            }
            
            var PostCaption = $("#PostCaption").val();
            if(PostCaption){
                data += '&PostCaption='+PostCaption;
            }


            var ParentCategoryGUID = $("#ParentCategoryGUID").val();
            if(ParentCategoryGUID){
                data += '&ParentCategoryGUID='+ParentCategoryGUID;
            }

            //console.log(ParentCategoryGUID); return false;
            
            $http.post(API_URL+'content/addContent', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    $scope.getList(); 
                    SuccessPopup(response.Message);

                    $timeout(function(){ 
                      $scope.editDataLoading = false;
                        $scope.loadeSubmit = false;
                        $('#add_model .close').click();                        
                    }, 100);              
                    //SuccessPopup(response.Message);
                    //$scope.getList();
                    //$('.modal-header .close').click();
                   // window.location.reload();
                   // $('.modal').modal('toggle');
                }else{
                    ErrorPopup(response.Message);
                }
                $scope.editDataLoading = false;
                $scope.loadeSubmit = false;      
            });
        }
    }


    $scope.updateContentMedia = function (MediaGUID,PostGUID)
    {
    	if(PostGUID.length > 0 && MediaGUID.length > 0){

            $scope.addDataLoading = true; 
	    	var data = 'SessionKey='+SessionKey;

			$scope.Media = [];

			$scope.Media.push({
			    MediaGUID: MediaGUID, 
			    MediaCaption:  $("#MediaCaption").val()
			});


			if($scope.Media.length > 0){
			    data += '&Media='+JSON.stringify($scope.Media);
			}else{
			    data += '&Media='+$scope.Media;
			}

			data += '&PostGUID='+PostGUID;

			$http.post(API_URL+'content/updateContentMedia', data, contentType).then(function(response) {
	            var response = response.data;
	            $scope.addDataLoading = false; 

                if(response.ResponseCode==200){ /* success case */               
	                SuccessPopup(response.Message);
	                $scope.getDetailsList();
	                $('.modal-header .close').click();
	               // $('.modal').modal('toggle');
	            }else{
	                ErrorPopup(response.Message);
	            }
	              
                       
	        });
	    }
    }

    $scope.updateContentVideo = function (VideoEntityGUID)
    {
    	var PostVideoLink = $("#PostVideoLink").val();
		var isUrlValidsss  = isUrlValid(PostVideoLink); 
        
    	if(PostVideoLink.length > 0){ 

        $scope.addDataLoading = true;          
          var a = $scope.check_url(PostVideoLink);
          if(a == false){
            check = false;
            ErrorPopup("Only youtube and vimeo links are allowed.");
            return false;
          }
          else{
        	var data = 'SessionKey='+SessionKey+'&'+$('#video_form').serialize();

				$http.post(API_URL+'content/updateContentVideo', data, contentType).then(function(response) {
		            var response = response.data;
		            $scope.addDataLoading = false; 
                    if(response.ResponseCode==200){ /* success case */               
		                SuccessPopup(response.Message);
		                $scope.getDetailsList();
		                $('.modal-header .close').click();
		               // $('.modal').modal('toggle');
		            }else{
		                ErrorPopup(response.Message);
		            }
		              
                    //window.location.href = window.location.href;       
		        });
	        }
        }
    }


    /*edit data*/
    $scope.editData = function (PostGUID)
    {
        if(PostGUID){
            $scope.addDataLoading = true;
            var check = true;
            tinyMCE.triggerSave();
            var PostVideoLink = $("input[name='video_file[]']")
            .map(function() {
                if($(this).val()){
                    return $(this).val();
                }
            }).get();

            ////console.log(PostVideoLink.length); return false;

            if(PostVideoLink.length > 0){
                $.each(PostVideoLink, function( key, value ) {
                    if(value){
                       var a = $scope.check_url(value);
                      if(a == false){
                        check = false;
                        ErrorPopup("Wrong entry exist for Video Link.");
                        return false;
                      } 
                  }
                });
            }

            if(check){

                var data = 'SessionKey='+SessionKey;

                data += '&PostGUID='+PostGUID;

                $scope.Media = [];

                var audio_MediaGUIDs = $("#audio_MediaGUIDs").val();
                if(audio_MediaGUIDs){
                    data += '&audio_MediaGUIDs='+audio_MediaGUIDs;
                    var audio_MediaGUIDs = audio_MediaGUIDs.split(",");
                    for (i = 0; i < audio_MediaGUIDs.length; i++) {
                        $scope.Media.push({
                            MediaGUID: audio_MediaGUIDs[i], 
                            MediaCaption:  $("."+audio_MediaGUIDs[i]).val()
                        });
                    }
                }

                var file_MediaGUIDs = $("#file_MediaGUIDs").val();
                if(file_MediaGUIDs){
                    data += '&file_MediaGUIDs='+file_MediaGUIDs;
                    var file_MediaGUIDs = file_MediaGUIDs.split(",");
                    for (i = 0; i < file_MediaGUIDs.length; i++) {
                      $scope.Media.push({
                            MediaGUID: file_MediaGUIDs[i], 
                            MediaCaption:  $("."+file_MediaGUIDs[i]).val()
                        });
                    }
                }

                if($scope.Media.length > 0){
                    data += '&Media='+JSON.stringify($scope.Media);
                }else{
                    data += '&Media='+$scope.Media;
                }
                
                
                var PostType = $("#PostType").val();
                if(PostType){
                    data += '&PostType='+PostType;
                }

                var CategoryGUIDs = $("#subject").val();
                if(CategoryGUIDs){
                    data += '&CategoryGUID='+CategoryGUIDs;
                }
                
                var PostVideoTitle = $("input[name='video_MediaCaption[]']")
                    .map(function() {
                        if($(this).val()){
                            return $(this).val();
                        }
                    }).get();
                if(PostVideoTitle.length > 0){
                   if(PostVideoTitle!=""){
                     data += '&PostVideoTitle='+JSON.stringify(PostVideoTitle);
                   }
                }


                if(PostVideoLink.length > 0){
                   if(PostVideoLink!=""){
                     data += '&PostVideoLink='+JSON.stringify(PostVideoLink);
                   }
                }



                var PostContent = $("#PostContent").val();
                if(PostContent){
                    data += '&PostContent='+PostContent;
                }
                
                var PostCaption = $("#PostCaption").val();
                if(PostCaption){
                    data += '&PostCaption='+PostCaption;
                }
                
                $http.post(API_URL+'content/editContent', data, contentType).then(function(response) {
                    var response = response.data;
                    $scope.addDataLoading = false; 

                    if(response.ResponseCode==200){ /* success case */               
                        SuccessPopup(response.Message);
                        $scope.getDetailsList();
                        $('.modal-header .close').click();
                       // $('.modal').modal('toggle');
                    }else{
                        ErrorPopup(response.Message);
                    }
                    
                             
                });
            }
        }
    }



    /*delete selected */
    $scope.deleteSelectedRecords = function ()
    {
        alertify.confirm('Are you sure you want to delete?', function(){  
            var data = 'SessionKey='+SessionKey+'&'+$('#records_form').serialize();
            $http.post(API_URL+'admin/entity/deleteSelected', data, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */               
                    SuccessPopup(response.Message);
                    $scope.applyFilter();
                }else{
                    ErrorPopup(response.Message);
                }
                if($scope.data.totalRecords==0){
                   $scope.data.noRecords = true;
               }
           });
        }).set('labels', {ok:'Yes', cancel:'No'});
    }


    $scope.deleteContentData = function(EntityGUID) {
        $scope.deleteDataLoading = true;
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&EntityGUID=' + EntityGUID;
            $http.post(API_URL + 'admin/entity/delete', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $('.modal-header .close').click();
                    $scope.applyFilter();
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;

    }

    $scope.changeButtonStatus = function(status){ 
        $scope.formDataCheck = status; 
        ////console.log("formDataCheck=="+status); 
        if(status == true){
           $("button[data-toggle=tooltip]").attr('title', '');
           $("#submit-btn").removeClass("disabled-btn");
           $("#submit-btn").tooltip('disable');
           $("#submit-btn").addClass("enabled-btn");
           $("#submit-btn").prop("disabled", false); 
          // //console.log("formDataCheck=="+status); 
        }else{
           $("#submit-btn").removeClass("enabled-btn");
           $("#submit-btn").tooltip('enable');
           $("#submit-btn").addClass("disabled-btn");
           $("#submit-btn").prop("disabled", true); 
           $("#submit-btn[data-toggle=tooltip]").attr('title', 'Course, subject and atleast one type of content is required!');
          // //console.log($scope.formDataCheck); 
        } 
    }

}); 

function getCategoryFilterData(ParentCategoryGUIDIndex){
  angular.element(document.getElementById('content-body')).scope().getCategoryFilterData(ParentCategoryGUIDIndex,'subCategory');  
}

function getCategorySubjects(ParentCategoryGUID){
    console.log(ParentCategoryGUID);
  angular.element(document.getElementById('content-body')).scope().getSubCategoryList(ParentCategoryGUID);
  angular.element(document.getElementById('content-body')).scope().getList(ParentCategoryGUID,"",1);     
}

function againLoadFormAdd(){
  angular.element(document.getElementById('content-body')).scope().againLoadFormAdd();  
}

var arr_mediaGUID = [];
var audio_mediaGUID = [];
var file_mediaGUID = [];
function check_subject_available(){
    if($('#subject option').length < 2){
        if($('#Courses').val()){
            alert("Subjects are not available for the selected course.")
        }else{
           alert("You need to select course before adding the subject.");   
        }
    }
}

/*Enable disable submit button*/
// function formDataCheck(PostContentText=""){    
//     var title_length = 0;
//     var file_length = 0;

//     $(":input.title").each(function(i, val) {
//       if ($(this).val() || $(this).prop('disabled')) {
//         title_length++;
//       }
//     });

//     $(":input.file").each(function(i, val) {
//       if ($(this).val() || $(this).prop('disabled')) {
//         file_length++;
//       }
//     });

//     //console.log(title_length);
//     //console.log(file_length);

//     if(title_length > 0){ 
//         if(file_length > 0){
//             if(file_length == title_length){
//                 if( $('#subject').val() ) {
//                     angular.element(document.getElementById('content-body')).scope().changeButtonStatus(true);
//                     //$scope.formDataCheck = true;
//                 }else{
//                     $("#submit-btn[data-toggle=tooltip]").attr('title', 'Please select course and subject to add content.');
//                     angular.element(document.getElementById('content-body')).scope().changeButtonStatus(false);
//                 }
//             }else{
//                 $("#submit-btn[data-toggle=tooltip]").attr('title', 'Please select atleast one type of content.');
//                 angular.element(document.getElementById('content-body')).scope().changeButtonStatus(false);
//             }
//         }else{
//           angular.element(document.getElementById('content-body')).scope().changeButtonStatus(false);
//         }
//     }else{ 
//         ////console.log($(".manual_title").length); //console.log($(".manual_text").length);
//         if($(".manual_title").val()){ console.log($("#tinymce").text());
//             //if(PostContentText.length > 0){
//                 if( $('#subject').val() ) {
//                     $("#submit-btn[data-toggle=tooltip]").attr('title', 'Please select course and subject to add content.');
//                     angular.element(document.getElementById('content-body')).scope().changeButtonStatus(true);
//                     //$scope.formDataCheck = true;
//                 }else{
//                     ////console.log("fdsf");
//                     $("#submit-btn[data-toggle=tooltip]").attr('title', 'Please select atleast one type of content.');
//                     angular.element(document.getElementById('content-body')).scope().changeButtonStatus(false);
//                 }
//             // }else{
//             //     angular.element(document.getElementById('content-body')).scope().changeButtonStatus(false);
//             // }
//         }else{
//             $("#submit-btn[data-toggle=tooltip]").attr('title', 'Please select atleast one type of content.');
//             angular.element(document.getElementById('content-body')).scope().changeButtonStatus(false);
//         }          
//     }
// }

/*check duplicate entry for form input values*/
function find_duplicate_in_array(title_name) {
    var object = {};
    var result = [];

    var arra1 = $("."+title_name)
            .map(function() {
                if($(this).val()){
                 return $(this).val();
                }
            }).get();

    arra1.forEach(function (item) {
      if(!object[item])
          object[item] = 0;
        object[item] += 1;
    })

    for (var prop in object) {
       if(object[prop] >= 2) {
           result.push(prop);
       }
    }
    return result;
}


/*check url is video or vimeo*/
function check_url(url) {
    var regYoutube = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    var regVimeo = new RegExp(/(vimeo\.com)/);
    if(url.match(regYoutube)) {
        return 'youtube';
    }else if(regVimeo.test(url)) {
        return 'vimeo';
    }else{
        return false;
    }
}

function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

/*check empty inputs and duplicate titles*/
function check_empty_input(title_name,file_name,type) {
    if(type == "doc"){
        var check = true;
        var files = $("."+file_name)
            .map(function() {
                return $(this).val();
            }).get();
        var titles = $("."+title_name)
            .map(function() {
                return $(this).val();
            }).get();
           // //console.log(files); //console.log(titles);
        var duplicate_vals = find_duplicate_in_array(title_name);
    }else{
        var check = true;
        var files = $("."+file_name)
            .map(function() {
                return $(this).val();
            }).get();
        var titles = $("."+title_name)
            .map(function() {
                return $(this).val();
            }).get();
        var duplicate_vals = find_duplicate_in_array(title_name);
    }

    if(duplicate_vals.length > 0){
        alert("Duplicate entries are found for titles! Please remove duplicate entries before adding new.");
        check = false;
        return false;
    }else{      
        $.each(titles, function(index, value) {
            if (value == "") {
                if(type == "doc"){
                    alert("Please fill title before adding a file.");
                }else{
                    alert("Please fill previously blank fields before adding new.");
                }
                check = false;
                var n = index + 1;
                //$(".content-t" + n).css('border', '1px solid red');
                return false;
            } else {                
                var n = index + 1;
                //$(".content-t" + n).css('border', '1px solid rgba(33, 33, 33, 0.12)');
            }
            if (files[index] == "") {
                alert("Please fill previously blank fields before adding new.");
                check = false;
                var n = index + 1;
                //$(".content-f" + n).closest(".form-group").addClass('has-error has-danger');
                //$(".content-f"+n).css('border','1px solid red');
                return false;
            } else {
                if(file_name == "video_file"){
                    //var url_type = check_url(value);
                    var isUrlValidsss  = isUrlValid(files[index]);
                    if(isUrlValidsss){
                        var url_type = check_url(files[index]);
                        //console.log(url_type);
                        if(url_type != 'youtube' && url_type != 'vimeo'){
                            alert("Only youtube and vimeo links are allowed.");
                            check = false;
                            var n = index + 1;
                            return false;
                        }
                    }else{
                        alert("Video link is not valid.");
                        check = false;
                        var n = index + 1;
                        return false;
                    }
                }
                var n = index + 1;
                //$(".content-f" + n).closest(".form-group").removeClass('has-error has-danger');
            }
        });
    }
    return check;
}


function remove_media(mediaguid){
    //console.log(mediaguid);
    //console.log(arr_mediaGUID);
    if(mediaguid){
        var index = arr_mediaGUID.indexOf(mediaguid);
        if (index > -1) {
          arr_mediaGUID.splice(index, 1);
        }
        $("#MediaGUIDs").val(arr_mediaGUID);
        arr_mediaGUID = arr_mediaGUID;
        //console.log(arr_mediaGUID);
    }
}



/* Add More JavaScript Start Here */
$(document).ready(function() {

    $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

    $("body").on("click", ".add-more", function() {
    var mediaguid = $(".add-more").attr("mediaguid");
    var check = check_empty_input('doc_title','docfileInput','doc');
    if (check) {
            var numItems = $('.after-add-more').length;
            //if (numItems <= 4) {
                //var html = $(".after-add-more").first().clone(true, true);
                var count = numItems + 1;                
                $(".section1-add-more-btn").last().html('<div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br/><a class="btn btn-danger remove" ng-click="deleteMedia(\''+mediaguid+'\')" mediaguid="'+mediaguid+'">-</a></div>');
                $(".progressBar-doc").remove();
                var html = $(".after-add-more").last().after('<div class="after-add-more"><div class="row"><form enctype="multipart/form-data" action="../api/upload/file" method="post" class="col-md-12"><input type="hidden" name="Section" value="File"><label class="filter-col" for="inputName" style="width: 45%;float: left;">Title</label><label class="filter-col" style="width: 48%;float: right;margin-right: 42px;">Upload File:- Allowed file type : (docx, doc, pdf, xls, xlsx, ppt, pptx)</label><input type="text" class="form-control title doc_title file_MediaCaption1" id="doctitleInput-'+count+'" name="MediaCaption" onkeyup="" style="width:45%; float: left;"><input type="file" accept=".xls,.xlsx,.doc,.docx,.pdf,.ppt,.pptx" name="File" id="docfileInput-'+count+'"  class="form-control file upload docfileInput"  data-targetinput="#file_MediaGUIDs" style="width: 45%;float: left; margin-left: 20px;"><div class="section1-add-more-btn" style="width: 5%;float: right;margin-right: 3px;margin-top: -32px;"><div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br/> <a class="btn btn-success add-more" mediaguid=""><i class="fa fa-plus-circle"></i></a></div></div><div class="progressBar progressBar-doc"><div class="bar"></div><div class="percent">0%</div></div>  </form></div></div>');
            // } else {
            //     alert("You can upload maximum 5 files");
            // }
        }
    });

    $("body").on("click", ".add-more1", function() {
    var mediaguid = $(".add-more1").attr("mediaguid");
    var check = check_empty_input('audioTitleInput','audioFileInput','doc');
    //var check = check_empty_input('MediaCaption','file','audio');
    if (check) {
        if(mediaguid){
                var numItems = $('.after-add-more1').length;
                //if (numItems <= 4) {
                    var count = numItems + 1;
                    $(".section2-add-more-btn").last().html('<div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br/><a class="btn btn-danger remove" ng-click="deleteMedia(\''+mediaguid+'\')" mediaguid="'+mediaguid+'">-</a></div>');
                    $(".progressBar-audio").remove();
                    var html = $(".after-add-more1").last().after('<div class="after-add-more1 content-audio-row1"><div class="row"><form enctype="multipart/form-data" action="../api/upload/file" method="post" class="col-md-12"><input type="hidden" name="Section" value="Audio"><label class="filter-col" for="inputName" style="width: 45%;float: left;">Title</label><label class="filter-col" style="width: 48%;float: right;margin-right: 42px;">Upload Audio File:- Allowed  only Mp3</label><div class="title-sec"><input type="text" class="form-control title audioTitleInput file_MediaCaption1" id="audioTitleInput-'+count+'" name="MediaCaption" onkeyup="" style="width:45%; float: left;"></div><input type="file" accept="audio/mp3" name="File" id="audioFileInput-'+count+'"  class="form-control file upload audioFileInput"  data-targetinput="#audio_MediaGUIDs" style="width: 45%;float: left; margin-left: 20px;"><div class="section2-add-more-btn" style="width: 5%;float: right; margin-left: 20px; margin-top: -32px;"><div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br/><a class="btn btn-success add-more1" mediaguid=""><i class="fa fa-plus-circle"></i></a></div><!-- <div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br><a class="btn btn-danger remove">-</a></div> --></div><div class="progressBar progressBar-audio"><div class="bar"></div><div class="percent">0%</div></div></form></div></div>');
                // } else {
                //     alert("You can upload maximum 5 files");
                // }
            }else{
                 alert("File upload is in progress.");
            }
        }
    });

    $("body").on("click", ".video-add-more", function() {
        var check = check_empty_input('video_MediaCaption','video_file','video');
        if (check) {
            //if(mediaguid){
                var numItems = $('.after-video-add-more').length;
                //if (numItems <= 4) {
                    var count = numItems + 1;
                    $(".section2-add-more-video").html('<div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br/><a class="btn btn-danger remove">-</a></div>');
                    var html = $(".after-video-add-more").last().after('<div class="after-video-add-more content-video-row1"><div class="row"><label class="filter-col" for="inputName" style="width: 46%;float: left;margin-left: 10px;">Title</label><label for="inputName" class="control-label mb-10" style="width: 48%;float: right;margin-right: 42px;">Video Link :- Allowed link type : (youtube, vimeo etc.)</label><div class="title-sec" style="width: 44%;float: left; margin-left: 10px;"><input type="text" class="form-control content-tv1 title video_MediaCaption" name="video_MediaCaption[]" onkeyup="" style="width:100%; float: left;" autocomplete="off"></div><input type="url" class="form-control content-v1 file video_file" name="video_file[]" onkeyup="" style="width: 44%;float: left; margin-left: 20px;"><div class="section2-add-more-video" style="width: 5%;float: right; margin-left: 20px; margin-top: -32px;"><div class="form-group change"><label for="" style="margin: 5px 0;">&nbsp;</label><br><a class="btn btn-success video-add-more"><i class="fa fa-plus-circle"></i></a></div></div></div></div>');
                // } else {
                //     alert("You can add maximum 5 videos");
                // }
            //}
        }
    });

    $("body").on("click",".remove",function(){ 
        var mediaguid = $(this).attr("mediaguid");
       // alert($(this).parents(".after-add-more"));
        if(mediaguid){
            if($(this).parents(".after-add-more")){
                var MediaGUIDs = $("#file_MediaGUIDs").val();
            }else{
                var MediaGUIDs = $("#audio_MediaGUIDs").val();
            }
            
            if(MediaGUIDs){
                var MediaGUIDs_array = MediaGUIDs.split(',');
                MediaGUIDs_array = jQuery.grep(MediaGUIDs_array, function(value) {
                  return value != mediaguid;
                });
                if(MediaGUIDs_array.length > 0){
                    MediaGUIDs_string = MediaGUIDs_array.join(',');
                    if($(this).parents(".after-add-more")){
                        $("#file_MediaGUIDs").val(MediaGUIDs_string);
                    }else{
                        $("#audio_MediaGUIDs").val(MediaGUIDs_string);
                    }                    
                }else{
                    if($(this).parents(".after-add-more")){
                        $("#file_MediaGUIDs").val('');
                    }else{
                        $("#audio_MediaGUIDs").val('');
                    }   
                }              
            }
        }
        $(this).parents(".after-add-more").remove();
        $(this).parents(".after-add-more1").remove();
        $(this).parents(".after-video-add-more").remove();
        window.setTimeout(function() {
            //formDataCheck();
        }, 1000);        
    });
});
/* Add More JavaScript End Here */

function FilterByType(contentType){
    $("#no-records").hide();
    if(contentType == "all"){
        $(".content-details-tr").show();
    }else{
        $(".content-details-tr").hide();
        if($("."+contentType).length > 0){
            $("."+contentType).show();
        }else{
            $("#no-records").show();
        }
    }
}


function getDetailsListByCategory(CategoryGUID){
    if(CategoryGUID.length){
        var infds = $("#Courses").val();
        //window.location.replace("http://sandbox.prepindia.in/content/details_list/?index="+infds+"&CategoryGUID="+CategoryGUID);
        angular.element(document.getElementById('content-body')).scope().getDetailsList(1);
    }
}

/*doc file upload*/
$(document).on('change', '.docfileInput', function() {
    var check = check_empty_input('doc_title','docfileInput','doc');
    if (check) { 
        var MediaGUIDs = $("#file_MediaGUIDs").val();
        if(MediaGUIDs){
            arr_mediaGUID = MediaGUIDs.split(',');
            file_mediaGUID = MediaGUIDs.split(',');   
        }else{
            file_mediaGUID = []; 
        }

        var file_id = $(this).attr('id');
        var mediaGUID = $(this).data('targetinput');
        var progressBar = $('.progressBar-doc'),
            bar = $('.progressBar-doc .bar'),
            percent = $('.progressBar-doc .percent');
        $(this).parent().ajaxForm({
            data: {
                SessionKey: SessionKey
            },
            dataType: 'json',
            beforeSend: function() {
                progressBar.fadeIn();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function(obj, statusText, xhr, $form) {
                //console.log(obj);
                if (obj.ResponseCode == 200) {
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    arr_mediaGUID.push(obj.Data.MediaGUID);
                    file_mediaGUID.push(obj.Data.MediaGUID);                   
                    var string_mediaGUID = file_mediaGUID.join(",");
                    $(mediaGUID).val(string_mediaGUID);
                    $("#"+file_id).attr("disabled","disabled");
                    var ids = file_id.split("-");
                    $("#doctitleInput-"+ids[1]).addClass(obj.Data.MediaGUID);
                    $("#doctitleInput-"+ids[1]).attr("disabled","disabled");
                    $(".add-more").attr("mediaguid",obj.Data.MediaGUID);
                    //console.log(arr_mediaGUID);
                    //formDataCheck();
                    angular.element(document.getElementById('content-body')).scope().changeButtonStatus(true);
                } else {
                    ErrorPopup(obj.Message);
                }
            },
            complete: function(xhr) {
                progressBar.fadeOut();
                $('#fileInput').val("");
            }
        }).submit();
    }else{
        $(this).val("");
    }

});


function MediaDataCheck(field_val) {
	if(field_val != ""){
       $("#submit-btn").removeClass("disabled-btn");
       $("#submit-btn").tooltip('disable');
       $("#submit-btn").addClass("enabled-btn");
       $("#submit-btn").prop("disabled", false); 
      // //console.log("formDataCheck=="+status); 
    }else{
       $("#submit-btn").removeClass("enabled-btn");
       $("#submit-btn").tooltip('enable');
       $("#submit-btn").addClass("disabled-btn");
       $("#submit-btn").prop("disabled", true);
        $("button[data-toggle=tooltip]").attr('title', 'Title is required.'); 
      // //console.log($scope.formDataCheck); 
    } 
	// body...
}


function EditDataCheck() {//alert(2);
	if($("#PostVideoTitle").val().length > 0 && $("#PostVideoLink").val().length > 0){ //alert(1);
	   $("#submit-btn").removeClass("disabled-btn");
       $("#submit-btn").tooltip('disable');
       $("#submit-btn").addClass("enabled-btn");
       $("#submit-btn").prop("disabled", false); 
	}else{  
	   $("#submit-btn").removeClass("enabled-btn");
       $("#submit-btn").tooltip('enable');
       $("#submit-btn").addClass("disabled-btn");
       $("#submit-btn").prop("disabled", true); 
        $("button[data-toggle=tooltip]").attr('title', 'Title and video link is required.');
	}
}


/*audio file upload*/
$(document).on('change', '.audioFileInput', function() {
    var check = check_empty_input('audioTitleInput','audioFileInput','doc');
    if (check) { 
        var MediaGUIDs = $("#audio_MediaGUIDs").val();
        if(MediaGUIDs){
            arr_mediaGUID = MediaGUIDs.split(',');   
            audio_mediaGUID = MediaGUIDs.split(',');   
        }else{
            audio_mediaGUID = []; 
        }

        var file_id = $(this).attr('id');
        var mediaGUID = $(this).data('targetinput');
        var progressBar = $('.progressBar-audio'),
            bar = $('.progressBar-audio .bar'),
            percent = $('.progressBar-audio .percent');
        $(this).parent().ajaxForm({
            data: {
                SessionKey: SessionKey
            },
            dataType: 'json',
            beforeSend: function() {
                progressBar.fadeIn();
                var percentVal = '0%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function(obj, statusText, xhr, $form) {
                //console.log(obj);
                if (obj.ResponseCode == 200) {
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                    arr_mediaGUID.push(obj.Data.MediaGUID);  
                    audio_mediaGUID.push(obj.Data.MediaGUID);                   
                    var string_mediaGUID = audio_mediaGUID.join(",");
                    $(mediaGUID).val(string_mediaGUID);
                    $("#"+file_id).attr("disabled","disabled");
                    var ids = file_id.split("-");
                    $("#audioTitleInput-"+ids[1]).addClass(obj.Data.MediaGUID);
                    $("#audioTitleInput-"+ids[1]).attr("disabled","disabled");
                    $(".add-more1").attr("mediaguid",obj.Data.MediaGUID);
                    //console.log(arr_mediaGUID);
                    //formDataCheck();

                } else {
                    ErrorPopup(obj.Message);
                }
            },
            complete: function(xhr) {
                progressBar.fadeOut();
                $('#fileInput').val("");
            }
        }).submit();
    }else{
        $(this).val("");
    }

});

/* file size checks*/
$(document).on('change', 'input[type=file]', function (e) {
    if ($(this).attr('name') == "doc_file[]") {
        if (this.files[0].size / 1024 / 1024 > 2 || this.files[0].size / 1024 / 1024 == 2) {
            var total = this.files[0].size / 1024 / 1024;
            var rounded = Math.round(total * 10) / 10;
            alert('Files must be less than 2MB and this file size is: ' + rounded + " MB");
            $(this).val('');
        } else {
        }
    } else if ($(this).attr('name') == "audio_file[]") {
        if (this.files[0].size / 1024 / 1024 > 5 || this.files[0].size / 1024 / 1024 == 5) {
            var total = this.files[0].size / 1024 / 1024;
            var rounded = Math.round(total * 10) / 10;
            alert('Files must be less than 5MB and this file size is: ' + rounded + " MB");
            $(this).val('');
        } else {
            $(this).closest(".form-group").removeClass('has-error has-danger');
            $(".file_error").hide();
        }
    }
});

$("#add_model").on("hidden.bs.modal", function () {
    tinymce.remove();
});

$("#edit_model").on("hidden.bs.modal", function () {
    tinymce.remove();
});


// function getCategorySubjects(ParentCategoryGUIDIndex,apiCall){
//   angular.element(document.getElementById('content-body')).scope().getCategorySubjects(ParentCategoryGUIDIndex,apiCall);  
// }


function filterContentStatistics(CategoryGUID){
  var ParentCategoryGUID = $("#ParentCategoryGUID").val(); //console.log(ParentCategoryGUID);  
  angular.element(document.getElementById('content-body')).scope().getList(ParentCategoryGUID,CategoryGUID,1);  
}