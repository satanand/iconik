app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.Users = [];
    $scope.data.InterestedIn = [];

    $( "#filterFromDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterToDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#filterToDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterFromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentParentCategoryGUID = ParentCategoryGUID;

    $scope.CategoryTypeID = $scope.getUrlParameter('CategoryTypeID');

    $scope.APIURL = API_URL + "upload/image";
    /*----------------*/

    $scope.getFilterData = function ()

    {
        
        $scope.AssignedToUsersList();

        $scope.InterestedInList();

        /*$scope.getSecondLevelPermission();
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();

        $http.post(API_URL+'admin/category/getFilterData', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ 

             $scope.filterData =  response.Data;

             $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 300);          

         }

        });*/

    }





    /*list*/

    $scope.applyFilter = function ()

    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/

        
        $scope.getList(0);

    }


    /*------------------------------------------------------------------
    Load master filters
    -----------------------------------------------------------------*/
    $scope.AssignedToUsersList = function ()
    {
        var data = 'SessionKey='+SessionKey;

        //alert("AssignedToUsersList");
        $http.post(API_URL+'enquiry/getAssignedToUsersList', data, contentType).then(function(response) 
        {
            var response = response.data;
            //alert(response.Users);
            if(response.ResponseCode==200 && response.Users)
            {
                //alert($scope.data.Users.length);
                if($scope.data.Users.length <= 0)
                {
                    for (var i in response.Users) 
                    {
                        $scope.data.Users.push(response.Users[i]);
                    }
                }
            }
        });
    }

    $scope.InterestedInList = function ()
    {
        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'enquiry/getInterestedInList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.InterestedIn)
            {
                if($scope.data.InterestedIn.length <= 0)
                {
                    for (var i in response.InterestedIn) 
                    {
                        $scope.data.InterestedIn.push(response.InterestedIn[i]);
                    }
                }
            }
        });
    }




    /*list append*/

    $scope.getList = function (IsSet)
    {
        if(IsSet == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }    

        $scope.data.listLoading = true;        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
     

        $http.post(API_URL+'enquiry/getEnquiryList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records)
            { /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) 
                {
                    $scope.data.dataList.push(response.Data.Records[i]);
                }                 
                
                $scope.data.pageNo++;               
                
            }
            else
            {

                $scope.data.noRecords = true;
            }

            if(IsSet == 1)
            {
                checkEnquiryLength();
            }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }





    /*load add form*/

    $scope.loadFormAdd = function (Position, CategoryGUID)
    {

        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

        $('#add_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

    }


    /*add data*/

    $scope.addData = function ()
    {
        $scope.addDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'enquiry/addEnquiry', data, contentType).then(function(response) 
        {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);

                //$scope.applyFilter();

                $timeout(function()
                {            

                   $('#add_model .close').click();
                   window.location.href = BASE_URL + "enquiry";

               }, 1000);

            }
            else
            {

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }




    /*load view form*/

    $scope.loadFormView = function (Position, EnquiryGUID)

    {
        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'enquiry/getEnquiry', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#view_model').modal({show:true});

            }

        });

    }



    /*load edit form*/

    $scope.loadFormEdit = function (Position, EnquiryGUID)
    {
        $scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'enquiry/getEnquiry', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#edits_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }

        });

    }

    
    /*edit data*/

    $scope.editData = function ()
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'enquiry/addEnquiry', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);

                //$scope.applyFilter();

                $timeout(function()
                {            

                   $('#edits_model .close').click();
                   window.location.href = BASE_URL + "enquiry";

                }, 1000);

            }
            else
            {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }



    /*load delete */

    $scope.loadFormDelete = function(Position, EnquiryGUID) 
    {

        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() 
        {

            $http.post(API_URL + 'enquiry/delete', 'SessionKey=' + SessionKey + '&EnquiryGUID=' + EnquiryGUID, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) 
                {
                    /* success case */
                    SuccessPopup(response.Message);
                    
                    $scope.deleteDataLoading = true;                    

                    //$scope.applyFilter();

                    $timeout(function()
                    {            

                       $('.modal-header .close').click();
                       window.location.href = BASE_URL + "enquiry";

                    }, 1000);

                    
                }
                else
                {
                    ErrorPopup(response.Message);
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }



    /*load add Assign Enquiry*/
    $scope.loadFormAssign = function (Position, EnquiryGUID)
    {
        $scope.data.Position = Position;

        $scope.templateURLAssign = PATH_TEMPLATE+module+'/assign_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'enquiry/assignEnquiry', 'SessionKey='+SessionKey+'&EnquiryGUID='+EnquiryGUID, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;

                $('#assign_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }
            else
            {
                $scope.data.pageLoading = false;

                ErrorPopup(response.Message);
            } 
        });

    }
    
    //Post form and save assign enquiry 
    $scope.assignData = function ()
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='assign_form']").serialize();

        $http.post(API_URL+'enquiry/addAssignEnquiry', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);

                //$scope.applyFilter();

                $timeout(function()
                {            

                   $('#assign_model .close').click();
                   window.location.href = BASE_URL + "enquiry";

               }, 1000);

            }
            else
            {

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }


    $scope.loadFormAssigned = function ()
    {        
        window.location.href = BASE_URL + "enquiry/assigned";
    }

    $scope.loadFormChurned = function ()
    {        
        window.location.href = BASE_URL + "enquiry/churned";
    }



    /*load Bulk Assign Enquiry*/
    $scope.loadFormBulkAssign = function ()
    {
        $scope.templateURLBulkAssign = PATH_TEMPLATE+module+'/bulk_assign_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var dataVar = 'SessionKey='+SessionKey+'&'+$("form[name='bulk_assign_form']").serialize();

        $http.post(API_URL+'enquiry/bulkAssignEnquiry', dataVar, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data;

                $('#bulk_assign_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }
            else
            {
                $scope.data.pageLoading = false;

                ErrorPopup(response.Message);
            } 
        });

    }


    //Post form and save bulk assign enquiry 
    $scope.bulkAssignData = function ()
    {
        var selEnqCount = $("#EnquiryGUIDCount").val();
        var selEnqAss = $("#AssignedTo").val();
        var selEnqAssText = $("#AssignedTo").find("option:selected").html();
        var selRem = $("#Remarks").val();
        
        if(selEnqAss == "") { alert("Please select Assigned To"); return;}
        if(selRem == "") { alert("Please enter Remarks"); return;}

        var confirmText = "You are assigning <b>"+selEnqCount+"</b> enquiries to <b>"+selEnqAssText+"</b>";

        alertify.confirm(confirmText, function() 
        {
            $scope.editDataLoading = true;

            var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_bulk_assign_form']").serialize();

            $http.post(API_URL+'enquiry/addBulkAssignEnquiry', data, contentType).then(function(response) 
            {

                var response = response.data;

                if(response.ResponseCode==200)
                { /* success case */               

                    SuccessPopup(response.Message);                    

                    $timeout(function(){            

                       $('#bulk_assign_model .close').click();

                       window.location.href = BASE_URL + "enquiry";

                   }, 200);

                }
                else
                {

                    ErrorPopup(response.Message);

                }

                $scope.editDataLoading = false;          

            });

        }).set('labels', {
            ok: 'Confirm',
            cancel: 'Cancel'
        });
    }



    /*load Bulk Un Assign Enquiry*/
    $scope.loadFormBulkUnAssign = function ()
    {
        $scope.editDataLoading = true;

        var AssignedToUser = $("#filterAssignedToUser").val();
        var AssignedToUserText = $("#filterAssignedToUser").find("option:selected").html();

        var chk_len = $("#tabledivbody").find("input[name='EnquiryChk[]']:checked").length;
        
        if(AssignedToUser == "") { alert("Please select Assigned To User"); return;}
        if(chk_len <= 0) { alert("Please select atleast one record."); return;}

        var confirmText = "You are Un-assigning <b>"+chk_len+"</b> enquiries from <b>"+AssignedToUserText+"</b>";

        alertify.confirm(confirmText, function() 
        {
            var data = 'SessionKey='+SessionKey+'&AssignedTo='+AssignedToUser+'&'+$("form[name='bulk_assign_form']").serialize();

            $http.post(API_URL+'enquiry/addBulkUnAssignEnquiry', data, contentType).then(function(response) 
            {

                var response = response.data;

                if(response.ResponseCode==200)
                { /* success case */               

                    SuccessPopup(response.Message);                    

                    $timeout(function()
                    {                       

                       window.location.href = BASE_URL + "enquiry";

                   }, 1000);

                }
                else
                {

                    ErrorPopup(response.Message);

                }

                $scope.editDataLoading = false;          

            });

        }).set('labels', {
            ok: 'Confirm',
            cancel: 'Cancel'
        });

    }




}); 



/*function filterEnquiryList()
{
    angular.element(document.getElementById('content-body')).scope().getList(1);

    //angular.element(document.getElementById('content-body')).scope().data.noRecords = false;

    //angular.element(document.getElementById('content-body')).scope().data.listLoading = false;    
}*/


function filterEnquiryDate()
{
    if($("#filterFromDate").val() !="" && $("#filterToDate").val() !="")
    {
        filterEnquiryList();
    }
}

function checkEnquiryLength()
{
    var chk_len = $("#tabledivbody").find("input[name='EnquiryChk[]']:checked").length;
    var chk_total = $("#TotalRecordsEnquiry").val();
    var chk_status = $("#filterStatus").val();
    
    if(chk_total == null || chk_total == undefined || chk_total <= 0)
    {
        $("#BulkAssignEnquiry").attr("disabled", "disabled");
        $("#BulkUnAssignEnquiry").attr("disabled", "disabled");
    }
    else if(chk_len > 0 && chk_status == 2)
    {
        $("#BulkAssignEnquiry").attr("disabled", "disabled");
        $("#BulkUnAssignEnquiry").removeAttr("disabled");
    }
    else if(chk_len > 0 && chk_status == 1)
    {
        $("#BulkAssignEnquiry").removeAttr("disabled");
        $("#BulkUnAssignEnquiry").attr("disabled", "disabled");
    }
    else
    {
        $("#BulkAssignEnquiry").attr("disabled", "disabled");
        $("#BulkUnAssignEnquiry").attr("disabled", "disabled");
    }
    
}
                    




/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

            type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }
}
/*
function ParentcategoryCheck(CatType){
    if(CatType == 'Stream'){
        $("#SelectParentCategory").hide();
        $("#category-name-label").text("Stream Name");
        $("#select-label-parent-category").text("");
        $("#course_duration").hide();
    }else if(CatType == 'Course'){
        $("#SelectParentCategory").show();
        $("#category-name-label").text("Course Name");
        $("#select-label-parent-category").text("Select Stream");
        $("#course_duration").show();
        $("#course_duration").val("");
    }else if(CatType == 'Subject'){
        $("#category-name-label").text("Subject Name");
        $("#SelectParentCategory").show();
        $("#select-label-parent-category").text("Select Course");
        $("#course_duration").hide();
    }else{
       $("#category-name-label").text("Name");
       $("#SelectParentCategory").hide();
       $("#select-label-parent-category").text("Select Parent"); 
       $("#course_duration").hide();
    }
}*/
/* sortable - ends */

function search_records()
{
    angular.element(document.getElementById('content-body')).scope().getList(1);
}


function clear_search_records()
{    
    $("#filterStatus, #filterSource, #filterAssignedToUser, #filterInterestedIn, #filterFromDate, #filterToDate").val("");    

    search_records();
}
