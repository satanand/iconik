<h2 style="margin-bottom:20px; font-size: 18px; color: #000000; margin-top: 0;  line-height: 1.4;  font-weight: 400; ">Hi,</h2>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Welcome to <?php  echo SITE_NAME; ?>.</p>

<p style="margin-bottom:30px;font-family: 'Noto Sans', sans-serif; font-weight: 400;
font-size: 15px; ">Hello <?php echo $Name; ?>,<br> Your withdrawal request for <?php echo DEFAULT_CURRENCY.''.$Amount; ?> is approved by admin and will be transferred to your given account details within 3-4 working days. </p>