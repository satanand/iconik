app.controller('PageController', function ($scope, $http,$timeout){
    $scope.APIURL = API_URL + "upload/image";

    $scope.getFilterData = function (){
        $scope.getSecondLevelPermission();
        $scope.getList();
        $scope.getRoles();
        $scope.getStates();
        $scope.getCourse(); 
        $scope.getQualification();
        $scope.getIndustries();
        $scope.getDepartment();
        $scope.getUserData();
    }


    $scope.applyFilter = function (){
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
        $scope.getFilterData();       
    }


    $scope.remove_ownername_field  = function (key){
        $("#owner-row-"+key).remove();
    }

    /*list append*/



    $scope.getList = function ()
    {
        if ($scope.data.listLoading || $scope.data.noRecords) return;
        $scope.data.listLoading = true;
        var data = 'SessionKey='+SessionKey+'&UserGUID='+UserGUID+'&IsAdmin=Yes&Params=RegisteredOn,LastLoginDate,UserTypeName, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,,OwnerName,Achivements,ScholarshipTest,Classrooms,Library,TeachingAids,HostelFacility,NumberOfRooms,StudentPerRooms,MessFacility';
        $http.post(API_URL+'Users/getProfile', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ 

                $scope.UserData = response.Data;

                if($scope.UserData.TeachingAids && $scope.UserData.TeachingAids.length > 0){
                    $scope.UserData.TeachingAids = $scope.UserData.TeachingAids.split(",");
                }else{
                    $scope.UserData.TeachingAids = [];
                }

                if($scope.UserData.OwnerName && $scope.UserData.OwnerName.length > 0){
                    $scope.UserData.OwnerName = $scope.UserData.OwnerName.split(",");
                }else if($scope.UserData.OwnerName){
                    $scope.UserData.OwnerName.push($scope.UserData.OwnerName);
                }else{
                    $scope.UserData.OwnerName = [];
                }
                
               // alert($scope.UserData.StateName);

                //console.log($scope.UserData.StateName);

                $scope.getCities($scope.UserData.State_id);

            }else{
               $scope.data.noRecords = true;
            }
        $scope.data.listLoading = false;
        });
    }



    $scope.getRoles = function(){

        $scope.data.Roles = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'roles/getRoles', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {

                 $scope.data.Roles.push(response.Data.Records[i]);
             }
            
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

    }  


    $scope.getCourse = function(){

        $scope.data.Course = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'admin/staff/getCourse', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {

                 $scope.data.Course.push(response.Data.Records[i]);
             }
            

             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

      } 




      $scope.getSubject = function(CourseID,check){
        //alert(CourseID);

        
        if(check == "edit"){

         $scope.formData.Course_subjects = [];
        }else{
           $scope.data.Subject = [];
        }
        var data = 'SessionKey='+SessionKey+'&CategoryID='+CourseID;

        $http.post(API_URL+'admin/staff/getSubject', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {
                if(check == "edit"){

                 $scope.formData.Course_subjects.push(response.Data.Records[i]);
                }else{
                    $scope.data.Subject.push(response.Data.Records[i]);
                }
             }
            

             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

      }   


      $scope.getSubjectOnEdit = function(CourseID){
        //alert(CourseID);

        $scope.formData.Course_subjects = [];

        var data = 'SessionKey='+SessionKey+'&CategoryID='+CourseID;

        $http.post(API_URL+'admin/staff/getSubject', data, contentType).then(function(response) {

          var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

             for (var i in response.Data.Records) {

                 $scope.formData.Course_subjects.push(response.Data.Records[i]);
             }
            

             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);
         }

        });

      }
    /*state and city*/
    $scope.getStates = function(){

        var data = 'SessionKey='+SessionKey;

        $scope.data.states = [];

        $http.post(API_URL+'students/getStatesList', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.states = response.Data;

            }
             $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

        });

    }





    $scope.getCities = function(State_id){
     // alert(State_id);
       $scope.data.cities = [];
       //console.log(State_id);

        var data = 'SessionKey='+SessionKey+'&State_id='+State_id;
       
        $http.post(API_URL+'students/getCitiesList', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.cities.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }





    $scope.getCitiesByStateName = function(StateName){
        $scope.data.cities = [];
        var data = 'SessionKey='+SessionKey+'&StateName='+StateName;
        $http.post(API_URL+'students/getCitiesByStateName', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data){ /* success case */

                //$scope.data.cities = response.Data;

                for (var i in response.Data) {
                    $scope.data.cities.push(response.Data[i]);

                }

               // console.log($scope.data.cities);

            }

        });

    }
  $scope.getCitiesByStateNames = function(StateName){

        $scope.data.CitiesEmergency = [];
        var data = 'SessionKey='+SessionKey+'&StateName='+StateName;
        $http.post(API_URL+'students/getCitiesByStateName', data, contentType).then(function(response) {

            var response = response.data;

             if(response.ResponseCode==200 && response.Data){ /* success case */
              for (var i in response.Data) {
                     $scope.data.CitiesEmergency.push(response.Data[i]);
                }

                $timeout(function(){   
           
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

             }, 200);

            }

        });

    }


    $scope.getQualification = function(){
        $scope.data.qualification = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'qualification/getQualification', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.qualification.push(response.Data.Records[i]);
          }

          }

        });

    }
    $scope.getIndustries = function(){
        $scope.data.Industries = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'industry/getIndustry', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.Industries.push(response.Data.Records[i]);
          }

          }

        });

    } 
    $scope.getDepartment = function(){
        $scope.data.Department = [];
        var data = 'SessionKey='+SessionKey;
        $http.post(API_URL+'Department/getDepartment', data, contentType).then(function(response) {

            var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.Department.push(response.Data.Records[i]);
          }

          }

        });

    }

     /*get user NAme*/
    $scope.getUserData = function ()

    {

        var data = 'SessionKey='+SessionKey;
        $scope.data.UserData=[];
        $http.post(API_URL+'admin/staff', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

            
             for (var i in response.Data.Records) {

                 $scope.data.UserData.push(response.Data.Records[i]);
             }

          //console.log($scope.data.UserData);

            $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 200);         
         }

     });


    }

    /*load add form*/



    $scope.loadFormAdd = function (Position, CategoryGUID)
    {

       $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
         $('#add_model').modal({show:true});
            $timeout(function(){            
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
            }, 200);

    }

     /*load edit form*/



    $scope.loadFormEdit = function(UserID,UserGUID) {
        $scope.templateURLEdit = PATH_TEMPLATE + module + '/edit_form.htm?' + Math.random();
        $scope.data.pageLoading = true;
        $('#edit_model').modal({show: true});        
        $scope.data.pageLoading = false;
        $timeout(function(){
            $("#BirthDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});            
            $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
        }, 700);
        //  //$scope.getFilterData();
        // $http.post(API_URL + 'users/getProfile', 'SessionKey=' + SessionKey + '&UserGUID=' + UserGUID + '&UserID=' + UserID + '&Params=Status, FullName, Email, Username, ProfilePic, Gender, BirthDate, PhoneNumber, Status, StatusID,StateName,CityName,Address,Postal,PhoneNumberForChange,Website,FacebookURL,TwitterURL,GoogleURL,LinkedInURL,RegistrationNumber,CityCode,TelePhoneNumber,UrlType,UserTypeName', contentType).then(function(response) {
        //     var response = response.data;
        //     if (response.ResponseCode == 200) { /* success case */
        //        $scope.data.pageLoading = false;
        //         $scope.formData = response.Data;
        //         $('#edit_model').modal({show: true});
        //         $timeout(function() {
        //             $("#BirthDate,#To,#From").datetimepicker({format: 'dd/mm/yyyy',minView: 2});
        //             $(".chosen-select").chosen({
        //                 width: '100%',
        //                 "disable_search_threshold": 8,
        //                 "placeholder_text_multiple": "Please Select",
        //             }).trigger("chosen:updated");

        //         }, 600);

        //     }
        // });
    }

    /*load staff edit form*/



    $scope.loadFormEditStaff = function(UserID,UserGUID) {

 
        $scope.templateURLSaffEdit = PATH_TEMPLATE+module+'/staffedit_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        //$scope.getRoles();
        $http.post(API_URL+'admin/staff/getProfile','SessionKey='+SessionKey+'&UserID='+UserID+'&Params=Status,ProfilePic', contentType).then(function(response) {

                console.log( response.Data);
            var response = response.data;

            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;

                

                $scope.formData = response.Data;

                console.log($scope.formData.TeachingAids);
              //  alert($scope.formData.MaritalStatus)
                if($scope.formData.AdminAccess == 'No'){
                    $scope.showDivs = true;
                }else{
                     $scope.showDivs = false;
                }

                //alert($scope.showDiv);
  
                $scope.getCities($scope.formData.State_id);
                $scope.getCitiesByStateNames($scope.formData.EmergencyState);
                $scope.getCitiesByStateName($scope.formData.NomineeState);

                $scope.StateID=$scope.formData.State_id;

                $scope.NomineeState=$scope.formData.NomineeState;
                $scope.EmergencyState=$scope.formData.EmergencyState;

                $scope.Qualification=$scope.formData.Qualification;

                if($scope.formData.MaritalStatus.length > 0){
                    $scope.MaritalStatus=$scope.formData.MaritalStatus;
                }else{
                   $scope.MaritalStatus='';
                }
                

                //$scope.getSubject($scope.formData.CourseID);

                $scope.CourseID=$scope.formData.CourseID.split(",");

                $scope.SubjectIDA=$scope.formData.SubjectID.split(",");

                $scope.UserTypeID=$scope.formData.UserTypeID;

                $('#staffedit_model').modal({show:true});

                $timeout(function(){  
   
           $("#BirthDate,#JoiningDate,#AppraisalDate,#AnniversaryDate,#AnniversaryDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});
           $("#Form").datetimepicker({format: 'dd-mm-yyyy',minView: 2,onSelect: function(selected) {

                  $("#To").datepicker("option","minDate", selected) }});

                $("#To").datetimepicker({format: 'dd-mm-yyyy',minView: 2,onSelect: function(selected) {

                       $("#Form").datepicker("option","maxDate", selected)

                    }
                });
                $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
             }, 1200);

           }

        });
    }

  /*add data*/
    $scope.addData = function ()
    {

        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'admin/users/add', data, contentType).then(function(response) {
            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                 $scope.applyFilter();
                 $('.modal-header .close').click();
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });

    }


    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getFilterData();
    }  



        /*edit data*/
   $scope.editData = function ()

    {

        $scope.editDataLoading = true;

        $scope.loadeSubmit = true;

        var data = 'SessionKey='+SessionKey+'&UserGUID='+$scope.UserData.UserGUID+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'admin/users/updateUserProfile', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

             $scope.applyFilter();

                $scope.data.dataList[$scope.data] = response.Data;

                $timeout(function(){

                   var MediaName = $("#MediaName").val(); 

                   if(MediaName != "")
                   {
                        MediaName = response.Data.ProfilePic;

                        $http.post(BASE_URL + 'profile/changeProfilePic', 'MediaName='+MediaName, contentType).then(function(response) 
                        {
                            window.location.href = window.location.href
                        });
                   }           

                   $('.modal-header .close').click();

               }, 200);

            }else{
                ErrorPopup(response.Message);

            }

            $scope.loadeSubmit = false;


            $scope.editDataLoading = false;          

        });

    }  

        /*edit data*/
   $scope.staffeditData = function ()

    {
         $scope.loadeSubmit = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='staffedit_form']").serialize();
        $http.post(API_URL+'admin/staff/updateUserInfo', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;

                $('#staffedit_model .close').click();
            }else{
                ErrorPopup(response.Message);

            }

            $scope.loadeSubmit = false;          
        });
    }

}); 



function getCalendar(){
   
    $("#AnniversaryDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});
                
}
function getCalendarForm(){
     var d = new Date();
    $(".Form").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setEndDate',d);
                
}
function getCalendarTo(){
     var d = $('.Form').val();
     var end = new Date();
    $(".To").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d).datetimepicker('setEndDate',end);
                
}

function getCalendarDOB(){
    var d = new Date();
    $(".BirthDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setEndDate',d);
                
}
function getAppraisalDate(){
    var d = new Date();
    $(".AppraisalDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d);
                
}
function getJoiningDate(){
    var d = new Date();
    $(".JoiningDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2});
                
}

/*check duplicate entry for form input values*/
function find_duplicate_in_array(title_name) {
    var object = {};
    var result = [];

    var arra1 = $("."+title_name)
            .map(function() {
                if($(this).val()){
                 return $(this).val();
                }
            }).get();

    arra1.forEach(function (item) {
      if(!object[item])
          object[item] = 0;
        object[item] += 1;
    })

    for (var prop in object) {
       if(object[prop] >= 2) {
           result.push(prop);
       }
    }
    return result;
}

/*check empty inputs and duplicate titles*/
function check_empty_input(ClsName) {
    var check = true;
    var ClsData = $("."+ClsName)
            .map(function() {
                return $(this).val();
            }).get();

    var duplicate_vals = find_duplicate_in_array(ClsName);

    if(duplicate_vals.length > 0){
        alert("Duplicate entries are found for Owner Name! Please remove duplicate entries before adding new.");
        check = false;
        return false;
    }else{      
        $.each(ClsData, function(index, value) {
            if (value == "") {
                alert("Please fill previously blank fields before adding new.");
                 check = false;
                return false;
            } 
        });
    }
    return check;
}

/* Add More JavaScript Start Here */
$(document).ready(function() {/*
    $("body").on("click", ".owner-add-more", function() {
    var check = check_empty_input('owner_name');
    if (check) {
            var numItems = $('.owner_name').length;
            if(numItems < 5){
                var count = numItems + 1;                
                
                var cls = ""; 
                if(count == 2) { cls = "margin-top:25px;";  }

                $(".section2-add-more-owner").last().html('<div class="form-group change" ng-hide="$last"> <div class="form-group change"><a class="btn btn-danger" onclick="remove_ownername_field(\''+numItems+'\')" style="min-width: 60px; margin-left: 20px; '+cls+'">-</a></div></div>');                

                $(".progressBar-doc").remove();
                var html = $("#owner_name").append('<div class="form-group" id="owner-row-'+count+'" style="float:left;width:100%;"><div style="width: 85%; float: left;"><input type="text"  class="form-control owner_name" name="OwnerName[]"></div><div class="section2-add-more-owner"><div class="form-group change"><a class="btn btn-success owner-add-more"  style="min-width: 60px; margin-left: 20px;"><i class="fa fa-plus-circle"></i></a></div></div></div>');
            }else{
                alert("Sorry! you can add max 5 names.");
            }
           
        }
    });
*/});



function remove_ownername_field(key) {
    $("#owner-row-"+key).remove();
}














