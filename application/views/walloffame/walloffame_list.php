<style type="text/css">
   .glyphicon {
   position: relative;
   top: -4px;
   display: inline-block;
   font-family: 'Glyphicons Halflings';
   font-style: normal;
   font-weight: normal;
   line-height: 1;
   -webkit-font-smoothing: antialiased;
   font-size: 20px;
   padding: 4px;
   }
</style>
<header class="panel-heading">
   <h1 class="h4">Manage Wall Of Fame</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController"  id="content-body" ng-init="getFilterData()">
<!-- Body -->
<!-- <header class="panel-heading"> <h2 class="h4">Manage Wall Of Fame</h2> </header>
<?php $this->load->view('includes/breadcrumb'); ?> -->
<!-- Top container -->
<div class="clearfix mt-2 mb-2">
   <div class="row">
      <div class="col-md-8">
         <span class="h5">Total Records: {{data.totalRecords}}</span>
      </div>

      <div class="col-md-4">
         <div class="float-right">
            <!-- ng-if="filterData.CategoryTypes.length>1" -->
            <button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Entry</button>
         </div>
      </div>
   </div>
   


   <div class="row">
      <!-- <label for="inputName" class="control-label mb-10">Select Course</label> -->
      <div class="col-md-3">
         <input type="text" name="Year" id="Year" class="form-control Year" placeholder="Select Year" readonly>
      </div>


      <div class="col-md-3">
         <form class="form-inline" id="filterForm" role="form" autocomplete="off">
            <div id="universal_search">
                <input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Type and search" style="" autocomplete="off">                
            </div>
         </form>
      </div>


      <div class="col-md-3">
         <div class="form-group">
            <button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
            <button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
         </div>
      </div>

      
   </div>
</div>
<!-- Top container/ -->
<!-- Data table -->
<div class="table-responsive block_pad_md" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0">
   <!-- loading -->
   <p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
   <!-- data table -->
   <table class="table table-striped table-condensed table-hover table-sortable">
      <!-- table heading -->
      <thead>
         <tr>
            <th  style="width: 200px;">Title/Name</th>
            <th  style="width: 100px;">Year</th>
            <!-- <th>Content</th> -->
            <th style="width: 200px;">Images</th>
            <th style="width: 150px;" class="text-center">Action</th>
         </tr>
      </thead>
      <!-- table body -->
      <tbody id="tabledivbody">
         <tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.MenuOrder}}.{{row.UserTypeID}}">
            <td>
               <strong>{{row.MediaCaption}}<br>{{row.FullName}}</strong>
            </td>
            <td>
               <strong>{{row.Year}}</strong>
            </td>
            <!-- <td ng-bind-html="row.MediaContent">
            </td> -->
            <td>
               <img src="{{row.MediaID.Records[0].MediaThumbURL}}" style="height: 100px;width: 150px;margin-bottom: 10px;">
            </td>
            <td class="text-center" style="text-align: center;">
               <a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.MediaGUID)" data-toggle="tooltip" title="View"></a>
               <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.MediaGUID)" data-toggle="tooltip" title="Edit"></a>
               <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormWallDelete(key, row.MediaGUID)" data-toggle="tooltip" title="Delete"></a>
            </td>
         </tr>
      </tbody>
   </table>
   <!-- no record -->
   <p class="no-records text-center" ng-if="data.noRecords">
      <span ng-if="data.dataList.length">No more records found.</span>
      <span ng-if="!data.dataList.length">No records found.</span>
   </p>
</div>
<!-- Data table/ -->
<!-- add Modal -->
<div class="modal fade" id="add_model">
   <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title h5">Add Entry</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <!-- form -->
         <form id="add_form" name="add_form" autocomplete="off" ng-include="templateURLAdd">
         </form>
         <!-- /form -->
      </div>
   </div>
</div>
<!-- edit Modal -->
<div class="modal fade" id="edits_model">
   <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title h5">Edit Entry</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <!-- form -->
         <form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLEdit">
         </form>
         <!-- /form -->
      </div>
   </div>
   <!-- issunebook_model form -->
</div>
<!-- view Modal -->
<div class="modal fade" id="view_model">
   <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h3 class="modal-title h5">View Entry</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         </div>
         <!-- form -->
         <form id="view_form" name="view_form" autocomplete="off" ng-include="templateURLView">
         </form>
         <!-- /form -->
      </div>
   </div>
</div>
<!-- Body/ -->
</div>