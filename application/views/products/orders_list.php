<style type="text/css">
.glyphicon 
{
    position: relative;
    top: -4px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    font-size: 20px;
    padding: 4px;
}
</style>  
<header class="panel-heading"> <h1 class="h4">Manage Orders</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getOrdersList(1);" id="content-body"><!-- Body -->
	<!-- Top container -->

	<div class="clearfix mt-2 mb-2">

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<span class="h5">Total Orders: {{data.dataList.length}}</span>
				</div>
			</div>			
		</div>


		<form id="filterForm" role="form" autocomplete="off">
		<div class="row">		
		

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterStartDate" type="text" placeholder="Order From Date" id="FilterStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterEndDate" type="text" placeholder="Order To Date" id="FilterEndDate" class="form-control">
			</div>
		</div>


		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterPayoutStartDate" type="text" placeholder="Payout From Date" id="FilterPayoutStartDate" class="form-control">
			</div>
		</div>

		<div class="col-md-2">
			<div class="form-group">
	            <input name="FilterPayoutEndDate" type="text" placeholder="Payout To Date" id="FilterPayoutEndDate" class="form-control">
			</div>
		</div>
				

		<div class="col-md-3">
			<div class="form-group">			 	
	        	<input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Search by word" >
	    	</div>
		</div>

		
		<div class="col-md-3">
			<div class="form-group">
				<button class="btn btn-primary btn-sm ml-1" name="search_btn" ng-click="getOrdersList(1);"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_order_filters();"><?php echo CLEAR_SEARCH_BTN;?></button>
			</div>
		</div>		

	</div>
	</form>		

	</div>

	
 
	<!-- Data table -->

	<div class="table-responsive block_pad_md" infinite-scroll="getOrdersList(0);" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading --> 

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



<table class="table table-striped table-condensed table-hover table-sortable">
<thead>
<tr>
	<th style="min-width: 70px;">Order ID</th>
    <th style="min-width: 100px;">Order Date</th>
    <th style="min-width: 70px;">Order<br/>Status</th>    
    <th style="min-width: 100px;">Customer Name</th>
    <th style="min-width: 100px;">Delivery Details</th>   
    <th style="min-width: 120px;">Amount</th>    
    <th style="min-width: 130px;">Payable Amount</th>    
    <th style="min-width: 130px;">Payment Date</th>
    <th style="min-width: 130px;">Payout Received<br/>Date</th>
    <th style="min-width: 130px;">Remarks</th>
    <th style="min-width: 130px;">Action</th>
</tr>

</thead>

<tbody id="tabledivbody">

<tr scope="row" ng-repeat="(key, row) in data.dataList" id="sectionsid_{{row.OrderID}}">

	<td>{{row.OrderID}}</td>
	
	<td>{{row.OrderDate | date}}</td>	

	<td>{{row.OrderStatus}}</td>

	<td>
		{{row.CustomerName}}<br/>
		<label style="font-size: 10px;">{{row.CustomerEmail}}/{{row.CustomerMobile}}</label>
	</td>

	<td>
		{{row.BillingAddress}}<br/>
		<label style="font-size: 10px;">{{row.BillingCity}}, {{row.BillingState}}, {{row.BillingZipCode}}</label>
	</td>	

	<td>{{row.Payouts.NetAmountSum}}</td>	

	<td>{{row.Payouts.PayableAmount}}</td>	

	<td>
		<label ng-if="row.OrderStatus == 'success'">{{row.PaymentDate}}</label>
	</td>

	<td>{{row.PayoutDate}}</td>

	<td>
		<label ng-bind-html="row.PayoutDesc"></label>
	</td>

	<td>
		<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadViewOrder(row.OrderID);" data-toggle="tooltip" title="View"></a>

		<a ng-if="row.OrderStatus == 'success'" class="glyphicon glyphicon-file" href="" ng-click="generateInvoice(row.OrderID);" data-toggle="tooltip" title="Generate Invoice"></a>
	</td>

</tr>
</tbody>

</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="data.noRecords">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->



	
<!-- View Modal -->
<div class="modal fade" id="view_order_model">
<div class="modal-dialog modal-lg" role="document" style="max-width: 90% !important;">
<div class="modal-content">
<div class="modal-header">
	<h3 class="modal-title h5">View Order Details</h3>     	
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

<div class="row">
<div class="col-md-12">
<table class="table table-striped table-condensed table-hover table-sortable">
<thead>
<tr>
	<th>Order ID</th>
    <th>Order Date</th>
    <th>Order Status</th>    
    <th>Customer Name</th>
    <th>Product Name</th>    
    <th>Unit Price</th>
    <th>Ordered Qty</th>
    <th>Amount</th>
    <th>Discount Amount</th>
    <th>Amount After Discount</th>
    <th>Tax Amount</th>
    <th>Shipping Cost</th>
    <th>Net Amount A</th>
</tr>

</thead>

<tbody id="tabledivbody">

<tr scope="row" ng-repeat="(key, row) in data.OrderDetails" id="sectionsid_{{row.OrderID}}">

	<td>{{row.OrderID}}</td>
	
	<td>{{row.OrderDate | date}}</td>	

	<td>{{row.OrderStatus}}</td>

	<td>{{row.CustomerName}}</td>

	<td>{{row.ProductName}}</td> 

	<td>{{row.ProductUnitPrice}}</td>

	<td>{{row.OrderedQty}}</td>

	<td>{{row.Amount}}</td>	

	<td>{{row.DiscountAmount}}</td>

	<td><b>{{row.AmountAfterDiscount}}</b></td>

	<td><b>{{row.TaxAmount}}</b></td>

	<td><b>{{row.ShippingCost}}</b></td>

	<td><b>{{row.NetAmount}}</b></td>

</tr>

<tr ng-if="OrderDetailsLoading">
    <td colspan="13">
        <img src="./asset/img/ajax-loader.gif" class="processingLoader">
    </td>
</tr>

<tr ng-if="data.OrderDetails.length <= 0 && !OrderDetailsLoading">
  <td colspan="13">No record found.</td>
</tr> 
</tbody>

</table>
</div>
</div>
</div>
</div>
</div>



</div><!-- Body/ -->


