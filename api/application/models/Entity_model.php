<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Entity_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}
	


	/*
	Description: 	Use to update order of any entity
	*/
	function setOrder($sectionsid = array()){
		if(!empty($sectionsid)){
			$Order = 1;
			foreach($sectionsid as $Data){
				$Data = explode(".",$Data);
				$this->db->where('EntityID', $Data[1]);
				$this->db->limit(1);
				$this->db->update('tbl_entity', array("MenuOrder"=>$Order));
				$Order++;
			}
		}
	}




	/*
	Description: 	Use to add new entity to system.
	*/
	function addEntity($EntityGUID, $Input = array()){
		$InsertData = array_filter(array(
			"EntityGUID" 		=>	$EntityGUID,
			"EntityTypeID"		=>	$Input['EntityTypeID'],
			"CreatedByUserID" 	=>	@$Input['UserID'],
			"EntryDate" 		=>	date("Y-m-d H:i:s"),
			"StatusID"			=>	$Input['StatusID']
		));
		$this->db->insert('tbl_entity', $InsertData);
		$EntityID = $this->db->insert_id();
		/*add event attributes*/
		$this->addEntityAttributes($EntityID,@$Input['Attributes']);
		return $EntityID;
	}

		/*
	Description: 	Use to add new entity to system.
	*/
	function addEntitys($EntityGUID, $Input = array()){
		$InsertData = array_filter(array(
			"EntityGUID" 		=>	$EntityGUID,
			"EntityTypeID"		=>	$Input['EntityTypeID'],
			"CreatedByUserID" 	=>	@$Input['UserID'],
			"InstituteID" 	    =>	@$Input['InstituteID'],
			"EntryDate" 		=>	date("Y-m-d H:i:s"),
			"StatusID"			=>	$Input['StatusID']
		));
		$this->db->insert('tbl_entity', $InsertData);
		$EntityID = $this->db->insert_id();
		/*add event attributes*/
		$this->addEntityAttributes($EntityID,@$Input['Attributes']);
		return $EntityID;
	}

	/*
	Description: 	Use to update user profile info.
	*/
	function updateEntityInfo($EntityID, $Input=array()){
		$UpdateArray = array_filter(array(
			"StatusID" 			=>	@$Input['StatusID'],
			"InstituteID" 			=>	@$Input['InstituteID'],						
			"ModifiedDate"		=>	date("Y-m-d H:i:s")
		));

		if(!empty($UpdateArray)){
			/* Update entity Data. */
			$this->db->where('EntityID', $EntityID);
			$this->db->limit(1);
			$this->db->update('tbl_entity', $UpdateArray);
		}

		/*add event attributes*/
		$this->addEntityAttributes($EntityID,@$Input['Attributes']);
		return TRUE;
	}


	/*
	Description: 	Use to update master institute or created by instiute.
	*/
	function updateMasterInstitute($EntityID, $Input=array()){
		$UpdateArray = array_filter(array(
			"InstituteID" 			=>	@$Input['InstituteID'],						
			"ModifiedDate"		=>	date("Y-m-d H:i:s")
		));

		if(!empty($UpdateArray)){
			/* Update entity Data. */
			$this->db->where('EntityID', $EntityID);
			$this->db->limit(1);
			$this->db->update('tbl_entity', $UpdateArray);
		}

		//echo $this->db->last_query(); die;

		return TRUE;
	}





	/*
	Description: 	Use to add attributed of entity.
	*/
	function addEntityAttributes($EntityID, $Attributes){

		if(empty($Attributes)){
			return false;
		}		
		/*Remove old attributes - starts*/
		$this->db->where(array("EntityID"=>$EntityID));
		$this->db->delete('tbl_entity_attributes');
		/*Remove old attributes - ends*/

		foreach($Attributes as $Value){
			if(!empty($Value['AttributeGUID'])){
				/* get To Entity Data */
				$EntityData=$this->getEntity("E.EntityID", array("EntityGUID"=>$Value['AttributeGUID']));
			}

			$InsertData[] = array(
				"EntityID" 			=>	$EntityID,
				"AttributeID"		=>	(!empty($EntityData) ? $EntityData['EntityID'] : ''),		
				"AttributeName"		=>	$Value['AttributeName'],
				"AttributeValue" 	=>	$Value['AttributeValue']
			);
		}


		if(!empty($InsertData)){
			$this->db->insert_batch('tbl_entity_attributes', $InsertData); 	
		}
	}



	function getEntityAttributes($EntityID){
		$this->db->select("AttributeName, AttributeValue");
		$this->db->from('tbl_entity_attributes');
		$this->db->where("EntityID",$EntityID);			
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			$Return = $Query->result_array();
			return $Return;
		}
		return array();	
	}






	/*
	Description: 	Use to delete
	*/
	function deleteEntity($EntityID=''){
		if(empty($EntityID)){
			return true;
		}
		$this->db->where(array("EntityID"=>$EntityID));
		$this->db->delete('tbl_entity');
		$this->db->limit(1);
		return TRUE;
	}


	/*
	Description: 	get flagged inappropriate content
	*/
	function getFlagged($Field='*', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=5){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */

		$this->db->select($Field);
		$this->db->select('EF.ToEntityID ToEntityIDForUse, ET.EntityTypeID EntityTypeIDForUse');
		$this->db->select('
			CASE EF.StatusID
			when "1" then "Pending"
			when "2" then "Verified"
			when "3" then "Deleted"
			when "4" then "Blocked"
			END as Status', false);

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_entity_type ET');
		$this->db->where("E.EntityTypeID","ET.EntityTypeID", FALSE);

		$this->db->from('tbl_action EF');
		$this->db->where("EF.ToEntityID", "E.EntityID", FALSE);

		/*Join reported by user*/
		$this->db->from('tbl_users U');
		$this->db->where("EF.EntityID", "U.UserID", FALSE);		


		if(!empty($Where['EntityID'])){
			$this->db->where("E.EntityID",$Where['EntityID']);
		}	


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$this->db->order_by('EF.EntryDate','ASC');
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){


				if(!$multiRecords && $Record['EntityTypeIDForUse']=='5'){
					$this->load->model('Post_model');
					$Record['Content']=$this->Post_model->getPosts('
						E.EntryDate,CONCAT_WS(" ",U.FirstName,U.LastName) FullName,
						IF(U.ProfilePic = "","",CONCAT("'.PROFILE_PICTURE_URL.'",U.ProfilePic)) AS ProfilePic,
						E.EntityGUID,
						P.PostContent Content,
						P.Caption Title,
						',
						array('PostID' => $Record['ToEntityIDForUse']));
				}

				unset($Record['EntityTypeIDForUse']);
				unset($Record['ToEntityIDForUse']);
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}






	/*
	Description: 	Use to get single entity data.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getEntity($Field='E.EntityGUID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */

		$this->db->select($Field);
		$this->db->from('tbl_entity E');
		$this->db->from('tbl_entity_type ET');
		$this->db->where("E.EntityTypeID","ET.EntityTypeID", FALSE);

		if(!empty($Where['CreatedByUserID'])){
			$this->db->where("E.CreatedByUserID",$Where['CreatedByUserID']);
		}

		if(!empty($Where['EntityID'])){
			$this->db->where("E.EntityID",$Where['EntityID']);
		}
		if(!empty($Where['EntityGUID'])){
			$this->db->where("E.EntityGUID",$Where['EntityGUID']);
		}

		if(!empty($Where['EntityTypeName'])){
			$this->db->where("ET.EntityTypeName",$Where['EntityTypeName']);
		}

		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){
			if($multiRecords){
				$Return['Data']['Records'] = $Query->result_array();
				return $Return;	
			}else{
				return $Query->row_array();
			}
		}
		return FALSE;		
	}




	
	/*
	Description: 	Use to add entity view count
	*/
	function addViewCount($UserID, $EntityID){
		$this->db->select('1');
		$this->db->from('tbl_entity_views');
		$this->db->where(array('UserID' => $UserID, 'EntityID' => $EntityID));
		$this->db->limit(1);
		$Query = $this->db->get();	
		if($Query->num_rows()==0){
			/* Add to entity views table if not already exist */
			$this->db->insert('tbl_entity_views', array("UserID" =>	$UserID,"EntityID" => $EntityID));
			/* Update entity view count */
			$this->db->set('ViewCount', 'ViewCount+1', FALSE);
			$this->db->where('EntityID', $EntityID);
			$this->db->limit(1);
			$this->db->update('tbl_entity');
		}

	}

	function getCreatedByEntityID($EntityID)
	{
		$query = $this->db->query("select CreatedByUserID from tbl_entity where  EntityID= ".$EntityID." and EntityTypeID = 1");
          //echo $this->db->last_query(); die();
		  $EntityID= $query->row_array();

		if(!empty($EntityID)){
			return $EntityID;
		}

	}


	function getUserRelatedEntityList($EntityID)
	{
		$query = $this->db->query("select EntityID from tbl_entity where  EntityID= ".$EntityID." and EntityTypeID = 1 UNION DISTINCT select EntityID from tbl_entity where CreatedByUserID =(select CreatedByUserID from tbl_entity where  EntityID= ".$EntityID." and EntityTypeID = 1) UNION DISTINCT select EntityID from tbl_entity where CreatedByUserID =".$EntityID." and EntityTypeID = 1");

		$EntityID = $query->result_array();
         //print_r($EntityID);
		if(!empty($EntityID)){

			return $EntityID;
		}
	}


}

