app.controller('PageController', function ($scope, $http,$timeout){



    $scope.data.pageSize = 100;



    $scope.data.dataLists = [];



    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    /*----------------*/



    $scope.getFilterData = function ()
    {


        $scope.getSecondLevelPermission();
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();



        $http.post(API_URL+'admin/roles/getFilterData', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200 && response.Data){ /* success case */



             $scope.filterData =  response.Data;



             $timeout(function(){



                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");



            }, 300);          



         }



     });



    }


    /*list*/



    $scope.applyFilter = function ()



    {



        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/



        $scope.getList();



    }











    /*list append*/



    $scope.getList = function (check="")
    {
        if(check!=1){
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }else{
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
       
       $scope.data.listLoading = true;

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        $http.post(API_URL+'roles/getRoles', data, contentType).then(function(response) {

           var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.dataList.push(response.Data.Records[i]);
          }

          $scope.data.pageNo++;               

         }else{

            $scope.data.noRecords = true;

        }



        $scope.data.listLoading = false;



        setTimeout(function(){ tblsort(); }, 1000);



    });



    }











    /*load add form*/



    $scope.loadFormAdd = function (Position, CategoryGUID)
   {

       $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

      $('#add_model').modal({show:true});

      $timeout(function(){   

        tinymce.init({
          selector: '#Description'
        });  
         

         $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

     }, 700);


    }





    /*load edit form*/



    $scope.loadFormEdit = function (Position, UserTypeID)
   {

     $scope.data.Position = Position;

       $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();

        $scope.data.pageLoading = true;
        $http.post(API_URL+'roles/getEDRoles', 'SessionKey='+SessionKey+'&UserTypeID='+UserTypeID, contentType).then(function(response) {
           var response = response.data;
        if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#edit_model').modal({show:true});
                $timeout(function(){            
                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
               }, 700);
            }

        });
    }



    $scope.loadFormView = function (Position, UserTypeID)
    {

       $scope.data.Position = Position;

       $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;
        $http.post(API_URL+'roles/getEDRoles', 'SessionKey='+SessionKey+'&UserTypeID='+UserTypeID, contentType).then(function(response) {
           var response = response.data;
        if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data
                $('#view_model').modal({show:true});
                $timeout(function(){            
                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
               }, 700);
            }

        });
    }

    /*load permission form*/

    $scope.loadFormPermission = function (Position, UserTypeID)
    {

       $scope.templateURLPermission = PATH_TEMPLATE+module+'/permission_form.htm?'+Math.random();
       $scope.data.pageLoading = true;

        $scope.data.UserTypeID = UserTypeID;

        //$http.post(API_URL+'roles/getmodules', data, contentType).then(function(response) {

        $http.post(API_URL+'roles/getmodules', 'SessionKey='+SessionKey+'&UserTypeID='+UserTypeID, contentType).then(function(response) {
           var response = response.data;
           if(response.ResponseCode==200){ /* success case */

               $scope.data.pageLoading = false;

                $scope.data.dataLists=[];
                // if(response.Data.type){
                //   $scope.PermissionType = response.Data.type;
                // }else{
                //   $scope.PermissionType = "All";
                // }
               for (var j in response.Data.Records){

                    //console.log(response.Data.Records);
                    //alert(response.Data.type);
                   
                    
                 $scope.data.dataLists.push(response.Data.Records[j]);

                }

                $('#permission_model').modal({show:true});

                $timeout(function(){            

                  $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

              }, 700);

            }



        });



    }

    /*add data*/



    $scope.addData = function ()
    {
        tinyMCE.triggerSave();
        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'roles/add', data, contentType).then(function(response) {
            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                 $scope.getList(1);
                $timeout(function(){            
                   $('#add_model .close').click();

               }, 200);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });

    }





    $scope.loadFormDelete = function (Position, UserTypeID)
    {
        $scope.data.Position = Position;
        $scope.listLoading = true;
        $scope.UserTypeID = UserTypeID;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'roles/deleteRole', 'SessionKey='+SessionKey+'&UserTypeID='+UserTypeID, contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    //$('.modal-header .close').click();
                    $scope.listLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.listLoading = false;
    }





    /*edit data*/



    $scope.editData = function ()



    {



        $scope.editDataLoading = true;



        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();



        $http.post(API_URL+'roles/editRoles', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */               



              SuccessPopup(response.Message);



                $scope.data.dataList[$scope.data.Position] = response.Data;

                

                $('.modal-header .close').click();



            }else{



                ErrorPopup(response.Message);



            }



            $scope.editDataLoading = false;          



        });



    }  /*PermissionData data*/



    $scope.PermissionData = function ()



    {



        $scope.PermissionDataLoading = true;



        var data = 'SessionKey='+SessionKey+'&'+$("form[name='permission_form']").serialize();

        //console.log(data);

        $http.post(API_URL+'roles/PermissionRoles', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */               



              SuccessPopup(response.Message);



                $scope.data.dataList[$scope.data.Position] = response.Data;

                

                $('#permission_model .close').click();



            }else{



                ErrorPopup(response.Message);



            }



            $scope.PermissionDataLoading = false;          



        });



    }


}); 



/* sortable - starts */



/* sortable - starts */



function tblsort() {







  var fixHelper = function(e, ui) {



    ui.children().each(function() {



        $(this).width($(this).width());



    });



    return ui;



}







$(".table-sortable tbody").sortable({



    placeholder: 'tr_placeholder',



    helper: fixHelper,



    cursor: "move",



    tolerance: 'pointer',



    axis: 'y',



    dropOnEmpty: false,



    update: function (event, ui) {



      sendOrderToServer();



  }      



}).disableSelection();



$(".table-sortable thead").disableSelection();











function sendOrderToServer() {



    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");



    $.ajax({



        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',



        data: order,



        stop: function(response) {



            if (response.status == "success") {



                window.location.href = window.location.href;



            } else {



                alert('Some error occurred');



            }



        }



    });



  }
}


function CheckPermission(PermissionType) {
  if(PermissionType == "All"){
     $('.module').prop('checked', true);
     $('.all').prop('checked', true);
     $('.view').prop('checked', false);
  }else if(PermissionType == "View"){
    $('.module').prop('checked', true);
    $('.all').prop('checked', false);
    $('.view').prop('checked', true);
  }else{
    /*$('.module').prop('checked', false);
    $('.all').prop('checked', false);
    $('.view').prop('checked', false);*/
  }
}

function CheckOnce(type, checkID){
  if(type == 'all'){
    if($('#all'+checkID).prop("checked") == true){
      $('#view'+checkID).prop('checked', false);
    }
  }else{
    if($('#view'+checkID).prop("checked") == true){
      $('#all'+checkID).prop('checked', false);
    }
  }
}
/* sortable - ends */



function search_records()
{
    angular.element(document.getElementById('panel-body')).scope().applyFilter(); 
}

function clear_search_records()
{    
    $("#Keyword").val("");    

    search_records();
}