app.controller('PageController', function ($scope, $http,$timeout){



    $scope.data.pageSize = 100;



    $scope.data.dataLists = [];



    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    /*----------------*/


    /*list*/

    $scope.getFilterData = function(Category = "") {
        $scope.getSecondLevelPermission();
    }

    $scope.applyFilter = function ()
    {
      $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
      $scope.getList();
    }



    /*list append*/



    $scope.getList = function ()
    {

       if ($scope.data.listLoading || $scope.data.noRecords) return;
       $scope.data.listLoading = true;

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        $http.post(API_URL+'industry/getIndustry', data, contentType).then(function(response) {

           var response = response.data;

           if(response.ResponseCode==200 && response.Data.Records){ /* success case */

            $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                 $scope.data.dataList.push(response.Data.Records[i]);
          }

          $scope.data.pageNo++;               

         }else{

            $scope.data.noRecords = true;

        }



        $scope.data.listLoading = false;



        setTimeout(function(){ tblsort(); }, 1000);



    });



    }


    /*load add form*/



    $scope.loadFormAdd = function (Position, CategoryGUID)
   {

      tinymce.remove('#Description');
       $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

        $scope.listLoading = true;

        $('#add_model').modal({show:true});



        $timeout(function(){    

            tinymce.init({
              selector: '#Description'
            });  

           //$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 700);

       $scope.listLoading = false;
    }





    /*load edit form*/



    $scope.loadFormEdit = function (Position, industryID)
   {

     $scope.data.Position = Position;
      $scope.listLoading = true;
      $scope.IndustryID = industryID;
       $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();
       $('#edit_model').modal({show:true});
        $scope.data.pageLoading = true;
        $scope.formDatas = [];
        $http.post(API_URL+'industry/getIndustry', 'SessionKey='+SessionKey+'&industryID='+industryID, contentType).then(function(response) {
           var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;
                //alert(response.Data.Records[0]);
                $scope.formDatas = response.Data.Records[0];
               // console.log($scope.formData);  
                $scope.listLoading = false;
               $timeout(function(){    

                    tinymce.init({
                      selector: '#Description'
                    });  

                   //$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 700);
            }
             $scope.listLoading = false;
        });
    }

    /*load permission form*/



    $scope.loadFormView = function (Position, industryID)
   {

     $scope.data.Position = Position;
     $scope.listLoading = true;
     $scope.IndustryID = industryID;
       $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();
       $('#view_model').modal({show:true});
       $scope.formDatas = [];
        $scope.data.pageLoading = true;
        $http.post(API_URL+'industry/getIndustry', 'SessionKey='+SessionKey+'&industryID='+$scope.IndustryID, contentType).then(function(response) {
           var response = response.data;
            if(response.ResponseCode==200){ /* success case */
                $scope.data.pageLoading = false;

                $scope.formDatas = response.Data.Records[0];
                console.log($scope.formDatas);
                $scope.listLoading = false;
               //  $timeout(function(){            
               //     $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
               // }, 700);
            }
             $scope.listLoading = false;
        });
    }



    $scope.loadFormDelete = function (Position, IndustryID)
    {
        $scope.data.Position = Position;
        $scope.listLoading = true;
        $scope.IndustryID = IndustryID;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'industry/deleteIndustry', 'SessionKey='+SessionKey+'&IndustryID='+IndustryID+'&StatusID=3', contentType).then(function(response) {
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    //$('.modal-header .close').click();
                    $scope.listLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.listLoading = false;
    }

  

    /*add data*/



    $scope.addData = function ()
    {
      //alert(1);
        tinyMCE.triggerSave();
       // e.preventDefault();
        $scope.listLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'industry/addIndustry', data, contentType).then(function(response) {
            var response = response.data;

            if(response.ResponseCode==200){ /* success case */
                $scope.getList();               
                SuccessPopup(response.Message);
                $timeout(function(){            
                   $('#add_model .close').click();
                   window.location.href = window.location.href;
               }, 200);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.listLoading = false;          
        });

    }











    /*edit data*/



    $scope.editData = function ()



    {


        tinyMCE.triggerSave();

        $scope.editDataLoading = true;

        console.log($scope.formdata);

        var data = 'SessionKey='+SessionKey+'&IndustryID='+$scope.IndustryID+'&'+$("form[name='edit_form']").serialize();



        $http.post(API_URL+'industry/editindustry', data, contentType).then(function(response) {



            var response = response.data;



            if(response.ResponseCode==200){ /* success case */               



              SuccessPopup(response.Message);



                $scope.data.dataList[$scope.data.Position] = response.Data;

                

                $('#edit_model .close').click();

                window.location.href = window.location.href;

            }else{



                ErrorPopup(response.Message);



            }



            $scope.editDataLoading = false;          



        });



    }

}); 




/* sortable - starts */



/* sortable - starts */



function tblsort() {







  var fixHelper = function(e, ui) {



    ui.children().each(function() {



        $(this).width($(this).width());



    });



    return ui;



}







$(".table-sortable tbody").sortable({



    placeholder: 'tr_placeholder',



    helper: fixHelper,



    cursor: "move",



    tolerance: 'pointer',



    axis: 'y',



    dropOnEmpty: false,



    update: function (event, ui) {



      sendOrderToServer();



  }      



}).disableSelection();



$(".table-sortable thead").disableSelection();











function sendOrderToServer() {



    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");



    $.ajax({



        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',



        data: order,



        stop: function(response) {



            if (response.status == "success") {



                window.location.href = window.location.href;



            } else {



                alert('Some error occurred');



            }



        }



    });



  }
}
/* sortable - ends */


function search_records()
{
    angular.element(document.getElementById('panel-body')).scope().applyFilter(); 
}

function clear_search_records()
{    
    $("#Keyword").val("");    

    search_records();
}