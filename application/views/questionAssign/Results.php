<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Results</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getQuestionPaperlist()"><!-- Body -->

	<div class="clearfix mt-4 mb-2">

	   <div class="row float-left ml-6" style="width:80%;">

	   		<div class="col-md-3">
		      	<div class="form-group">
			      <span class="float-left records hidden-sm-down">
					<span class="h5">Total Records: {{data.dataList.length}}</span>
				  </span>
				 </div>
			</div>

	      <div class="col-md-4" style="">

	         <div class="form-group">

	            <select class="form-control" ng-model="QtPaperGUID" id="SelQtPaperGUID" ng-change="getTestResults(1,QtPaperGUID)">

	               <option value="">Select Paper</option>

	               <option ng-repeat="row in data.qplist" value="{{row.QtPaperGUID}}" ng-selected="row.QtPaperGUID == QtPaperGUID">{{row.QtPaperTitle}}</option>
	            </select>

	         </div>

	      </div>

	      <div class="col-md-3">
			<form class="form-inline float-left ml-4 ng-pristine ng-valid ng-submitted" id="filterForm" role="form" autocomplete="off" ng-submit="applyFilter()">  
				<div id="universal_search">
                    <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
                    <span class="glyphicon glyphicon-search" ng-click="applyFilter()" data-toggle="tooltip" title="Search"></span>
                    <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
                </div>
			</form>
		  </div>

	   </div>

	  <!--  <div class="float-right mr-2">		

			<a class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" href="<?php echo base_url().'questionassign/quiz/'; ?>">Quiz Statistics</a>

	   </div> -->

	   	<div class="float-right mr-2" ng-show="!data.listLoading">		
			<img src="./asset/img/ajax-loader.gif" ng-if="data.excelLoading" style="position: absolute;    top: -3%;    right: 17%; ">

			<a class="btn btn-success btn-sm ml-1 float-right" href="<?php echo base_url();?>questionassign/result_export_pdf?QtPaperGUID={{QtPaperGUID}}">Export in PDF</a>

			<a class="btn btn-success btn-sm ml-1 float-right" href="" ng-click="tableDatatoExcel()">Export in Excel</a>
		</div>

	</div>


	<!-- Data table -->

	<div class="table-responsive block_pad_md" style="margin-top: 45px;" infinite-scroll="getTestResults()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr>
					<th style="width: 150px; text-align: center;">Student</th>

					<th style="width: 130px; text-align: center;">Attempt Date</th>		

					<th style="width: 70px; text-align: center;">Total<br/>Questions</th>								

					<th style="width: 75px; text-align: center;">Attempted</th>

					<th style="width: 70px; text-align: center;">Skip<br/>Questions</th>

					<th style="width: 70px; text-align: center;">Correct<br/>Answers</th>

					<th style="width: 70px; text-align: center;">Wrong<br/>Answers</th>

					<th style="width: 70px; text-align: center;">Total<br/>Marks</th>

					<th style="width: 50px; text-align: center;">Passing %</th>

					<th style="width: 70px; text-align: center;">Obtained<br/>Marks</th>

					<th style="width: 100px; text-align: center;">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row"  ng-repeat="(key, row) in data.dataList">

					<th style="text-align: center;">{{row.FirstName}} {{row.LastName}}</th>

					<td style="text-align: center;">{{row.AttemptDate}}</td>

					<td style="text-align: center;">{{row.TotalQuestions}}</td>

					<td style="text-align: center;">{{row.AttemptQuestions}}</td>

					<td style="text-align: center;">{{row.SkipQuestions}}</td>

					<td style="text-align: center;">{{row.CorrectAnswers}}</td>

					<td style="text-align: center;">{{row.WrongAnswers}}</td>

					<th style="text-align: center;">{{row.TotalMarks}}</th>  

					<td style="text-align: center;">{{row.PassingMarks}}</td>  

					<th style="width: 50px; text-align: center;">{{row.GainedMarks}}</th>
					

					<td class="text-center" style="text-align: center;">

						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'questionassign/result_paper/?ResultGUID={{row.ResultID}}'; ?>" data-toggle="tooltip" title="Results"></a> 

						<!-- <a class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEdit(key,row.QtPaperGUID)"></a>		 -->				

						<!-- <a class="glyphicon glyphicon-trash" href="" ng-click="deleteQuestionPaper(key,row.QtPaperGUID)"></a>

						<a class="glyphicon glyphicon-repeat" href="" ng-click="loadAssignPaper1(key,row.QtPaperGUID)" data-toggle="tooltip" title="Assign Question Paper"></a> -->

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Generate Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<!-- add Modal -->

	<div class="modal fade" id="assign_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Assign Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAssign"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_subject">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Subjcet</h3>     	

					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="subjectTemplateURLAdd"></div>

			</div>

		</div>

	</div>











	<!-- edit Modal -->

	<div class="modal fade" id="edits_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>





	<!-- edit Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Question Details</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>

</div><!-- Body/ -->