app.controller('PageController', function ($scope, $http,$timeout,$compile){
 $scope.data.ParentCategoryGUID = ParentCategoryGUID;
 $scope.categoryDataList = [];
  $scope.data.dataList = [];
$scope.SelectedCategoryFormAdd = false;
    /*----------------*/

    $scope.getFilterData = function() {
        $scope.getSecondLevelPermission();
    }

    $scope.calculate_moreInstallment = function(key){
        var fees = document.getElementById('fee1').value;
        var installment = document.getElementById('NoInstallment'+key).value;
        var discount = document.getElementById('markup'+key).value;
        var discount_amt = document.getElementById('intallment_amt'+key).value;
       
        document.getElementById('intallment_amt'+key).value = 0;
        document.getElementById('finalInstallment'+key).value = 0;

        if(fees == "") { alert("Please enter Course Fee.");  fees = 0; return; }

        if(installment !='' && installment != 0 && fees != ""){

            if(discount!='' && discount != 0){
          
            var result_discount = parseInt(fees) * parseInt(discount)/100;
            var final_amount = fees-result_discount;
            var total = parseInt(result_discount) + parseInt(fees);
            var finalamt = total/installment;

           
            document.getElementById('intallment_amt'+key).value = finalamt.toFixed(2);
            document.getElementById('finalInstallment'+key).value = total;
         

          }else{
            var finalamt = fees/installment;
            document.getElementById('finalInstallment'+key).value = fees;
            document.getElementById('markup'+key).value = "";
            document.getElementById('intallment_amt'+key).value= finalamt.toFixed(2);;

          }

        }
    }


    $scope.RemoveOne=function(Position,InstallmentID=""){
        if(InstallmentID!=''){
           var data = 'SessionKey=' + SessionKey + '&InstallmentID=' + InstallmentID;
            $http.post(API_URL + 'fee/Installmentremove', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.getList();
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });  
        }
        $('#row'+Position).remove(); 
    }; 

      $scope.clicked = 0;
      $scope.addMore = function(clicked) {
        //alert(clicked);
      

        var i=$scope.clicked + clicked;
        $('.installment_row').last().after($compile('<div class="row installment_row" id="row'+i+'"> <div class="col-sm-12" style="margin-left:20px;"></div><div class="col"><div class="form-group" style="padding-left:15px;"><label class="control-label mb-10">No.Of Installment</label><input type="text" name="installment[]" id="NoInstallment'+i+'"  placeholder="" class="form-control qty integer"  onkeyup="calculate_moreInstallment('+i+')"/></div></div><div class="col"><div class="form-group"><label class="control-label mb-10">Markup(%)</label><input type="text" name="markup[]" class="form-control price integer" onkeyup="calculate_moreInstallment('+i+')" id="markup'+i+'"  placeholder="0.00"></div></div><div class="col"><div class="form-group"><label class="control-label mb-10" for="exampleInputuname_2">Installment Amt (<i class="fa fa-rupee"></i>)</label><div class="input-group"><input type="text" name="installment_amt[]" class="form-control output" id="intallment_amt'+i+'" placeholder="0.00" readonly=""><div class="input-group-addon"></div></div></div></div><div class="col"><div class="form-group"><label class="control-label mb-10" for="exampleInputuname_2">Total Fee (<i class="fa fa-rupee"></i>)</label><div class="input-group"><input type="text" name="total_fees[]" class="form-control output" id="finalInstallment'+i+'" placeholder="0.00" readonly="" onkeyup="calculate_moreInstallment('+i+')"><div class="input-group-addon"></div></div></div></div> <div class="col"><div class="form-group change"><label for="" style="">&nbsp;</label><br><button type="button" name="add" id="add'+i+'" onclick="addMore('+i+')" class="btn btn-success" style="display:none;">+</button><button type="button" name="remove" id="'+i+'" onclick="RemoveOne('+i+')" class="btn btn-danger btn_remove" >Remove</button></div></div></div>')($scope));
     
      } 



    $scope.getCategoryFilterData = function (ParentCategoryGUID)
    {
        if(ParentCategoryGUID){
            if($scope.categoryDataList){
                //console.log(ParentCategoryGUID);
                //console.log($scope.categoryDataList);
                if($scope.categoryDataList[ParentCategoryGUID].SubCategories){
                    var SubCategories = $scope.categoryDataList[ParentCategoryGUID].SubCategories.Records;
                    var options = '<option value="">Select Course</option>';
                    for (var i in SubCategories) {
                        if(SubCategories[i].Status == 'Active'){
                            options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>'; 
                        }                                      
                    } 
                    if(options){
                        $("#course").html(options);
                    }
                }
            }
        }else{
            var options = '<option value="">Select Course</option>';
            $("#CategoryGUID").html(options);
        }
    }


    $scope.getSubCategoryList = function (ParentCategoryGUID){
        //console.log(ParentCategoryGUID);
        $scope.subCategoryDataList = [];
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&CategoryTypeID=2';
        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                $scope.subCategoryDataList = response.Data.Records; 
            } //////console.log($scope.categoryDataList);
        });
        console.log($scope.subCategoryDataList);
    }


    $scope.getCourses = function (ParentCategoryGUID){
        //console.log(ParentCategoryGUID);
        $scope.CoursesDataList = [];
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&CategoryTypeID=2';
        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                $scope.CoursesDataList = response.Data.Records; 
            } //////console.log($scope.categoryDataList);
        });
        console.log($scope.CoursesDataList);
    }


    /*list*/
    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList();
    }




    $scope.getList = function (check="",ParentCategoryGUID="",CategoryGUID=""){
        if(check!=1){
            $scope.ParentCategoryGUID = ParentCategoryGUID; $scope.CategoryGUID = CategoryGUID;
            if ($scope.data.listLoading || $scope.data.noRecords) return;         
        }else{
            if(ParentCategoryGUID.length > 0){
                $scope.ParentCategoryGUID = ParentCategoryGUID;
                $scope.CategoryGUID = CategoryGUID;
            }else if(CategoryGUID.length > 0){
                $scope.CategoryGUID = CategoryGUID; 
            }else{
                $scope.ParentCategoryGUID = ""; $scope.CategoryGUID = "";
                $scope.CoursesDataList = [];
            }
        }
         
        $scope.data.listLoading = true;        
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+$scope.ParentCategoryGUID+'&CategoryGUID='+$scope.CategoryGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'
        +$('#filterForm').serialize();
        $scope.data.dataList = [];
        $http.post(API_URL+'fee/getFee', data, contentType).then(function(response) {
                var response = response.data;
                ////console.log(response.Data.length);
                if(response.ResponseCode==200 && response.Data.length){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    //console.log(response.Data.length);
                
                    for (var i in response.Data) {
                            $scope.data.dataList.push(response.Data[i]);
                    }

                    if(check!=1){
                        $scope.getCategoryList();
                    }
                    

                    $scope.data.pageNo++;               
                }else{
                    $scope.data.noRecords = true;
                }

            $scope.data.listLoading = false;
        });
    }



    $scope.getDuration = function (CategoryGUID){
       //alert(CategoryGUID);
        $scope.Duration = 0;
        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CategoryGUID;

        $http.post(API_URL+'fee/getDuration', data, contentType).then(function(response) {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length){ 

                $scope.Duration = response.Data[0].Duration;
               //console.log($scope.Duration);

            } 
        });
    }


/*list append*/
   $scope.getCategoryList = function ()
    {
       // alert(1);
        var data = '';
        //$scope.getCategoryFilterData();
        $scope.categoryDataList = [];
       // var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
        $http.post(API_URL+'category/getCategories','SessionKey='+SessionKey+'&CategoryTypeID=1', contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200 && response.Data.Records){ /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                //console.log(response.data);
                for (var i in response.Data.Records) {
                   // for(var j in response.Data.Records[i].SubCategories.Records){
                    if(response.Data.Records[i].Status == 'Active'){
                        $scope.categoryDataList.push(response.Data.Records[i]);
                    }
                    //}                                  
                }
            //console.log($scope.data.categoryDataList);
             $scope.data.pageNo++;               
         }else{
            $scope.data.noRecords = true;
        }
    });
}


    /*load edit form*/
    $scope.loadFormAdd = function ()
    {
        $scope.SelectedCategoryFormAdd = false;
        //$scope.categoryDataList = [];
        $scope.subCategoryDataList = [];
        $scope.Duration = "";
        $scope.SubjectGUID = "";
        $scope.CourseGUID = "";
        $scope.templateURLEdit = ""; 
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
            $('#add_model').modal({show:true});
            
            $scope.feeBreakupListing();
           // $scope.getCategoryList();
            $timeout(function(){        
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
         }, 200);
    }


    $scope.LoadSelectedCategoryFormAdd = function (parentCategoryGUID,SubCategoryGUID)
    {
        $scope.AddBySelectedCategory = 1;
        $scope.getSubCategoryList(parentCategoryGUID);
        $scope.getDuration(SubCategoryGUID);
        var a = parentCategoryGUID;
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $scope.CourseGUID = SubCategoryGUID;
        $scope.StreamGUID = parentCategoryGUID; 
        $scope.SelectedCategoryFormAdd = true;      
        $timeout(function(){  
             $('#add_model').modal({show:true});
             $scope.feeBreakupListing();
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
        }, 800);
        $scope.data.pageLoading = false;
    }


    /*load edit form*/
    $scope.loadFormEdit = function (Position, FeeGUID)
    {
        $scope.data.feeEditBreakupListing = [];
        $scope.getCategoryFilterData();
        $scope.data.Position = Position;
        $scope.templateURLAdd = "";
        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
         // $('#edit_model').modal({show:true});
        $http.post(API_URL+'fee/getFeeData', 'SessionKey='+SessionKey+'&FeeGUID='+FeeGUID, contentType).then(function(response) {
            var response = response.data;
           // console.log(response);
           // return false;
            if(response.ResponseCode==200 && response.Data)
                { /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data[0];
                $scope.getSubCategoryList($scope.formData.ParentCategoryGUID);
                $scope.StreamID=$scope.formData.ParentCategoryGUID;
                $scope.CourseID=$scope.formData.CategoryGUID;
                $('#edit_model').modal({show:true});
                
                for (var i in response.Data.FeeBreakupList) 
                {                   
                   $scope.data.feeEditBreakupListing.push(response.Data.FeeBreakupList[i]);
                }

                $timeout(function(){            
                 $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
             }, 200);
            }

        });
    }

 /*load edit form*/
    $scope.loadFormView = function (Position, FeeGUID)
    {
        $scope.data.feeViewBreakupListing = [];
        $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
   
           $http.post(API_URL+'fee/getFeeData', 'SessionKey='+SessionKey+'&FeeGUID='+FeeGUID, contentType).then(function(response) {
            var response = response.data;
            console.log(response);
           // return false;
            if(response.ResponseCode==200 && response.Data){ /* success case */
                $scope.data.pageLoading = false;
                $scope.formData = response.Data[0];
                console.log($scope.formData);
                $('#view_model').modal({show:true});
               
                for (var i in response.Data.FeeBreakupList) 
                {                   
                   $scope.data.feeViewBreakupListing.push(response.Data.FeeBreakupList[i]);
                }

                $timeout(function(){            
                 $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
             }, 200);
            }

        });
    }

    /*load delete form*/
    $scope.loadFormDelete = function(Position,FeeGUID)
    {
      
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {

            $http.post(API_URL+'fee/delete', 'SessionKey='+SessionKey+'&FeeGUID='+FeeGUID, contentType).then(function(response) {
              
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */

                    SuccessPopup(response.Message);
                    $scope.getList(1,$scope.ParentCategoryGUID,$scope.CategoryGUID);
                    //$scope.data.dataList.splice($scope.data.Position, 1); 
                    //$scope.data.totalRecords--;
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }

    $scope.deleteInstallment = function(InstallmentID) {
        $scope.deleteDataLoading = true;
        console.log('id_'+InstallmentID);
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&InstallmentID=' + InstallmentID;
            $http.post(API_URL + 'fee/deleteInstallment', data, contentType).then(function(response) {

                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $('#id_'+InstallmentID).remove();
                    $timeout(function(){ 
                        $('#delete_model .close').click();
                        
                    });
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;

    }

    /*delete Entity*/
    $scope.deleteData = function(Position, EntityGUID) {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&EntityGUID=' + EntityGUID;
            $http.post(API_URL + 'admin/entity/delete', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.getList();
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;

    }

    /*add data*/
    $scope.addData = function()
    {
       // alert(1);
        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'fee/addFee', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                $scope.getList(1,$scope.ParentCategoryGUID,$scope.CategoryGUID);
                // if($scope.ParentCategoryGUID.length > 0){
                //    $scope.getSubCategoryList($scope.ParentCategoryGUID)
                // }else{
                //    $scope.subCategoryDataList = []; 
                // }
                $timeout(function(){    
                    $('#add_model .close').click();
                    $scope.subCategoryDataList = [];
                }, 200);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });
    }


    /*edit data*/
    $scope.editData = function()
    {
        $scope.editDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();
        $http.post(API_URL+'fee/editfee', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                $scope.data.dataList[$scope.data.Position] = response.Data;
                $scope.getList(1,$scope.ParentCategoryGUID,$scope.CategoryGUID);
                // if($scope.ParentCategoryGUID.length > 0){
                //    $scope.getSubCategoryList($scope.ParentCategoryGUID)
                // }else{
                //    $scope.subCategoryDataList = []; 
                // }
                  $timeout(function(){  
                        $('#edit_model .close').click();
                        //$scope.subCategoryDataList = [];
                  }, 200);
                 //$scope.applyFilter();
            }else{
                ErrorPopup(response.Message);
            }
            $scope.editDataLoading = false;          
        });
    }


    $scope.addNewBreakup = function ()
    {        
        $scope.templateURLBreakup = PATH_TEMPLATE+module+'/breakup_name.htm?'+Math.random();
        
        $('#fee_breakup_model .close').click();

        $('#breakup_name_model').modal({show:true}); 

        $scope.breakupListing();                   
    }

    $scope.showParentFee = function ()
    {        
        $('#fee_breakup_model').modal({show:true});         
    }

    $scope.loadFormFeeBreakup = function ()
    {        
        $scope.templateURLFeeBreakup = PATH_TEMPLATE+module+'/fee_breakup.htm?'+Math.random();
        
        $('#fee_breakup_model').modal({show:true}); 
        
        $scope.breakupListing();
        $scope.feeBreakupListing();           
    }

    $scope.addFormFeeBreakup = function()
    {        
        var fee_head_name = $("#fee_head_name").val();
        var taxable_percent = $("#fee_taxable_percent").val();
        var refundable_percent = $("#fee_refundable_percent").val();

        if(fee_head_name == "") { alert("Please select Breakup Name."); return false;}
        if(taxable_percent != "" && taxable_percent > 100) { alert("Taxable percentage should be less than or equal to 100"); return false; }
        if(refundable_percent != "" && refundable_percent > 100) { alert("Refundable percentage should be less than or equal to 100"); return false; }

        $scope.addFormFeeBreakupLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("#fee_breakup_form").serialize();
        
        $http.post(API_URL+'fee/addFeeBreakup', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode==200)
            { 
                $("#fee_breakup_form").find("input[type='text'], select").val('');
                $("#fee_is_taxable, #fee_is_refundable").removeAttr("checked");
                $("#fee_taxable_percent, #fee_refundable_percent").attr("disabled", "disabled");

                //SuccessPopup(response.Message);

                $scope.feeBreakupListing();
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.addFormFeeBreakupLoading = false;          
        }); 
    }


    $scope.feeBreakupListing = function ()
    {
        $scope.data.fee_breakup_list = [];

        $scope.data.addFormFeeBreakupLoading = true;       

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'fee/feeBreakupListing', data, contentType).then(function(response) 
        {
            $scope.data.addFormFeeBreakupLoading = false;

            var response = response.data;

            if(response.ResponseCode == 200 && response.Data)
            {
                for (var i in response.Data.Records) 
                {
                    $scope.data.fee_breakup_list.push(response.Data.Records[i]);
                }               
            }
        });
    }

    $scope.feeBreakupDelete = function (fid)
    {
        if(fid <= 0) return false;

        alertify.confirm('Are you sure you want to delete?', function() 
        {
            $scope.data.deleteDataLoading = true;

            var data = 'SessionKey='+SessionKey+"&fid="+fid;

            $http.post(API_URL+'fee/feeBreakupDelete', data, contentType).then(function(response) 
            {
                $scope.data.deleteDataLoading = false;

                var response = response.data;

                if(response.ResponseCode == 200)
                {
                    $("#sectionsid_"+fid).remove();
                }
                else
                {
                    ErrorPopup(response.Message);
                }
            });
         }).set('labels', {
            ok: 'Confirm',
            cancel: 'Cancel'
        });   
    }


    $scope.enable_disable = function (fbid)
    {
        var chk = $("#fb_chk_"+fbid).is(":checked");
        if(chk == true)
        {
            $("#fb_amt_"+fbid).removeAttr("disabled");
        }
        else
        {
            $("#fb_amt_"+fbid).val('');
            $("#fb_netamt_"+fbid).val('');
            $("#fb_amt_"+fbid).attr("disabled", "disabled");

            $("#discount, #discount_amt, #final_total").val('');            
        }

        $scope.fb_total_fee_calculate();
    }

    $scope.fb_calculate = function (fbid)
    {
        var chk = $("#fb_chk_"+fbid).is(":checked");
        if(chk == true)
        {
            var amt = $("#fb_amt_"+fbid).val();
            var tax = $("#fb_tax_"+fbid).val();

            if(!isNaN(tax) && tax != '' && tax != null && tax != undefined && tax > 0 && amt != '' && amt != null && amt != undefined)
            {
                amt = parseFloat(amt) + parseFloat( (amt * tax) / 100 );
                amt = Math.ceil(amt);
            }

            if(!isNaN(amt) && amt != '' && amt != null && amt != undefined)
                $("#fb_netamt_"+fbid).val(amt);
            else
                $("#fb_netamt_"+fbid).val('');

            $scope.fb_total_fee_calculate();
        }
        else
        {
            $("#fb_amt_"+fbid).val('');
            $("#fb_netamt_"+fbid).val('');
            $("#fb_amt_"+fbid).attr("disabled", "disabled");
        }
    }

    $scope.fb_total_fee_calculate = function ()
    {
        var ttl = 0;
        var temp = 0;

        $(".fb_netamt").each(function()
        {
            temp = $(this).val();

            if(!isNaN(temp) && temp != '' && temp != null && temp != undefined && temp > 0)
            {
                ttl = parseFloat(ttl) + parseFloat(temp);
            }    
        });

        $("#fb_total").val(ttl);
        $("#fee, #fee1").val(ttl);

        calculate();
    }


    $scope.addFormBreakup = function()
    {        
        var breakup_head_name = $("#breakup_head_name").val();
        
        if(breakup_head_name == "") { alert("Please enter Break Name."); return false; }

        $scope.addFormBreakupLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("#breakup_head_form").serialize();
        
        $http.post(API_URL+'fee/addBreakup', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode==200)
            {                
                $("#breakup_head_id").val(0);
                $("#breakup_head_name").val(''); 
                $scope.breakupListing();
                $("#breakup_head_id").val(0);
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.addFormBreakupLoading = false;          
        });
    }


    $scope.breakupListing = function ()
    {
        $scope.data.breakup_list = [];

        //$scope.data.addFormBreakupLoading = true;       

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'fee/breakupListing', data, contentType).then(function(response) 
        {
            //$scope.data.addFormBreakupLoading = false;

            var response = response.data;

            if(response.ResponseCode == 200 && response.Data)
            {
                for (var i in response.Data.Records) 
                {
                    $scope.data.breakup_list.push(response.Data.Records[i]);
                }               
            }
        });
    }

    $scope.setEditBreakName = function (bid, bnm)
    {
        if(bnm != "" && bid > 0)
        {
            $("#breakup_head_id").val(bid);
            $("#breakup_head_name").val(bnm);
        }
        else
        {
            $("#breakup_head_id").val(0);
            $("#breakup_head_name").val('');
        }
    }


    $scope.resetFormBreakup = function ()
    {   
        $("#breakup_head_id").val(0);
        $("#breakup_head_name").val('');
    }  


    $scope.getFeeReminders = function()
    {
        $scope.data.duedays = [1,2,3,4,5,6,7,8,9,10];
        $scope.data.section1 = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]; 
        $scope.data.section2 = [16,17,18,19,20,21,22,23,24,25]; 
        $scope.data.section3 = [26,27,28,29,30];  

        $scope.addBeforeDueDateLoading = false;

        tinymce.init({
            selector: '#bf_email_content, #af_email_content1, #af_email_content2, #af_email_content3'
        });    

        $scope.getBeforeDueDateSetting();
        $scope.getAfterDueDateSetting();
    } 


    /*--------------------------------------------------------
    Before due date settings
    ---------------------------------------------------------*/
    $scope.addFormBeforeDueDate = function()
    {
        var v1 = tinymce.get('bf_email_content').getContent();
        //var v2 = tinymce.get('bf_sms_content').getContent();

        $("#bf_email_content").val(v1);
        //$("#bf_sms_content").val(v2);

        tinymce.execCommand("mceRepaint");

        $scope.addBeforeDueDateLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("#setting_before_due_date").serialize();
        
        $http.post(API_URL+'fee/settingsBeforeDueDate', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode==200)
            {                
                SuccessPopup(response.Message);

                $timeout(function()
                {  
                    window.location.href = window.location.href;
                }, 1000);
            }
            else
            {
                ErrorPopup(response.Message);
            }

            $scope.addBeforeDueDateLoading = false;          
        });
    }


    $scope.getBeforeDueDateSetting = function()
    {
        var data = 'SessionKey='+SessionKey;        
        
        $http.post(API_URL+'fee/getBeforeDueDateSetting', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode == 200)
            {                
                $timeout(function()
                {
                    $("#bf_email_days").val(response.Data.BFMaster.EmailDaysCount);
                    $("#bf_sms_days").val(response.Data.BFMaster.SmsDaysCount);

                    tinymce.get('bf_email_content').setContent(response.Data.BFMaster.EmailContent);
                    $("#bf_sms_content").val(response.Data.BFMaster.SmsContent);

                    $("#bf_email_days").trigger("change");
                    $("#bf_sms_days").trigger("change");


                    if(response.Data.BFEmailDays)
                    {
                        $(response.Data.BFEmailDays).each(function(i,v)
                        {
                            $("#bf_email_remind_days_"+v).attr("checked", "checked");

                            $("#bf_email_remind_days_"+v).prop("checked", "checked");
                        });
                    }

                    if(response.Data.BFSmsDays)
                    {
                        $(response.Data.BFSmsDays).each(function(i,v)
                        {
                            $("#bf_sms_remind_days_"+v).attr("checked", "checked");
                            
                            $("#bf_sms_remind_days_"+v).prop("checked", "checked");
                        });
                    }

                }, 1000);
            }
            else
            {
                ErrorPopup(response.Message);
            }         
        });
    }


    /*--------------------------------------------------------
    After due date settings
    ---------------------------------------------------------*/
    $scope.addFormAfterDueDate = function(formID)
    {
        if(formID <= 0 || formID > 3)
        {
            alert("Sorry! Illegal form submission."); return false;
        }

        var v1 = tinymce.get('af_email_content'+formID).getContent();
        //var v2 = tinymce.get('af_sms_content'+formID).getContent();
        $("#af_email_content"+formID).val(v1);
        //$("#af_sms_content"+formID).val(v2);

        tinymce.execCommand("mceRepaint");

        if(formID == 1)
        {
            $scope.addFormAfterDueDateLoading1 = true;
            $scope.addFormAfterDueDateLoading2 = false;
            $scope.addFormAfterDueDateLoading3 = false;
        }
        else if(formID == 2)    
        {
            $scope.addFormAfterDueDateLoading1 = false;
            $scope.addFormAfterDueDateLoading2 = true;
            $scope.addFormAfterDueDateLoading3 = false;
        }
        else if(formID == 3)
        {
            $scope.addFormAfterDueDateLoading1 = false;
            $scope.addFormAfterDueDateLoading2 = false;
            $scope.addFormAfterDueDateLoading3 = true;
        }


        var data = 'SessionKey='+SessionKey+'&formID='+formID+'&'+$("#setting_after_due_date"+formID).serialize();
        
        $http.post(API_URL+'fee/settingsAfterDueDate', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode==200)
            {                
                SuccessPopup(response.Message);

                $timeout(function()
                {  
                    window.location.href = window.location.href;
                }, 1000);
            }
            else
            {
                ErrorPopup(response.Message);
            }
            
            $scope.addFormAfterDueDateLoading1 = false;
            $scope.addFormAfterDueDateLoading2 = false;
            $scope.addFormAfterDueDateLoading3 = false;            

        });
    }


    $scope.getAfterDueDateSetting = function()
    {
        var data = 'SessionKey='+SessionKey;        
        
        $http.post(API_URL+'fee/getAfterDueDateSetting', data, contentType).then(function(response) 
        {
            var response = response.data;
            if(response.ResponseCode == 200)
            {                
                $timeout(function()
                {
                    if(response.Data.AFMaster.phase1)
                    {   
                        var obj = response.Data.AFMaster.phase1;

                        $("#af_email_days1").val(obj.EmailDaysCount);
                        $("#af_sms_days1").val(obj.SmsDaysCount);

                        tinymce.get('af_email_content1').setContent(obj.EmailContent);
                        $("#af_sms_content1").val(obj.SmsContent);

                        $("#af_email_days1").trigger("change");
                        $("#af_sms_days1").trigger("change");

                        if(obj.RemindToParent == 1)
                        {
                            $("#af_alert_p1").attr("checked", "checked");
                            $("#af_alert_p1").prop("checked", "checked");
                        }    
                    }


                    if(response.Data.AFMaster.phase2)
                    {   
                        var obj = response.Data.AFMaster.phase2;

                        $("#af_email_days2").val(obj.EmailDaysCount);
                        $("#af_sms_days2").val(obj.SmsDaysCount);

                        tinymce.get('af_email_content2').setContent(obj.EmailContent);
                        $("#af_sms_content2").val(obj.SmsContent);

                        $("#af_email_days2").trigger("change");
                        $("#af_sms_days2").trigger("change");

                        if(obj.RemindToParent == 1)
                        {
                            $("#af_alert_p2").attr("checked", "checked");
                            $("#af_alert_p2").prop("checked", "checked");
                        } 

                        if(obj.RemindToFaculty == 1)
                        {
                            $("#af_alert_f2").attr("checked", "checked");
                            $("#af_alert_f2").prop("checked", "checked");
                        } 
                    }


                    if(response.Data.AFMaster.phase3)
                    {   
                        var obj = response.Data.AFMaster.phase3;

                        $("#af_email_days3").val(obj.EmailDaysCount);
                        $("#af_sms_days3").val(obj.SmsDaysCount);

                        tinymce.get('af_email_content3').setContent(obj.EmailContent);
                        $("#af_sms_content3").val(obj.SmsContent);

                        $("#af_email_days3").trigger("change");
                        $("#af_sms_days3").trigger("change");

                        if(obj.RemindToParent == 1)
                        {
                            $("#af_alert_p3").attr("checked", "checked");
                            $("#af_alert_p3").prop("checked", "checked");
                        } 

                        if(obj.RemindToFaculty == 1)
                        {
                            $("#af_alert_f3").attr("checked", "checked");
                            $("#af_alert_f3").prop("checked", "checked");
                        } 

                        if(obj.RemindToManagement == 1)
                        {
                            $("#af_alert_m3").attr("checked", "checked");
                            $("#af_alert_m3").prop("checked", "checked");
                        }
                    }


                                            
                    if(response.Data.AFEmailDays.phase1)
                    {
                        $(response.Data.AFEmailDays.phase1).each(function(i,v)
                        {                                
                            $("#af_email_remind_days1_"+v).attr("checked", "checked");
                            $("#af_email_remind_days1_"+v).prop("checked", "checked");
                        });
                    }
                    if(response.Data.AFEmailDays.phase2)
                    {
                        $(response.Data.AFEmailDays.phase2).each(function(i,v)
                        {                                
                            $("#af_email_remind_days2_"+v).attr("checked", "checked");
                            $("#af_email_remind_days2_"+v).prop("checked", "checked");
                        });
                    }
                    if(response.Data.AFEmailDays.phase3)
                    {
                        $(response.Data.AFEmailDays.phase3).each(function(i,v)
                        {                                
                            $("#af_email_remind_days3_"+v).attr("checked", "checked");
                            $("#af_email_remind_days3_"+v).prop("checked", "checked");
                        });
                    }


                    if(response.Data.AFSmsDays.phase1)
                    {
                        $(response.Data.AFSmsDays.phase1).each(function(i,v)
                        {
                            $("#af_sms_remind_days1_"+v).attr("checked", "checked");
                            $("#af_sms_remind_days1_"+v).prop("checked", "checked");
                        });
                    }
                    if(response.Data.AFSmsDays.phase2)
                    {
                        $(response.Data.AFSmsDays.phase2).each(function(i,v)
                        {
                            $("#af_sms_remind_days2_"+v).attr("checked", "checked");
                            $("#af_sms_remind_days2_"+v).prop("checked", "checked");
                        });
                    }
                    if(response.Data.AFSmsDays.phase3)
                    {
                        $(response.Data.AFSmsDays.phase3).each(function(i,v)
                        {
                            $("#af_sms_remind_days3_"+v).attr("checked", "checked");
                            $("#af_sms_remind_days3_"+v).prop("checked", "checked");
                        });
                    }
                    
                    

                    

                }, 1000);
            }
            else
            {
                ErrorPopup(response.Message);
            }         
        });
    }

}); 





function getCategoryFilterData() {
    var stream = $("#stream").val();
    if(stream){
        angular.element(document.getElementById('fee-body')).scope().getCategoryFilterData(stream);
    }
}

function getSubCategoryList(ParentCategoryGUID){
    angular.element(document.getElementById('fee-body')).scope().getSubCategoryList(ParentCategoryGUID);
}

function getDuration(CategoryGUID){
   angular.element(document.getElementById('fee-body')).scope().getDuration(CategoryGUID); 
}

function FilterAndGetSubCategoryList(ParentCategoryGUID="",CategoryGUID="")
{
    if(ParentCategoryGUID.length > 0)
    {
        angular.element(document.getElementById('fee-body')).scope().getCourses(ParentCategoryGUID);
    }
}

function search_records()
{
    var ParentCategoryGUID = $("#Courses").val();
    var CategoryGUID = $("#subject").val();

    angular.element(document.getElementById('fee-body')).scope().getList(1,ParentCategoryGUID,CategoryGUID);
}

function clear_search_records()
{
    $("#Courses").val("");
    $("#subject").val("");

    search_records();
}


function show_remind_days(typ, nd, field)
{
    var rd = "";
    

    if(typ == "bfe" || typ == "bfs")
    {
        if(nd != "")
        {
            for(nd; nd >= 1;  nd--)
            {
                rd = rd + '<div class="form-check ml-3" style="float:left;"><input class="form-check-input rounded-0" type="checkbox" value="'+nd+'" id="'+field+'_'+nd+'" name="'+field+'[]"><label class="form-check-label">'+nd+'</label></div>';
            }
        }
    }
    else
    {
        var sc = 0;
        var ec = nd;

        var sc1 = 0;
        var ec1 = 0;
     

        if(typ == "afe1" || typ == "afs1")
        {
            sc = 1;
            ec = nd;

            sc1 = parseInt(ec) + 1;
            ec1 = parseInt(sc1) + 10;
            
            var opt = "";
            for(sc1; sc1 <= ec1;  sc1++)
            {
                opt = opt + "<option value='"+sc1+"'>"+sc1+"</option>";
            }

            if(typ == "afe1")
            {
                $("#af_email_days2").find("option:gt(0)").remove();
                $("#af_email_days2").append(opt);
            }
            else if(typ == "afs1")
            {
                $("#af_sms_days2").find("option:gt(0)").remove();
                $("#af_sms_days2").append(opt);
            }
        }
        else if(typ == "afe2" || typ == "afs2")
        {
            if(typ == "afe2")
            {
                sc = parseInt($("#af_email_days1").val()) + 1;
                ec = nd;
            }
            else if(typ == "afs2")
            {
                sc = parseInt($("#af_sms_days1").val()) + 1;
                ec = nd;
            }             

            sc1 = parseInt(ec) + 1;
            ec1 = parseInt(sc1) + 5;
            
            var opt = "";
            for(sc1; sc1 <= ec1;  sc1++)
            {
                opt = opt + "<option value='"+sc1+"'>"+sc1+"</option>";
            }

            if(typ == "afe2")
            {
                $("#af_email_days3").find("option:gt(0)").remove();
                $("#af_email_days3").append(opt);
            }
            else if(typ == "afs2")
            {
                $("#af_sms_days3").find("option:gt(0)").remove();
                $("#af_sms_days3").append(opt);
            }
        }
        else if(typ == "afe3" || typ == "afs3")
        {
            if(typ == "afe3")
            {
                sc = parseInt($("#af_email_days2").val()) + 1;
                ec = nd;
            }
            else if(typ == "afs3")
            {
                sc = parseInt($("#af_sms_days2").val()) + 1;
                ec = nd;
            }          
            
        }


        for(sc; sc <= ec;  sc++)
        {
            rd = rd + '<div class="form-check ml-3" style="float:left;"><input class="form-check-input rounded-0" type="checkbox" value="'+sc+'" id="'+field+'_'+sc+'" name="'+field+'[]"><label class="form-check-label">'+sc+'</label></div>';
        }
    }


    $("#"+ field).html(rd);
}