<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bulksms extends Admin_Controller_Secure
{
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
		);
		
		$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js'
		);	

		$load['breadcrumb'] = array(
			"Dashboard" => base_url()."dashboard",
			"Communication" => "",
			"Bulk SMS" =>  ""
		);


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('bulksms/bulksms');
		$this->load->view('includes/footer');
	}


	public function orders()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
		);
		
		$load['js']=array(
			'asset/js/bulksms.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js'
		);	

		$load['breadcrumb'] = array(
			"Dashboard" => base_url()."dashboard",
			"Bulk SMS" =>  base_url()."bulksms",
			"My Orders" => ""
		);


		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('bulksms/sms_orders');
		$this->load->view('includes/footer');
	}


}