app.controller('PageController', function ($scope, $http,$timeout,$compile){
 //$scope.data.ParentCategoryGUID = ParentCategoryGUID;
 $scope.categoryDataList = [];
 $scope.data.dataList = [];
 $scope.SelectedCategoryFormAdd = false;
 $scope.APIURL = API_URL+"upload/image";

$scope.CurrencyNotes = [2000, 500, 200, 100, 50, 20, 10, 5, 2, 1];

    /*----------------*/

    $scope.getCourses = function(){
        var data = 'SessionKey='+SessionKey+'&CategoryTypeName=Course';
        $scope.data.course = [];
        $http.post(API_URL+'students/getCourses', data, contentType).then(function(response) {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */
                $scope.data.course = response.Data;
            }

            $timeout(function(){  
               $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
            }, 200);
        });
    }


    $scope.getBatchByCourse = function(CourseGUID){

        var data = 'SessionKey='+SessionKey+'&CategoryGUID='+CourseGUID;

        $scope.data.batch = [];

        $http.post(API_URL+'keys/getBatchByCourse', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length)
            { /* success case */

                for (var i in response.Data) 
                {

                     $scope.data.batch.push(response.Data[i]);
                }

                $timeout(function()
                { 
                       $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
                }, 200);

            }
            else
            {
                alert("No batch found for selected course.");
            }

        });

    }

    $scope.getFilterData = function(){
        $scope.getCourses();
    }

    $scope.getList = function (check="",CategoryGUID="",BatchGUID=""){
        if(check!=1){
            //$scope.CategoryGUID = CategoryGUID; $scope.BatchGUID = BatchGUID;
            if ($scope.data.listLoading || $scope.data.noRecords) return;         
        }else{
            $scope.data.pageNo = 0;
            $scope.data.dataList = [];
        }
         
        $scope.data.listLoading = true;        
        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $scope.data.dataList = [];
        $http.post(API_URL+'feecollection/getFeeCollections', data, contentType).then(function(response) {
                var response = response.data;
                ////console.log(response.Data.length);
                if(response.ResponseCode==200 && response.Data.length){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    //console.log(response.Data.length);
                
                    for (var i in response.Data) {
                            $scope.data.dataList.push(response.Data[i]);
                    }

                    if(check!=1){
                        //$scope.getCategoryList();
                    }
                    

                    $scope.data.pageNo++;               
                }else{
                    $scope.data.noRecords = true;
                }

            $scope.data.listLoading = false;
        });
    }


    $scope.getStudentsList = function (check="",CourseGUID="",BatchGUID="")

    { 

        // alert(1);   
        $scope.data.students = [];

        var data = 'SessionKey='+SessionKey;

        $http.post(API_URL+'feecollection/getStudents', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.students.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;

                }else{

                    $scope.data.noRecords = true;

                   // ////console.log(data.dataList.length);

                }

                        

                $scope.data.listLoading = false;   //console.log($scope.data.pageNo);

        });

    }


    $scope.getStudentDetail = function (StudentGUID)
    {
        $scope.data.DueDates = [];

        $http.post(API_URL+'feecollection/getStudentFeeDetail', 'SessionKey='+SessionKey+'&UserGUID='+StudentGUID, contentType).then(function(response) 
        {
            var response = response.data;
            
            if(response.ResponseCode==200 && response.Data)
            { 
                $scope.formData = response.Data[0];

                if(response.Data[0].DueDates)
                {                    
                    for (var i in response.Data[0].DueDates) 
                    {
                        $scope.data.DueDates.push(response.Data[0].DueDates[i]);                       
                    }
                } 
            }
        });
    }

    /*load edit form*/
    $scope.loadFormAdd = function ()
    {
        $scope.data.DueDates = [];
        $scope.SelectedCategoryFormAdd = false;
        $scope.formData = [];
        $scope.subCategoryDataList = [];
        $scope.Duration = "";
        $scope.SubjectGUID = "";
        $scope.CourseGUID = ""; 
        $scope.templateURLAdd = PATH_TEMPLATE+'feecollection/addfee.htm?'+Math.random();
        $('#add_model').modal({show:true});
           // $scope.getCategoryList();
       
        $timeout(function(){ 
            var d = new Date();

           $("#PaymentDate").datepicker({format: 'dd-mm-yyyy',minView: 2,maxDate: 0});
           $("#ChequeDate").datepicker({format: 'dd-mm-yyyy',minView: 2,maxDate: 0}); 

       }, 1000);
    }


    $scope.LoadSelectedCategoryFormAdd = function (parentCategoryGUID,SubCategoryGUID)
    {
        $scope.AddBySelectedCategory = 1;
        $scope.getSubCategoryList(parentCategoryGUID);
        $scope.getDuration(SubCategoryGUID);
        var a = parentCategoryGUID;
        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $scope.CourseGUID = SubCategoryGUID;
        $scope.StreamGUID = parentCategoryGUID; 
        $scope.SelectedCategoryFormAdd = true;      
        $timeout(function(){  
             $('#add_model').modal({show:true});       
             $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");
        }, 800);
        $scope.data.pageLoading = false;
    }


   

 /*load edit form*/
    $scope.loadFormView = function (Position, FeeCollectionID)
    {        
        $scope.templateURLView = PATH_TEMPLATE+'feecollection/view_form.htm?'+Math.random();
        $scope.data.pageLoading = true;
        $scope.formdata = [];
        var data = 'SessionKey='+SessionKey+'&FeeCollectionID='+FeeCollectionID;
        $http.post(API_URL+'feecollection/getFeeCollections', data, contentType).then(function(response) {
                var response = response.data;
                ////console.log(response.Data.length);
                if(response.ResponseCode==200 && response.Data.length){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    //console.log(response.Data.length);
                    $scope.formData = response.Data[0];
                    $('#view_model').modal({show:true});

                    $timeout(function()
                    {                         
                       $("#DenominationJSON").val($scope.formData.Denomination);

                       if($scope.formData.Denomination != "")
                       {
                          $scope.calculate_notes_amount_total();
                       }
                    }, 1000);   

                    $scope.data.pageLoading = false;          
                }else{
                    ErrorPopup(response.Message);
                }

            //$scope.data.listLoading = false;
        });
    }


    $scope.loadFormEdit = function (Position, FeeCollectionID)
    {        
        $scope.data.DueDates = [];
        $scope.templateURLEdit = PATH_TEMPLATE+'feecollection/edit_form.htm?'+Math.random();
        $('#edit_model').modal({show:true});
        $scope.data.pageLoading = true;
        $scope.formdata = [];
        var data = 'SessionKey='+SessionKey+'&FeeCollectionID='+FeeCollectionID;
        $http.post(API_URL+'feecollection/getFeeCollections', data, contentType).then(function(response) 
        {
            
                var response = response.data;
                ////console.log(response.Data.length);
                if(response.ResponseCode==200 && response.Data.length){ /* success case */
                    
                    $scope.formData = response.Data[0];
                    if(response.Data[0].DueDates)
                    {                    
                        for (var i in response.Data[0].DueDates) 
                        {
                            $scope.data.DueDates.push(response.Data[0].DueDates[i]);                       
                        }
                    } 
                    
                    $timeout(function(){ 
                        var d = new Date();
                        $("#PaymentDate").datepicker({format: 'dd-mm-yyyy',minView: 2,maxDate: 0});
                        $("#ChequeDate").datepicker({format: 'dd-mm-yyyy',minView: 2,maxDate: 0}); 
                        

 
                       $("#DenominationJSON").val($scope.formData.Denomination);

                       if($scope.formData.Denomination != "")
                       {
                          $.each($scope.formData.Denomination, function(i, v)
                          {
                                $("#"+v.NoteType+"_Note").val(v.Count);
                                $("#"+v.NoteType+"_Cash").val(v.Amount);
                          });

                          $scope.calculate_notes_amount_total();
                       }
                    }, 3000);         
                }else{
                    ErrorPopup(response.Message);
                }

            $scope.data.pageLoading = false; 
        });
    }

    /*load delete form*/
    $scope.loadFormDelete = function(Position,FeeCollectionID)
    {      
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            $http.post(API_URL+'feecollection/delete', 'SessionKey='+SessionKey+'&FeeCollectionID='+FeeCollectionID, contentType).then(function(response) {              
                var response = response.data;
                if(response.ResponseCode==200){ /* success case */
                    SuccessPopup(response.Message);
                    $scope.getList();
                    $scope.deleteDataLoading = false;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;
    }

    

    /*delete Entity*/
    $scope.deleteData = function(Position, EntityGUID) {
        $scope.deleteDataLoading = true;
        $scope.data.Position = Position;
        alertify.confirm('Are you sure you want to delete?', function() {
            var data = 'SessionKey=' + SessionKey + '&EntityGUID=' + EntityGUID;
            $http.post(API_URL + 'admin/entity/delete', data, contentType).then(function(response) {
                var response = response.data;
                if (response.ResponseCode == 200) { /* success case */
                    SuccessPopup(response.Message);
                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/
                    $scope.data.totalRecords--;
                    $scope.getList();
                } else {
                    ErrorPopup(response.Message);
                }
                if ($scope.data.totalRecords == 0) {
                    $scope.data.noRecords = true;
                }
            });
        }).set('labels', {
            ok: 'Yes',
            cancel: 'No'
        });
        $scope.deleteDataLoading = false;

    }

    /*add data*/
    $scope.addData = function ()
    {
        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();
        $http.post(API_URL+'feecollection/addCollection', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                $scope.getList(1);
                $timeout(function(){    
                    $('#add_model .close').click();
                    $scope.subCategoryDataList = [];
                }, 200);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });
    }


    $scope.editData = function ()
    {
        $scope.addDataLoading = true;
        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();
        $http.post(API_URL+'feecollection/editCollection', data, contentType).then(function(response) {
            var response = response.data;
            if(response.ResponseCode==200){ /* success case */               
                SuccessPopup(response.Message);
                $scope.getList(1);
                $timeout(function(){    
                    $('#edit_model .close').click();
                    $scope.subCategoryDataList = [];
                }, 200);
            }else{
                ErrorPopup(response.Message);
            }
            $scope.addDataLoading = false;          
        });
    }



    $scope.updatePaymentMode = function(PaymentMode)
    {
        $scope.formData.PaymentMode = PaymentMode;    
    }


    $scope.tableDatatoExcel = function(){
        
        $scope.data.newDataList = [];
        var data = 'SessionKey='+SessionKey+'&'+$('#filterForm').serialize();
        
        $http.post(API_URL+'feecollection/getFeeCollections', data, contentType).then(function(response) {
                var response = response.data;
                ////console.log(response.Data.length);
                if(response.ResponseCode==200 && response.Data.length){ /* success case */
                    $scope.data.totalRecords = response.Data.TotalRecords;
                    //console.log(response.Data.length);                
                    for (var i in response.Data) {
                        delete response.Data[i].CategoryGUID;
                        delete response.Data[i].UserGUID;
                        delete response.Data[i].MediaID;
                        delete response.Data[i].InstituteID;
                        delete response.Data[i].UserID;
                        delete response.Data[i].StudentID;
                        delete response.Data[i].FeeCollectionID;
                        delete response.Data[i].TotalFee;
                        delete response.Data[i].FeeStatusID;
                        delete response.Data[i].InstallmentID;
                        delete response.Data[i].FeeID;
                        delete response.Data[i].BatchID;
                        delete response.Data[i].CourseID;
                        var newObject= {
                            'FirstName': response.Data[i].FirstName,
                            'LastName': response.Data[i].LastName,
                            'CourseName': response.Data[i].CourseName,
                            'BatchName': response.Data[i].BatchName,
                            'TotalFeeAmount': response.Data[i].TotalFeeAmount,
                            'TotalFeeInstallments': response.Data[i].TotalFeeInstallments,
                            'InstallmentAmount': response.Data[i].InstallmentAmount,
                            'PaidEMICount': response.Data[i].PaidEMICount,
                            'RemainingEMI': response.Data[i].RemainingEMI,
                            'PaymentMode': response.Data[i].PaymentMode,
                            'Amount': response.Data[i].Amount,
                            'PaymentDate': response.Data[i].PaymentDate,
                            'ChequeNumber': response.Data[i].ChequeNumber,
                            'ChequeDate': response.Data[i].ChequeDate,
                            'BankName': response.Data[i].BankName,
                            'PaymentType': response.Data[i].PaymentType,
                            'TransactionID': response.Data[i].TransactionID,
                            'EntryDate': response.Data[i].EntryDate,
                            'ModifyDate': response.Data[i].ModifyDate,                            
                            'PaidAmount': response.Data[i].PaidAmount,
                            'balance': response.Data[i].balance,
                        };
                        //console.log(newObject); console.log(response.Data[i]);
                        $scope.data.newDataList.push(newObject);
                    }

                    $("#dvjson").excelexportjs({
                        containerid: "dvjson"
                           , datatype: 'json'
                           , dataset: $scope.data.newDataList
                           , columns: getColumns($scope.data.newDataList)     
                    });             
                }else{
                    ErrorPopup("Sorry! Data not found to export.");
                }
        });       
    }


    $scope.calculate_notes_amount = function(Note)
    {        
        if($("#"+Note+"_Note").val() != "")
        {
            var a = Note * $("#"+Note+"_Note").val();
            a = parseFloat(a);            
            $("#"+Note+"_Cash").val(a);
        }
        else
        {
            $("#"+Note+"_Cash").val("");
        }

        $scope.calculate_notes_amount_total();
    }


    $scope.calculate_notes_amount_total = function()
    {
        var total = 0;

        $(".NotesCashAmount:visible").each(function()
        {
            var a = $(this).val();           

            if(a != "" && a > 0)
            {
                a = parseFloat(a);
                total = total + a;
            }
        });

        $("#TotalCash").val(total);
        $(".TotalCashTop").html(" (" + total + ")");
    }

}); 

function getCategoryFilterData() {
    var stream = $("#stream").val();
    if(stream){
        angular.element(document.getElementById('fee-body')).scope().getCategoryFilterData(stream);
    }
}

function getStudentDetail(UserGUID){
    angular.element(document.getElementById('fee-body')).scope().getStudentDetail(UserGUID);
}


function FilterCollectionList(CourseGUID="",BatchGUID="")
{
    if(CourseGUID.length)
    {
       angular.element(document.getElementById('fee-body')).scope().getBatchByCourse(CourseGUID); 
    }
}


function search_records()
{
    angular.element(document.getElementById('fee-body')).scope().getList(1); 
}

function clear_search_records()
{    
    $("#CategoryGUID, #BatchGUID, #Keyword, #startDate, #endDate").val("");    

    search_records();
}