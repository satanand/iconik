app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.pageSize = 30;

    $scope.categoryDataList = [];

    $scope.filterData = [];

    $scope.data.dataList = [];

    $scope.formDataCheck = false;

    $scope.hidden = true;

    $scope.PostGUID;

    $scope.selectedOption;

    $scope.API_URL = API_URL;

    $scope.ParentCategoryGUID;

    $scope.ParentCategoryGUIDIndex;

    $scope.subjectTemplateURLAdd;

    $scope.CategoryGUID;

    $scope.SubCategoryGUID = "";

    ////console.log( $scope.API_URL);

    $scope.Media = [];

    $scope.subCategoryDataList = [];

    //$scope.data.ParentCategoryGUID = ParentCategoryGUID;
    $( "#filterFromDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterToDate" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#filterToDate" ).datepicker({
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#filterFromDate" ).datepicker( "option", "maxDate", selectedDate );
      }
    });



    $scope.deleteQuestionPaper = function(Position,EntityGUID) {

        $scope.deleteDataLoading = true;

        $scope.data.Position = Position;

        alertify.confirm('Are you sure you want to delete?', function() {

            var data = 'SessionKey=' + SessionKey + '&EntityGUID=' + EntityGUID;

            $http.post(API_URL + 'admin/entity/delete', data, contentType).then(function(response) {

                var response = response.data;

                if (response.ResponseCode == 200) { /* success case */

                    SuccessPopup(response.Message);

                    $scope.data.dataList.splice($scope.data.Position, 1); /*remove row*/

                    $scope.data.totalRecords--;

                    $timeout(function(){ 

                        //$('.modal-header .close').click();

                       // location.reload();

                    });

                } else {

                    ErrorPopup(response.Message);

                }

                if ($scope.data.totalRecords == 0) {

                    $scope.data.noRecords = true;

                }

            });

        }).set('labels', {

            ok: 'Yes',

            cancel: 'No'

        });

        $scope.deleteDataLoading = false;

    }


    $scope.applyFilter = function ()
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getCategoryList();
        $scope.getQuestionPaperlist1(1);
    }



    /*get category*/

    $scope.getCategoryFilterData = function (ParentCategoryGUIDIndex,addSelected="")

    {

        //console.log(addSelected);    

        $scope.filterData = [];

        var options = "";        

        $scope.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;

        $("#ParentCategoryGUIDIndex").val(ParentCategoryGUIDIndex);

        if(ParentCategoryGUIDIndex){ //////console.log(ParentCategoryGUIDIndex);

            if($scope.categoryDataList){

                $scope.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;

                $scope.filterData.ParentCategoryName = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryName;

                if($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories){

                    var SubCategories = $scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records;

                    if(addSelected == 'addSelected'){

                        var options = '<select  name="CategoryGUID" class="form-control subject" onchange="setSubject(this.value)" required=""><option value="">Select Subject</option>';

                    }else if(addSelected == 'getQuestionPaperlist'){

                        var options = '<select id="subject" name="CategoryGUIDs" onchange="filterQuestionPaperList(this.value)"  class="form-control"><option value="">Select Subject</option>';

                    }else if(addSelected == 'fromQuestionPaperList'){

                        var options = '<select  name="CategoryGUID" onchange="getAvailableQuestion(this.value)"  id="subjectGUID"  class="form-control subject" required=""><option value="" selected>Select Subject</option>';

                        //console.log(options);

                    }

                    else{

                        var options = '<select  name="CategoryGUID" onchange="getAvailableQuestion(this.value)"  id="subject"  class="form-control subject" required=""><option value="" selected>Select Subject</option>';

                    }                    

                    for (var i in SubCategories) {

                        $scope.filterData.push($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records);

                        options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               

                    } 

                    options += '</select>';

                    //console.log($scope.filterData);

                    if(options){  //////console.log(options);

                        // $("#subjects_section").html(options);

                        // $("#subcategory_section").html(options);

                        // $("#link_add_subject").show();

                        // $timeout(function(){            

                        //        $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

                        //    }, 200);

                    }else{

                        $("#link_add_subject").show();

                    }

                }

            }

        }else{

            var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" required=""><option value="">Select Subject</option></select>';

            $("#subcategory_section").html(options);

            $("#link_add_subject").show();

        }

    }





    $scope.getBatch = function(){

        var data = 'SessionKey='+SessionKey;   

        $scope.batch = [];     

        $http.post(API_URL+'batch/getBatch', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.batch = response.Data.Records; 

            } //////console.log($scope.categoryDataList);

        });

    }





    $scope.getSubjectByBatch = function(BatchGUID){

        $scope.BatchGUID = BatchGUID;

        var data = 'SessionKey='+SessionKey+'&BatchGUID='+BatchGUID;   

        $scope.subject = [];     

        $http.post(API_URL+'questionpaper/getSubjectByBatch', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.subject = response.Data.Records; 

            } console.log($scope.subject);

        });

    }





    /*get subject*/

    $scope.getCategorySubjects = function (ParentCategoryGUIDIndex,apiCall="")

    {        

        $scope.filterData = [];

        var options = "";        

        $scope.ParentCategoryGUIDIndex = ParentCategoryGUIDIndex;

        $("#ParentCategoryGUIDIndex").val(ParentCategoryGUIDIndex);

        if(ParentCategoryGUIDIndex){ //////console.log(ParentCategoryGUIDIndex);

            if($scope.categoryDataList){

                $scope.ParentCategoryGUID = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryGUID;

                $scope.filterData.ParentCategoryName = $scope.categoryDataList[ParentCategoryGUIDIndex].CategoryName;

                if(apiCall == 'getQuestionPaperStatistics'){

                    $scope.getQuestionPaperStatistics($scope.ParentCategoryGUID);

                    $("#ParentCategoryGUID").val(ParentCategoryGUID);

                }

                if($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories){

                    var SubCategories = $scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records;

                    var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control subject"  onchange="filterQuestionPaperStatistics(this.value)"><option value="">Select Subject</option>';

                    for (var i in SubCategories) {

                        $scope.filterData.push($scope.categoryDataList[ParentCategoryGUIDIndex].SubCategories.Records);

                        options += '<option value="'+SubCategories[i].CategoryGUID+'">'+SubCategories[i].CategoryName+'</option>';               

                    } 

                    options += '</select>';

                    if(options){

                        $("#subcategory_section").html(options);

                        $("#subjects_section").html(options);

                        $("#link_add_subject").show();

                         $scope.$apply();

                    }else{

                        $("#link_add_subject").show();

                    }

                }

            }

        }else{

            if(apiCall == 'getQuestionPaperStatistics'){

                $scope.getQuestionPaperStatistics();

                $("#ParentCategoryGUID").val(ParentCategoryGUID);

            }

            if(apiCall == 'getQuestionsList'){

                $scope.getQuestionsList();

                $("#ParentCategoryGUID").val(ParentCategoryGUID);

            }

            var options = '<select  name="CategoryGUIDs" id="subject"  class="form-control" onchange="formDataCheck()" onchange="check_subject_available()" required=""><option value="">Select Subject</option></select>';

            $("#subcategory_section").html(options);

            $("#link_add_subject").show();

        }

    }





    /*get Course List*/

    $scope.getCategoryList = function (categoryGUID="")

    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        var data = 'SessionKey='+SessionKey;

        $scope.categoryDataList = [];

        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Records) {

                    for(var j in response.Data.Records[i].SubCategories.Records){

                        if(categoryGUID.length > 0){

                            if(response.Data.Records[i].SubCategories.Records[j].CategoryGUID == categoryGUID){

                                $scope.ParentCategoryGUIDIndex = j;

                            }

                        }

                        //////console.log(response.Data.Records[i].SubCategories.Records[j].CategoryGUID);

                        $scope.categoryDataList.push(response.Data.Records[i].SubCategories.Records[j]);

                        

                    } 

                }          

            } ////////console.log($scope.categoryDataList);

        });

    }





    $scope.getSubCategoryList = function (ParentCategoryGUID){

        //console.log(ParentCategoryGUID);

        $scope.subCategoryDataList = [];

        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID;

        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                $scope.subCategoryDataList = response.Data.Records; 

            } ////////console.log($scope.categoryDataList);

        });

        //console.log($scope.subCategoryDataList);

    }





    $scope.getQuestionPaperlist1 = function (check="",type="",val="")

    {

        $scope.questionPaperList = 1; 

        if(check != 1){

            if ($scope.data.listLoading || $scope.data.noRecords) return;

        }

        $scope.data.listLoading = true;       





        if(check == 1){

            $scope.data.dataList = [];

            $scope.data.pageNo = 1;

            $scope.data.noRecords = false;

            if(type=='ParentCat'){

                var CourseGUID = $("#Courses").val();

                var SubjectGUID = "";

                $scope.CourseGUID =  CourseGUID;  

                $scope.SubjectGUID =  SubjectGUID; 

                $scope.getSubCategoryList(CourseGUID);

                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

            }else{

                var CourseGUID = $("#Courses").val();

                var SubjectGUID = $("#subject").val();

                $scope.CourseGUID =  CourseGUID;  

                $scope.SubjectGUID =  SubjectGUID; 

                $scope.getSubCategoryList(CourseGUID);

                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

            }            

            $scope.data.count++; 

            $scope.data.dataList = [];             

        }else{

            //$scope.data.pageNo = 1;

            $scope.data.count == 0;

            if($("#subject").val() == ""){ 

                var SubjectGUID = $scope.getUrlParameter('CategoryGUID');

                var CourseGUID = $scope.getUrlParameter('parentCategoryGUID');

                $scope.getSubCategoryList(CourseGUID);

            }else{

                var CourseGUID = $("#Courses").val();

                var SubjectGUID = $("#subject").val();

                $scope.getSubCategoryList(CourseGUID);

            } 

            $scope.SubjectGUID =  SubjectGUID;    

            $scope.CourseGUID =  CourseGUID;       

           // $scope.getCategoryList(CourseGUID); 

            var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubjectGUID+'&CourseGUID='+CourseGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize;

        }

        
        data = data + "&PageName=QPList";
        ////console.log(check);

        $http.post(API_URL+'questionpaper/getQuestionsPaperList', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.length > 0){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataList.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;

                    $scope.data.listLoading = false; 

                }else{

                    $scope.data.noRecords = true;

                    $scope.data.listLoading = false; 

                }

                        

                $scope.data.listLoading = false;   //console.log(check);

                if(check != 1 && $scope.data.pageNo == 2){              

                    $timeout(function(){

                        $timeout(function(){

                            //if($scope.ParentCategoryGUIDIndex){

                               //console.log($scope.SubjectGUID);

                               // $("#Courses").val($scope.CourseGUID);

                               // $("#subject").val($scope.SubjectGUID); 

                               $scope.data.listLoading = false; 

                           // }                       

                        }, 1000);

                    }, 1000);

                }

                //setTimeout(function(){ tblsort();  $scope.$apply(); }, 1000);

               

        });

    }





    $scope.getList = function (check="")

    {

        $scope.getCategoryList();

        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        if(check == 1){

            $scope.getQuestionPaperlist1();

        }else{

            $scope.getQuestionPaperStatistics();

        }

    }





    /*list append*/

    /*list append*/

    $scope.getQuestionPaperStatistics = function (courseID="",subjectID="")

    {

        //if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.listLoading = true;

        $scope.data.dataList = [];

        var data = 'SessionKey='+SessionKey+'&SubjectGUID='+subjectID+'&CourseGUID='+courseID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();

        $http.post(API_URL+'questionpaper/getQuestionPaperStatistics', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    for (var i in response.Data) {

                       $scope.data.dataList.push(response.Data[i]);

                    }

                    $scope.data.pageNo++;               

               }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort();  $scope.$apply(); }, 1000);

        });

    }



    /*load add form*/



    $scope.loadFormAdd = function (Position="", CategoryGUID="")
    {

        $scope.data.pageLoading = true;

       // $scope.getCategoryList();



        $scope.templateURLAdd = PATH_TEMPLATE+'questionpaper/generate_question_paper.htm?'+Math.random();



        if(CategoryGUID.length > 0){

           $scope.fromQuestionsList = 1;

           $scope.getAvailableQuestion($scope.SubjectGUID)

        }else{

           $scope.fromQuestionsList = 0;

           $scope.question = {"Easy":0, "Moderate":0, "High":0, "MockTest":0, "Quiz": 0, "Test": 0, "Contest": 0, "Total":0}; 

        }



        $('#add_model').modal({show:true});

        $scope.data.pageLoading = false;



        $timeout(function(){            

            $('#StartDates,#EndDate').datetimepicker({
                format: 'dd-mm-yyyy H:i'
            });


            tinymce.init({

                selector: '#Instruction'

            });

            

           // $('#StartDates,#EndDate').hide();

           ///$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



        }, 1000);



    }





     /*load add form*/

    $scope.loadAssignPaper1 = function (position,QtPaperGUID)

    {

        if(QtPaperGUID.length){

            $scope.getBatch();

            $scope.QtPaperGUID = QtPaperGUID;
            console.log(PATH_TEMPLATE+module);

            var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;   

            $scope.batch = [];     

            $http.post(API_URL+'questionpaper/getBatchByPaper', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;

                    $scope.batch = response.Data.Records; 

                } console.log($scope.batch);

            });

            $scope.templateURLAssign = PATH_TEMPLATE+'questionpaper/assign_paper.htm?'+Math.random();

            $scope.data.pageLoading = true;   

            $('#assign_model').modal({show:true}); 

            $scope.data.pageLoading = false; 



            $timeout(function(){        

                var d = new Date();

                var month = d.getMonth();

                var day = d.getDate();

                var year = d.getFullYear();

                $("#Date").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',d);
                
               // $("#StartTime").datetimepicker({format: 'H:i',minView: 2});

                 $('#StartTime').timepicker({
                    format: 'H:mm',minView: 1
                });

            }, 600);

        }        

    }





     /*load edit form*/

    $scope.loadFormEdit = function (Position, QtPaperGUID)

    {

        $scope.data.Position = Position;

        

        $scope.templateURLEdit = PATH_TEMPLATE+'questionpaper/edit_question_paper.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var SubjectGUID = $("#subject").val();

        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;

        $http.post(API_URL+'questionpaper/getQuestionsPaperByID', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data.length){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data[0];  

                $scope.getSubCategoryList($scope.formData.CourseGUID);

                $scope.getAvailableQuestionOnEdit(QtPaperGUID,$scope.formData.SubjectGUID,$scope.formData.QuestionsGroupID);

                if($scope.formData.NegativeMarks) {

                   $scope.formData.NegativeMarks = $scope.formData.NegativeMarks.split('.');

                }else{

                    $scope.formData.NegativeMarks[0] = 0;

                    $scope.formData.NegativeMarks[1] = 0;

                } 

                $('#edit_model').modal({show:true});

                $timeout(function(){

                    $("#QtPaperGUID").val($scope.formData.QtPaperGUID);                

                }, 1000);

                $timeout(function(){

                    tinymce.init({

                        selector: '#Instruction'

                    });

                }, 1000);

            }else{                

                $scope.data.pageLoading = false;

                ErrorPopup("Not available for edit!");

            }

        });

    }





    $scope.htmlToPlaintext = function(text) {

      $scope.answer =  text ? String(text).replace(/<[^>]+>/gm, '') : '';

    }





    
    $scope.exportQuestionPaper = function ()
    {
        var QtPaperGUID = $scope.getUrlParameter('QtPaperGUID');

        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;
        
        $http.post("questionpaper/paper_export", data, contentType).then(function(Response) 
        {
            var response = response.data;

            if(response.ResponseCode==200){

                $scope.formData = response.Data;

                $scope.data.pageLoading = false;

                if(!response.Data.question_answer.length){

                    $scope.data.noRecords = true;

                }

            }
            else
            {                

                $scope.data.noRecords = true;

                $scope.data.pageLoading = false;

            }

                              
        });

    }


      /*load edit form*/

    $scope.viewQuestionPaper = function ()

    {
        if(!$scope.data.SecondLevelPermission.length){
            $scope.getSecondLevelPermission();
        }

        $scope.data.pageLoading = true;

        var QtPaperGUID = $scope.getUrlParameter('QtPaperGUID');

        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;

        $http.post(API_URL+'questionpaper/getQuestionAnswerList', data, contentType).then(function(response) 
        {

            var response = response.data;

            if(response.ResponseCode==200){

                $scope.formData = response.Data;

                $scope.data.pageLoading = false;

                if(!response.Data.question_answer.length){

                    $scope.data.noRecords = true;

                }

            }else{                

                $scope.data.noRecords = true;

                $scope.data.pageLoading = false;

            }

        });

    }





    $scope.addBlockQuestion = function (position,QtPaperGUID,QtBankGUID)

    {

        $scope.data.position = position;

        console.log($scope.data.position);

        var QtPaperGUID = $scope.getUrlParameter('QtPaperGUID');

        $.confirm({

            title: 'Confirm!',

            content: 'Are you sure?',

            buttons: {

                removefromcurrent: {

                    text: 'Remove from here', // With spaces and symbols

                    action: function () {

                        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID+'&QtBankGUID='+QtBankGUID+'&addNext=No';

                        $http.post(API_URL+'questionpaper/addBlockQuestion', data, contentType).then(function(response) {

                            var response = response.data;

                            if(response.ResponseCode==200){

                                SuccessPopup('Removed Successfully!');

                                $scope.data.pageLoading = true;

                                $scope.formData.question_answer.splice($scope.data.Position, 1);

                                $scope.data.pageLoading = false;

                            }else{                

                                ErrorPopup(response.Message);

                            }

                        });

                    }
                },
                removefromall: {

                    text: 'Remove from all papers', // With spaces and symbols

                    action: function () {

                        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID+'&QtBankGUID='+QtBankGUID+'&addNext=Yes';

                        $http.post(API_URL+'questionpaper/addBlockQuestion', data, contentType).then(function(response) {

                            var response = response.data;

                            if(response.ResponseCode==200){ //console.log(response.Data.QtBankID);

                                if(response.Data.QtBankID){

                                    $.confirm({

                                        title: 'Confirm!',

                                        content: response.Message,

                                        buttons: {

                                            yes: function () {

                                                var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID+'&QtBankGUID='+QtBankGUID+'&addQtBankID='+response.Data.QtBankID+'&QuestionsLevel='+response.Data.QuestionsLevel;

                                                $http.post(API_URL+'questionpaper/replaceQuestion', data, contentType).then(function(response) {

                                                    var response = response.data;

                                                    if(response.ResponseCode==200){

                                                        SuccessPopup('Replaced Question Successfully!');

                                                        $scope.data.pageLoading = true;

                                                       // $scope.viewQuestionPaper();

                                                        $scope.formData.question_answer[$scope.data.position] = response.Data[0];

                                                        console.log($scope.formData.question_answer);

                                                        $scope.data.pageLoading = false;

                                                    }else{                

                                                        ErrorPopup(response.Message);

                                                    }

                                                });

                                            },

                                            cancel: function () {

                                               

                                            },

                                        }

                                    });

                                }else{

                                    if(response.Data.length > 0){

                                        SuccessPopup('Replaced Question Successfully!');

                                        $scope.data.pageLoading = true;

                                       // $scope.viewQuestionPaper();

                                        $scope.formData.question_answer[$scope.data.position] = response.Data[0];

                                        console.log($scope.formData.question_answer);

                                        $scope.data.pageLoading = false;

                                    }else{

                                        ErrorPopup(response.Message);

                                    }

                                }

                            }else{                

                                ErrorPopup(response.Message);

                            }

                        });

                    }

                },

                cancel: function () {

                   

                },

            }

        });

    }



    $scope.removeQuestion = function (position,QtPaperGUID,QtBankGUID,QuestionsMarks)

    {
       // alert("fdsfdsf");
        $scope.data.position = position;
        
        console.log($scope.data.position);

        var QtPaperGUID = $scope.getUrlParameter('QtPaperGUID');

        $.confirm({

            title: 'Confirm!',

            content: 'Are you sure?',

            buttons: {

                removefromcurrent: {

                    text: 'Remove from here', // With spaces and symbols

                    action: function () {

                        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID+'&QtBankGUID='+QtBankGUID+'&check=Only';

                        $http.post(API_URL+'questionpaper/removeQuestion', data, contentType).then(function(response) {

                            var response = response.data;

                            if(response.ResponseCode==200){

                                SuccessPopup('Removed Successfully!');

                                $scope.data.pageLoading = true;

                                $scope.formData.Total_marks = $scope.formData.Total_marks - QuestionsMarks;

                                $scope.formData.TotalQuestions = $scope.formData.TotalQuestions - 1;

                                $scope.formData.question_answer.splice($scope.data.Position, 1);

                                $scope.data.pageLoading = false;

                            }else{                

                                ErrorPopup(response.Message);

                            }

                        });

                    }
                },
                removefromall: {

                    text: 'Remove from all papers', // With spaces and symbols

                    action: function () {

                        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID+'&QtBankGUID='+QtBankGUID+'&check=All';

                        $http.post(API_URL+'questionpaper/removeQuestion', data, contentType).then(function(response) {

                            var response = response.data;

                            if(response.ResponseCode==200){

                                SuccessPopup('Removed Successfully!');

                                $scope.data.pageLoading = true;

                                $scope.formData.Total_marks = $scope.formData.Total_marks - QuestionsMarks;

                                $scope.formData.TotalQuestions = $scope.formData.TotalQuestions - 1;

                                $scope.formData.question_answer.splice($scope.data.Position, 1);

                                $scope.data.pageLoading = false;

                            }else{                

                                ErrorPopup(response.Message);

                            }

                        });

                    }

                },

                cancel: function () {

                   

                },

            }

        });

    }





    $scope.addAssignPaper = function ()

    {

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='assign_paper']").serialize();

        //console.log(data);

        $http.post(API_URL+'questionpaper/addAssignPaper', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){

                SuccessPopup(response.Message);

                $scope.data.pageLoading = true;

                $scope.viewQuestionPaper();

                $scope.data.pageLoading = false;

                $('#assign_model .close').click();

            }else{                

                ErrorPopup(response.Message);

            }

        });

    }





    /*load delete form*/

    $scope.loadFormView = function (Position, EntityGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+'questionpaper/delete_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'admin/entity/getFlaggedContent', 'SessionKey='+SessionKey+'&EntityGUID='+EntityGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#view_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }

        });



    }





    $scope.getAvailableQuestion = function(SubCategoryGUID="",QuestionGroup=""){

      // //console.log(SubCategoryGUID);

        //if(SubCategoryGUID.length > 0){ ////console.log(SubCategoryGUID);

            if(QuestionGroup.length > 0){

                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubCategoryGUID+'&QuestionGroup='+QuestionGroup;

            }else{

                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubCategoryGUID;

            }

            

            $http.post(API_URL+'questionpaper/getQuestionsCountByCategory', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data){ /* success case */

                  $scope.question = response.Data;

                }else{

                    $scope.question = {"Easy":0, "Moderate":0, "High":0, "MockTest":0, "Quiz": 0, "Test": 0, "Contest": 0, "Total":0};

                }

            });

        // }else{  //alert(SubCategoryGUID);

        //     $scope.question = {Easy:"0", Moderate:"0", High:"0"};

        //     //console.log($scope.question);

        // }

    }





    $scope.getAvailableQuestionOnEdit = function(QtPaperGUID,SubCategoryGUID="",QuestionGroup=""){

      // //console.log(SubCategoryGUID);

        //if(SubCategoryGUID.length > 0){ ////console.log(SubCategoryGUID);

            if(QuestionGroup.length > 0){

                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubCategoryGUID+'&QuestionGroup='+QuestionGroup+'&QtPaperGUID='+QtPaperGUID;

            }else{

                var data = 'SessionKey='+SessionKey+'&SubjectGUID='+SubCategoryGUID+'&QtPaperGUID='+QtPaperGUID;

            }

            

            $http.post(API_URL+'questionpaper/getQuestionsCountOnEdit', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data){ /* success case */

                  $scope.question = response.Data;

                }else{

                    $scope.question = {"Easy":0, "Moderate":0, "High":0, "MockTest":0, "Quiz": 0, "Test": 0, "Contest": 0, "Total":0};

                }

            });

        // }else{  //alert(SubCategoryGUID);

        //     $scope.question = {Easy:"0", Moderate:"0", High:"0"};

        //     //console.log($scope.question);

        // }

    }





    $scope.checkAndAdd = function(check){


        


        var QuestionsLevelEasy = $("#QuestionsLevelEasy").val();

        var QuestionsLevelModerate = $("#QuestionsLevelModerate").val();

        var QuestionsLevelHigh = $("#QuestionsLevelHigh").val();

        var TotalLevelquestion = parseInt(QuestionsLevelEasy)+parseInt(QuestionsLevelModerate)+parseInt(QuestionsLevelHigh);

        var TotalQuestions = $("#TotalQuestions").val();

        var questions = $scope.question;

        if(TotalQuestions > questions.Total){

            alert("Sorry! Total question value is greater than total available questions.");

            return false;

        }



        if(QuestionsLevelEasy > questions.Easy){

            alert("Sorry! Question level easy value is greater than total available easy questions.");

            return false;

        }else if(QuestionsLevelModerate > questions.Moderate){

            alert("Sorry! Question level moderate value is greater than total available moderate questions.");

            return false;

        }else if(QuestionsLevelHigh > questions.High){

            alert("Sorry! Question level high value is greater than total available high questions.");

            return false;

        }else if(TotalLevelquestion < TotalQuestions){

            alert("Total question is greater than total of questions from question level.")

        }else if(TotalLevelquestion > TotalQuestions){

            alert("Total question is less than total of questions from question level.")

        }else{        

            $scope.editDataLoading = true;
            $scope.loadeSubmit = true;

            tinyMCE.triggerSave();

            var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();



            $http.post(API_URL+'questionpaper/createQusetionPaper', data, contentType).then(function(response)
            {

                var response = response.data;

                $scope.editDataLoading = false;
                $scope.loadeSubmit = false;

                if(response.ResponseCode==200){ /* success case */               



                    SuccessPopup(response.Message);



                    if(check == 1){

                        $scope.getQuestionPaperlist1(1);

                    }else{

                        $scope.getQuestionPaperStatistics();

                    }



                    $timeout(function(){            



                       $('#add_model .close').click();

                 

                    }, 200);



                }else{



                    ErrorPopup(response.Message);



                }



                

            });

        }

    }





    $scope.checkAndEdit = function(){

        $scope.editDataLoading = true;
 $scope.loadeSubmit = true;

        console.log($scope.question); console.log($scope.formData);

        var QuestionsLevelEasy = $("#QuestionsLevelEasy").val();

        var QuestionsLevelModerate = $("#QuestionsLevelModerate").val();

        var QuestionsLevelHigh = $("#QuestionsLevelHigh").val();

        var TotalLevelquestion = parseInt(QuestionsLevelEasy)+parseInt(QuestionsLevelModerate)+parseInt(QuestionsLevelHigh);

        var TotalQuestions = $("#TotalQuestions").val();

        var questions = $scope.question;

        if(TotalQuestions > parseInt(TotalQuestions)+parseInt($scope.question.Total)){

            alert("Sorry! Total question value is greater than total available questions.");

            return false;

        }



        console.log(parseInt(TotalQuestions)+parseInt($scope.question.Total));



        if(QuestionsLevelEasy > parseInt(questions.Easy)+parseInt($scope.formData.QuestionsLevelEasy)){

            alert("Sorry! Question level easy value is greater than total available easy questions.");

            return false;

        }else if(QuestionsLevelModerate > parseInt(questions.Moderate)+parseInt($scope.formData.QuestionsLevelModerate)){

            alert("Sorry! Question level moderate value is greater than total available moderate questions.");

            return false;

        }else if(QuestionsLevelHigh > parseInt(questions.High)+parseInt($scope.formData.QuestionsLevelHigh)){

            alert("Sorry! Question level high value is greater than total available high questions.");

            return false;

        }else if(TotalLevelquestion < parseInt(TotalQuestions)){

            alert("Total question is greater than total of questions from question level.")

        }else if(TotalLevelquestion > parseInt(TotalQuestions)){

            alert("Total question is less than total of questions from question level.")

        }else{        



            tinyMCE.triggerSave();

            var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();



            $http.post(API_URL+'questionpaper/editQusetionPaper', data, contentType).then(function(response) {

                var response = response.data;



                if(response.ResponseCode==200){ /* success case */               



                    SuccessPopup(response.Message);

                    $scope.data.dataList[$scope.data.Position] = response.Data[0];

                    $timeout(function(){            



                       $('#edit_model .close').click();


$scope.editDataLoading = false;
 $scope.loadeSubmit = false;
                       //$scope.getQuestionPaperlist1(1);



                    }, 200);



                }else{



                    ErrorPopup(response.Message);



                }



          $scope.editDataLoading = false;
         $scope.loadeSubmit = false;

            });

        }

    }



    $scope.changeStatus = function(position,QtPaperGUID){

        $scope.data.position = position;      

       // alert($('input[name="switch-status"]:checked').length);  

        $.confirm({

            title: 'Confirm!',

            content: 'Are you sure to change the status of this question?',

            buttons: {

                Ok: function () {

                    if($('input[name="switch-status"]:checked').length == 2){

                        var StatusID = 2;

                    }else{

                        var StatusID = 6;

                    }

                    if($('input[name="switch-status'+position+'"]:checked').length > 0){

                        var StatusID = 2;

                    }else{

                        var StatusID = 6;

                    }  

                    var data = 'SessionKey=' + SessionKey + '&QtPaperGUID=' + QtPaperGUID+ '&StatusID=' + StatusID;

                    $http.post(API_URL+'questionpaper/changeStatus', data, contentType).then(function(response) {

                        var response = response.data;

                        if(response.ResponseCode==200){ /* success case */ 

                        //console.log(response.Data[0]);     

                            $scope.data.dataList[$scope.data.Position] = response.Data[0];

                            SuccessPopup(response.Message);

                            $timeout(function(){ 

                                $('#edit_model .close').click(); 

                            }, 100);

                        }else{

                            ErrorPopup(response.Message);

                        }

                        //$scope.addDataLoading = false;          

                    });

                },

                cancel: function () { 

                console.log($('input[name="switch-status'+position+'"]:checked').length);                   

                    if($('input[name="switch-status'+position+'"]:checked').length == 0){                        

                        $('input[name="switch-status'+position+'"]').attr("checked",true);

                    }else{

                        $('input[name="switch-status'+position+'"]').attr("checked",false);

                    }                 

                },

            }

        });        

    }




    /*-----------------------------------------------------------------------
    Scholarship Module List
    -----------------------------------------------------------------------*/
    $scope.getScholarshipList = function (IsSet="")
    {
        if(!$scope.data.SecondLevelPermission.length)
        {
            $scope.getSecondLevelPermission();
        }
        
        if(IsSet == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }    

        $scope.data.listLoading = true;        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();     

        $http.post(API_URL+'questionpaper/getScholarshipList', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Data) 
                {
                    $scope.data.dataList.push(response.Data.Data[i]);
                }                 
                
                $scope.data.pageNo++;                
            }
            else
            {
                $scope.data.noRecords = true;
            }            

            $scope.data.listLoading = false;
        });           
    }


    $scope.loadFormSchedule = function (Position="", CategoryGUID="")
    {
        $scope.data.pageLoading = true;

        $scope.templateURLAdd = PATH_TEMPLATE+'scholarshiptest/schedule.htm?'+Math.random();

        $('#add_model').modal({show:true});

        $scope.data.pageLoading = false;

        $timeout(function()
        {
            $("#ScheduleDate").datepicker({"dateFormat":"dd-mm-yy", minDate:0});

            $("#ScheduleDuration").timepicker({
                "timeFormat":"H:i", 
                "minTime":"01:00:00", 
                "maxTime":"12:00:00"
            });
            
            
            $('#StartTime').timepicker({
                format: 'H:mm',
                minView: 1
            });

            
            $('#ActivatedFromTime, #ActivatedToTime').timepicker({
                format: 'H:mm',
                minView: 1
            });

        }, 1000);
    }


    $scope.ScholarshipTestSave = function()
    {
        $scope.ScholarshipDataLoading = true;        

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_schedule_form']").serialize();

        $http.post(API_URL+'questionpaper/ScholarshipTestSave', data, contentType).then(function(response)
        {
            var response = response.data;
            
            $scope.ScholarshipDataLoading = false;            

            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                $timeout(function()
                {
                    window.location.href = window.location.href;

                }, 3000);
            }
            else
            {
                ErrorPopup(response.Message);
            }
        });
    }


    $scope.loadFormScholarshipEdit = function (RowContent)
    {
        $scope.templateURLEdit = PATH_TEMPLATE+'scholarshiptest/schedule_edit.htm?'+Math.random();

        $('#edit_model').modal({show:true});

        $scope.formData = RowContent;

        $timeout(function()
        {
            $("#ScheduleDate").datepicker({"dateFormat":"dd-mm-yy", minDate:0});

            $("#ScheduleDuration").timepicker({
                "timeFormat":"H:i", 
                "minTime":"01:00:00", 
                "maxTime":"12:00:00"
            });
            
            
            $('#StartTime').timepicker({
                format: 'H:mm',
                minView: 1
            });

            
            $('#ActivatedFromTime, #ActivatedToTime').timepicker({
                format: 'H:mm',
                minView: 1
            });

        }, 1000);

    }


    $scope.ScholarshipTestSaveEdit = function()
    {
        $scope.ScholarshipDataLoading = true;        

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_schedule_form']").serialize();

        $http.post(API_URL+'questionpaper/ScholarshipTestSaveEdit', data, contentType).then(function(response)
        {
            var response = response.data;
            
            $scope.ScholarshipDataLoading = false;            

            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                $timeout(function()
                {
                    window.location.href = window.location.href;

                }, 3000);
            }
            else
            {
                ErrorPopup(response.Message);
            }
        });
    }


    $scope.getScholarshipApplicants = function (IsSet="")
    {
        if(!$scope.data.SecondLevelPermission.length)
        {
            $scope.getSecondLevelPermission();
        }
        
        if(IsSet == 1)
        {
            $scope.data.dataList = [];
            $scope.data.pageNo = 1;
        }
        else
        {
            if ($scope.data.listLoading || $scope.data.noRecords) return;
        }    

        $scope.data.listLoading = true;        

        var data = 'SessionKey='+SessionKey+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();     

        $http.post(API_URL+'questionpaper/getScholarshipApplicants', data, contentType).then(function(response)
        {
            var response = response.data;

            if(response.ResponseCode==200 && response.Data)
            {
                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data.Data) 
                {
                    $scope.data.dataList.push(response.Data.Data[i]);
                }                 
                
                $scope.data.pageNo++;                
            }
            else
            {
                $scope.data.noRecords = true;
            }            

            $scope.data.listLoading = false;
        });           
    }


    $scope.loadFormGeneratePaper = function(QtPaperID)
    {
        $scope.data.pageLoading = true;

        $scope.templateURLAdd = PATH_TEMPLATE+'scholarshiptest/generate_paper.htm?'+Math.random();
        
        var data = 'SessionKey='+SessionKey+'&QuestionsGroup=5&QtPaperID=0';     

        $http.post(API_URL+'questionpaper/getQuestionsCount', data, contentType).then(function(response) 
        {
            var response = response.data;

            $scope.data.pageLoading = false;

            if(response.ResponseCode==200 && response.Data)
            {
               $scope.data.QuestionCounts = response.Data.Counts;
               $scope.data.TestNameList = response.Data.TestNameList;              
            }   

            $('#add_model').modal({show:true});            

            $timeout(function()
            {
                tinymce.init({
                    selector: '#Instruction'
                });

            }, 1000);
        });        
    }


    $scope.loadFormGeneratePaperEdit = function(RowData, QtPaperID)
    {
        $scope.data.pageLoading = true;

        $scope.templateURLEdit = PATH_TEMPLATE+'scholarshiptest/generate_paper_edit.htm?'+Math.random();
        
        var data = 'SessionKey='+SessionKey+'&QuestionsGroup=5&QtPaperID='+QtPaperID;     

        $http.post(API_URL+'questionpaper/getQuestionsCount', data, contentType).then(function(response) 
        {
            var response = response.data;

            $scope.data.pageLoading = false;

            if(response.ResponseCode==200 && response.Data)
            {
               $scope.data.QuestionCounts = response.Data.Counts;
               $scope.data.TestNameList = response.Data.TestNameList;
               $scope.data.RowData = RowData;             
            }   

            $('#edit_model').modal({show:true});            

            $timeout(function()
            {
                tinymce.init({
                    selector: '#Instruction'
                });

            }, 1000);
        });        
    }


    $scope.GeneratePaperSave = function()
    {
        $scope.ScholarshipDataLoading = true;  

        tinyMCE.triggerSave();      

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'questionpaper/GeneratePaperSave', data, contentType).then(function(response)
        {
            var response = response.data;
           
            $scope.ScholarshipDataLoading = false;            

            if(response.ResponseCode==200)
            {
                SuccessPopup(response.Message);

                $timeout(function()
                {
                    if(response.Data.QtPaperGUID != "")
                    {
                        window.location.href = BASE_URL + "scholarshiptest/view_paper/?QtPaperGUID=" + response.Data.QtPaperGUID;
                    }
                    else
                    {
                        window.location.href = window.location.href;
                    }                  

                }, 3000);
            }
            else
            {
                ErrorPopup(response.Message);
            }
        });
    }


    $scope.GeneratePaperDelete = function(QtPaperGUID, QuestionsGroup) 
    {
        alertify.confirm('Are you sure you want to delete?', function() 
        {
            $scope.data.listLoading = true;

            var data = 'SessionKey=' + SessionKey + '&QtPaperGUID=' + QtPaperGUID + '&QuestionsGroup=' + QuestionsGroup;

            $http.post(API_URL + 'questionpaper/GeneratePaperDelete', data, contentType).then(function(response) 
            {
                $scope.data.listLoading = false;

                var response = response.data;

                if (response.ResponseCode == 200) 
                {
                    SuccessPopup(response.Message);                                        

                    $timeout(function()
                    { 
                        window.location.href = window.location.href;

                    }, "3000");
                } 
                else 
                {
                    ErrorPopup(response.Message);
                }
            });

        }).set('labels', {

            ok: 'Yes',

            cancel: 'No'
        });        
    }


    $scope.viewPaper = function()
    {
        if(!$scope.data.SecondLevelPermission.length)
        {
            $scope.getSecondLevelPermission();
        }

        $scope.data.pageLoading = true;

        var QtPaperGUID = $scope.getUrlParameter('QtPaperGUID');

        var data = 'SessionKey='+SessionKey+'&QtPaperGUID='+QtPaperGUID;

        $http.post(API_URL+'questionpaper/viewPaper', data, contentType).then(function(response) 
        {
            var response = response.data;

            if(response.ResponseCode==200)
            {
                $scope.formData = response.Data;

                $scope.data.pageLoading = false;

                if(!response.Data.question_answer.length){

                    $scope.data.noRecords = true;

                }

            }
            else
            {                

                $scope.data.noRecords = true;

                $scope.data.pageLoading = false;

            }

        });
    }

}); 







function getCategoryFilterData(ParentCategoryGUID,addSelected=""){

  if(addSelected.length > 0){

    angular.element(document.getElementById('question-body')).scope().getCategoryFilterData(ParentCategoryGUID,addSelected); 

  }else{

    angular.element(document.getElementById('question-body')).scope().getCategoryFilterData(ParentCategoryGUID); 

  }

}



function getAvailableQuestion(SubCategoryGUID, typeval = ""){

    if(typeval == 5){
        $(".scholarship-time").show();
        // $('#StartDates,#EndDate').datetimepicker({
        //     format: 'dd-mm-yyyy H:i',
        //     pickDate: false
        // });
    }else{
        $(".scholarship-time").hide();
    }

    var QtPaperGUID = $("#QtPaperGUID").val();

    if($("#QtPaperGUID").length > 0){

        var QtPaperGUID = $("#QtPaperGUID").val();

        if(SubCategoryGUID == 1){

            var categoryID = $("#subjectID").val();

            if($("#subjectID").val() || $("#subjectID").val()){

                var categoryID = $("#subjectID").val();

            }else{

                alert("Please select course and subject before it.");

                $("#QuestionsGroup").val('');

                return false;

            }

            var QuestionsGroup = $("#QuestionsGroup").val();

            angular.element(document.getElementById('question-body')).scope().getAvailableQuestionOnEdit(QtPaperGUID,categoryID,QuestionsGroup); 

        }

        else if(SubCategoryGUID.length > 0){

            angular.element(document.getElementById('question-body')).scope().getAvailableQuestionOnEdit(QtPaperGUID,SubCategoryGUID); 

        }else{

            $("input[name='QuestionsGroup[]']:checkbox").prop('checked',false);

            angular.element(document.getElementById('question-body')).scope().getAvailableQuestionOnEdit(QtPaperGUID); 

        }

    }else{

        console.log(SubCategoryGUID);

       if(SubCategoryGUID == 1){

            var categoryID = $("#subjectID").val();

            if($("#subjectID").val() || $("#subjectID").val()){

                var categoryID = $("#subjectID").val();

            }else{

                alert("Please select course and subject before it.");

                $("#QuestionsGroup").val('');

                return false;

            }

            var QuestionsGroup = $("#QuestionsGroup").val();

            console.log(QuestionsGroup);

            angular.element(document.getElementById('question-body')).scope().getAvailableQuestion(categoryID,QuestionsGroup); 

        }

        else if(SubCategoryGUID.length > 0){

            angular.element(document.getElementById('question-body')).scope().getAvailableQuestion(SubCategoryGUID); 

        }else{

            $("input[name='QuestionsGroup[]']:checkbox").prop('checked',false);

            angular.element(document.getElementById('question-body')).scope().getAvailableQuestion(); 

        } 

    }

}





function getAvailableQuestionOnEdit(SubCategoryGUID){

    ////console.log($("#add_model .subject").val());

    

    

}



$("#add_model").on("hidden.bs.modal", function () {

    tinymce.remove();

});





function getCategorySubjects(ParentCategoryGUIDIndex,apiCall){

  angular.element(document.getElementById('question-body')).scope().getCategorySubjects(ParentCategoryGUIDIndex,apiCall);  

}



function filterQuestionPaperStatistics(CategoryGUID){

  var ParentCategoryGUID = $("#ParentCategoryGUID").val();

  angular.element(document.getElementById('question-body')).scope().getQuestionPaperStatistics(ParentCategoryGUID,CategoryGUID);  

}





/* sortable - starts */

function tblsort() {



  var fixHelper = function(e, ui) {

    ui.children().each(function() {

        $(this).width($(this).width());

    });

    return ui;

}



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

        type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

        data: order,

        stop: function(response) {

            if (response.status == "success") {

                window.location.href = window.location.href;

            } else {

                alert('Some error occurred');

            }

        }

    });

}

/* sortable - ends */

}



function getSubjectByBatch(BatchGUID){

    angular.element(document.getElementById('question-body')).scope().getSubjectByBatch(BatchGUID);  

}



function filterQuestionPaperList(val,type=""){

    if(type.length > 0){

         $("#"+type).val(val);

    } 

    angular.element(document.getElementById('question-body')).scope().getQuestionPaperlist1(1,type,val);  

}



function getSubCategoryList(parentCategoryGUID){

    angular.element(document.getElementById('question-body')).scope().getSubCategoryList(parentCategoryGUID);

}


function setEventEndDateTime(date){     
    var d = new Date(date);
    d.setMinutes(d.getMinutes() + 5);
    $('#EndTime').timepicker({format: 'H:mm',minView: 1});
    //$('#EndTime').timepicker({format: 'H:i',pickDate: false }).timepicker('setStartDate',d);
}


function search_records(PgName)
{
    if(PgName == 'app')
    {
        angular.element(document.getElementById('question-body')).scope().getScholarshipApplicants(1);
    }
    else if(PgName == 'genpaper')
    {
        angular.element(document.getElementById('question-body')).scope().getScholarshipList(1);
    }
    else
    {
        angular.element(document.getElementById('question-body')).scope().getScholarshipList(1);
    }    
}


function clear_search_records(PgName)
{    
    if(PgName == 'app' || PgName == 'ppr' || PgName == 'genpaper')
    {
        $("#filterKeyword, #filterFromDate, #filterToDate").val("");    
    }   

    search_records(PgName);
}


function enableDisableTest(TypeSel)
{
    if(TypeSel == 1)
    {
        $("#ActivatedFromTime").removeAttr("disabled");
        $("#ActivatedToTime").removeAttr("disabled");
        $("#ActivatedToTime").removeAttr("ng-disabled");
        $("#ActivatedToTime").removeAttr("ng-disabled");

        $("#StartTime").val("");
        $("#StartTime").attr("disabled", "disabled");
    }
    else if(TypeSel == 2)
    {
        $("#StartTime").removeAttr("disabled");
        $("#StartTime").removeAttr("ng-disabled");
        
        $("#ActivatedFromTime, #ActivatedToTime").val("");

        $("#ActivatedFromTime").attr("disabled", "disabled");
        $("#ActivatedToTime").attr("disabled", "disabled");
    }
}