<style type="text/css">
.glyphicon {
    position: relative;
    top: -4px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    font-size: 20px;
    padding: 4px;
}
</style>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<header class="panel-heading" > <h1 class="h4">Fee Collection</h1> </header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" id="fee-body" ng-controller="PageController" ng-init="getFilterData()"><!-- Body -->

	<!-- Top container -->
	<div class="row clearfix mt-2 mb-2">
		<div class="col-md-8">
			<span class="float-left records d-none d-sm-block">
				<span class="h5">Total records: {{data.dataList.length}}</span>
			</span>
		</div>
		<div class="col-md-4" style="float:right;" align="right">
			<button ng-click="tableDatatoExcel()" class="btn btn-success btn-sm" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Export Data to Excel</button>
			<span id="dvjson"></span>	
			<button class="btn btn-success btn-sm ml-1" ng-click="loadFormAdd();" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add</button>			
		</div>
	</div>


	<form class="" id="filterForm" role="form" autocomplete="off" ><!--ng-submit="getList()"-->
		<div class="row clearfix mt-2 mb-2">		
			  <div class="col-md-2">
		         <div class="form-group">
		            <select class="form-control" onchange="FilterCollectionList(this.value,'')" name="CategoryGUID" id="CategoryGUID">
		               <option value="">Select Course</option>
		               <option ng-repeat="row in data.course" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>
		            </select>
		         </div>
		      </div>
		      <div class="col-md-2">
		         <div class="form-group">
		            <div id="subcategory_section">
		               <select id="BatchGUID" name="BatchGUID" class="form-control">
		                  <option value="">Select Batch</option>
		                  <option ng-repeat="row in data.batch" value="{{row.BatchGUID}}">{{row.BatchName}}</option>
		               </select>
		            </div>
		         </div>
		      </div>
		      <div class="col-md-2 float-right">
		      		<div class="form-group">
		      			 <input type="text" class="form-control" name="Keyword" id="Keyword" placeholder="Enter search word" style="" autocomplete="off">
		      		</div>
			  </div>
		     
		      <div class="col-md-2 float-right">
		      		<div class="form-group">
		      			<input id="startDate" name="startDate" placeholder="From Date" />
		      		</div>
			  </div>
			  <div class="col-md-2 float-right">
			  		<div class="form-group">
		      			<input id="endDate" name="endDate" placeholder="To Date" />
		      		</div>
			  </div>
			  <div class="col-md-2 float-right">
		  		<div class="form-group">
	      			<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records();"><?php echo SEARCH_BTN;?></button>&nbsp;
					<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records();"><?php echo CLEAR_SEARCH_BTN;?></button>
	      		</div>
			  </div>
		</div>
	</form>
	<!-- Top container/ -->
	<!-- Data table -->
	<div> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>

		<!-- data table -->
		<table class="table table-striped table-hover" ng-init="getList()">
			<!-- table heading -->
			<thead>
				<tr>
					<th>Student</th>
					<th style="width: 100px;">Batch</th>
					<th style="width: 100px;">Amount</th>
					<th style="width: 100px;">Payment Mode</th>
					<th style="width: 100px;">Payment Date</th>
					<th style="width: 200px;">Transaction ID</th>
					<th style="width: 100px;">Collection</th>
					<th style="width: 100px;">Entry By</th>
					<th style="width: 150px;" class="text-center">Action</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row" ng-repeat="(key, row) in data.dataList">
					<td>{{row.FirstName}} {{row.LastName}}</td>
					<td>{{row.BatchName}}</td>
					<td>{{row.Amount}}</td>
					<td ng-if="row.PaymentMode == 'Online'">{{row.PaymentMode}}/{{row.PaymentType}}</td>
					<td ng-if="row.PaymentMode != 'Online'">{{row.PaymentMode}}</td>
					<td>{{row.PaymentDate}}</td>
					<td>{{row.TransactionID}}</td>					
					<td>{{row.balance}}</td>
					<td>{{row.fullname}}</td>
					<td class="text-center">
						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key, row.FeeCollectionID)"  title="View"></a>
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key, row.FeeCollectionID)"  title="Edit"></a>
						<!-- <a class="glyphicon glyphicon-plus" href="" ng-click="loadFormAdd(key, row.PostGUID)"></a> -->
						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key, row.FeeCollectionID)" title="Delete"></a>
					</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="data.noRecords">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->

	<!-- add Modal -->
	<div class="modal fade" id="add_model" ng-init="getStudentsList()">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Add Fee Collection</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLAdd"></div>
			</div>
		</div>
	</div>



	<!-- edit Modal -->
	<div class="modal fade" id="edit_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Edit Fee Collection</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLEdit"></div>
			</div>
		</div>
	</div>

	<!-- edit Modal -->
	<div class="modal fade" id="view_model">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">View Fee Collection</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div ng-include="templateURLView"></div>
			</div>
		</div>
	</div>

	<!-- delete Modal -->
	<div class="modal fade" id="delete_model">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title h5">Delete Fee Collection</h3>     	
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<!-- form -->
				<div ng-include="templateURLDelete"></div>
				</form>
				<!-- /form -->
			</div>
		</div>
	</div>

<script>
	function  RemoveSearchKeyword1() {
	    document.getElementById("filterForm").reset();
	    angular.element(document.getElementById('fee-body')).scope().getList(1); 
	}
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    $('#startDate').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        maxDate: function () 
        { 
            /*if($('#endDate').val() == "") 
            	return 0;
            else 
            	*/return $('#endDate').val();
        }        
    });
    $('#endDate').datepicker({
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        minDate: function () {
            return $('#startDate').val();
        }
    });
  //   $(function () {
  //   	$(".ui-state-default").click(function() {
  //           alert('Item selected');
  //       });

  //       $("#startDate").datepicker({ 
		//     uiLibrary: 'bootstrap4',
	 //        iconsLibrary: 'fontawesome',
		//     maxDate: $('#endDate').val(),
		//     onSelect: function(date){
		//     	angular.element(document.getElementById('fee-body')).scope().getList(1);
		//     }
		// });

		// $("#endDate").datepicker({ 
		//     uiLibrary: 'bootstrap4',
	 //        iconsLibrary: 'fontawesome',
	 //        minDate:  function () {
	 //            return $('#startDate').val();
	 //        },
	 //        onSelect: function(selected,evnt) {
		//          angular.element(document.getElementById('fee-body')).scope().getList(1); 
		//     }
		// });
  //   });
</script>



</div><!-- Body/ -->