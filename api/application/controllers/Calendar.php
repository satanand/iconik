<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends API_Controller_Secure {

	function __construct()
	{
        // Call the Model constructor
		parent::__construct();
		$this->load->model('Notification_model');
		$this->load->model('Utility_model');
		$this->load->model('Calendar_model');
		$this->load->model('Common_model');
		$this->load->model('Entity_model');

	}


	function getEvents_get()
	{	
		$this->form_validation->set_rules('start', 'start','trim|required');
		$this->form_validation->set_rules('end', 'end','trim|required');

		$this->form_validation->validation($this);  /* Run validation */	
		$Results=$this->Calendar_model->getEvents($this->Post);

		$this->Return['Data'] = $Results;
		//echo json_encode($result);
	}
	/*Get all Faculty */

	function getFaculty_post()
	{
	
       $Results=$this->Calendar_model->getFaculty($this->Post['CourseID']);
	   $this->Return['Data'] = $Results;

	} 

	function getFacultyList_post()
	{
	
       $Results=$this->Calendar_model->getFacultyList();
	   $this->Return['Data'] = $Results;
	   
	} 

	function getBatch_post()
	{
	
		$Results=$this->Calendar_model->getBatch($this->Post['FacultyID']);
		$this->Return['Data'] = $Results;


	}
	function getSubject_post()
	{
	
		$Results=$this->Calendar_model->getSubject($this->Post['BatchID']);
		$this->Return['Data'] = $Results;


	}

	/*Add new event */
	function addEvent_post()
	{
		$this->form_validation->set_rules('FacultyID', 'Faculty','trim|required');
		$this->form_validation->set_rules('BatchID', 'Batch','trim|required');
		$this->form_validation->set_rules('CourseID', 'Course','trim|required');
		$this->form_validation->set_rules('SubjectID', 'Subject','trim');
		$this->form_validation->set_rules('forDates', 'Date','trim|required');
		$this->form_validation->set_rules('StartTime', 'StartTime','trim|required');
		$this->form_validation->set_rules('EndTime', 'EndTime','trim|required');
		

		$this->form_validation->validation($this);  /* Run validation */		

		$Results=$this->Calendar_model->addEvent();

		if(!empty($Results)){
			if(in_array("exist", $Results)){

				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"End Time should be ahead of start time";

			}else{
				$this->Return['Data'] = $Results;
			}
		}
		
	}


	/*Update Event */
	function updateEvent_post()
	{
		$this->form_validation->set_rules('CourseID', 'Course','trim|required');
		$this->form_validation->set_rules('FacultyID', 'Faculty','trim|required');
		$this->form_validation->set_rules('BatchID', 'Batch','trim|required');
		$this->form_validation->set_rules('SubjectID', 'Subject','trim');
		$this->form_validation->set_rules('StartTime', 'StartTime','trim|required');
		$this->form_validation->set_rules('EndTime', 'EndTime','trim|required');
		$this->form_validation->set_rules('id', 'id','trim|required');

		$this->form_validation->validation($this);  

		$Results=$this->Calendar_model->updateEvent();

		if(!empty($Results)){
			if(in_array("exist", $Results)){

				$this->Return['ResponseCode'] 	=	500;
				$this->Return['Message']      	=	"End Time should be ahead of start time";

			}else{

				$this->Return['Data'] = $Results;
			}
		}
	}

	/*Delete Event*/
	function deleteEvent()
	{
		$result=$this->Calendar_model->deleteEvent();
		echo $result;
	}

	function dragUpdateEvent_post()
	{	

		$Results=$this->Calendar_model->dragUpdateEvent();
		if(!empty($Results)){

			$this->Return['Data'] = $Results;

		}
	}


	function getMySchecdules_post()
	{
		$this->form_validation->set_rules('SessionKey', 'SessionKey', 'trim|callback_validateSession');
		$this->form_validation->set_rules('UserGUID', 'UserGUID', 'trim|callback_validateEntityGUID[User,UserID]');
		$this->form_validation->set_rules('CategoryGUID', 'CategoryGUID', 'trim|callback_validateEntityGUID[Category,CategoryID]');

		// $this->form_validation->set_rules('BatchDate', 'BatchDate', 'trim');

		$this->form_validation->validation($this);

		//print_r($this->CategoryID); die;

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$Results = $this->Common_model->getMySchecdules($this->EntityID,@$this->UserID,@$this->CategoryID, @$this->Post);


		if(!empty($Results)){
			$this->Return['Data'] = $Results;
		}else{
			$this->Return['ResponseCode'] = 500;
			$this->Return['Data'] = [];
		}		
		
	}




}
