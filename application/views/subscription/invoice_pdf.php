<style type="text/css">
.table td, .table tr, .table th
{
	border: 1px solid #ccc !important;
}
body
{
	color: #111 !important;
	font-size: 11px !important;
}
</style>

<?php
$address = array();
if(isset($session_data['Address']) && !empty($session_data['Address']))
$address[] = $session_data['Address'];

if(isset($session_data['CityName']) && !empty($session_data['CityName']))
$address[] = $session_data['CityName'];

if(isset($session_data['State']) && !empty($session_data['State']))
$address[] = $session_data['State'];


$addressStr = "";
if(isset($address) && !empty($address))
{
	$addressStr = implode(", ", $address);
	$addressStr = $addressStr."<br/>"; 
}


$FilterPaymentStartDate = $FilterPaymentEndDate = $FilterStartDate = $FilterEndDate = $Keyword = "";
if($filters['FilterPaymentStartDate'] && !empty($filters['FilterPaymentStartDate']))
{
	$FilterPaymentStartDate = date("d-M-Y", strtotime($filters['FilterPaymentStartDate']));
}

if($filters['FilterPaymentEndDate'] && !empty($filters['FilterPaymentEndDate']))
{
	$FilterPaymentEndDate = date("d-M-Y", strtotime($filters['FilterPaymentEndDate']));
}

if($filters['FilterStartDate'] && !empty($filters['FilterStartDate']))
{
	$FilterStartDate = date("d-M-Y", strtotime($filters['FilterStartDate']));
}

if($filters['FilterEndDate'] && !empty($filters['FilterEndDate']))
{
	$FilterEndDate = date("d-M-Y", strtotime($filters['FilterEndDate']));
}

if($filters['Keyword'] && !empty($filters['Keyword']))
{
	$Keyword = $filters['Keyword'];
}
?>

<table border="0" width="100%">
<tr>
	<td width="10%">
		<img src="<?php echo $session_data['InstituteProfilePic'];?>" width="100px" height="70px">
	</td>  

	<td width="90%" align="center">
		<h1><?php echo ucwords($session_data['FullName']);?></h1>
		<?php echo ucwords($addressStr);?>
		<?php echo $session_data['Email'];?>
	</td>
</tr>
</table>

<hr class="light-grey-hr" style='color:#ccc !important;' />

<table border="0" width="100%">
<tr>
<td colspan="2" align="center"><b><?php echo ucfirst($report_title);?></b></td>
</tr>
</table>

<hr class="light-grey-hr" style='color:#ccc !important;' />

<table border="0" width="100%">
<tr>
	<td align="left">
		<b>Payout From Date:</b> <?php echo $FilterStartDate;?><br/>
		<b>Payment From Date:</b> <?php echo $FilterPaymentStartDate;?>
	</td>
	<td align="right">
		<b>Payout To Date:</b> <?php echo $FilterEndDate;?><br/>
		<b>Payment To Date:</b> <?php echo $FilterPaymentEndDate;?>
	</td>
</tr>

<tr>
	<td colspan="2"><b>Keyword:</b> <?php echo $Keyword;?></td>
</tr>	
</table>  

<table class="table" width="100%" border="1" style="border-collapse: collapse;" cellpadding="5" cellspacing="5">
<thead>
<tr>
	<th style="min-width: 70px;">Payout ID</th>
	<th style="min-width: 70px;">Order ID</th>
    <th style="min-width: 100px;">Order Date</th>    
    <th style="min-width: 150px;">Seller Name</th>     
    <th style="min-width: 120px;">Amount</th>    
    <th style="min-width: 130px;">Payable Amount</th>  
    <th style="min-width: 100px;">Commission</th>  
    <th style="min-width: 100px;">Payment<br/>Date</th>   
    <th style="min-width: 100px;">Payout Date</th>
    <th style="min-width: 100px;">Description</th>
</tr>
</thead>

<tbody id="tabledivbody">
<?php
if(isset($content['Data']['Records']) && !empty($content['Data']['Records']))
{
	$total_amt = $total_pay = $total_com = 0;
	foreach($content['Data']['Records'] as $arr)
	{	
		if(isset($arr['Payouts']['NetAmountSum']) && !empty($arr['Payouts']['NetAmountSum']))
		{
			$total_amt = $total_amt + $arr['Payouts']['NetAmountSum'];
		}

		if(isset($arr['Payouts']['PayableAmount']) && !empty($arr['Payouts']['PayableAmount']))
		{
			$total_pay = $total_pay + $arr['Payouts']['PayableAmount'];
		}

		if(isset($arr['Payouts']['Commission']) && !empty($arr['Payouts']['Commission']))
		{
			$total_com = $total_com + $arr['Payouts']['Commission'];
		}

	?>

		<tr>
			<td><?php echo $arr['OrderedPayoutID'];?></td>

			<td><?php echo $arr['OrderID'];?></td>
	
			<td><?php echo $arr['OrderDate'];?></td>		

			<td><?php echo ucwords($arr['FirstName']." ".$arr['LastName']);?></td>	

			<td align="center"><?php echo $arr['Payouts']['NetAmountSum'];?></td>	

			<td align="center"><?php echo $arr['Payouts']['PayableAmount'];?></td>	

			<td align="center"><?php echo $arr['Payouts']['Commission'];?></td>	

			<td><?php echo $arr['PaymentDate'];?></td>	

			<td><?php echo $arr['PayoutDate'];?></td>

			<td><?php echo substr($arr['PayoutDesc'], 0, 200);?></td>

		</tr>	
<?php
	}	
?>
		<tr>
			<td colspan="4" align="right"><b>TOTAL</b></td>	
			<td align="center"><b><?php echo $total_amt;?></b></td>
			<td align="center"><b><?php echo $total_pay;?></b></td>
			<td align="center"><b><?php echo $total_com;?></b></td>
			<td colspan="3"></td>
		</tr>	
			

		
<?php
}
?>			
</tbody>
</table>
	