<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/*------------------------------*/
/*------------------------------*/
function sendPushMessage($UserID, $Message, $Data = array(), $package_name = "") 
{
    //Forcefully sending notification------------------------------
    $res_arr = send_notification("dVYrfxZmVyk:APA91bEbXh7EfCs_H2kz5kYdgXKlyaw2tqLw6fWyWE0N8KjXw9vE27YP3x0vEcbWyu6y6XrOm2JAyK_i-tPpHmI7Wx0BqJM4cvAQsjvmkgv13c44yy3b2hzvi5cbeYCVEJntUHBFIA5u", $Message);
    
    echo "<pre>"; print_r($res_arr);   die;

    if (!isset($Data['content_available'])) {
        $Data['content_available'] = 1;
    }
    $Obj = & get_instance();
    $Obj->db->select('U.UserTypeID, US.DeviceTypeID, US.DeviceToken');
    $Obj->db->from('tbl_users_session US');
    $Obj->db->from('tbl_users U');
    $Obj->db->where("US.UserID", $UserID);
    $Obj->db->where("US.UserID", "U.UserID", FALSE);
    $Obj->db->where("US.DeviceToken!=", '');
    $Obj->db->where('US.DeviceToken is NOT NULL', NULL, FALSE);
    if(!MULTISESSION){ 
        $this->db->limit(1);
    }
    $Query = $Obj->db->get();
    //echo $Obj->db->last_query();
    if ($Query->num_rows() > 0) 
    {
        foreach ($Query->result_array() as $Notifications) 
        {
            if ($Notifications['DeviceTypeID'] == 2) 
            { /*I phone */
                pushNotificationIphone($Notifications['DeviceToken'], $Notifications['UserTypeID'], $Message, 0, $Data, $package_name);
            } 
            elseif ($Notifications['DeviceTypeID'] == 3) 
            { /* android */
                pushNotificationAndroid($Notifications['DeviceToken'], $Notifications['UserTypeID'], $Message, $Data, $package_name);
            }
        }
    }
}
/*------------------------------*/
/*------------------------------*/
function send_notification($registered_id, $msg)
{
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array('to' => $registered_id, 'notification' => array("body" => $msg, "title" => $msg, "click_action" => "com.iconik.qikllp.CourseModule"));
    $fields = json_encode($fields);
    $headers = array('Authorization: key=' . "AIzaSyCSbm6o4EmKJFdgllrJLQfnp0TyU1lVo9U", 'Content-Type: application/json');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    $result = curl_exec($ch);
    curl_close($ch);   
    return $result;
}

function pushNotificationAndroid($DeviceIDs, $UserTypeID, $Message, $Data = array(), $package_name = "") 
{
    //API URL of FCM
    $URL = 'https://fcm.googleapis.com/fcm/send';
    /*ApiKey available in:  Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/
    if (ENVIRONMENT == 'production') 
    {
        $ApiKey = 'AIzaSyCSbm6o4EmKJFdgllrJLQfnp0TyU1lVo9U';
    } 
    else 
    {
        $ApiKey = 'AIzaSyCSbm6o4EmKJFdgllrJLQfnp0TyU1lVo9U';
    }
    //$Fields = array('registration_ids' => array($DeviceIDs), 'data' => array("Message" => $Message, "Data" => $Data));
    
    $fields = array('to' => $DeviceIDs, 'notification' => array("body" => $Message, "title" => $Message, "click_action" => $package_name)); 

    //header includes Content type and api key
    $Headers = array('Authorization: key=' . $ApiKey, 'Content-Type: application/json');    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $Headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    $Result = curl_exec($ch);
    curl_close($ch);   
    
    $obj = & get_instance();
    /*Save Log*/
    $PushData = array('Body' => json_encode(array_merge($Headers, $Fields), 1), 'DeviceTypeID' => '3', 'Return' => $Result, 'EntryDate' => date("Y-m-d H:i:s"));
    @$obj->db->insert('log_pushdata', $PushData);
    /*if ($Result === FALSE) 
    {
        die('FCM Send Error: ' . curl_error($Ch));
    }*/    
    return $Result;
}
/*------------------------------*/
/*------------------------------*/
function pushNotificationIphone($DeviceToken = '', $UserTypeID, $Message = '', $Badge = 1, $Data = array(), $package_name = "") {
    $Badge = ($Badge == 0 ? 1 : 0);
    $Pass = '123456';
    $Body['aps'] = $Data;
    $Body['aps']['alert'] = $Message;
    $Body['aps']['badge'] = (int)$Badge;
    // if ($sound)//$Body['aps']['sound'] = $sound;
    /* End of Configurable Items */
    $Ctx = @stream_context_create();
    // assume the private key passphase was removed.
    stream_context_set_option($Ctx, 'ssl', 'passphrase', $Pass);

    if (ENVIRONMENT == 'production') {
        $Certificate = 'app2-ck-live.pem';
        @stream_context_set_option($Ctx, 'ssl', 'local_cert', $Certificate);
            $Fp = @stream_socket_client('ssl://gateway.push.apple.com:2195', $Err, $Errstr, 60, STREAM_CLIENT_CONNECT, $Ctx); //For Live
        } else {
            $Certificate = 'app2-ck-dev.pem';
            @stream_context_set_option($Ctx, 'ssl', 'local_cert', $Certificate);
            $Fp = @stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $Err, $Errstr, 60, STREAM_CLIENT_CONNECT, $Ctx); //For Testing
        }

        if (!$Fp) {
            return "Failed to connect $Err $Errstr";
        } else {
            try {
                $obj = & get_instance();
                /*Save Log*/
                $PushData = array('Body' => json_encode($Body, 1), 'DeviceTypeID' => '2', 'Return' => $Certificate, 'EntryDate' => date("Y-m-d H:i:s"),);
                @$obj->db->insert('log_pushdata', $PushData);
                $Payload = @json_encode($Body, JSON_NUMERIC_CHECK);
                $Msg = @chr(0) . @pack("n", 32) . @pack('H*', @str_replace(' ', '', $DeviceToken)) . @pack("n", @strlen($Payload)) . $Payload;
                @fwrite($Fp, $Msg);
                @fclose($Fp);
            }
            catch(Exception $E) {
                return 'Caught exception';
            }
        }
}
    /*------------------------------*/
    /*------------------------------*/
    function sendSMS($input = array()) 
    {
        //Your authentication key
        $authKey = "tCMw6RIaEUyQUHgAYZEYIQ";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $input['PhoneNumber'];

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "ICONIK";

        //Your message to send, Add URL encoding here.
        $message = urlencode($input['Message']);

        //Define route 
        $route = 0;
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );



        $url = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=$authKey&senderid=$senderId&channel=OTP&DCS=0&flashsms=0&number=$mobileNumber&text=$message&route=0";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);

        //echo "<pre>"; print_r($output); die;
    }

    function sendSMS_Backup($input = array()) 
    {        

    //Your authentication key
        $authKey = "ukxEyt4Ue02x3t307TrlxA";
    //Multiple mobiles numbers separated by comma
        $mobileNumber = $input['PhoneNumber'];
    //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "ICONIKT";
    //Your message to send, Add URL encoding here.
        $message = $input['Message'];
    //Define route
        $route = "1";
    //Prepare you post parameters
        $postData = array('authkey' => $authKey, 'mobiles' => $mobileNumber, 'message' => $message, 'sender' => $senderId, 'route' => $route);

        $message = rawurlencode($message);
        //curl_setopt($ch, CURLOPT_URL, 'http://12.23.54.18/api/smsapi.aspx?username=myusername&password=mypassword&to=7897015426&from=DEMOAB&message=' . $msg);  
        //echo $url = "https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=$authKey&senderid=$senderId&channel=2&DCS=0&flashsms=0&number=$mobileNumber&text=$message&route=$route"; die;
        // if (ENVIRONMENT == 'production') {
        //     $url = urlencode("http://smsgateway.ca/SendSMS.aspx?CellNumber=$mobileNumber&AccountKey=zP70k0f8S70AacoogxnU3Q7WVnh460yx&MessageBody=$message");
        // } else {
        //     $url = "http://sms.bulksmsserviceproviders.com/api/send_http.php";
        // }
        // $url = "http://sms.bulksmsserviceproviders.com/api/send_http.php";
    // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true, CURLOPT_POST => true, CURLOPT_POSTFIELDS => $postData
    //,CURLOPT_FOLLOWLOCATION => true
        ));
    //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //get response
        $output = curl_exec($ch);
        //print_r($output);
    //Print error if any
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
    //echo $output;

    }
    /*------------------------------*/
    /*------------------------------*/
    function sendMail($Input = array()) {
        $CI = & get_instance();
        $CI->load->library('email');
        // $config['protocol'] = "smtp";
        // $config['smtp_host'] = "smtp.mailgun.org";
        // $config['smtp_port'] = "587";
        // $config['smtp_user'] = "postmaster@cp.iconik.in";
        // $config['smtp_pass'] = "6245b028b8406f5f3eb7d69ddc5fa4d5-29b7488f-5c9ce55f";
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.hostinger.in";
        $config['smtp_port'] = 587;
        $config['smtp_user'] = "test@thejbpmart.com";
        $config['smtp_pass'] = "Test@123";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['wordwrap'] = TRUE;
       // $config['smtp_crypto'] = SMTP_CRYPTO;
        $CI->email->initialize($config);
        $CI->email->set_newline("\r\n");
        $CI->email->clear();

        if(!empty($Input['From_email'])){
            $CI->email->from($Input['From_email'], $Input['From_name']);
        }else if(!empty($Input['From_name'])){
            $CI->email->from(FROM_EMAIL, $Input['From_name']);
        }else{
            $CI->email->from(FROM_EMAIL, FROM_EMAIL_NAME);
        }
        
        $CI->email->reply_to(NOREPLY_EMAIL, NOREPLY_NAME);
        $CI->email->to($Input['emailTo']);

        if(!empty($Input['CC_email'])){
             $CI->email->cc($Input['CC_email']);
        }


        if(!empty($Input['BCC_email'])){
            $CI->email->bcc($Input['BCC_email']);
        }else if (defined('TO_BCC') && !empty(TO_BCC)) {
            $CI->email->bcc(TO_BCC);
        }

        if(!empty($Input['Attachment'])){
            $CI->email->attach($Input['Attachment']);
        }

        $CI->email->subject($Input['emailSubject']);
        $CI->email->message($Input['emailMessage']);
       
        if (@$CI->email->send()) {
            return 1;
        } else {
        echo $CI->email->print_debugger();
            return 2;
        }
    }
    /*------------------------------*/
    /*------------------------------*/
    function emailTemplate($HTML) {
        $CI = & get_instance();
        return $CI->load->view("emailer/layout", array("HTML" => $HTML), TRUE);
    }
    /*------------------------------*/
    /*------------------------------*/
    function checkDirExist($DirName) {
        if (!is_dir($DirName)) mkdir($DirName, 0777, true);
    }
    /*------------------------------*/
    /*------------------------------*/
    function validateEmail($Str) {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $Str)) ? FALSE : TRUE;
    }
    /*------------------------------*/
    /*------------------------------*/
    function validateDate($Date) {
        if (strtotime($Date)) {
            return true;
        } else {
            return false;
        }
    }
    /*------------------------------*/
    /*------------------------------*/
    function paginationOffset($PageNo, $PageSize) {
        if (empty($PageNo)) {
            $PageNo = 1;
        }
        $offset = ($PageNo - 1) * $PageSize;
        return $offset;
    }
    /*------------------------------*/
    /*------------------------------*/
    function get_guid() {
        if (function_exists('com_create_guid')) {
            return strtolower(com_create_guid());
        } else {
        mt_srand((double)microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
        return strtolower($uuid);
    }
}
/*------------------------------*/
/*------------------------------*/
function dateDiff($FromDateTime, $ToDateTime) {
    $start = date_create($FromDateTime);
    $end = date_create($ToDateTime); // Current time and date
    return $diff = date_diff($start, $end);
    echo 'The difference is ';
    echo $diff->y . ' years, ';
    echo $diff->m . ' months, ';
    echo $diff->d . ' days, ';
    echo $diff->h . ' hours, ';
    echo $diff->i . ' minutes, ';
    echo $diff->s . ' seconds';
    // Output: The difference is 28 years, 5 months, 19 days, 20 hours, 34 minutes, 36 seconds
    echo 'The difference in days : ' . $diff->days;
    // Output: The difference in days : 10398
    
}
/*------------------------------*/
/*------------------------------*/
function diffInHours($startdate, $enddate) {
    $starttimestamp = strtotime($startdate);
    $endtimestamp = strtotime($enddate);
    $difference = abs($endtimestamp - $starttimestamp) / 3600;
    return $difference;
}
/*------------------------------*/
/*------------------------------*/
function array_keys_exist(array $needles, array $StrArray) {
    foreach ($needles as $needle) {
        if (in_array($needle, $StrArray))
            return true;
    } return false;
}


function get_time_ago($datetime)
{ 
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );

    foreach ($string as $k => &$v) 
    {
        if ($diff->$k) 
        {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } 
        else 
        {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


function getEmployementType()
{
    $arr = array("Fulltime" => "Fulltime",
        "Partime" => "Partime",
        "Contractual" => "Contractual"
    );

    return $arr;
}


function validate_input_url($url)
{
    if($url == "") return false;

    if(filter_var($url, FILTER_VALIDATE_URL)) 
    {
        return true;
    } 
    else 
    {
        return false;
    }
}