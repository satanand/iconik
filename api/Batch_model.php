<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Batch_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}


	/*
	Description: 	Use to add new category
	*/
	function addBatch($data){

		

		$new_arr = array();
		array_push($new_arr, $Input);

		//$this->db->trans_start();
		$EntityGUID = get_guid();

		$this->InstituteID = (!empty($this->InstituteID) ? $this->InstituteID : $this->SessionUserID);
		//echo $this->InstituteID; die();
		$this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);

		$EntityID=$this->Entity_model->addEntitys($EntityGUID, $Input = array('UserID'=>$this->UserID,"EntityTypeID"=>1,'InstituteID'=>$this->InstituteID,"StatusID"=>1));
	    	
		    
			/* Add Batch */
			$InsertData = array_filter(array(
				"EntityID"      =>	$EntityID,
				"BatchName" 	=>	$data['BatchName'],	
				"Duration" 		=>	$data['Duration'],	
				"StartDate" 	=>	$data['StartDate'],	
				"Course" 		=>	$data['Course'],	
				"Subject" 		=>	$data['Subject']	
				
			));

		//	print_r($InsertData); die();

			$this->db->insert('tbl_batch', $InsertData);

			
			return TRUE;
		
	}



	/*
	Description: 	Use to update Batch.
	*/
	function editBatch($UserTypeID, $UserTypeName){
		//echo $UserTypeID; die;
		//$EntityGUID = get_guid();
		$UpdateArray = array_filter(array(
			"UserTypeName" => $UserTypeName,
		));

		if(!empty($UpdateArray)){
			/* Update User details to users table. */
			$this->db->where('UserTypeID', $UserTypeID);
			//$this->db->limit(1);
			$data=$this->db->update('tbl_users_type', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		
		return TRUE;
	}

	


	
	/*
	Description: 	Use to get Cetegories
	*/
	function getBatchd($UserTypeID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){
		$this->db->select('*');
		
		//$this->db->where('Entity',$EntityGUID);
		$this->db->from('tbl_users_type');

		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$this->db->where("UserTypeID",$UserTypeID);
		$this->db->order_by('UserTypeID','ASC');
		

		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
				if(!$multiRecords){
					return $Record;
				}
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	/*
	Description: 	Use to get Cetegories
	*/
	function getBatch($InstituteID, $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10)
	{
		
		$this->db->select('*');
		
		//$this->db->where('Entity',$EntityGUID);
		$this->db->from('tbl_batch B');
		$this->db->join('tbl_entity E', 'E.EntityID = B.EntityID', 'left');
		$this->db->where("E.InstituteID",$InstituteID);
		
		if(isset($Where['CategoryID']) && !empty($Where['CategoryID']))
		{
			$this->db->where("B.CourseID",$Where['CategoryID']);
		}

		$this->db->order_by('StartDate','ASC');



		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		
		

		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	
	/*
	Description: 	Use to get Modules
	*/
	function getmodules($UserTypeID){
	

$Query = $this->db->query("SELECT `ModuleID`, `ModuleTitle`,'c' as C FROM `admin_modules` where `ModuleID` not in(select `ModuleID` from admin_user_type_permission where UserTypeID=$UserTypeID)  UNION SELECT m.`ModuleID`, m.`ModuleTitle`,'sc' as C FROM `admin_modules` m JOIN admin_user_type_permission a on m.ModuleID=a.ModuleID where UserTypeID=$UserTypeID");

      	$data = $Query->result_array();
		if(count($data) > 0){
			foreach($data as $Record){

			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;		
	}	

/*edit get data*/
	function getEDBatch($Field='EntityID', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=10){

		$this->db->select('*');
		$this->db->from('tbl_batch');
		$this->db->limit(1);
		if(!empty($Where['EntityID'])){
			$this->db->where("EntityID",$Where['EntityID']);
		}
		$Query = $this->db->get();	
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

			
				$Records = $Record;
			}
			//$Return['Records'] = $Records;

			return $Records;
		}
		return FALSE;		
	}


}