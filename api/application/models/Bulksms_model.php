<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bulksms_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}	

	/*
	Description: 	Use to get list of post.
	Note:			$Field should be comma seprated and as per selected tables alias. 
	*/
	function getCourseAndBatch($EntityID,$where=""){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		/* Define section  */
		$Return = array('Data' => array('Records' => array()));
		/* Define variables - ends */
		$this->db->select('C.CategoryID, C.CategoryID, C.CategoryName, B.BatchID, B.BatchGUID, B.BatchName');
		$this->db->from('set_categories C');
		$this->db->join('tbl_batch B', 'B.CourseID = C.CategoryID');
		$this->db->join('tbl_entity E', 'E.EntityID = C.CategoryID');

		if(!empty($InstituteID)){
			$this->db->where('InstituteID', $InstituteID);
		}else{
			$this->db->where('InstituteID', $EntityID);
		}

		if(!empty($where['CategoryID'])){
			$this->db->where('C.CategoryID', $where['CategoryID']);
		}

		if(!empty($where['BatchID'])){
			$this->db->where('B.BatchID', $where['BatchID']);
		}
		
		$this->db->order_by('C.CategoryID','DESC');

		/* Total records count only if want to get multiple records */
		// if($multiRecords){ 
		// 	$TempOBJ = clone $this->db;
		// 	$TempQ = $TempOBJ->get();
		// 	$Return['Data']['TotalRecords'] = $TempQ->num_rows();
		// 	$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		// }else{
		// 	$this->db->limit(1);
		// }

		$Query = $this->db->get();	
		//echo $this->db->last_query();
		if($Query->num_rows()>0){	
			$category = array();
			foreach($Query->result_array() as $Record){
				if(!in_array($Record['CategoryName'], $category)){
					array_push($category, $Record['CategoryName']);
					$Record['heading'] = 1;
				}else{
					$Record['CategoryName'] = "";
					$Record['heading'] = 0;
				}
				$Records[] = $Record;
			}
			return 	$Records;	
		}
		return FALSE;		
	}



	function sendMessage($EntityID,$Input){
		//print_r($Input); die;
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$InstituteEmail = $this->Common_model->getUserEmailByID($InstituteID);
		$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");
		$this->db->select('SMSCreditID,AvailableSMS,UsedSMS');
		$this->db->from('tbl_bulk_sms_credits');
		if(!empty($InstituteID)){
			$this->db->where("UserID",$InstituteID);
		}else{
			$this->db->where("UserID",$EntityID);
		}
		$this->db->order_by('SMSCreditID','DESC');
		$this->db->limit(1);
		$Que = $this->db->get();
		//echo $this->db->last_query(); die;
		
		if($Que->num_rows() > 0){
			$SMSCredit = $Que->result_array();
			$AvailableSMS = $SMSCredit[0]['AvailableSMS'];
			$SMSCreditID = $SMSCredit[0]['SMSCreditID'];
			$UsedSMS = $SMSCredit[0]['UsedSMS'];
		}else{ 
			$AvailableSMS = "";
		}

		if(!empty($AvailableSMS)){

			$this->db->select("U.PhoneNumber,U.UserID");
			$this->db->from("tbl_users U");
			$this->db->join("tbl_entity E", "E.EntityID = U.UserID");	
			

			//$this->db->where("E.EntityTypeID",1);
			if(!empty($Input['BatchGUID'])){
				$Input['BatchGUID'] = implode("','", $Input['BatchGUID']);
			}

			if(!empty($Input['UserGUID'])){
				$Input['UserGUID'] = implode("','", $Input['UserGUID']);
			}
			
			//$Input['BatchGUID'] = array_map(function($var){ return $var->__toString(); }, $Input['BatchGUID']);
			//$Input['BatchGUID'] = array_map('strval', $Input['BatchGUID']);
			if($Input['SendTo'] == 'All'){				
				$this->db->where("E.EntityTypeID IN (1,15)");	
			}else if($Input['SendTo'] == 'Staff'){
				$this->db->where("E.EntityTypeID",1);
				$this->db->where("U.UserGUID IN ('".$Input['UserGUID']."')");
			}else if($Input['SendTo'] == 'Students'){	
				$this->db->join("tbl_students S", "S.StudentID = U.UserID");			
				$this->db->join("tbl_batch B", "B.BatchID = S.BatchID");
				$this->db->where("B.BatchGUID IN ('".$Input['BatchGUID']."')");
				$this->db->where("E.EntityTypeID",15);
			}

			$this->db->where("U.UserTypeID NOT IN (1,10)");

			if(!empty($InstituteID)){
				$this->db->where("InstituteID",$InstituteID);
			}else{
				$this->db->where("InstituteID",$EntityID);
			}

			$this->db->where("E.StatusID",2);
			$this->db->where("U.PhoneNumber IS NOT NULL");

			$Query = $this->db->get();
			//echo $this->db->last_query(); die;
			if($Query->num_rows() > 0){
				$data = $Query->result_array();

				$TotalUser = $Query->num_rows();

				$PhoneNumber = array_column($data, 'PhoneNumber');
				$UserIDs = array_column($data, 'UserID');

				if($TotalUser > $AvailableSMS){
					$PhoneNumber = array_chunk($PhoneNumber, $AvailableSMS);
					$PhoneNumber = $PhoneNumber[0];

					$UserIDs = array_chunk($UserIDs, $AvailableSMS);
					$UserIDs = $UserIDs[0];
				}	

				$UserIDsCount = count($UserIDs);

				$PhoneNumber = implode(",", $PhoneNumber);
				$UserIDs = implode(",", $UserIDs);

				//print_r($PhoneNumber); 
				//print_r($UserIDs); die;

				
				$Input["PhoneNumber"] = $PhoneNumber;

				sendSMS($Input);

				$content = $this->load->view('emailer/bulk_email',array("Name" => $InstituteData['FirstName'], 'EmailText' => "SMS successfully sent to ".$UserIDsCount." members."),TRUE);

				$SendMail = sendMail(array(
					'emailTo' 		=> $InstituteEmail,
					'emailSubject'	=> "Bulk SMS Report",
					'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
				));


				// $url = 'https://www.smsgatewayhub.com/api/mt/SendSMS?APIKey=ukxEyt4Ue02x3t307TrlxA&senderid=SMSTST&channel=2&DCS=0&flashsms=0&number='.$PhoneNumber.'&text=test%20message&route=1';

				// $ch = curl_init(); 
				// curl_setopt($ch,CURLOPT_URL,$url);
				// curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				// $output=curl_exec($ch);
				// if(curl_errno($ch))
				// {
				//     echo 'error:' . curl_error($c);
				// }
				// curl_close($ch);
				//echo count($UserIDs);
				$AvailableSMS = $AvailableSMS - $UserIDsCount;
				$UsedSMS = $UsedSMS + $UserIDsCount;
				$updateArr = array("AvailableSMS"=>$AvailableSMS,"UsedSMS"=>$UsedSMS);

				//print_r($updateArr); die;

				$this->db->where("SMSCreditID",$SMSCreditID);
				$this->db->update("tbl_bulk_sms_credits",$updateArr);

				$Insert = array("SendTo"=>$Input['SendTo'],"Message"=>$Input['Message'],"StatusID"=>2,"OnDate"=>date("Y-m-d H:i:s"),"UserIDs"=>$UserIDs);
				if($Insert && !empty($Input["PhoneNumber"])){
					$this->db->insert("tbl_bulk_sms_logs",$Insert);
					$InsertID = $this->db->insert_id();
					//echo $this->db->last_query();
					if($InsertID){
						return $UserIDsCount;
					}else{
						return FALSE;
					}
				}
			}else{
				return 'User not available';
			}
		}else{
			return 'SMS not available';
		}
	}


	function sendEmail($EntityID,$Input)
	{
		//print_r($Input); die;
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$InstituteEmail = $this->Common_model->getUserEmailByID($InstituteID);
		$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

		$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());

        if(!empty($Record['InstituteProfilePic'])){
	    	$MediaURL = $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
	    }else{
	    	$MediaURL = "";
	    }

		if(!empty($Input['SignatureGUID'])){
			$this->db->select("FromName,FromEmail,Signature");
			$this->db->from("tbl_email_signature");
			$this->db->where("SignatureGUID", $Input['SignatureGUID']);	
			$this->db->limit(1);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				$signDetails = $query->result_array();
				$Input['From_name'] = $signDetails[0]['FromName'];
				$Input['From_email'] = "";
				$Input['Signature'] = $signDetails[0]['Signature'];
			}else{
				//$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");
				$Input['From_name'] = $InstituteData['FirstName'];
				$Input['From_email'] = "";
				$Input['Signature'] = "Thanks,<br>".$InstituteData['FirstName'];
			}
		}

		$this->db->select("U.Email,U.UserID");
		$this->db->from("tbl_users as U");
		$this->db->join("tbl_entity as E", "E.EntityID = U.UserID");
		

		//$this->db->where("E.EntityTypeID",1);

		if($Input['SendTo'] == 'All'){				
			$this->db->where("E.EntityTypeID IN (1,15)");	
		}else if($Input['SendTo'] == 'Staff'){
			if(!empty($Input['UserGUID'])){
				$Input['UserGUID'] = implode("','", $Input['UserGUID']);
			}
			$this->db->where("E.EntityTypeID",1);
			$this->db->where("U.UserGUID IN ('".$Input['UserGUID']."')");
		}else if($Input['SendTo'] == 'Students'){
			$this->db->join("tbl_students as S", "S.StudentID = U.UserID");	
			$Input['BatchGUID'] = implode("','", $Input['BatchGUID']);			
			$this->db->join("tbl_batch as B", "B.BatchID = S.BatchID");
			$this->db->where("B.BatchGUID IN ('".$Input['BatchGUID']."')");
			$this->db->where("E.EntityTypeID",15);
		}

		$this->db->where("U.UserTypeID NOT IN (1,10)");	

		if(!empty($InstituteID)){
			$this->db->where("E.InstituteID",$InstituteID);
		}else{
			$this->db->where("E.InstituteID",$EntityID);
		}

		$this->db->where("E.StatusID",2);
		$this->db->where("U.Email IS NOT NULL");

		$Query = $this->db->get();
		//echo $this->db->last_query(); die;
		//echo $Query->num_rows(); die;
		if($Query->num_rows() > 0){
			$data = $Query->result_array();

			$TotalUser = $Query->num_rows();



			$Email = array_column($data, 'Email');
			$TotalMemebers = count($Email);
			$Email = implode(",", $Email);

			$UserIDs = array_column($data, 'UserID');				
			$UserIDs = implode(",", $UserIDs);

			// print_r($Input); 
			// $this->load->view('emailer/bulk_email',array("Name" => 'There', 'EmailText' => $Input['Message'],'Signature' => $Input['Signature'], "InstituteProfilePic"=>@$MediaURL));
			// die;

			$content = $this->load->view('emailer/bulk_email',array("Name" => 'There', 'EmailText' => $Input['Message'],'Signature' => $Input['Signature'], "InstituteProfilePic"=>@$MediaURL),TRUE);
			$SendMail = sendMail(array(
				'emailTo' 		=> $Email,
				'From_email' 		=> @$Input['From_email'],	
				'From_name' 		=> @$Input['From_name'],	
				'CC_email' 		=> @$Input['CC_email'],	
				'BCC_email' 		=> @$Input['BCC_email'],	
				'emailSubject'	=> $Input['Subject'],
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));

			
			$content = $this->load->view('emailer/bulk_email',array("Name" => $InstituteData['FirstName'], 'EmailText' => "Email successfully sent to ".$TotalMemebers." members.", "InstituteProfilePic"=>"", 'Signature' => ""),TRUE);			
			$SendMail = sendMail(array(
				'emailTo' 		=> $InstituteEmail,
				'emailSubject'	=> "Bulk Email Report",
				'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
			));


			$Insert = array("Subject"=>$Input['Subject'],"CC"=>$Input['CC_email'],"BCC"=>$Input['BCC_email'],"FromEmail"=>$Input['From_email'],"SendTo"=>$Input['SendTo'],"Message"=>$Input['Message'],"StatusID"=>2,"OnDate"=>date("Y-m-d H:i:s"),"UserIDs"=>$UserIDs);
			if($Insert && !empty($Email)){
				$this->db->insert("tbl_bulk_email_logs",$Insert);
				$InsertID = $this->db->insert_id();
				//echo $this->db->last_query();
				if($InsertID){
					return $TotalMemebers;
				}else{
					return FALSE;
				}
			}

			return $TotalUser;				
			die;
		}else{
			return FALSE;
		}
	}


	function getAvailableSMSCredits($EntityID){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('*');
		$this->db->from('tbl_bulk_sms_credits');
		$this->db->where('UserID',$InstituteID);
		$this->db->order_by('SMSCreditID','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}


	function getSignature($EntityID, $Where="", $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$this->db->select('*');
		$this->db->from('tbl_email_signature');
		$this->db->where('InstituteID',$InstituteID);
		if(!empty($Where['SignatureGUID'])){
			$this->db->where('SignatureGUID',$Where['SignatureGUID']);
		}
		if(!empty($Where['Keyword'])){
			$this->db->group_start();
			$this->db->like("FromName", $Where['Keyword']);
			$this->db->or_like("FromEmail", $Where['Keyword']);
			$this->db->or_like("Signature", $Where['Keyword']);
			$this->db->group_end();		
		}
		$this->db->order_by('SignatureID','DESC');
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}
		$query = $this->db->get(); //echo $this->db->last_query();
		if($query->num_rows() > 0){
			return $query->result_array();
		}else{
			return FALSE;
		}
	}

	function addSignature($EntityID, $Input){
		$SignatureGUID = get_guid();
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('*');
		$this->db->from('tbl_email_signature');
		$this->db->where('InstituteID',$InstituteID);
		$this->db->where('FromEmail',$Input['FromEmail']);
		$this->db->where('Signature',$Input['Signature']);
		$this->db->limit(1);
		$query = $this->db->get();
		
		//echo $query->num_rows(); die;
		if($query->num_rows() > 0){
			//echo $this->db->last_query();
			return FALSE;
		}else{ 
			$array = array("SignatureGUID"=>$SignatureGUID,"UserID"=>$EntityID,"InstituteID"=>$InstituteID, "FromName"=>$Input['FromName'], "FromEmail"=>$Input['FromEmail'], "Signature"=>$Input['Signature']);
			$this->db->insert("tbl_email_signature",$array);
			if($this->db->insert_id()){
				return true;
			}else{
				return FALSE;
			}
		}
		
	}


	function editSignature($EntityID, $Input){
		$SignatureGUID = get_guid();
		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$array = array("FromName"=>$Input['FromName'], "FromEmail"=>$Input['FromEmail'], "Signature"=>$Input['Signature']);

		$this->db->where('SignatureGUID', $Input['SignatureGUID']);
		$this->db->update("tbl_email_signature",$array);

		//if($this->db->affected_rows()){
			return true;
		// }else{
		// 	return FALSE;
		// }
		
	}

	function deleteSignature($EntityID, $SignatureGUID){
		$this->db->where('SignatureGUID', $SignatureGUID);
 		$this->db->delete('tbl_email_signature');

 		//echo $this->db->last_query(); die;
 		return true;
	}


	/*
	Description: 	Use to get staff
	*/
	function getStaffList($EntityID,$UserTypeID=""){

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);

		$this->db->select('CONCAT_WS(" ",u.FirstName,u.MiddleName,u.LastName) FullName,u.UserID,u.UserGUID,ut.UserTypeName,ut.UserTypeID');
		$this->db->from('tbl_users u');
		$this->db->join('tbl_entity e','e.EntityID=u.UserID','left');
		$this->db->join('tbl_users_type ut','ut.UserTypeID = u.UserTypeID');			
		$this->db->where('e.InstituteID',$InstituteID);
		$this->db->where('u.UserTypeID!=',10);
		$this->db->where('u.UserTypeID!=',1);
		$this->db->where('u.UserTypeID!=',7);
		$this->db->where('u.FirstName!=','');	

		if(!empty($UserTypeID))	{
			$this->db->where('u.UserTypeID',$UserTypeID);
		}

		$this->db->order_by('u.UserTypeID','DESC');		

		$Query = $this->db->get();	//echo $this->db->last_query();
      	$data = $Query->result_array();

      	$usertype = array();

		if(count($data) > 0){
			foreach($data as $Record){
				if(!in_array($Record['UserTypeID'], $usertype)){
					array_push($usertype, $Record['UserTypeID']);
					$Record['heading'] = 1;
				}else{
					$Record['UserTypeName'] = "";
					$Record['heading'] = 0;
				}
			 	$Records[] = $Record;
			}
			return $Records;
		}
		return FALSE;		
	}
}