app.controller('PageController', function ($scope, $http,$timeout){

    $scope.data.pageSize = 100;

    $scope.data.ParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentParentCategoryGUID = ParentCategoryGUID;

    $scope.CurrentCategoryGUID = CurrentCategoryGUID;

    $scope.CategoryTypeID = $scope.getUrlParameter('CategoryTypeID');

    $scope.APIURL = API_URL + "upload/image";
    /*----------------*/

    $scope.getFilterData = function ()

    {
        $scope.getSecondLevelPermission();
        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&'+$('#filterPanel form').serialize();

        $http.post(API_URL+'admin/category/getFilterData', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

             $scope.filterData =  response.Data;

             $timeout(function(){

                $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

            }, 300);          

         }

     });

    }





    /*list*/

    $scope.applyFilter = function ()

    {
        
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.data.ParentCategoryGUID = ParentCategoryGUID;
        $scope.getList();

    }





    /*list append*/

    $scope.getList = function ()

    {
        

        if ($scope.data.listLoading || $scope.data.noRecords) return;

        $scope.data.listLoading = true;
        var CategoryTypeID = $scope.getUrlParameter('CategoryTypeID'); 
        $scope.CategoryGUID = $scope.getUrlParameter('CategoryGUID'); 

        if(CategoryTypeID == 1){
            $scope.type = "Course";
            $("#top-heading").text("Manage Courses");
        }
        else if(CategoryTypeID == 2){
            $scope.type = "Subject";
            $("#top-heading").text("Manage Subjects");
        }
        else if(CategoryTypeID == 3){
            $scope.type = "Topic";
            $("#top-heading").text("Manage Topics");
        }
        else{
            $scope.type = "Stream";
            $("#top-heading").text("Manage Stream");
        }

        
        //console.log(ParentCategoryGUID);

        var data = 'SessionKey='+SessionKey+'&ParentCategoryGUID='+ParentCategoryGUID+'&PageNo='+$scope.data.pageNo+'&PageSize='+$scope.data.pageSize+'&'+$('#filterForm').serialize();
     

        $http.post(API_URL+'category/getCategories', data, contentType).then(function(response) {

                var response = response.data;

                if(response.ResponseCode==200 && response.Data.Records){ /* success case */

                    $scope.data.totalRecords = response.Data.TotalRecords;
                    if($scope.data.pageNo == 1)
            $scope.data.dataList = [];
                    for (var i in response.Data.Records) {

                     $scope.data.dataList.push(response.Data.Records[i]);

                    }

                     $scope.data.pageNo++;               

                }else{

                    $scope.data.noRecords = true;
                }

            $scope.data.listLoading = false;

            setTimeout(function(){ tblsort(); }, 1000);

        });

    }





    /*load add form*/

    $scope.loadFormAdd = function (Position, CategoryGUID)

    {

        $scope.templateURLAdd = PATH_TEMPLATE+module+'/add_form.htm?'+Math.random();

        $('#add_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);

    }


    /*load view form*/

    $scope.loadFormView = function (Position, CategoryGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+module+'/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'category/getCategory', 'SessionKey='+SessionKey+'&CategoryGUID='+CategoryGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#view_model').modal({show:true});

            }

        });

    }



    /*load edit form*/

    $scope.loadFormEdit = function (Position, CategoryGUID)

    {

        $scope.data.Position = Position;

        $scope.templateURLEdit = PATH_TEMPLATE+module+'/edit_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        $http.post(API_URL+'category/getCategory', 'SessionKey='+SessionKey+'&CategoryGUID='+CategoryGUID, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */

                $scope.data.pageLoading = false;

                $scope.formData = response.Data

                $('#edits_model').modal({show:true});

                $timeout(function(){            

                   $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

               }, 200);

            }

        });

    }



    /*load delete form*/

    $scope.loadFormDelete = function (Position, CategoryGUID)

    {

        $scope.data.Position = Position;

        // $scope.templateURLDelete = PATH_TEMPLATE+module+'/delete_form.htm?'+Math.random();

        // $scope.data.pageLoading = true;

        // $http.post(API_URL+'category/getCategory', 'SessionKey='+SessionKey+'&CategoryGUID='+CategoryGUID, contentType).then(function(response) {

        //     var response = response.data;

        //     if(response.ResponseCode==200){ /* success case */

        //         $scope.data.pageLoading = false;

        //         $scope.formData = response.Data

        //         $timeout(function(){            
        //            $('.modal-header .close').click();
        //            $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

        //        }, 200);

        //     }

        // });

        $scope.deleteData(CategoryGUID);

    }



    /*add data*/

    $scope.addData = function (actType)

    {

        $scope.addDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='add_form']").serialize();

        $http.post(API_URL+'admin/category/add', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200)
            { /* success case */               

                SuccessPopup(response.Message);

                if(actType == 1)
                {
                    $("#CategoryName, #Duration").val('');
                }
                else
                {
                    $scope.applyFilter();

                    $timeout(function(){            

                       $('#add_model .close').click();

                   }, 200);
                }

            }else{

                ErrorPopup(response.Message);

            }

            $scope.addDataLoading = false;          

        });

    }





    /*edit data*/

    $scope.editData = function ()

    {

        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'admin/category/editCategory', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;

                $timeout(function(){            

                   $('#edits_model .close').click();

               }, 200);

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }
}); 









/* sortable - starts */

function tblsort() {



    var fixHelper = function(e, ui) {

        ui.children().each(function() {

            $(this).width($(this).width());

        });

        return ui;

    }



$(".table-sortable tbody").sortable({

    placeholder: 'tr_placeholder',

    helper: fixHelper,

    cursor: "move",

    tolerance: 'pointer',

    axis: 'y',

    dropOnEmpty: false,

    update: function (event, ui) {

      sendOrderToServer();

  }      

}).disableSelection();

$(".table-sortable thead").disableSelection();





function sendOrderToServer() {

    var order = 'SessionKey='+SessionKey+'&'+$("#tabledivbody").sortable("serialize");

    $.ajax({

            type: "POST", dataType: "json", url: API_URL+'admin/entity/setOrder',

            data: order,

            stop: function(response) {

                if (response.status == "success") {

                    window.location.href = window.location.href;

                } else {

                    alert('Some error occurred');

                }

            }

        });

    }
}

function ParentcategoryCheck(CatType){
    if(CatType == 'Stream'){
        $("#SelectParentCategory").hide();
        $("#category-name-label").text("Stream Name");
        $("#select-label-parent-category").text("");
        $("#course_duration").hide();
    }else if(CatType == 'Course'){
        $("#SelectParentCategory").show();
        $("#category-name-label").text("Course Name");
        $("#select-label-parent-category").text("Select Stream");
        $("#course_duration").show();
        $("#course_duration").val("");
    }else if(CatType == 'Subject'){
        $("#category-name-label").text("Subject Name");
        $("#SelectParentCategory").show();
        $("#select-label-parent-category").text("Select Course");
        $("#course_duration").hide();
    }else{
       $("#category-name-label").text("Name");
       $("#SelectParentCategory").hide();
       $("#select-label-parent-category").text("Select Parent"); 
       $("#course_duration").hide();
    }
}
/* sortable - ends */


function clear_search()
{
    $("#Keyword").val('');
    
    angular.element(document.getElementById('panel-body')).scope().applyFilter();  
}