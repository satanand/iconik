<header class="panel-heading">
	<h1 class="h4">Manage Subscription</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="Subscription-body"><!-- Body -->


	<!-- Data table -->
	<div class="table-responsive block_pad_md" style="margin-top: 45px;" infinite-scroll="getList()" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 

		<!-- loading -->
		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
		
		<!-- data table -->
		<table class="table table-striped table-hover">
			<!-- table heading -->
			<thead>
				<tr>
					<th style="width: 100px; text-align: center;">Email</th>
					<th style="width: 100px; text-align: center;">Entry Date</th>
				</tr>
			</thead>
			<!-- table body -->
			<tbody>
				<tr scope="row"  ng-repeat="(key, row) in data.dataList">
					<td style="text-align: center;">{{row.Email}}</td>
					<td style="text-align: center;">{{row.EntryDate}}</td>
				</tr>
			</tbody>
		</table>

		<!-- no record -->
		<p class="no-records text-center" ng-if="data.noRecords">
			<span ng-if="data.dataList.length">No more records found.</span>
			<span ng-if="!data.dataList.length">No records found.</span>
		</p>
	</div>
	<!-- Data table/ -->
</div><!-- Body/ -->