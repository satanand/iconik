<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Assign Quiz</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getQuizAssignStatistics()"><!-- Body -->

	<div class="clearfix mt-4 mb-2">
		
		<div class="row" style="margin-top: 10px;">		 
			<div class="col-md-10">
		      	<div class="form-group">
			      <span class="float-left records hidden-sm-down">
					<span class="h5">Total records: {{data.dataList.length}}</span>
				  </span>
				 </div>
			</div>
			 <div class="col-md-2" style="    margin-top: 0px; float: right;">		

				<button ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadAssignPaper1('Quiz');">Assign Quiz</button>

		   </div>

		</div>
		<form id="filterForm" role="form" autocomplete="off" ng-submit="applyFilterQuiz()">
		<div class="row" style="margin-top: 10px;">			
			<div class="col-md-3">

				<div class="form-group">

	            <select class="form-control" ng-model="BatchGUID" ng-change="applyFilterQuiz()"  name="BatchGUID" id="Batch">

	               <option value="">Select Batch</option>

	               <option ng-repeat="row in batch" value="{{row.BatchGUID}}" ng-selected="row.BatchGUID == BatchGUID">{{row.BatchName}}</option>

	            </select>

	         </div>

			</div>
			<div class="col-md-3">

					<div class="form-group">
					 	 <div id="universal_search">
	                        <input type="text" class="form-control" name="Keyword" placeholder="Type and search" style="" autocomplete="off" onkeyup="SearchTextClear(this.value)">
	                        <span class="glyphicon glyphicon-search" ng-click="applyFilterQuiz()" data-toggle="tooltip" title="Search"></span>
	                        <a style="cursor: pointer;  display: none;" class="glyphicon glyphicon-repeat" onclick="RemoveSearchKeyword()" id="SearchTextClear" data-toggle="tooltip" title="Refresh"></a>
	                    </div>
					</div>

			  </div>	
			
		</div>
		</form>

	  
	</div>



	<!-- Data table -->

	<div class="table-responsive block_pad_md"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr>

					<th>Title</th>

					<th style="width: 150px;">Run Date</th>

					<th style="width: 150px; text-align: center;">Activation From/<br/>To Time</th>

					<th style="width: 250px;">Batch</th>

					<th style="width: 150px; text-align: center;">Result/Action</th>

					<th style="width: 100px; text-align: center;"></th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row" ng-repeat="(key, row) in data.dataList">

					<!-- <td>{{$index+1}}</td> -->

					<td><b>{{row.QtPaperTitle}}</b></td>

					<td>{{row.Date | date}}</td>

					<td style="text-align: center;">{{row.ActivatedFrom}}/<br/>{{row.ActivatedTo}}</td>

					<td><b>{{row.BatchName}}</b></td>

					<td style="text-align: center;">
						<a class="glyphicon glyphicon-eye-open" href="<?php echo base_url().'questionassign/results/?QtPaperGUID={{row.QtPaperGUID}}'; ?>" data-toggle="tooltip" title="View Results"></a>

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-if="row.EditStatus == 'No'" class="glyphicon glyphicon-pencil" href="" ng-click="loadFormEdit(key,row.QtAssignGUID, row.QtPaperGUID, 'Quiz', row.QtAssignBatch)" data-toggle="tooltip" title="Edit"></a>						

						<!-- <a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" ng-if="row.EditStatus == 'No'" class="glyphicon glyphicon-trash" href="" ng-click="loadFormDelete(key,row.QtAssignGUID)" data-toggle="tooltip" title="Delete"></a>  -->

						<a class="glyphicon glyphicon-export" href="<?php echo base_url().'questionassign/result_export_pdf?QtPaperGUID={{row.QtPaperGUID}}'; ?>"  data-toggle="tooltip" title="Export Paper Result In PDF"></a>
					</td>

					<td class="text-center" style="text-align: center;">

						<a style="text-decoration: underline;"  ng-click="loadAssignQuiz(key,row.QtPaperGUID,row.CourseGUID,row.SubjectGUID)">Assign again</a>

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Result</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>



	<!-- add Modal -->

	<div class="modal fade" id="assign_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Assign Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAssign"></div>

			</div>

		</div>

	</div>

	<!-- edit Modal -->

	<div class="modal fade" id="edit_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Assign Question Paper</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>

</div><!-- Body/ -->