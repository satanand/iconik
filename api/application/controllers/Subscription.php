<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends API_Controller_Secure
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Subscription_model');
	}


	

	/*
	Name: 			getPage
	Description: 	Use to get page data.
	*/
	public function getSubscribedUsers_post()
	{
		/* Validation section */
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('SubscribeID', 'SubscribeID', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		
		/* Validation - ends */

		$PageData = $this->Subscription_model->getSubscribedUsers("",$this->Post,TRUE, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($PageData){
			$this->Return['Data'] = $PageData['Data'];
		}	
	}


	public function getBrochures_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterStartDate', 'Start Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Created End Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */		

		$Data = $this->Subscription_model->getBrochures("", $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}



	/*------------------------------------------------------------------------
	Manage Orders
	------------------------------------------------------------------------*/
	public function getOrders_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterStartDate', 'Start Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Created End Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->load->model('Products_model');
		$Data = $this->Products_model->getOrdersAdmin($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}

	public function getOrdersDetails_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->set_rules('InstituteID', 'Institute ID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->load->model('Products_model');
		$Data = $this->Products_model->getOrdersDetailsAdmin($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}


	public function getInvoiceDetails_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');		
		$this->form_validation->set_rules('OrderID', 'Order ID', 'trim|required');
		$this->form_validation->set_rules('InstituteID', 'Institute ID', 'trim|required');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->load->model('Products_model');
		$Data = $this->Products_model->getInvoiceDetailsAdmin($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data; 
		}
	}



	public function getPayouts_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterStartDate', 'Start Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Created End Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');
		$this->form_validation->set_rules('PageNo', 'PageNo', 'trim|integer');
		$this->form_validation->set_rules('PageSize', 'PageSize', 'trim|integer');
		$this->form_validation->validation($this);  /* Run validation */		

		/* Validation - ends */

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->load->model('Products_model');
		$Data = $this->Products_model->getPayoutsAdmin($this->EntityID, $this->Post, @$this->Post['PageNo'], @$this->Post['PageSize']);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}


	public function addPayout_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');	
		$this->form_validation->set_rules('OrderID', 'OrderID','trim|required');
		$this->form_validation->set_rules('InstituteID', 'InstituteID','trim|required');
		$this->form_validation->set_rules('PayoutDate', 'Payout Date','trim|required');
		$this->form_validation->set_rules('Remarks', 'Remarks','trim|required');
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
		$this->load->model('Products_model');
		$Data = $this->Products_model->addPayout($this->EntityID, $this->Post);

		if($Data === "NotExist")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"Order details not found in the system.";
		}
		elseif($Data === "AlreadyPaid")
		{
			$this->Return['ResponseCode'] 	=	500;
			$this->Return['Message']      	=	"You already add payment for selected institute order.";
		}
		else 
		{
            $this->Return['ResponseCode'] 	=	200;
			$this->Return['Message']      	=	"Saved successfully.";
		}
	}


	public function getPayoutInvoiceDetails_post()
	{
		$this->form_validation->set_rules('EntityGUID', 'EntityGUID', 'trim|callback_validateEntityGUID');
		$this->form_validation->set_rules('FilterPaymentStartDate', 'Payment Start Date', 'trim');
		$this->form_validation->set_rules('FilterPaymentEndDate', 'Payment End Date', 'trim');
		$this->form_validation->set_rules('FilterStartDate', 'Payout From Date', 'trim');
		$this->form_validation->set_rules('FilterEndDate', 'Payout To Date', 'trim');
		$this->form_validation->set_rules('Keyword', 'Keyword', 'trim');		
		$this->form_validation->validation($this);

		$this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$this->load->model('Products_model');
		$Data = $this->Products_model->getPayoutInvoiceDetailsAdmin($this->EntityID, $this->Post);

		if($Data)
		{
			$this->Return['Data']      	=	$Data['Data']; 
		}
	}
}