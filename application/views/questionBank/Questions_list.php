<style type="text/css">

	.content {

	 /* width: 240px;*/

	  overflow: hidden;

	  word-wrap: break-word;

	  text-overflow: ellipsis;

	  line-height: 18px;

	  text-align: center;

	}

	.less {

	  max-height: 54px;

	}

	.menu-text {

	    /*display: block !important;*/

	}

	.modal { overflow: auto !important; }

	.glyphicon {

	    position: relative;

	    top: -4px;

	    display: inline-block;

	    font-family: 'Glyphicons Halflings';

	    font-style: normal;

	    font-weight: normal;

	    line-height: 1;

	    -webkit-font-smoothing: antialiased;

	    font-size: 20px;

	    padding: 4px;

	}

</style>

<style>

	.switch {

	  position: relative;

	  display: inline-block;

	  width: 60px;

	  height: 34px;

	}



	.switch input { 

	  opacity: 0;

	  width: 0;

	  height: 0;

	}



	.slider {

	  position: absolute;

	  cursor: pointer;

	  top: 0;

	  left: 0;

	  right: 0;

	  bottom: 0;

	  background-color: #ccc;

	  -webkit-transition: .4s;

	  transition: .4s;

	}



	.slider:before {

	  position: absolute;

	  content: "";

	  height: 26px;

	  width: 26px;

	  left: 4px;

	  bottom: 4px;

	  background-color: white;

	  -webkit-transition: .4s;

	  transition: .4s;

	}



	input:checked + .slider {

	  background-color: #2196F3;

	}



	input:focus + .slider {

	  box-shadow: 0 0 1px #2196F3;

	}



	input:checked + .slider:before {

	  -webkit-transform: translateX(26px);

	  -ms-transform: translateX(26px);

	  transform: translateX(26px);

	}



	/* Rounded sliders */

	.slider.round {

	  border-radius: 34px;

	}



	.slider.round:before {

	  border-radius: 50%;

	}

</style>
<header class="panel-heading">
	<h1 class="h4">Manage Question Repository</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" id="question-body" ng-init="getCategoryList()"><!-- Body -->

	
		<div class="row" style="margin-top: 20px;">		 
			<div class="col-md-10">
		      	<div class="form-group">
			      <span class="float-left records hidden-sm-down">
					<span class="h5">Total records: {{data.dataList.length}}</span>
				  </span>
				 </div>
			</div>
			<div class="col-md-2" style="    margin-top: 0px; float: right;">

				<label for="inputName" class="control-label mb-10"></label>

				<button class="btn btn-success btn-sm ml-1 float-right" id="add-question-btn" ng-click="loadFormAdd(1,'getQuestionList');" ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0">Add Question</button>

			</div>	
		</div>
		
	<form id="filterForm" role="form" autocomplete="off">
		<div class="row" style="margin-top: 20px;">		 

			<div class="col-md-3">				

				<select class="form-control" onchange="getSubCategoryList(this.value, 's');" name="CourseGUID" id="CourseGUID">

	               <option value="">Select Course</option>

	               <option ng-repeat="row in categoryDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

	            </select>

			</div>

			<div class="col-md-3">

				<div id="subcategory_section" class="subcategory_section">

	               <select id="SubjectGUID" name="SubjectGUID"  class="form-control" onchange="getSubCategoryList(this.value, 't')">

				      <option value="">Select Subject</option>

				      <option ng-repeat="row in subCategoryDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

				   </select>

	            </div>

			</div>

			<div class="col-md-3">

				<div class="subcategory_section">

	               <select id="TopicGUID" name="TopicGUID"  class="form-control">

				      <option value="">Select Topic</option>

				      <option ng-repeat="row in subTopicDataList" value="{{row.CategoryGUID}}">{{row.CategoryName}}</option>

				   </select>

	            </div>

			</div>



			<div class="col-md-3">

				<div class="multiselect">

					
					<select id="QuestionsType" name="QuestionsType" class="form-control">

						<option value="">All Question Type</option>

						<option value="Multiple Choice">Multiple Choice</option>									

						<option value="Logical Answer">Logical Answer (True/False)</option>						

					</select>

				   

				</div>

			</div>
		</div>
		

		<div class="row" style="margin-top: 20px;">		
			
			<div class="col-md-3">

				<div class="multiselect">

				    <select class="form-control" id="QuestionsLevel" name="QuestionsLevel">

							<option value="">All Question Level</option>

							<option value="Easy">Easy</option>

							<option value="Moderate">Moderate</option>

							<option value="High">High</option>

					</select>

				</div>

			</div>


			<div class="col-md-3">

				<div class="multiselect">

					<!-- <label for="inputName" class="control-label mb-10">Question Group</label> -->

				    <select class="form-control" id="QuestionsGroup" name="QuestionsGroup">

							<option value="">All Question Group</option>

							<option value="1">Practice Test</option>

							<option value="2">Quiz</option>

							<option value="3">Assessment</option>

							<option value="4">Contest</option>

							<option value="5">Scholarship Test</option>

					</select>

				</div>

			</div>


			<div class="col-md-3">

				<div class="">

				    <input type="text" class="form-control" id="Keyword" name="Keyword" placeholder="Search by word">					

				</div>

			</div>


			<div class="col-md-3">
				<div class="form-group">
					<button class="btn btn-primary btn-sm ml-1" name="search_btn" onclick="search_records('vql');"><?php echo SEARCH_BTN;?></button>&nbsp;
				<button class="btn btn-danger btn-sm ml-1" name="clear_search_btn" onclick="clear_search_records('vql');"><?php echo CLEAR_SEARCH_BTN;?></button>
				</div>
			</div>


			

		</div>												

	</form>



	
	<!-- <input type="hidden" name="QuestionsLevel" id="QuestionsLevel">

	<input type="hidden" name="QuestionsGroup" id="QuestionsGroup">

	<input type="hidden" name="QuestionsType" id="QuestionsType"> -->





	<!-- Data table -->

	<div class="table-responsive block_pad_md" style="margin-top: 30px;" infinite-scroll="getQuestionsList(0)" infinite-scroll-disabled='data.listLoading' infinite-scroll-distance="0"> 



		<!-- loading -->

		<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>



		<!-- data table -->

		<table class="table table-striped table-hover">

			<!-- table heading -->

			<thead>

				<tr>

					<th >Question</th>

					<th style="width: 150px;">Question Type</th>

					<th style="width: 150px;">Difficulty Level</th>

					<th style="width: 150px;">Question Group</th>

					<th style="width: 150px;">Marks</th>

					<!-- <th style="width: 100px; text-align: center;">Status</th> -->

					<th style="width: 100px; text-align: center;">Action</th>

				</tr>

			</thead>

			<!-- table body -->

			<tbody>

				<tr scope="row"  ng-repeat="(key, row) in data.dataList">

					<!-- <td>{{key+1}}</td> -->

					<td>
						<p ng-bind-html="row.QuestionContent"></p>												
					</td>

					<td>{{row.QuestionsType}}</td>

					<td >{{row.QuestionsLevel}}</td>

					<td>{{row.QuestionsGroup}}</td>

					<td style="text-align: center;">{{row.QuestionsMarks}}</td>

					<!-- <td style="text-align: center;"><label class="switch">

					  <input type="checkbox" id="switch-status{{key}}" name="switch-status{{key}}" ng-checked="row.StatusID==2" ng-click="changeStatus(key,row.QtBankGUID)">

					  <span class="slider round"></span>

					</label></td> -->

					<td class="text-center" style="text-align: center;">

						<a class="glyphicon glyphicon-eye-open" href="" ng-click="loadFormView(key,row.QtBankGUID,row.CourseID,row.SubjectID)" title="View"></a>

						<a ng-if="(UserTypeID == 10 || UserTypeID == 1) || data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0" class="glyphicon glyphicon-edit" href="" ng-click="loadFormEdit(key,row.QtBankGUID)" title="Edit"></a>
  
						<!-- <a class="glyphicon glyphicon-plus" href="" ng-click="loadFormAdd(key, row.PostGUID)"></a> -->

						<!-- <a class="glyphicon glyphicon-trash" href="" ng-click="deleteQuestion(key,row.QtBankGUID)"></a> -->

					</td>

				</tr>

			</tbody>

		</table>



		<!-- no record -->

		<p class="no-records text-center" ng-if="!data.dataList.length">

			<span ng-if="data.dataList.length">No more records found.</span>

			<span ng-if="!data.dataList.length">No records found.</span>

		</p>

	</div>

	<!-- Data table/ -->









	<!-- add Modal -->

	<div class="modal fade" id="add_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Question</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLAdd"></div>

			</div>

		</div>

	</div>





	<div class="modal fade" id="add_subject">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Add Subjcet</h3>     	

					<button type="button" class="close close-subject" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="subjectTemplateURLAdd"></div>

			</div>

		</div>

	</div>











	<!-- edit Modal -->

	<div class="modal fade" id="edits_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Edit Question</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLEdit"></div>

			</div>

		</div>

	</div>





	<!-- edit Modal -->

	<div class="modal fade" id="view_model">

		<div class="modal-dialog modal-lg" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">View Question Details</h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<div ng-include="templateURLView"></div>

			</div>

		</div>

	</div>





	<!-- delete Modal -->

	<div class="modal fade" id="delete_model">

		<div class="modal-dialog modal-md" role="document">

			<div class="modal-content">

				<div class="modal-header">

					<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>     	

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				</div>

				<!-- form -->

				<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">

				</form>

				<!-- /form -->

			</div>

		</div>

	</div>

</div><!-- Body/ -->



<script type="text/javascript">

	function test(status){

		alert(status);

	}

</script>