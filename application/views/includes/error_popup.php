<!-- New Success Alert Modal -->
<div class="modal" id="SuccessModal">
  <div class="modal-dialog">
     <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content container-fluid border border-success">
      <div class="alert row my-1" role="alert" style=" margin-bottom: -1rem;">
        <p class="text-center py-3 px-1 alert alert-success" style="position: absolute;border-radius: 50px;height:60px;width:60px;top: 3px;left: -30px;border:1px solid #28A745;">
          <i class="fa fa-check-circle text-success" aria-hidden="true" style="font-size: 24px;"></i>
     </p>
        <div class=" col-md-12 mt-1" style="padding: 0px">
            <p class="pl-4 mt-2" id="SuccessModalMessage" style="font-weight: bold;"></p>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- New Error Alert Modal -->
<div class="modal" id="ErrorModal">
  <div class="modal-dialog">
     <button type="button" class="close" data-dismiss="modal">&times;</button>  
    <div class="modal-content container-fluid border border-danger">
      <div class="alert row my-1" role="alert" style=" margin-bottom: -1rem;">
        <p class="text-center py-3 px-1 alert alert-danger" style="position: absolute;border-radius: 50px;height:60px;width:60px;top: 1px;left: -30px;border:1px solid #DC3545;">
          <i class="fa fa-warning text-danger" aria-hidden="true" style="font-size: 24px;"></i>
     </p>
        <div class=" col-md-12" style="padding: 0px">
            <p class="pl-4 mt-2" id="ErrorModalMessage" style="font-weight: bold;"></p>
        </div>
      </div>
    </div>
  </div>
</div>





<!-- <div class="modal" id="SuccessModal">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header text-white bg-white" style="height: 120px;">
      <h2 class="modal-title"><i class="glyphicon glyphicon-ok-circle text-success" style="position: absolute; left: 45%; top: 10%; font-size: 65px;"></i></h2>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    
    <div class="modal-body" style="border-top: 1px solid #ccc; background-color: #f1f1f1;">
      <h6 id="SuccessModalMessage" class="text-center text-success align-bottom"></h6>
    </div>
</div>
</div>
</div> -->


<!-- Error Alert Modal -->
<!-- <div class="modal" id="ErrorModal">
<div class="modal-dialog">
<div class="modal-content"> 
    <div class="modal-header text-white bg-white" style="height: 120px;">
      <h2 class="modal-title"><i class="glyphicon glyphicon-exclamation-sign text-danger" style="position: absolute; left: 45%; top: 10%; font-size: 65px;"></i></h2>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>    
    
    <div class="modal-body" style="border-top: 1px solid #ccc; background-color: #f1f1f1;">
      <h6 id="ErrorModalMessage" class="text-center text-danger"></h6>
    </div>
</div>
</div>
</div> -->

<script type="text/javascript">
function ErrorPopup(Message)
{
    $("#ErrorModal").modal();
        
    $("#ErrorModal").find("#ErrorModalMessage").html(Message);

    setTimeout(function()
    {            
       $('#ErrorModal .close').click();

    }, 3000);
}


function SuccessPopup(Message)
{
    $("#SuccessModal").modal();
    
    $("#SuccessModal").find("#SuccessModalMessage").html(Message);

    setTimeout(function()
    {            
       $('#SuccessModal .close').click();

    }, 3000);
}  

$(document).ready(function()
{  
  $('#add_model, #add_subject, #filter_model, #edit_model, #delete_model, #addasaining_model, #edits_model, #view_model, #editAssign_model, #viewAssign_model, #permission_model, #attendance_model, #email_model, #active_model, #View_model, #inactive_model, #permission_model, #add_subject, #add_payment, #feecollection_model, #assign_model, #bulk_assign_model, #addcategory_model, #view_activity_model, #edit_activity_model, #manage_activity_model, #manage_participants_model, #issunebook_model, #returnbook_model, #view_gallery_model, #edit_album_model, #extension_popup, #library_popup, #batchfaculty, #bulk_assign_student').modal({
    backdrop: 'static',
    keyboard: false,
    show: false
  });
  
});
</script>