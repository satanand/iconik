<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();	
	}

   

	/*
	Description: 	Use to update user profile info.
	*/
	function updateUserInfo($UserID, $Input=array()){
        if(!empty($Input['BirthDate'])){
			$originalDate = $Input['BirthDate'];
			$newDate = date("Y-m-d", strtotime($originalDate));
		}else{
			$newDate = '';
		}

		if(!empty($Input['AnniversaryDate'])){
			$AnniversaryDate = date("Y-m-d", strtotime($Input['AnniversaryDate']));
		}else{
			$AnniversaryDate = '';
		}

		$UpdateEntity = array_filter(array(
			"StatusID" 	 =>	@$Input['StatusID']
		));
		if(!empty($UpdateEntity)){
			$this->db->where('EntityID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_entity', $UpdateEntity);
	  	}

		$UpdateArray = array_filter(array(						
			"UserTypeID" 			=>	$Input['UserTypeID'],
			"StoreID" 				=>	@$Input['StoreID'],
			"FirstName" 			=> 	@$Input['FirstName'],			
			"LastName" 				=> 	@$Input['LastName'],	
			"Email" 				=>	@strtolower($Input['Email']),
			"PhoneNumber" 			=>	@$Input['PhoneNumber'],
			"AdminAccess" 			=>	@$Input['Access'],
			"AppAccess" 			=>	@$Input['AppAccess']
		));

		
		
		/* Update User details to users table. */
		if(!empty($UpdateArray)){
			$this->db->where('UserID', $UserID);
			$this->db->update('tbl_users', $UpdateArray);
			//echo $this->db->last_query(); die;
		}

		if($Input['UserTypeID'] == "11"){
			if(!empty($Input['CourseID'])){
				$CourseID = implode(",",$Input['CourseID']);
			}else{
				$CourseID = NULL;
			}

			if(!empty($Input['SubjectID'])){
				$SubjectID = implode(",",$Input['SubjectID']);
			}else{
				$SubjectID = NULL;
			}

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$UserID,
				"CourseID" 		        =>	@$CourseID,
				"SubjectID" 		    =>	@$SubjectID
			));

			$this->db->select("UserID");
			$this->db->from("tbl_user_jobs");
			$this->db->where("UserID",$UserID);
			$this->db->limit(1);
			$check = $this->db->get();
			if($check->num_rows() > 0){
				$this->db->where("UserID",$UserID);
				$this->db->update('tbl_user_jobs', $InsertDatad);
			}else if(!empty($InsertDatad)){
				$this->db->set($InsertDatad);
    			$this->db->insert($this->db->dbprefix . 'tbl_user_jobs');
				//$this->db->insert('tbl_user_jobs', $InsertDatad);
			}
		}

		//if(!empty($Input['type']) && $Input['type']=='PersonalDetails'){
			$InsertDataU = array_filter(array(
				"Gender" 				=>	@$Input['Gender'],
				"BirthDate" 			=>	@$newDate,
				"MaritalStatus" 		=>	@$Input['MaritalStatus'],
				"LivingStatus" 			=>	@$Input['LivingStatus'],
				"AnniversaryDate" 		=>	@$AnniversaryDate,
				"Address" 				=>	@$Input['Address'],
				"CityName" 				=>	@$Input['CityName'],
				"StateID" 				=>	@$Input['StateID'],
				"CityCode" 				=>	@$Input['CityCode']
			));
			$this->db->where("UserID",$UserID);
			$this->db->update('tbl_users', $InsertDataU);
		//}
		//else if(!empty($Input['type']) && $Input['type']=='EmergencyDetails'){
			$InsertDataNom = array_filter(array(
				"UserID" 				=> 	$UserID,
				"Emergency" 			=>	@$Input['Emergency'],
				"RelationWithEmergency" =>	@$Input['RelationWithEmergency'],
				"EmergencyPhoneNumber" 	=>	@$Input['EmergencyPhoneNumber'],
				"EmergencyGender" 		=>	@$Input['EmergencyGender'],
				"EmergencyAddress" 		=>	@$Input['EmergencyAddress'],
				"EmergencyState" 		=>	@$Input['EmergencyState'],
				"EmergencyCityName" 	=>	@$Input['EmergencyCityName'],
				"EmergencyCityCode" 	=>	@$Input['EmergencyCityCode'],
			));

			$this->db->select("UserID");
			$this->db->from("tbl_user_contact_info");
			$this->db->where("UserID",$UserID);
			$this->db->limit(1);
			$check = $this->db->get();
			if($check->num_rows() > 0){
				$this->db->where("UserID",$UserID);
				$this->db->update('tbl_user_contact_info', $InsertDataNom);
			}else if(!empty($InsertDataNom)){
				$this->db->set($InsertDataNom);
    			$this->db->insert($this->db->dbprefix . 'tbl_user_contact_info');
				//$this->db->insert('tbl_user_contact_info', $InsertDataNom);
			}
			
		//}
		//else if(!empty($Input['type']) && $Input['type']=='NomineeDetails'){
			$InsertDataNom = array_filter(array(
				"UserID" 				=> 	$UserID,			
				"Nominee" 			    =>	@$Input['Nominee'],
				"RelationWithNominee" 	=>	@$Input['RelationWithNominee'],
				"NomineePhoneNumber" 	=>	@$Input['NomineePhoneNumber'],
				"NomineeGender" 		=>	@$Input['NomineeGender'],
				"NomineeAddress" 		=>	@$Input['NomineeAddress'],
				"NomineeState" 		    =>	@$Input['NomineeState'],
				"NomineeCityName" 		=>	@$Input['NomineeCityName'],
				"NomineeCityCode" 		=>	@$Input['NomineeCityCode']
			));
			$this->db->select("UserID");
			$this->db->from("tbl_user_contact_info");
			$this->db->where("UserID",$UserID);
			$this->db->limit(1);
			$check = $this->db->get();
			if($check->num_rows() > 0){
				$this->db->where("UserID",$UserID);
				$this->db->update('tbl_user_contact_info', $InsertDataNom);
			}else if(!empty($InsertDataNom)){
				$this->db->set($InsertDataNom);
    			$this->db->insert($this->db->dbprefix . 'tbl_user_contact_info');
				//$this->db->insert('tbl_user_contact_info', $InsertDataNom);
			}
		//}
		//else if(!empty($Input['type']) && $Input['type']=='Qualification'){
			$this->db->where('UserID', $UserID);
			$this->db->delete('tbl_user_eduction');
			$count = count($Input['Qualification']);
			$emptys=0;
			for($key=0; $key<$count; $key++) {
			   if(!empty($Input['Qualification'][$key])){
				   	$InsertDataQu = array_filter(array(
						"UserID" 				=> 	$UserID,
						"Qualification" 		=>	@$Input['Qualification'][$key],
						"Univercity" 			=>	@$Input['Univercity'][$key],
						"Specialization" 		=>	@$Input['Specialization'][$key],
						"PassingYear" 			 =>	@$Input['PassingYear'][$key],
						"Percentage" 			=>	@$Input['Percentage'][$key]
					 ));
				  $this->db->set($InsertDataQu);
    			  $this->db->insert($this->db->dbprefix . 'tbl_user_eduction');
				  //$this->db->insert('tbl_user_eduction', $InsertDataQu);
			   	}
			}
		//}
		//else if(!empty($Input['type']) && $Input['type']=='JobsDetails'){
			if(!empty($Input['JoiningDate'])){
				$JoiningDate = date("Y-m-d", strtotime($Input['JoiningDate']));
			}else{
				$JoiningDate = NULL;
			}

			if(!empty($Input['AppraisalDate'])){
				$AppraisalDate = date("Y-m-d", strtotime($Input['AppraisalDate']));
			}else{
				$AppraisalDate = NULL;
			}

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$UserID,
				"Job" 		            =>	@$Input['Job'],
				"Department" 			=>	@$Input['Department'],
				"Designation" 			=>	@$Input['JobDesignation'],
				"ReportingManager" 		 =>	@$Input['ReportingManager'],
				"JoiningDate" 			=>	@$JoiningDate,
				"AppraisalDate" 		=>	@$AppraisalDate
			));

			$this->db->select("UserID");
			$this->db->from("tbl_user_jobs");
			$this->db->where("UserID",$UserID);
			$this->db->limit(1);
			$check = $this->db->get();
			if($check->num_rows() > 0){
				$this->db->where("UserID",$UserID);
				$this->db->update('tbl_user_jobs', $InsertDatad);
			}else if(!empty($InsertDatad)){
				$this->db->set($InsertDatad);
    			$this->db->insert($this->db->dbprefix . 'tbl_user_jobs');
				//$this->db->insert('tbl_user_jobs', $InsertDatad);
			}
		//}
		//else if(!empty($Input['type']) && $Input['type']=='WorkExperience'){
			$this->db->where('UserID', $UserID);
			$this->db->delete('tbl_user_experience');

			$counts = count($Input['Employer']);
			$empty=0;
			for($keys=0; $keys<$counts; $keys++) {

				if(empty($Input['Employer'][$keys])){
					$empty=$empty+1; 
					if($counts==$empty){
						if(!empty($Input['From'])){
							$From = date("Y-m-d", strtotime($Input['From'][$keys]));
						}else{
							$From = NULL;
						}

						if(!empty($Input['To'])){
							$To = date("Y-m-d", strtotime($Input['To'][$keys]));
						}else{
							$To = NULL;
						}
						$InsertDatae = array_filter(array(
							"UserID" 				=> 	$UserID,
							"Employer" 			    =>	@$Input['Employer'][$keys],
							"Industries" 	=>	@$Input['Industries'][$keys],
							"ExpDesignation" 	=>	@$Input['ExpDesignation'][$keys],
							"JobProfile" 	 =>	@$Input['JobProfile'][$keys],
							"From" 	=>	$From,
							"To" =>	$To
						));
					}
				}

				
				if(!empty($Input['Employer'][$keys])){
					if(!empty($Input['From'])){
						$From = date("Y-m-d", strtotime($Input['From'][$keys]));
					}else{
						$From = NULL;
					}

					if(!empty($Input['To'])){
						$To = date("Y-m-d", strtotime($Input['To'][$keys]));
					}else{
						$To = NULL;
					}	
					$InsertDatae[$keys] = array_filter(array(
						"UserID" 				=> 	$UserID,
						"Employer" 			    =>	@$Input['Employer'][$keys],
						"Industries" 	=>	@$Input['Industries'][$keys],
						"ExpDesignation" 	=>	@$Input['ExpDesignation'][$keys],
						"JobProfile" 	 =>	@$Input['JobProfile'][$keys],
						"From" 	=>	$From,
						"To" =>	$To));
					$this->db->set($InsertDatae[$keys]);
    				$this->db->insert($this->db->dbprefix . 'tbl_user_experience');
					//$this->db->insert('tbl_user_experience', $InsertDatae[$keys]);
				}
			}

		//}
		//else if(!empty($Input['type']) && $Input['type']=='SocialMedia'){
			$InsertDataU = (array(
				"GoogleURL" 			=>	@strtolower($Input['GoogleURL']),				
				"TwitterURL" 				=>	@strtolower($Input['TwitterURL']),
				"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),
				"LinkedInURL" 				=>	@strtolower($Input['LinkedInURL']),
				"Website" 				=>	@strtolower($Input['Website'])
			));
			$this->db->where("UserID",$UserID);
			$this->db->update('tbl_users', $InsertDataU);
		//}
		
	
		if(!empty($Input['MediaName'])){
			$this->db->where('UserID',$UserID);
			$this->db->update('tbl_users',array("ProfilePic"=>$Input['MediaName']));
		}
		$this->Entity_model->updateEntityInfo($UserID,array('StatusID'=>@$Input['StatusID']));
		return $UserID;
	}






	/*
	Description: 	ADD user to system.
	Procedures:
	1. Add user to user table and get UserID.
	2. Save login info to users_login table.
	3. Save User details to users_profile table.

	*/
	function addUser($EntityID, $Input=array(), $UserTypeID, $SourceID, $StatusID=1){

		//print_r($EntityID);

		$InstituteID = $this->Common_model->getInstituteByEntity($EntityID);
		$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"FirstName, LastName");

		$EntityGUID = get_guid();

		$InsertDataEnt = array_filter(array(
			"EntityGUID" 		=>	$EntityGUID,
			"EntityTypeID"		=>	1,
			"EntryDate" 		=>	date("Y-m-d H:i:s"),
			"CreatedByUserID"   =>   $EntityID,
			"StatusID"			=>	 2,
			"InstituteID"       =>   $InstituteID
		));
		$this->db->set($InsertDataEnt);
    	$this->db->insert($this->db->dbprefix . 'tbl_entity');
		//$this->db->insert('tbl_entity', $InsertDataEnt);

		$StaffEntityID = $this->db->insert_id();

		if(!empty($Input['BirthDate'])){
			$originalDate = $Input['BirthDate'];
			$newDate = date("Y-m-d", strtotime($originalDate));
		}else{
			$newDate = '';
		}

		if(!empty($Input['AnniversaryDate'])){
			$AnniversaryDate = date("Y-m-d", strtotime($Input['AnniversaryDate']));
		}else{
			$AnniversaryDate = '';
		}

		$InsertDataU = array_filter(array(
				"UserID" 				=> 	$StaffEntityID,
				"UserGUID" 				=> 	$EntityGUID,			
				"UserTypeID" 			=>	$UserTypeID,
				"StoreID" 				=>	@$Input['StoreID'],
				"FirstName" 			=> 	@$Input['FirstName'],			
				"LastName" 				=> 	@$Input['LastName'],	
				"Email" 				=>	@strtolower($Input['Email']),
				"PhoneNumber" 			=>	@$Input['PhoneNumber'],
				"AdminAccess" 			=>	@$Input['Access'],
				"AppAccess" 			=>	@$Input['AppAccess']
		));
		$this->db->set($InsertDataU);
    	$this->db->insert($this->db->dbprefix . 'tbl_users');
		//$this->db->insert('tbl_users', $InsertDataU);


		if($UserTypeID == 11){
			if(!empty($Input['CourseID'])){
				$CourseID = implode(",",$Input['CourseID']);
			}else{
				$CourseID = NULL;
			}

			if(!empty($Input['SubjectID'])){
				$SubjectID = implode(",",$Input['SubjectID']);
			}else{
				$SubjectID = NULL;
			}

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$StaffEntityID,
				"CourseID" 		        =>	@$CourseID,
				"SubjectID" 		    =>	@$SubjectID
			));

			$this->db->select("UserID");
			$this->db->from("tbl_user_jobs");
			$this->db->where("UserID",$StaffEntityID);
			$this->db->limit(1);
			$check = $this->db->get();
			if($check->num_rows() > 0){
				$this->db->where("UserID",$StaffEntityID);
				$this->db->update('tbl_user_jobs', $InsertDatad);
			}else{
				$this->db->set($InsertDatad);
    			$this->db->insert($this->db->dbprefix . 'tbl_user_jobs');
				//$this->db->insert('tbl_user_jobs', $InsertDatad);
			}
		}

		//if(!empty($Input['type']) && $Input['type']=='PersonalDetails'){
			$InsertDataU = array_filter(array(
				"Gender" 				=>	@$Input['Gender'],
				"BirthDate" 			=>	@$newDate,
				"MaritalStatus" 		=>	@$Input['MaritalStatus'],
				"LivingStatus" 			=>	@$Input['LivingStatus'],
				"AnniversaryDate" 		=>	@$AnniversaryDate,
				"Address" 				=>	@$Input['Address'],
				"CityName" 				=>	@$Input['CityName'],
				"StateID" 				=>	@$Input['StateID'],
				"CityCode" 				=>	@$Input['CityCode']
			));
			if(!empty($InsertDataU)){
				$this->db->where("UserID",$StaffEntityID);
				$this->db->update('tbl_users', $InsertDataU);
			}
			
		// }
		// else if(!empty($Input['type']) && $Input['type']=='EmergencyDetails'){
			$InsertDataNom = array_filter(array(
				"UserID" 				=> 	$StaffEntityID,
				"Emergency" 			=>	@$Input['Emergency'],
				"RelationWithEmergency" =>	@$Input['RelationWithEmergency'],
				"EmergencyPhoneNumber" 	=>	@$Input['EmergencyPhoneNumber'],
				"EmergencyGender" 		=>	@$Input['EmergencyGender'],
				"EmergencyAddress" 		=>	@$Input['EmergencyAddress'],
				"EmergencyState" 		=>	@$Input['EmergencyState'],
				"EmergencyCityName" 	=>	@$Input['EmergencyCityName'],
				"EmergencyCityCode" 	=>	@$Input['EmergencyCityCode'],
				"Nominee" 			    =>	@$Input['Nominee'],
				"RelationWithNominee" 	=>	@$Input['RelationWithNominee'],
				"NomineePhoneNumber" 	=>	@$Input['NomineePhoneNumber'],
				"NomineeGender" 		=>	@$Input['NomineeGender'],
				"NomineeAddress" 		=>	@$Input['NomineeAddress'],
				"NomineeState" 		    =>	@$Input['NomineeState'],
				"NomineeCityName" 		=>	@$Input['NomineeCityName'],
				"NomineeCityCode" 		=>	@$Input['NomineeCityCode']
			));
			if(!empty($InsertDataNom)){
				$this->db->set($InsertDataNom);
    			$this->db->insert($this->db->dbprefix . 'tbl_user_contact_info');
			}
			
			//$this->db->insert('tbl_user_contact_info', $InsertDataNom);
		// }
		// else if(!empty($Input['type']) && $Input['type']=='NomineeDetails'){
			// $InsertDataNom = array_filter(array(
			// 	"UserID" 				=> 	$StaffEntityID,			
			// 	"Nominee" 			    =>	@$Input['Nominee'],
			// 	"RelationWithNominee" 	=>	@$Input['RelationWithNominee'],
			// 	"NomineePhoneNumber" 	=>	@$Input['NomineePhoneNumber'],
			// 	"NomineeGender" 		=>	@$Input['NomineeGender'],
			// 	"NomineeAddress" 		=>	@$Input['NomineeAddress'],
			// 	"NomineeState" 		    =>	@$Input['NomineeState'],
			// 	"NomineeCityName" 		=>	@$Input['NomineeCityName'],
			// 	"NomineeCityCode" 		=>	@$Input['NomineeCityCode']
			// ));
			
			// $this->db->set($InsertDataNom);
   //  		$this->db->insert($this->db->dbprefix . 'tbl_user_contact_info');
   //  		$this->db->insert('tbl_user_contact_info', $InsertDataNom);
		// }
		// else if(!empty($Input['type']) && $Input['type']=='Qualification'){
			$count = count($Input['Qualification']);
			$emptys=0;
			for($key=0; $key<$count; $key++) {
			   if(!empty($Input['Qualification'][$key])){
				   	$InsertDataQu = array_filter(array(
						"UserID" 				=> 	$StaffEntityID,
						"Qualification" 		=>	@$Input['Qualification'][$key],
						"Univercity" 			=>	@$Input['Univercity'][$key],
						"Specialization" 		=>	@$Input['Specialization'][$key],
						"PassingYear" 			 =>	@$Input['PassingYear'][$key],
						"Percentage" 			=>	@$Input['Percentage'][$key]
					 ));
				  if(!empty($InsertDataQu)){
				  	$this->db->set($InsertDataQu);
    			  	$this->db->insert($this->db->dbprefix . 'tbl_user_eduction');
				  }
				  
				  //$this->db->insert('tbl_user_eduction', $InsertDataQu);
			   	}
			}
		// }
		// else if(!empty($Input['type']) && $Input['type']=='JobsDetails'){
			if(!empty($Input['JoiningDate'])){
				$JoiningDate = date("Y-m-d", strtotime($Input['JoiningDate']));
			}else{
				$JoiningDate = NULL;
			}

			if(!empty($Input['AppraisalDate'])){
				$AppraisalDate = date("Y-m-d", strtotime($Input['AppraisalDate']));
			}else{
				$AppraisalDate = NULL;
			}

			$InsertDatad = array_filter(array(
				"UserID" 				=> 	$StaffEntityID,
				"Job" 		            =>	@$Input['Job'],
				"Department" 			=>	@$Input['Department'],
				"Designation" 			=>	@$Input['JobDesignation'],
				"ReportingManager" 		 =>	@$Input['ReportingManager'],
				"JoiningDate" 			=>	@$JoiningDate,
				"AppraisalDate" 		=>	@$AppraisalDate
			));

			if(!empty($InsertDatad)){

				$this->db->select("UserID");
				$this->db->from("tbl_user_jobs");
				$this->db->where("UserID",$StaffEntityID);
				$this->db->limit(1);
				$check = $this->db->get();
				if($check->num_rows() > 0){
					$this->db->where("UserID",$StaffEntityID);
					$this->db->update('tbl_user_jobs', $InsertDatad);
				}else{
					$this->db->set($InsertDatad);
	    			$this->db->insert($this->db->dbprefix . 'tbl_user_jobs');
					//$this->db->insert('tbl_user_jobs', $InsertDatad);
				}
			}
		// }
		// else if(!empty($Input['type']) && $Input['type']=='WorkExperience'){
			$counts = count($Input['Employer']);
			$empty=0;
			for($keys=0; $keys<$counts; $keys++) {

				if(empty($Input['Employer'][$keys])){
					$empty=$empty+1; 
					if($counts==$empty){
						if(!empty($Input['From'])){
							$From = date("Y-m-d", strtotime($Input['From'][$keys]));
						}else{
							$From = NULL;
						}

						if(!empty($Input['To'])){
							$To = date("Y-m-d", strtotime($Input['To'][$keys]));
						}else{
							$To = NULL;
						}
					$InsertDatae = array_filter(array(
						"UserID" 				=> 	$StaffEntityID,
						"Employer" 			    =>	@$Input['Employer'][$keys],
						"Industries" 	=>	@$Input['Industries'][$keys],
						"ExpDesignation" 	=>	@$Input['ExpDesignation'][$keys],
						"JobProfile" 	 =>	@$Input['JobProfile'][$keys],
						"From" 	=>	$From,
						"To" =>	$To
					));
					}
				}

				
				if(!empty($Input['Employer'][$keys])){
					if(!empty($Input['From'])){
						$From = date("Y-m-d", strtotime($Input['From'][$keys]));
					}else{
						$From = NULL;
					}

					if(!empty($Input['To'])){
						$To = date("Y-m-d", strtotime($Input['To'][$keys]));
					}else{
						$To = NULL;
					}	
					$InsertDatae[$keys] = array_filter(array(
						"UserID" 				=> 	$StaffEntityID,
						"Employer" 			    =>	@$Input['Employer'][$keys],
						"Industries" 	=>	@$Input['Industries'][$keys],
						"ExpDesignation" 	=>	@$Input['ExpDesignation'][$keys],
						"JobProfile" 	 =>	@$Input['JobProfile'][$keys],
						"From" 	=>	$From,
						"To" =>	$To));

					if(!empty($InsertDatae[$keys])){
						$this->db->set($InsertDatae[$keys]);
    					$this->db->insert($this->db->dbprefix . 'tbl_user_experience');
					}
					
					//$this->db->insert('tbl_user_experience', $InsertDatae[$keys]);
				}
			}

		// }
		// else if(!empty($Input['type']) && $Input['type']=='SocialMedia'){
			$InsertDataU = array_filter(array(
				"GoogleURL" 			=>	@strtolower($Input['GoogleURL']),
				"Website" 				=>	@strtolower($Input['Website']),
				"FacebookURL" 			=>	@strtolower($Input['FacebookURL']),
				"LinkedInURL" 				=>	@strtolower($Input['LinkedInURL']),
			));

			if(!empty($InsertDataU)){
				$this->db->where("UserID",$StaffEntityID);
				$this->db->update('tbl_users', $InsertDataU);
			}
		// }

		//$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}else{
			if(!empty($Input['Password'])){
	        	$UpdateArray = array_filter(array(
					"Password" 	=>	NULL ,
					"EntryDate" =>	date("Y-m-d H:i:s"),
					"UserID"  => $StaffEntityID,
					"SourceID" => 1
				));
				$this->db->insert('tbl_users_login', $UpdateArray);	

				$this->db->select('*');
				$this->db->from('admin_user_type_permission');	
				$this->db->where('UserTypeID',$UserTypeID);	
				$this->db->where('ModuleID',1);
				$this->db->limit(1);
				$que = $this->db->get();
				if($que->num_rows() == 0)
				{
					$this->Roles_model->AddPrmissionRoles($UserTypeID,1);
				}
	        }
			//print_r($Input['Email']); die;
			if($Input['Access']=='Yes'){
	
			  if (!empty($Input['Email'])) {
					/* Send welcome Email to User with Token. (only if source is Direct) */
					$InstituteData = $this->Common_model->getInstituteDetailByID($InstituteID,"Email,UserID,FirstName");

					//print_r($InstituteData); die;

					$MediaData = $this->Media_model->getMedia('',array("SectionID" => 'ProfilePic',"EntityID" => $InstituteID),TRUE);
			        $Record['InstituteProfilePic'] = ($MediaData ? $MediaData['Data'] : array());
			
			        if(!empty($Record['InstituteProfilePic'])){
				    	$MediaURL= $Record['InstituteProfilePic']['Records'][0]['MediaURL'];
				    }else{
				    	$MediaURL= "";
				    }

					$Token = $this->Recovery_model->generateToken($StaffEntityID, 1);

					

					$EmailText = "One time password reset code is given below:";
					
					$content = $this->load->view('emailer/recovery', array(
							"Name" => $Input['FirstName'].' '.$Input['LastName'],
							'Token' => $Token,
							"InstituteProfilePic"=>@$MediaURL,
							'Signature' => "Thanks, <br>".$InstituteData['FirstName'],
							'InstituteName' => $InstituteData['FirstName'],
							'$EmailText'	=> 	$EmailText				
						) , TRUE);

					sendMail(array(
						"From_name"  => $InstituteData['FirstName'],
						'emailTo' => $Input['Email'],
						'emailSubject' => $Input['FirstName'].' '.$Input['LastName']." Verify Your Email And Set Password",
						'emailMessage'	=>  emailTemplate($this->load->view('emailer/email_template',array("Content" =>  $content),TRUE))
					));

					/* Send SMS to employee to verify his email. */					
					sendSMS(array(
						'PhoneNumber' 	=> $Input['PhoneNumber'],			
						'Text'			=> "Welcome to ". $InstituteData['FirstName']." Institute. Please follow your email to verify your email address.",
					));

				}
			}	
		}

		if(!empty($Input['MediaName'])){
			$this->db->where('UserID',$StaffEntityID);
			$this->db->update('tbl_users',array("ProfilePic"=>$Input['MediaName']));
		}
		return $StaffEntityID;
	}

	function getUsers($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		//echo $Where['Keyword']; die();
		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'ls.StateName',
				'State_id'				=>	'ls.State_id',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	=>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	=>	'U.CityCode',
				'UrlType'	=>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
				"MaritalStatus"			=>  'U.MaritalStatus',

				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			=>	'E.StatusID',
				'PanStatusID'				=>	'U.PanStatus',
				'BankStatusID'				=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		 $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
        $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);

		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.FirstName,U.LastName,U.Email,U.UserTypeID,U.ProfilePic,U.PhoneNumber,U.CityName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.AnniversaryDate,U.CityCode,U.BirthDate,U.MaritalStatus,U.Gender,U.TelePhoneNumber,U.UrlType,U.AdminAccess,U.AppAccess,U.GoogleURL,U.LinkedInURL,U.FacebookURL,U.TwitterURL,UJ.Job,UJ.Department,UJ.Designation,UJ.ReportingManager,UT.UserTypeName,UJ.JoiningDate,UJ.AppraisalDate,UJ.CourseID,UJ.SubjectID,UCI.Nominee,UCI.RelationWithNominee,UCI.NomineePhoneNumber,UCI.NomineeGender,UCI.NomineeAddress,UCI.NomineeState,UCI.NomineeCityName,UCI.NomineeCityCode,UCI.Emergency,UCI.RelationWithEmergency,UCI.EmergencyPhoneNumber,UCI.EmergencyGender,UCI.EmergencyAddress,UCI.EmergencyState,UCI.EmergencyCityName,UCI.EmergencyCityCode,ls.StateName,ls.State_id,DP.DepartmentName,UK.Key,UK.Validity,UK.KeyAssignedOn,UK.ActivatedOn,UK.ExpiredOn');
	
		$this->db->select($Field,false);
        $this->db->from('tbl_users U');
		$this->db->join('tbl_entity E','U.UserID=E.EntityID');
		$this->db->join('tbl_users_type UT','UT.UserTypeID=U.UserTypeID','left');
		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID','left');
		$this->db->join('set_location_states ls', 'ls.State_id = U.StateID','left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID','left');
		/*shakti*/
		//$this->db->join('tbl_user_eduction UED', 'U.UserID = UED.UserID','left');
		$this->db->join('tbl_user_jobs UJ', 'U.UserID = UJ.UserID','left');
		$this->db->join('set_department DP', 'DP.DepartmentID = UJ.Department','left');
		$this->db->join('tbl_user_contact_info UCI', 'U.UserID = UCI.UserID','left');
		$this->db->join('tbl_staff_assign_keys UK', 'U.UserID = UK.StaffID','left');

		if(!empty($Where['PageType']) && $Where['PageType'] == "AssignKeys"){
			$this->db->where("U.AppAccess","Yes");
			$this->db->where('U.UserID NOT IN (select StaffID from  tbl_staff_assign_keys where KeyStatusID = 2 OR KeyStatusID = 1)');
		}

		if(!empty($Where['AppAccess'])){
			$this->db->where("U.AppAccess",$Where['AppAccess']);
		}

		if(!empty($Where['AdminAccess'])){
			$this->db->where("U.AdminAccess",$Where['AdminAccess']);
		}

		if(!empty($Where['Keyword'])){

			//$Where['Keyword'] = trim($Where['Keyword']);
			$this->db->group_start();
			$this->db->like("U.Email",$Where['Keyword']);
	
			$this->db->or_like("U.PhoneNumber",$Where['Keyword']);
			$this->db->or_like("UT.UserTypeName",$Where['Keyword']);

			$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);

			if( strpos( $Where['Keyword'], "Pending" ) !== false || strpos( $Where['Keyword'], "pending" ) !== false) {
    			$this->db->or_where("E.StatusID",1);
			}
			if( strpos( $Where['Keyword'], "Verified" ) !== false || strpos( $Where['Keyword'], "verified" ) !== false) {
    			$this->db->or_where("E.StatusID",2);
			}
			$this->db->group_end();
		}

		$this->db->where("E.StatusID!=",3);

		if(!empty($Where['UserTypeID'])){

			$this->db->where("U.UserTypeID",$Where['UserTypeID']);

		}
		
		if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}

		$this->db->where("E.InstituteID",$InstituteID);	
		$this->db->where("U.UserTypeID!=",10);	
		$this->db->where("U.UserTypeID!=",7);	

		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}
		

		// if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
		// 	$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		// }else{
		// 	$this->db->order_by('U.FirstName','ASC');			
		// }
		$this->db->group_by('U.UserID');
		$this->db->order_by('U.UserID','DESC');	
		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	
		//echo $this->db->last_query(); die;
		if($Query->num_rows()>0){

			foreach($Query->result_array() as $Record)
			{

				if(!empty($Where['UserID']))
				{
				if(!empty($Record['SubjectID']) && !empty($Record['CourseID'])){
					$querySubjects = $this->db->query("(SELECT `CategoryID`, `CategoryName`,'sc' as C FROM `set_categories` where `ParentCategoryID` in(".$Record['CourseID'].") and CategoryID Not IN (".$Record['SubjectID']."))  UNION SELECT m.`CategoryID`, m.`CategoryName`,'c' as C FROM `set_categories` as m where m.CategoryID IN (".$Record['SubjectID'].") group by CategoryID");
			
					if($querySubjects->num_rows() > 0){
						$Record['Course_subjects'] = $querySubjects->result_array();
					}
				 }else{
				 	$Record['Course_subjects']=array();
				 }
				}


			$Record['BatchesList'] = "";
			if(!empty($Record['UserID']))
			{
				$BatchesList = $this->getBatchsOfFaculty($Record['UserID']);
				if(isset($BatchesList) && !empty($BatchesList))
				{
					$temp = array();
					foreach ($BatchesList as $arr)
					{
						$temp[] = $arr['BatchName'];
					}

					$Record['BatchesList'] = implode(", ", $temp);
				}
			}	

			
			$Record['CoursesList'] = "";	
			if(!empty($Record['CourseID']))
			{
				$this->db->select('CategoryName');
				$this->db->from('set_categories');
				$this->db->where('CategoryID',$Record['CourseID']);
				$query = $this->db->get(); 
				$Record['Courses'] = $query->result_array();
				
				if(isset($Record['Courses']) && !empty($Record['Courses']))
				{
					$temp = array();
					foreach ($Record['Courses'] as $arr)
					{
						$temp[] = $arr['CategoryName'];
					}

					$Record['CoursesList'] = implode(", ", $temp);
				}

			}
			else
			{
				$Record['Courses']=array();
			}


				if(!empty($Where['UserID'])){

				$this->db->select('EXP.Employer,EXP.Industries,EXP.ExpDesignation,EXP.JobProfile,EXP.From,EXP.To,I.IndustryName');
				$this->db->from('tbl_user_experience EXP');
				$this->db->join('set_industries I','I.IndustryID = EXP.Industries');
				$this->db->where('EXP.UserID',$Record['UserID']);
				$query = $this->db->get(); 
			
				$Record['Work_experience'] = $query->result_array();
				
				$this->db->select('Qualification,Univercity,Specialization,PassingYear,Percentage');
				$this->db->from('tbl_user_eduction');
				$this->db->where('UserID',$Record['UserID']);
				$query = $this->db->get(); 
				$Record['Work_eduction'] = $query->result_array();

				}

				//$Record['UserID'] =$Record['UserID'];
				
				
				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			
			
			return $Return;
		}
		return FALSE;		
	}



	function getUserss($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'U.StateName',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	    =>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'           	=>	'U.CityCode',
				'UrlType'	            =>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',

				'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			    =>	'E.StatusID',
				'PanStatusID'			=>	'U.PanStatus',
				'BankStatusID'			=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		 $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
         $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);


		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);


		$InstituteID=$this->Common_model->getInstituteByEntity($this->EntityID);
		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.FirstName,U.LastName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.BirthDate,U.Gender,U.TelePhoneNumber,U.UrlType,UED.*,UEX.*,UJ.*');

		//$this->db->select('U.UserGUID, U.UserID,U.FirstName,U.LastName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.BirthDate,U.Gender,U.TelePhoneNumber,U.UrlType,UED.*,UEX.*,UJ.*');
		$this->db->select($Field,false);

		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}		
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		$this->db->where("U.UserID","E.EntityID", FALSE);

		$this->db->where("E.InstituteID",$InstituteID);

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
				$this->db->where("UT.EntityID",$InstituteID);	
		}
		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		/*shakti*/
		$this->db->join('tbl_user_eduction UED', 'U.UserID = UED.UserID', 'left');
		$this->db->join('tbl_user_experience UEX', 'U.UserID = UEX.UserID', 'left');
		$this->db->join('tbl_user_jobs UJ', 'U.UserID = UJ.UserID', 'left');

	    $this->db->where("E.InstituteID",$InstituteID);


		/* Total records count only if want to get multiple records */
		if($multiRecords){ 
			$TempOBJ = clone $this->db;
			$TempQ = $TempOBJ->get();
			$Return['Data']['TotalRecords'] = $TempQ->num_rows();
			$this->db->limit($PageSize, paginationOffset($PageNo, $PageSize)); /*for pagination*/
		}else{
			$this->db->limit(1);
		}

		$Query = $this->db->get();	echo $this->db->last_query(); die;
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}

	/* get profile */
	function getprofile($Field='', $Where=array(), $multiRecords=FALSE,  $PageNo=1, $PageSize=15){
		/*Additional fields to select*/
		$Params = array();
		if(!empty($Field)){
			$Params = array_map('trim',explode(',',$Field));
			$Field = '';
			$FieldArray = array(
				'RegisteredOn'			=>	'DATE_FORMAT(E.EntryDate, "'.DATE_FORMAT.' %h:%i %p") RegisteredOn',
				'LastLoginDate'			=>	'DATE_FORMAT(UL.LastLoginDate, "'.DATE_FORMAT.' %h:%i %p") LastLoginDate',
				'Rating'				=>	'E.Rating',	
				'UserTypeName'			=>	'UT.UserTypeName',
				'IsAdmin'				=>	'UT.IsAdmin',				
				'UserID'				=>	'U.UserID',
				'UserTypeID'			=>	'U.UserTypeID',
				'FirstName'				=>	'U.FirstName',
				'MiddleName'			=>	'U.MiddleName',
				'LastName'				=>	'U.LastName',
				'ProfilePic'			=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/picture/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfilePic)) AS ProfilePic',
				'ProfileCoverPic'		=>	'IF(U.ProfilePic IS NULL,CONCAT("'.BASE_URL.'","uploads/profile/cover/","default.jpg"),CONCAT("'.BASE_URL.'","uploads/profile/picture/",U.ProfileCoverPic)) AS ProfileCoverPic',
				'About'					=>	'U.About',
				'About1'				=>	'U.About1',
				'About2'				=>	'U.About2',
				'Email'					=>	'U.Email',
				'EmailForChange'		=>	'U.EmailForChange',				
				'Username'				=>	'U.Username',
				'Gender'				=>	'U.Gender',
				'BirthDate'				=>	'DATE_FORMAT(U.BirthDate, "'.DATE_FORMAT.'") BirthDate',
				'Address'				=>	'U.Address',
				'Address1'				=>	'U.Address1',
				'Postal'				=>	'U.Postal',
				'CountryCode'			=>	'U.CountryCode',
				'CountryName'			=>	'CO.CountryName',
				'CityName'				=>	'U.CityName',
				'StateName'				=>	'U.StateName',
				'PhoneNumber'			=>	'U.PhoneNumber',
				'PhoneNumberForChange'	=>	'U.PhoneNumberForChange',
				'TelePhoneNumber'	    =>	'U.TelePhoneNumber',
				'RegistrationNumber'	=>	'U.RegistrationNumber',
				'CityCode'	            =>	'U.CityCode',
				'UrlType'	            =>	'U.UrlType',
				'Website'				=>	'U.Website',
				'FacebookURL'			=>	'U.FacebookURL',
				'TwitterURL'			=>	'U.TwitterURL',
				'GoogleURL'				=>	'U.GoogleURL',
				'InstagramURL'			=>	'U.InstagramURL',
				'LinkedInURL'			=>	'U.LinkedInURL',
				'WhatsApp'				=>	'U.WhatsApp',
                'ReferralCode'			=>	'(SELECT ReferralCode FROM tbl_referral_codes WHERE tbl_referral_codes.UserID=U.UserID LIMIT 1) AS ReferralCode',

				'Status'				=>	'CASE E.StatusID
				when "1" then "Pending"
				when "2" then "Verified"
				when "3" then "Deleted"
				when "4" then "Blocked"
				when "8" then "Hidden"		
				END as Status',
				'StatusID'			    =>	'E.StatusID',
				'PanStatusID'			=>	'U.PanStatus',
				'BankStatusID'			=>	'U.BankStatus',
			);
			foreach($Params as $Param){
				$Field .= (!empty($FieldArray[$Param]) ? ','.$FieldArray[$Param] : '');
			}
		}
		 $this->UserID = (!empty($this->UserID) ? $this->UserID : $this->SessionUserID);
		$this->db->select('U.UserGUID, U.UserID,  CONCAT_WS(" ",U.FirstName,U.LastName) FullName,U.FirstName,U.LastName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.BirthDate,U.Gender,U.TelePhoneNumber,U.UrlType,UED.*,UEX.*,UJ.*');
		//$this->db->select('U.UserGUID, U.UserID,U.FirstName,U.LastName,U.Email,U.ProfilePic,U.PhoneNumber,U.CityName,U.StateName,U.Address,U.Postal,U.Username,U.RegistrationNumber,U.CityCode,U.BirthDate,U.Gender,U.TelePhoneNumber,U.UrlType,UED.*,UEX.*,UJ.*');
		$this->db->select($Field,false);

		/* distance calculation - starts */
		/* this is called Haversine formula and the constant 6371 is used to get distance in KM, while 3959 is used to get distance in miles. */
		if(!empty($Where['Latitude']) && !empty($Where['Longitude'])){
			$this->db->select("(3959*acos(cos(radians(".$Where['Latitude']."))*cos(radians(E.Latitude))*cos(radians(E.Longitude)-radians(".$Where['Longitude']."))+sin(radians(".$Where['Latitude']."))*sin(radians(E.Latitude)))) AS Distance",false);
			$this->db->order_by('Distance','ASC');

			if(!empty($Where['Radius'])){
				$this->db->having("Distance <= ".$Where['Radius'],null,false);
			}		
		}		
		/* distance calculation - ends */

		$this->db->from('tbl_entity E');
		$this->db->from('tbl_users U');
		$this->db->where("U.UserID",$this->UserID, FALSE);	
		//$this->db->where("E.CreatedByUserID",);	
		//$this->db->where("U.UserID",12093, FALSE);	

		if(array_keys_exist($Params, array('UserTypeName','IsAdmin')) || !empty($Where['IsAdmin'])) {
			$this->db->from('tbl_users_type UT');
			$this->db->where("UT.UserTypeID","U.UserTypeID", FALSE);	
		}
		$this->db->join('tbl_users_login UL', 'U.UserID = UL.UserID', 'left');
		$this->db->join('tbl_users_settings US', 'U.UserID = US.UserID', 'left');
		/*shakti*/
		$this->db->join('tbl_user_eduction UED', 'U.UserID = UED.UserID', 'left');
		//$this->db->join('tbl_user_experience UEX', 'U.UserID = UEX.UserID', 'left');
		$this->db->join('tbl_user_jobs UJ', 'U.UserID = UJ.UserID', 'left');
		$this->db->join('tbl_user_nominee nominee', 'U.UserID = nominee.UserID', 'left');

		if(array_keys_exist($Params, array('CountryName'))) {
			$this->db->join('set_location_country CO', 'U.CountryCode = CO.CountryCode', 'left');
		}

		if(!empty($Where['Keyword'])){
			$Where['Keyword'] = trim($Where['Keyword']);
			if(validateEmail($Where['Keyword'])){
				$Where['Email'] = $Where['Keyword'];
			}
			elseif(is_numeric($Where['Keyword'])){
				$Where['PhoneNumber'] = $Where['Keyword'];
			}else{
				$this->db->group_start();
				$this->db->like("U.FirstName", $Where['Keyword']);
				$this->db->or_like("U.LastName", $Where['Keyword']);
				$this->db->or_like("CONCAT_WS('',U.FirstName,U.Middlename,U.LastName)", preg_replace('/\s+/', '', $Where['Keyword']), FALSE);
				$this->db->group_end();				
			}

		}

		if(!empty($Where['SourceID'])){
			$this->db->where("UL.SourceID",$Where['SourceID']);			
		}

		if(!empty($Where['UserTypeID'])){
			$this->db->where_in("U.UserTypeID",$Where['UserTypeID']);
		}
		
		if(!empty($Where['UserTypeIDNot']) && $Where['UserTypeIDNot']=='Yes'){
			$this->db->where("U.UserTypeID!=",$Where['UserTypeIDNot']);
		}
        if(!empty($Where['UserID'])){
			$this->db->where("U.UserID",$Where['UserID']);
		}
		if(!empty($Where['UserIDNot'])){
			$this->db->where("U.UserID!=",$Where['UserIDNot']);
		}
		if(!empty($Where['UserGUID'])){
			$this->db->where("U.UserGUID",$Where['UserGUID']);
		}
		if(!empty($Where['Username'])){
			$this->db->where("U.Username",$Where['Username']);
		}
		if(!empty($Where['Email'])){
			$this->db->where("U.Email",$Where['Email']);
		}
		if(!empty($Where['PhoneNumber'])){
			$this->db->where("U.PhoneNumber",$Where['PhoneNumber']);
		}

		if(!empty($Where['LoginKeyword'])){
			$this->db->group_start();
			$this->db->where("U.Email",$Where['LoginKeyword']);
			$this->db->or_where("U.Username",$Where['LoginKeyword']);
			$this->db->or_where("U.PhoneNumber",$Where['LoginKeyword']);
			$this->db->group_end();
		}
		if(!empty($Where['Password'])){
			$this->db->where("UL.Password",md5($Where['Password']));
		}

		if(!empty($Where['IsAdmin'])){
			$this->db->where("UT.IsAdmin",$Where['IsAdmin']);
		}
		if(!empty($Where['StatusID'])){
			$this->db->where("E.StatusID",$Where['StatusID']);
		}
		if(!empty($Where['PanStatus'])){
			$this->db->where("U.PanStatus",$Where['PanStatus']);
		}
		if(!empty($Where['BankStatus'])){
			$this->db->where("U.BankStatus",$Where['BankStatus']);
		}

		if(!empty($Where['OrderBy']) && !empty($Where['Sequence']) && in_array($Where['Sequence'], array('ASC','DESC'))){
			$this->db->order_by($Where['OrderBy'],$Where['Sequence']);
		}else{
			$this->db->order_by('U.FirstName','ASC');			
		}


      $this->db->limit(1);
		$Query = $this->db->get();	//echo $this->db->last_query(); die;
		if($Query->num_rows()>0){
			foreach($Query->result_array() as $Record){

				$this->db->select('*');
				$this->db->from('tbl_user_experience');
				$this->db->where('UserID',$Record['UserID']);
				$query = $this->db->get(); 
				$Record['Work_experience'] = $query->result_array();

				if(!$multiRecords){
					return $Record;
				}
				$Records[] = $Record;
			}

			$Return['Data']['Records'] = $Records;
			return $Return;
		}
		return FALSE;		
	}
	
	
	/*
	Description: 	Use to set new email address of user.
	*/
	function updateEmail($UserID, $Email){
		/*check new email address is not in use*/
		$UserData=$this->Users_model->getUsers('', array('Email'=>$Email,));
		if(!$UserData){
			$this->db->trans_start();
			/*update profile table*/
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', array("Email" => $Email, "EmailForChange" => null));

			/* Delete session */
			$this->db->limit(1);
			$this->db->delete('tbl_users_session', array('UserID' => $UserID));
			/* Delete session - ends */
			$this->db->trans_complete();	        
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	
		return TRUE;
	}


	/*
	Description: 	Use to set new email address of user.
	*/
	function updatePhoneNumber($UserID, $PhoneNumber){
		/*check new PhoneNumber is not in use*/
		$UserData=$this->Users_model->getUsers('StatusID', array('PhoneNumber'=>$PhoneNumber));
		if(!$UserData){
			$this->db->trans_start();
			/*update profile table*/
			$this->db->where('UserID', $UserID);
			$this->db->limit(1);
			$this->db->update('tbl_users', array("PhoneNumber" => $PhoneNumber, "PhoneNumberForChange" => null));

			/* change entity status to activate */
			if($UserData['StatusID']==1){
				$this->Entity_model->updateEntityInfo($UserID, array("StatusID"=>2));					
			}

			$this->db->trans_complete();	        
			if ($this->db->trans_status() === FALSE)
			{
				return FALSE;
			}
		}	
		return TRUE;
	}

	/*user type get*/
	function getusertype($EntityID)
	{
	    $this->db->select('*');
		$this->db->from('tbl_users_type');
		$this->db->where("EntityID",$EntityID);
		$Query = $this->db->get();	
		
		if($Query->result_array()>0){
			foreach($Query->result_array() as $Record){
				
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;
   }


   function getCourse($InstituteID){

		$this->db->select("CategoryName,CategoryGUID,CategoryID");
		$this->db->from("set_categories");
		$this->db->join("tbl_entity as E", "E.EntityID = set_categories.CategoryID");
		$this->db->join("tbl_entity as EP", "EP.EntityID = set_categories.ParentCategoryID");
		$this->db->where("E.StatusID", 2);
		$this->db->where("EP.StatusID", 2);
		$this->db->where("EP.InstituteID", $InstituteID);
		$this->db->where("CategoryTypeID", 2);

		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$course = $query->result_array();
			return 	$course;
		}
		return FALSE;

   } 



   function getSubject($CategoryID){
   	    $this->db->select('C.*');
		$this->db->from('set_categories C');
		$this->db->join('tbl_entity E','E.EntityID=C.CategoryID','left');
		$this->db->where("C.CategoryTypeID",3);
		$this->db->where("C.ParentCategoryID IN (".$CategoryID.")");
		//$this->db->where("E.InstituteID",$InstituteID);
		$this->db->where("E.StatusID!=",6);
		$Query = $this->db->get();	
		//echo $this->db->last_query(); die();
		
		if($Query->result_array()>0){

			foreach($Query->result_array() as $Record){
				
			 $Records[] = $Record;
			}
			$Return['Data']['Records'] = $Records;

			return $Return;
		}
		return FALSE;

   }



   function delete($UserID){

		if(!empty($UserID)){

			$UpdateData = array_filter(array(

				"StatusID"  => 3
				
			));

			$this->db->where('EntityID', $UserID);

			$this->db->delete('tbl_entity');
			
			return TRUE;
		}
		return FALSE;
	}



	function getBatchsOfFaculty($EntityID)
	{
		$bulk_data = array();

		$sql = "SELECT b.BatchID, b.BatchName, b.BatchGUID
		FROM tbl_batch b
		INNER JOIN tbl_batchbyfaculty f ON f.BatchID = b.BatchID AND f.FacultyID = $EntityID		
		GROUP BY b.BatchID";

		$BulkQuery = $this->db->query($sql);

		if($BulkQuery->num_rows()>0)
		{
			foreach($BulkQuery->result_array() as $Where)
			{				
				/*$sql = "SELECT s.StudentGUID, u.FirstName, u.LastName 
				FROM tbl_students s
				INNER JOIN tbl_users u ON u.UserID = s.StudentID AND u.UserTypeID = 7
				WHERE s.BatchID = ".$Where['BatchID']."
				ORDER BY u.FirstName, u.LastName
				";				

				$Query = $this->db->query($sql);

				if($Query->num_rows() > 0)
				{
					$data = $Query->result_array();
					
					if(count($data) > 0)
					{ 
						$indata = array();
						
						foreach($data as $Record)
						{
							$indata[] = $Record;
						}						
					}
				}

				$bulk_data[] = array("BatchID" => $Where['BatchID'], "BatchName" => $Where['BatchName'], "StudentsList" => $indata);*/

				$bulk_data[] = array("BatchID" => $Where['BatchID'], "BatchName" => $Where['BatchName'], "BatchGUID" => $Where['BatchGUID']);
			}

			return $bulk_data;
		}

		return FALSE;	
	}




	//Used to save Annoucement record------------------------------------------------
	function addAnnoucement($EntityID, $Input=array(), $actionType = 0)
	{
		$EntityID = $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);

		$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);
		
		$this->db->trans_start();

		@$Input['BatchID'] = str_replace("[", "", @$Input['BatchID']);
		@$Input['BatchID'] = str_replace("]", "", @$Input['BatchID']);


		@$Input['StudentID'] = str_replace("[", "", @$Input['StudentID']);
		@$Input['StudentID'] = str_replace("]", "", @$Input['StudentID']);

		if($actionType == 1 && $Input['AnnoucementID'] > 0)
		{
			$EntityID = $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
			$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

			$sql = "SELECT AnnoucementID
			FROM tbl_annoucement
			WHERE AnnoucementID = ".$Input['AnnoucementID']." AND AnnoucementFacultyID = $EntityID			
			LIMIT 1";

			$Query = $this->db->query($sql);

			if($Query->num_rows()>0)
			{
				$InsertData = array(
				"AnnoucementBatchID" => @$Input['BatchID'],			
				"AnnoucementStudentID" => @$Input['StudentID'],
				"AnnoucementMessage" => @$Input['Message'],
				"AnnoucementForDate" =>	date("Y-m-d H:i:s", strtotime(@$Input['ForDate'])),
				"AnnoucementModifiedDate" =>	date('Y-m-d H:i:s'));

				$this->db->where('AnnoucementID', @$Input['AnnoucementID']);
				$this->db->where('AnnoucementInstituteID', $InstituteID);			
				$this->db->where('AnnoucementFacultyID', $EntityID);
				
				$this->db->update('tbl_annoucement', $InsertData);

				$AnnoucementID = $this->db->affected_rows();
			}
			else
			{
				return 0;
			}	
		}
		else
		{
			$InsertData = array(
			"AnnoucementFacultyID" => $EntityID,			
			"AnnoucementInstituteID" => $InstituteID,			

			"AnnoucementBatchID" => @$Input['BatchID'],			
			"AnnoucementStudentID" => @$Input['StudentID'],
			"AnnoucementMessage" => @$Input['Message'],
			"AnnoucementForDate" =>	date("Y-m-d H:i:s", strtotime(@$Input['ForDate'])),
			"AnnoucementCreatedDate" =>	date('Y-m-d H:i:s'),
			"AnnoucementModifiedDate" =>	date('Y-m-d H:i:s'));

			$this->db->insert('tbl_annoucement', $InsertData);
			
			$AnnoucementID = $this->db->insert_id();
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			return FALSE;
		}

		return $AnnoucementID;		
	}


	//Used to delete Annoucement record------------------------------------------------
	function deleteAnnoucement($EntityID, $Input = array())
	{
		if(!empty(@$Input['AnnoucementID']) && @$Input['AnnoucementID'] > 0)
		{
			$EntityID = $this->EntityID = (!empty($this->EntityID) ? $this->EntityID : $this->SessionUserID);
			$InstituteID = $this->Common_model->getInstituteByEntity($this->EntityID);

			$sql = "SELECT AnnoucementID
			FROM tbl_annoucement
			WHERE AnnoucementID = ".$Input['AnnoucementID']." AND AnnoucementFacultyID = $EntityID			
			LIMIT 1";

			$Query = $this->db->query($sql);

			if($Query->num_rows()>0)
			{
				$this->db->where('AnnoucementID', @$Input['AnnoucementID']);
				$this->db->where('AnnoucementInstituteID', $InstituteID);			
				$this->db->where('AnnoucementFacultyID', $EntityID);

				$this->db->delete('tbl_annoucement');
				
				return 1;
			}
			else
			{
				return 3;
			}	
		}
		else
		{
			return 2;
		}
	}


	function listAnnoucement($EntityID)
	{
		$output_data = array();

		$sql = "SELECT a.AnnoucementID, a.AnnoucementMessage, a.AnnoucementForDate, a.AnnoucementIsSent, a.AnnoucementBatchID, a.AnnoucementStudentID
		FROM tbl_batch b
		INNER JOIN tbl_batchbyfaculty f ON f.BatchID = b.BatchID AND f.FacultyID = $EntityID		
		INNER JOIN tbl_annoucement a ON a.AnnoucementFacultyID = f.FacultyID AND a.AnnoucementFacultyID = $EntityID
		WHERE b.BatchID IN (a.AnnoucementBatchID) AND b.StatusID = 2 AND DATE(a.AnnoucementForDate) >= CURDATE() 
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{				
				$sql = "SELECT GROUP_CONCAT(b.BatchName SEPARATOR ', ') as BatchName
				FROM tbl_batch b
				INNER JOIN tbl_batchbyfaculty f ON f.BatchID = b.BatchID AND f.FacultyID = $EntityID	
				WHERE b.BatchID IN (".$Where['AnnoucementBatchID'].") AND f.FacultyID = $EntityID				
				";				

				$Query = $this->db->query($sql);

				$data[0]['BatchName'] = "";

				if($Query->num_rows() > 0)
				{
					$data = $Query->result_array();
				}

				$output_data[] = array("BatchName" => $data[0]['BatchName'], 
					"AnnoucementID" => $Where['AnnoucementID'],
					"AnnoucementMessage" => $Where['AnnoucementMessage'],
					"AnnoucementForDate" => $Where['AnnoucementForDate'],
					"AnnoucementIsSent" => $Where['AnnoucementIsSent'],
					"AnnoucementBatchID" => $Where['AnnoucementBatchID'],
					"AnnoucementStudentID" => $Where['AnnoucementStudentID']
				);
			}

			return $output_data;
		}

		return FALSE;	
	}



	function getStudentsOfBatchForFaculty($EntityID, $Inputs)
	{
		$bulk_data = array();

		$Inputs['BatchID'] = str_replace("[", "", $Inputs['BatchID']);
		$BatchIDS = str_replace("]", "", $Inputs['BatchID']);

		$sql = "SELECT s.StudentID, u.FirstName, u.LastName 
		FROM tbl_students s
		INNER JOIN tbl_users u ON u.UserID = s.StudentID AND u.UserTypeID = 7
		WHERE s.BatchID IN (".$BatchIDS.") 
		ORDER BY u.FirstName, u.LastName
		";	

		$BulkQuery = $this->db->query($sql);

		if($BulkQuery->num_rows()>0)
		{
			foreach($BulkQuery->result_array() as $Record)
			{
				$bulk_data[] = $Record;					
			}

			return $bulk_data;
		}

		return FALSE;	
	}



	function getLoanList($EntityID)
	{
		$output_data = array();

		$sql = "SELECT LoanID, LoanPurpose, LoanPurpose, LoanAmount, LoanFamilyIncome, DATE_FORMAT(LoanRequestDate, '%d-%M-%Y %H:%i:%s') as LoanRequestDate, Status
		FROM tbl_apply_for_loan		
		WHERE StudentID = '$EntityID'
		ORDER BY LoanID DESC
		LIMIT 1 
		";

		$Query = $this->db->query($sql);

		if($Query->num_rows()>0)
		{
			foreach($Query->result_array() as $Where)
			{
				$output_data[] = $Where;
			}

			return $output_data;
		}

		return FALSE;	
	}


}
