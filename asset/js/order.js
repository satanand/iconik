app.controller('PageController', function ($scope, $http,$timeout){

    $scope.availableData = [];
    $scope.APIURL = API_URL+"upload/image";
    $scope.SessionKey = SessionKey;

    /*----------------*/

    $scope.getFilterData = function ()

    {
        $scope.getInstitutes();

        $scope.getSecondLevelPermission();

     //    var data = 'SessionKey='+SessionKey+$('#filterPanel form').serialize();

     //    $http.post(API_URL+'admin/store/getOrderFilterData', data, contentType).then(function(response) {

     //        var response = response.data;

     //        if(response.ResponseCode==200 && response.Data){ /* success case */

     //         $scope.filterData =  response.Data;

     //         $timeout(function(){

     //            $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

     //        }, 300);          

     //     }

     // });

    }

    $scope.applyFilter = function (check,OrderType)
    {
        $scope.data = angular.copy($scope.orig); /*copy and reset from original scope*/
        $scope.getList(check,OrderType);
    }



    /*list append*/
    $scope.getInstitutes = function() {
        $scope.Institutes = [];
        var data = 'SessionKey=' + SessionKey+'&Params=FullName,UserGUID';

        $http.post(API_URL + 'institute/getInstitutes', data, contentType).then(function(response) {
            var response = response.data;

            if (response.ResponseCode == 200 && response.Data.Records) { /* success case */
                $scope.data.totalRecords = response.Data.TotalRecords;
                for (var i in response.Data.Records) {
                    $scope.Institutes.push(response.Data.Records[i]);
                }
                
            } else {
               $scope.Institutes = [];
            }
        });
    }





    /*----------------*/

    $scope.getKeysPrice = function ()

    {

        var data = 'SessionKey='+SessionKey;
        $scope.priceKey3 = 0; $scope.priceKey6 = 0; $scope.priceKey12 = 0; $scope.priceKey24 = 0;
        $http.post(API_URL+'order/getKeysPrice', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

             $scope.data.KeysPrice =  response.Data;
            
            $.each($scope.data.KeysPrice, function( index, value ) {
                if(value.Validity == 3 || value.Validity == 03){
                    $scope.priceKey3 = value.Price;
                }


                if(value.Validity == 6 || value.Validity == 06){
                    $scope.priceKey6 = value.Price;
                }


                if(value.Validity == 12){
                    $scope.priceKey12 = value.Price;
                }


                if(value.Validity == 24){
                    $scope.priceKey24 = value.Price;
                }
            });      

             console.log($scope.data.KeysPrice); 

            }

        });

    }





    /*----------------*/

    $scope.calculateKeysPrice = function (Validity,TotalKeys)

    {   //alert(Validity);

        $scope.availableData.Price = 0;

        console.log($scope.data.KeysPrice);

        $.each($scope.data.KeysPrice, function(i, v) {

            //console.log(Price); console.log(TotalKeys);            

            //if (v.Validity == Validity) {

                var Price = parseInt(v.Price); 

                var TotalKeys = $("#key"+v.Validity).val();

                if($.isNumeric(TotalKeys)){
                   var key_price  = parseInt(Price)*parseInt(TotalKeys); 
               }else{
                   var key_price  = parseInt(Price)*0; 
               }

                

                $("#price"+v.Validity).val(key_price);

                console.log($scope.availableData.Price);

                $scope.availableData.Price = parseInt($scope.availableData.Price)+parseInt(key_price);

                console.log($scope.availableData.Price);                

                $("#price-msg").hide();

                

            

        });

        $("#TotalPrice").val($scope.availableData.Price);

    }





    /*list append*/

    $scope.getList = function (check,OrderType)

    {
       // alert($scope.UserGUID);
       
       var addId = 0;
       $scope.UserGUID = "";
       if(check!=1 && check!=0){
            if ($scope.data.listLoading || $scope.data.noRecords) return;

            if($scope.getUrlParameter('UserGUID'))
            {
                var UserGUID = $scope.getUrlParameter('UserGUID');
                $scope.UserGUID = UserGUID;
                addId = 1;
            }
                
            $scope.data.dataList = [];
        }

        if($scope.getUrlParameter('UserGUID'))
        {
            var UserGUID = $scope.getUrlParameter('UserGUID');
            $scope.UserGUID = UserGUID;
            addId = 1;
        }
      // alert($scope.UserGUID);
       $scope.data.listLoading = true;
       $scope.data.noRecords = true;

       var data = 'SessionKey='+SessionKey+'&menu_type='+OrderType+'&OrderFor=Keys&'+$('#filterForm').serialize();      
        if(addId == 1)
        {
            data = data + '&UserGUID='+$scope.UserGUID;
        }
        
        
        $http.post(API_URL+'order/getOrders', data, contentType).then(function(response) {

            var response = response.data;

            

            if(response.ResponseCode==200 && response.Data){ /* success case */

                $scope.data.totalRecords = response.Data.TotalRecords;

                for (var i in response.Data) {

                 $scope.data.dataList.push(response.Data[i]);

                }
                console.log($scope.data.dataList);
                $scope.data.pageNo++;     

                $timeout(function() 
                {
                    $(".chosen-select").chosen({
                        width: '100%',
                        "disable_search_threshold": 8,
                        "placeholder_text_multiple": "Please Select",
                    }).trigger("chosen:updated");

                }, 200);          

            }else{

                $scope.data.noRecords = true;

            }

            $scope.data.listLoading = false;

        });

    }







    /*load add form*/



    $scope.loadFormAdd = function (Position, CategoryGUID)



    {

        $scope.availableData.Price = 0;



        $scope.getKeysPrice();



        $scope.templateURLAdd = PATH_TEMPLATE+'order/add_form.htm?'+Math.random();



        $('#add_model').modal({show:true});



        $timeout(function(){            



           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");



       }, 200);



    }



    $scope.getSingleOrder = function (OrderGUID){

        $scope.templateURLPayment = PATH_TEMPLATE+'order/payment_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&OrderGUID='+OrderGUID;

        $http.post(API_URL+'order/getOrders', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200 && response.Data){ /* success case */

                //$scope.formData = response.Data[0];
                //$scope.data.dataList[$scope.data.Position] = response.Data[0];
                $scope.data.dataList.push(response.Data[0]);
               
            }
        });
    }


    $scope.payNow = function (Position, OrderGUID, check)
    {

        if(check == 1){
            $('#view_model .close').click();   
        }

        $scope.data.Position = Position;

        $scope.templateURLPayment = PATH_TEMPLATE+'order/payment_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

        var data = 'SessionKey='+SessionKey+'&OrderGUID='+OrderGUID+'&OrderFor=Keys';

        $http.post(API_URL+'order/getOrders', data, contentType).then(function(response) {

            var response = response.data;

            $scope.payNowStatus = true;

            console.log(response.Data);

            if(response.ResponseCode==200 && response.Data)
            { /* success case */

                $scope.formData = response.Data[0];

                $('#add_payment').modal({show:true});   

                 $scope.data.pageLoading = false;                     

            }else{

               // $scope.data.noRecords = true;

            }
        });
    }


    /*load delete form*/

    $scope.loadFormView = function (Position, OrderGUID)

    {
        //$('#myModal').modal({show:true});
        console.log(OrderGUID);
        // if($('#myModal').is(':visible')){
        //     $('#myModal .close').click();
        // }
        

        $scope.data.Position = Position;

        $scope.templateURLView = PATH_TEMPLATE+'order/view_form.htm?'+Math.random();

        $scope.data.pageLoading = true;

       

        $scope.data.pageLoading = false;

        var data = 'SessionKey='+SessionKey+'&OrderGUID='+OrderGUID;

        $http.post(API_URL+'order/getOrders', data, contentType).then(function(response) {

            var response = response.data;

            console.log(response.Data);

            if(response.ResponseCode==200 && response.Data){ /* success case */

                
                $scope.formData = response.Data[0];  

                $('#view_model').modal({show:true});

            }

        });



    }





    /*edit data*/

    $scope.addData = function (check)

    {

        if($("#TotalPrice").val() > 0){
            //$("input[name='sport']:checked")
            var Validity = $("input[name='Validity']:checked")
                .map(function() {
                    if($(this).val()){
                        return $(this).val();
                    }
                }).get();
            console.log(Validity);
            var OrderKeys = $("input[name='OrderKeys[]']")
                .map(function() {
                    if($(this).val()){
                        return $(this).val();
                    }
                }).get();

            var Price = $("input[name='Price[]']")
                .map(function() {
                    if($(this).val() > 0){
                        return $(this).val();
                    }
                }).get();

            var TotalPrice = $("#TotalPrice").val();
            var OrderGUID = "";
            if($scope.OrderGUID){
                OrderGUID = $scope.formData.OrderGUID;
            }

            $scope.formData = [];

            $scope.formData.RemainingAmount = TotalPrice;

            $scope.addDataLoading = true;
            
            $http.post(API_URL+'order/add', 'SessionKey='+SessionKey+'&OrderGUID='+OrderGUID+'&Validity='+Validity+'&OrderKeys='+OrderKeys+'&Price='+Price+'&TotalPrice='+TotalPrice, contentType).then(function(response) {

                var response = response.data;
                $scope.addDataLoading = false;
                if(response.ResponseCode==200){ /* success case */

                   //SuccessPopup(response.Message);

                    $scope.data.pageLoading = false;

                    $scope.formData = response.Data

                    $scope.data.dataList = [];

                    //$scope.data.dataList = [];

                    $scope.data.noRecords = false;

                    $scope.getList(1,'buy_now');

                   // $scope.data.dataList.push(response.Data[i]);

                    $('#add_model .close').click();

                    
                    if(check == 2){
                        $timeout(function(){  

                            $scope.payNowStatus = false;

                            $scope.templateURLPayment = PATH_TEMPLATE+'order/payment_form.htm?'+Math.random();

                            $('#add_payment').modal({show:true});

                            $scope.formData.RemainingAmount = TotalPrice;

                            $("#ChequeAmount").val(TotalPrice);
                         
                        }, 500);
                    }

                    

                }else{

                    ErrorPopup(response.Message);
                }

            });

        }else{

            alert("Sorry! Can not continue with total price 0.");

        }        

    }

    $scope.closeView = function(key,MediaURL,OrderGUID){
        $('#view_model .close').click();
        $scope.key = key;
        $scope.MediaURL = MediaURL;
        $scope.POrderGUID = OrderGUID;
        

        $timeout(function(){  

            $('#myModal').modal({show:true});
         
        }, 500);

       
    }



    $scope.makePayment = function()
    {
        $scope.makePaymentDataLoading = true;

        //var PaymentType = $('input[name=PaymentType]');

        var PaymentType = $("input[name='PaymentType']:checked").val();

        if(PaymentType == 'Offline'){
            var ChequeNumber = $("#ChequeNumber").val();
            var DrawnBank = $("#DrawnBank").val();
            var ChequeDate = $("#ChequeDate").val();
            var ChequeAmount = $("#ChequeAmount").val();
            var DepositedDate = $("#DepositedDate").val();
            var MediaGUIDs = $("#MediaGUIDs").val();
            var OrderGUID = $("#OrderGUID").val();
            var OrderFor = "Keys";
            var data = 'SessionKey='+SessionKey+'&OrderFor='+OrderFor+'&PaymentType='+PaymentType+'&ChequeNumber='+ChequeNumber+'&DrawnBank='+DrawnBank+'&ChequeDate='+ChequeDate+'&ChequeAmount='+ChequeAmount+'&DepositedDate='+DepositedDate+'&OrderGUID='+OrderGUID+'&MediaGUID='+MediaGUIDs;
        }else{
            $scope.makePaymentDataLoading = false;
           ErrorPopup("Sorry! We can not continue without payment details."); 
           return false;
        }

       // console.log(PaymentType); return false;

        //var data = 'SessionKey='+SessionKey+'&'+$("form[name='payment_form']").serialize();

        $http.post(API_URL+'order/makePayment', data, contentType).then(function(response) {

            var response = response.data;

            $scope.makePaymentDataLoading = false;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $('#add_payment .close').click();

                $scope.data.dataList = [];

                $scope.data.noRecords = false;

                $scope.getList(1,'buy_now');

            }else{

                ErrorPopup(response.Message);

            }

                      

        });
    }





    $scope.backOnOrder = function (OrderGUID)

    {
        //alert(OrderGUID);
        $scope.OrderGUID = OrderGUID;

        $('#add_payment .close').click();

        $('#add_model').modal({show:true});

    }



    /*edit data*/

    $scope.editData = function ()

    {

        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='edit_form']").serialize();

        $http.post(API_URL+'api_admin/store/editOrder', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $scope.data.dataList[$scope.data.Position] = response.Data;

                $('.modal-header .close').click();

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });

    }

    $scope.availableKeys = function (validity)

    {

        var data = 'SessionKey='+SessionKey+'&validity='+validity;

        $http.post(API_URL+'keys/availableKeys', data, contentType).then(function(response) {

            var response = response.data;

                if(response.ResponseCode==200 && response.Data){ /* success case */

                 $scope.availableData =  response.Data[0]; console.log($scope.availableData);

                 $timeout(function(){

                    $("select.chosen-select").chosen({ width: '100%',"disable_search_threshold": 8}).trigger("chosen:updated");

                }, 300);          

            }

        });

    }


    $scope.loadFormKeysAllot = function(Position,OrderGUID,OrderDetails,type)

    {
       // $scope.getUser();

        //console.log(OrderDetails);

        $scope.OrderGUID = OrderGUID;

        $scope.availableData.AvailableKeys = 0;

        $scope.templateURLAllot = PATH_TEMPLATE+'order/allot_keys.htm?'+Math.random();

        $scope.OrderDetails = OrderDetails;

        $scope.keysType = type;

        $('#allot_key_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);
    }


    $scope.loadFormSMSAllot = function(Position,OrderGUID,OrderDetails)

    {
       // $scope.getUser();

        //console.log(OrderDetails);

        $scope.OrderGUID = OrderGUID;

        $scope.availableData.AvailableKeys = 0;

        $scope.templateURLAllot = PATH_TEMPLATE+'order/allot_sms.htm?'+Math.random();

        $scope.OrderDetails = OrderDetails;

        $('#allot_sms_model').modal({show:true});

        $timeout(function(){            

           $(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 200);
    }


    $scope.loadPaymentDetails = function(Position,OrderGUID,OrderFor,PaymentDetails)

    {
       // $scope.getUser();

        //console.log(OrderDetails);
        $scope.Position = Position;

        $scope.OrderGUID = OrderGUID;

        $scope.OrderFor = OrderFor;

        $scope.availableData.AvailableKeys = 0;

        $scope.PaymentDetails = PaymentDetails;

        $scope.templateURLPaymentDetails = PATH_TEMPLATE+'order/view_payment_to_verify.htm?'+Math.random();

        $('#payment_details_model').modal({show:true});
    }


    $scope.loadPaymentVerifyForm = function(keyposition,OrderGUID,PaymentGUID,OrderFor)

    {
       // $scope.getUser();

        //console.log(OrderDetails);

        $("#payment_details_model  .close").click();

        $scope.OrderGUID = OrderGUID;

        $scope.keyposition = keyposition;

        $scope.PaymentGUID = PaymentGUID;

        $scope.OrderFor = OrderFor;

        $scope.templateURLVerify = PATH_TEMPLATE+'order/verify_order.htm?'+Math.random();

        $('#order_verify_model').modal({show:true});

        $timeout(function(){ 
            var d = new Date();

           $("#CreditedOn").datetimepicker({format: 'dd-mm-yyyy',minView: 2});           

           //$(".chosen-select").chosen({ width: '100%',"disable_search_threshold": 8 ,"placeholder_text_multiple": "Please Select",}).trigger("chosen:updated");

       }, 1000);
    }


    $scope.NotVerifyOrder = function()
    {
        $('#payment_details_model').modal({show:true});
    }



    $scope.verifyOrder = function()
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='verify_form']").serialize();

        $http.post(API_URL+'order/verifyOrder', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                //$scope.data.dataList[$scope.data.Position] = response.Data;

                $('#order_verify_model .close').click();

                $scope.data.dataList = [];

                $scope.getList(1,'manage_order');

                 // $timeout(function(){ 

                 //    $("#PaymentDetails"+$scope.Position).trigger("click");
                 // }, 1000);

                //$scope.loadPaymentDetails($scope.Position,$scope.OrderGUID,$scope.OrderFor,$scope.PaymentDetails);

                $('#payment_details_model').modal({show:true});

                $("#Verify"+$scope.keyposition).hide();

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });
    }





    $scope.AllotSMSCredits = function(Position,OrderGUID)
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='allot_sms_form']").serialize();

        $http.post(API_URL+'order/allotOrderedSMS', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                //$scope.data.dataList[$scope.data.Position] = response.Data;

                $('.modal-header .close').click();

                $scope.data.dataList = [];

                $scope.getList(1,'manage_order');

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });
    }


    $scope.AllotKeys = function(Position,OrderGUID)
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='allot_keys_form']").serialize();

        $http.post(API_URL+'order/allotOrderedKeys', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $('#allot_key_model .close').click();

                $scope.data.dataList = [];

                $scope.getList(1,'manage_order');

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });
    }


    $scope.UnallotKeys = function(Position,OrderGUID)
    {
        $scope.editDataLoading = true;

        var data = 'SessionKey='+SessionKey+'&'+$("form[name='allot_keys_form']").serialize();

        $http.post(API_URL+'order/unallotOrderedKeys', data, contentType).then(function(response) {

            var response = response.data;

            if(response.ResponseCode==200){ /* success case */               

                SuccessPopup(response.Message);

                $('#allot_key_model .close').click();

                $scope.data.dataList = [];

                $scope.getList(1,'manage_order');

            }else{

                ErrorPopup(response.Message);

            }

            $scope.editDataLoading = false;          

        });
    }


    $scope.getDiscountCoupon = function(OrderGUID)
    {
        var data = 'SessionKey='+SessionKey+'&OrderGUID='+OrderGUID+'&OrderFor=Keys';

        $http.post(API_URL+'order/getOrders', data, contentType).then(function(response) 
        {
            var response = response.data;            

            if(response.ResponseCode==200 && response.Data)
            { 
                $scope.Order_details = response.Data[0].Order_details;
                $scope.IsDiscount = 0;
                $scope.DiscountOnAmount = 0;
                if($scope.Order_details)
                {
                    for (var i in $scope.Order_details) 
                    {
                        if($scope.Order_details[i].Validity == 12 || $scope.Order_details[i].Validity == 24)
                        {
                            $scope.IsDiscount = 1;
                            $scope.DiscountOnAmount = $scope.DiscountOnAmount + $scope.Order_details[i].Price;
                        }
                    }
                }

                
            }
        });
    }



}); 



function getTotalPriceByValidity(Validity){

    var TotalKeys = $("#KeyCount").val();

    angular.element(document.getElementById('order-body')).scope().calculateKeysPrice(Validity,TotalKeys);

}



function getTotalPrice(TotalKeys,Validity){

    var isChecked = $("#validity"+Validity).is(':checked');

    if(!isChecked){

        alert("Please select validity before keys.");

        $("#key"+Validity).val('');

        $("#price"+Validity).val('');

    }else{

        angular.element(document.getElementById('order-body')).scope().calculateKeysPrice(Validity,TotalKeys); 

    }

}


function ChangePrice(Validity){
    var isChecked = $("#validity"+Validity).is(':checked');

    if(!isChecked){

        var price = $("#price"+Validity).val();

        $("#key"+Validity).val('');

        $("#price"+Validity).val('');

        var TotalPrice = $("#TotalPrice").val();

        var TotalPrice = parseInt(TotalPrice)-parseInt(price);

        console.log(TotalPrice);  

        $("#TotalPrice").val(TotalPrice);


    }
}

function display_payment_form(payment_type) {
   // alert(payment_type);
    if(payment_type == 'Offline'){
        $("#modal_footer").show();
        $("#online_payment_form").hide();
        $("#direct_payment_form").hide(); 
        $("#offline_payment_form").toggle();
        var currentTime = new Date();  
        $('input[name=ChequeDate]').datetimepicker({format: 'dd-mm-yyyy',minView: 2});
        //$('input[name=DepositedDate]').datetimepicker({format: 'dd-mm-yyyy',minView: 2});
    }else if(payment_type == 'Direct'){
        $("#modal_footer").show();
        $("#online_payment_form").hide();
        $("#offline_payment_form").hide();
        $("#direct_payment_form").toggle(); 
        var currentTime = new Date();  
       // $('input[name=DepositedOn]').datetimepicker({format: 'dd-mm-yyyy',minView: 2});       
    }else{
        $("#modal_footer").hide();
        $("#online_payment_form").show();
       $("#direct_payment_form").hide();
       $("#offline_payment_form").hide(); 
       //$("#continue_payment").trigger("click");
    }
}

function getOrders(OrderType,UserGUID){
    angular.element(document.getElementById('order-body')).scope().applyFilter(1,OrderType,UserGUID); 
}

$( document ).ready(function() {
    $('#CreditedOn').on('click', function(e) {
       e.preventDefault();
       $(this).attr("autocomplete", "off");  
    });
});

function setDepositeDate(date){ //alert(date);
    var d = new Date();

    var month = d.getMonth();

    var day = d.getDate();

    var year = d.getFullYear();

    $("#DepositedDate").datetimepicker({format: 'dd-mm-yyyy',minView: 2}).datetimepicker('setStartDate',date).datetimepicker('setEndDate',d);

    //$('input[name=DepositedDate]').datetimepicker({pickDate: false });
    //$('input[name=DepositedDate]').datepicker({maxDate: date});
}


function CheckAmount(givenAmount){
	var OrderAmount = $("#OrderAmount").val();
	if(givenAmount.length > 0 && !$.isNumeric(givenAmount)){
		alert("Sorry! Only numbers are allowed");
		$("#ChequeAmount").val(OrderAmount);
	}
	else if(parseInt(givenAmount) > parseInt(OrderAmount)){
		alert("Sorry! Amount can not be greater than "+OrderAmount);
		$("#ChequeAmount").val(OrderAmount);
	}else if(parseInt(givenAmount) < 0){
		alert("Sorry! Amount can not be less than "+OrderAmount);
		$("#ChequeAmount").val(OrderAmount);
	}

}

// $( document ).ready(function() {
//     $("#ChequeDate").datepicker({
// 	    onClose: function () {
// 	        $("#DepositedDate").datepicker(
// 	            "change", {
// 	            minDate: new Date($('#DepositedDate').val())
// 	        });
// 	    }
// 	});

// 	$("#DepositedDate").datepicker({
// 	    onClose: function () {
// 	        $("#DepositedDate").datepicker(
// 	            "change", {
// 	            maxDate: new Date($('#ChequeDate').val())
// 	        });
// 	    }
// 	});
// });
(function () {
    var m = {
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        s = {
            array: function (x) {
                var a = ['['], b, f, i, l = x.length, v;
                for (i = 0; i < l; i += 1) {
                    v = x[i];
                    f = s[typeof v];
                    if (f) {
                        v = f(v);
                        if (typeof v == 'string') {
                            if (b) {
                                a[a.length] = ',';
                            }
                            a[a.length] = v;
                            b = true;
                        }
                    }
                }
                a[a.length] = ']';
                return a.join('');
            },
            'boolean': function (x) {
                return String(x);
            },
            'null': function (x) {
                return "null";
            },
            number: function (x) {
                return isFinite(x) ? String(x) : 'null';
            },
            object: function (x) {
                if (x) {
                    if (x instanceof Array) {
                        return s.array(x);
                    }
                    var a = ['{'], b, f, i, v;
                    for (i in x) {
                        v = x[i];
                        f = s[typeof v];
                        if (f) {
                            v = f(v);
                            if (typeof v == 'string') {
                                if (b) {
                                    a[a.length] = ',';
                                }
                                a.push(s.string(i), ':', v);
                                b = true;
                            }
                        }
                    }
                    a[a.length] = '}';
                    return a.join('');
                }
                return 'null';
            },
            string: function (x) {
                if (/["\\\x00-\x1f]/.test(x)) {
                    x = x.replace(/([\x00-\x1f\\"])/g, function(a, b) {
                        var c = m[b];
                        if (c) {
                            return c;
                        }
                        c = b.charCodeAt();
                        return '\\u00' +
                            Math.floor(c / 16).toString(16) +
                            (c % 16).toString(16);
                    });
                }
                return '"' + x + '"';
            }
        };

  /*  Object.prototype.toJSONString = function () {
        return s.object(this);
    };*/

    Array.prototype.toJSONString = function () {
        return s.array(this);
    };
})();

String.prototype.parseJSON = function () {
    try {
        return !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(
                this.replace(/"(\\.|[^"\\])*"/g, ''))) &&
            eval('(' + this + ')');
    } catch (e) {
        return false;
    }
};

function initRequest(url) 
{
    if (window.XMLHttpRequest) 
    {
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) 
    {
        isIE = true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}