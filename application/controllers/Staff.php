<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff extends Admin_Controller_Secure {
	
	/*------------------------------*/
	/*------------------------------*/	
	public function index()
	{
		$load['css']=array(
			'asset/plugins/chosen/chosen.min.css',
			'asset/plugins/datepicker/css/bootstrap-datetimepicker.css',
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
		);
		
		$load['js']=array(
			'asset/js/'.$this->ModuleData['ModuleName'].'.js',
			'asset/plugins/chosen/chosen.jquery.min.js',
			'asset/plugins/jquery.form.js',	
			'asset/plugins/datepicker/js/bootstrap-datetimepicker.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/jquery.tinymce.min.js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
		);

		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Staff" => ""			
		);		

		$this->load->view('includes/header',$load);
		$this->load->view('includes/menu');
		$this->load->view('staff/staff_list');
		$this->load->view('includes/footer');
	}


	public function keys()

	{

		$load['css']=array(

			'asset/plugins/chosen/chosen.min.css'

		);

		$load['js']=array(

			'asset/js/staff.js',

			'asset/plugins/chosen/chosen.jquery.min.js',

			'asset/plugins/jquery.form.js',

		);	


		$load['breadcrumb']=array(
			"Dashboard" => base_url()."dashboard",
			"Manage Staff" => base_url()."staff",
			"Assign Keys" => ""
		);	



		$this->load->view('includes/header',$load);

		$this->load->view('includes/menu');

		$this->load->view('staff/assign_staff_keys');

		$this->load->view('includes/footer');

	}
}
