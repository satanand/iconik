<header class="panel-heading">
<h1 class="h4">Manage Profile</h1>
</header>
<?php $this->load->view('includes/breadcrumb'); ?>
<div class="panel-body" ng-controller="PageController" ng-init="getFilterData()">
<p ng-if="data.listLoading" class="text-center data-loader"><img src="asset/img/loader.svg"></p>
<div class="row">
<div class="col-md-12">
<div class="panel panel-default card-view">
<div class="panel-wrapper">
<div class="panel-body">
   <div class="row" >
      <div class="col-sm-12 col-xs-12">
         <div class="form-wrap">
            <!--   <form action="#" class="form-horizontal"> -->
            <div class="form-body">
               <!-- <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Institute Info</h6> 
                  <hr/>-->
               <div class="row">
                  <div class="col-md-12">
                     <img src="{{UserData.ProfilePic}}" alt="Logo" class="thumbnail rounded-circle" style="float: left;">

                     <button type="button"  ng-if="UserData.UserTypeName.includes('Institute')" class="btn btn-info btn-icon left-icon  mr-10" ng-click="loadFormEdit(UserData.UserID,UserData.UserGUID)" style="float: right;" >Update Profile</button>

                     <button type="button" ng-if="!UserData.UserTypeName.includes('Institute') && (data.SecondLevelPermission.indexOf('all') == 0 || data.SecondLevelPermission.indexOf('all') > 0)" class="btn btn-info btn-icon left-icon  mr-10" ng-click="loadFormEditStaff(UserData.UserID,UserData.UserGUID)" style="float: right;" >Update Profile</button> 
                     <!-- <img src="dist/img/user1.png" alt="user_auth" class="user-auth-img img-circle"> -->
                  </div>
               </div>
               <div class="clear" >&nbsp;</div>
               <div ng-if="UserData.UserTypeName=='Institute' || UserData.UserTypeName=='Market Place Institute'">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label  class="control-label">Institute Name:</label></label>
                           <p class="form-control-static"> {{UserData.FirstName}}</p>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Registration No:</label>
                           <div class="">
                              <p class="form-control-static"> {{UserData.RegistrationNumber}} </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- /Row -->
                  <div class="clear">&nbsp;</div>
                  <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>Postal Details</h6>
                  
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Address:</label>
                           <div class="">
                              <p class="form-control-static"> {{UserData.Address}}
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Country:</label>
                           <div class="">
                              <p class="form-control-static">India</p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">State:</label>
                           <div class="">
                              <p class="form-control-static"> {{UserData.StateName}}</p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">City:</label>
                           <div class="">
                              <p class="form-control-static"> {{UserData.CityName}} </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->                                                
                  </div>
                  <!-- /Row -->
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Zip Code:</label>
                           <div class="">
                              <p class="form-control-static">{{UserData.Postal}}</p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Email address:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.EmailForChange!=''"> <a href="mailto:{{UserData.EmailForChange}}">{{UserData.EmailForChange}}</a></p>
                              <p class="form-control-static" ng-if="UserData.EmailForChange==''"> <a href="mailto:{{UserData.Email}}">{{UserData.Email}}</a></p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <!-- /Row -->
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Telephone Number:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.TelePhoneNumber"> {{UserData.CityCode}} - {{UserData.TelePhoneNumber}} </p>
                              <p class="form-control-static" ng-if="!UserData.TelePhoneNumber"> - </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Mobile No:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.PhoneNumberForChange==''"> +91-{{UserData.PhoneNumber}} </p>
                              <p class="form-control-static" ng-if="UserData.PhoneNumberForChange!=''"> +91-{{UserData.PhoneNumberForChange}} </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <!-- /Row -->
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Website:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.Website">
                                 <a target="_blank" href="{{UserData.UrlType}}{{UserData.Website}}">{{UserData.UrlType}}{{UserData.Website}}</a></p>
                              <p class="form-control-static" ng-if="!UserData.Website"> - </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                   <hr/>
                  <div class="clear">&nbsp;</div>
                  <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>Other Details</h6>
                  
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Owner Name:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.OwnerName.length" ng-repeat="(key,row) in UserData.OwnerName track by $index">{{key+1}}. {{row}}</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Teaching Aids:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.TeachingAids.length" ng-repeat="(key,row) in UserData.TeachingAids">{{key+1}}. {{row}}</p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Achievement:</label>
                           <div class="">
                              <p class="form-control-static"> {{UserData.Achivements}}</p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Scholarship Test Offered:</label>
                           <div class="">
                              <p class="form-control-static"> {{UserData.ScholarshipTest}} </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->                                                
                  </div>
                  <!-- /Row -->
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">No of Classrooms:</label>
                           <div class="">
                              <p class="form-control-static">{{UserData.Classrooms}}</p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Library Facility:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.Library!=''"> {{UserData.Library}} </p>
                              <p class="form-control-static" ng-if="UserData.Library==''"> - </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <!-- /Row -->
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Hostel Facility:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.HostelFacility"> {{UserData.HostelFacility}} </p>
                              <p class="form-control-static" ng-if="!UserData.HostelFacility"> - </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Mess Facility:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.MessFacility==''"> - </p>
                              <p class="form-control-static" ng-if="UserData.MessFacility!=''"> {{UserData.MessFacility}} </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <!-- /Row -->
                  <div class="row" ng-if="UserData.HostelFacility">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Number of Rooms:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.NumberOfRooms">{{UserData.NumberOfRooms}}</p>
                              <p class="form-control-static" ng-if="!UserData.NumberOfRooms"> - </p>
                           </div>
                        </div>
                     </div>
                      <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Students/Room:</label>
                           <div class="">
                              <p class="form-control-static" ng-if="UserData.StudentPerRooms">{{UserData.StudentPerRooms}}</p>
                              <p class="form-control-static" ng-if="!UserData.StudentPerRooms"> - </p>
                           </div>
                        </div>
                     </div>
                     <!--/span-->
                  </div>
                  <!-- /Row -->   
                  <hr class="light-grey-hr"/>  
                  <div class="clear">&nbsp;</div>
                  <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>Social Profile</h6>
                  
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="input-group"> 
                           <span class="input-group-btn">
                           <button type="button" class="btn btn-icon-anim btn-facebook btn-square"><span class="fa fa-facebook"></span></button>
                           </span>
                           <label class="form-control"  style="border: 0px none; background-color: #fff; box-shadow: 0px 0px 0px #fff !important;">{{UserData.FacebookURL}}</label>
                           
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="input-group"> 
                           <span class="input-group-btn">
                           <button type="button" class="btn btn-icon-anim btn-twitter btn-square"><span class="fa fa-twitter"></span></button>
                           </span> 
                           <label class="form-control"  style="border: 0px none; background-color: #fff; box-shadow: 0px 0px 0px #fff !important;">{{UserData.TwitterURL}}</label>
                        </div>
                     </div>
                  </div>
                  <div class="clear">&nbsp;</div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="input-group"> 
                           <span class="input-group-btn">
                           <button class="btn btn-linkedin btn-icon-anim btn-square"><span class="fa fa-linkedin"></span></button>
                           </span>
                           <label class="form-control"  style="border: 0px none; background-color: #fff; box-shadow: 0px 0px 0px #fff !important;">{{UserData.LinkedInURL}}</label>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="input-group"> 
                           <span class="input-group-btn">
                           <button class="btn btn-googleplus btn-icon-anim btn-square"><span class="fa fa-google-plus"></span></button>
                           </span>
                           <label class="form-control"  style="border: 0px none; background-color: #fff; box-shadow: 0px 0px 0px #fff !important;">{{UserData.GoogleURL}}</label>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- staff view -->
               <div ng-if="UserData.UserTypeName!='Institute' && UserData.UserTypeName!='Market Place Institute'">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">First Name </label><br>
                           {{UserData.FirstName}}
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Last Name </label><br>
                           {{UserData.LastName}}
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Email</label><br>
                           {{UserData.Email}}
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Role </label><br>
                           {{UserData.UserTypeName}}
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">Admin Panel Access</label><br> 
                           {{UserData.AdminAccess}}
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label">App Panel Access</label><br>{{UserData.AppAccess}}
                        </div>
                     </div>
                  </div>
                  <div>
                     <div ng-if="UserData.AdminAccess=='No'">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" data-toggle="tab" href="#PersonalDetailsv" role="tab" aria-controls="PersonalDetailsv">Personal Details</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#EmergencyDetailsv" role="tab" aria-controls="EmergencyDetailsv">Emergency</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#NomineeDetailsv" role="tab" aria-controls="NomineeDetailsv">Nominee</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#Eductionv" role="tab" aria-controls="Eductionv">Qualification</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#WorkExperiencev" role="tab" aria-controls="WorkExperiencev">Work Experience</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#JobsDetailsv" role="tab" aria-controls="JobsDetailsv">Jobs Details</a>
                           </li>
                           <li class="nav-item" ng-if="UserData.UserTypeID == 11" id="subject_tabs">
                              <a class="nav-link" data-toggle="tab" href="#Subjectv" role="tab" aria-controls="Subjectv">Subject</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" data-toggle="tab" href="#SocialMediav" role="tab" aria-controls="SocialMediav">Social Media</a>
                           </li>
                        </ul>
                        <div class="tab-content" style="margin:15px;">
                           <!-- Personal details-->
                           <div class="tab-pane active" id="PersonalDetailsv" role="tabpanel">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Contact No.</label><br>
                                       +91- {{UserData.PhoneNumber}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Gender</label><br>
                                       {{UserData.Gender}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label" ng-if="UserData.BirthDate">Date of Birth</label></label><br>
                                       {{UserData.BirthDate|date:'dd-MM-yyyy'}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Marital Status</label><br>
                                       <p ng-if="UserData.MaritalStatus!='? undefined'">{{UserData.MaritalStatus}}</p>
                                    </div>
                                 </div>
                              </div>
                              <div class="row" ng-if="MaritalStatus == 'married'">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label" ng-if="UserData.AnniversaryDate">Anniversary Date</label></label><br>
                                       {{UserData.AnniversaryDate|date:'dd-MM-yyyy'}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Address</label><br>
                                       {{UserData.Address}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">State</label><br>
                                       {{UserData.StateName}}
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label">City</label><br>
                                       {{UserData.CityName}}
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label">ZIP Code</label><br>
                                       {{UserData.CityCode}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Emergency Details-->
                           <div class="tab-pane" id="EmergencyDetailsv" role="tabpanel">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Name of Person</label><br>
                                       <div class="input-group mb-3">
                                          {{UserData.Emergency}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Relation with Person</label><br>
                                       {{UserData.RelationWithEmergency}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Contact No.</label>
                                       <div class="input-group mb-3">
                                          +91-{{UserData.EmergencyPhoneNumber}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Gender</label><br>
                                       {{UserData.EmergencyGender}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Address</label><br>
                                       {{UserData.EmergencyAddress}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">State</label><br>
                                       {{UserData.EmergencyState}}
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label">City</label><br>
                                       {{UserData.EmergencyCityName}}
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label">ZIP Code</label><br>
                                       {{UserData.EmergencyCityCode}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- NomineeDetailsv Details-->
                           <div class="tab-pane" id="NomineeDetailsv" role="tabpanel">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Nominee</label><br>
                                       <div class="input-group mb-3">
                                          {{UserData.Nominee}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Relation with nominee</label><br>
                                       {{UserData.RelationWithNominee}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Contact No.</label><br>
                                       <div class="input-group mb-3">
                                          +91-{{UserData.NomineePhoneNumber}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Gender</label><br>
                                       {{UserData.NomineeGender}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Address</label><br>
                                       {{UserData.NomineeAddress}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">State</label><br>
                                       {{UserData.NomineeState}}
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label">City</label><br>
                                       {{UserData.NomineeCityName}}
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label">ZIP Code</label><br>
                                       {{UserData.NomineeCityCode}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Eductionv Qualification-->
                           <div class="tab-pane" id="Eductionv" role="tabpanel">
                              <div id="dynamic_field" ng-repeat="(key,ques) in UserData.Work_eduction">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Qualification</label><br>
                                          {{ques.Qualification}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Specialization</label><br>
                                          {{ques.Specialization}}
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Univercity / Institute</label><br>
                                          {{ques.Univercity}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Passing Out Year</label><br>
                                          {{ques.PassingYear}}
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Percentage %</label><br>
                                          {{ques.Percentage}}
                                       </div>
                                    </div>
                                 </div>
                                 <hr>
                              </div>
                           </div>
                           <!-- job Profile-->
                           <div class="tab-pane" id="JobsDetailsv" role="tabpanel">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Job</label><br>
                                       {{UserData.Job}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Department</label><br>
                                       {{UserData.DepartmentName}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Designation</label><br>
                                       {{UserData.Designation}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Reporting Manager</label><br>
                                       {{UserData.ReportingManager}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Joining Date</label><br>
                                       {{UserData.JoiningDate | date:'dd-MM-yyyy'}}
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Appraisal Date</label><br>
                                       {{UserData.AppraisalDate | date:'dd-MM-yyyy'}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- Work Experience-->
                           <div class="tab-pane" id="WorkExperiencev" role="tabpanel" >
                              <div id="dynamic_field_exp" ng-repeat="(key,exp) in UserData.Work_experience">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Employer</label><br>
                                          {{exp.Employer}}
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Industries</label><br>
                                          {{exp.IndustryName}}
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Position</label><br>
                                          {{exp.ExpDesignation}}
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Job Profile</label><br>
                                          {{exp.JobProfile}}
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">Started Working Form</label><br>
                                          {{exp.From | date:'dd-MM-yyyy'}}
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="form-group">
                                          <label class="control-label">To</label><br>
                                          {{exp.To | date:'dd-MM-yyyy'}}
                                       </div>
                                    </div>
                                 </div>
                                 <hr>
                              </div>
                           </div>
                           <!--Social Media-->
                           <div class="tab-pane" id="SocialMediav" role="tabpanel">
                              <div class="row">
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                       <span class="fa fa-facebook"></span>  {{UserData.FacebookURL}}
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                       <span class="fa fa-twitter"></span>  {{UserData.TwitterURL}}
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                       <span class="fa fa-linkedin"></span>  {{UserData.LinkedInURL}}
                                    </div>
                                 </div>
                                 <div class="col-sm-6">
                                    <div class="form-group">
                                       <span class="fa fa-google-plus"></span>  {{UserData.GoogleURL}}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--course Subjectv-->
                           <div class="tab-pane" id="Subjectv" role="tabpanel">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Coures</label><br>
                                       <div class="col-md-4" ng-repeat="Course in UserData.Courses">{{Course.CategoryName}}</div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label">Subjects</label><br>
                                       <div class="row">
                                          <div class="col-md-6" ng-repeat="sub in UserData.Course_subjects">
                                             {{sub.CategoryName}}
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <br>
               <div class="form-actions mt-10">
                  <!-- <a href=""> <i class="zmdi zmdi-edit"></i> <span ng-click="loadFormEdit(UserData.UserGUID)">Update</span></a> --><!-- </button>
                     <button type="button" class="btn btn-default">Cancel</button> -->
               </div>
            </div>
            <div class="clear">&nbsp;</div>
         </div>
         <!--   </form> -->
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- Top container/ -->
<!-- Data table -->
<!-- Data table/ -->
<!-- edit Modal -->
<div class="modal fade" id="edit_model">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h3 class="modal-title h5">Update <?php echo $this->ModuleData['ModuleName'];?></h3>
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<!-- form -->
<div ng-include="templateURLEdit">
</div>
<!-- /form -->
</div>
</div>
</div>
<!-- edit Modal -->
<div class="modal fade" id="staffedit_model">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content">
<div class="modal-header">
<h3 class="modal-title h5">Update <?php echo $this->ModuleData['ModuleName'];?></h3>
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<!-- form -->
<div ng-include="templateURLSaffEdit">
</div>
<!-- /form -->
</div>
</div>
</div>
<!-- delete Modal -->
<div class="modal fade" id="delete_model">
<div class="modal-dialog modal-md" role="document">
<div class="modal-content">
<div class="modal-header">
<h3 class="modal-title h5">Delete <?php echo $this->ModuleData['ModuleName'];?></h3>
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<!-- form -->
<form id="edit_form" name="edit_form" autocomplete="off" ng-include="templateURLDelete">
</form>
<!-- /form -->
</div>
</div>
</div>
<!-- Body/ -->
</div>
<!-- Body -->